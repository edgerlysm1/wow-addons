
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 26,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008051,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Defias Cutpurse"] = true,
							},
							["targets"] = {
								["Defias Cutpurse"] = 126,
							},
							["colocacao"] = 1,
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 126.008051,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 126.008051,
							["classe"] = "PALADIN",
							["serial"] = "Player-4408-01FD226F",
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 4,
										["n_max"] = 10,
										["targets"] = {
											["Defias Cutpurse"] = 96,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 64,
										["n_min"] = 9,
										["g_dmg"] = 32,
										["counter"] = 12,
										["total"] = 96,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Defias Cutpurse"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["end_time"] = 1589504491,
							["last_dps"] = 3.46537734448041,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 93.008051,
							["start_time"] = 1589504453,
							["delay"] = 0,
							["last_event"] = 1589504489,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.001196,
							["monster"] = true,
							["damage_from"] = {
								["Dethrak"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 93,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 93.001196,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 93.001196,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-94-00003DDEAE",
							["nome"] = "Defias Cutpurse",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Dethrak"] = 0,
											["Thorddin"] = 93,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 93,
										["MISS"] = 1,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 19,
										["a_amt"] = 0,
										["total"] = 93,
										["c_max"] = 0,
										["DODGE"] = 3,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 14,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504491,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 245.001196,
							["start_time"] = 1589504453,
							["delay"] = 0,
							["last_event"] = 1589504488,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 26,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorb"] = 0.007488,
							["last_hps"] = 0,
							["targets_overheal"] = {
							},
							["targets"] = {
								["Thorddin"] = 44,
							},
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									["Holy Light"] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 44,
										["targets"] = {
											["Thorddin"] = 44,
										},
										["n_min"] = 44,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 44,
										["c_max"] = 0,
										["id"] = "Holy Light",
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 44,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["heal_enemy_amt"] = 0,
							["totalover"] = 0.007488,
							["total_without_pet"] = 44.007488,
							["end_time"] = 1589504491,
							["totalover_without_pet"] = 0.007488,
							["fight_component"] = true,
							["total"] = 44.007488,
							["healing_taken"] = 44.007488,
							["serial"] = "Player-4408-01FD226F",
							["nome"] = "Thorddin",
							["targets_absorbs"] = {
							},
							["grupo"] = true,
							["classe"] = "PALADIN",
							["heal_enemy"] = {
							},
							["start_time"] = 1589504490,
							["custom"] = 0,
							["tipo"] = 2,
							["on_hold"] = false,
							["totaldenied"] = 0.007488,
							["delay"] = 1589504478,
							["last_event"] = 1589504478,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 26,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 26,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 57,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Holy Light"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 19,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 38,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504491,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 26,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1589504453,
				["enemy"] = "Defias Cutpurse",
				["combat_counter"] = 29,
				["playing_solo"] = true,
				["totals"] = {
					218.996234, -- [1]
					44, -- [2]
					{
						-0.00577000000000005, -- [1]
						[0] = -0.0051819999999978,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 42186.435,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "21:00:53",
				["end_time"] = 42186.435,
				["totals_grupo"] = {
					126, -- [1]
					44, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 26,
				["TotalElapsedCombatTime"] = 42186.435,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 44.007488,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 126.008051,
						}, -- [1]
					},
				},
				["frags"] = {
					["Defias Cutpurse"] = 2,
				},
				["data_fim"] = "21:01:31",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 42148.419,
				["contra"] = "Defias Cutpurse",
				["TimeData"] = {
				},
			}, -- [1]
			{
				{
					["combatId"] = 25,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002279,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 72,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 72.002279,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1589504441,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Mangy Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Thorddin"] = 72,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 72,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 17,
										["total"] = 72,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 17,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 72.002279,
							["serial"] = "Creature-0-4411-0-12-525-00003DDE5F",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1589504421,
							["damage_taken"] = 102.002279,
							["start_time"] = 1589504413,
							["delay"] = 1589504421,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002743,
							["damage_from"] = {
								["Mangy Wolf"] = true,
								["Young Wolf"] = true,
							},
							["targets"] = {
								["Mangy Wolf"] = 102,
								["Young Wolf"] = 45,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 147.002743,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504441,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 40,
										["g_amt"] = 2,
										["n_max"] = 12,
										["targets"] = {
											["Mangy Wolf"] = 76,
											["Young Wolf"] = 35,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 54,
										["n_min"] = 9,
										["g_dmg"] = 17,
										["counter"] = 10,
										["total"] = 111,
										["c_max"] = 20,
										["a_amt"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 20,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Mangy Wolf"] = 26,
											["Young Wolf"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 36,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 36,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 147.002743,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 3.21198118731839,
							["custom"] = 0,
							["last_event"] = 1589504440,
							["damage_taken"] = 92.002743,
							["start_time"] = 1589504395,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 2600,
							["totalabsorbed"] = 0.004423,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 20,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 20.004423,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 20.004423,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-299-00003DDE5F",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorddin"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 21,
										["total"] = 20,
										["c_max"] = 0,
										["MISS"] = 3,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 18,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504441,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 45.004423,
							["start_time"] = 1589504400,
							["delay"] = 0,
							["last_event"] = 1589504440,
						}, -- [3]
					},
				}, -- [1]
				{
					["combatId"] = 25,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorb"] = 0.006336,
							["last_hps"] = 0,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 85,
							},
							["targets_absorbs"] = {
							},
							["pets"] = {
							},
							["totalover_without_pet"] = 0.006336,
							["targets_overheal"] = {
							},
							["classe"] = "PALADIN",
							["totalover"] = 0.006336,
							["total_without_pet"] = 85.006336,
							["iniciar_hps"] = false,
							["heal_enemy_amt"] = 0,
							["fight_component"] = true,
							["end_time"] = 1589504441,
							["serial"] = "Player-4408-01FD226F",
							["healing_taken"] = 85.006336,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									["Holy Light"] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 45,
										["targets"] = {
											["Thorddin"] = 85,
										},
										["n_min"] = 40,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 85,
										["c_max"] = 0,
										["id"] = "Holy Light",
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 85,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 85.006336,
							["heal_enemy"] = {
							},
							["start_time"] = 1589504440,
							["custom"] = 0,
							["last_event"] = 1589504428,
							["on_hold"] = false,
							["totaldenied"] = 0.006336,
							["delay"] = 1589504428,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 25,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 25,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 86,
							["spell_cast"] = {
								["Seal of Righteousness"] = 2,
								["Holy Light"] = 2,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 39,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 47,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504441,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 25,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 42147.678,
				["tempo_start"] = 1589504394,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 28,
				["playing_solo"] = true,
				["totals"] = {
					239, -- [1]
					85, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					147, -- [1]
					85, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "21:00:41",
				["cleu_timeline"] = {
				},
				["enemy"] = "Mangy Wolf",
				["TotalElapsedCombatTime"] = 42136.373,
				["CombatEndedAt"] = 42136.373,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 85.006336,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 147.002743,
						}, -- [1]
					},
				},
				["end_time"] = 42136.373,
				["combat_id"] = 25,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "20:59:54",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Mangy Wolf"] = 1,
					["Young Wolf"] = 1,
				},
				["start_time"] = 42089.816,
				["TimeData"] = {
				},
				["contra"] = "Mangy Wolf",
			}, -- [2]
			{
				{
					["combatId"] = 24,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007664,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 50,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 50.007664,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504235,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Kobold Vermin"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 42,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 42,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Kobold Vermin"] = 8,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 8,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 8,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 50.007664,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 4.94733518005532,
							["custom"] = 0,
							["last_event"] = 1589504233,
							["damage_taken"] = 6.007664,
							["start_time"] = 1589504225,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006475,
							["damage_from"] = {
								["Edarien"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.006475,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.006475,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE7D8",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Edarien"] = 0,
											["Thorddin"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 6,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504235,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 71.006475,
							["start_time"] = 1589504225,
							["delay"] = 0,
							["last_event"] = 1589504245,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 24,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 24,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 24,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 6,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504235,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 24,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 42088.987,
				["tempo_start"] = 1589504225,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 27,
				["playing_solo"] = true,
				["totals"] = {
					55.96753, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					50, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:57:15",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 10.1080000000002,
				["CombatEndedAt"] = 41930.323,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 50.007664,
						}, -- [1]
					},
				},
				["end_time"] = 41930.323,
				["combat_id"] = 24,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:57:05",
				["start_time"] = 41920.215,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [3]
			{
				{
					["combatId"] = 23,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004313,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 44,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 44.004313,
							["friendlyfire"] = {
							},
							["end_time"] = 1589504223,
							["dps_started"] = false,
							["total"] = 44.004313,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Kobold Vermin"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 14,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 6.00086090276936,
							["custom"] = 0,
							["last_event"] = 1589504221,
							["damage_taken"] = 1.004313,
							["start_time"] = 1589504214,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00865,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 1,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["end_time"] = 1589504223,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 1.00865,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 1.00865,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE84B",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Thorddin"] = 1,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1,
										["c_max"] = 0,
										["DODGE"] = 3,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 44.00865,
							["start_time"] = 1589504219,
							["delay"] = 0,
							["last_event"] = 1589504221,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 23,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 23,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 23,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504223,
							["buff_uptime"] = 9,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 23,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41910.454,
				["tempo_start"] = 1589504214,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 26,
				["playing_solo"] = true,
				["totals"] = {
					44.994818, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					44, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:57:03",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 7.74500000000262,
				["CombatEndedAt"] = 41918.199,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 44.004313,
						}, -- [1]
					},
				},
				["end_time"] = 41918.199,
				["combat_id"] = 23,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:56:55",
				["start_time"] = 41910.051,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [4]
			{
				{
					["combatId"] = 22,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007133,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 56,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 56.007133,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504212,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 2,
										["n_max"] = 11,
										["targets"] = {
											["Kobold Vermin"] = 39,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 10,
										["g_dmg"] = 18,
										["counter"] = 4,
										["total"] = 39,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 56.007133,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.4705150420017,
							["custom"] = 0,
							["last_event"] = 1589504211,
							["damage_taken"] = 10.007133,
							["start_time"] = 1589504201,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001372,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 10,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.001372,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 10.001372,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-0000BDE7BE",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorddin"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504212,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 56.001372,
							["start_time"] = 1589504201,
							["delay"] = 0,
							["last_event"] = 1589504209,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 22,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 22,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 22,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504212,
							["buff_uptime"] = 11,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 22,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41896.749,
				["tempo_start"] = 1589504201,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 25,
				["playing_solo"] = true,
				["totals"] = {
					65.979457, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					56, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:56:52",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 10.5400000000009,
				["CombatEndedAt"] = 41907.289,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 56.007133,
						}, -- [1]
					},
				},
				["end_time"] = 41907.289,
				["combat_id"] = 22,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:56:41",
				["start_time"] = 41896.749,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [5]
			{
				{
					["combatId"] = 21,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004914,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 58,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 58.004914,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504180,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Kobold Vermin"] = 44,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 44,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 44,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 14,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 58.004914,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.49653311854455,
							["custom"] = 0,
							["last_event"] = 1589504178,
							["damage_taken"] = 10.004914,
							["start_time"] = 1589504170,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003067,
							["damage_from"] = {
								["Edarien"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Edarien"] = 13,
								["Thorddin"] = 10,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 23.003067,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 23.003067,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE7A4",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Edarien"] = 13,
											["Thorddin"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 15,
										["MISS"] = 2,
										["total"] = 23,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 12,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504201,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 167.003067,
							["start_time"] = 1589504170,
							["delay"] = 0,
							["last_event"] = 1589504196,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 21,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 21,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 21,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 10,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504180,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 21,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41865.574,
				["tempo_start"] = 1589504170,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 24,
				["playing_solo"] = true,
				["totals"] = {
					80.987129, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					58, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:56:20",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 10.1739999999991,
				["CombatEndedAt"] = 41875.748,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 58.004914,
						}, -- [1]
					},
				},
				["end_time"] = 41875.748,
				["combat_id"] = 21,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:56:10",
				["start_time"] = 41865.195,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
					["Kobold Worker"] = 2,
				},
			}, -- [6]
			{
				{
					["combatId"] = 20,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007648,
							["damage_from"] = {
								["Kobold Worker"] = true,
							},
							["targets"] = {
								["Kobold Worker"] = 78,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 78.007648,
							["friendlyfire"] = {
							},
							["end_time"] = 1589504167,
							["dps_started"] = false,
							["total"] = 78.007648,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 4,
										["n_max"] = 10,
										["targets"] = {
											["Kobold Worker"] = 56,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 19,
										["n_min"] = 9,
										["g_dmg"] = 37,
										["counter"] = 6,
										["total"] = 56,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Worker"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.06905244005365,
							["custom"] = 0,
							["last_event"] = 1589504166,
							["damage_taken"] = 17.007648,
							["start_time"] = 1589504151,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003237,
							["damage_from"] = {
								["Crossoff"] = true,
								["Edarien"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Crossoff"] = 17,
								["Edarien"] = 12,
								["Thorddin"] = 17,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 46.003237,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 46.003237,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-257-00003DE80C",
							["nome"] = "Kobold Worker",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Crossoff"] = 17,
											["Edarien"] = 12,
											["Thorddin"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 46,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 19,
										["MISS"] = 2,
										["total"] = 46,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 16,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504170,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 215.003237,
							["start_time"] = 1589504152,
							["delay"] = 0,
							["last_event"] = 1589504170,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 20,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 20,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 20,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504167,
							["buff_uptime"] = 16,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 16,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 20,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41847.377,
				["tempo_start"] = 1589504151,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 23,
				["playing_solo"] = true,
				["totals"] = {
					123.987268, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					78, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:56:07",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Worker",
				["TotalElapsedCombatTime"] = 14.9809999999998,
				["CombatEndedAt"] = 41862.358,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 78.007648,
						}, -- [1]
					},
				},
				["end_time"] = 41862.358,
				["combat_id"] = 20,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:55:52",
				["start_time"] = 41846.969,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Worker"] = 1,
				},
			}, -- [7]
			{
				{
					["combatId"] = 19,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.0061,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 43,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 43.0061,
							["friendlyfire"] = {
							},
							["end_time"] = 1589504149,
							["dps_started"] = false,
							["total"] = 43.0061,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Kobold Vermin"] = 31,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 31,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 31,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Kobold Vermin"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 6.16928704633683,
							["custom"] = 0,
							["last_event"] = 1589504148,
							["damage_taken"] = 6.0061,
							["start_time"] = 1589504141,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003291,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.003291,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.003291,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE7ED",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorddin"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504149,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 43.003291,
							["start_time"] = 1589504141,
							["delay"] = 0,
							["last_event"] = 1589504147,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 19,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 19,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 19,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 8,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504149,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 19,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41836.857,
				["tempo_start"] = 1589504141,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 22,
				["playing_solo"] = true,
				["totals"] = {
					49, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					43, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:55:49",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 7.65199999999459,
				["CombatEndedAt"] = 41844.509,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 43.0061,
						}, -- [1]
					},
				},
				["end_time"] = 41844.509,
				["combat_id"] = 19,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:55:41",
				["start_time"] = 41836.41,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [8]
			{
				{
					["combatId"] = 18,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001616,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 51,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 51.001616,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504134,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Kobold Vermin"] = 39,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 39,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 39,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Kobold Vermin"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 51.001616,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.83208873641907,
							["custom"] = 0,
							["last_event"] = 1589504132,
							["damage_taken"] = 5.001616,
							["start_time"] = 1589504124,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00503,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.00503,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.00503,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE7E3",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Thorddin"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504134,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 51.00503,
							["start_time"] = 1589504124,
							["delay"] = 0,
							["last_event"] = 1589504132,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 18,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 18,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 18,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504134,
							["buff_uptime"] = 10,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 18,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41819.454,
				["tempo_start"] = 1589504124,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 21,
				["playing_solo"] = true,
				["totals"] = {
					56, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					51, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:55:34",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 10.1430000000037,
				["CombatEndedAt"] = 41829.597,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 51.001616,
						}, -- [1]
					},
				},
				["end_time"] = 41829.597,
				["combat_id"] = 18,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:55:24",
				["start_time"] = 41819.24,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [9]
			{
				{
					["combatId"] = 17,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004978,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 45,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 45.004978,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504121,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Kobold Vermin"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 45.004978,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 6.5825622348988,
							["custom"] = 0,
							["last_event"] = 1589504120,
							["damage_taken"] = 4.004978,
							["start_time"] = 1589504113,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005404,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 4,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 4.005404,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 4.005404,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE7F8",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorddin"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 4,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504121,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 45.005404,
							["start_time"] = 1589504115,
							["delay"] = 0,
							["last_event"] = 1589504119,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 17,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 17,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 17,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504121,
							["buff_uptime"] = 8,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 17,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41808.96,
				["tempo_start"] = 1589504113,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 20,
				["playing_solo"] = true,
				["totals"] = {
					49, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					45, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:55:22",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 8.05900000000111,
				["CombatEndedAt"] = 41817.019,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 45.004978,
						}, -- [1]
					},
				},
				["end_time"] = 41817.019,
				["combat_id"] = 17,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:55:13",
				["start_time"] = 41808.547,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [10]
			{
				{
					["combatId"] = 16,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002632,
							["damage_from"] = {
								["Kobold Worker"] = true,
							},
							["targets"] = {
								["Kobold Worker"] = 72,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 72.002632,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504109,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Kobold Worker"] = 49,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 49,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 49,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Worker"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 72.002632,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.61117768079706,
							["custom"] = 0,
							["last_event"] = 1589504107,
							["damage_taken"] = 15.002632,
							["start_time"] = 1589504095,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007473,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 15,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.007473,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 15.007473,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-257-00003DE731",
							["nome"] = "Kobold Worker",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorddin"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 7,
										["DODGE"] = 1,
										["total"] = 15,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504109,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 72.007473,
							["start_time"] = 1589504095,
							["delay"] = 0,
							["last_event"] = 1589504107,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 16,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 16,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 16,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504109,
							["buff_uptime"] = 14,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 14,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 16,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41790.302,
				["tempo_start"] = 1589504095,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 19,
				["playing_solo"] = true,
				["totals"] = {
					87, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					72, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:55:09",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Worker",
				["TotalElapsedCombatTime"] = 13.8109999999942,
				["CombatEndedAt"] = 41804.113,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 72.002632,
						}, -- [1]
					},
				},
				["end_time"] = 41804.113,
				["combat_id"] = 16,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:54:55",
				["start_time"] = 41790.149,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Worker"] = 1,
				},
			}, -- [11]
			{
				{
					["combatId"] = 15,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003206,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 43,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 43.003206,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504091,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Kobold Vermin"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 43.003206,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 6.19642737751954,
							["custom"] = 0,
							["last_event"] = 1589504090,
							["damage_taken"] = 6.003206,
							["start_time"] = 1589504083,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.004715,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.004715,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.004715,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-0000BDE817",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorddin"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504091,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 43.004715,
							["start_time"] = 1589504083,
							["delay"] = 0,
							["last_event"] = 1589504089,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 15,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 15,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 15,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504091,
							["buff_uptime"] = 8,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 15,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41778.887,
				["tempo_start"] = 1589504083,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 18,
				["playing_solo"] = true,
				["totals"] = {
					49, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					43, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:54:51",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 7.7609999999986,
				["CombatEndedAt"] = 41786.648,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 43.003206,
						}, -- [1]
					},
				},
				["end_time"] = 41786.648,
				["combat_id"] = 15,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:54:43",
				["start_time"] = 41778.649,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 1,
				},
			}, -- [12]
			{
				{
					["combatId"] = 14,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004314,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 44,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 44.004314,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504076,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Kobold Vermin"] = 31,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 31,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 31,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 44.004314,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.37424450415116,
							["custom"] = 0,
							["last_event"] = 1589504075,
							["damage_taken"] = 4.004314,
							["start_time"] = 1589504068,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001256,
							["damage_from"] = {
								["Epzita"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Epzita"] = 9,
								["Thorddin"] = 4,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 13.001256,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 13.001256,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE82C",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Epzita"] = 9,
											["Thorddin"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 13,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504076,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 96.001256,
							["start_time"] = 1589504068,
							["delay"] = 0,
							["last_event"] = 1589504076,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 14,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 14,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 14,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504076,
							["buff_uptime"] = 8,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 14,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41763.982,
				["tempo_start"] = 1589504068,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 17,
				["playing_solo"] = true,
				["totals"] = {
					56.980263, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					44, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:54:37",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 8.0879999999961,
				["CombatEndedAt"] = 41772.07,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 44.004314,
						}, -- [1]
					},
				},
				["end_time"] = 41772.07,
				["combat_id"] = 14,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:54:29",
				["start_time"] = 41763.882,
				["TimeData"] = {
				},
				["frags"] = {
					["Young Wolf"] = 1,
					["Kobold Vermin"] = 1,
				},
			}, -- [13]
			{
				{
					["combatId"] = 13,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006164,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 55,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 55.006164,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504065,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 21,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Kobold Vermin"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 42,
										["c_max"] = 21,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 21,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Kobold Vermin"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 55.006164,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 7.05116831175538,
							["custom"] = 0,
							["last_event"] = 1589504064,
							["damage_taken"] = 8.006164,
							["start_time"] = 1589504057,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001926,
							["damage_from"] = {
								["Epzita"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Epzita"] = 6,
								["Thorddin"] = 8,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 14.001926,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 14.001926,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-6-00003DE7BE",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Epzita"] = 6,
											["Thorddin"] = 8,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504068,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 92.001926,
							["start_time"] = 1589504057,
							["delay"] = 0,
							["last_event"] = 1589504068,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 13,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 13,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 13,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504065,
							["buff_uptime"] = 8,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 13,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41752.663,
				["tempo_start"] = 1589504057,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 16,
				["playing_solo"] = true,
				["totals"] = {
					68.978664, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					55, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:54:25",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 7.67199999999866,
				["CombatEndedAt"] = 41760.335,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 55.006164,
						}, -- [1]
					},
				},
				["end_time"] = 41760.335,
				["combat_id"] = 13,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:54:17",
				["start_time"] = 41752.534,
				["TimeData"] = {
				},
				["frags"] = {
					["Kobold Vermin"] = 2,
					["Timber Wolf"] = 1,
				},
			}, -- [14]
			{
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006314,
							["damage_from"] = {
								["Young Wolf"] = true,
							},
							["targets"] = {
								["Young Wolf"] = 50,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 50.006314,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504042,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 21,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Young Wolf"] = 41,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 41,
										["c_max"] = 21,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 21,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Young Wolf"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 9,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 50.006314,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 6.89171912899553,
							["custom"] = 0,
							["last_event"] = 1589504040,
							["damage_taken"] = 5.006314,
							["start_time"] = 1589504034,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007257,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.007257,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.007257,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-299-00013DE812",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorddin"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504042,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 50.007257,
							["start_time"] = 1589504035,
							["delay"] = 0,
							["last_event"] = 1589504039,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 12,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 12,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 12,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504042,
							["buff_uptime"] = 8,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41730.38,
				["tempo_start"] = 1589504034,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 15,
				["playing_solo"] = true,
				["totals"] = {
					54.981241, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					50, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:54:02",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Young Wolf",
				["TotalElapsedCombatTime"] = 6.84900000000198,
				["CombatEndedAt"] = 41737.229,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 50.006314,
						}, -- [1]
					},
				},
				["end_time"] = 41737.229,
				["combat_id"] = 12,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:53:55",
				["start_time"] = 41729.973,
				["TimeData"] = {
				},
				["frags"] = {
					["Young Wolf"] = 1,
				},
			}, -- [15]
			{
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002864,
							["damage_from"] = {
								["Young Wolf"] = true,
							},
							["targets"] = {
								["Young Wolf"] = 50,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 50.002864,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504033,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 21,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Young Wolf"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 42,
										["c_max"] = 21,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 21,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Young Wolf"] = 8,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 8,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 8,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 50.002864,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.69898153635897,
							["custom"] = 0,
							["last_event"] = 1589504030,
							["damage_taken"] = 6.002864,
							["start_time"] = 1589504024,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006181,
							["damage_from"] = {
								["Epzita"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Epzita"] = 4,
								["Thorddin"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.006181,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 10.006181,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-299-0000BDE7EF",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Epzita"] = 4,
											["Thorddin"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 10,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504033,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 82.006181,
							["start_time"] = 1589504024,
							["delay"] = 0,
							["last_event"] = 1589504032,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 11,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 11,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 11,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 9,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504033,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41719.809,
				["tempo_start"] = 1589504024,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 14,
				["playing_solo"] = true,
				["totals"] = {
					59.991881, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					50, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:53:53",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Young Wolf",
				["TotalElapsedCombatTime"] = 8.91799999999785,
				["CombatEndedAt"] = 41728.727,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 50.002864,
						}, -- [1]
					},
				},
				["end_time"] = 41728.727,
				["combat_id"] = 11,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:53:44",
				["start_time"] = 41719.809,
				["TimeData"] = {
				},
				["frags"] = {
					["Young Wolf"] = 1,
				},
			}, -- [16]
			{
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004589,
							["damage_from"] = {
								["Young Wolf"] = true,
							},
							["targets"] = {
								["Young Wolf"] = 43,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 43.004589,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504020,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Young Wolf"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Young Wolf"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 43.004589,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 5.21773707838007,
							["custom"] = 0,
							["last_event"] = 1589504018,
							["damage_taken"] = 8.004589,
							["start_time"] = 1589504012,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008577,
							["damage_from"] = {
								["Epzita"] = true,
								["Askir"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Epzita"] = 2,
								["Askir"] = 1,
								["Thorddin"] = 8,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 11.008577,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 11.008577,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-299-0000BDE803",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Epzita"] = 2,
											["Askir"] = 1,
											["Thorddin"] = 8,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 11,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504024,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 76.008577,
							["start_time"] = 1589504012,
							["delay"] = 0,
							["last_event"] = 1589504024,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 10,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 10,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 10,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1589504020,
							["buff_uptime"] = 8,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41707.307,
				["tempo_start"] = 1589504012,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 13,
				["playing_solo"] = true,
				["totals"] = {
					53.979656, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					43, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:53:40",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Young Wolf",
				["TotalElapsedCombatTime"] = 8.08899999999994,
				["CombatEndedAt"] = 41715.396,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 43.004589,
						}, -- [1]
					},
				},
				["end_time"] = 41715.396,
				["combat_id"] = 10,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:53:32",
				["start_time"] = 41707.154,
				["TimeData"] = {
				},
				["frags"] = {
					["Young Wolf"] = 2,
					["Kobold Worker"] = 1,
				},
			}, -- [17]
			{
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001474,
							["damage_from"] = {
								["Young Wolf"] = true,
							},
							["targets"] = {
								["Young Wolf"] = 64,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 64.001474,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1589504008,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorddin",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 20,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Young Wolf"] = 51,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 31,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 51,
										["c_max"] = 20,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 20,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Young Wolf"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 64.001474,
							["serial"] = "Player-4408-01FD226F",
							["last_dps"] = 6.65988283038461,
							["custom"] = 0,
							["last_event"] = 1589504007,
							["damage_taken"] = 5.001474,
							["start_time"] = 1589503998,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005104,
							["damage_from"] = {
								["Askir"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Askir"] = 4,
								["Thorddin"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.005104,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 9.005104,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-12-299-00013DE7EF",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Askir"] = 4,
											["Thorddin"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 9,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1589504012,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 111.005104,
							["start_time"] = 1589503999,
							["delay"] = 0,
							["last_event"] = 1589504011,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 9,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 9,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 9,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 10,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-01FD226F",
							["last_event"] = 1589504008,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 41693.886,
				["tempo_start"] = 1589503998,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 12,
				["playing_solo"] = true,
				["totals"] = {
					72.987365, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					64, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "20:53:28",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Young Wolf",
				["TotalElapsedCombatTime"] = 9.36000000000058,
				["CombatEndedAt"] = 41703.246,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorddin"] = 64.001474,
						}, -- [1]
					},
				},
				["end_time"] = 41703.246,
				["combat_id"] = 9,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "20:53:18",
				["start_time"] = 41693.636,
				["TimeData"] = {
				},
				["frags"] = {
					["Young Wolf"] = 2,
				},
			}, -- [18]
		},
	},
	["last_version"] = "v1.13.3.201",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1590799650,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorddin-Faerlina",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 0.2,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_STREAM_OVERLAY"] = {
			["font_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["is_first_run"] = false,
			["arrow_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				0.5, -- [4]
			},
			["main_frame_size"] = {
				300, -- [1]
				500.000030517578, -- [2]
			},
			["minimap"] = {
				["minimapPos"] = 160,
				["radius"] = 160,
				["hide"] = false,
			},
			["arrow_anchor_x"] = 0,
			["row_texture"] = "Details Serenity",
			["scale"] = 1,
			["point"] = "CENTER",
			["y"] = -1.525878906250e-05,
			["enabled"] = false,
			["arrow_size"] = 10,
			["author"] = "Details! Team",
			["row_spacement"] = 21,
			["main_frame_color"] = {
				0, -- [1]
				0, -- [2]
				0, -- [3]
				0.2, -- [4]
			},
			["main_frame_strata"] = "LOW",
			["arrow_texture"] = "Interface\\CHATFRAME\\ChatFrameExpandArrow",
			["row_height"] = 20,
			["per_second"] = {
				["enabled"] = false,
				["point"] = "CENTER",
				["scale"] = 1,
				["font_shadow"] = true,
				["y"] = -1.525878906250e-05,
				["x"] = -3.05175781250e-05,
				["update_speed"] = 0.05,
				["size"] = 32,
				["attribute_type"] = 1,
			},
			["x"] = 3.05175781250e-05,
			["font_face"] = "Friz Quadrata TT",
			["row_color"] = {
				0.1, -- [1]
				0.1, -- [2]
				0.1, -- [3]
				0.4, -- [4]
			},
			["arrow_anchor_y"] = 0,
			["use_spark"] = true,
			["main_frame_locked"] = false,
			["font_size"] = 10,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "14",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 142,
	["combat_id"] = 26,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 29,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.136043,
					["damage_from"] = {
						["Mangy Wolf"] = true,
						["Young Wolf"] = true,
						["Defias Cutpurse"] = true,
						["Kobold Worker"] = true,
						["Kobold Vermin"] = true,
						["Timber Wolf"] = true,
					},
					["targets"] = {
						["Mangy Wolf"] = 102,
						["Young Wolf"] = 550,
						["Defias Cutpurse"] = 126,
						["Kobold Worker"] = 150,
						["Kobold Vermin"] = 489,
						["Timber Wolf"] = 115,
					},
					["pets"] = {
					},
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 1532.136043,
					["classe"] = "PALADIN",
					["dps_started"] = false,
					["total"] = 1532.136043,
					["last_event"] = 0,
					["end_time"] = 1589503873,
					["nome"] = "Thorddin",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 10,
								["b_amt"] = 0,
								["c_dmg"] = 198,
								["g_amt"] = 15,
								["n_max"] = 12,
								["targets"] = {
									["Mangy Wolf"] = 76,
									["Young Wolf"] = 438,
									["Defias Cutpurse"] = 96,
									["Kobold Worker"] = 105,
									["Kobold Vermin"] = 358,
									["Timber Wolf"] = 90,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 836,
								["n_min"] = 0,
								["g_dmg"] = 129,
								["counter"] = 111,
								["total"] = 1163,
								["c_max"] = 21,
								["MISS"] = 1,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 84,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							["Seal of Righteousness"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 6,
								["targets"] = {
									["Mangy Wolf"] = 26,
									["Young Wolf"] = 112,
									["Defias Cutpurse"] = 30,
									["Kobold Worker"] = 45,
									["Kobold Vermin"] = 131,
									["Timber Wolf"] = 25,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 369,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 85,
								["total"] = 369,
								["c_max"] = 0,
								["id"] = "Seal of Righteousness",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 85,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["on_hold"] = false,
					["serial"] = "Player-4408-01FD226F",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1589503551,
					["delay"] = 0,
					["damage_taken"] = 359.136043,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.078453,
					["damage_from"] = {
						["Edarien"] = true,
						["Thorddin"] = true,
						["Epzita"] = true,
						["Zunny"] = true,
						["Askir"] = true,
						["Polizzly"] = true,
					},
					["targets"] = {
						["Edarien"] = 9,
						["Thorddin"] = 86,
						["Epzita"] = 4,
						["Askir"] = 8,
						["Polizzly"] = 1,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 108.078453,
					["fight_component"] = true,
					["dps_started"] = false,
					["total"] = 108.078453,
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["nome"] = "Young Wolf",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2,
								["targets"] = {
									["Edarien"] = 9,
									["Thorddin"] = 86,
									["Epzita"] = 4,
									["Askir"] = 8,
									["Polizzly"] = 1,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 108,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 82,
								["total"] = 108,
								["c_max"] = 0,
								["DODGE"] = 1,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["MISS"] = 4,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 77,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["friendlyfire"] = {
					},
					["end_time"] = 1589503873,
					["last_event"] = 0,
					["serial"] = "Creature-0-4411-0-12-299-00013DE75F",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1589503731,
					["delay"] = 0,
					["damage_taken"] = 779.078453,
				}, -- [2]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.033088,
					["damage_from"] = {
						["Polizzly"] = true,
						["Thorddin"] = true,
						["Edarien"] = true,
						["Zunny"] = true,
					},
					["targets"] = {
						["Polizzly"] = 11,
						["Thorddin"] = 16,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 27.033088,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1589503873,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Timber Wolf",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Polizzly"] = 11,
									["Thorddin"] = 16,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 27,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 27,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["DODGE"] = 1,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 12,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["total"] = 27.033088,
					["serial"] = "Creature-0-4411-0-12-69-00003DE737",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 309.033088,
					["start_time"] = 1589503854,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [3]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.053127,
					["damage_from"] = {
						["Epzita"] = true,
						["Harrybawlla"] = true,
						["Edarien"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Epzita"] = 13,
						["Edarien"] = 4,
						["Thorddin"] = 60,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 77.053127,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1589503934,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Kobold Vermin",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Epzita"] = 13,
									["Edarien"] = 4,
									["Thorddin"] = 60,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 77,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 53,
								["total"] = 77,
								["c_max"] = 0,
								["DODGE"] = 4,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["MISS"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 47,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["total"] = 77.053127,
					["serial"] = "Creature-0-4411-0-12-6-00003DE785",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 636.053127,
					["start_time"] = 1589503848,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [4]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.043369,
					["fight_component"] = true,
					["damage_from"] = {
						["Edarien"] = true,
						["Thorddin"] = true,
						["Harrybawlla"] = true,
						["Crossoff"] = true,
						["Rinkzz"] = true,
					},
					["targets"] = {
						["Edarien"] = 6,
						["Crossoff"] = 18,
						["Rinkzz"] = 3,
						["Thorddin"] = 32,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 59.043369,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1589503989,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Kobold Worker",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 4,
								["targets"] = {
									["Edarien"] = 6,
									["Crossoff"] = 18,
									["Rinkzz"] = 3,
									["Thorddin"] = 32,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 59,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 25,
								["total"] = 59,
								["c_max"] = 0,
								["MISS"] = 3,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["DODGE"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 20,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["total"] = 59.043369,
					["serial"] = "Creature-0-4411-0-12-257-00003DE7A4",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 443.043369,
					["start_time"] = 1589503948,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [5]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.009984,
					["on_hold"] = false,
					["damage_from"] = {
						["Thorddin"] = true,
					},
					["targets"] = {
						["Thorddin"] = 72,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 72.009984,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1589504441,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Mangy Wolf",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5,
								["targets"] = {
									["Thorddin"] = 72,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 72,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 17,
								["total"] = 72,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 17,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["monster"] = true,
					["total"] = 72.009984,
					["serial"] = "Creature-0-4411-0-12-525-00003DDE5F",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 102.009984,
					["start_time"] = 1589504410,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [6]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.008255,
					["on_hold"] = false,
					["damage_from"] = {
						["Thorddin"] = true,
						["Dethrak"] = true,
					},
					["targets"] = {
						["Thorddin"] = 93,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 93.008255,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1589504491,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Defias Cutpurse",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 8,
								["targets"] = {
									["Thorddin"] = 93,
									["Dethrak"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 93,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 19,
								["a_amt"] = 0,
								["total"] = 93,
								["c_max"] = 0,
								["DODGE"] = 3,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["MISS"] = 1,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 14,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["monster"] = true,
					["total"] = 93.008255,
					["serial"] = "Creature-0-4411-0-12-94-00003DDEAE",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 245.008255,
					["start_time"] = 1589504450,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [7]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorb"] = 0.017575,
					["last_hps"] = 0,
					["targets_overheal"] = {
					},
					["targets"] = {
						["Thorddin"] = 129,
					},
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							["Holy Light"] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 45,
								["targets"] = {
									["Thorddin"] = 129,
								},
								["n_min"] = 0,
								["counter"] = 3,
								["overheal"] = 0,
								["total"] = 129,
								["c_max"] = 0,
								["id"] = "Holy Light",
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 129,
								["n_amt"] = 3,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["pets"] = {
					},
					["totalover_without_pet"] = 0.017575,
					["healing_from"] = {
						["Thorddin"] = true,
					},
					["classe"] = "PALADIN",
					["totalover"] = 0.017575,
					["total_without_pet"] = 129.017575,
					["healing_taken"] = 129.017575,
					["iniciar_hps"] = false,
					["fight_component"] = true,
					["total"] = 129.017575,
					["heal_enemy_amt"] = 0,
					["end_time"] = 1589504441,
					["nome"] = "Thorddin",
					["targets_absorbs"] = {
					},
					["grupo"] = true,
					["start_time"] = 1589504436,
					["heal_enemy"] = {
					},
					["serial"] = "Player-4408-01FD226F",
					["custom"] = 0,
					["tipo"] = 2,
					["on_hold"] = false,
					["totaldenied"] = 0.017575,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["fight_component"] = true,
					["nome"] = "Thorddin",
					["grupo"] = true,
					["buff_uptime_targets"] = {
					},
					["pets"] = {
					},
					["spell_cast"] = {
						["Seal of Righteousness"] = 11,
						["Holy Light"] = 3,
					},
					["buff_uptime"] = 368,
					["classe"] = "PALADIN",
					["last_event"] = 0,
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							["Seal of Righteousness"] = {
								["refreshamt"] = 5,
								["activedamt"] = 30,
								["appliedamt"] = 30,
								["id"] = "Seal of Righteousness",
								["uptime"] = 283,
								["targets"] = {
								},
								["counter"] = 0,
							},
							["Devotion Aura"] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = "Devotion Aura",
								["uptime"] = 85,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["serial"] = "Player-4408-01FD226F",
					["tipo"] = 4,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1589503863,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			2087.296898, -- [1]
			129.013824, -- [2]
			{
				9.00577, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "20:51:03",
		["end_time"] = 42186.435,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 38.015999999996,
				["type"] = 0,
				["name"] = "Defias Cutpurse",
				["clock"] = "21:00:53",
			}, -- [1]
			{
				["elapsed"] = 46.5570000000007,
				["type"] = 0,
				["name"] = "Mangy Wolf",
				["clock"] = "20:59:54",
			}, -- [2]
			{
				["elapsed"] = 10.1080000000002,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:57:05",
			}, -- [3]
			{
				["elapsed"] = 8.14800000000105,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:56:55",
			}, -- [4]
			{
				["elapsed"] = 10.5400000000009,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:56:41",
			}, -- [5]
			{
				["elapsed"] = 10.5529999999999,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:56:10",
			}, -- [6]
			{
				["elapsed"] = 15.3890000000029,
				["type"] = 0,
				["name"] = "Kobold Worker",
				["clock"] = "20:55:52",
			}, -- [7]
			{
				["elapsed"] = 8.0989999999947,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:55:41",
			}, -- [8]
			{
				["elapsed"] = 10.3570000000036,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:55:24",
			}, -- [9]
			{
				["elapsed"] = 8.47200000000157,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:55:13",
			}, -- [10]
			{
				["elapsed"] = 13.9639999999999,
				["type"] = 0,
				["name"] = "Kobold Worker",
				["clock"] = "20:54:55",
			}, -- [11]
			{
				["elapsed"] = 7.99900000000343,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:54:43",
			}, -- [12]
			{
				["elapsed"] = 8.18800000000192,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:54:29",
			}, -- [13]
			{
				["elapsed"] = 7.80099999999948,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "20:54:17",
			}, -- [14]
			{
				["elapsed"] = 7.25600000000122,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:53:55",
			}, -- [15]
			{
				["elapsed"] = 8.91799999999785,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:53:44",
			}, -- [16]
			{
				["elapsed"] = 8.24199999999837,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:53:32",
			}, -- [17]
			{
				["elapsed"] = 9.61000000000058,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:53:18",
			}, -- [18]
			{
				["elapsed"] = 6.61000000000058,
				["type"] = 0,
				["name"] = "Timber Wolf",
				["clock"] = "20:53:03",
			}, -- [19]
			{
				["elapsed"] = 10.760000000002,
				["type"] = 0,
				["name"] = "Timber Wolf",
				["clock"] = "20:52:49",
			}, -- [20]
			{
				["elapsed"] = 10.4049999999988,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:52:37",
			}, -- [21]
			{
				["elapsed"] = 10.5880000000034,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:52:23",
			}, -- [22]
			{
				["elapsed"] = 9.54099999999744,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:52:04",
			}, -- [23]
			{
				["elapsed"] = 9.72800000000279,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:51:34",
			}, -- [24]
			{
				["elapsed"] = 10.2379999999976,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:51:16",
			}, -- [25]
			{
				["elapsed"] = 10.2900000000009,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "20:51:03",
			}, -- [26]
		},
		["totals_grupo"] = {
			1532.13449, -- [1]
			129.013824, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "21:01:31",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 41870.058,
		["TimeData"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 62.8148498535156,
					["x"] = -864.259170532227,
					["w"] = 310.000213623047,
					["h"] = 158.000030517578,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
	},
}
