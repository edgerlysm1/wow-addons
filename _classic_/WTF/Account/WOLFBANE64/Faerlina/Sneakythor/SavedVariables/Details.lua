
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
		},
	},
	["last_version"] = "v1.13.3.201",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1590709207,
		["last_version"] = 11,
		["Sneakythor"] = {
			"Thor", -- [1]
			"Interface\\EncounterJournal\\UI-EJ-BOSS-Lethtendris", -- [2]
			{
				0, -- [1]
				1, -- [2]
				0, -- [3]
				1, -- [4]
			}, -- [3]
			"Interface\\PetBattles\\Weather-Moonlight", -- [4]
			{
				0.02765625, -- [1]
				0.94359375, -- [2]
				1, -- [3]
				0, -- [4]
			}, -- [5]
			{
				1, -- [1]
				1, -- [2]
				1, -- [3]
			}, -- [6]
			4, -- [7]
		},
	},
	["last_instance_id"] = 230,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 1572493188,
	["active_profile"] = "Sneakythor-Faerlina",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 0.2,
			["animate"] = false,
			["showamount"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_STREAM_OVERLAY"] = {
			["font_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["is_first_run"] = false,
			["arrow_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				0.5, -- [4]
			},
			["main_frame_size"] = {
				300, -- [1]
				500.000030517578, -- [2]
			},
			["minimap"] = {
				["minimapPos"] = 160,
				["radius"] = 160,
				["hide"] = false,
			},
			["arrow_anchor_x"] = 0,
			["row_texture"] = "Details Serenity",
			["font_size"] = 10,
			["main_frame_locked"] = false,
			["use_spark"] = true,
			["enabled"] = false,
			["arrow_size"] = 10,
			["arrow_anchor_y"] = 0,
			["row_spacement"] = 21,
			["main_frame_color"] = {
				0, -- [1]
				0, -- [2]
				0, -- [3]
				0.2, -- [4]
			},
			["author"] = "Details! Team",
			["arrow_texture"] = "Interface\\CHATFRAME\\ChatFrameExpandArrow",
			["row_color"] = {
				0.1, -- [1]
				0.1, -- [2]
				0.1, -- [3]
				0.4, -- [4]
			},
			["y"] = -1.525878906250e-05,
			["x"] = 3.05175781250e-05,
			["font_face"] = "Friz Quadrata TT",
			["per_second"] = {
				["enabled"] = false,
				["point"] = "CENTER",
				["scale"] = 1,
				["font_shadow"] = true,
				["y"] = -1.525878906250e-05,
				["x"] = -3.05175781250e-05,
				["attribute_type"] = 1,
				["update_speed"] = 0.05,
				["size"] = 32,
			},
			["point"] = "CENTER",
			["main_frame_strata"] = "LOW",
			["row_height"] = 20,
			["scale"] = 1,
		},
	},
	["cached_talents"] = {
		["Player-4408-011753D3"] = {
			{
				132292, -- [1]
				0, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
				259, -- [6]
				3, -- [7]
			}, -- [1]
			{
				132151, -- [1]
				0, -- [2]
				1, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				2, -- [7]
			}, -- [2]
			{
				132277, -- [1]
				5, -- [2]
				1, -- [3]
				3, -- [4]
				1, -- [5]
				259, -- [6]
				5, -- [7]
			}, -- [3]
			{
				132122, -- [1]
				3, -- [2]
				2, -- [3]
				1, -- [4]
				1, -- [5]
				259, -- [6]
				3, -- [7]
			}, -- [4]
			{
				136147, -- [1]
				0, -- [2]
				2, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				2, -- [7]
			}, -- [5]
			{
				132306, -- [1]
				2, -- [2]
				2, -- [3]
				4, -- [4]
				1, -- [5]
				259, -- [6]
				3, -- [7]
			}, -- [6]
			{
				132340, -- [1]
				1, -- [2]
				3, -- [3]
				1, -- [4]
				1, -- [5]
				259, -- [6]
				1, -- [7]
			}, -- [7]
			{
				132354, -- [1]
				0, -- [2]
				3, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				2, -- [7]
			}, -- [8]
			{
				132109, -- [1]
				5, -- [2]
				3, -- [3]
				3, -- [4]
				1, -- [5]
				259, -- [6]
				5, -- [7]
			}, -- [9]
			{
				132293, -- [1]
				0, -- [2]
				4, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				5, -- [7]
			}, -- [10]
			{
				132273, -- [1]
				0, -- [2]
				4, -- [3]
				3, -- [4]
				1, -- [5]
				259, -- [6]
				5, -- [7]
			}, -- [11]
			{
				135988, -- [1]
				0, -- [2]
				5, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				1, -- [7]
			}, -- [12]
			{
				132298, -- [1]
				0, -- [2]
				5, -- [3]
				3, -- [4]
				1, -- [5]
				259, -- [6]
				3, -- [7]
			}, -- [13]
			{
				136130, -- [1]
				0, -- [2]
				6, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				5, -- [7]
			}, -- [14]
			{
				136023, -- [1]
				0, -- [2]
				7, -- [3]
				2, -- [4]
				1, -- [5]
				259, -- [6]
				1, -- [7]
			}, -- [15]
			{
				132155, -- [1]
				3, -- [2]
				1, -- [3]
				1, -- [4]
				2, -- [5]
				260, -- [6]
				3, -- [7]
			}, -- [16]
			{
				136189, -- [1]
				2, -- [2]
				1, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				2, -- [7]
			}, -- [17]
			{
				136047, -- [1]
				0, -- [2]
				1, -- [3]
				3, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [18]
			{
				132090, -- [1]
				0, -- [2]
				2, -- [3]
				1, -- [4]
				2, -- [5]
				260, -- [6]
				3, -- [7]
			}, -- [19]
			{
				132269, -- [1]
				1, -- [2]
				2, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [20]
			{
				132222, -- [1]
				5, -- [2]
				2, -- [3]
				3, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [21]
			{
				136205, -- [1]
				2, -- [2]
				3, -- [3]
				1, -- [4]
				2, -- [5]
				260, -- [6]
				2, -- [7]
			}, -- [22]
			{
				132336, -- [1]
				0, -- [2]
				3, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				1, -- [7]
			}, -- [23]
			{
				132307, -- [1]
				2, -- [2]
				3, -- [3]
				4, -- [4]
				2, -- [5]
				260, -- [6]
				2, -- [7]
			}, -- [24]
			{
				132219, -- [1]
				0, -- [2]
				4, -- [3]
				1, -- [4]
				2, -- [5]
				260, -- [6]
				2, -- [7]
			}, -- [25]
			{
				135641, -- [1]
				0, -- [2]
				4, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [26]
			{
				132147, -- [1]
				5, -- [2]
				4, -- [3]
				3, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [27]
			{
				133476, -- [1]
				0, -- [2]
				5, -- [3]
				1, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [28]
			{
				132350, -- [1]
				1, -- [2]
				5, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				1, -- [7]
			}, -- [29]
			{
				135328, -- [1]
				5, -- [2]
				5, -- [3]
				3, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [30]
			{
				132938, -- [1]
				0, -- [2]
				5, -- [3]
				4, -- [4]
				2, -- [5]
				260, -- [6]
				5, -- [7]
			}, -- [31]
			{
				135882, -- [1]
				2, -- [2]
				6, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				2, -- [7]
			}, -- [32]
			{
				132275, -- [1]
				1, -- [2]
				6, -- [3]
				3, -- [4]
				2, -- [5]
				260, -- [6]
				3, -- [7]
			}, -- [33]
			{
				136206, -- [1]
				0, -- [2]
				7, -- [3]
				2, -- [4]
				2, -- [5]
				260, -- [6]
				1, -- [7]
			}, -- [34]
			{
				136129, -- [1]
				0, -- [2]
				1, -- [3]
				2, -- [4]
				3, -- [5]
				261, -- [6]
				5, -- [7]
			}, -- [35]
			{
				132366, -- [1]
				0, -- [2]
				1, -- [3]
				3, -- [4]
				3, -- [5]
				261, -- [6]
				5, -- [7]
			}, -- [36]
			{
				132294, -- [1]
				0, -- [2]
				2, -- [3]
				1, -- [4]
				3, -- [5]
				261, -- [6]
				2, -- [7]
			}, -- [37]
			{
				135994, -- [1]
				0, -- [2]
				2, -- [3]
				2, -- [4]
				3, -- [5]
				261, -- [6]
				2, -- [7]
			}, -- [38]
			{
				132320, -- [1]
				0, -- [2]
				2, -- [3]
				3, -- [4]
				3, -- [5]
				261, -- [6]
				5, -- [7]
			}, -- [39]
			{
				136159, -- [1]
				0, -- [2]
				3, -- [3]
				1, -- [4]
				3, -- [5]
				261, -- [6]
				3, -- [7]
			}, -- [40]
			{
				136136, -- [1]
				0, -- [2]
				3, -- [3]
				2, -- [4]
				3, -- [5]
				261, -- [6]
				1, -- [7]
			}, -- [41]
			{
				132282, -- [1]
				0, -- [2]
				3, -- [3]
				3, -- [4]
				3, -- [5]
				261, -- [6]
				3, -- [7]
			}, -- [42]
			{
				136056, -- [1]
				0, -- [2]
				4, -- [3]
				1, -- [4]
				3, -- [5]
				261, -- [6]
				3, -- [7]
			}, -- [43]
			{
				132310, -- [1]
				0, -- [2]
				4, -- [3]
				2, -- [4]
				3, -- [5]
				261, -- [6]
				3, -- [7]
			}, -- [44]
			{
				135315, -- [1]
				0, -- [2]
				4, -- [3]
				3, -- [4]
				3, -- [5]
				261, -- [6]
				3, -- [7]
			}, -- [45]
			{
				132089, -- [1]
				0, -- [2]
				5, -- [3]
				1, -- [4]
				3, -- [5]
				261, -- [6]
				2, -- [7]
			}, -- [46]
			{
				136121, -- [1]
				0, -- [2]
				5, -- [3]
				2, -- [4]
				3, -- [5]
				261, -- [6]
				1, -- [7]
			}, -- [47]
			{
				136220, -- [1]
				0, -- [2]
				5, -- [3]
				3, -- [4]
				3, -- [5]
				261, -- [6]
				2, -- [7]
			}, -- [48]
			{
				136168, -- [1]
				0, -- [2]
				5, -- [3]
				4, -- [4]
				3, -- [5]
				261, -- [6]
				1, -- [7]
			}, -- [49]
			{
				135540, -- [1]
				0, -- [2]
				6, -- [3]
				3, -- [4]
				3, -- [5]
				261, -- [6]
				5, -- [7]
			}, -- [50]
			{
				136183, -- [1]
				0, -- [2]
				7, -- [3]
				2, -- [4]
				3, -- [5]
				261, -- [6]
				1, -- [7]
			}, -- [51]
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "13",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 142,
	["combat_id"] = 7461,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 7679,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 5546.209,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 7675,
		["totals"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = 0,
		["end_time"] = 5546.209,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["overall_refreshed"] = true,
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = 0,
		["cleu_events"] = {
			["n"] = 1,
		},
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 5546.209,
		["TimeData"] = {
		},
		["totals_grupo"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["last_raid_plugin"] = "DETAILS_PLUGIN_TINY_THREAT",
			["is_open"] = true,
			["isLocked"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[2] = 2,
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 110.814544677734,
					["x"] = -755.616638183594,
					["w"] = 162.246948242188,
					["h"] = 110.197624206543,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["last_raid_plugin"] = "DETAILS_PLUGIN_TINY_THREAT",
			["is_open"] = true,
			["isLocked"] = false,
			["sub_atributo_last"] = {
				3, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[4] = 1,
			},
			["mode"] = 4,
			["attribute"] = 2,
			["pos"] = {
				["normal"] = {
					["y"] = -6.93862915039063,
					["x"] = -755.616638183594,
					["w"] = 162.246948242188,
					["h"] = 85.3086776733399,
				},
				["solo"] = {
					["y"] = 1.99993896484375,
					["x"] = 1.00006103515625,
					["w"] = 300,
					["h"] = 300,
				},
			},
		}, -- [2]
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["is_open"] = false,
			["isLocked"] = false,
			["snap"] = {
			},
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["mode"] = 2,
			["attribute"] = 2,
			["pos"] = {
				["normal"] = {
					["y"] = 13.2345581054688,
					["x"] = -252.913879394531,
					["w"] = 236.123474121094,
					["h"] = 144.172882080078,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [3]
	},
	["character_data"] = {
		["logons"] = 169,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-4408-011753D3"] = 260,
	},
}
