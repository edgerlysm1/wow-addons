
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 34,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005947,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 82,
							},
							["colocacao"] = 1,
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 82.005947,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 82.005947,
							["classe"] = "WARRIOR",
							["serial"] = "Player-4408-0145B28C",
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[0] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Small Crag Boar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 0,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 37,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 37,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 37,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["end_time"] = 1569797263,
							["last_dps"] = 5.53159844856634,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 19.005947,
							["start_time"] = 1569797247,
							["delay"] = 0,
							["last_event"] = 1569797261,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003637,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 19,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 19.003637,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 19.003637,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000912EA8",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 19,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 19,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 19,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 8,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797263,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 82.003637,
							["start_time"] = 1569797247,
							["delay"] = 0,
							["last_event"] = 1569797261,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 34,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 34,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 34,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 3,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 34,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31704.305,
				["tempo_start"] = 1569797247,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 40,
				["playing_solo"] = true,
				["totals"] = {
					101, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					82, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:47:43",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 16.1630000000005,
				["CombatEndedAt"] = 31720.468,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 82.005947,
						}, -- [1]
					},
				},
				["end_time"] = 31720.468,
				["combat_id"] = 34,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:47:27",
				["start_time"] = 31704.195,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [1]
			{
				{
					["combatId"] = 33,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003162,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 17,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 17.003162,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 17.003162,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000112E4A",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797239,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 71.003162,
							["start_time"] = 1569797226,
							["delay"] = 0,
							["last_event"] = 1569797237,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001672,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 71,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 71.001672,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797239,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 26,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 26,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 26,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 71.001672,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 6.01199593564894,
							["custom"] = 0,
							["last_event"] = 1569797238,
							["damage_taken"] = 17.001672,
							["start_time"] = 1569797229,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 33,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 33,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 33,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 33,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1569797226,
				["enemy"] = "Small Crag Boar",
				["combat_counter"] = 39,
				["playing_solo"] = true,
				["totals"] = {
					88, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 31696.623,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "18:47:07",
				["end_time"] = 31696.623,
				["totals_grupo"] = {
					71, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 33,
				["TotalElapsedCombatTime"] = 31696.623,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 71.001672,
						}, -- [1]
					},
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
				["data_fim"] = "18:47:19",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 31684.01,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [2]
			{
				{
					["combatId"] = 32,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005056,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 17,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 17.005056,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 17.005056,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000112EA8",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797223,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 71.005056,
							["start_time"] = 1569797210,
							["delay"] = 0,
							["last_event"] = 1569797220,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003377,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 71,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 71.003377,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797223,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Small Crag Boar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 26,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 26,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 26,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 71.003377,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 5.97420084139752,
							["custom"] = 0,
							["last_event"] = 1569797226,
							["damage_taken"] = 17.003377,
							["start_time"] = 1569797213,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 32,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 32,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 32,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 32,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31684.01,
				["tempo_start"] = 1569797210,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 38,
				["playing_solo"] = true,
				["totals"] = {
					88, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					71, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:47:03",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 31680.448,
				["CombatEndedAt"] = 31680.448,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 71.003377,
						}, -- [1]
					},
				},
				["end_time"] = 31680.448,
				["combat_id"] = 32,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:46:51",
				["start_time"] = 31667.936,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [3]
			{
				{
					["combatId"] = 31,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00824,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 81,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 81.00824,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797207,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Small Crag Boar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["a_amt"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Small Crag Boar"] = 35,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 35,
										["n_min"] = 11,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 35,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 46,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 46,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 46,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 81.00824,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 6.93385603012871,
							["custom"] = 0,
							["last_event"] = 1569797210,
							["damage_taken"] = 15.00824,
							["start_time"] = 1569797194,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00412,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 15,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.00412,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 15.00412,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000112E7E",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797207,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 81.00412,
							["start_time"] = 1569797195,
							["delay"] = 0,
							["last_event"] = 1569797205,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 31,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 31,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 31,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 31,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31667.936,
				["tempo_start"] = 1569797194,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 37,
				["playing_solo"] = true,
				["totals"] = {
					96, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					81, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:46:47",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 12.5430000000015,
				["CombatEndedAt"] = 31664.685,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 81.00824,
						}, -- [1]
					},
				},
				["end_time"] = 31664.685,
				["combat_id"] = 31,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:46:34",
				["start_time"] = 31651.732,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [4]
			{
				{
					["combatId"] = 30,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004001,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 82,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 82.004001,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797182,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 37,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 25,
										["n_min"] = 12,
										["g_dmg"] = 12,
										["counter"] = 3,
										["total"] = 37,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 82.004001,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 6.9512588793769,
							["custom"] = 0,
							["last_event"] = 1569797181,
							["damage_taken"] = 13.004001,
							["start_time"] = 1569797169,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001371,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 13,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 13.001371,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 13.001371,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000113268",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorend"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797182,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 82.001371,
							["start_time"] = 1569797169,
							["delay"] = 0,
							["last_event"] = 1569797179,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 30,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 30,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 30,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 30,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31627.036,
				["tempo_start"] = 1569797169,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 36,
				["playing_solo"] = true,
				["totals"] = {
					95, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					82, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:46:22",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 12.5430000000015,
				["CombatEndedAt"] = 31639.579,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 82.004001,
						}, -- [1]
					},
				},
				["end_time"] = 31639.579,
				["combat_id"] = 30,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:46:09",
				["start_time"] = 31626.626,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [5]
			{
				{
					["combatId"] = 29,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004728,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 78,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 78.004728,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797161,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 36,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 25,
										["n_min"] = 12,
										["g_dmg"] = 11,
										["counter"] = 3,
										["total"] = 36,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Small Crag Boar"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 42,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 42,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 20,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 78.004728,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 6.69223816060453,
							["custom"] = 0,
							["last_event"] = 1569797160,
							["damage_taken"] = 14.004728,
							["start_time"] = 1569797148,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001861,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 14,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 14.001861,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 14.001861,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-000011322F",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorend"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 14,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797161,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 78.001861,
							["start_time"] = 1569797150,
							["delay"] = 0,
							["last_event"] = 1569797158,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 29,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 29,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 29,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 29,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31606.01,
				["tempo_start"] = 1569797148,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 35,
				["playing_solo"] = true,
				["totals"] = {
					92, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					78, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:46:01",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 12.5499999999993,
				["CombatEndedAt"] = 31618.56,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 78.004728,
						}, -- [1]
					},
				},
				["end_time"] = 31618.56,
				["combat_id"] = 29,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:45:48",
				["start_time"] = 31605.76,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [6]
			{
				{
					["combatId"] = 28,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004219,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 71,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 71.004219,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797144,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 26,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 26,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 26,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 71.004219,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 8.04671566183089,
							["custom"] = 0,
							["last_event"] = 1569797143,
							["damage_taken"] = 10.004219,
							["start_time"] = 1569797134,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.004436,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 10,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.004436,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 10.004436,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000113215",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorend"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797144,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 71.004436,
							["start_time"] = 1569797137,
							["delay"] = 0,
							["last_event"] = 1569797143,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 28,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 28,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 28,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 28,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31592.266,
				["tempo_start"] = 1569797134,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 34,
				["playing_solo"] = true,
				["totals"] = {
					81, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					71, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:45:44",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 9.29500000000189,
				["CombatEndedAt"] = 31601.561,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 71.004219,
						}, -- [1]
					},
				},
				["end_time"] = 31601.561,
				["combat_id"] = 28,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:45:35",
				["start_time"] = 31591.996,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [7]
			{
				{
					["combatId"] = 27,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.0078,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 81,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 81.0078,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797127,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Small Crag Boar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["a_amt"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 12,
										["targets"] = {
											["Small Crag Boar"] = 58,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 46,
										["n_min"] = 11,
										["g_dmg"] = 12,
										["counter"] = 5,
										["total"] = 58,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Small Crag Boar"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 81.0078,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 4.66044183638306,
							["custom"] = 0,
							["last_event"] = 1569797125,
							["damage_taken"] = 17.0078,
							["start_time"] = 1569797108,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002152,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 17,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 17.002152,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 17.002152,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-0000112EF1",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorend"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 3,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797127,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 81.002152,
							["start_time"] = 1569797110,
							["delay"] = 0,
							["last_event"] = 1569797125,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 27,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 27,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 27,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 27,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31565.545,
				["tempo_start"] = 1569797108,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 33,
				["playing_solo"] = true,
				["totals"] = {
					98, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					81, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:45:27",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 19.0370000000003,
				["CombatEndedAt"] = 31584.582,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 81.0078,
						}, -- [1]
					},
				},
				["end_time"] = 31584.582,
				["combat_id"] = 27,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:45:08",
				["start_time"] = 31565.435,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [8]
			{
				{
					["combatId"] = 26,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001746,
							["damage_from"] = {
								["Small Crag Boar"] = true,
							},
							["targets"] = {
								["Small Crag Boar"] = 80,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 80.001746,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1569797080,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 13,
										["targets"] = {
											["Small Crag Boar"] = 58,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 47,
										["n_min"] = 10,
										["g_dmg"] = 11,
										["counter"] = 5,
										["total"] = 58,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 10,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Small Crag Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 80.001746,
							["serial"] = "Player-4408-0145B28C",
							["last_dps"] = 5.46833533834548,
							["custom"] = 0,
							["last_event"] = 1569797078,
							["damage_taken"] = 20.001746,
							["start_time"] = 1569797063,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006448,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 20,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 20.006448,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 20.006448,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4414-0-305-708-00021120BF",
							["nome"] = "Small Crag Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorend"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569797080,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 80.006448,
							["start_time"] = 1569797063,
							["delay"] = 0,
							["last_event"] = 1569797077,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 26,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 26,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 26,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 26,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 31521.016,
				["tempo_start"] = 1569797063,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 32,
				["playing_solo"] = true,
				["totals"] = {
					100, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					80, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "18:44:40",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Small Crag Boar",
				["TotalElapsedCombatTime"] = 16.1749999999993,
				["CombatEndedAt"] = 31537.191,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 80.001746,
						}, -- [1]
					},
				},
				["end_time"] = 31537.191,
				["combat_id"] = 26,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "18:44:23",
				["start_time"] = 31520.786,
				["TimeData"] = {
				},
				["frags"] = {
					["Small Crag Boar"] = 1,
				},
			}, -- [9]
			{
				{
					["tipo"] = 2,
					["combatId"] = 25,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005763,
							["damage_from"] = {
								["Burly Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Burly Rockjaw Trogg"] = 56,
							},
							["pets"] = {
							},
							["total"] = 56.005763,
							["last_event"] = 1569618659,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 56.005763,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618661,
							["friendlyfire"] = {
							},
							["damage_taken"] = 10.005763,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 12,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 12,
										["g_dmg"] = 10,
										["counter"] = 3,
										["total"] = 34,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 5.47626508262499,
							["colocacao"] = 1,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569618651,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002596,
							["damage_from"] = {
								["Manlaser"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 10,
							},
							["pets"] = {
							},
							["last_event"] = 1569618659,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.002596,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 10.002596,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Burly Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618661,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 74.002596,
							["start_time"] = 1569618651,
							["serial"] = "Creature-0-4414-0-305-724-00000E79C6",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 25,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 25,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 25,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 25,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19944.738,
				["tempo_start"] = 1569618651,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					65.969943, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:11:02",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Burly Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 10.1369999999988,
				["CombatEndedAt"] = 19954.875,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 56.005763,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19954.875,
				["combat_id"] = 25,
				["frags"] = {
					["Burly Rockjaw Trogg"] = 2,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 28,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					56, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19944.648,
				["TimeData"] = {
				},
				["data_inicio"] = "17:10:51",
			}, -- [10]
			{
				{
					["tipo"] = 2,
					["combatId"] = 24,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007962,
							["damage_from"] = {
								["Burly Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Burly Rockjaw Trogg"] = 56,
							},
							["pets"] = {
							},
							["total"] = 56.007962,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 56.007962,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618633,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 0,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 12,
										["counter"] = 1,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 44,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 44,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 44,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 7.74232264307397,
							["colocacao"] = 1,
							["last_event"] = 1569618632,
							["damage_taken"] = 6.007962,
							["start_time"] = 1569618626,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007152,
							["damage_from"] = {
								["Manlaser"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Manlaser"] = 17,
								["Thorend"] = 6,
							},
							["pets"] = {
							},
							["last_event"] = 1569618650,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 23.007152,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 23.007152,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Burly Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Manlaser"] = 17,
											["Thorend"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 12,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 1,
										["n_amt"] = 12,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618651,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 150.007152,
							["start_time"] = 1569618626,
							["serial"] = "Creature-0-4414-0-305-724-00000E78DB",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 24,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 24,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 24,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 24,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19920.48,
				["tempo_start"] = 1569618626,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					78.980573, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:10:34",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Burly Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 6.83400000000256,
				["CombatEndedAt"] = 19927.314,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 56.007962,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19927.314,
				["combat_id"] = 24,
				["frags"] = {
					["Burly Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 27,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					56, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19920.08,
				["TimeData"] = {
				},
				["data_inicio"] = "17:10:27",
			}, -- [11]
			{
				{
					["tipo"] = 2,
					["combatId"] = 23,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.0073,
							["damage_from"] = {
								["Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Rockjaw Trogg"] = 55,
							},
							["pets"] = {
							},
							["total"] = 55.0073,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 55.0073,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618622,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 12,
										["targets"] = {
											["Rockjaw Trogg"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 11,
										["g_dmg"] = 11,
										["counter"] = 3,
										["total"] = 34,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Rockjaw Trogg"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 5.15435719640069,
							["colocacao"] = 1,
							["last_event"] = 1569618620,
							["damage_taken"] = 9.0073,
							["start_time"] = 1569618611,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001583,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 9,
							},
							["pets"] = {
							},
							["last_event"] = 1569618620,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.001583,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 9.001583,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 9,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618622,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 55.001583,
							["start_time"] = 1569618611,
							["serial"] = "Creature-0-4414-0-305-707-00000E78CC",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 23,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 23,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 23,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 23,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19905.472,
				["tempo_start"] = 1569618611,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					63.98084, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:10:23",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 10.5519999999997,
				["CombatEndedAt"] = 19916.024,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 55.0073,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19916.024,
				["combat_id"] = 23,
				["frags"] = {
					["Ragged Young Wolf"] = 1,
					["Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 26,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					55, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19905.352,
				["TimeData"] = {
				},
				["data_inicio"] = "17:10:12",
			}, -- [12]
			{
				{
					["tipo"] = 2,
					["combatId"] = 22,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002524,
							["damage_from"] = {
								["Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Rockjaw Trogg"] = 45,
							},
							["pets"] = {
							},
							["total"] = 45.002524,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 45.002524,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618592,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Rockjaw Trogg"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 11.114478636703,
							["colocacao"] = 1,
							["last_event"] = 1569618591,
							["damage_taken"] = 2.002524,
							["start_time"] = 1569618588,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008193,
							["damage_from"] = {
								["Manlaser"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Manlaser"] = 13,
								["Thorend"] = 2,
							},
							["pets"] = {
							},
							["last_event"] = 1569618607,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.008193,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 15.008193,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Manlaser"] = 13,
											["Thorend"] = 2,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 9,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 9,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618611,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 119.008193,
							["start_time"] = 1569618588,
							["serial"] = "Creature-0-4414-0-305-707-00000E6693",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 22,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 22,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 22,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 22,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					59.979535, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					45, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:09:53",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 19886.083,
				["CombatEndedAt"] = 19886.083,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 45.002524,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19886.083,
				["combat_id"] = 22,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Rockjaw Trogg"] = 1,
				},
				["data_inicio"] = "17:09:49",
				["tempo_start"] = 1569618588,
				["CombatSkillCache"] = {
				},
				["combat_counter"] = 25,
				["start_time"] = 19882.034,
				["TimeData"] = {
				},
				["player_last_events"] = {
				},
			}, -- [13]
			{
				{
					["tipo"] = 2,
					["combatId"] = 21,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004566,
							["damage_from"] = {
								["Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Rockjaw Trogg"] = 46,
							},
							["pets"] = {
							},
							["total"] = 46.004566,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 46.004566,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618582,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Rockjaw Trogg"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 11,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Rockjaw Trogg"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 6.85509849500888,
							["colocacao"] = 1,
							["last_event"] = 1569618581,
							["damage_taken"] = 3.004566,
							["start_time"] = 1569618576,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008875,
							["damage_from"] = {
								["Manlaser"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Manlaser"] = 8,
								["Thorend"] = 3,
							},
							["pets"] = {
							},
							["last_event"] = 1569618587,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 11.008875,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 11.008875,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Manlaser"] = 8,
											["Thorend"] = 3,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618588,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 94.008875,
							["start_time"] = 1569618576,
							["serial"] = "Creature-0-4414-0-305-707-00008E6679",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 21,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 21,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 21,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 21,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19882.034,
				["tempo_start"] = 1569618576,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					56.994638, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:09:43",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 6.46099999999933,
				["CombatEndedAt"] = 19876.336,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 46.004566,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19876.336,
				["combat_id"] = 21,
				["frags"] = {
					["Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 24,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					46, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19869.625,
				["TimeData"] = {
				},
				["data_inicio"] = "17:09:36",
			}, -- [14]
			{
				{
					["tipo"] = 2,
					["combatId"] = 20,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003249,
							["damage_from"] = {
								["Burly Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Burly Rockjaw Trogg"] = 58,
							},
							["pets"] = {
							},
							["total"] = 58.003249,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 58.003249,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618573,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 12,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 35,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 11,
										["g_dmg"] = 12,
										["counter"] = 3,
										["total"] = 35,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 5.42492040778198,
							["colocacao"] = 1,
							["last_event"] = 1569618572,
							["damage_taken"] = 10.003249,
							["start_time"] = 1569618563,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003431,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 10,
							},
							["pets"] = {
							},
							["last_event"] = 1569618571,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.003431,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 10.003431,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Burly Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618573,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 58.003431,
							["start_time"] = 1569618563,
							["serial"] = "Creature-0-4414-0-305-724-00000E7A14",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 20,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 20,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 20,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 20,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19856.884,
				["tempo_start"] = 1569618563,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					67.984833, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:09:34",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Burly Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 10.5819999999985,
				["CombatEndedAt"] = 19867.466,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 58.003249,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19867.466,
				["combat_id"] = 20,
				["frags"] = {
					["Rockjaw Trogg"] = 1,
					["Burly Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 23,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					58, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19856.774,
				["TimeData"] = {
				},
				["data_inicio"] = "17:09:23",
			}, -- [15]
			{
				{
					["tipo"] = 2,
					["combatId"] = 19,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003266,
							["damage_from"] = {
								["Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Rockjaw Trogg"] = 54,
							},
							["pets"] = {
							},
							["total"] = 54.003266,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 54.003266,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618560,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Rockjaw Trogg"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Rockjaw Trogg"] = 44,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 44,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 44,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 7.85959336341237,
							["colocacao"] = 1,
							["last_event"] = 1569618559,
							["damage_taken"] = 4.003266,
							["start_time"] = 1569618553,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00233,
							["damage_from"] = {
								["Manlaser"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Manlaser"] = 5,
								["Thorend"] = 4,
							},
							["pets"] = {
							},
							["last_event"] = 1569618562,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.00233,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 9.00233,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Manlaser"] = 5,
											["Thorend"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 9,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618563,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 75.00233,
							["start_time"] = 1569618553,
							["serial"] = "Creature-0-4414-0-305-707-00000E7A05",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 19,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 19,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 19,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 19,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19847.176,
				["tempo_start"] = 1569618553,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					62.991788, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:09:21",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 6.87099999999919,
				["CombatEndedAt"] = 19854.047,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 54.003266,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19854.047,
				["combat_id"] = 19,
				["frags"] = {
					["Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 22,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					54, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19847.176,
				["TimeData"] = {
				},
				["data_inicio"] = "17:09:14",
			}, -- [16]
			{
				{
					["tipo"] = 2,
					["combatId"] = 18,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008661,
							["damage_from"] = {
								["Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Rockjaw Trogg"] = 63,
							},
							["pets"] = {
							},
							["total"] = 63.008661,
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 63.008661,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569618551,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Rockjaw Trogg"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Rockjaw Trogg"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 42,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 42,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 7.16577516205952,
							["colocacao"] = 1,
							["last_event"] = 1569618550,
							["damage_taken"] = 10.008661,
							["start_time"] = 1569618541,
							["serial"] = "Player-4408-0145B28C",
							["classe"] = "WARRIOR",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001428,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 10,
							},
							["pets"] = {
							},
							["last_event"] = 1569618549,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.001428,
							["classe"] = "UNKNOW",
							["dps_started"] = false,
							["total"] = 10.001428,
							["delay"] = 0,
							["on_hold"] = false,
							["nome"] = "Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569618551,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 63.001428,
							["start_time"] = 1569618541,
							["serial"] = "Creature-0-4414-0-305-707-00000E6BE5",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 18,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 18,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 18,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 18,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19835.05,
				["tempo_start"] = 1569618541,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					73, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:09:12",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 10.1180000000022,
				["CombatEndedAt"] = 19845.168,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 63.008661,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19845.168,
				["combat_id"] = 18,
				["frags"] = {
					["Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 21,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					63, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19835.05,
				["TimeData"] = {
				},
				["data_inicio"] = "17:09:02",
			}, -- [17]
			{
				{
					["tipo"] = 2,
					["combatId"] = 17,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004885,
							["damage_from"] = {
								["Burly Rockjaw Trogg"] = true,
							},
							["targets"] = {
								["Burly Rockjaw Trogg"] = 57,
							},
							["pets"] = {
							},
							["end_time"] = 1569618537,
							["tipo"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 57.004885,
							["delay"] = 0,
							["dps_started"] = false,
							["total"] = 57.004885,
							["on_hold"] = false,
							["damage_taken"] = 6.004885,
							["nome"] = "Thorend",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 22,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 34,
										["c_max"] = 22,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 22,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Burly Rockjaw Trogg"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 8.56186317212447,
							["colocacao"] = 1,
							["last_event"] = 1569618536,
							["friendlyfire"] = {
							},
							["start_time"] = 1569618530,
							["serial"] = "Player-4408-0145B28C",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005324,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 6,
							},
							["pets"] = {
							},
							["last_event"] = 1569618534,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.005324,
							["end_time"] = 1569618537,
							["dps_started"] = false,
							["total"] = 6.005324,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Burly Rockjaw Trogg",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_amt"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 57.005324,
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569618530,
							["serial"] = "Creature-0-4414-0-305-724-00000E7A1F",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 17,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 17,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 17,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4408-0145B28C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 17,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 19824.168,
				["tempo_start"] = 1569618530,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					62.982577, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "17:08:57",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Burly Rockjaw Trogg",
				["TotalElapsedCombatTime"] = 6.47499999999855,
				["CombatEndedAt"] = 19830.643,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorend"] = 57.004885,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 19830.643,
				["combat_id"] = 17,
				["frags"] = {
					["Burly Rockjaw Trogg"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 20,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					57, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 19823.985,
				["TimeData"] = {
				},
				["data_inicio"] = "17:08:51",
			}, -- [18]
		},
	},
	["last_version"] = "v1.13.2.162",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1570914165,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorend-Faerlina",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 0.2,
			["enabled"] = true,
			["showamount"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["animate"] = false,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "29",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["character_data"] = {
		["logons"] = 4,
	},
	["combat_id"] = 34,
	["savedStyles"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 206.222229003906,
					["x"] = -603.518524169922,
					["w"] = 310.000061035156,
					["h"] = 157.999984741211,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["force_font_outline"] = "",
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.175299,
					["damage_from"] = {
						["Ragged Timber Wolf"] = true,
						["Ragged Young Wolf"] = true,
						["Rockjaw Trogg"] = true,
						["Burly Rockjaw Trogg"] = true,
						["Small Crag Boar"] = true,
					},
					["targets"] = {
						["Ragged Timber Wolf"] = 61,
						["Ragged Young Wolf"] = 453,
						["Rockjaw Trogg"] = 438,
						["Burly Rockjaw Trogg"] = 407,
						["Small Crag Boar"] = 697,
					},
					["pets"] = {
					},
					["total"] = 2056.175299,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 2056.175299,
					["last_dps"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569618229,
					["classe"] = "WARRIOR",
					["damage_taken"] = 306.175299,
					["nome"] = "Thorend",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Small Crag Boar"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[0] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Ragged Young Wolf"] = 0,
									["Small Crag Boar"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 0,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 0,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["MISS"] = 2,
								["DODGE"] = 1,
							},
							["!Melee"] = {
								["c_amt"] = 3,
								["b_amt"] = 1,
								["c_dmg"] = 63,
								["g_amt"] = 16,
								["n_max"] = 13,
								["targets"] = {
									["Ragged Timber Wolf"] = 41,
									["Ragged Young Wolf"] = 170,
									["Rockjaw Trogg"] = 154,
									["Burly Rockjaw Trogg"] = 189,
									["Small Crag Boar"] = 339,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 658,
								["n_min"] = 0,
								["g_dmg"] = 172,
								["counter"] = 76,
								["total"] = 893,
								["c_max"] = 22,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 10,
								["n_amt"] = 57,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							["Heroic Strike"] = {
								["c_amt"] = 3,
								["b_amt"] = 1,
								["c_dmg"] = 126,
								["g_amt"] = 0,
								["n_max"] = 23,
								["targets"] = {
									["Ragged Timber Wolf"] = 20,
									["Ragged Young Wolf"] = 283,
									["Rockjaw Trogg"] = 284,
									["Burly Rockjaw Trogg"] = 218,
									["Small Crag Boar"] = 358,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1037,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 50,
								["total"] = 1163,
								["c_max"] = 44,
								["id"] = "Heroic Strike",
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 20,
								["n_amt"] = 47,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["grupo"] = true,
					["delay"] = 0,
					["on_hold"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1569617901,
					["serial"] = "Player-4408-0145B28C",
					["tipo"] = 1,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.066882,
					["damage_from"] = {
						["Manlaser"] = true,
						["Shujiro"] = true,
						["Thorend"] = true,
					},
					["targets"] = {
						["Manlaser"] = 17,
						["Shujiro"] = 2,
						["Thorend"] = 53,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 72.066882,
					["friendlyfire_total"] = 0,
					["fight_component"] = true,
					["end_time"] = 1569618229,
					["delay"] = 0,
					["on_hold"] = false,
					["nome"] = "Rockjaw Trogg",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Manlaser"] = 0,
									["Thorend"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 0,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["MISS"] = 2,
								["DODGE"] = 1,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Manlaser"] = 17,
									["Shujiro"] = 2,
									["Thorend"] = 53,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 72,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 41,
								["total"] = 72,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 41,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["total"] = 72.066882,
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1569618148,
					["serial"] = "Creature-0-4414-0-305-707-00000E791A",
					["damage_taken"] = 568.066882,
				}, -- [2]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.052288,
					["damage_from"] = {
						["Manlaser"] = true,
						["Shujiro"] = true,
						["Thorend"] = true,
					},
					["targets"] = {
						["Manlaser"] = 8,
						["Shujiro"] = 5,
						["Thorend"] = 53,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 66.052288,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["total"] = 66.052288,
					["delay"] = 0,
					["on_hold"] = false,
					["nome"] = "Burly Rockjaw Trogg",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Shujiro"] = 0,
									["Thorend"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 0,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["MISS"] = 1,
								["DODGE"] = 1,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2,
								["targets"] = {
									["Manlaser"] = 8,
									["Shujiro"] = 5,
									["Thorend"] = 53,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 66,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 34,
								["total"] = 66,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 34,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 484.052288,
					["end_time"] = 1569618229,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["last_event"] = 0,
					["last_dps"] = 0,
					["start_time"] = 1569618160,
					["serial"] = "Creature-0-4414-0-305-724-00000E7555",
					["fight_component"] = true,
				}, -- [3]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.089093,
					["damage_from"] = {
						["Bigsplash"] = true,
						["Demoncrit"] = true,
						["Cisonius"] = true,
						["Gnomerta"] = true,
						["Thorend"] = true,
					},
					["targets"] = {
						["Demoncrit"] = 4,
						["Gnomerta"] = 9,
						["Cisonius"] = 9,
						["Thorend"] = 47,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 69.089093,
					["classe"] = "UNKNOW",
					["dps_started"] = false,
					["end_time"] = 1569618275,
					["delay"] = 0,
					["on_hold"] = false,
					["nome"] = "Ragged Young Wolf",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Gnomerta"] = 0,
									["Cisonius"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 0,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["DODGE"] = 1,
								["MISS"] = 1,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2,
								["targets"] = {
									["Demoncrit"] = 4,
									["Gnomerta"] = 9,
									["Cisonius"] = 9,
									["Thorend"] = 47,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 69,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 45,
								["total"] = 69,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 45,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 756.089093,
					["friendlyfire"] = {
					},
					["total"] = 69.089093,
					["custom"] = 0,
					["last_event"] = 0,
					["last_dps"] = 0,
					["start_time"] = 1569618182,
					["serial"] = "Creature-0-4414-0-305-705-00000E7953",
					["fight_component"] = true,
				}, -- [4]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.007851,
					["damage_from"] = {
						["Thorend"] = true,
					},
					["targets"] = {
						["Thorend"] = 11,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 11.007851,
					["friendlyfire_total"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569618362,
					["delay"] = 0,
					["on_hold"] = false,
					["nome"] = "Ragged Timber Wolf",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorend"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["DODGE"] = 1,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Thorend"] = 11,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 11,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 11,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["total"] = 11.007851,
					["friendlyfire"] = {
					},
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 61.007851,
					["start_time"] = 1569618347,
					["serial"] = "Creature-0-4414-0-305-704-00000E79A1",
					["fight_component"] = true,
				}, -- [5]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.035277,
					["fight_component"] = true,
					["damage_from"] = {
						["Thorend"] = true,
					},
					["targets"] = {
						["Thorend"] = 142,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 142.035277,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1569797080,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Small Crag Boar",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorend"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 0,
								["c_max"] = 0,
								["MISS"] = 4,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 3,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Thorend"] = 142,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 142,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 54,
								["total"] = 142,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 54,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["total"] = 142.035277,
					["serial"] = "Creature-0-4414-0-305-708-00021120BF",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 697.035277,
					["start_time"] = 1569796958,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [6]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["nome"] = "Thorend",
					["grupo"] = true,
					["pets"] = {
					},
					["spell_cast"] = {
						["Heroic Strike"] = 51,
					},
					["classe"] = "WARRIOR",
					["last_event"] = 0,
					["serial"] = "Player-4408-0145B28C",
					["tipo"] = 4,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1569618217,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 16.273000000001,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:47:27",
			}, -- [1]
			{
				["elapsed"] = 12.6129999999976,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:47:07",
			}, -- [2]
			{
				["elapsed"] = 12.5119999999988,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:46:51",
			}, -- [3]
			{
				["elapsed"] = 12.9530000000013,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:46:34",
			}, -- [4]
			{
				["elapsed"] = 12.9530000000013,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:46:09",
			}, -- [5]
			{
				["elapsed"] = 12.7999999999993,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:45:48",
			}, -- [6]
			{
				["elapsed"] = 9.56500000000233,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:45:35",
			}, -- [7]
			{
				["elapsed"] = 19.1470000000008,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:45:08",
			}, -- [8]
			{
				["elapsed"] = 16.4049999999988,
				["type"] = 0,
				["name"] = "Small Crag Boar",
				["clock"] = "18:44:23",
			}, -- [9]
			{
				["elapsed"] = 10.226999999999,
				["type"] = 0,
				["name"] = "Burly Rockjaw Trogg",
				["clock"] = "17:10:51",
			}, -- [10]
			{
				["elapsed"] = 7.23400000000038,
				["type"] = 0,
				["name"] = "Burly Rockjaw Trogg",
				["clock"] = "17:10:27",
			}, -- [11]
			{
				["elapsed"] = 10.6720000000023,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:10:12",
			}, -- [12]
			{
				["elapsed"] = 4.04899999999907,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:09:49",
			}, -- [13]
			{
				["elapsed"] = 6.71099999999933,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:09:36",
			}, -- [14]
			{
				["elapsed"] = 10.6919999999991,
				["type"] = 0,
				["name"] = "Burly Rockjaw Trogg",
				["clock"] = "17:09:23",
			}, -- [15]
			{
				["elapsed"] = 6.87099999999919,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:09:14",
			}, -- [16]
			{
				["elapsed"] = 10.1180000000022,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:09:02",
			}, -- [17]
			{
				["elapsed"] = 6.65799999999945,
				["type"] = 0,
				["name"] = "Burly Rockjaw Trogg",
				["clock"] = "17:08:51",
			}, -- [18]
			{
				["elapsed"] = 6.88300000000163,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:08:39",
			}, -- [19]
			{
				["elapsed"] = 4.59100000000035,
				["type"] = 0,
				["name"] = "Burly Rockjaw Trogg",
				["clock"] = "17:08:31",
			}, -- [20]
			{
				["elapsed"] = 10.4950000000026,
				["type"] = 0,
				["name"] = "Rockjaw Trogg",
				["clock"] = "17:08:16",
			}, -- [21]
			{
				["elapsed"] = 13.0649999999987,
				["type"] = 0,
				["name"] = "Burly Rockjaw Trogg",
				["clock"] = "17:07:59",
			}, -- [22]
			{
				["elapsed"] = 6.87800000000061,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:06:56",
			}, -- [23]
			{
				["elapsed"] = 4.21500000000015,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:06:46",
			}, -- [24]
			{
				["elapsed"] = 6.77200000000084,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:06:22",
			}, -- [25]
			{
				["elapsed"] = 6.88400000000183,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:06:07",
			}, -- [26]
			{
				["elapsed"] = 12.8580000000002,
				["type"] = 0,
				["name"] = "Ragged Timber Wolf",
				["clock"] = "17:05:49",
			}, -- [27]
			{
				["elapsed"] = 12.7520000000004,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:05:22",
			}, -- [28]
			{
				["elapsed"] = 6.94100000000253,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:05:08",
			}, -- [29]
			{
				["elapsed"] = 4.04199999999764,
				["type"] = 0,
				["name"] = "Ragged Young Wolf",
				["clock"] = "17:04:58",
			}, -- [30]
		},
		["combat_counter"] = 3,
		["totals"] = {
			2926.498315, -- [1]
			0.00426, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "17:03:38",
		["end_time"] = 31720.468,
		["cleu_timeline"] = {
		},
		["totals_grupo"] = {
			2056.169093, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = "18:47:43",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["frags"] = {
		},
		["start_time"] = 31390.658,
		["TimeData"] = {
		},
		["cleu_events"] = {
			["n"] = 1,
		},
	},
	["combat_counter"] = 40,
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["last_realversion"] = 140,
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
	},
}
