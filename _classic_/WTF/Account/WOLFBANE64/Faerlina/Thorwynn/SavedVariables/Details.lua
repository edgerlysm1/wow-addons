
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 103,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007528,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Stonetusk Boar"] = true,
							},
							["targets"] = {
								["Stonetusk Boar"] = 122,
							},
							["colocacao"] = 1,
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 122.007528,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 122.007528,
							["classe"] = "PALADIN",
							["serial"] = "Player-4408-014A6E72",
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Stonetusk Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Stonetusk Boar"] = 77,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 77,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 77,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Stonetusk Boar"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["end_time"] = 1583639522,
							["last_dps"] = 13.7566273537058,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 31.007528,
							["start_time"] = 1583639512,
							["delay"] = 0,
							["last_event"] = 1583639521,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006042,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 31,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.006042,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 31.006042,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-13-113-0000E46878",
							["nome"] = "Stonetusk Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Thorwynn"] = 31,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 31,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 31,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1583639522,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 122.006042,
							["start_time"] = 1583639513,
							["delay"] = 0,
							["last_event"] = 1583639521,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 103,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 103,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 103,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1583639522,
							["buff_uptime"] = 30,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Blessing of Might"] = {
										["activedamt"] = 1,
										["id"] = "Blessing of Might",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 103,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1583639512,
				["enemy"] = "Stonetusk Boar",
				["combat_counter"] = 108,
				["playing_solo"] = true,
				["totals"] = {
					153, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 9218.075,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:51:52",
				["end_time"] = 9218.075,
				["totals_grupo"] = {
					122, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 103,
				["TotalElapsedCombatTime"] = 9218.075,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 122.007528,
						}, -- [1]
					},
				},
				["frags"] = {
					["Stonetusk Boar"] = 1,
				},
				["data_fim"] = "22:52:02",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 9208.334,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 102,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006997,
							["damage_from"] = {
								["Kobold Tunneler"] = true,
							},
							["targets"] = {
								["Kobold Tunneler"] = 120,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 120.006997,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639496,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Kobold Tunneler"] = 75,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Kobold Tunneler"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Kobold Tunneler"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 120.006997,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 10.5891641224737,
							["custom"] = 0,
							["last_event"] = 1583639494,
							["damage_taken"] = 24.006997,
							["start_time"] = 1583639485,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.005165,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Tessahe"] = true,
								["Banoo"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 24,
								["Tessahe"] = 10,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.005165,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1583639512,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Kobold Tunneler",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Thorwynn"] = 24,
											["Tessahe"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 34,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 8,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 34.005165,
							["serial"] = "Creature-0-4411-0-13-475-0000E46B4D",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1583639500,
							["damage_taken"] = 202.005165,
							["start_time"] = 1583639495,
							["delay"] = 1583639500,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 102,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 102,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 102,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 35,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Blessing of Might"] = {
										["activedamt"] = 1,
										["id"] = "Blessing of Might",
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639496,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 102,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 9207.938,
				["tempo_start"] = 1583639484,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 107,
				["playing_solo"] = true,
				["totals"] = {
					153.988556, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					120, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:51:36",
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Tunneler",
				["TotalElapsedCombatTime"] = 9192.16,
				["CombatEndedAt"] = 9192.16,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 120.006997,
						}, -- [1]
					},
				},
				["end_time"] = 9192.16,
				["combat_id"] = 102,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:51:24",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Kobold Tunneler"] = 1,
				},
				["start_time"] = 9180.827,
				["TimeData"] = {
				},
				["contra"] = "Kobold Tunneler",
			}, -- [2]
			{
				{
					["combatId"] = 101,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007617,
							["damage_from"] = {
								["Kobold Tunneler"] = true,
							},
							["targets"] = {
								["Kobold Tunneler"] = 136,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 136.007617,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639478,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Kobold Tunneler"] = 14,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 39,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Kobold Tunneler"] = 74,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 35,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 74,
										["c_max"] = 39,
										["a_amt"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 39,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Kobold Tunneler"] = 48,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 48,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 48,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 136.007617,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 13.3629020436237,
							["custom"] = 0,
							["last_event"] = 1583639477,
							["damage_taken"] = 25.007617,
							["start_time"] = 1583639467,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.006188,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 25,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 25.006188,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1583639478,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Kobold Tunneler",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Thorwynn"] = 25,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 25,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 25,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 25.006188,
							["serial"] = "Creature-0-4411-0-13-475-0000646B4D",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1583639475,
							["damage_taken"] = 136.006188,
							["start_time"] = 1583639467,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 101,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 101,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 101,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 31,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Blessing of Might"] = {
										["activedamt"] = 1,
										["id"] = "Blessing of Might",
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639478,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 101,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 9180.026,
				["tempo_start"] = 1583639467,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 106,
				["playing_solo"] = true,
				["totals"] = {
					161, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					136, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:51:18",
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Tunneler",
				["TotalElapsedCombatTime"] = 9173.942,
				["CombatEndedAt"] = 9173.942,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 136.007617,
						}, -- [1]
					},
				},
				["end_time"] = 9173.942,
				["combat_id"] = 101,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:51:07",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Kobold Tunneler"] = 1,
				},
				["start_time"] = 9163.024,
				["TimeData"] = {
				},
				["contra"] = "Kobold Tunneler",
			}, -- [3]
			{
				{
					["combatId"] = 100,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.008241,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 34,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.008241,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1583639455,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Kobold Tunneler",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Thorwynn"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 8,
										["MISS"] = 1,
										["total"] = 34,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 34.008241,
							["serial"] = "Creature-0-4411-0-13-475-0000646ACF",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1583639453,
							["damage_taken"] = 136.008241,
							["start_time"] = 1583639439,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007025,
							["damage_from"] = {
								["Kobold Tunneler"] = true,
							},
							["targets"] = {
								["Kobold Tunneler"] = 136,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 136.007025,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639455,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Kobold Tunneler"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Kobold Tunneler"] = 91,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 91,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 91,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Kobold Tunneler"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 136.007025,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 9.24400360225667,
							["custom"] = 0,
							["last_event"] = 1583639453,
							["damage_taken"] = 34.007025,
							["start_time"] = 1583639440,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 100,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 100,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 100,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 46,
							["spell_cast"] = {
								["Seal of Righteousness"] = 2,
								["Judgement"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 16,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 14,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Blessing of Might"] = {
										["activedamt"] = 1,
										["id"] = "Blessing of Might",
										["targets"] = {
										},
										["uptime"] = 16,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639455,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 100,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 9161.807,
				["tempo_start"] = 1583639439,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 105,
				["playing_solo"] = true,
				["totals"] = {
					170, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					136, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:50:55",
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Tunneler",
				["TotalElapsedCombatTime"] = 9151.281,
				["CombatEndedAt"] = 9151.281,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 136.007025,
						}, -- [1]
					},
				},
				["end_time"] = 9151.281,
				["combat_id"] = 100,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:50:39",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Kobold Tunneler"] = 1,
				},
				["start_time"] = 9135.09,
				["TimeData"] = {
				},
				["contra"] = "Kobold Tunneler",
			}, -- [4]
			{
				{
					["combatId"] = 99,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006155,
							["damage_from"] = {
								["Stonetusk Boar"] = true,
							},
							["targets"] = {
								["Stonetusk Boar"] = 126,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 126.006155,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639377,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Stonetusk Boar"] = 28,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 28,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 28,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 2,
										["n_max"] = 20,
										["targets"] = {
											["Stonetusk Boar"] = 74,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 37,
										["n_min"] = 17,
										["g_dmg"] = 37,
										["counter"] = 4,
										["total"] = 74,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Stonetusk Boar"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 126.006155,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 12.8315840122203,
							["custom"] = 0,
							["last_event"] = 1583639376,
							["damage_taken"] = 29.006155,
							["start_time"] = 1583639367,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008084,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 29,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 29.008084,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 29.008084,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-13-113-0000646B41",
							["nome"] = "Stonetusk Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Thorwynn"] = 29,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 29,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 29,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1583639377,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 126.008084,
							["start_time"] = 1583639367,
							["delay"] = 0,
							["last_event"] = 1583639375,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 99,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 99,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 99,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 30,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Blessing of Might"] = {
										["activedamt"] = 1,
										["id"] = "Blessing of Might",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639377,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 99,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 9134.251,
				["tempo_start"] = 1583639367,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 104,
				["playing_solo"] = true,
				["totals"] = {
					155, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					126, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:49:38",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Stonetusk Boar",
				["TotalElapsedCombatTime"] = 9073.877,
				["CombatEndedAt"] = 9073.877,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 126.006155,
						}, -- [1]
					},
				},
				["end_time"] = 9073.877,
				["combat_id"] = 99,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "22:49:27",
				["start_time"] = 9062.985,
				["TimeData"] = {
				},
				["frags"] = {
					["Stonetusk Boar"] = 1,
				},
			}, -- [5]
			{
				{
					["combatId"] = 98,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.0073,
							["damage_from"] = {
								["Stonetusk Boar"] = true,
							},
							["targets"] = {
								["Stonetusk Boar"] = 102,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 102.0073,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639360,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Stonetusk Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Stonetusk Boar"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 15,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 36,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Stonetusk Boar"] = 36,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 36,
										["c_max"] = 36,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 36,
									},
								},
							},
							["grupo"] = true,
							["total"] = 102.0073,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 13.8240005420773,
							["custom"] = 0,
							["last_event"] = 1583639359,
							["damage_taken"] = 17.0073,
							["start_time"] = 1583639352,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006493,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 17,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 17.006493,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 17.006493,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-13-113-00006464B7",
							["nome"] = "Stonetusk Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Thorwynn"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1583639360,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 102.006493,
							["start_time"] = 1583639352,
							["delay"] = 0,
							["last_event"] = 1583639358,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 98,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 98,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 98,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 16,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Blessing of Might"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Blessing of Might"] = {
										["activedamt"] = 1,
										["id"] = "Blessing of Might",
										["targets"] = {
										},
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639360,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 98,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 9062.612,
				["tempo_start"] = 1583639352,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 103,
				["playing_solo"] = true,
				["totals"] = {
					119, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					102, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:49:20",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Stonetusk Boar",
				["TotalElapsedCombatTime"] = 9056.558,
				["CombatEndedAt"] = 9056.558,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 102.0073,
						}, -- [1]
					},
				},
				["end_time"] = 9056.558,
				["combat_id"] = 98,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "22:49:12",
				["start_time"] = 9048.028,
				["TimeData"] = {
				},
				["frags"] = {
					["Stonetusk Boar"] = 1,
				},
			}, -- [6]
			{
				{
					["combatId"] = 97,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004495,
							["damage_from"] = {
								["Stonetusk Boar"] = true,
							},
							["targets"] = {
								["Stonetusk Boar"] = 117,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 117.004495,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639333,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Stonetusk Boar"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 32,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Stonetusk Boar"] = 80,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 48,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 80,
										["c_max"] = 32,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 32,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Stonetusk Boar"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 117.004495,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 9.95105417588041,
							["custom"] = 0,
							["last_event"] = 1583639332,
							["damage_taken"] = 21.004495,
							["start_time"] = 1583639320,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00748,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 21,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 21.00748,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 21.00748,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-13-113-0000646A38",
							["nome"] = "Stonetusk Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Thorwynn"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 21,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1583639333,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 117.00748,
							["start_time"] = 1583639320,
							["delay"] = 0,
							["last_event"] = 1583639331,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 97,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 97,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 97,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 25,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 13,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639333,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 97,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 9047.633,
				["tempo_start"] = 1583639320,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 102,
				["playing_solo"] = true,
				["totals"] = {
					138, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					117, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:48:53",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Stonetusk Boar",
				["TotalElapsedCombatTime"] = 12.9650000000001,
				["CombatEndedAt"] = 9029.826,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 117.004495,
						}, -- [1]
					},
				},
				["end_time"] = 9029.826,
				["combat_id"] = 97,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "22:48:40",
				["start_time"] = 9016.449,
				["TimeData"] = {
				},
				["frags"] = {
					["Stonetusk Boar"] = 1,
				},
			}, -- [7]
			{
				{
					["combatId"] = 96,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008526,
							["damage_from"] = {
								["Kobold Tunneler"] = true,
							},
							["targets"] = {
								["Kobold Tunneler"] = 111,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 111.008526,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639307,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Kobold Tunneler"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Kobold Tunneler"] = 66,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 66,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 66,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Kobold Tunneler"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 111.008526,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 12.4911135366282,
							["custom"] = 0,
							["last_event"] = 1583639306,
							["damage_taken"] = 16.008526,
							["start_time"] = 1583639297,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.004737,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 16,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 16.004737,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1583639307,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Kobold Tunneler",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Thorwynn"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 16,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 16.004737,
							["serial"] = "Creature-0-4411-0-13-475-0000E46AB2",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1583639305,
							["damage_taken"] = 111.004737,
							["start_time"] = 1583639297,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 96,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 96,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 96,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1583639307,
							["buff_uptime"] = 20,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 1,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 96,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1583639297,
				["enemy"] = "Kobold Tunneler",
				["combat_counter"] = 101,
				["playing_solo"] = true,
				["totals"] = {
					127, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 9003.499,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:48:17",
				["end_time"] = 9003.499,
				["totals_grupo"] = {
					111, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 96,
				["TotalElapsedCombatTime"] = 9003.499,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 111.008526,
						}, -- [1]
					},
				},
				["frags"] = {
					["Kobold Tunneler"] = 1,
				},
				["data_fim"] = "22:48:27",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 8993.317,
				["contra"] = "Kobold Tunneler",
				["TimeData"] = {
				},
			}, -- [8]
			{
				{
					["combatId"] = 95,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004446,
							["damage_from"] = {
								["Stonetusk Boar"] = true,
							},
							["targets"] = {
								["Stonetusk Boar"] = 121,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 121.004446,
							["friendlyfire"] = {
							},
							["end_time"] = 1583639276,
							["dps_started"] = false,
							["total"] = 121.004446,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Stonetusk Boar"] = 14,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 15,
										["targets"] = {
											["Stonetusk Boar"] = 59,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 43,
										["n_min"] = 14,
										["g_dmg"] = 16,
										["counter"] = 5,
										["total"] = 59,
										["c_max"] = 0,
										["a_amt"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Stonetusk Boar"] = 48,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 48,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 48,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 9.86824710487635,
							["custom"] = 0,
							["last_event"] = 1583639274,
							["damage_taken"] = 32.004446,
							["start_time"] = 1583639262,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00807,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 32,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 32.00807,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 32.00807,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-13-113-0000E46626",
							["nome"] = "Stonetusk Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Thorwynn"] = 32,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 32,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 32,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1583639276,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 121.00807,
							["start_time"] = 1583639263,
							["delay"] = 0,
							["last_event"] = 1583639273,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 95,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 95,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 95,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 27,
							["spell_cast"] = {
								["Seal of Righteousness"] = 2,
								["Judgement"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 13,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 14,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639276,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 95,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 8992.536,
				["tempo_start"] = 1583639262,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 100,
				["playing_solo"] = true,
				["totals"] = {
					153, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					121, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:47:56",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Stonetusk Boar",
				["TotalElapsedCombatTime"] = 8972.321,
				["CombatEndedAt"] = 8972.321,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 121.004446,
						}, -- [1]
					},
				},
				["end_time"] = 8972.321,
				["combat_id"] = 95,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "22:47:42",
				["start_time"] = 8958.542,
				["TimeData"] = {
				},
				["frags"] = {
					["Stonetusk Boar"] = 1,
				},
			}, -- [9]
			{
				{
					["combatId"] = 94,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002666,
							["damage_from"] = {
								["Stonetusk Boar"] = true,
							},
							["targets"] = {
								["Stonetusk Boar"] = 129,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 129.002666,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1583639237,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorwynn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 34,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Stonetusk Boar"] = 84,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 50,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 84,
										["c_max"] = 34,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 34,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Stonetusk Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Stonetusk Boar"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 129.002666,
							["serial"] = "Player-4408-014A6E72",
							["last_dps"] = 11.0941405228752,
							["custom"] = 0,
							["last_event"] = 1583639235,
							["damage_taken"] = 22.002666,
							["start_time"] = 1583639224,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005421,
							["damage_from"] = {
								["Thorwynn"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 22,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 22.005421,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 22.005421,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4411-0-13-113-00006468BC",
							["nome"] = "Stonetusk Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Thorwynn"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 22,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1583639237,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 129.005421,
							["start_time"] = 1583639224,
							["delay"] = 0,
							["last_event"] = 1583639234,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 94,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 94,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 94,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["grupo"] = true,
							["nome"] = "Thorwynn",
							["buff_uptime"] = 26,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["activedamt"] = 2,
										["id"] = "Seal of Righteousness",
										["targets"] = {
										},
										["uptime"] = 13,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									["Devotion Aura"] = {
										["activedamt"] = 1,
										["id"] = "Devotion Aura",
										["targets"] = {
										},
										["uptime"] = 13,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-4408-014A6E72",
							["last_event"] = 1583639237,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 94,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 8958.069,
				["tempo_start"] = 1583639224,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 99,
				["playing_solo"] = true,
				["totals"] = {
					151, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					129, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:47:17",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Stonetusk Boar",
				["TotalElapsedCombatTime"] = 12.5249999999996,
				["CombatEndedAt"] = 8932.99,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorwynn"] = 129.002666,
						}, -- [1]
					},
				},
				["end_time"] = 8932.99,
				["combat_id"] = 94,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "22:47:04",
				["start_time"] = 8920.172,
				["TimeData"] = {
				},
				["frags"] = {
					["Stonetusk Boar"] = 1,
				},
			}, -- [10]
			{
				{
					["tipo"] = 2,
					["combatId"] = 93,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008376,
							["damage_from"] = {
								["Young Wolf"] = true,
							},
							["targets"] = {
								["Young Wolf"] = 49,
							},
							["pets"] = {
							},
							["last_event"] = 1569811329,
							["end_time"] = 1569811330,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 49.008376,
							["delay"] = 0,
							["dps_started"] = false,
							["total"] = 49.008376,
							["classe"] = "PALADIN",
							["damage_taken"] = 1.008376,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Young Wolf"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Young Wolf"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Young Wolf"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 16.6468668478495,
							["colocacao"] = 1,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811327,
							["serial"] = "Player-4408-014A6E72",
							["on_hold"] = false,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.004814,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Garodin"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 1,
								["Garodin"] = 17,
							},
							["pets"] = {
							},
							["end_time"] = 1583639224,
							["nome"] = "Young Wolf",
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 18.004814,
							["last_event"] = 1569811353,
							["fight_component"] = true,
							["total"] = 18.004814,
							["delay"] = 1569811353,
							["classe"] = "UNKNOW",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Garodin"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorwynn"] = 1,
											["Garodin"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 13,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 71.004814,
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1583639198,
							["serial"] = "Creature-0-4411-0-302-299-0000116B00",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 93,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 93,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 93,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811330,
							["buff_uptime"] = 9,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 93,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45785.858,
				["tempo_start"] = 1569811327,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					66.980153, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:42:11",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Young Wolf",
				["TotalElapsedCombatTime"] = 2.84200000000419,
				["CombatEndedAt"] = 45788.7,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:42:08",
				["end_time"] = 45788.7,
				["combat_id"] = 93,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Young Wolf"] = 1,
				},
				["combat_counter"] = 97,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					49, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45785.756,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 49.008376,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [11]
			{
				{
					["tipo"] = 2,
					["combatId"] = 92,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005075,
							["damage_from"] = {
								["Kobold Vermin"] = true,
							},
							["targets"] = {
								["Kobold Vermin"] = 61,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 61.005075,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811311,
							["total"] = 61.005075,
							["damage_taken"] = 1.005075,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Kobold Vermin"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Vermin"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 33,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Kobold Vermin"] = 33,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 33,
										["c_max"] = 33,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 33,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 18.7881352017072,
							["colocacao"] = 1,
							["last_event"] = 1569811309,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811308,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003674,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Mtaal"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 1,
								["Mtaal"] = 10,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 11.003674,
							["last_event"] = 1569811327,
							["fight_component"] = true,
							["total"] = 11.003674,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Vermin",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Mtaal"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["DODGE"] = 1,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorwynn"] = 1,
											["Mtaal"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 127.003674,
							["end_time"] = 1569811327,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811308,
							["serial"] = "Creature-0-4411-0-302-6-00001168AD",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 92,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 92,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 92,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811311,
							["buff_uptime"] = 8,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 2,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 92,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45766.396,
				["tempo_start"] = 1569811308,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					71.970571, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:41:52",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Vermin",
				["TotalElapsedCombatTime"] = 2.85700000000361,
				["CombatEndedAt"] = 45769.253,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:41:49",
				["end_time"] = 45769.253,
				["combat_id"] = 92,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Worker"] = 1,
					["Kobold Vermin"] = 1,
				},
				["combat_counter"] = 96,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					61, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45766.006,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 61.005075,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [12]
			{
				{
					["tipo"] = 2,
					["combatId"] = 91,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002195,
							["damage_from"] = {
								["Kobold Worker"] = true,
							},
							["targets"] = {
								["Kobold Worker"] = 91,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 91.002195,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811303,
							["total"] = 91.002195,
							["damage_taken"] = 6.002195,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Kobold Worker"] = 63,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 63,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 63,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Kobold Worker"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Worker"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 10.806578197361,
							["colocacao"] = 1,
							["last_event"] = 1569811301,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811295,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.0089,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Mtaal"] = true,
								["Markoriol"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 6,
								["Mtaal"] = 20,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 26.0089,
							["last_event"] = 1569811308,
							["fight_component"] = true,
							["total"] = 26.0089,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Worker",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorwynn"] = 6,
											["Mtaal"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 26,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 10,
										["total"] = 26,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 10,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 169.0089,
							["end_time"] = 1569811308,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811295,
							["serial"] = "Creature-0-4411-0-302-257-0000116A8E",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 91,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 91,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 91,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811303,
							["buff_uptime"] = 22,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 6,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 91,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45753.424,
				["tempo_start"] = 1569811295,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					116.9795, -- [1]
					0, -- [2]
					{
						-0.00143299999999957, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:41:44",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Worker",
				["TotalElapsedCombatTime"] = 8.05200000000332,
				["CombatEndedAt"] = 45761.476,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:41:36",
				["end_time"] = 45761.476,
				["combat_id"] = 91,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Laborer"] = 1,
					["Kobold Worker"] = 1,
				},
				["combat_counter"] = 95,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					91, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45753.055,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 91.002195,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [13]
			{
				{
					["tipo"] = 2,
					["combatId"] = 90,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007708,
							["damage_from"] = {
								["Kobold Laborer"] = true,
							},
							["targets"] = {
								["Kobold Laborer"] = 92,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 92.007708,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811278,
							["total"] = 92.007708,
							["damage_taken"] = 6.007708,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Kobold Laborer"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 38,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Kobold Laborer"] = 58,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 58,
										["c_max"] = 38,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 38,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Laborer"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 15.1055176489895,
							["colocacao"] = 1,
							["last_event"] = 1569811276,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811272,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005881,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Vazer"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 6,
								["Vazer"] = 24,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 30.005881,
							["last_event"] = 1569811294,
							["fight_component"] = true,
							["total"] = 30.005881,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Laborer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Vazer"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["DODGE"] = 1,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Thorwynn"] = 6,
											["Vazer"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 12,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 12,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 272.005881,
							["end_time"] = 1569811295,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811272,
							["serial"] = "Creature-0-4411-0-302-80-0000116AF8",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 90,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 90,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 90,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811278,
							["buff_uptime"] = 18,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 6,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 6,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 6,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 90,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45730.383,
				["tempo_start"] = 1569811272,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					121.981515, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:41:19",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Laborer",
				["TotalElapsedCombatTime"] = 5.69099999999889,
				["CombatEndedAt"] = 45736.074,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:41:13",
				["end_time"] = 45736.074,
				["combat_id"] = 90,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Laborer"] = 2,
				},
				["combat_counter"] = 94,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					92, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45729.983,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 92.007708,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [14]
			{
				{
					["tipo"] = 2,
					["combatId"] = 89,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00115,
							["damage_from"] = {
								["Kobold Laborer"] = true,
							},
							["targets"] = {
								["Kobold Laborer"] = 90,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 90.00115,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811268,
							["total"] = 90.00115,
							["damage_taken"] = 6.00115,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Kobold Laborer"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 36,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Kobold Laborer"] = 56,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 56,
										["c_max"] = 36,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 36,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Laborer"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 17.1332857414909,
							["colocacao"] = 1,
							["last_event"] = 1569811267,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811263,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006921,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Vazer"] = true,
								["Cornhusker"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 6,
								["Vazer"] = 5,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 11.006921,
							["last_event"] = 1569811270,
							["fight_component"] = true,
							["total"] = 11.006921,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Laborer",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorwynn"] = 6,
											["Vazer"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 165.006921,
							["end_time"] = 1569811272,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811263,
							["serial"] = "Creature-0-4411-0-302-80-0001116B04",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 89,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 89,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 89,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811268,
							["buff_uptime"] = 15,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 5,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 5,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 5,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 89,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45721.461,
				["tempo_start"] = 1569811263,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					100.9883, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:41:09",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Laborer",
				["TotalElapsedCombatTime"] = 5.25299999999697,
				["CombatEndedAt"] = 45726.714,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:41:04",
				["end_time"] = 45726.714,
				["combat_id"] = 89,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Laborer"] = 3,
				},
				["combat_counter"] = 93,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					90, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45721.461,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 90.00115,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [15]
			{
				{
					["tipo"] = 2,
					["combatId"] = 88,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002853,
							["damage_from"] = {
								["Kobold Laborer"] = true,
							},
							["targets"] = {
								["Kobold Laborer"] = 112,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 112.002853,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811260,
							["total"] = 112.002853,
							["damage_taken"] = 10.002853,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Kobold Laborer"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 39,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Kobold Laborer"] = 78,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 39,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 78,
										["c_max"] = 39,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 39,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Laborer"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 16.7844826914368,
							["colocacao"] = 1,
							["last_event"] = 1569811259,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811253,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007726,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Vazer"] = true,
								["Cornhusker"] = true,
							},
							["targets"] = {
								["Cornhusker"] = 9,
								["Vazer"] = 6,
								["Thorwynn"] = 10,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 25.007726,
							["last_event"] = 1569811263,
							["fight_component"] = true,
							["total"] = 25.007726,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Laborer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Vazer"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Cornhusker"] = 9,
											["Vazer"] = 6,
											["Thorwynn"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 25,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 25,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 8,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 264.007726,
							["end_time"] = 1569811263,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811253,
							["serial"] = "Creature-0-4411-0-302-80-0000916B18",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 88,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 88,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 88,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811260,
							["buff_uptime"] = 21,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 88,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45711.377,
				["tempo_start"] = 1569811253,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					136.99051, -- [1]
					0, -- [2]
					{
						-0.0052140000000005, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:41:00",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Laborer",
				["TotalElapsedCombatTime"] = 6.46300000000338,
				["CombatEndedAt"] = 45717.84,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:40:54",
				["end_time"] = 45717.84,
				["combat_id"] = 88,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Laborer"] = 2,
				},
				["combat_counter"] = 92,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					112, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45711.167,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 112.002853,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [16]
			{
				{
					["tipo"] = 2,
					["combatId"] = 87,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008359,
							["damage_from"] = {
							},
							["targets"] = {
								["Kobold Laborer"] = 26,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 26.008359,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811251,
							["total"] = 26.008359,
							["damage_taken"] = 0.008359,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Kobold Laborer"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Kobold Laborer"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 9.65417928729386,
							["colocacao"] = 1,
							["last_event"] = 1569811249,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811248,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007059,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Vazer"] = true,
								["Cornhusker"] = true,
							},
							["targets"] = {
								["Vazer"] = 6,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.007059,
							["last_event"] = 1569811253,
							["fight_component"] = true,
							["total"] = 6.007059,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Laborer",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Vazer"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 1,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 87.007059,
							["end_time"] = 1569811253,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811249,
							["serial"] = "Creature-0-4411-0-302-80-0000116B18",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 87,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 87,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 87,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["last_event"] = 1569811251,
							["tipo"] = 4,
							["buff_uptime"] = 9,
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 87,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45706.507,
				["tempo_start"] = 1569811248,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					31.986435, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:40:52",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Laborer",
				["TotalElapsedCombatTime"] = 2.37400000000343,
				["CombatEndedAt"] = 45708.881,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:40:49",
				["end_time"] = 45708.881,
				["combat_id"] = 87,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Laborer"] = 1,
				},
				["combat_counter"] = 91,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					26, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45706.187,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 26.008359,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [17]
			{
				{
					["tipo"] = 2,
					["combatId"] = 86,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003544,
							["damage_from"] = {
								["Kobold Laborer"] = true,
							},
							["targets"] = {
								["Kobold Laborer"] = 89,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 89.003544,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1569811242,
							["total"] = 89.003544,
							["damage_taken"] = 4.003544,
							["nome"] = "Thorwynn",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Laborer"] = 60,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 60,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 60,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									["Seal of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Kobold Laborer"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = "Seal of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
									["Judgement of Righteousness"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Kobold Laborer"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Judgement of Righteousness",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 2,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 12.9970128504708,
							["colocacao"] = 1,
							["last_event"] = 1569811241,
							["friendlyfire"] = {
							},
							["start_time"] = 1569811235,
							["serial"] = "Player-4408-014A6E72",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003131,
							["damage_from"] = {
								["Thorwynn"] = true,
								["Cornhusker"] = true,
							},
							["targets"] = {
								["Thorwynn"] = 4,
								["Cornhusker"] = 11,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.003131,
							["last_event"] = 1569811248,
							["fight_component"] = true,
							["total"] = 15.003131,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Kobold Laborer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorwynn"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Thorwynn"] = 4,
											["Cornhusker"] = 11,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 141.003131,
							["end_time"] = 1569811248,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1569811238,
							["serial"] = "Creature-0-4411-0-302-80-0000916B0E",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 86,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 86,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 86,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorwynn",
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1569811242,
							["buff_uptime"] = 21,
							["tipo"] = 4,
							["spell_cast"] = {
								["Seal of Righteousness"] = 1,
								["Judgement"] = 1,
							},
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									["Devotion Aura"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Devotion Aura",
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									["Seal of Righteousness"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = "Seal of Righteousness",
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									["Blessing of Might"] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = "Blessing of Might",
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-4408-014A6E72",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 86,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorwynn"] = true,
				},
				["CombatStartedAt"] = 45693.953,
				["tempo_start"] = 1569811235,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					103.99409, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "22:40:43",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Kobold Laborer",
				["TotalElapsedCombatTime"] = 6.45799999999872,
				["CombatEndedAt"] = 45700.411,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:40:36",
				["end_time"] = 45700.411,
				["combat_id"] = 86,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Kobold Laborer"] = 1,
				},
				["combat_counter"] = 90,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					89, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 45693.563,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorwynn"] = 89.003544,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [18]
		},
	},
	["last_version"] = "v1.13.2.177",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1584935116,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorwynn-Faerlina",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 0.2,
			["showamount"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "07",
	["cached_talents"] = {
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 103,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 108,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.547798,
					["damage_from"] = {
						["Kobold Tunneler"] = true,
						["Kobold Laborer"] = true,
						["Defias Thug"] = true,
						["Young Wolf"] = true,
						["Stonetusk Boar"] = true,
						["Garrick Padfoot"] = true,
						["Kobold Worker"] = true,
						["Timber Wolf"] = true,
						["Kobold Vermin"] = true,
					},
					["targets"] = {
						["Kobold Tunneler"] = 503,
						["Kobold Laborer"] = 1140,
						["Defias Thug"] = 3026,
						["Young Wolf"] = 719,
						["Stonetusk Boar"] = 717,
						["Garrick Padfoot"] = 116,
						["Kobold Worker"] = 1305,
						["Timber Wolf"] = 447,
						["Kobold Vermin"] = 600,
					},
					["pets"] = {
					},
					["end_time"] = 1569797805,
					["classe"] = "PALADIN",
					["raid_targets"] = {
					},
					["total_without_pet"] = 8573.547798,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["total"] = 8573.547798,
					["friendlyfire_total"] = 0,
					["last_event"] = 0,
					["nome"] = "Thorwynn",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Garrick Padfoot"] = 0,
									["Defias Thug"] = 0,
									["Kobold Vermin"] = 0,
									["Kobold Worker"] = 0,
									["Timber Wolf"] = 0,
									["Kobold Laborer"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 15,
								["MISS"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["DODGE"] = 7,
								["id"] = 1,
								["r_dmg"] = 0,
								["b_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 7,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[0] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Kobold Laborer"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 0,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["n_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["RESIST"] = 1,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							["Seal of Righteousness"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 7,
								["targets"] = {
									["Kobold Tunneler"] = 77,
									["Kobold Laborer"] = 149,
									["Defias Thug"] = 643,
									["Young Wolf"] = 117,
									["Stonetusk Boar"] = 118,
									["Garrick Padfoot"] = 16,
									["Kobold Worker"] = 299,
									["Timber Wolf"] = 47,
									["Kobold Vermin"] = 132,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1598,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 297,
								["total"] = 1598,
								["c_max"] = 0,
								["id"] = "Seal of Righteousness",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 297,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
							["!Melee"] = {
								["c_amt"] = 24,
								["b_amt"] = 3,
								["c_dmg"] = 598,
								["g_amt"] = 29,
								["n_max"] = 22,
								["targets"] = {
									["Kobold Tunneler"] = 306,
									["Kobold Laborer"] = 713,
									["Defias Thug"] = 2237,
									["Young Wolf"] = 454,
									["Stonetusk Boar"] = 419,
									["Garrick Padfoot"] = 61,
									["Kobold Worker"] = 918,
									["Timber Wolf"] = 284,
									["Kobold Vermin"] = 413,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 4898,
								["n_min"] = 0,
								["g_dmg"] = 309,
								["counter"] = 419,
								["total"] = 5805,
								["c_max"] = 39,
								["a_amt"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["DODGE"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 2,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 23,
								["n_amt"] = 362,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
							["Judgement of Righteousness"] = {
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 133,
								["g_amt"] = 0,
								["n_max"] = 24,
								["targets"] = {
									["Kobold Tunneler"] = 120,
									["Kobold Laborer"] = 278,
									["Defias Thug"] = 146,
									["Young Wolf"] = 148,
									["Stonetusk Boar"] = 180,
									["Garrick Padfoot"] = 39,
									["Kobold Worker"] = 88,
									["Timber Wolf"] = 116,
									["Kobold Vermin"] = 55,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1037,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 51,
								["total"] = 1170,
								["c_max"] = 36,
								["id"] = "Judgement of Righteousness",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 47,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["grupo"] = true,
					["serial"] = "Player-4408-014A6E72",
					["last_dps"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 1666.547798,
					["start_time"] = 1569796622,
					["delay"] = 0,
					["on_hold"] = false,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.132509,
					["damage_from"] = {
						["Sourcin"] = true,
						["Garodin"] = true,
						["Legostarwars"] = true,
						["Thorwynn"] = true,
						["Cokerage"] = true,
						["Guissy"] = true,
						["Ditnoka"] = true,
						["Absolutionn"] = true,
						["Wolfguard"] = true,
					},
					["targets"] = {
						["Wolfguard"] = 9,
						["Guissy"] = 3,
						["Ditnoka"] = 9,
						["Garodin"] = 0,
						["Thorwynn"] = 42,
						["Cokerage"] = 2,
						["Absolutionn"] = 17,
						["Sourcin"] = 1,
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 83.132509,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["total"] = 83.132509,
					["serial"] = "Creature-0-4411-0-302-299-0000113572",
					["damage_taken"] = 1419.132509,
					["nome"] = "Young Wolf",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Wolfguard"] = 0,
									["Garodin"] = 0,
									["Thorwynn"] = 0,
									["Absolutionn"] = 0,
									["Cokerage"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["total"] = 0,
								["c_max"] = 0,
								["DODGE"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 10,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 2,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2,
								["targets"] = {
									["Wolfguard"] = 9,
									["Guissy"] = 3,
									["Ditnoka"] = 9,
									["Garodin"] = 0,
									["Thorwynn"] = 42,
									["Cokerage"] = 2,
									["Absolutionn"] = 17,
									["Sourcin"] = 1,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 83,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 66,
								["total"] = 83,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 2,
								["n_amt"] = 66,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["on_hold"] = false,
					["end_time"] = 1569797805,
					["friendlyfire_total"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1569797697,
					["delay"] = 0,
					["fight_component"] = true,
				}, -- [2]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.133759,
					["damage_from"] = {
						["Sarolyn"] = true,
						["Ditnoka"] = true,
						["Cymye"] = true,
						["Thorwynn"] = true,
						["Mtaal"] = true,
						["Quinscrub"] = true,
						["Janaomille"] = true,
					},
					["targets"] = {
						["Sarolyn"] = 4,
						["Ditnoka"] = 17,
						["Thorwynn"] = 63,
						["Cymye"] = 12,
						["Mtaal"] = 2,
						["Janaomille"] = 2,
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 100.133759,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["total"] = 100.133759,
					["serial"] = "Creature-0-4411-0-302-6-000011328D",
					["damage_taken"] = 875.133759,
					["nome"] = "Kobold Vermin",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorwynn"] = 0,
									["Ditnoka"] = 0,
									["Mtaal"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 0,
								["c_max"] = 0,
								["DODGE"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 3,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 1,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Sarolyn"] = 4,
									["Ditnoka"] = 17,
									["Thorwynn"] = 63,
									["Cymye"] = 12,
									["Mtaal"] = 2,
									["Janaomille"] = 2,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 100,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 65,
								["total"] = 100,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 1,
								["n_amt"] = 65,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["on_hold"] = false,
					["end_time"] = 1569798064,
					["friendlyfire_total"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1569797927,
					["delay"] = 0,
					["fight_component"] = true,
				}, -- [3]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.074576,
					["damage_from"] = {
						["Griffeth"] = true,
						["Ditnoka"] = true,
						["Absolutionn"] = true,
						["Thorwynn"] = true,
						["Legostarwars"] = true,
						["Cokerage"] = true,
						["Guissy"] = true,
					},
					["targets"] = {
						["Griffeth"] = 4,
						["Ditnoka"] = 4,
						["Thorwynn"] = 35,
						["Cokerage"] = 2,
						["Absolutionn"] = 15,
						["Guissy"] = 4,
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 64.074576,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["total"] = 64.074576,
					["serial"] = "Creature-0-4411-0-302-69-00001134A1",
					["fight_component"] = true,
					["nome"] = "Timber Wolf",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorwynn"] = 0,
									["Ditnoka"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 0,
								["c_max"] = 0,
								["MISS"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 2,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Griffeth"] = 4,
									["Ditnoka"] = 4,
									["Thorwynn"] = 35,
									["Cokerage"] = 2,
									["Absolutionn"] = 15,
									["Guissy"] = 4,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 64,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 33,
								["total"] = 64,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 33,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["on_hold"] = false,
					["end_time"] = 1569798080,
					["damage_taken"] = 631.074576,
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1569798004,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
				}, -- [4]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.141031,
					["damage_from"] = {
						["Wolfguard"] = true,
						["Griffeth"] = true,
						["Thorwynn"] = true,
						["Markoriol"] = true,
						["Roelg"] = true,
						["Sarolyn"] = true,
						["Ditnoka"] = true,
						["Berkz"] = true,
						["Blondy"] = true,
						["Quinscrub"] = true,
						["Mtaal"] = true,
					},
					["targets"] = {
						["Wolfguard"] = 12,
						["Berkz"] = 19,
						["Ditnoka"] = 21,
						["Mtaal"] = 11,
						["Thorwynn"] = 205,
						["Blondy"] = 6,
						["Quinscrub"] = 3,
						["Roelg"] = 15,
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 292.141031,
					["friendlyfire"] = {
					},
					["fight_component"] = true,
					["end_time"] = 1569798279,
					["serial"] = "Creature-0-4411-0-302-257-0000113828",
					["last_dps"] = 0,
					["nome"] = "Kobold Worker",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorwynn"] = 0,
									["Berkz"] = 0,
									["Ditnoka"] = 0,
									["Wolfguard"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 14,
								["total"] = 0,
								["c_max"] = 0,
								["MISS"] = 4,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 10,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Wolfguard"] = 12,
									["Berkz"] = 19,
									["Ditnoka"] = 21,
									["Mtaal"] = 11,
									["Thorwynn"] = 205,
									["Blondy"] = 6,
									["Quinscrub"] = 3,
									["Roelg"] = 15,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 292,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 115,
								["total"] = 292,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 115,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["on_hold"] = false,
					["total"] = 292.141031,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 1927.141031,
					["start_time"] = 1569798065,
					["delay"] = 0,
					["dps_started"] = false,
				}, -- [5]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.169361,
					["damage_from"] = {
						["Projectbaby"] = true,
						["Ditnoka"] = true,
						["Absolutionn"] = true,
						["Thorwynn"] = true,
						["Blondy"] = true,
						["Leönes"] = true,
						["Serpenthelm"] = true,
					},
					["targets"] = {
						["Projectbaby"] = 106,
						["Ditnoka"] = 156,
						["Absolutionn"] = 102,
						["Thorwynn"] = 931,
						["Blondy"] = 13,
						["Leönes"] = 30,
						["Serpenthelm"] = 36,
					},
					["pets"] = {
					},
					["dps_started"] = false,
					["damage_taken"] = 4906.169361,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 1374.169361,
					["serial"] = "Creature-0-4411-0-302-38-0000112CFC",
					["fight_component"] = true,
					["total"] = 1374.169361,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Defias Thug",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Projectbaby"] = 0,
									["Ditnoka"] = 0,
									["Thorwynn"] = 0,
									["Absolutionn"] = 0,
									["Leönes"] = 0,
									["Serpenthelm"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 41,
								["total"] = 0,
								["c_max"] = 0,
								["MISS"] = 17,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 24,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 1,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5,
								["targets"] = {
									["Projectbaby"] = 106,
									["Ditnoka"] = 156,
									["Absolutionn"] = 102,
									["Thorwynn"] = 931,
									["Blondy"] = 13,
									["Leönes"] = 30,
									["Serpenthelm"] = 36,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1374,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 431,
								["total"] = 1374,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 2,
								["n_amt"] = 431,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["monster"] = true,
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1569798515,
					["delay"] = 0,
					["end_time"] = 1569799071,
				}, -- [6]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.00954,
					["damage_from"] = {
						["Thorwynn"] = true,
						["Absolutionn"] = true,
					},
					["targets"] = {
						["Thorwynn"] = 33,
						["Absolutionn"] = 25,
						["Leönes"] = 0,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["last_dps"] = 0,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 58.00954,
					["serial"] = "Creature-0-4411-0-302-103-0000113E36",
					["dps_started"] = false,
					["total"] = 58.00954,
					["friendlyfire"] = {
					},
					["on_hold"] = false,
					["nome"] = "Garrick Padfoot",
					["spells"] = {
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5,
								["targets"] = {
									["Thorwynn"] = 33,
									["Absolutionn"] = 25,
									["Leönes"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 58,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 58,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 13,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
							["Defensive Stance"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = "Defensive Stance",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_event"] = 0,
					["end_time"] = 1569799796,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 207.00954,
					["start_time"] = 1569799770,
					["delay"] = 0,
					["monster"] = true,
				}, -- [7]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.080962,
					["damage_from"] = {
						["Vazer"] = true,
						["Karnam"] = true,
						["Thorwynn"] = true,
						["Cornhusker"] = true,
						["Jakkarn"] = true,
						["Darkie"] = true,
					},
					["targets"] = {
						["Berkz"] = 0,
						["Vazer"] = 78,
						["Healinghoe"] = 0,
						["Cornhusker"] = 17,
						["Darkie"] = 8,
						["Quinscrub"] = 0,
						["Calian"] = 0,
						["Broocck"] = 0,
						["Williefister"] = 0,
						["Karnam"] = 0,
						["Jakkarn"] = 24,
						["Thorwynn"] = 106,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 233.080962,
					["damage_taken"] = 2250.080962,
					["fight_component"] = true,
					["total"] = 233.080962,
					["delay"] = 0,
					["friendlyfire"] = {
					},
					["nome"] = "Kobold Laborer",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Berkz"] = 0,
									["Vazer"] = 0,
									["Healinghoe"] = 0,
									["Thorwynn"] = 0,
									["Broocck"] = 0,
									["Quinscrub"] = 0,
									["Darkie"] = 0,
									["Calian"] = 0,
									["Williefister"] = 0,
									["Jakkarn"] = 0,
									["Cornhusker"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 14,
								["total"] = 0,
								["c_max"] = 0,
								["c_min"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["b_dmg"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["MISS"] = 7,
								["DODGE"] = 7,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 1,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 4,
								["targets"] = {
									["Berkz"] = 0,
									["Vazer"] = 78,
									["Healinghoe"] = 0,
									["Cornhusker"] = 17,
									["Darkie"] = 8,
									["Quinscrub"] = 0,
									["Calian"] = 0,
									["Broocck"] = 0,
									["Williefister"] = 0,
									["Karnam"] = 0,
									["Jakkarn"] = 24,
									["Thorwynn"] = 106,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 233,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 79,
								["total"] = 233,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 2,
								["n_amt"] = 79,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["end_time"] = 1569808776,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1569808679,
					["serial"] = "Creature-0-4411-0-302-80-0000116166",
					["dps_started"] = false,
				}, -- [8]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.046229,
					["fight_component"] = true,
					["damage_from"] = {
						["Thorwynn"] = true,
					},
					["targets"] = {
						["Thorwynn"] = 152,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 152.046229,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1583639237,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Stonetusk Boar",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 7,
								["targets"] = {
									["Thorwynn"] = 152,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 152,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 32,
								["total"] = 152,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["DODGE"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 29,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["total"] = 152.046229,
					["serial"] = "Creature-0-4411-0-13-113-00006468BC",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 717.046229,
					["start_time"] = 1583639168,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [9]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.02581,
					["damage_from"] = {
						["Thorwynn"] = true,
					},
					["targets"] = {
						["Thorwynn"] = 99,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 99.02581,
					["last_event"] = 0,
					["on_hold"] = false,
					["monster"] = true,
					["total"] = 99.02581,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Kobold Tunneler",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 7,
								["targets"] = {
									["Thorwynn"] = 99,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 99,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 24,
								["total"] = 99,
								["c_max"] = 0,
								["MISS"] = 2,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["DODGE"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 20,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1583639307,
					["serial"] = "Creature-0-4411-0-13-475-0000E46AB2",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1583639255,
					["delay"] = 0,
					["damage_taken"] = 503.02581,
				}, -- [10]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorb"] = 0.011479,
					["last_hps"] = 0,
					["healing_from"] = {
						["Thorwynn"] = true,
					},
					["targets"] = {
						["Thorwynn"] = 0,
					},
					["end_time"] = 1569799071,
					["pets"] = {
					},
					["totalover_without_pet"] = 0.011479,
					["targets_overheal"] = {
					},
					["classe"] = "PALADIN",
					["totalover"] = 0.011479,
					["total_without_pet"] = 212.011479,
					["iniciar_hps"] = false,
					["start_time"] = 1569799066,
					["fight_component"] = true,
					["total"] = 212.011479,
					["healing_taken"] = 212.011479,
					["heal_enemy_amt"] = 0,
					["nome"] = "Thorwynn",
					["targets_absorbs"] = {
					},
					["grupo"] = true,
					["spells"] = {
						["_ActorTable"] = {
							["Holy Light"] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 47,
								["targets"] = {
									["Thorwynn"] = 212,
								},
								["n_min"] = 0,
								["counter"] = 4,
								["overheal"] = 0,
								["total"] = 212,
								["c_max"] = 72,
								["id"] = "Holy Light",
								["targets_absorbs"] = {
								},
								["c_curado"] = 72,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 140,
								["n_amt"] = 3,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["heal_enemy"] = {
					},
					["serial"] = "Player-4408-014A6E72",
					["custom"] = 0,
					["tipo"] = 2,
					["on_hold"] = false,
					["totaldenied"] = 0.011479,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["fight_component"] = true,
					["buff_uptime_targets"] = {
					},
					["nome"] = "Thorwynn",
					["grupo"] = true,
					["pets"] = {
					},
					["spell_cast"] = {
						["Blessing of Might"] = 1,
						["Holy Light"] = 4,
						["Seal of Righteousness"] = 67,
						["Judgement"] = 31,
					},
					["buff_uptime"] = 2220,
					["classe"] = "PALADIN",
					["last_event"] = 0,
					["buff_uptime_spells"] = {
						["_ActorTable"] = {
							["Devotion Aura"] = {
								["refreshamt"] = 0,
								["activedamt"] = 79,
								["appliedamt"] = 79,
								["id"] = "Devotion Aura",
								["uptime"] = 976,
								["targets"] = {
								},
								["counter"] = 0,
							},
							["Seal of Righteousness"] = {
								["refreshamt"] = 16,
								["activedamt"] = 127,
								["appliedamt"] = 127,
								["id"] = "Seal of Righteousness",
								["uptime"] = 1033,
								["targets"] = {
								},
								["counter"] = 0,
							},
							["Blessing of Might"] = {
								["refreshamt"] = 0,
								["activedamt"] = 30,
								["appliedamt"] = 30,
								["id"] = "Blessing of Might",
								["uptime"] = 211,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["serial"] = "Player-4408-014A6E72",
					["tipo"] = 4,
				}, -- [1]
				{
					["monster"] = true,
					["tipo"] = 4,
					["nome"] = "Garrick Padfoot",
					["pets"] = {
					},
					["spell_cast"] = {
						["Defensive Stance"] = 2,
					},
					["flag_original"] = 68168,
					["last_event"] = 0,
					["fight_component"] = true,
					["serial"] = "Creature-0-4411-0-302-103-0000113E36",
					["classe"] = "UNKNOW",
				}, -- [2]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1569797798,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["spells_cast_timeline"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			11029.11601, -- [1]
			211.998746, -- [2]
			{
				-0.0070809999999959, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "18:56:38",
		["end_time"] = 9218.075,
		["cleu_events"] = {
			["n"] = 1,
		},
		["totals_grupo"] = {
			8573.54512, -- [1]
			212.009114, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["frags"] = {
		},
		["hasSaved"] = true,
		["segments_added"] = {
			{
				["elapsed"] = 9.74099999999999,
				["type"] = 0,
				["name"] = "Stonetusk Boar",
				["clock"] = "22:51:52",
			}, -- [1]
			{
				["elapsed"] = 11.3330000000005,
				["type"] = 0,
				["name"] = "Kobold Tunneler",
				["clock"] = "22:51:24",
			}, -- [2]
			{
				["elapsed"] = 10.9180000000015,
				["type"] = 0,
				["name"] = "Kobold Tunneler",
				["clock"] = "22:51:07",
			}, -- [3]
			{
				["elapsed"] = 16.1910000000007,
				["type"] = 0,
				["name"] = "Kobold Tunneler",
				["clock"] = "22:50:39",
			}, -- [4]
			{
				["elapsed"] = 10.8919999999998,
				["type"] = 0,
				["name"] = "Stonetusk Boar",
				["clock"] = "22:49:27",
			}, -- [5]
			{
				["elapsed"] = 8.53000000000066,
				["type"] = 0,
				["name"] = "Stonetusk Boar",
				["clock"] = "22:49:12",
			}, -- [6]
			{
				["elapsed"] = 13.3770000000004,
				["type"] = 0,
				["name"] = "Stonetusk Boar",
				["clock"] = "22:48:40",
			}, -- [7]
			{
				["elapsed"] = 10.1819999999989,
				["type"] = 0,
				["name"] = "Kobold Tunneler",
				["clock"] = "22:48:17",
			}, -- [8]
			{
				["elapsed"] = 13.7790000000005,
				["type"] = 0,
				["name"] = "Stonetusk Boar",
				["clock"] = "22:47:42",
			}, -- [9]
			{
				["elapsed"] = 12.8179999999993,
				["type"] = 0,
				["name"] = "Stonetusk Boar",
				["clock"] = "22:47:04",
			}, -- [10]
			{
				["elapsed"] = 2.94400000000314,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "22:42:08",
			}, -- [11]
			{
				["elapsed"] = 3.24700000000303,
				["type"] = 0,
				["name"] = "Kobold Vermin",
				["clock"] = "22:41:49",
			}, -- [12]
			{
				["elapsed"] = 8.4210000000021,
				["type"] = 0,
				["name"] = "Kobold Worker",
				["clock"] = "22:41:36",
			}, -- [13]
			{
				["elapsed"] = 6.09100000000035,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:41:13",
			}, -- [14]
			{
				["elapsed"] = 5.25299999999697,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:41:04",
			}, -- [15]
			{
				["elapsed"] = 6.6730000000025,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:40:54",
			}, -- [16]
			{
				["elapsed"] = 2.69400000000314,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:40:49",
			}, -- [17]
			{
				["elapsed"] = 6.84799999999814,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:40:36",
			}, -- [18]
			{
				["elapsed"] = 6.91500000000087,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:40:17",
			}, -- [19]
			{
				["elapsed"] = 16.7520000000004,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:39:50",
			}, -- [20]
			{
				["elapsed"] = 6.86299999999756,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:39:37",
			}, -- [21]
			{
				["elapsed"] = 6.71099999999569,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:39:29",
			}, -- [22]
			{
				["elapsed"] = 7.03100000000268,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:39:19",
			}, -- [23]
			{
				["elapsed"] = 4.31199999999808,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:39:08",
			}, -- [24]
			{
				["elapsed"] = 7.79699999999866,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "22:38:26",
			}, -- [25]
			{
				["elapsed"] = 7.7589999999982,
				["type"] = 0,
				["name"] = "Kobold Laborer",
				["clock"] = "21:59:28",
			}, -- [26]
			{
				["elapsed"] = 6.65999999999622,
				["type"] = 0,
				["name"] = "Kobold Worker",
				["clock"] = "21:59:16",
			}, -- [27]
			{
				["elapsed"] = 6.57400000000052,
				["type"] = 0,
				["name"] = "Kobold Worker",
				["clock"] = "21:59:03",
			}, -- [28]
			{
				["elapsed"] = 4.77799999999843,
				["type"] = 0,
				["name"] = "Timber Wolf",
				["clock"] = "21:58:40",
			}, -- [29]
			{
				["elapsed"] = 4.06300000000192,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "21:58:29",
			}, -- [30]
		},
		["data_fim"] = "22:52:02",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage_section"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage"] = {
			},
		},
		["start_time"] = 8002.247,
		["TimeData"] = {
		},
		["cleu_timeline"] = {
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 34.765380859375,
					["x"] = -778.53076171875,
					["w"] = 208.074157714844,
					["h"] = 98.7407608032227,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 3,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
	},
}
