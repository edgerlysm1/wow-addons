
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00376,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 60,
							},
							["colocacao"] = 1,
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 60.00376,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 60.00376,
							["classe"] = "WARRIOR",
							["serial"] = "Player-4406-015FB949",
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 24,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Mottled Boar"] = 37,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 37,
										["c_max"] = 24,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 24,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Mottled Boar"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["end_time"] = 1570937714,
							["last_dps"] = 8.23209768143703,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 7.00376,
							["start_time"] = 1570937706,
							["delay"] = 0,
							["last_event"] = 1570937712,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.004818,
							["damage_from"] = {
								["Tuukülo"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 7,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7.004818,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 7.004818,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-00002299A9",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorend"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937714,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 81.004818,
							["start_time"] = 1570937707,
							["delay"] = 0,
							["last_event"] = 1570937711,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 12,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 12,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 12,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12540.208,
				["tempo_start"] = 1570937706,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 15,
				["playing_solo"] = true,
				["totals"] = {
					66.991918, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					60, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:35:14",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.88500000000022,
				["CombatEndedAt"] = 12547.093,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 60.00376,
						}, -- [1]
					},
				},
				["end_time"] = 12547.093,
				["combat_id"] = 12,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:35:06",
				["start_time"] = 12539.804,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [1]
			{
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008859,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 47,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 47.008859,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937699,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Mottled Boar"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Mottled Boar"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 47.008859,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 7.57474363519159,
							["custom"] = 0,
							["last_event"] = 1570937697,
							["damage_taken"] = 5.008859,
							["start_time"] = 1570937692,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007746,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.007746,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.007746,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229AF7",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937699,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 47.007746,
							["start_time"] = 1570937692,
							["delay"] = 0,
							["last_event"] = 1570937698,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 11,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 11,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 11,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12525.147,
				["tempo_start"] = 1570937692,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 14,
				["playing_solo"] = true,
				["totals"] = {
					52, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					47, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:35:00",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 7.77899999999863,
				["CombatEndedAt"] = 12532.926,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 47.008859,
						}, -- [1]
					},
				},
				["end_time"] = 12532.926,
				["combat_id"] = 11,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:34:52",
				["start_time"] = 12525.147,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [2]
			{
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00492,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 46,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 46.00492,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937686,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Mottled Boar"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 11,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 46.00492,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 6.7023484848489,
							["custom"] = 0,
							["last_event"] = 1570937685,
							["damage_taken"] = 3.00492,
							["start_time"] = 1570937679,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005859,
							["damage_from"] = {
								["Tazziro"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 3,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 3.005859,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 3.005859,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229931",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Thorend"] = 3,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937686,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 105.005859,
							["start_time"] = 1570937680,
							["delay"] = 0,
							["last_event"] = 1570937684,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 10,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 10,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 10,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12513.117,
				["tempo_start"] = 1570937679,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 13,
				["playing_solo"] = true,
				["totals"] = {
					48.996632, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					46, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:34:46",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.46399999999994,
				["CombatEndedAt"] = 12519.581,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 46.00492,
						}, -- [1]
					},
				},
				["end_time"] = 12519.581,
				["combat_id"] = 10,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:34:39",
				["start_time"] = 12512.717,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [3]
			{
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003849,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 48,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 48.003849,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937672,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Mottled Boar"] = 25,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 25,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 25,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Mottled Boar"] = 23,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 23,
										["n_min"] = 23,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 23,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 48.003849,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 8.06516280242028,
							["custom"] = 0,
							["last_event"] = 1570937671,
							["damage_taken"] = 4.003849,
							["start_time"] = 1570937665,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001465,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 4,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 4.001465,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 4.001465,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229B3B",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937672,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 48.001465,
							["start_time"] = 1570937666,
							["delay"] = 0,
							["last_event"] = 1570937670,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 9,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 9,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 9,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12499.247,
				["tempo_start"] = 1570937665,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 12,
				["playing_solo"] = true,
				["totals"] = {
					52, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					48, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:34:32",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.58800000000156,
				["CombatEndedAt"] = 12505.835,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 48.003849,
						}, -- [1]
					},
				},
				["end_time"] = 12505.835,
				["combat_id"] = 9,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:34:26",
				["start_time"] = 12499.001,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [4]
			{
				{
					["combatId"] = 8,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001073,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 43,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 43.001073,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937637,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Mottled Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 43.001073,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 6.22482237984831,
							["custom"] = 0,
							["last_event"] = 1570937636,
							["damage_taken"] = 5.001073,
							["start_time"] = 1570937630,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008395,
							["damage_from"] = {
								["Gizmix"] = true,
								["Tuukülo"] = true,
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.008395,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.008395,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229B07",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937637,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 68.008395,
							["start_time"] = 1570937630,
							["delay"] = 0,
							["last_event"] = 1570937634,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 8,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 8,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 8,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 8,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12463.678,
				["tempo_start"] = 1570937630,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 11,
				["playing_solo"] = true,
				["totals"] = {
					47.987905, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					43, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:33:57",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.48700000000099,
				["CombatEndedAt"] = 12470.165,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 43.001073,
						}, -- [1]
					},
				},
				["end_time"] = 12470.165,
				["combat_id"] = 8,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:33:50",
				["start_time"] = 12463.257,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [5]
			{
				{
					["combatId"] = 7,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003149,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 42,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 42.003149,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937613,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Mottled Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Mottled Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 42.003149,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 6.63242523290834,
							["custom"] = 0,
							["last_event"] = 1570937611,
							["damage_taken"] = 5.003149,
							["start_time"] = 1570937605,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008435,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.008435,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.008435,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229ACD",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937613,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 42.008435,
							["start_time"] = 1570937605,
							["delay"] = 0,
							["last_event"] = 1570937611,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 7,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 7,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 7,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 7,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12438.976,
				["tempo_start"] = 1570937605,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 10,
				["playing_solo"] = true,
				["totals"] = {
					47, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					42, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:33:33",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 7.70099999999911,
				["CombatEndedAt"] = 12446.677,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 42.003149,
						}, -- [1]
					},
				},
				["end_time"] = 12446.677,
				["combat_id"] = 7,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:33:25",
				["start_time"] = 12438.816,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [6]
			{
				{
					["combatId"] = 6,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007519,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 42,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 42.007519,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937597,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Mottled Boar"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 42.007519,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 6.67315631453527,
							["custom"] = 0,
							["last_event"] = 1570937596,
							["damage_taken"] = 4.007519,
							["start_time"] = 1570937590,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002011,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 4,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 4.002011,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 4.002011,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000A29AE2",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Thorend"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937597,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 42.002011,
							["start_time"] = 1570937590,
							["delay"] = 0,
							["last_event"] = 1570937596,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 6,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 6,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 6,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 6,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12423.564,
				["tempo_start"] = 1570937590,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 9,
				["playing_solo"] = true,
				["totals"] = {
					46, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					42, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:33:17",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.88500000000022,
				["CombatEndedAt"] = 12430.449,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 42.007519,
						}, -- [1]
					},
				},
				["end_time"] = 12430.449,
				["combat_id"] = 6,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:33:10",
				["start_time"] = 12423.454,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [7]
			{
				{
					["combatId"] = 5,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005711,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 42,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 42.005711,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937582,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 11,
										["targets"] = {
											["Mottled Boar"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 11,
										["g_dmg"] = 9,
										["counter"] = 2,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 42.005711,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 7.0479380872472,
							["custom"] = 0,
							["last_event"] = 1570937581,
							["damage_taken"] = 4.005711,
							["start_time"] = 1570937576,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006338,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 4,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 4.006338,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 4.006338,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229ABE",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937582,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 42.006338,
							["start_time"] = 1570937576,
							["delay"] = 0,
							["last_event"] = 1570937580,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 5,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 5,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 5,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 5,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12409.397,
				["tempo_start"] = 1570937576,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 8,
				["playing_solo"] = true,
				["totals"] = {
					46, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					42, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:33:03",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.48999999999978,
				["CombatEndedAt"] = 12415.887,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 42.005711,
						}, -- [1]
					},
				},
				["end_time"] = 12415.887,
				["combat_id"] = 5,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:32:56",
				["start_time"] = 12409.187,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [8]
			{
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002067,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 43,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 43.002067,
							["friendlyfire"] = {
							},
							["end_time"] = 1570937560,
							["dps_started"] = false,
							["total"] = 43.002067,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Mottled Boar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 7.15746787616317,
							["custom"] = 0,
							["last_event"] = 1570937560,
							["damage_taken"] = 5.002067,
							["start_time"] = 1570937554,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001868,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["end_time"] = 1570937560,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.001868,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.001868,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000A29ABE",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 43.001868,
							["start_time"] = 1570937554,
							["delay"] = 0,
							["last_event"] = 1570937558,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 4,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 4,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 4,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12387.528,
				["tempo_start"] = 1570937554,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 7,
				["playing_solo"] = true,
				["totals"] = {
					48, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					43, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:32:41",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.46999999999935,
				["CombatEndedAt"] = 12393.998,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 43.002067,
						}, -- [1]
					},
				},
				["end_time"] = 12393.998,
				["combat_id"] = 4,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:32:34",
				["start_time"] = 12387.14,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [9]
			{
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00585,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 53,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 53.00585,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937525,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Mottled Boar"] = 11,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 11,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Mottled Boar"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 42,
										["n_min"] = 21,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 42,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 53.00585,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 8.33032374665882,
							["custom"] = 0,
							["last_event"] = 1570937524,
							["damage_taken"] = 3.00585,
							["start_time"] = 1570937517,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007994,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 3,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 3.007994,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 3.007994,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-00012293DB",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorend"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Thorend"] = 3,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937525,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 53.007994,
							["start_time"] = 1570937518,
							["delay"] = 0,
							["last_event"] = 1570937523,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 3,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 3,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 3,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 2,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12351.089,
				["tempo_start"] = 1570937517,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 6,
				["playing_solo"] = true,
				["totals"] = {
					56, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					53, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:32:05",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 7.70600000000013,
				["CombatEndedAt"] = 12358.795,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 53.00585,
						}, -- [1]
					},
				},
				["end_time"] = 12358.795,
				["combat_id"] = 3,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:31:58",
				["start_time"] = 12350.99,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [10]
			{
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002759,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 65,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 65.002759,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937511,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 11,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 11,
										["g_dmg"] = 11,
										["counter"] = 2,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 43,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Mottled Boar"] = 43,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 43,
										["c_max"] = 43,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 43,
									},
								},
							},
							["grupo"] = true,
							["total"] = 65.002759,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 10.6387494271707,
							["custom"] = 0,
							["last_event"] = 1570937511,
							["damage_taken"] = 4.002759,
							["start_time"] = 1570937505,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002822,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 4,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 4.002822,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 4.002822,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-00002293C1",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937511,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 65.002822,
							["start_time"] = 1570937505,
							["delay"] = 0,
							["last_event"] = 1570937509,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 2,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 2,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 2,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12338.437,
				["tempo_start"] = 1570937505,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 5,
				["playing_solo"] = true,
				["totals"] = {
					69, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					65, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:31:52",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.55999999999949,
				["CombatEndedAt"] = 12344.997,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 65.002759,
						}, -- [1]
					},
				},
				["end_time"] = 12344.997,
				["combat_id"] = 2,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:31:45",
				["start_time"] = 12338.237,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [11]
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004921,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 44,
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 44.004921,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1570937499,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorend",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 11,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Heroic Strike"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 22,
										["targets"] = {
											["Mottled Boar"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = "Heroic Strike",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 44.004921,
							["serial"] = "Player-4406-015FB949",
							["last_dps"] = 7.32926732178448,
							["custom"] = 0,
							["last_event"] = 1570937497,
							["damage_taken"] = 6.004921,
							["start_time"] = 1570937491,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005414,
							["damage_from"] = {
								["Thorend"] = true,
							},
							["targets"] = {
								["Thorend"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.005414,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.005414,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4390-1-17629-3098-0000229866",
							["nome"] = "Mottled Boar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Thorend"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1570937499,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 44.005414,
							["start_time"] = 1570937492,
							["delay"] = 0,
							["last_event"] = 1570937496,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorend",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Heroic Strike"] = 1,
							},
							["serial"] = "Player-4406-015FB949",
							["classe"] = "WARRIOR",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorend"] = true,
				},
				["CombatStartedAt"] = 12325.16,
				["tempo_start"] = 1570937491,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					50, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					44, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:31:39",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mottled Boar",
				["TotalElapsedCombatTime"] = 6.90099999999984,
				["CombatEndedAt"] = 12332.061,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorend"] = 44.004921,
						}, -- [1]
					},
				},
				["end_time"] = 12332.061,
				["combat_id"] = 1,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:31:31",
				["start_time"] = 12324.75,
				["TimeData"] = {
				},
				["frags"] = {
					["Mottled Boar"] = 1,
				},
			}, -- [12]
		},
	},
	["last_version"] = "v1.13.2.162",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1572233423,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorend-Herod",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 0.2,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "12",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 12,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 15,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.061012,
					["last_event"] = 0,
					["damage_from"] = {
						["Mottled Boar"] = true,
					},
					["targets"] = {
						["Mottled Boar"] = 575,
					},
					["pets"] = {
					},
					["classe"] = "WARRIOR",
					["raid_targets"] = {
					},
					["total_without_pet"] = 575.061012,
					["friendlyfire_total"] = 0,
					["dps_started"] = false,
					["end_time"] = 1570937499,
					["friendlyfire"] = {
					},
					["on_hold"] = false,
					["nome"] = "Thorend",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 24,
								["g_amt"] = 2,
								["n_max"] = 13,
								["targets"] = {
									["Mottled Boar"] = 269,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 225,
								["n_min"] = 0,
								["g_dmg"] = 20,
								["counter"] = 23,
								["total"] = 269,
								["c_max"] = 24,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 20,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							["Heroic Strike"] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 43,
								["g_amt"] = 0,
								["n_max"] = 23,
								["targets"] = {
									["Mottled Boar"] = 306,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 263,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 306,
								["c_max"] = 43,
								["id"] = "Heroic Strike",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 12,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 575.061012,
					["serial"] = "Player-4406-015FB949",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 55.061012,
					["start_time"] = 1570937411,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.071651,
					["fight_component"] = true,
					["damage_from"] = {
						["Tazziro"] = true,
						["Thorend"] = true,
					},
					["targets"] = {
						["Thorend"] = 55,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 55.071651,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1570937499,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Mottled Boar",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorend"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 1,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Thorend"] = 55,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 55,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 39,
								["total"] = 55,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 39,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["total"] = 55.071651,
					["serial"] = "Creature-0-4390-1-17629-3098-0000229866",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 591.071651,
					["start_time"] = 1570937416,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["tipo"] = 4,
					["nome"] = "Thorend",
					["grupo"] = true,
					["pets"] = {
					},
					["classe"] = "WARRIOR",
					["spell_cast"] = {
						["Heroic Strike"] = 13,
					},
					["serial"] = "Player-4406-015FB949",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1570937491,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			646.12097, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "23:31:31",
		["end_time"] = 12547.093,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 7.28900000000067,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:35:06",
			}, -- [1]
			{
				["elapsed"] = 7.77899999999863,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:34:52",
			}, -- [2]
			{
				["elapsed"] = 6.86399999999958,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:34:39",
			}, -- [3]
			{
				["elapsed"] = 6.83400000000074,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:34:26",
			}, -- [4]
			{
				["elapsed"] = 6.90800000000127,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:33:50",
			}, -- [5]
			{
				["elapsed"] = 7.86099999999897,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:33:25",
			}, -- [6]
			{
				["elapsed"] = 6.9950000000008,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:33:10",
			}, -- [7]
			{
				["elapsed"] = 6.70000000000073,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:32:56",
			}, -- [8]
			{
				["elapsed"] = 6.85800000000018,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:32:34",
			}, -- [9]
			{
				["elapsed"] = 7.80500000000029,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:31:58",
			}, -- [10]
			{
				["elapsed"] = 6.7599999999984,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:31:45",
			}, -- [11]
			{
				["elapsed"] = 7.3109999999997,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "23:31:31",
			}, -- [12]
		},
		["totals_grupo"] = {
			575.054437, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "23:35:14",
		["overall_enemy_name"] = "Mottled Boar",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 12461.129,
		["TimeData"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 158.419555664063,
					["x"] = -593.382919311523,
					["w"] = 310.000061035156,
					["h"] = 158.000061035156,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
	},
}
