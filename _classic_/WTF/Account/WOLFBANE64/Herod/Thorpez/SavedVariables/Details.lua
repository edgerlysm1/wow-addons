
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 36,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004068,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Battleboar"] = true,
							},
							["targets"] = {
								["Battleboar"] = 90,
							},
							["colocacao"] = 1,
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 90.004068,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 90.004068,
							["classe"] = "SHAMAN",
							["serial"] = "Player-4406-015FBB7A",
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Battleboar"] = 75,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Battleboar"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["end_time"] = 1571459103,
							["last_dps"] = 8.99680807676856,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 7.004068,
							["start_time"] = 1571459093,
							["delay"] = 0,
							["last_event"] = 1571459101,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.005415,
							["monster"] = true,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 7,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7.005415,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 7.005415,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2966-00002A8E8A",
							["nome"] = "Battleboar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorpez"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Thorpez"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571459103,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 90.005415,
							["start_time"] = 1571459100,
							["delay"] = 0,
							["last_event"] = 1571459102,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 36,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 36,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 36,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 36,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1571459093,
				["enemy"] = "Battleboar",
				["combat_counter"] = 40,
				["playing_solo"] = true,
				["totals"] = {
					96.988371, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 48860.072,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "00:24:54",
				["end_time"] = 48860.072,
				["totals_grupo"] = {
					90, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 36,
				["TotalElapsedCombatTime"] = 48860.072,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 90.004068,
						}, -- [1]
					},
				},
				["frags"] = {
					["Battleboar"] = 1,
				},
				["data_fim"] = "00:25:04",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 48850.068,
				["contra"] = "Battleboar",
				["TimeData"] = {
				},
			}, -- [1]
			{
				{
					["combatId"] = 35,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008974,
							["damage_from"] = {
								["Battleboar"] = true,
							},
							["targets"] = {
								["Battleboar"] = 77,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 77.008974,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571459078,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Battleboar"] = 77,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 77,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 77,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 77.008974,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 12.2430801271843,
							["custom"] = 0,
							["last_event"] = 1571459077,
							["damage_taken"] = 5.008974,
							["start_time"] = 1571459071,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.004934,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 5,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.004934,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1571459078,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Battleboar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 5.004934,
							["serial"] = "Creature-0-4392-1-10272-2966-00002A8E99",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1571459075,
							["damage_taken"] = 77.004934,
							["start_time"] = 1571459073,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 35,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 35,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 35,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 35,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 48848.808,
				["tempo_start"] = 1571459071,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 39,
				["playing_solo"] = true,
				["totals"] = {
					82, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					77, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "00:24:39",
				["cleu_timeline"] = {
				},
				["enemy"] = "Battleboar",
				["TotalElapsedCombatTime"] = 48835.438,
				["CombatEndedAt"] = 48835.438,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 77.008974,
						}, -- [1]
					},
				},
				["end_time"] = 48835.438,
				["combat_id"] = 35,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "00:24:32",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Battleboar"] = 1,
				},
				["start_time"] = 48827.733,
				["TimeData"] = {
				},
				["contra"] = "Battleboar",
			}, -- [2]
			{
				{
					["combatId"] = 34,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005839,
							["damage_from"] = {
								["Battleboar"] = true,
							},
							["targets"] = {
								["Battleboar"] = 90,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 90.005839,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571459053,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Battleboar"] = 75,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 1,
										["n_max"] = 8,
										["targets"] = {
											["Battleboar"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 8,
										["n_min"] = 8,
										["g_dmg"] = 7,
										["counter"] = 2,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 90.005839,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 9.46135172921364,
							["custom"] = 0,
							["last_event"] = 1571459051,
							["damage_taken"] = 10.005839,
							["start_time"] = 1571459042,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.00366,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 10,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.00366,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1571459053,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Battleboar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Thorpez"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 10.00366,
							["serial"] = "Creature-0-4392-1-10272-2966-00002A8C95",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1571459050,
							["damage_taken"] = 90.00366,
							["start_time"] = 1571459046,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 34,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 34,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 34,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 34,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 48826.923,
				["tempo_start"] = 1571459042,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 38,
				["playing_solo"] = true,
				["totals"] = {
					100, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					90, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "00:24:14",
				["cleu_timeline"] = {
				},
				["enemy"] = "Battleboar",
				["TotalElapsedCombatTime"] = 48810.304,
				["CombatEndedAt"] = 48810.304,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 90.005839,
						}, -- [1]
					},
				},
				["end_time"] = 48810.304,
				["combat_id"] = 34,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "00:24:03",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Battleboar"] = 1,
				},
				["start_time"] = 48798.91,
				["TimeData"] = {
				},
				["contra"] = "Battleboar",
			}, -- [3]
			{
				{
					["combatId"] = 33,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.008357,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 11,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 11.008357,
							["monster"] = true,
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1571459031,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Battleboar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 11,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["total"] = 11.008357,
							["serial"] = "Creature-0-4392-1-10272-2966-00002A4703",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1571459029,
							["damage_taken"] = 75.008357,
							["start_time"] = 1571459022,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005285,
							["damage_from"] = {
								["Battleboar"] = true,
							},
							["targets"] = {
								["Battleboar"] = 75,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 75.005285,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571459031,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Battleboar"] = 75,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 15,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 75.005285,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 9.71193642366275,
							["custom"] = 0,
							["last_event"] = 1571459030,
							["damage_taken"] = 11.005285,
							["start_time"] = 1571459023,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 33,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 33,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 33,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 5,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 33,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 48797.76,
				["tempo_start"] = 1571459022,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 37,
				["playing_solo"] = true,
				["totals"] = {
					86, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					75, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "00:23:52",
				["cleu_timeline"] = {
				},
				["enemy"] = "Battleboar",
				["TotalElapsedCombatTime"] = 48788.413,
				["CombatEndedAt"] = 48788.413,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 75.005285,
						}, -- [1]
					},
				},
				["end_time"] = 48788.413,
				["combat_id"] = 33,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "00:23:43",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Battleboar"] = 1,
				},
				["start_time"] = 48779.496,
				["TimeData"] = {
				},
				["contra"] = "Battleboar",
			}, -- [4]
			{
				{
					["combatId"] = 32,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005353,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 74,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 74.005353,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571457831,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 74,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 74,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 74,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 74.005353,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 9.25645440901102,
							["custom"] = 0,
							["last_event"] = 1571457830,
							["damage_taken"] = 6.005353,
							["start_time"] = 1571457823,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00353,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.00353,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.00353,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A83F1",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571457831,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 74.00353,
							["start_time"] = 1571457826,
							["delay"] = 0,
							["last_event"] = 1571457828,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 32,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 32,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 32,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 32,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 48777.065,
				["tempo_start"] = 1571457823,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 36,
				["playing_solo"] = true,
				["totals"] = {
					79.971558, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					74, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "00:03:52",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47588.024,
				["CombatEndedAt"] = 47588.024,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 74.005353,
						}, -- [1]
					},
				},
				["end_time"] = 47588.024,
				["combat_id"] = 32,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "00:03:44",
				["start_time"] = 47580.029,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [5]
			{
				{
					["combatId"] = 31,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006165,
							["damage_from"] = {
								["Environment (Falling)"] = true,
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 74,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 74.006165,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571457618,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Mountain Cougar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 2,
										["n_max"] = 8,
										["targets"] = {
											["Mountain Cougar"] = 44,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 6,
										["g_dmg"] = 14,
										["counter"] = 6,
										["total"] = 44,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 74.006165,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 5.06371296613139,
							["custom"] = 0,
							["last_event"] = 1571457617,
							["damage_taken"] = 17.006165,
							["start_time"] = 1571457603,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008668,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 15,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.008668,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 15.008668,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A4C7E",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorpez"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571457618,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 74.008668,
							["start_time"] = 1571457605,
							["delay"] = 0,
							["last_event"] = 1571457616,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 31,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 31,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 31,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 1,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 31,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47579.119,
				["tempo_start"] = 1571457603,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 35,
				["playing_solo"] = true,
				["totals"] = {
					88.998365, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					74, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "00:00:19",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47374.67,
				["CombatEndedAt"] = 47374.67,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 74.006165,
						}, -- [1]
					},
				},
				["end_time"] = 47374.67,
				["combat_id"] = 31,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
					["Thorpez"] = {
						{
							true, -- [1]
							"Falling", -- [2]
							2, -- [3]
							1571457816.193, -- [4]
							117, -- [5]
							"Environment (Falling)", -- [6]
							nil, -- [7]
							3, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "00:00:04",
				["start_time"] = 47360.055,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [6]
			{
				{
					["combatId"] = 30,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 73,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 73.006,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571457595,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 57,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 57,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 57,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Mountain Cougar"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 73.006,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 8.08124861633859,
							["custom"] = 0,
							["last_event"] = 1571457594,
							["damage_taken"] = 6.006,
							["start_time"] = 1571457585,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003247,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 6,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.003247,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.003247,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A85BC",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorpez"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["DODGE"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571457595,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 73.003247,
							["start_time"] = 1571457589,
							["delay"] = 0,
							["last_event"] = 1571457593,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 30,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 30,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 30,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 3,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 30,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47358.815,
				["tempo_start"] = 1571457585,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 34,
				["playing_solo"] = true,
				["totals"] = {
					79, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					73, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:59:56",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47352.356,
				["CombatEndedAt"] = 47352.356,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 73.006,
						}, -- [1]
					},
				},
				["end_time"] = 47352.356,
				["combat_id"] = 30,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:59:46",
				["start_time"] = 47342.192,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [7]
			{
				{
					["combatId"] = 29,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002325,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 75,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 75.002325,
							["friendlyfire"] = {
							},
							["end_time"] = 1571457572,
							["dps_started"] = false,
							["total"] = 75.002325,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 75,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 11.4507366412163,
							["custom"] = 0,
							["last_event"] = 1571457571,
							["damage_taken"] = 5.002325,
							["start_time"] = 1571457564,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008202,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 5,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["end_time"] = 1571457572,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5.008202,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 5.008202,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A4CE1",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 5,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 5,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 75.008202,
							["start_time"] = 1571457567,
							["delay"] = 0,
							["last_event"] = 1571457569,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 29,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 29,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 29,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 29,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47341.392,
				["tempo_start"] = 1571457564,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 33,
				["playing_solo"] = true,
				["totals"] = {
					80, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					75, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:59:33",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47329.248,
				["CombatEndedAt"] = 47329.248,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 75.002325,
						}, -- [1]
					},
				},
				["end_time"] = 47329.248,
				["combat_id"] = 29,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:59:25",
				["start_time"] = 47321.273,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [8]
			{
				{
					["combatId"] = 28,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008098,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 78,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 78.008098,
							["friendlyfire"] = {
							},
							["end_time"] = 1571457543,
							["dps_started"] = false,
							["total"] = 78.008098,
							["colocacao"] = 1,
							["friendlyfire_total"] = 0,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 57,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 57,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 57,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Mountain Cougar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 5.78523420350184,
							["custom"] = 0,
							["last_event"] = 1571457541,
							["damage_taken"] = 15.008098,
							["start_time"] = 1571457528,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008171,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 15,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["end_time"] = 1571457543,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.008171,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 15.008171,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A8563",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 78.008171,
							["start_time"] = 1571457531,
							["delay"] = 0,
							["last_event"] = 1571457539,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 28,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 28,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 28,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 3,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 28,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47320.283,
				["tempo_start"] = 1571457528,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 32,
				["playing_solo"] = true,
				["totals"] = {
					93, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					78, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:59:04",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47299.635,
				["CombatEndedAt"] = 47299.635,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 78.008098,
						}, -- [1]
					},
				},
				["end_time"] = 47299.635,
				["combat_id"] = 28,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:58:48",
				["start_time"] = 47284.539,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [9]
			{
				{
					["combatId"] = 27,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006824,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 20,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 20.006824,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 20.006824,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A8554",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571457522,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 72.006824,
							["start_time"] = 1571457507,
							["delay"] = 0,
							["last_event"] = 1571457519,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005151,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 72,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 72.005151,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571457522,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 59,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 59,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 59,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Mountain Cougar"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 13,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 5,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 72.005151,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 5.34559398663651,
							["custom"] = 0,
							["last_event"] = 1571457520,
							["damage_taken"] = 20.005151,
							["start_time"] = 1571457511,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 27,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 27,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 27,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 27,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47283.429,
				["tempo_start"] = 1571457507,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 31,
				["playing_solo"] = true,
				["totals"] = {
					92, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					72, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:58:43",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47278.947,
				["CombatEndedAt"] = 47278.947,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 72.005151,
						}, -- [1]
					},
				},
				["end_time"] = 47278.947,
				["combat_id"] = 27,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:58:28",
				["start_time"] = 47263.954,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [10]
			{
				{
					["combatId"] = 26,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008056,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 74,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 74.008056,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571457497,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Mountain Cougar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 74,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 74,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 74,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 74.008056,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 12.092819607838,
							["custom"] = 0,
							["last_event"] = 1571457507,
							["damage_taken"] = 3.008056,
							["start_time"] = 1571457490,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001357,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 3,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 3.001357,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 3.001357,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A7E80",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 3,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571457497,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 74.001357,
							["start_time"] = 1571457496,
							["delay"] = 0,
							["last_event"] = 1571457496,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 26,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 26,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 26,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 3,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 26,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47263.954,
				["tempo_start"] = 1571457490,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 30,
				["playing_solo"] = true,
				["totals"] = {
					77, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					74, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:58:18",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47253.813,
				["CombatEndedAt"] = 47253.813,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 74.008056,
						}, -- [1]
					},
				},
				["end_time"] = 47253.813,
				["combat_id"] = 26,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:58:11",
				["start_time"] = 47247.303,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [11]
			{
				{
					["combatId"] = 25,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00466,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 77,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 77.00466,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571457474,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[0] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Mountain Cougar"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["RESIST"] = 1,
										["id"] = 0,
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 56,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 56,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 56,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Mountain Cougar"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 77.00466,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 6.21255828963452,
							["custom"] = 0,
							["last_event"] = 1571457472,
							["damage_taken"] = 15.00466,
							["start_time"] = 1571457460,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002865,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 15,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 15.002865,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 15.002865,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A871A",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571457474,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 77.002865,
							["start_time"] = 1571457463,
							["delay"] = 0,
							["last_event"] = 1571457471,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 25,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 25,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 25,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 25,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47245.663,
				["tempo_start"] = 1571457460,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 29,
				["playing_solo"] = true,
				["totals"] = {
					92, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					77, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:57:55",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 47230.684,
				["CombatEndedAt"] = 47230.684,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 77.00466,
						}, -- [1]
					},
				},
				["end_time"] = 47230.684,
				["combat_id"] = 25,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:57:41",
				["start_time"] = 47217.029,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [12]
			{
				{
					["combatId"] = 24,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001927,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 74,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 74.001927,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571456613,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 24,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mountain Cougar"] = 67,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 43,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 67,
										["c_max"] = 24,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 24,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Mountain Cougar"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 74.001927,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 8.31949713322391,
							["custom"] = 0,
							["last_event"] = 1571456611,
							["damage_taken"] = 9.001927,
							["start_time"] = 1571456604,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001983,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 9,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.001983,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 9.001983,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A8354",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 9,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571456613,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 74.001983,
							["start_time"] = 1571456606,
							["delay"] = 0,
							["last_event"] = 1571456610,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 24,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 24,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 24,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 3,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 24,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 47216.079,
				["tempo_start"] = 1571456604,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 28,
				["playing_solo"] = true,
				["totals"] = {
					83, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					74, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:43:34",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 46370.195,
				["CombatEndedAt"] = 46370.195,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 74.001927,
						}, -- [1]
					},
				},
				["end_time"] = 46370.195,
				["combat_id"] = 24,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:43:25",
				["start_time"] = 46361.3,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [13]
			{
				{
					["combatId"] = 23,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00389,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 84,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 84.00389,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571455647,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 22,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mountain Cougar"] = 77,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 55,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 77,
										["c_max"] = 22,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 22,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Mountain Cougar"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 84.00389,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 3.59621088231567,
							["custom"] = 0,
							["last_event"] = 1571455646,
							["damage_taken"] = 25.00389,
							["start_time"] = 1571455638,
							["delay"] = 1571455627,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007175,
							["damage_from"] = {
								["Thorpez"] = true,
								["Milkrage"] = true,
							},
							["targets"] = {
								["Thorpez"] = 25,
								["Milkrage"] = 44,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 69.007175,
							["end_time"] = 1571456604,
							["dps_started"] = false,
							["total"] = 69.007175,
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorpez"] = 0,
											["Milkrage"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 2,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 25,
											["Milkrage"] = 44,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 69,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 25,
										["total"] = 69,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 1,
										["n_amt"] = 25,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["on_hold"] = false,
							["serial"] = "Creature-0-4392-1-10272-2961-00002A5F71",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1571456218,
							["damage_taken"] = 530.007175,
							["start_time"] = 1571456116,
							["delay"] = 1571456218,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 23,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 23,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 23,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 23,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 46360.9,
				["tempo_start"] = 1571455623,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 27,
				["playing_solo"] = true,
				["totals"] = {
					152.996435, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					84, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:27:28",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 45403.78,
				["CombatEndedAt"] = 45403.78,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 84.00389,
						}, -- [1]
					},
				},
				["end_time"] = 45403.78,
				["combat_id"] = 23,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:27:04",
				["start_time"] = 45380.421,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [14]
			{
				{
					["combatId"] = 22,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002123,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 73,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 73.002123,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571455613,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 14,
										["targets"] = {
											["Mountain Cougar"] = 41,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 41,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 41,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Mountain Cougar"] = 32,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 32,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 32,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 73.002123,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 6.32874928478352,
							["custom"] = 0,
							["last_event"] = 1571455612,
							["damage_taken"] = 14.002123,
							["start_time"] = 1571455601,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006211,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 14,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 14.006211,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 14.006211,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A7ED4",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 14,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 14,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571455613,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 73.006211,
							["start_time"] = 1571455604,
							["delay"] = 0,
							["last_event"] = 1571455612,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 22,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 22,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 22,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 2,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 22,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 45379.421,
				["tempo_start"] = 1571455601,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 26,
				["playing_solo"] = true,
				["totals"] = {
					87, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					73, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:26:54",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 45370.513,
				["CombatEndedAt"] = 45370.513,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 73.002123,
						}, -- [1]
					},
				},
				["end_time"] = 45370.513,
				["combat_id"] = 22,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:26:42",
				["start_time"] = 45357.888,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [15]
			{
				{
					["combatId"] = 21,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00246,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 75,
							},
							["pets"] = {
							},
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 75.00246,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1571455591,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorpez",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 75,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 75.00246,
							["serial"] = "Player-4406-015FBB7A",
							["last_dps"] = 11.1395306698349,
							["custom"] = 0,
							["last_event"] = 1571455590,
							["damage_taken"] = 9.00246,
							["start_time"] = 1571455583,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007804,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 9,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.007804,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 9.007804,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4392-1-10272-2961-00002A4BF1",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 9,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1571455591,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 75.007804,
							["start_time"] = 1571455585,
							["delay"] = 0,
							["last_event"] = 1571455589,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 21,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 21,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 21,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Lightning Bolt"] = 4,
							},
							["serial"] = "Player-4406-015FBB7A",
							["classe"] = "SHAMAN",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 21,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 45356.718,
				["tempo_start"] = 1571455583,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 25,
				["playing_solo"] = true,
				["totals"] = {
					84, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					75, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "23:26:32",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 45347.781,
				["CombatEndedAt"] = 45347.781,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpez"] = 75.00246,
						}, -- [1]
					},
				},
				["end_time"] = 45347.781,
				["combat_id"] = 21,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "23:26:24",
				["start_time"] = 45340.088,
				["TimeData"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
			}, -- [16]
			{
				{
					["tipo"] = 2,
					["combatId"] = 20,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001355,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 75,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 75.001355,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1570938795,
							["on_hold"] = false,
							["damage_taken"] = 11.001355,
							["nome"] = "Thorpez",
							["spells"] = {
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mountain Cougar"] = 42,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 42,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 42,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 8,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 2,
										["n_max"] = 8,
										["targets"] = {
											["Mountain Cougar"] = 33,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 7,
										["g_dmg"] = 11,
										["counter"] = 5,
										["total"] = 33,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 5.075546795696,
							["colocacao"] = 1,
							["last_event"] = 1570938793,
							["friendlyfire"] = {
							},
							["start_time"] = 1570938780,
							["serial"] = "Player-4406-015FBB7A",
							["total"] = 75.001355,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003855,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 11,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 11.003855,
							["last_event"] = 1570938791,
							["fight_component"] = true,
							["total"] = 11.003855,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorpez"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 11,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 75.003855,
							["end_time"] = 1570938795,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1570938783,
							["serial"] = "Creature-0-4391-1-18397-2961-0000228B48",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 20,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 20,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 20,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								["Lightning Bolt"] = 2,
							},
							["tipo"] = 4,
							["classe"] = "SHAMAN",
							["serial"] = "Player-4406-015FBB7A",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 20,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 45339.208,
				["tempo_start"] = 1570938780,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					86, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					75, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "23:53:15",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 13628.061,
				["CombatEndedAt"] = 13628.061,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "23:53:00",
				["end_time"] = 13628.061,
				["combat_id"] = 20,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorpez"] = 75.001355,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
				["overall_added"] = true,
				["combat_counter"] = 23,
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 13613.284,
				["TimeData"] = {
				},
				["spells_cast_timeline"] = {
				},
			}, -- [17]
			{
				{
					["tipo"] = 2,
					["combatId"] = 19,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004812,
							["damage_from"] = {
								["Mountain Cougar"] = true,
							},
							["targets"] = {
								["Mountain Cougar"] = 75,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["on_hold"] = false,
							["classe"] = "SHAMAN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 75.004812,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1570938770,
							["total"] = 75.004812,
							["damage_taken"] = 12.004812,
							["nome"] = "Thorpez",
							["spells"] = {
								["_ActorTable"] = {
									["Lightning Bolt"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mountain Cougar"] = 60,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 60,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 60,
										["c_max"] = 0,
										["id"] = "Lightning Bolt",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["spellschool"] = 8,
									},
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Mountain Cougar"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 7,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["last_dps"] = 7.64341302354012,
							["colocacao"] = 1,
							["last_event"] = 1570938769,
							["friendlyfire"] = {
							},
							["start_time"] = 1570938759,
							["serial"] = "Player-4406-015FBB7A",
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007948,
							["damage_from"] = {
								["Thorpez"] = true,
							},
							["targets"] = {
								["Thorpez"] = 12,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 12.007948,
							["last_event"] = 1570938769,
							["fight_component"] = true,
							["total"] = 12.007948,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Mountain Cougar",
							["spells"] = {
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorpez"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 75.007948,
							["end_time"] = 1570938770,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1570938763,
							["serial"] = "Creature-0-4391-1-18397-2961-0000228C03",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 19,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 19,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 19,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorpez",
							["grupo"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								["Lightning Bolt"] = 3,
							},
							["tipo"] = 4,
							["classe"] = "SHAMAN",
							["serial"] = "Player-4406-015FBB7A",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 19,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpez"] = true,
				},
				["CombatStartedAt"] = 13612.314,
				["tempo_start"] = 1570938759,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					87, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "23:52:50",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Mountain Cougar",
				["TotalElapsedCombatTime"] = 13603.806,
				["CombatEndedAt"] = 13603.806,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "23:52:39",
				["end_time"] = 13603.806,
				["combat_id"] = 19,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Mountain Cougar"] = 1,
				},
				["combat_counter"] = 22,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					75, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 13592.816,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorpez"] = 75.004812,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [18]
		},
	},
	["last_version"] = "v1.13.2.162",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1572233815,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorpez-Herod",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 0.2,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
	},
	["last_day"] = "19",
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["character_data"] = {
		["logons"] = 2,
	},
	["combat_id"] = 36,
	["savedStyles"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 157.234436035156,
					["x"] = -603.518524169922,
					["w"] = 310.000061035156,
					["h"] = 158.000061035156,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["force_font_outline"] = "",
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.202643,
					["damage_from"] = {
						["Battleboar"] = true,
						["Plainstrider"] = true,
						["Mountain Cougar"] = true,
					},
					["targets"] = {
						["Battleboar"] = 332,
						["Plainstrider"] = 848,
						["Mountain Cougar"] = 1204,
					},
					["pets"] = {
					},
					["damage_taken"] = 247.202643,
					["classe"] = "SHAMAN",
					["raid_targets"] = {
					},
					["total_without_pet"] = 2384.202643,
					["last_dps"] = 0,
					["dps_started"] = false,
					["end_time"] = 1570938010,
					["friendlyfire_total"] = 0,
					["total"] = 2384.202643,
					["nome"] = "Thorpez",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Mountain Cougar"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 1,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[0] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Plainstrider"] = 0,
									["Mountain Cougar"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 0,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["n_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["RESIST"] = 2,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 1,
								["c_dmg"] = 0,
								["g_amt"] = 10,
								["n_max"] = 8,
								["targets"] = {
									["Battleboar"] = 30,
									["Plainstrider"] = 65,
									["Mountain Cougar"] = 209,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 240,
								["n_min"] = 0,
								["g_dmg"] = 64,
								["counter"] = 44,
								["total"] = 304,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 5,
								["n_amt"] = 34,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							["Lightning Bolt"] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 111,
								["g_amt"] = 0,
								["n_max"] = 17,
								["targets"] = {
									["Battleboar"] = 302,
									["Plainstrider"] = 783,
									["Mountain Cougar"] = 995,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1969,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 141,
								["total"] = 2080,
								["c_max"] = 24,
								["id"] = "Lightning Bolt",
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 136,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["grupo"] = true,
					["delay"] = 0,
					["on_hold"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1570937699,
					["serial"] = "Player-4406-015FBB7A",
					["tipo"] = 1,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.09385,
					["damage_from"] = {
						["Thorpez"] = true,
					},
					["targets"] = {
						["Thorpez"] = 31,
						["Druiderp"] = 0,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 31.09385,
					["damage_taken"] = 848.09385,
					["fight_component"] = true,
					["end_time"] = 1570938010,
					["delay"] = 0,
					["classe"] = "UNKNOW",
					["nome"] = "Plainstrider",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorpez"] = 0,
									["Druiderp"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["DODGE"] = 2,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2,
								["targets"] = {
									["Thorpez"] = 31,
									["Druiderp"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 31,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 19,
								["total"] = 31,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 19,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["total"] = 31.09385,
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1570937968,
					["serial"] = "Creature-0-4391-1-18397-2955-00002297F3",
					["dps_started"] = false,
				}, -- [2]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.092711,
					["damage_from"] = {
						["Thorpez"] = true,
					},
					["targets"] = {
						["Thorpez"] = 183,
					},
					["fight_component"] = true,
					["pets"] = {
					},
					["damage_taken"] = 1204.092711,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 183.092711,
					["tipo"] = 1,
					["dps_started"] = false,
					["total"] = 183.092711,
					["classe"] = "UNKNOW",
					["last_dps"] = 0,
					["nome"] = "Mountain Cougar",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorpez"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 0,
								["c_max"] = 0,
								["DODGE"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["MISS"] = 3,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Thorpez"] = 183,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 183,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 64,
								["total"] = 183,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 64,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["end_time"] = 1570938715,
					["delay"] = 0,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1570938567,
					["serial"] = "Creature-0-4391-1-18397-2961-0000228B19",
					["on_hold"] = false,
				}, -- [3]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.023744,
					["on_hold"] = false,
					["damage_from"] = {
						["Thorpez"] = true,
					},
					["targets"] = {
						["Thorpez"] = 33,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 33.023744,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1571459032,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Battleboar",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Thorpez"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 1,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 4,
								["targets"] = {
									["Thorpez"] = 33,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 33,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 11,
								["total"] = 33,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 11,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["monster"] = true,
					["total"] = 33.023744,
					["serial"] = "Creature-0-4392-1-10272-2966-00002A4703",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 332.023744,
					["start_time"] = 1571459005,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [4]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["nome"] = "Thorpez",
					["grupo"] = true,
					["spell_cast"] = {
						["Lightning Bolt"] = 104,
					},
					["pets"] = {
					},
					["tipo"] = 4,
					["last_event"] = 0,
					["serial"] = "Player-4406-015FBB7A",
					["classe"] = "SHAMAN",
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1570938006,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["cleu_timeline"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			2631.387523, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "23:40:06",
		["end_time"] = 48860.072,
		["totals_grupo"] = {
			2384.195988, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["segments_added"] = {
			{
				["elapsed"] = 10.0040000000008,
				["type"] = 0,
				["name"] = "Battleboar",
				["clock"] = "00:24:54",
			}, -- [1]
			{
				["elapsed"] = 7.70500000000175,
				["type"] = 0,
				["name"] = "Battleboar",
				["clock"] = "00:24:32",
			}, -- [2]
			{
				["elapsed"] = 11.3940000000002,
				["type"] = 0,
				["name"] = "Battleboar",
				["clock"] = "00:24:03",
			}, -- [3]
			{
				["elapsed"] = 8.91700000000128,
				["type"] = 0,
				["name"] = "Battleboar",
				["clock"] = "00:23:43",
			}, -- [4]
			{
				["elapsed"] = 7.99499999999534,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "00:03:44",
			}, -- [5]
			{
				["elapsed"] = 14.614999999998,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "00:00:04",
			}, -- [6]
			{
				["elapsed"] = 10.163999999997,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:59:46",
			}, -- [7]
			{
				["elapsed"] = 7.97499999999855,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:59:25",
			}, -- [8]
			{
				["elapsed"] = 15.0959999999977,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:58:48",
			}, -- [9]
			{
				["elapsed"] = 14.9930000000022,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:58:28",
			}, -- [10]
			{
				["elapsed"] = 6.51000000000204,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:58:11",
			}, -- [11]
			{
				["elapsed"] = 13.6549999999988,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:57:41",
			}, -- [12]
			{
				["elapsed"] = 8.8949999999968,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:43:25",
			}, -- [13]
			{
				["elapsed"] = 23.3589999999967,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:27:04",
			}, -- [14]
			{
				["elapsed"] = 12.625,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:26:42",
			}, -- [15]
			{
				["elapsed"] = 7.6929999999993,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:26:24",
			}, -- [16]
			{
				["elapsed"] = 14.777,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:53:00",
			}, -- [17]
			{
				["elapsed"] = 10.9899999999998,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:52:39",
			}, -- [18]
			{
				["elapsed"] = 10.2259999999988,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:52:18",
			}, -- [19]
			{
				["elapsed"] = 11.4970000000012,
				["type"] = 0,
				["name"] = "Mountain Cougar",
				["clock"] = "23:51:43",
			}, -- [20]
			{
				["elapsed"] = 5.13900000000103,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:50:42",
			}, -- [21]
			{
				["elapsed"] = 10.9419999999991,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:50:26",
			}, -- [22]
			{
				["elapsed"] = 3.51499999999942,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:50:10",
			}, -- [23]
			{
				["elapsed"] = 4.00199999999859,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:49:56",
			}, -- [24]
			{
				["elapsed"] = 4.99800000000141,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:49:39",
			}, -- [25]
			{
				["elapsed"] = 5.39999999999964,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:49:14",
			}, -- [26]
			{
				["elapsed"] = 8.78999999999905,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:43:29",
			}, -- [27]
			{
				["elapsed"] = 4.04099999999926,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:43:11",
			}, -- [28]
			{
				["elapsed"] = 6.83300000000054,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:42:56",
			}, -- [29]
			{
				["elapsed"] = 4.23999999999978,
				["type"] = 0,
				["name"] = "Plainstrider",
				["clock"] = "23:42:26",
			}, -- [30]
		},
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = "00:25:04",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["frags"] = {
		},
		["start_time"] = 48533.144,
		["TimeData"] = {
		},
		["cleu_events"] = {
			["n"] = 1,
		},
	},
	["combat_counter"] = 40,
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["last_realversion"] = 140,
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
	},
}
