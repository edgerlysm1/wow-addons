
_detalhes_global = {
	["npcid_pool"] = {
		[12236] = "Lord Vyletongue",
		[4097] = "Galak Stormer",
		[1547] = "Decrepit Darkhound",
		[1549] = "Ravenous Darkhound",
		[8318] = "Atal'ai Slave",
		[1557] = "Elder Mistvale Gorilla",
		[1561] = "Bloodsail Raider",
		[1563] = "Bloodsail Swashbuckler",
		[1565] = "Bloodsail Sea Dog",
		[4249] = "Pesterhide Snarler",
		[8510] = "Atal'ai Totem <Zolo>",
		[4281] = "Scarlet Scout",
		[4289] = "Scarlet Evoker",
		[4297] = "Scarlet Conjuror",
		[4361] = "Mirefin Muckdweller",
		[4377] = "Darkmist Lurker",
		[3225] = "Nokzord",
		[4417] = "Defias Taskmaster",
		[3241] = "Yllek",
		[3245] = "Ornery Plainstrider",
		[8894] = "Anvilrage Medic",
		[8910] = "Unknown",
		[6520] = "Scorching Elemental",
		[8958] = "Angerclaw Mauler",
		[6552] = "Gorishi Worker",
		[9038] = "Seeth'rel",
		[412] = "Stitches",
		[1653] = "Bloodsail Elder Magus",
		[9166] = "Pterrordax",
		[1663] = "Dextren Ward",
		[417] = "Droomon",
		[4633] = "Kolkar Scout",
		[4641] = "Magram Windchaser",
		[4657] = "Maraudine Windchaser",
		[4665] = "Burning Blade Adept",
		[4673] = "Hatefury Betrayer",
		[4681] = "Mage Hunter",
		[4689] = "Starving Bonepaw",
		[422] = "Murloc Flesheater",
		[423] = "Redridge Mongrel",
		[424] = "Redridge Poacher",
		[3397] = "Kolkar Bloodcharger",
		[13596] = "Rotgrip",
		[426] = "Redridge Brute",
		[1707] = "Defias Captive",
		[3417] = "Living Flame <Defias Magician>",
		[1711] = "Defias Convict",
		[3425] = "Poot",
		[429] = "Shadowhide Darkweaver",
		[1717] = "Hamhock",
		[430] = "Redridge Mystic",
		[431] = "Shadowhide Slayer",
		[1725] = "Defias Watchman",
		[1727] = "Defias Worker",
		[1729] = "Defias Evoker",
		[1731] = "Goblin Craftsman",
		[434] = "Rabid Shadowhide Gnoll",
		[435] = "Blackrock Champion",
		[437] = "Blackrock Renegade",
		[4969] = "Old Town Thug",
		[440] = "Blackrock Grunt",
		[1763] = "Gilnid",
		[1765] = "Worg",
		[442] = "Tarantula",
		[1769] = "Moonrage Whitescalp",
		[7112] = "Jaedenar Cultist",
		[7120] = "Jaedenar Warlock",
		[446] = "Redridge Basher",
		[5089] = "Balos Jacken",
		[3573] = "Mana Spring Totem <Doody>",
		[1789] = "Skeletal Acolyte",
		[448] = "Hogger",
		[449] = "Defias Knuckleduster",
		[452] = "Riverpaw Bandit",
		[8319] = "Nightmare Whelp",
		[453] = "Riverpaw Mystic",
		[1815] = "Diseased Black Bear",
		[7272] = "Theka the Martyr",
		[5241] = "Gordunni Warlord",
		[8447] = "Clunk",
		[7320] = "Stonevault Mauler",
		[7328] = "Withered Reaver",
		[7344] = "Splinterbone Warrior",
		[7352] = "Frozen Soul",
		[7376] = "Sky Shadow",
		[5337] = "Hatecrest Siren",
		[7400] = "Searing Totem V <Hotpotato>",
		[7416] = "Mana Spring Totem IV <Tectonix>",
		[3717] = "Wrathtail Sorceress",
		[7440] = "Winterfall Den Watcher",
		[1863] = "Aeztai",
		[7464] = "Magma Totem II",
		[1869] = "Ravenclaw Champion",
		[1871] = "Eliza's Guard",
		[3757] = "Xavian Hellcaller",
		[8895] = "Anvilrage Officer",
		[8911] = "Fireguard Destroyer",
		[8959] = "Felpaw Wolf",
		[473] = "Morgan the Collector",
		[474] = "Defias Rogue Wizard",
		[9039] = "Doom'rel",
		[476] = "Kobold Geomancer",
		[3809] = "Ashenvale Bear",
		[1907] = "Naga Explorer",
		[1909] = "Vile Fin Lakestalker",
		[478] = "Riverpaw Outrunner",
		[5601] = "Khan Jehn",
		[9167] = "Frenzied Pterrordax",
		[30] = "Forest Spider",
		[480] = "Rusty Harvest Golem",
		[481] = "Defias Footpad",
		[5649] = "Sandfury Blood Drinker",
		[485] = "Blackrock Outrunner",
		[5713] = "Gasher",
		[5721] = "Dreamscythe",
		[3909] = "Healing Stream Totem V",
		[3913] = "Stoneclaw Totem IV <Daps>",
		[1961] = "Mangeclaw",
		[7856] = "Southsea Freebooter",
		[7872] = "Death's Head Cultist",
		[5833] = "Margol the Rager",
		[5857] = "Searing Lava Spider",
		[11790] = "Putridus Satyr",
		[3977] = "High Inquisitor Whitemane",
		[5913] = "Tremor Totem <Grimlok>",
		[5929] = "Magma Totem <Witherbark Witch Doctor>",
		[1999] = "Webwood Venomfang",
		[501] = "Riverpaw Herbalist",
		[2005] = "Vicious Grell",
		[502] = "Benny Blaanco",
		[5985] = "Snickerfang Hyena",
		[5993] = "Helboar",
		[504] = "Defias Trapper",
		[6017] = "Lava Spout Totem <Stonevault Oracle>",
		[505] = "Greater Tarantula",
		[4041] = "Scorched Basilisk",
		[10079] = "Brave Moonhorn",
		[8120] = "Sul'lithuz Abomination",
		[8136] = "Lord Shalzaru",
		[12206] = "Primordial Behemoth",
		[12222] = "Creeping Sludge",
		[511] = "Insane Ghoul",
		[515] = "Murloc Raider",
		[2070] = "Moonstalker Runt",
		[6193] = "Spitelash Screamer",
		[4154] = "Salt Flats Scavenger",
		[522] = "Mor'Ladim",
		[8384] = "Deep Lurker",
		[8400] = "Obsidion",
		[14621] = "Overseer Maltorius",
		[533] = "Nightbane Shadow Weaver",
		[534] = "Nefaru",
		[4282] = "Scarlet Magician",
		[4290] = "Scarlet Guardsman",
		[4298] = "Scarlet Defender",
		[4306] = "Scarlet Torturer",
		[6377] = "Thunderhead Stagwing",
		[4362] = "Mirefin Coastrunner",
		[4378] = "Darkmist Recluse",
		[2206] = "Greymist Hunter",
		[4418] = "Defias Wizard",
		[6497] = "Astor Hadren",
		[6505] = "Ravasaur",
		[6513] = "Un'Goro Stomper",
		[6521] = "Living Blaze",
		[8960] = "Felpaw Scavenger",
		[2246] = "Syndicate Assassin",
		[2250] = "Mountain Yeti",
		[6553] = "Gorishi Reaver",
		[565] = "Rabid Dire Wolf",
		[9040] = "Dope'rel",
		[568] = "Shadowhide Warrior",
		[569] = "Green Recluse",
		[570] = "Brain Eater",
		[36] = "Harvest Golem",
		[578] = "Murloc Scout",
		[579] = "Shadowhide Assassin",
		[4634] = "Kolkar Mauler",
		[4642] = "Magram Stormer",
		[4666] = "Burning Blade Felsworn",
		[4674] = "Hatefury Shadowstalker",
		[4682] = "Nether Sister",
		[587] = "Bloodscalp Warrior",
		[2350] = "Forest Moss Creeper",
		[2354] = "Vicious Gray Bear",
		[4714] = "Slitherblade Myrmidon",
		[2370] = "Daggerspine Screamer",
		[2374] = "Torn Fin Muckdweller",
		[595] = "Bloodscalp Hunter",
		[598] = "Defias Miner",
		[2406] = "Mountain Lion",
		[604] = "Plague Spreader",
		[4850] = "Stonevault Cave Lurker",
		[6913] = "Lost One Rift Traveler",
		[11791] = "Putridus Trickster",
		[615] = "Blackrock Tracker",
		[2462] = "Flesh Eating Worm",
		[619] = "Defias Conjurer",
		[622] = "Goblin Engineer",
		[623] = "Skeletal Miner",
		[624] = "Undead Excavator",
		[625] = "Undead Dynamiter",
		[626] = "Foreman Thistlenettle",
		[628] = "Black Ravager",
		[2522] = "Humperdoo",
		[2530] = "Yenniku",
		[2534] = "Zanzil the Outcast",
		[12207] = "Thessala Hydra",
		[12223] = "Cavern Lurker",
		[12239] = "Gelk",
		[40] = "Kobold Miner",
		[2562] = "Boulderfist Ogre",
		[642] = "Sneed's Shredder",
		[643] = "Sneed <Sneed's Shredder>",
		[2574] = "Drywhisker Digger",
		[645] = "Cookie",
		[646] = "Mr. Smite",
		[2586] = "Syndicate Highwayman",
		[12431] = "Shiba",
		[5226] = "Murk Worm",
		[5234] = "Gordunni Mauler",
		[657] = "Defias Pirate",
		[2630] = "Earthbind Totem <Trucc>",
		[8497] = "Unknown",
		[7329] = "Withered Quilguard",
		[7337] = "Death's Head Necromancer",
		[2650] = "Witherbark Zealot",
		[7353] = "Freezing Spirit",
		[667] = "Skullsplitter Warrior",
		[669] = "Skullsplitter Hunter",
		[670] = "Skullsplitter Witch Doctor",
		[672] = "Skullsplitter Spiritchaser",
		[674] = "Venture Co. Strip Miner",
		[675] = "Venture Co. Foreman",
		[676] = "Venture Co. Surveyor",
		[677] = "Venture Co. Tinkerer",
		[7465] = "Magma Totem III <Melstar>",
		[5426] = "Blisterpaw Hyena",
		[5434] = "Coral Shark",
		[681] = "Wolf",
		[682] = "Stranglethorn Tiger",
		[683] = "Young Panther",
		[684] = "Shadowmaw Panther",
		[685] = "Stranglethorn Raptor",
		[2742] = "Shadowforge Chanter",
		[8929] = "Princess Moira Bronzebeard",
		[10992] = "Enraged Panther",
		[689] = "Crystal Spine Basilisk",
		[690] = "Cold Eye Basilisk",
		[2762] = "Thundering Exile",
		[694] = "Bloodscalp Axe Thrower",
		[696] = "Skullsplitter Axe Thrower",
		[697] = "Bloodscalp Shaman",
		[702] = "Bloodscalp Scavenger",
		[5618] = "Wastewander Bandit",
		[5650] = "Sandfury Witch Doctor",
		[2830] = "Buzzard",
		[709] = "Mosh'Ogg Warmonger",
		[712] = "Redridge Thrasher",
		[2850] = "Zeros",
		[5714] = "Loro",
		[5722] = "Hazzas",
		[7785] = "Ward of Zum'rah <Witch Doctor Zum'rah>",
		[11552] = "Timbermaw Mystic",
		[2894] = "Stonevault Shaman",
		[9537] = "Hurley Blackbreath",
		[2906] = "Dustbelcher Warrior",
		[729] = "Sin'Dall",
		[731] = "Calypso",
		[5850] = "Blazing Elemental",
		[5858] = "Greater Lava Spider",
		[5874] = "Strength of Earth Totem <Johncheeks>",
		[736] = "Panther",
		[2946] = "Puppet of Helcular <Helcular's Remains>",
		[2950] = "Palemane Skinner",
		[740] = "Adolescent Whelp",
		[741] = "Dreaming Whelp",
		[2966] = "Battleboar",
		[747] = "Marsh Murloc",
		[5978] = "Dreadmaul Warlock",
		[750] = "Marsh Inkspewer",
		[751] = "Marsh Flesheater",
		[752] = "Marsh Oracle",
		[756] = "Skullsplitter Panther",
		[757] = "Lost One Fisherman",
		[759] = "Lost One Hunter",
		[760] = "Lost One Muckdweller",
		[761] = "Lost One Seer",
		[762] = "Lost One Riftseeker",
		[764] = "Swampwalker",
		[12224] = "Cavern Shambler",
		[3] = "Flesh Eater",
		[767] = "Cat",
		[768] = "Shadow Panther",
		[769] = "Deathstrike Tarantula",
		[772] = "Stranglethorn Tigress",
		[4139] = "Scorpid Terror",
		[4147] = "Saltstone Basilisk",
		[6202] = "Legashi Hellcaller",
		[3110] = "Dreadmaw Crocolisk",
		[780] = "Skullsplitter Mystic",
		[14527] = "Simone the Seductress",
		[782] = "Skullsplitter Scout",
		[783] = "Skullsplitter Berserker",
		[784] = "Skullsplitter Beastmaster",
		[785] = "Skeletal Warder",
		[787] = "Skeletal Healer",
		[4283] = "Scarlet Sentry",
		[4291] = "Scarlet Diviner",
		[4299] = "Scarlet Chaplain",
		[6386] = "Ward of Zanzil <Zanzil the Outcast>",
		[4355] = "Bloodfen Scytheclaw",
		[4363] = "Mirefin Oracle",
		[4379] = "Darkmist Silkspinner",
		[4411] = "Darkfang Lurker",
		[12976] = "Kolkar Waylayer",
		[8898] = "Anvilrage Marshal",
		[6506] = "Ravasaur Runner",
		[3258] = "Bristleback Hunter",
		[3274] = "Kolkar Pack Runner",
		[9026] = "Overmaster Pyron",
		[822] = "Young Forest Bear",
		[824] = "Defias Digger",
		[832] = "Dust Devil",
		[833] = "Coyote Packleader",
		[4619] = "Geltharis",
		[4635] = "Kolkar Windchaser",
		[4667] = "Burning Blade Shadowmage",
		[4675] = "Hatefury Hellcaller",
		[13456] = "Unknown <Noxxion>",
		[4699] = "Scorpashi Venomlash",
		[4715] = "Slitherblade Razortail",
		[4723] = "Foreman Cozzle",
		[11521] = "Kodo Apparition",
		[11553] = "Timbermaw Woodbender",
		[854] = "Young Jungle Stalker",
		[855] = "Young Stranglethorn Raptor",
		[856] = "Young Lashtail Raptor",
		[13696] = "Noxxious Scion",
		[858] = "Sorrow Spinner",
		[861] = "Stonard Scout",
		[862] = "Stonard Explorer",
		[4851] = "Stonevault Rockchewer",
		[11777] = "Shadowshard Rumbler",
		[11793] = "Celebrian Dryad",
		[867] = "Stonard Cartographer",
		[11937] = "Demon Portal Guardian",
		[9938] = "Magmus",
		[7026] = "Blackrock Sorcerer",
		[880] = "Erlan Drudgemoor",
		[881] = "Surena Caledon",
		[7050] = "Defias Drone",
		[5043] = "Defias Rioter",
		[889] = "Splinter Fist Ogre",
		[7114] = "Jaedenar Enforcer",
		[891] = "Splinter Fist Fire Weaver",
		[892] = "Splinter Fist Taskmaster",
		[12225] = "Celebras the Cursed",
		[12241] = "Magra",
		[7154] = "Deadwood Gardener",
		[3586] = "Miner Johnson",
		[898] = "Nightbane Worgen",
		[12369] = "Lord Kragaru",
		[14432] = "Threggil",
		[7226] = "Unknown <Ancient Stone Keeper>",
		[905] = "Sharptooth Frenzy",
		[909] = "Defias Night Blade",
		[910] = "Defias Enchanter",
		[8419] = "Twilight Idolater",
		[7290] = "Shadowforge Sharpshooter",
		[5251] = "Woodpaw Trapper",
		[5259] = "Atal'ai Witch Doctor",
		[5267] = "Unliving Atal'ai",
		[5283] = "Nightmare Wanderer",
		[7346] = "Splinterbone Centurion",
		[920] = "Nightbane Tainted One",
		[921] = "Venture Co. Lumberjack",
		[923] = "Young Black Ravager",
		[7402] = "Searing Totem VI <Adeemdoe>",
		[8675] = "Felbeast",
		[930] = "Black Widow Hatchling",
		[7450] = "Ragged Owlbeast",
		[7458] = "Ice Thistle Yeti",
		[5419] = "Glasshide Basilisk",
		[5427] = "Rabid Blisterpaw",
		[3742] = "Saltspittle Oracle",
		[937] = "Kurzen Jungle Fighter",
		[938] = "Kurzen Commando",
		[3754] = "Xavian Betrayer",
		[940] = "Kurzen Medicine Man",
		[941] = "Kurzen Headshrinker",
		[942] = "Kurzen Witch Doctor",
		[943] = "Kurzen Wrangler",
		[946] = "Frostmane Novice",
		[948] = "Rotted One",
		[949] = "Carrion Recluse",
		[3806] = "Forsaken Infiltrator",
		[3810] = "Unknown",
		[3834] = "Crazed Ancient",
		[5643] = "Tyranis Malem",
		[5715] = "Hukku",
		[7786] = "Skeleton of Zum'rah",
		[3902] = "Searing Totem II",
		[977] = "Kurzen War Panther",
		[978] = "Kurzen Subchief",
		[979] = "Kurzen Shadow Hunter",
		[7858] = "Southsea Swashbuckler",
		[7874] = "Razorfen Thornweaver",
		[5843] = "Slave Worker",
		[9683] = "Lar'korwi Mate",
		[11778] = "Shadowshard Smasher",
		[3974] = "Houndmaster Loksey",
		[5979] = "Wretched Lost One",
		[1007] = "Mosshide Gnoll",
		[1008] = "Mosshide Mongrel",
		[4038] = "Burning Destroyer",
		[1011] = "Mosshide Trapper",
		[4062] = "Dark Iron Bombardier",
		[8138] = "Sul'lithuz Broodling",
		[8154] = "Ghost Walker Brave",
		[1021] = "Mottled Screecher",
		[12242] = "Maraudos",
		[12258] = "Razorlash",
		[4094] = "Galak Scout",
		[1026] = "Bluegill Forager",
		[1028] = "Bluegill Muckdweller",
		[1030] = "Black Slime",
		[4124] = "MOMOOSHI",
		[1034] = "Dragonmaw Raider",
		[6187] = "Timbermaw Den Watcher",
		[6195] = "Spitelash Siren",
		[1040] = "Fen Creeper",
		[8324] = "Atal'ai Skeleton <Zolo>",
		[14529] = "Klinfran the Crazed",
		[1052] = "Dark Iron Saboteur",
		[4260] = "Venture Co. Shredder",
		[4284] = "Scarlet Augur",
		[8580] = "Atal'alarion",
		[4300] = "Scarlet Wizard",
		[4308] = "Unfettered Spirit",
		[4316] = "Kolkar Packhound",
		[2163] = "Thistle Bear",
		[1084] = "Young Sawtooth Crocolisk",
		[1094] = "Venture Co. Miner",
		[6427] = "Haunting Phantasm",
		[2207] = "Greymist Oracle",
		[1108] = "Mistvale Gorilla",
		[1110] = "Skeletal Raider",
		[8900] = "Doomforge Arcanasmith",
		[6507] = "Ravasaur Hunter",
		[6523] = "Dark Iron Rifleman",
		[2243] = "Syndicate Sentry",
		[2247] = "Syndicate Enforcer",
		[8996] = "Voidwalker Minion <Defias Conjurer>",
		[6555] = "Gorishi Tunneler",
		[1134] = "Young Wendigo",
		[4540] = "Scarlet Monk",
		[1138] = "Snow Tracker Wolf",
		[1142] = "Mosh'Ogg Brute",
		[2287] = "Crushridge Warmonger",
		[9156] = "Ambassador Flamelash",
		[13282] = "Noxxion",
		[1152] = "Snapjaw Crocolisk",
		[1158] = "Cursed Marine",
		[2319] = "Syndicate Wizard",
		[4644] = "Magram Marauder",
		[1164] = "Stonesplinter Bonesnapper",
		[1166] = "Stonesplinter Seer",
		[4668] = "Burning Blade Summoner",
		[2339] = "Twilight Thug",
		[4684] = "Nether Sorceress",
		[4692] = "Dread Swoop",
		[9396] = "Ground Pounder",
		[4716] = "Slitherblade Tidehunter",
		[1184] = "Cliff Lurker",
		[2371] = "Daggerspine Siren",
		[2375] = "Torn Fin Coastrunner",
		[1196] = "Ice Claw Bear",
		[4788] = "Unknown",
		[1200] = "Morbent Fel",
		[2407] = "Hulking Mountain Lion",
		[4844] = "Shadowforge Surveyor",
		[2427] = "Jailor Eston",
		[2431] = "Jailor Borhuin",
		[1236] = "Kobold Digger",
		[7011] = "Earthen Rocksmasher",
		[9956] = "Shadowforge Flame Keeper",
		[7043] = "Flamescale Wyrmkin",
		[7051] = "Malformed Defias Drone",
		[7067] = "Venture Co. Drone",
		[7091] = "Shadowforge Ambusher",
		[7099] = "Owl",
		[2531] = "Minion of Morganth <Morganth>",
		[2535] = "Maury \"Club Foot\" Wilkins",
		[1270] = "Fetid Corpse",
		[2547] = "Ironpatch",
		[12243] = "Veng",
		[2555] = "Witherbark Witch Doctor",
		[2559] = "Highland Strider",
		[2563] = "Plains Creeper",
		[2567] = "Boulderfist Magus",
		[2575] = "Dark Iron Supplier",
		[2579] = "Mesa Buzzard",
		[2587] = "Syndicate Pathstalker",
		[2595] = "Daggerspine Raider",
		[7267] = "Chief Ukorz Sandscalp",
		[7275] = "Shadowpriest Sezz'ziz",
		[2619] = "Hammerfall Grunt",
		[8437] = "Hakkari Minion",
		[5260] = "Groddoc Ape",
		[2635] = "Elder Saltwater Crocolisk",
		[5292] = "Feral Scar Yeti",
		[7347] = "Boneflayer Ghoul",
		[7355] = "Tuten'kash",
		[2667] = "Ward of Laze",
		[7443] = "Shardtooth Mauler",
		[7451] = "Raging Owlbeast",
		[7459] = "Ice Thistle Matriarch",
		[5420] = "Glasshide Gazer",
		[2715] = "Unknown",
		[8837] = "Muck Splash",
		[2727] = "Crag Coyote",
		[10916] = "Winterfall Runner",
		[2735] = "Lesser Rock Elemental",
		[8901] = "Anvilrage Reservist",
		[2743] = "Shadowforge Warrior",
		[10980] = "Umi's Mechanical Yeti",
		[1388] = "Vagash",
		[2831] = "Giant Buzzard",
		[1418] = "Bluegill Raider",
		[89] = "Infernal <Igmaroth>",
		[1426] = "Riverpaw Miner",
		[5708] = "Spawn of Hakkar",
		[5716] = "Zul'Lor",
		[7787] = "Sandfury Slave",
		[7795] = "Hydromancer Velratha",
		[9477] = "Cloned Ooze <Primal Ooze>",
		[9541] = "Blackbreath Crony",
		[2907] = "Dustbelcher Mystic",
		[2919] = "Fam'retor Guardian",
		[5844] = "Unknown",
		[7899] = "Treasure Hunting Pirate",
		[5860] = "Twilight Dark Shaman",
		[92] = "Rock Elemental",
		[1488] = "Zanzil Zombie",
		[1490] = "Zanzil Witch Doctor",
		[9877] = "Prince Xavalis",
		[5988] = "Scorpok Stinger",
		[94] = "Defias Cutpurse",
		[95] = "Defias Smuggler",
		[8179] = "Greater Healing Ward <Sandfury Witch Doctor>",
		[1548] = "Cursed Darkhound",
		[1550] = "Thrashtail Basilisk",
		[1554] = "Vampiric Duskbat",
		[1562] = "Bloodsail Mage",
		[1564] = "Bloodsail Warlock",
		[8438] = "Hakkari Bloodkeeper",
		[99] = "Morgaine the Sly",
		[4285] = "Scarlet Disciple",
		[4293] = "Scarlet Scryer",
		[4301] = "Scarlet Centurion",
		[6388] = "Zanzil Skeleton",
		[6412] = "Skeleton",
		[4389] = "Murk Thresher",
		[12900] = "Somnus",
		[4413] = "Darkfang Spider",
		[3239] = "Thunderhead",
		[3243] = "Cat",
		[6500] = "Unknown",
		[6508] = "Venomhide Ravasaur",
		[6516] = "Un'Goro Thunderer",
		[3263] = "Bristleback Geomancer",
		[6556] = "Muculent Ooze",
		[103] = "Garrick Padfoot",
		[11141] = "Spirit of Trey Lightforge",
		[1666] = "Kam Deepfury",
		[4637] = "Kolkar Destroyer",
		[4645] = "Magram Mauler",
		[9318] = "Incendosaur",
		[4677] = "Doomwarder",
		[4685] = "Ley Hunter",
		[4693] = "Dread Flyer",
		[1696] = "Targorr the Dread",
		[1706] = "Defias Prisoner",
		[1708] = "Defias Inmate",
		[9622] = "MorangoTango",
		[11685] = "Maraudine Priest",
		[1720] = "Bruegal Ironknuckle",
		[4845] = "Shadowforge Ruffian",
		[4853] = "Stonevault Geomancer",
		[4861] = "Shrike Bat",
		[11781] = "Ambershard Crusher",
		[1732] = "Defias Squallshaper",
		[3475] = "Blucifer",
		[9862] = "Jaedenar Legionnaire",
		[9878] = "Entropic Beast",
		[7012] = "Earthen Sculptor",
		[1756] = "Unknown",
		[7052] = "Defias Tower Patroller",
		[1766] = "Mottled Worg",
		[7076] = "Earthen Guardian",
		[7092] = "Tainted Ooze",
		[7100] = "Warpwood Moss Flayer",
		[1778] = "Ferocious Grizzled Bear",
		[14276] = "Scargil",
		[7156] = "Deadwood Den Watcher",
		[14356] = "Sawfin Frenzy",
		[14372] = "Winterfall Ambusher",
		[14388] = "Rogue Black Drake",
		[7228] = "Ironaya",
		[3619] = "Nidalee",
		[8391] = "Lathoric the Black",
		[7276] = "Unknown",
		[1822] = "Venom Mist Lurker",
		[5253] = "Woodpaw Brute",
		[5261] = "Enthralled Atal'ai",
		[5269] = "Atal'ai Priest",
		[5277] = "Nightmare Scalebane",
		[7332] = "Withered Spearhide",
		[7340] = "Skeletal Shadowcaster <Skeletal Summoner>",
		[115] = "Harvest Reaper",
		[8567] = "Glutton",
		[5333] = "Hatecrest Serpent Guard",
		[7396] = "Earthen Stonebreaker",
		[116] = "Defias Bandit",
		[1860] = "Zhar'nagma",
		[7444] = "Shardtooth Bear",
		[7460] = "Ice Thistle Patriarch",
		[1868] = "Ravenclaw Servant",
		[117] = "Riverpaw Gnoll",
		[3743] = "Foulweald Warrior",
		[5461] = "Sea Elemental",
		[5477] = "Noboru the Cudgel",
		[3767] = "Bleakheart Trickster",
		[118] = "Prowler",
		[8983] = "Golem Lord Argelmach",
		[3791] = "Terrowulf Shadow Weaver",
		[13141] = "Deeprot Stomper",
		[7604] = "Sergeant Bly",
		[3807] = "Forsaken Assassin",
		[3811] = "Giant Ashenvale Bear",
		[1908] = "Vile Fin Oracle",
		[3819] = "Wildthorn Stalker",
		[3823] = "Ghostpaw Runner",
		[1922] = "Gray Forest Wolf",
		[5645] = "Sandfury Hideskinner",
		[121] = "Defias Pathstalker",
		[5709] = "Shade of Eranikus",
		[5717] = "Mijan",
		[7788] = "Sandfury Drudge",
		[7796] = "Nekrum Gutchewer",
		[3903] = "Searing Totem III <Trucc>",
		[11558] = "Kernda",
		[7844] = "Fire Nova Totem IV <Praujekt>",
		[123] = "Riverpaw Mongrel",
		[3947] = "Goblin Shipbuilder",
		[5853] = "Tempered War Golem",
		[5861] = "Twilight Fire Guard",
		[124] = "Riverpaw Brute",
		[11782] = "Ambershard Destroyer",
		[3975] = "Herod",
		[3983] = "Interrogator Vishas",
		[5925] = "Grounding Totem <Dacapo>",
		[7980] = "Deathguard Elite",
		[2002] = "Rascal Sprite",
		[2004] = "Dark Sprite",
		[4011] = "Young Pridewing",
		[5981] = "Portal Seeker",
		[126] = "Murloc Coastrunner",
		[4067] = "Aegon",
		[2038] = "Lord Melenas",
		[8156] = "Servant of Antu'sul <Antu'sul>",
		[4095] = "Galak Mauler",
		[4126] = "Bigbeast",
		[6189] = "Timbermaw Ursa",
		[4150] = "Saltstone Gazer",
		[4158] = "Salt Flats Vulture",
		[6221] = "Addled Leper",
		[8440] = "Avatar of Hakkar",
		[8504] = "Dark Iron Sentry",
		[4286] = "Scarlet Soldier",
		[4294] = "Scarlet Sorcerer",
		[4302] = "Scarlet Champion",
		[2164] = "Rabid Thistle Bear",
		[4342] = "Drywallow Vicejaw",
		[4358] = "Mirefin Puddlejumper",
		[4382] = "Withervine Creeper",
		[4414] = "Darkfang Venomspitter",
		[6493] = "Illusionary Phantasm <Haunting Phantasm>",
		[6501] = "Stegodon",
		[8920] = "Weapon Technician",
		[6517] = "Tar Beast",
		[2240] = "Syndicate Footpad",
		[2248] = "Cave Yeti",
		[2252] = "Crushridge Ogre",
		[6557] = "Primal Ooze",
		[2260] = "Unknown",
		[13142] = "Deeprot Tangler",
		[4542] = "High Inquisitor Fairbanks",
		[4654] = "Maraudine Scout",
		[4670] = "Hatefury Rogue",
		[4678] = "Mana Eater",
		[6733] = "Stonevault Basher",
		[4694] = "Coffeecream",
		[2356] = "Elder Gray Bear",
		[4718] = "Slitherblade Oracle",
		[4726] = "Raging Thunder Lizard",
		[9464] = "Overlord Ror",
		[2372] = "Mudsnout Gnoll",
		[2376] = "Torn Fin Oracle",
		[11559] = "Outcast Necromancer",
		[2384] = "Starving Mountain Lion",
		[13718] = "The Nameless Prophet",
		[11687] = "Unknown",
		[2416] = "Crushridge Plunderer",
		[4846] = "Shadowforge Digger",
		[4854] = "Grimlok",
		[11783] = "Theradrim Shardling",
		[154] = "Greater Fleshripper",
		[7053] = "Klaven Mortwake",
		[7077] = "Earthen Hallshaper",
		[2520] = "Remote-Controlled Golem <Goblin Engineer>",
		[7101] = "Warpwood Shredder",
		[10120] = "Vault Warder",
		[2536] = "Jon-Jon the Crow",
		[7125] = "Jaedenar Hound",
		[2548] = "Captain Keelhaul",
		[2552] = "Witherbark Troll",
		[2556] = "Witherbark Headhunter",
		[2560] = "Highland Thrasher",
		[2564] = "Boulderfist Enforcer",
		[2572] = "Drywhisker Kobold",
		[2580] = "Elder Mesa Buzzard",
		[2588] = "Syndicate Prowler",
		[2592] = "Rumbling Exile",
		[2596] = "Daggerspine Sorceress",
		[5238] = "Gordunni Battlemaster",
		[5262] = "Groddoc Thunderer",
		[5270] = "Atal'ai Corpse Eater",
		[5286] = "Cat",
		[7341] = "Skeletal Frostweaver",
		[7349] = "Tomb Fiend",
		[7357] = "Mordresh Fire Eye",
		[8585] = "Unknown <Amnennar the Coldbringer>",
		[10648] = "Xavaric",
		[5334] = "Hatecrest Myrmidon",
		[2676] = "Compact Harvest Reaper <Venture Co. Tinkerer>",
		[7405] = "Deadly Cleft Scorpid",
		[5422] = "Scorpid Hunter",
		[2716] = "Dustbelcher Wyrmhunter",
		[2728] = "Feral Crag Coyote",
		[2732] = "Ridge Huntress",
		[2736] = "Greater Rock Elemental",
		[2740] = "Shadowforge Darkweaver",
		[8921] = "Bloodhound",
		[2748] = "Archaedas",
		[2760] = "Burning Exile",
		[9033] = "General Angerforge",
		[2776] = "Vengeful Surge",
		[7605] = "Raven",
		[5622] = "Ongeku",
		[5646] = "Sandfury Axe Thrower",
		[7725] = "Grimtotem Raider",
		[5710] = "Jammal'an the Prophet",
		[7789] = "Sandfury Cretin",
		[7797] = "Ruuzlu",
		[11560] = "Magrami Spectre",
		[11576] = "Whirlwind Ripper",
		[9545] = "Grim Patron",
		[5846] = "Dark Iron Taskmaster",
		[5854] = "Heavy War Golem",
		[5862] = "Twilight Geomancer",
		[7321] = "Stonevault Flameweaver",
		[2944] = "Boss Tho'grun",
		[11784] = "Theradrim Guardian",
		[500] = "Riverpaw Scout",
		[7523] = "Suffering Highborne",
		[7524] = "Anguished Highborne",
		[7448] = "Chillwind Chimaera",
		[416] = "Kartai",
		[9518] = "Rakaiah",
		[1770] = "Moonrage Darkrunner",
		[475] = "Kobold Tunneler",
		[5293] = "Hulking Feral Scar",
		[10882] = "Wind Serpent",
		[5982] = "Black Slayer",
		[5990] = "Redstone Basilisk",
		[7845] = "Fire Nova Totem V <Toxicityd>",
		[6006] = "Shadowsworn Adept",
		[5249] = "Woodpaw Mongrel",
		[7108] = "Jadefire Betrayer",
		[9879] = "Entropic Horror",
		[11794] = "Sister of Celebrian",
		[2892] = "Stonevault Seer",
		[122] = "Defias Highwayman",
		[7113] = "Jaedenar Guardian",
		[1716] = "Bazil Thredd",
		[703] = "Lieutenant Fangore",
		[3981] = "Vorrel Sengutz",
		[7106] = "Jadefire Rogue",
		[7291] = "Galgann Firehammer",
		[12216] = "Poison Sprite",
		[735] = "Murloc Streamrunner",
		[5771] = "Jugkar Grim'rod",
		[6196] = "Spitelash Myrmidon",
		[976] = "Kurzen War Tiger",
		[1726] = "Defias Magician",
		[9554] = "Hammered Patron",
		[4119] = "Wind Serpent",
		[46] = "Murloc Forager",
		[6198] = "Blood Elf Surveyor",
		[6190] = "Spitelash Warrior",
		[3100] = "Cujo",
		[11792] = "Putridus Shadowstalker",
		[8915] = "Twilight's Hammer Ambassador",
		[4457] = "Murkgill Forager",
		[4729] = "Hulking Gritjaw Basilisk",
		[5308] = "Rogue Vale Screecher",
		[3035] = "Flatland Cougar",
		[3765] = "Bleakheart Satyr",
		[9118] = "Larion",
		[8903] = "Anvilrage Captain",
		[2739] = "Shadowforge Tunneler",
		[5617] = "Wastewander Shadow Mage",
		[1715] = "Defias Insurgent",
		[8899] = "Doomforge Dragoon",
		[5760] = "Lord Azrethoc",
		[3821] = "Wildthorn Lurker",
		[10617] = "Galak Messenger",
		[4287] = "Scarlet Gallant",
		[4295] = "Scarlet Myrmidon",
		[4303] = "Scarlet Abbot",
		[199] = "Young Fleshripper",
		[5307] = "Vale Screecher",
		[4505] = "Bloodsail Deckhand",
		[691] = "Lesser Water Elemental",
		[4343] = "Drywallow Snapper",
		[4351] = "Bloodfen Raptor",
		[4359] = "Mirefin Murloc",
		[432] = "Shadowhide Brute",
		[2734] = "Ridge Stalker Patriarch",
		[6004] = "Shadowsworn Cultist",
		[10928] = "Succubus Minion",
		[2348] = "Elder Moss Creeper",
		[202] = "Skeletal Horror",
		[5428] = "Carrion Bird",
		[5228] = "Saturated Ooze",
		[4463] = "Blackrock Summoner",
		[203] = "Skeletal Mage",
		[8890] = "Anvilrage Warden",
		[6502] = "Plated Stegodon",
		[3256] = "Raymond",
		[3260] = "Bristleback Water Seeker",
		[781] = "Skullsplitter Headhunter",
		[5332] = "Hatecrest Wave Rider",
		[5268] = "Unknown",
		[205] = "Nightbane Dark Runner",
		[4511] = "Boar",
		[9034] = "Hate'rel",
		[4277] = "Eye of Kilrogg",
		[206] = "Nightbane Vile Fang",
		[4543] = "Bloodmage Thalnos",
		[5236] = "Gordunni Shaman",
		[2618] = "Hammerfall Peon",
		[7274] = "Sandfury Executioner",
		[3771] = "Bleakheart Hellcaller",
		[9162] = "Young Diemetradon",
		[2793] = "Kor'gresh Coldrage",
		[8298] = "Akubar the Seer",
		[441] = "Black Dragon Whelp",
		[3739] = "Saltspittle Warrior",
		[2554] = "Witherbark Axe Thrower",
		[634] = "Defias Overseer",
		[4639] = "Magram Outrunner",
		[636] = "Defias Blackguard",
		[4655] = "Maraudine Wrangler",
		[210] = "Bone Chewer",
		[4671] = "Hatefury Trickster",
		[4679] = "Nether Maiden",
		[4687] = "Deepstrider Searcher",
		[4695] = "Carrion Horror",
		[212] = "Splinter Fist Warrior",
		[4711] = "Slitherblade Naga",
		[4719] = "Slitherblade Sea Witch",
		[4727] = "Elder Thunder Lizard",
		[13601] = "Tinkerer Gizlock",
		[462] = "Vultros",
		[12221] = "Noxious Slime",
		[213] = "Starving Dire Wolf",
		[11577] = "Whirlwind Stormwalker",
		[819] = "Servant of Ilgalar",
		[4341] = "Drywallow Crocolisk",
		[8889] = "Anvilrage Overseer",
		[445] = "Redridge Alpha",
		[2733] = "Apothecary Jorell",
		[644] = "Rhahk'Zor",
		[215] = "Defias Night Runner",
		[4461] = "Murkgill Warrior",
		[4686] = "Deepstrider Giant",
		[4847] = "Shadowforge Relic Hunter",
		[4855] = "Stonevault Brawler",
		[4863] = "Jadespine Basilisk",
		[11785] = "Ambereye Basilisk",
		[6910] = "Revelosh",
		[217] = "Venom Web Spider",
		[4860] = "Stone Steward",
		[2434] = "Shadowy Assassin",
		[647] = "Captain Greenskin",
		[218] = "Grave Robber",
		[7447] = "Fledgling Chillwind",
		[4852] = "Stonevault Oracle",
		[397] = "Morganth",
		[6005] = "Shadowsworn Thug",
		[3415] = "Unknown",
		[43] = "Mine Spider",
		[7022] = "Venomlash Scorpid",
		[7030] = "Shadowforge Geologist",
		[7027] = "Blackrock Slayer",
		[7107] = "Jadefire Trickster",
		[3381] = "Southsea Brigand",
		[589] = "Defias Pillager",
		[7118] = "Jaedenar Darkweaver",
		[7078] = "Cleft Scorpid",
		[7086] = "Cursed Ooze",
		[12201] = "Princess Theradras",
		[2653] = "Witherbark Sadist",
		[7110] = "Jadefire Shadowstalker",
		[3560] = "Healing Ward",
		[7126] = "Unknown",
		[12217] = "Corruptor",
		[1162] = "Stonesplinter Scout",
		[5288] = "Rabid Longtooth",
		[7158] = "Deadwood Shaman",
		[5280] = "Nightmare Wyrmkin",
		[4636] = "Kolkar Battle Lord",
		[1160] = "Captain Halyndor",
		[7397] = "Earthen Stonecarver",
		[7901] = "Treasure Hunting Swashbuckler",
		[7206] = "Ancient Stone Keeper",
		[1150] = "River Crocolisk",
		[7309] = "Earthen Custodian",
		[327] = "Goldtooth",
		[2318] = "Argus Shadow Mage",
		[7246] = "Sandfury Shadowhunter",
		[575] = "Fire Elemental",
		[14523] = "Ulathek",
		[8902] = "Shadowforge Citizen",
		[6518] = "Tar Lurker",
		[7286] = "Zul'Farrak Zombie",
		[6514] = "Un'Goro Gorilla",
		[1144] = "Mosh'Ogg Witch Doctor",
		[5263] = "Mummified Atal'ai",
		[5271] = "Atal'ai Deathwalker",
		[7873] = "Razorfen Battleguard",
		[7334] = "Battle Boar Horror",
		[5295] = "Enraged Feral Scar",
		[1122] = "Unknown",
		[7358] = "Amnennar the Coldbringer",
		[594] = "Defias Henchman",
		[2927] = "Vicious Owlbeast",
		[5335] = "Hatecrest Screamer",
		[7175] = "Stonevault Ambusher",
		[7398] = "Stoneclaw Totem V <Packco>",
		[1020] = "Mottled Raptor",
		[7414] = "Mana Spring Totem II <Grizwöld>",
		[6426] = "Anguished Dead",
		[686] = "Lashtail Raptor",
		[7438] = "Winterfall Ursa",
		[7441] = "Winterfall Totemic",
		[7135] = "Infernal Bodyguard",
		[12203] = "Landslide",
		[5423] = "Scorpid Tail Lasher",
		[5431] = "Surf Glider",
		[7486] = "Grace of Air Totem <Packco>",
		[8906] = "Ragereaver Golem",
		[3752] = "Xavian Rogue",
		[5331] = "Hatecrest Warrior",
		[8891] = "Anvilrage Guardsman",
		[8907] = "Wrath Hammer Construct",
		[8923] = "Panzor the Invincible",
		[6350] = "Makrinni Razorclaw",
		[9462] = "Chieftain Bloodmaw",
		[3780] = "Shadethicket Moss Eater",
		[3784] = "Shadethicket Bark Ripper",
		[3804] = "Forsaken Intruder",
		[9019] = "Emperor Dagran Thaurissan",
		[9035] = "Anger'rel",
		[315] = "Stalvan Mistmantle",
		[7606] = "Oro Eyegouge",
		[3808] = "Forsaken Dark Stalker",
		[11789] = "Deep Borer",
		[5287] = "Longtooth Howler",
		[7333] = "Withered Battle Boar",
		[3824] = "Wolf",
		[9163] = "Diemetradon",
		[5615] = "Wastewander Rogue",
		[5623] = "Wastewander Assassin",
		[7342] = "Skeletal Summoner",
		[641] = "Goblin Woodcarver",
		[5647] = "Sandfury Firecaller",
		[7857] = "Southsea Dock Worker",
		[4151] = "Saltstone Crystalhide",
		[7271] = "Witch Doctor Zum'rah",
		[7726] = "Grimtotem Naturalist",
		[4663] = "Burning Blade Augur",
		[2351] = "Gray Bear",
		[639] = "Edwin VanCleef",
		[5711] = "Ogom the Wretched",
		[5719] = "Morphaz",
		[3121] = "peepoHappy",
		[8337] = "Dark Iron Steelshifter",
		[3912] = "Stoneclaw Totem III <Tzkalzuk>",
		[3928] = "Rotting Slime",
		[3904] = "Searing Totem IV",
		[3908] = "Healing Stream Totem IV <Tzkalzuk>",
		[9499] = "Plugger Spazzring",
		[11562] = "Drysnap Crawler",
		[11578] = "Whirlwind Shredder",
		[7846] = "Teremus the Devourer",
		[5807] = "Nastia",
		[1096] = "Venture Co. Geologist",
		[3755] = "Xavian Felsworn",
		[4292] = "Scarlet Protector",
		[5839] = "Dark Iron Geologist",
		[5855] = "Magma Elemental",
		[7902] = "Treasure Hunting Buccaneer",
		[9543] = "Ribbly Screwspigot",
		[9547] = "Guzzling Patron",
		[580] = "Redridge Drudger",
		[2242] = "Syndicate Spy",
		[11786] = "Ambereye Reaver",
		[3976] = "Scarlet Commander Mograine",
		[588] = "Bloodscalp Scout",
		[531] = "Skeletal Fiend",
		[3988] = "Venture Co. Operator",
		[8257] = "Oozeling <Saturated Ooze>",
		[2251] = "Giant Yeti",
		[5239] = "Gordunni Mage-Lord",
		[3770] = "Bleakheart Shadowstalker",
		[300] = "Zzarc' Vul",
		[4012] = "Pridewing Wyvern",
		[518] = "Yowler",
		[5991] = "Redstone Crystalhide",
		[1032] = "Black Ooze",
		[1120] = "Frostmane Troll",
		[1022] = "Mottled Scytheclaw",
		[4036] = "Rogue Flame Spirit",
		[548] = "Murloc Minor Tidecaller",
		[4044] = "Blackened Basilisk",
		[10043] = "Ribbly's Crony",
		[5243] = "Cursed Atal'ai",
		[1082] = "Sawtooth Crocolisk",
		[2208] = "Greymist Tidehunter",
		[4676] = "Lesser Infernal",
		[6170] = "Gutspill",
		[12218] = "Vile Larva",
		[4696] = "Scorpashi Snapper",
		[6111] = "Fire Nova Totem III <Trucc>",
		[1175] = "Tunnel Rat Digger",
		[1023] = "Mottled Razormaw",
		[1025] = "Bluegill Puddlejumper",
		[4096] = "Galak Windchaser",
		[1027] = "Bluegill Warrior",
		[1039] = "Fen Dweller",
		[4120] = "Thundering Boulderkin",
		[1057] = "Dragonmaw Bonewarder",
		[1035] = "Dragonmaw Swamprunner",
		[6551] = "Gorishi Wasp",
		[6199] = "Blood Elf Reclaimer",
		[4640] = "Magram Wrangler",
		[4632] = "Kolkar Centaur",
		[2089] = "Giant Wetlands Crocolisk",
		[1157] = "Cursed Sailor",
		[4304] = "Scarlet Tracking Hound",
		[1051] = "Dark Iron Dwarf",
		[2241] = "Syndicate Thief",
		[8876] = "Sandfury Acolyte",
		[8444] = "Trade Master Kovic",
		[1059] = "Ana'thek the Cruel",
		[1131] = "Winter Wolf",
		[2237] = "Moonstalker Sire",
		[1065] = "Riverpaw Shaman",
		[8908] = "Molten War Golem",
		[6519] = "Tar Lord",
		[4280] = "Scarlet Preserver",
		[4288] = "Scarlet Beastmaster",
		[4296] = "Scarlet Adept",
		[6351] = "Storm Bay Oracle",
		[8956] = "Angerclaw Bear",
		[2253] = "Crushridge Brute",
		[1083] = "Murloc Shorestriker",
		[1085] = "Elder Stranglethorn Tiger",
		[4344] = "Mottled Drywallow Crocolisk",
		[4352] = "Bloodfen Screecher",
		[4360] = "Mirefin Warrior",
		[2185] = "Darkshore Thresher",
		[1095] = "Venture Co. Workboss",
		[1097] = "Venture Co. Mechanic",
		[4392] = "Corrosive Swamp Ooze",
		[2201] = "Greymist Raider",
		[6559] = "Glutinous Ooze",
		[4416] = "Defias Strip Miner",
		[9036] = "Vile'rel",
		[1109] = "Fleshripper",
		[6487] = "Arcanist Doan",
		[8892] = "Anvilrage Footman",
		[6503] = "Spiked Stegodon",
		[1117] = "Rockjaw Bonesnapper",
		[1119] = "Hammerspine",
		[6527] = "Tar Creeper",
		[2245] = "Syndicate Saboteur",
		[13082] = "Milton Beats",
		[4504] = "Frostmaw",
		[4512] = "Munchausen",
		[2261] = "Syndicate Watchman",
		[6575] = "Scarlet Trainee",
		[1135] = "Wendigo",
		[2257] = "Mug'thol",
		[285] = "Murloc",
		[2249] = "Ferocious Yeti",
		[1123] = "Frostmane Headhunter",
		[1121] = "Frostmane Snowstrider",
		[9164] = "Elder Diemetradon",
		[1115] = "Rockjaw Skullthumper",
		[1151] = "Saltwater Crocolisk",
		[12922] = "Imp Minion",
		[4376] = "Darkmist Spider",
		[4624] = "Booty Bay Bruiser",
		[1159] = "First Mate Snellig",
		[1161] = "Stonesplinter Trogg",
		[1163] = "Stonesplinter Skullthumper",
		[4656] = "Maraudine Mauler",
		[4664] = "Burning Blade Reaver",
		[4672] = "Hatefury Felsworn",
		[4680] = "Doomwarder Captain",
		[1173] = "Tunnel Rat Scout",
		[2349] = "Giant Moss Creeper",
		[1177] = "Tunnel Rat Surveyor",
		[4712] = "Slitherblade Sorceress",
		[0] = "Nocato",
		[4728] = "Gritjaw Basilisk",
		[2369] = "Daggerspine Shorehunter",
		[2373] = "Mudsnout Shaman",
		[2377] = "Torn Fin Tidehunter",
		[11563] = "Drysnap Pincer",
		[2385] = "Leroy",
		[1195] = "Forest Lurker",
		[1197] = "Stonesplinter Shaman",
		[1199] = "Juvenile Snow Leopard",
		[1201] = "Snow Leopard",
		[2405] = "Tarren Mill Deathguard",
		[7153] = "Deadwood Warrior",
		[525] = "Mangy Wolf",
		[2417] = "Grel'borg the Miser",
		[1211] = "Leper Gnome",
		[4848] = "Shadowforge Darkcaster",
		[1024] = "Bluegill Murloc",
		[2433] = "Helcular's Remains",
		[11787] = "Rock Borer",
		[4140] = "Scorpid Reaver",
		[3450] = "Defias Companion",
		[456] = "Murloc Minor Oracle",
		[5274] = "Ironfur Patriarch",
		[12977] = "Kolkar Ambusher",
		[7157] = "Deadwood Avenger",
		[1124] = "Frostmane Shadowcaster",
		[7209] = "Unknown <Obsidian Sentinel>",
		[6194] = "Spitelash Serpent Guard",
		[6066] = "Earthgrab Totem <Lost One Seer>",
		[9916] = "Jarquia",
		[97] = "Riverpaw Runt",
		[7023] = "Obsidian Sentinel",
		[5430] = "Searing Roc",
		[5235] = "Fungal Ooze",
		[1251] = "Splinter Fist Firemonger",
		[2256] = "Crushridge Enforcer",
		[314] = "Eliza",
		[7345] = "Splinterbone Captain",
		[1259] = "Gobbler",
		[7042] = "Flamescale Dragonspawn",
		[866] = "Stonard Grunt",
		[5056] = "Keely",
		[7111] = "Jadefire Hellcaller",
		[2537] = "Chucky \"Ten Thumbs\"",
		[1271] = "Old Icebeard",
		[12219] = "Barbed Lasher",
		[2549] = "Garr Salthoof",
		[2553] = "Witherbark Shadowcaster",
		[2557] = "Witherbark Shadow Hunter",
		[2561] = "Highland Fleshstalker",
		[2565] = "Giant Plains Creeper",
		[7608] = "Murta Grimgut",
		[2573] = "Drywhisker Surveyor",
		[1114] = "Jungle Thunderer",
		[3251] = "Silithid Grub",
		[8904] = "Shadowforge Senator",
		[2589] = "Syndicate Mercenary",
		[8317] = "Atal'ai Deathwalker's Spirit <Atal'ai Deathwalker>",
		[1128] = "Young Black Bear",
		[7247] = "Sandfury Soul Eater",
		[7273] = "Gahz'rilla",
		[9684] = "Lar'korwi",
		[5224] = "Murk Slitherer",
		[5232] = "Gordunni Brute",
		[5240] = "Gordunni Warlock",
		[8566] = "Dark Iron Lookout",
		[5256] = "Atal'ai Warrior",
		[8477] = "Skeletal Servant",
		[5272] = "Grizzled Ironfur Bear",
		[7327] = "Withered Warrior",
		[7335] = "Death's Head Geomancer",
		[7343] = "Splinterbone Skeleton",
		[7351] = "Tomb Reaver",
		[3261] = "Bristleback Thornweaver",
		[4007] = "Deepmoss Venomspitter",
		[1172] = "Tunnel Rat Vermin",
		[5336] = "Hatecrest Sorceress",
		[8637] = "Dark Iron Watchman",
		[2368] = "Daggerspine Shorestalker",
		[5360] = "Zapped Deep Strider",
		[7415] = "Mana Spring Totem III <Daps>",
		[5225] = "Murk Spitter",
		[11686] = "Ghostly Raider",
		[3107] = "Ferdinand",
		[2701] = "Dustbelcher Ogre",
		[38] = "Defias Thug",
		[458] = "Murloc Hunter",
		[9696] = "Lobo",
		[2717] = "Dustbelcher Mauler",
		[2721] = "Forsaken Bodyguard",
		[2725] = "Scalding Whelp",
		[2729] = "Elder Crag Coyote",
		[8877] = "Sandfury Zealot",
		[5472] = "Unknown",
		[5273] = "Atal'ai High Priest",
		[13019] = "Burning Blade Seer",
		[12240] = "Kolk",
		[8957] = "Angerclaw Grizzly",
		[345] = "Bellygrub",
		[2761] = "Cresting Exile",
		[10080] = "Sandarr Dunereaver",
		[7883] = "Andre Firebeard",
		[9037] = "Gloom'rel",
		[2546] = "Fleet Master Firallon",
		[2578] = "Young Mesa Buzzard",
		[2566] = "Boulderfist Brute",
		[8961] = "Felpaw Ravager",
		[1397] = "Frostmane Seer",
		[7109] = "Jadefire Felsworn",
		[5600] = "Khan Dez'hepah",
		[9165] = "Fledgling Pterrordax",
		[5616] = "Wastewander Thief",
		[5462] = "Sea Spray",
		[7348] = "Thorn Eater Ghoul",
		[13323] = "Subterranean Diemetradon",
		[5648] = "Sandfury Shadowcaster",
		[2829] = "Starving Buzzard",
		[1417] = "Young Wetlands Crocolisk",
		[48] = "Skeletal Warrior",
		[7727] = "Grimtotem Shaman",
		[9454] = "Xavathras",
		[2714] = "Forsaken Courier",
		[2718] = "Dustbelcher Shaman",
		[5712] = "Zolo",
		[5720] = "Weaver",
		[6547] = "Suffering Victim",
		[3789] = "Terrowulf Fleshripper",
		[3825] = "Ghostpaw Alpha",
		[1188] = "Grizzled Black Bear",
		[11516] = "Timbermaw Warder",
		[5459] = "Centipaar Tunneler",
		[687] = "Jungle Stalker",
		[2893] = "Stonevault Bonesnapper",
		[7356] = "Plaguemaw the Rotting",
		[7847] = "Caliph Scorpidsting",
		[7855] = "Southsea Pirate",
		[732] = "Murloc Lurker",
		[1186] = "Elder Black Bear",
		[3989] = "Venture Co. Logger",
		[5840] = "Dark Iron Steamsmith",
		[7268] = "Sandfury Guardian",
		[5856] = "Glassweb Spider",
		[114] = "Harvest Watcher",
		[590] = "Defias Looter",
		[688] = "Stone Maw Basilisk",
		[2945] = "Murdaloc",
		[11788] = "Rock Worm",
		[6188] = "Timbermaw Shaman",
		[2926] = "Wolf",
		[5852] = "Inferno Elemental",
		[4849] = "Shadowforge Archaeologist",
		[4857] = "Stone Keeper",
		[2003] = "Shadow Sprite",
		[1489] = "Zanzil Hunter",
		[1491] = "Zanzil Naga",
		[539] = "Pygmy Venom Web Spider",
		[939] = "Kurzen Elite",
		[5984] = "Starving Snickerfang",
		[5992] = "Ashmane Boar",
		[7115] = "Jaedenar Adept",
		[4506] = "Bloodsail Swabby",
		[6349] = "Great Wavethrasher",
		[7155] = "Deadwood Pathfinder",
		[2992] = "Healing Ward V <Lost One Seer>",
		[11561] = "Undead Ravager",
		[8095] = "Sul'lithuz Sandcrawler",
		[13743] = "Corrupt Force of Nature",
		[5349] = "Wind Serpent",
		[5425] = "Starving Blisterpaw",
		[8127] = "Antu'sul",
		[834] = "Coyote",
		[428] = "Dire Condor",
		[433] = "Shadowhide Gnoll",
		[12220] = "Constrictor Vine",
		[1010] = "Mosshide Fenrunner",
	},
	["death_recap"] = {
		["enabled"] = true,
		["show_segments"] = false,
		["show_life_percent"] = false,
		["relevance_time"] = 7,
	},
	["spell_pool"] = {
		4, -- [1]
		9, -- [2]
		[0] = 4,
		["Seal of the Crusader"] = 2,
		["Cleanse"] = 2,
		["Shadow Word: Pain"] = 5,
		["Judgement of Command"] = 2,
		["Judgement of the Crusader"] = 2,
		["Backhand"] = "Defias Convict",
		["Berserker Rage"] = "Brockhoof",
		["Manifest Spirit"] = 2,
		["Frostbite"] = 8,
		["Fiery Weapon"] = 4,
		["Heroic Strike"] = 1,
		["Strike"] = "Dextren Ward",
		["Raptor Strike"] = 3,
		["Blind"] = 4,
		["Food"] = 8,
		["Dampen Magic"] = 8,
		["Sinister Strike"] = 4,
		["Acid Splash"] = "Cookie",
		["Track Humanoids"] = 3,
		["Ice Block"] = "Dærkness",
		["Chestnut Mare"] = 1,
		["Sand Storms"] = "Ancient Stone Keeper",
		["Stealth"] = 4,
		["Restore Mana"] = 3,
		["Fireball"] = 8,
		["Arcane Intellect"] = 8,
		["Stoneform"] = 3,
		["Fire Blast"] = 8,
		["Explosive Shot"] = "Doomforge Dragoon",
		["Blood Leech"] = "Sandfury Blood Drinker",
		["Scorpid Sting"] = 3,
		["Lesser Armor"] = 2,
		["Hex"] = "Sandfury Shadowhunter",
		["Toxic Volley"] = "Noxxion",
		["Hellfire Effect"] = 9,
		["Charge Stun"] = 1,
		["Furor"] = 11,
		["Wing Clip"] = 3,
		["Fire Shield Effect IV"] = "Earthen Sculptor",
		["Detect Lesser Invisibility"] = 9,
		["Summon Imp"] = 9,
		["Deep Sleep"] = "High Inquisitor Whitemane",
		["Poisoned Harpoon"] = "Captain Greenskin",
		["Poison"] = "Tomb Fiend",
		["Serpent Sting"] = 3,
		["Challenging Shout"] = 1,
		["Spirit"] = 5,
		["Minor Mana Potion"] = 5,
		["Frost Shock"] = "Shmore",
		["Lay on Hands"] = 2,
		["Crystal Charge"] = 1,
		["War Stomp"] = "Ironaya",
		["Travel Form"] = 11,
		["Nature's Swiftness"] = 11,
		["Cold Eye"] = 1,
		["Concussion Blow"] = 1,
		["Touch of Weakness"] = "Begging",
		["Reflection"] = "Jadespine Basilisk",
		["Curse of Tuten'kash"] = "Tuten'kash",
		["Seal of Command"] = 2,
		["Detonation"] = "Arcanist Doan",
		["Fireball Volley"] = "Fireguard Destroyer",
		["Improved Concussive Shot"] = 3,
		["Lesser Heal"] = 5,
		["Deep Wound"] = 1,
		["Searing Pain"] = 9,
		["Heal"] = 5,
		["Summon Warhorse"] = 2,
		["Shadowmeld"] = 11,
		["Earthen Guards Destroyed"] = "Earthen Hallshaper",
		["Blessing of Light"] = 2,
		["Web"] = "Tomb Reaver",
		["Charge"] = 1,
		["Conjure Mana Jade"] = 8,
		["Larva Goo"] = "Vile Larva",
		["Static Barrier"] = 3,
		["Seal of Wisdom"] = 2,
		["Brown Ram"] = 5,
		["Starfire"] = 11,
		["Fire Nova Totem"] = "Antu'sul",
		["Summon Frost Spectres"] = "Amnennar the Coldbringer",
		["Conjure Food"] = 8,
		["Living Flames"] = "Living Flame",
		["Concussive Shot"] = 3,
		["Shield Wall"] = 1,
		["Bestial Wrath"] = 3,
		["Summon Remote-Controlled Golem"] = "Goblin Engineer",
		["Disarm"] = 1,
		["Increased Stamina"] = 11,
		["Holy Shock"] = 2,
		["Argent Dawn Commission"] = 8,
		["Thunder Clap"] = 1,
		["Recently Bandaged"] = 4,
		["Diseased Shot"] = "Withered Spearhide",
		["Frost Arrow"] = 3,
		["Earthbind Totem"] = "Shmore",
		["Soul Bite"] = "Sandfury Soul Eater",
		["Rake"] = 11,
		["Arcane Bubble"] = "Arcanist Doan",
		["Life Tap"] = 9,
		["Amplify Damage"] = "Twilight's Hammer Ambassador",
		["Barkskin"] = 11,
		["Knockdown"] = "Cavern Lurker",
		["Sunder Armor"] = 1,
		["Enrage"] = "Targorr the Dread",
		["Garrote"] = 4,
		["Whirlwind"] = 1,
		["Arcane Bolt"] = "Doomforge Arcanasmith",
		["Silence"] = "Arcanist Doan",
		["Sharpen Blade II"] = 2,
		["Judgement"] = "Gar",
		["Arcing Smash"] = "Ironaya",
		["Thorn Volley"] = "Barbed Lasher",
		["Purify"] = 2,
		["Summon Atal'ai Skeleton"] = "Atal'ai Totem <Zolo>",
		["Blessing of Sacrifice"] = 2,
		["Summon Noxxion's Spawns"] = "Noxxion",
		["Greater Healthstone"] = 9,
		["Place Ghost Magnet"] = 2,
		["Immolation Trap Effect"] = 3,
		["Explosive Trap"] = 3,
		["Prowl"] = 11,
		["Deadly Poison III"] = 4,
		["Vengeance"] = 2,
		["Green Mechanostrider"] = 4,
		["Growl"] = 11,
		["Elixir of the Giants"] = 1,
		["Suppression"] = "Nightmare Suppressor",
		["Armor"] = 1,
		["Warder Despawn"] = "Antu'sul",
		["Deadly Poison II"] = 4,
		["Amplify Curse"] = 9,
		["Skinning"] = 8,
		["Cookie's Cooking"] = "Cookie",
		["Quick Flame Ward"] = "Defias Evoker",
		["Gahz'rilla Slam"] = "Gahz'rilla",
		["Concentration Aura"] = 2,
		["Shock"] = "Golem Lord Argelmach",
		["Dash"] = 11,
		["Bear Form"] = 11,
		["Bash"] = 11,
		["Flame Cannon"] = "Wrath Hammer Construct",
		["Holy Strength"] = 1,
		["Bloodrage"] = 1,
		["Shield Spike"] = "Loro",
		["Mage Armor"] = 8,
		["Mark of the Wild"] = 11,
		["Gift of Arthas"] = 1,
		["Ravage"] = 11,
		["Noxious Catalyst"] = "Noxxious Scion",
		["Lightning Shield"] = "Plue",
		["Shadowburn"] = 9,
		["Counterspell"] = 8,
		["Sword Specialization"] = "Trees",
		["Illumination"] = 2,
		["Instant Poison"] = 4,
		["Shadowform"] = 5,
		["Cure Poison"] = 11,
		["Consecration"] = 2,
		["Berserker Stance"] = "Brockhoof",
		["Fear"] = 9,
		["Create Soulstone (Lesser)"] = 9,
		["Greater Agility"] = 4,
		["Chilled"] = 8,
		["Crystal Force"] = 11,
		["Shoot Crossbow"] = 1,
		["VanCleef's Allies"] = "Edwin VanCleef",
		["Create Weegli's Barrel"] = "Weegli Blastfuse",
		["Slam"] = 1,
		["Lash of Pain"] = "Bronlissa <Zmân>",
		["Unbridled Wrath"] = 1,
		["Agility"] = 1,
		["Rupture"] = 4,
		["Summoned Demon"] = "Unknown",
		["Goblin Dragon Gun"] = "Tinkerer Gizlock",
		["Lesser Intellect"] = 5,
		["Abolish Poison"] = 11,
		["Clearcasting"] = 8,
		["Aspect of the Pack"] = "Whitegel",
		["Summon Shadowcaster"] = "Skeletal Summoner",
		["Reconstruct"] = "Earthen Hallshaper",
		["Strike of the Scorpok"] = 4,
		["Weakened Soul"] = 5,
		["Kick - Silenced"] = 4,
		["Crystalline Slumber"] = "Jadespine Basilisk",
		["Intimidating Shout"] = 1,
		["Expose Armor"] = 4,
		["Haste"] = 11,
		["Swipe"] = 11,
		["Voidwalker"] = "Defias Conjurer",
		["Frailty"] = "Zul'Lor",
		["Instant Poison II"] = 4,
		["Hellfire"] = 9,
		["Retribution Aura"] = "Aegeus",
		["Shadow Bolt"] = 9,
		["Force of Will"] = 11,
		["Sanctity Aura"] = 2,
		["Wide Slash"] = "Chief Ukorz Sandscalp",
		["Seal of Righteousness"] = "Gar",
		["Veil of Shadow"] = "Anguished Dead",
		["Mana Regeneration"] = "Maxfrost",
		["Torment"] = "Thoggron",
		["Polymorph"] = 8,
		["Remove Curse"] = 11,
		["Dust Field"] = "Princess Theradras",
		["Multi-Shot"] = 3,
		["Puncture"] = "Razorlash",
		["Divine Protection"] = 2,
		["Forked Lighting"] = 3,
		["Tiger's Fury"] = 11,
		["Drain Soul"] = 9,
		["Strength"] = 4,
		["Curse of Blood"] = "High Inquisitor Fairbanks",
		["Restore Energy"] = 4,
		["Claw"] = 11,
		["Will of the Forsaken"] = "Ashhaul",
		["Flare"] = 3,
		["Conjure Water"] = 8,
		["Summon Atal'ai Deathwalker's Spirit"] = "Atal'ai Deathwalker",
		["Faerie Fire (Feral)"] = 11,
		["Holy Wrath"] = 2,
		["Red Mechanostrider"] = 1,
		["Vindication"] = 2,
		["Heavy Linen Bandage"] = 4,
		["Intercept Stun"] = "Tbaggins",
		["Sap"] = 4,
		["Contagion of Rot"] = 4,
		["Rejuvenation"] = 11,
		["Rain of Fire"] = 9,
		["Vanish"] = 4,
		["Drunken Rage"] = "Hurley Blackbreath",
		["Curse of Tongues"] = "Avatar of Hakkar",
		["Chain Bolt"] = "Grimlok",
		["Earthbind"] = "Earthbind Totem <Shmore>",
		["Mana Spring"] = "Mana Spring Totem II",
		["Slice and Dice"] = 4,
		["Dazed"] = 3,
		["Death Wish"] = 1,
		["Instant Poison III"] = "Habibisauce",
		["Greater Heal"] = 5,
		["Blessing of Protection"] = 2,
		["Banish"] = "Plugger Spazzring",
		["Replenish Mana"] = 8,
		["Boulder"] = "Primordial Behemoth",
		["Fire"] = "Environment (Fire)",
		["Fire Shield III"] = "Scarlet Evoker",
		["Instant Poison IV"] = 4,
		["Execute"] = 1,
		["Rend"] = 1,
		["Arcane Explosion"] = 8,
		["Blade Flurry"] = 4,
		["Rough Dynamite"] = 4,
		["Winter's Chill"] = 8,
		["Ravenous Claw"] = "Thorn Eater Ghoul",
		["Noxious Cloud"] = "Noxious Slime",
		["Arcane Missiles"] = 8,
		["Bloodlust"] = "Hamhock",
		["Flip Out"] = 8,
		["Aspect of the Cheetah"] = 3,
		["!Melee"] = 4,
		["Ignite"] = 8,
		["Summon Living Flame"] = "Defias Magician",
		["Attack"] = "Searing Totem II <Aggrosaint>",
		["Water Breathing"] = 4,
		["Flame Breath"] = "Hurley Blackbreath",
		["Sweeping Strikes"] = 1,
		["Righteous Fury"] = 2,
		["Eject Sneed"] = "Sneed's Shredder",
		["Fiery Blaze"] = 1,
		["Pounce"] = 11,
		["Redoubt"] = 2,
		["Thrash"] = "Mr. Smite",
		["Earthgrab Totem"] = "Antu'sul",
		["Mana Shield"] = 8,
		["Escape Artist"] = 4,
		["Bomb"] = "Weegli Blastfuse",
		["Splintered Obsidian"] = "Obsidian Sentinel",
		["Burst of Knowledge"] = 5,
		["Abolish Disease"] = 5,
		["Regeneration"] = 2,
		["Scare Beast"] = 3,
		["Restoration"] = 3,
		["Devotion Aura"] = 2,
		["Aqua Jet"] = "Thessala Hydra",
		["Summon Hakkar"] = "Shade of Hakkar",
		["Fiery Burst"] = "Magmus",
		["Melt Ore"] = "Goblin Craftsman",
		["Intimidation"] = 3,
		["Adrenaline Rush"] = 4,
		["Stamina"] = 2,
		["Crusader Strike"] = "Scarlet Gallant",
		["Inferno Shell"] = "Stonevault Flameweaver",
		["Axe Toss"] = "Goblin Woodcarver",
		["Fishliver Oil"] = 1,
		["Withered Touch"] = "Withered Quilguard",
		["Piercing Howl"] = 1,
		["Shadow Vulnerability"] = 9,
		["Stun"] = 2,
		["Demon Skin"] = 9,
		["Shield Bash"] = 1,
		["Holy Strike"] = "Scarlet Champion",
		["Avatar of Flame"] = "Emperor Dagran Thaurissan",
		["Shoot Bow"] = 1,
		["Inspiration"] = 5,
		["Blessing of Salvation"] = 2,
		["Blast Wave"] = "Caduco",
		["Heavy Dynamite"] = 1,
		["Flames"] = "Emperor Dagran Thaurissan",
		["Tremor Totem"] = "Grimlok",
		["Piercing Shot"] = "Defias Taskmaster",
		["Dire Bear Form"] = 11,
		["Blue Mechanostrider"] = 8,
		["Arcane Shot"] = 3,
		["Ritual of Summoning"] = 9,
		["Rough Copper Bomb"] = 1,
		["Brown Horse"] = 1,
		["Knock Away"] = "Ironaya",
		["Fire Ward"] = 8,
		["Curse of the Elements"] = 9,
		["Recklessness"] = 1,
		["Hamstring"] = 1,
		["Soul Siphon"] = 9,
		["Distract"] = 4,
		["Mind-numbing Poison"] = 4,
		["Amplify Flames"] = "Galgann Firehammer",
		["Spotted Frostsaber"] = 3,
		["Bloodsail Companion"] = "Defias Pirate",
		["Holy Light"] = 2,
		["Sonic Burst"] = "Shrike Bat",
		["Healing Wave"] = "Stonevault Oracle",
		["Blizzard"] = 8,
		["Blood Craze"] = 1,
		["Ground Tremor"] = "Stone Steward",
		["Disease Cloud"] = "Glutton",
		["Soulstone Resurrection"] = 9,
		["Devouring Plague"] = "Skelesteve",
		["Mana Rejuvenation"] = 11,
		["Acid Spit"] = "Rock Worm",
		["Healthstone"] = 9,
		["Improved Blocking"] = "Scarlet Soldier",
		["Mind Flay"] = "Doubleboo",
		["Smite Slam"] = "Mr. Smite",
		["Cooked Deviate Fish"] = 1,
		["Psychic Scream"] = 5,
		["Blink"] = 8,
		["Heavy Wool Bandage"] = 4,
		["Power Word: Fortitude"] = 5,
		["Fear Ward"] = 5,
		["Resurrection"] = 5,
		["Arcane Brilliance"] = 8,
		["Wandering Plague"] = 4,
		["Defensive Stance"] = 1,
		["Health II"] = 11,
		["Inner Fire"] = 5,
		["Rip"] = 11,
		["Silk Bandage"] = 4,
		["Elixir of the Mongoose"] = 1,
		["Attach Medallion to Shaft"] = 8,
		["Seal of Justice"] = 2,
		["Throw"] = 4,
		["Rapid Fire"] = 3,
		["Frost Shot"] = "Weapon Technician",
		["Mortal Strike"] = 1,
		["Bone Armor"] = "Death's Head Necromancer",
		["Rage of Ages"] = 4,
		["Spirit of Boar"] = 4,
		["Yaaarrrr"] = 1,
		["Frost Trap"] = 3,
		["Frost Blast"] = 1,
		["Create Healthstone (Lesser)"] = 9,
		["Challenging Roar"] = 11,
		["Disengage"] = 3,
		["Replenish Mana II"] = 8,
		["Increased Agility"] = 1,
		["Dire Growl"] = "Bloodhound",
		["Fire Shield"] = 4,
		["Hammer of Wrath"] = 2,
		["Conjure Mana Citrine"] = 8,
		["Ice Armor"] = 8,
		["Deep Slumber"] = "Shade of Eranikus",
		["Flash of Light"] = 2,
		["Dispel Magic"] = 5,
		["Blessing of Wisdom"] = "Gar",
		["Hunter's Mark"] = 3,
		["Summon Oozeling"] = "Saturated Ooze",
		["Inner Focus"] = 5,
		["Thorns"] = 4,
		["Heavy Mageweave Bandage"] = 1,
		["Tranquility"] = 11,
		["Perception"] = 2,
		["Earth Shock"] = "Plue",
		["Shadow Shield"] = "Atal'ai High Priest",
		["Dark Offering"] = "Sandfury Soul Eater",
		["Flame Lash"] = "Shadowforge Geologist",
		["Gout of Flame"] = "Ironhand Guardian",
		["Explosive Trap Effect"] = 3,
		["Spore Cloud"] = "Spore Tree",
		["Acid of Hakkar"] = "Spawn of Hakkar",
		["Flamestrike"] = 8,
		["Pyroblast"] = 8,
		["White Ram"] = 5,
		["Frost Trap Aura"] = 3,
		["Trample"] = "Stone Keeper",
		["Strength of Stone"] = 1,
		["Heavy Silk Bandage"] = 9,
		["Speed"] = 1,
		["Hibernate"] = 11,
		["Flame Buffet"] = "Stonevault Geomancer",
		["Dragonbreath Chili"] = 1,
		["Poison Cloud"] = 1,
		["Thunderclap"] = "Splinterbone Centurion",
		["Sleep"] = "Scarlet Scryer",
		["Curse of Recklessness"] = 9,
		["Holy Fire"] = 5,
		["Evocation"] = 8,
		["Poison Shock"] = "Creeping Sludge",
		["Coarse Sharpening Stone"] = 2,
		["Shield Slam"] = "Kam Deepfury",
		["Judgement of Light"] = 1,
		["Call Pet"] = 3,
		["Battle Shout"] = 1,
		["Petrify"] = "Sul'lithuz Sandcrawler",
		["Retaliation"] = 1,
		["Immolate"] = 9,
		["Rebirth"] = 11,
		["Scarlet Resurrection"] = "High Inquisitor Whitemane",
		["Sand Storm"] = "Unknown <Ancient Stone Keeper>",
		["Crystal Protection"] = 4,
		["Summon Illusionary Phantasm"] = "Haunting Phantasm",
		["Aimed Shot"] = 3,
		["Deadly Poison"] = 4,
		["Battle Command"] = "Defias Overseer",
		["Freezing Trap Effect"] = 3,
		["Gift of the Wild"] = 11,
		["Exploding Shot"] = "Scarlet Beastmaster",
		["Rhahk'Zor Slam"] = "Rhahk'Zor",
		["Forbearance"] = 2,
		["Summon Skeletal Servant"] = "Death's Head Necromancer",
		["Drain Life"] = 9,
		["Atal'ai Skeleton Totem"] = "Zolo",
		["Feint"] = 4,
		["Wing Flap"] = "Dreamscythe",
		["Well Fed"] = 8,
		["Spirit Tap"] = 5,
		["Intellect"] = 4,
		["Frost Nova"] = 8,
		["Vampiric Embrace"] = 5,
		["Drowning"] = "Environment (Drowning)",
		["Fade"] = 5,
		["Arcane Missile"] = 8,
		["Dark Pact"] = 9,
		["Turn Undead"] = 2,
		["Ward of Zum'rah"] = "Witch Doctor Zum'rah",
		["Leader of the Pack"] = 11,
		["Revenge"] = 1,
		["Minor Healthstone"] = "Doomlock",
		["Bloodthirst"] = 1,
		["Omen of Clarity"] = 11,
		["Feed Pet"] = 3,
		["Pierce Armor"] = "Defias Miner",
		["Hooked Net"] = "Veng",
		["Overpower"] = 1,
		["Lesser Strength"] = 1,
		["Chains of Ice"] = "Defias Wizard",
		["Blessing of Kings"] = 2,
		["Wild Regeneration"] = "Cavern Shambler",
		["Acid Breath"] = "Dreamscythe",
		["Blessing of Freedom"] = 2,
		["Ghost Costume"] = 1,
		["Phantom Strike"] = 4,
		["Nature's Grasp"] = 11,
		["Unending Breath"] = 9,
		["Summon Voidwalker"] = 9,
		["Repulsive Gaze"] = "Princess Theradras",
		["Winterfall Firewater"] = 4,
		["Thorns Aura"] = "Mijan",
		["Smite Stomp"] = "Mr. Smite",
		["Freeze Solid"] = "Gahz'rilla",
		["Ancestral Spirit"] = "Melstar",
		["Cone of Cold"] = 8,
		["Taunt"] = 1,
		["Cripple"] = "Death's Head Necromancer",
		["Exorcism"] = 2,
		["Pinto Horse"] = 5,
		["Lava Spout Totem"] = "Stonevault Oracle",
		["Blood Pact"] = 9,
		["Fevered Plague"] = "Theka the Martyr",
		["Virulent Poison"] = "Tomb Reaver",
		["Slow"] = "Scarlet Sorcerer",
		["Fire Shield IV"] = "Earthen Sculptor",
		["Corrosive Poison"] = 1,
		["Second Wind"] = 5,
		["Judgement of Justice"] = 2,
		["Seal of Light"] = 2,
		["Goblin Land Mine"] = "Weegli Blastfuse",
		["Ice Barrier"] = "Mageboypapi",
		["Awaken Vault Warder"] = "Archaedas",
		["Summon Felsteed"] = 9,
		["Flame Spike"] = "Bloodmage Thalnos",
		["Chest Pains"] = 4,
		["Shield Block"] = 1,
		["Demoralizing Shout"] = 1,
		["Mighty Rage"] = 1,
		["Fire Nova"] = "Bloodmage Thalnos",
		["Aspect of the Monkey"] = 3,
		["Falling"] = "Environment (Falling)",
		["Shoot Gun"] = 1,
		["Cold Snap"] = 8,
		["Hearthstone"] = "Jincx",
		["Terrify"] = "Sneed's Shredder",
		["Bladestorm"] = 1,
		["Fire Shield Effect III"] = "Scarlet Evoker",
		["Shadow Trance"] = 9,
		["Divine Shield"] = 2,
		["Summon Theradrim Shardling"] = "Theradrim Guardian",
		["Cleave"] = "Goblin Woodcarver",
		["Minor Absorption"] = 1,
		["Auto Shot"] = 3,
		["Frostbolt"] = 8,
		["Summon Obsidian Shard"] = "Obsidian Sentinel",
		["Judgement of Wisdom"] = 2,
		["Eviscerate"] = 4,
		["Maul"] = 11,
		["Striped Nightsaber"] = 1,
		["Backstab"] = 4,
		["Furbolg Form"] = 2,
		["Sacrifice"] = 9,
		["!Autoshot"] = 3,
		["Create Healthstone (Minor)"] = 9,
		["Desperate Prayer"] = 5,
		["Siphon Life"] = 9,
		["Net"] = "Maraudos",
		["Gouge"] = 4,
		["Divine Spirit"] = 5,
		["Lightning Bolt"] = 1,
		["Cat Form"] = 11,
		["Phase Shift"] = "Karham <Jdrs>",
		["Health"] = 5,
		["Sprint"] = 4,
		["Power Word: Shield"] = 5,
		["Mocking Blow"] = 1,
		["Summon Shardlings"] = "Landslide",
		["Innervate"] = 11,
		["Burning Spirit"] = "Ambassador Flamelash",
		["Repentance"] = 2,
		["Corruption"] = 9,
		["Evasion"] = 4,
		["Uppercut"] = "Noxxion",
		["Demoralizing Roar"] = 11,
		["Awaken Earthen Guardians"] = "Archaedas",
		["Shield Specialization"] = "Scenicc",
		["M73 Frag Grenade"] = 4,
		["Moonfire"] = 11,
		["Sweeping Slam"] = "Atal'alarion",
		["Amplify Magic"] = 8,
		["Shadow Resistance Aura"] = 2,
		["Flame Shock"] = "Bloodmage Thalnos",
		["Flurry"] = 1,
		["Battle Stance"] = 1,
		["Feral Charge Effect"] = 11,
		["Shadow Protection"] = 5,
		["Cloaking"] = 5,
		["Focused Casting"] = 5,
		["Barbs"] = "Barbed Lasher",
		["Aspect of the Hawk"] = 3,
		["Volley"] = 3,
		["Riposte"] = 4,
		["Faded"] = "Defias Blackguard",
		["Frost Ward"] = 8,
		["Last Stand"] = 1,
		["Antu'sul's Minion"] = "Antu'sul",
		["Swiftmend"] = 11,
		["Pummel"] = "Defias Knuckleduster",
		["Power Infusion"] = 5,
		["Blessing of Might"] = 2,
		["Freezing Trap"] = 3,
		["Relentless Strikes Effect"] = 4,
		["Drain Mana"] = 9,
		["Self Destruct"] = "Stone Keeper",
		["Pick Pocket"] = 4,
		["Regrowth"] = 11,
		["Primal Fury"] = 11,
		["Scorch"] = 8,
		["Insect Swarm"] = 11,
		["Wound"] = 2,
		["Hurricane"] = 11,
		["Blackout"] = 5,
		["Curse of Weakness"] = 9,
		["Hammer of Justice"] = "Averroes",
		["Mithril Shield Spike"] = 1,
		["Firebolt"] = "Gelnar <Tinkerbeller>",
		["Holy Smite"] = "Scarlet Adept",
		["Icicle"] = "Gahz'rilla",
		["Divine Intervention"] = 2,
		["First Aid"] = 4,
		["Kidney Shot"] = 4,
		["Pounce Bleed"] = 11,
		["Black Stallion"] = 5,
		["Summon Voidwalkers"] = "Doom'rel",
		["Keeper's Sting"] = 3,
		["Drink"] = 8,
		["Cause Insanity"] = "Avatar of Hakkar",
		["Lesser Healthstone"] = 9,
		["Wrath"] = 11,
		["Create Soulstone (Minor)"] = 9,
		["Holy Nova"] = 5,
		["Mongoose Bite"] = 3,
		["Cheap Shot"] = 4,
		["Smoke Bomb"] = "Bazil Thredd",
		["Healing Wave of Antu'sul"] = "Antu'sul",
		["Plague Cloud"] = "Scarab",
		["Cure Disease"] = 5,
		["Kick"] = 4,
		["Shrink"] = "Grimlok",
		["Judgement of Righteousness"] = 2,
		["Lash"] = "Avatar of Hakkar",
		["Feral Charge"] = 11,
		["Venom Sting"] = "Venomlash Scorpid",
		["Chain Lightning"] = "Hamhock",
		["Rushing Charge"] = "Herod",
		["Frenzied Regeneration"] = 11,
		["Quick Shots"] = 3,
		["Noggenfogger Elixir"] = 3,
		["Putrid Stench"] = "Plaguemaw the Rotting",
		["Iron Grenade"] = 1,
		["Distracting Shot"] = 3,
		["Ambush"] = 4,
		["Shred"] = 11,
		["Mend Pet"] = 3,
		["Death Coil"] = 9,
		["Cower"] = 11,
		["Distracting Pain"] = "Sneed's Shredder",
		["Shoot"] = 9,
		["Nagmara's Vanish"] = "Private Rocknot",
		["Scatter Shot"] = "Brucewayner",
		["Striped Frostsaber"] = 3,
		["Smite"] = 5,
		["Demon Armor"] = 9,
		["Healing Ward"] = "Stonevault Oracle",
		["Bolt of Silk Cloth"] = 9,
		["Bite"] = "discombober <Manlydude>",
		["Healing Touch"] = 11,
		["Crippling Poison"] = 4,
		["Frost Armor"] = 8,
		["Mana Burn"] = "Scarlet Diviner",
		["Mark of the Chosen Effect"] = 11,
		["Faerie Fire"] = 11,
		["Web Spray"] = "Tuten'kash",
		["Remorseless"] = 4,
		["Mind Blast"] = 5,
		["Prayer of Healing"] = 5,
		["Entangling Roots"] = 11,
		["Ferocious Bite"] = 11,
		["Molten Metal"] = "Gilnid",
		["Create Scrying Bowl"] = 5,
		["Corrupt Forces of Nature"] = "Celebras the Cursed",
		["Feign Death"] = 3,
		["Flash Heal"] = 5,
		["Intercept"] = "Tbaggins",
		["Healing Potion"] = 4,
		["Divine Favor"] = 2,
		["Rumsey Rum Black Label"] = 1,
		["Renew"] = 5,
		["Arcane Blast"] = 1,
		["Infected Wound"] = "Defias Captive",
		["Poison Bolt"] = "Poison Sprite",
		["Shadow Bolt Volley"] = "Shadowforge Darkcaster",
		["Honorless Target"] = 11,
		["Harsh Winds"] = "Sand Storm <Ancient Stone Keeper>",
		["Cozy Fire"] = 1,
		["Flash Bomb"] = "Tinkerer Gizlock",
		["Revenge Stun"] = 1,
		["Curse of Agony"] = 9,
	},
	["encounter_spell_pool"] = {
	},
	["dungeon_data"] = {
	},
	["got_first_run"] = true,
	["report_pos"] = {
		1, -- [1]
		1, -- [2]
	},
	["latest_report_table"] = {
	},
	["__profiles"] = {
		["Thorddin-Faerlina"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["disable_mythic_dungeon"] = false,
				["use_animation_accel"] = true,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 17,
					["H"] = 17,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["maximize_method"] = 1,
				["border_size"] = 14,
				["fontshadow"] = false,
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["icon_border_texcoord"] = {
					["R"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["B"] = 0.921875,
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 62.8148498535156,
							["x"] = -864.259170532227,
							["w"] = 310.000213623047,
							["h"] = 158.000030517578,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["hide_in_combat_type"] = 1,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["bars_sort_direction"] = 1,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["clickthrough_rows"] = false,
					["ignore_mass_showhide"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["switch_damager"] = false,
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bars_inverted"] = false,
					["backdrop_texture"] = "Details Ground",
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["segmentType"] = 2,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["timeType"] = 1,
								["textXMod"] = 6,
								["textAlign"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
						},
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["bg_alpha"] = 0.183960914611816,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onleave"] = 1,
						["ignorebars"] = false,
						["iconstoo"] = true,
						["onenter"] = 1,
					},
					["show_statusbar"] = false,
					["__was_opened"] = true,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["menu_icons_size"] = 0.850000023841858,
					["hide_in_combat_alpha"] = 0,
					["grab_on_top"] = false,
					["micro_displays_side"] = 2,
					["libwindow"] = {
						["y"] = 62.8148345947266,
						["x"] = 0,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["bars_grow_direction"] = 1,
					["switch_all_roles_after_wipe"] = false,
					["skin_custom"] = "",
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["textR_outline_small"] = true,
						["textL_outline_small"] = true,
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["start_after_icon"] = true,
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["font_size"] = 16,
						["height"] = 21,
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["textR_bracket"] = "(",
						["backdrop"] = {
							["enabled"] = false,
							["texture"] = "Details BarBorder 2",
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["size"] = 12,
						},
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["use_spec_icons"] = true,
						["texture_custom"] = "",
						["texture_custom_file"] = "Interface\\",
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_enable_custom_text"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["textL_class_colors"] = false,
						["alpha"] = 1,
						["texture_background"] = "Details D'ictum (reverse)",
						["textR_class_colors"] = false,
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["percent_type"] = 1,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
					},
					["switch_damager_in_combat"] = false,
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["auto_current"] = true,
					["switch_tank_in_combat"] = false,
					["version"] = 3,
					["attribute_text"] = {
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
						["shadow"] = false,
						["side"] = 1,
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["enabled"] = true,
						["enable_custom_text"] = false,
						["text_size"] = 12,
					},
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 62.8148498535156,
							["x"] = -864.259170532227,
							["w"] = 310.000213623047,
							["h"] = 158.000030517578,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["switch_healer_in_combat"] = false,
					["switch_tank"] = false,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["stretch_button_side"] = 1,
					["desaturated_menu"] = false,
					["show_sidebars"] = false,
					["bg_b"] = 0.0941176470588235,
					["micro_displays_locked"] = true,
				}, -- [1]
			},
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["deadlog_limit"] = 16,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["instances_segments_locked"] = false,
			["instances_no_libwindow"] = false,
			["data_broker_text"] = "",
			["segments_amount"] = 18,
			["report_lines"] = 5,
			["clear_ungrouped"] = true,
			["font_sizes"] = {
				["menus"] = 10,
			},
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["total_abbreviation"] = 2,
			["report_schema"] = 1,
			["animation_speed_triggertravel"] = 5,
			["use_battleground_server_parser"] = false,
			["minimum_combat_time"] = 5,
			["use_scroll"] = false,
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["deadlog_events"] = 32,
			["overall_clear_logout"] = false,
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["trash_auto_remove"] = true,
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["numerical_system_symbols"] = "auto",
			["clear_graphic"] = true,
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["Ungroupplayer"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["Monk"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["Mage"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["Warlock"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["Hunter"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["Deathknight"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["Paladin"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["Rogue"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["Unknow"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["version"] = 1,
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["Druid"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["Warrior"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["Priest"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["Arena_yellow"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
			},
			["segments_auto_erase"] = 1,
			["options_group_edit"] = true,
			["broadcaster_enabled"] = false,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["hide"] = false,
				["minimapPos"] = 112.547846568358,
				["text_format"] = 3,
				["text_type"] = 1,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["deny_score_messages"] = false,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["default_bg_color"] = 0.0941,
			["standard_skin"] = false,
			["segments_amount_to_save"] = 18,
			["segments_panic_mode"] = false,
			["overall_flag"] = 16,
			["overall_clear_newboss"] = true,
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["player_details_window"] = {
				["scale"] = 1,
				["skin"] = "ElvUI",
				["bar_texture"] = "Skyline",
			},
			["numerical_system"] = 1,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["force_activity_time_pvp"] = true,
			["memory_threshold"] = 3,
			["pvp_as_group"] = true,
			["disable_reset_button"] = false,
			["animate_scroll"] = false,
			["death_tooltip_width"] = 350,
			["time_type"] = 2,
			["default_bg_alpha"] = 0.5,
			["time_type_original"] = 2,
		},
		["Hadren-Blaumeux"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["use_animation_accel"] = true,
				["disable_mythic_dungeon"] = false,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 13,
					["H"] = 13,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8697, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["icon_border_texcoord"] = {
					["B"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["R"] = 0.921875,
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["maximize_method"] = 1,
				["fontshadow"] = false,
				["border_size"] = 14,
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["time_type_original"] = 2,
			["default_bg_alpha"] = 0.5,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["time_type"] = 2,
			["death_tooltip_width"] = 350,
			["animate_scroll"] = false,
			["segments_amount"] = 18,
			["report_lines"] = 5,
			["clear_ungrouped"] = true,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["pvp_as_group"] = true,
			["force_activity_time_pvp"] = true,
			["numerical_system"] = 1,
			["player_details_window"] = {
				["scale"] = 1,
				["bar_texture"] = "Skyline",
				["skin"] = "ElvUI",
			},
			["minimum_combat_time"] = 5,
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["memory_threshold"] = 3,
			["deadlog_events"] = 32,
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["overall_flag"] = 16,
			["disable_alldisplays_window"] = false,
			["numerical_system_symbols"] = "auto",
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
			},
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["broadcaster_enabled"] = false,
			["clear_graphic"] = true,
			["total_abbreviation"] = 2,
			["segments_auto_erase"] = 1,
			["options_group_edit"] = true,
			["segments_amount_to_save"] = 18,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["text_type"] = 1,
				["minimapPos"] = 220,
				["text_format"] = 3,
				["hide"] = false,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["default_bg_color"] = 0.0941,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["deny_score_messages"] = false,
			["animation_speed_triggertravel"] = 5,
			["trash_auto_remove"] = true,
			["segments_panic_mode"] = false,
			["standard_skin"] = false,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["font_sizes"] = {
				["menus"] = 10,
			},
			["overall_clear_logout"] = false,
			["overall_clear_newboss"] = true,
			["report_schema"] = 1,
			["use_scroll"] = false,
			["use_battleground_server_parser"] = false,
			["disable_reset_button"] = false,
			["data_broker_text"] = "",
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 163.95068359375,
							["x"] = -603.518524169922,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["show_statusbar"] = false,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["bars_sort_direction"] = 1,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["textR_outline_small"] = true,
						["textL_outline_small"] = true,
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["start_after_icon"] = true,
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["font_size"] = 16,
						["height"] = 21,
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["use_spec_icons"] = true,
						["textR_bracket"] = "(",
						["texture_custom"] = "",
						["percent_type"] = 1,
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_enable_custom_text"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["texture_background"] = "Details D'ictum (reverse)",
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["textR_class_colors"] = false,
						["textL_class_colors"] = false,
						["alpha"] = 1,
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["texture_custom_file"] = "Interface\\",
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["backdrop"] = {
							["enabled"] = false,
							["size"] = 12,
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["texture"] = "Details BarBorder 2",
						},
					},
					["ignore_mass_showhide"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["switch_damager"] = false,
					["show_sidebars"] = false,
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bars_inverted"] = false,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textYMod"] = 1,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textYMod"] = 1,
								["segmentType"] = 2,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textYMod"] = 1,
								["textAlign"] = 0,
								["textXMod"] = 6,
								["textStyle"] = 2,
								["textFace"] = "Accidental Presidency",
								["timeType"] = 1,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
						},
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["bg_alpha"] = 0.183960914611816,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onenter"] = 1,
						["iconstoo"] = true,
						["ignorebars"] = false,
						["onleave"] = 1,
					},
					["clickthrough_rows"] = false,
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["bg_b"] = 0.0941176470588235,
					["hide_in_combat_alpha"] = 0,
					["micro_displays_side"] = 2,
					["stretch_button_side"] = 1,
					["libwindow"] = {
						["y"] = 163.950668334961,
						["x"] = 0,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["switch_all_roles_after_wipe"] = false,
					["bars_grow_direction"] = 1,
					["desaturated_menu"] = false,
					["switch_tank_in_combat"] = false,
					["switch_damager_in_combat"] = false,
					["grab_on_top"] = false,
					["backdrop_texture"] = "Details Ground",
					["attribute_text"] = {
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
						["shadow"] = false,
						["side"] = 1,
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["text_size"] = 12,
						["enable_custom_text"] = false,
						["enabled"] = true,
					},
					["auto_current"] = true,
					["version"] = 3,
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 163.95068359375,
							["x"] = -603.518524169922,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["skin_custom"] = "",
					["switch_tank"] = false,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["micro_displays_locked"] = true,
					["menu_icons_size"] = 0.850000023841858,
					["__was_opened"] = true,
					["hide_in_combat_type"] = 1,
				}, -- [1]
			},
			["instances_no_libwindow"] = false,
			["deadlog_limit"] = 16,
			["instances_segments_locked"] = false,
		},
		["Sneakythor-Faerlina"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["disable_mythic_dungeon"] = false,
				["use_animation_accel"] = true,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 17,
					["H"] = 17,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["maximize_method"] = 1,
				["border_size"] = 14,
				["fontshadow"] = false,
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["icon_border_texcoord"] = {
					["R"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["B"] = 0.921875,
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.0500000007450581,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["options_window"] = {
				["scale"] = 0.798126935958862,
			},
			["animation_speed_maxtravel"] = 3,
			["deadlog_limit"] = 16,
			["instances_segments_locked"] = false,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["instances_no_libwindow"] = false,
			["data_broker_text"] = "",
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 110.814544677734,
							["x"] = -755.616638183594,
							["w"] = 162.246948242188,
							["h"] = 110.197624206543,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["show_statusbar"] = false,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["micro_displays_locked"] = true,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["attribute_text"] = {
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
						["shadow"] = false,
						["side"] = 1,
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["enabled"] = true,
						["enable_custom_text"] = false,
						["text_size"] = 12,
					},
					["ignore_mass_showhide"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["desaturated_menu"] = false,
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bg_b"] = 0.0941176470588235,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textYMod"] = 1,
								["segmentType"] = 2,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textYMod"] = 1,
								["textXMod"] = 6,
								["textFace"] = "Accidental Presidency",
								["timeType"] = 1,
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
						},
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["bg_alpha"] = 0.183960914611816,
					["bars_inverted"] = false,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onleave"] = 1,
						["ignorebars"] = false,
						["iconstoo"] = true,
						["onenter"] = 1,
					},
					["skin_custom"] = "",
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["textR_outline_small"] = true,
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["backdrop"] = {
							["enabled"] = false,
							["texture"] = "Details BarBorder 2",
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["size"] = 12,
						},
						["font_size"] = 16,
						["texture_custom_file"] = "Interface\\",
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["height"] = 21,
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["textR_bracket"] = "(",
						["use_spec_icons"] = false,
						["textR_enable_custom_text"] = false,
						["start_after_icon"] = true,
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textL_outline_small"] = true,
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["texture_custom"] = "",
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["textL_class_colors"] = false,
						["alpha"] = 1,
						["texture_background"] = "Details D'ictum (reverse)",
						["textR_class_colors"] = false,
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["percent_type"] = 1,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
					},
					["switch_tank"] = false,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
						[2] = 2,
					},
					["stretch_button_side"] = 1,
					["hide_in_combat_alpha"] = 0,
					["switch_tank_in_combat"] = false,
					["switch_all_roles_after_wipe"] = false,
					["libwindow"] = {
						["y"] = 110.814529418945,
						["x"] = 182.519149780273,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["switch_damager"] = false,
					["__snapV"] = true,
					["bars_sort_direction"] = 1,
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["clickthrough_rows"] = false,
					["grab_on_top"] = false,
					["version"] = 3,
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["auto_current"] = true,
					["show_sidebars"] = false,
					["backdrop_texture"] = "Details Ground",
					["switch_damager_in_combat"] = false,
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 110.814544677734,
							["x"] = -755.616638183594,
							["w"] = 162.246948242188,
							["h"] = 110.197624206543,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["micro_displays_side"] = 2,
					["bars_grow_direction"] = 1,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["menu_icons_size"] = 0.850000023841858,
					["__was_opened"] = true,
					["hide_in_combat_type"] = 1,
					["__snapH"] = false,
				}, -- [1]
				{
					["__pos"] = {
						["normal"] = {
							["y"] = -6.93862915039063,
							["x"] = -755.616638183594,
							["w"] = 162.246948242188,
							["h"] = 85.3086776733399,
						},
						["solo"] = {
							["y"] = 1.99993896484375,
							["x"] = 1.00006103515625,
							["w"] = 300,
							["h"] = 300,
						},
					},
					["show_statusbar"] = false,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["micro_displays_locked"] = true,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["attribute_text"] = {
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
						["shadow"] = false,
						["side"] = 1,
						["text_size"] = 12,
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["enable_custom_text"] = false,
						["enabled"] = true,
					},
					["ignore_mass_showhide"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["desaturated_menu"] = false,
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bg_b"] = 0.0941176470588235,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["enabled"] = false,
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textYMod"] = 1,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textYMod"] = 1,
								["segmentType"] = 2,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textYMod"] = 1,
								["textFace"] = "Accidental Presidency",
								["textXMod"] = 6,
								["textStyle"] = 2,
								["timeType"] = 1,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
						},
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["bg_alpha"] = 0.183960914611816,
					["bars_inverted"] = false,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onenter"] = 1,
						["iconstoo"] = true,
						["ignorebars"] = false,
						["onleave"] = 1,
					},
					["skin_custom"] = "",
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textL_outline_small"] = true,
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["start_after_icon"] = true,
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
						["texture_custom_file"] = "Interface\\",
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["textR_bracket"] = "(",
						["font_size"] = 16,
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["backdrop"] = {
							["enabled"] = false,
							["size"] = 12,
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["texture"] = "Details BarBorder 2",
						},
						["use_spec_icons"] = false,
						["textR_enable_custom_text"] = false,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["textR_outline_small"] = true,
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["texture_background"] = "Details D'ictum (reverse)",
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["textR_class_colors"] = false,
						["textL_class_colors"] = false,
						["alpha"] = 1,
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["texture_custom"] = "",
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["percent_type"] = 1,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["height"] = 21,
					},
					["switch_tank"] = false,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
						[4] = 1,
					},
					["stretch_button_side"] = 1,
					["hide_in_combat_alpha"] = 0,
					["switch_tank_in_combat"] = false,
					["switch_all_roles_after_wipe"] = false,
					["libwindow"] = {
						["y"] = -6.93862915039063,
						["x"] = 182.519149780273,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["switch_damager"] = false,
					["__snapV"] = true,
					["bars_sort_direction"] = 1,
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["clickthrough_rows"] = false,
					["grab_on_top"] = false,
					["version"] = 3,
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["auto_current"] = true,
					["show_sidebars"] = false,
					["backdrop_texture"] = "Details Ground",
					["switch_damager_in_combat"] = false,
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = -6.93862915039063,
							["x"] = -755.616638183594,
							["w"] = 162.246948242188,
							["h"] = 85.3086776733399,
						},
						["solo"] = {
							["y"] = 1.99993896484375,
							["x"] = 1.00006103515625,
							["w"] = 300,
							["h"] = 300,
						},
					},
					["micro_displays_side"] = 2,
					["bars_grow_direction"] = 1,
					["wallpaper"] = {
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["enabled"] = false,
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["menu_icons_size"] = 0.850000023841858,
					["__was_opened"] = true,
					["hide_in_combat_type"] = 1,
					["__snapH"] = false,
				}, -- [2]
			},
			["segments_amount"] = 18,
			["report_lines"] = 5,
			["clear_ungrouped"] = true,
			["use_battleground_server_parser"] = false,
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["report_schema"] = 1,
			["use_scroll"] = false,
			["overall_clear_newboss"] = true,
			["minimum_combat_time"] = 5,
			["overall_clear_logout"] = false,
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["memory_threshold"] = 3,
			["deadlog_events"] = 32,
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["standard_skin"] = false,
			["total_abbreviation"] = 2,
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["Neutral"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["Unknow"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["Enemy"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["Shaman"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["Monk"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["Mage"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["Warlock"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["Rogue"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["Warrior"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["Hunter"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["Ungroupplayer"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["Deathknight"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["Demonhunter"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["Arena_green"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["Pet"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["Druid"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["Version"] = 1,
				["Paladin"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["Priest"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["Arena_yellow"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
			},
			["segments_auto_erase"] = 1,
			["clear_graphic"] = true,
			["trash_auto_remove"] = true,
			["animation_speed_triggertravel"] = 5,
			["options_group_edit"] = true,
			["segments_amount_to_save"] = 18,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["hide"] = false,
				["minimapPos"] = 223.358169175607,
				["text_format"] = 3,
				["text_type"] = 1,
			},
			["instances_amount"] = 2,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["deny_score_messages"] = false,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["default_bg_color"] = 0.0941,
			["broadcaster_enabled"] = false,
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["segments_panic_mode"] = false,
			["numerical_system_symbols"] = "auto",
			["overall_flag"] = 16,
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["font_sizes"] = {
				["menus"] = 10,
			},
			["player_details_window"] = {
				["scale"] = 1,
				["skin"] = "ElvUI",
				["bar_texture"] = "Skyline",
			},
			["numerical_system"] = 1,
			["force_activity_time_pvp"] = true,
			["pvp_as_group"] = true,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["disable_reset_button"] = false,
			["animate_scroll"] = false,
			["death_tooltip_width"] = 350,
			["time_type"] = 2,
			["default_bg_alpha"] = 0.5,
			["time_type_original"] = 2,
		},
		["Thorwynn-Faerlina"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["use_animation_accel"] = true,
				["disable_mythic_dungeon"] = false,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 17,
					["H"] = 17,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["icon_border_texcoord"] = {
					["B"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["R"] = 0.921875,
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["fontshadow"] = false,
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["border_size"] = 14,
				["maximize_method"] = 1,
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["time_type_original"] = 2,
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["default_bg_alpha"] = 0.5,
			["time_type"] = 2,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["death_tooltip_width"] = 350,
			["animate_scroll"] = false,
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 34.765380859375,
							["x"] = -778.53076171875,
							["w"] = 208.074157714844,
							["h"] = 98.7407608032227,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["show_statusbar"] = false,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["bars_sort_direction"] = 1,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["clickthrough_rows"] = false,
					["ignore_mass_showhide"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["desaturated_menu"] = false,
					["show_sidebars"] = false,
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bg_b"] = 0.0941176470588235,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textYMod"] = 1,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textYMod"] = 1,
								["segmentType"] = 2,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textYMod"] = 1,
								["timeType"] = 1,
								["textXMod"] = 6,
								["textAlign"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
						},
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["version"] = 3,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onenter"] = 1,
						["iconstoo"] = true,
						["ignorebars"] = false,
						["onleave"] = 1,
					},
					["menu_icons_size"] = 0.850000023841858,
					["micro_displays_locked"] = true,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["hide_in_combat_alpha"] = 0,
					["switch_damager"] = false,
					["stretch_button_side"] = 1,
					["libwindow"] = {
						["y"] = 34.765380859375,
						["x"] = 136.691421508789,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["switch_tank"] = false,
					["switch_all_roles_after_wipe"] = false,
					["switch_damager_in_combat"] = false,
					["grab_on_top"] = false,
					["bars_grow_direction"] = 1,
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["textR_outline_small"] = true,
						["textL_outline_small"] = true,
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
						["height"] = 21,
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["texture_custom_file"] = "Interface\\",
						["backdrop"] = {
							["enabled"] = false,
							["size"] = 12,
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["texture"] = "Details BarBorder 2",
						},
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["use_spec_icons"] = true,
						["textR_bracket"] = "(",
						["textR_enable_custom_text"] = false,
						["start_after_icon"] = true,
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["texture"] = "BantoBar",
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["texture_custom"] = "",
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["textR_class_colors"] = false,
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["textL_class_colors"] = false,
						["texture_background"] = "Details D'ictum (reverse)",
						["alpha"] = 1,
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["percent_type"] = 1,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["font_size"] = 16,
					},
					["auto_current"] = true,
					["micro_displays_side"] = 2,
					["attribute_text"] = {
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
						["shadow"] = false,
						["side"] = 1,
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["text_size"] = 12,
						["enable_custom_text"] = false,
						["enabled"] = true,
					},
					["bg_alpha"] = 0.183960914611816,
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 34.765380859375,
							["x"] = -778.53076171875,
							["w"] = 208.074157714844,
							["h"] = 98.7407608032227,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["bars_inverted"] = false,
					["skin_custom"] = "",
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_tank_in_combat"] = false,
					["backdrop_texture"] = "Details Ground",
					["__was_opened"] = true,
					["hide_in_combat_type"] = 1,
				}, -- [1]
			},
			["segments_amount"] = 18,
			["report_lines"] = 5,
			["pvp_as_group"] = true,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["force_activity_time_pvp"] = true,
			["numerical_system"] = 1,
			["player_details_window"] = {
				["scale"] = 1,
				["bar_texture"] = "Skyline",
				["skin"] = "ElvUI",
			},
			["font_sizes"] = {
				["menus"] = 10,
			},
			["minimum_combat_time"] = 5,
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["memory_threshold"] = 3,
			["deadlog_events"] = 32,
			["overall_flag"] = 16,
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["numerical_system_symbols"] = "auto",
			["trash_auto_remove"] = true,
			["total_abbreviation"] = 2,
			["segments_amount_to_save"] = 18,
			["clear_graphic"] = true,
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["animation_speed_triggertravel"] = 5,
			["options_group_edit"] = true,
			["broadcaster_enabled"] = false,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["text_type"] = 1,
				["minimapPos"] = 220,
				["text_format"] = 3,
				["hide"] = false,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["default_bg_color"] = 0.0941,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["deny_score_messages"] = false,
			["segments_auto_erase"] = 1,
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
			},
			["segments_panic_mode"] = false,
			["standard_skin"] = false,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["overall_clear_logout"] = false,
			["overall_clear_newboss"] = true,
			["report_schema"] = 1,
			["use_scroll"] = false,
			["use_battleground_server_parser"] = false,
			["disable_reset_button"] = false,
			["data_broker_text"] = "",
			["clear_ungrouped"] = true,
			["instances_no_libwindow"] = false,
			["deadlog_limit"] = 16,
			["instances_segments_locked"] = false,
		},
		["Thorend-Herod"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["disable_mythic_dungeon"] = false,
				["use_animation_accel"] = true,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 13,
					["H"] = 13,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8697, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["maximize_method"] = 1,
				["fontshadow"] = false,
				["border_size"] = 14,
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["icon_border_texcoord"] = {
					["R"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["B"] = 0.921875,
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["instances_segments_locked"] = false,
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["deadlog_limit"] = 16,
			["instances_no_libwindow"] = false,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["clear_ungrouped"] = true,
			["data_broker_text"] = "",
			["segments_amount"] = 18,
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 158.419555664063,
							["x"] = -593.382919311523,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["hide_in_combat_type"] = 1,
					["menu_icons_size"] = 0.850000023841858,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["micro_displays_locked"] = true,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["clickthrough_rows"] = false,
					["ignore_mass_showhide"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["desaturated_menu"] = false,
					["micro_displays_side"] = 2,
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bg_b"] = 0.0941176470588235,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textStyle"] = 2,
								["textAlign"] = 0,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["segmentType"] = 2,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textAlign"] = 0,
								["textXMod"] = 6,
								["textStyle"] = 2,
								["textFace"] = "Accidental Presidency",
								["timeType"] = 1,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
						},
					},
					["switch_tank_in_combat"] = false,
					["bg_alpha"] = 0.183960914611816,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onleave"] = 1,
						["ignorebars"] = false,
						["iconstoo"] = true,
						["onenter"] = 1,
					},
					["show_statusbar"] = false,
					["__was_opened"] = true,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["clickthrough_window"] = false,
					["hide_in_combat_alpha"] = 0,
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["textR_outline_small"] = true,
						["textL_outline_small"] = true,
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["start_after_icon"] = true,
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["font_size"] = 16,
						["height"] = 21,
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["backdrop"] = {
							["enabled"] = false,
							["texture"] = "Details BarBorder 2",
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["size"] = 12,
						},
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["textR_bracket"] = "(",
						["use_spec_icons"] = true,
						["texture_custom"] = "",
						["texture_custom_file"] = "Interface\\",
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_enable_custom_text"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["texture_background"] = "Details D'ictum (reverse)",
						["alpha"] = 1,
						["textR_class_colors"] = false,
						["textL_class_colors"] = false,
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["percent_type"] = 1,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
					},
					["stretch_button_side"] = 1,
					["libwindow"] = {
						["y"] = 158.419555664063,
						["x"] = 10.1355972290039,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["bars_grow_direction"] = 1,
					["bars_sort_direction"] = 1,
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["switch_damager_in_combat"] = false,
					["grab_on_top"] = false,
					["version"] = 3,
					["attribute_text"] = {
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
						["shadow"] = false,
						["side"] = 1,
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["enabled"] = true,
						["enable_custom_text"] = false,
						["text_size"] = 12,
					},
					["auto_current"] = true,
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["backdrop_texture"] = "Details Ground",
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 158.419555664063,
							["x"] = -593.382919311523,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["switch_tank"] = false,
					["switch_all_roles_after_wipe"] = false,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_damager"] = false,
					["show_sidebars"] = false,
					["bars_inverted"] = false,
					["skin_custom"] = "",
				}, -- [1]
			},
			["report_lines"] = 5,
			["use_battleground_server_parser"] = false,
			["use_scroll"] = false,
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["report_schema"] = 1,
			["overall_clear_newboss"] = true,
			["overall_clear_logout"] = false,
			["font_sizes"] = {
				["menus"] = 10,
			},
			["minimum_combat_time"] = 5,
			["memory_threshold"] = 3,
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["deadlog_events"] = 32,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["standard_skin"] = false,
			["total_abbreviation"] = 2,
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
			},
			["segments_auto_erase"] = 1,
			["clear_graphic"] = true,
			["trash_auto_remove"] = true,
			["animation_speed_triggertravel"] = 5,
			["options_group_edit"] = true,
			["segments_amount_to_save"] = 18,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["hide"] = false,
				["minimapPos"] = 220,
				["text_format"] = 3,
				["text_type"] = 1,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["deny_score_messages"] = false,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["default_bg_color"] = 0.0941,
			["broadcaster_enabled"] = false,
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["segments_panic_mode"] = false,
			["numerical_system_symbols"] = "auto",
			["overall_flag"] = 16,
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["player_details_window"] = {
				["scale"] = 1,
				["skin"] = "ElvUI",
				["bar_texture"] = "Skyline",
			},
			["numerical_system"] = 1,
			["force_activity_time_pvp"] = true,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["pvp_as_group"] = true,
			["disable_reset_button"] = false,
			["animate_scroll"] = false,
			["death_tooltip_width"] = 350,
			["time_type"] = 2,
			["default_bg_alpha"] = 0.5,
			["time_type_original"] = 2,
		},
		["Thorpez-Herod"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["use_animation_accel"] = true,
				["disable_mythic_dungeon"] = false,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 17,
					["H"] = 17,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["icon_border_texcoord"] = {
					["B"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["R"] = 0.921875,
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["fontshadow"] = false,
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["border_size"] = 14,
				["maximize_method"] = 1,
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["time_type_original"] = 2,
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["default_bg_alpha"] = 0.5,
			["time_type"] = 2,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["death_tooltip_width"] = 350,
			["animate_scroll"] = false,
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 157.234436035156,
							["x"] = -603.518524169922,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["show_statusbar"] = false,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["bars_sort_direction"] = 1,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["clickthrough_rows"] = false,
					["switch_tank"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["switch_damager"] = false,
					["show_sidebars"] = false,
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bg_b"] = 0.0941176470588235,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textYMod"] = 1,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["segmentType"] = 2,
								["textFace"] = "Accidental Presidency",
								["textXMod"] = 0,
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textStyle"] = 2,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textXMod"] = 6,
								["timeType"] = 1,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
						},
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["bg_alpha"] = 0.183960914611816,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onenter"] = 1,
						["iconstoo"] = true,
						["ignorebars"] = false,
						["onleave"] = 1,
					},
					["hide_in_combat_type"] = 1,
					["__was_opened"] = true,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["menu_icons_size"] = 0.850000023841858,
					["hide_in_combat_alpha"] = 0,
					["micro_displays_locked"] = true,
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["libwindow"] = {
						["y"] = 157.234451293945,
						["x"] = 0,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["skin_custom"] = "",
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["backdrop_texture"] = "Details Ground",
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["textL_enable_custom_text"] = false,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["start_after_icon"] = true,
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["backdrop"] = {
							["enabled"] = false,
							["size"] = 12,
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["texture"] = "Details BarBorder 2",
						},
						["font_size"] = 16,
						["height"] = 21,
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["textR_bracket"] = "(",
						["use_spec_icons"] = true,
						["texture_custom"] = "",
						["percent_type"] = 1,
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textL_outline_small"] = true,
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["textR_outline_small"] = true,
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["textR_class_colors"] = false,
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["textL_class_colors"] = false,
						["texture_background"] = "Details D'ictum (reverse)",
						["alpha"] = 1,
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["textR_enable_custom_text"] = false,
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["texture_custom_file"] = "Interface\\",
					},
					["grab_on_top"] = false,
					["switch_damager_in_combat"] = false,
					["switch_tank_in_combat"] = false,
					["version"] = 3,
					["auto_current"] = true,
					["attribute_text"] = {
						["enabled"] = true,
						["shadow"] = false,
						["side"] = 1,
						["text_size"] = 12,
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["enable_custom_text"] = false,
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
					},
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["bars_inverted"] = false,
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 157.234436035156,
							["x"] = -603.518524169922,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["bars_grow_direction"] = 1,
					["micro_displays_side"] = 2,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["stretch_button_side"] = 1,
					["ignore_mass_showhide"] = false,
					["switch_all_roles_after_wipe"] = false,
					["desaturated_menu"] = false,
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
				}, -- [1]
			},
			["segments_amount"] = 18,
			["report_lines"] = 5,
			["pvp_as_group"] = true,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["force_activity_time_pvp"] = true,
			["numerical_system"] = 1,
			["player_details_window"] = {
				["scale"] = 1,
				["bar_texture"] = "Skyline",
				["skin"] = "ElvUI",
			},
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["minimum_combat_time"] = 5,
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["memory_threshold"] = 3,
			["deadlog_events"] = 32,
			["overall_flag"] = 16,
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["numerical_system_symbols"] = "auto",
			["trash_auto_remove"] = true,
			["total_abbreviation"] = 2,
			["segments_amount_to_save"] = 18,
			["clear_graphic"] = true,
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["animation_speed_triggertravel"] = 5,
			["options_group_edit"] = true,
			["broadcaster_enabled"] = false,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["text_type"] = 1,
				["minimapPos"] = 220,
				["text_format"] = 3,
				["hide"] = false,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["default_bg_color"] = 0.0941,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["deny_score_messages"] = false,
			["segments_auto_erase"] = 1,
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
			},
			["segments_panic_mode"] = false,
			["standard_skin"] = false,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["font_sizes"] = {
				["menus"] = 10,
			},
			["overall_clear_logout"] = false,
			["overall_clear_newboss"] = true,
			["report_schema"] = 1,
			["use_scroll"] = false,
			["use_battleground_server_parser"] = false,
			["disable_reset_button"] = false,
			["data_broker_text"] = "",
			["clear_ungrouped"] = true,
			["instances_no_libwindow"] = false,
			["deadlog_limit"] = 16,
			["instances_segments_locked"] = false,
		},
		["Thorend-Faerlina"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["use_animation_accel"] = true,
				["disable_mythic_dungeon"] = false,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 13,
					["H"] = 13,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8697, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["icon_border_texcoord"] = {
					["B"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["R"] = 0.921875,
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["fontshadow"] = false,
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["border_size"] = 14,
				["maximize_method"] = 1,
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["time_type_original"] = 2,
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["default_bg_alpha"] = 0.5,
			["time_type"] = 2,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["death_tooltip_width"] = 350,
			["animate_scroll"] = false,
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 206.222229003906,
							["x"] = -603.518524169922,
							["w"] = 310.000061035156,
							["h"] = 157.999984741211,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["hide_in_combat_type"] = 1,
					["clickthrough_window"] = false,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["micro_displays_locked"] = true,
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textL_outline_small"] = true,
						["percent_type"] = 1,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
						["texture_custom_file"] = "Interface\\",
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["height"] = 21,
						["backdrop"] = {
							["enabled"] = false,
							["size"] = 12,
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["texture"] = "Details BarBorder 2",
						},
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["textR_bracket"] = "(",
						["use_spec_icons"] = true,
						["texture_custom"] = "",
						["textL_enable_custom_text"] = false,
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["textR_enable_custom_text"] = false,
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["textR_class_colors"] = false,
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["textL_class_colors"] = false,
						["texture_background"] = "Details D'ictum (reverse)",
						["alpha"] = 1,
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["textR_outline_small"] = true,
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["start_after_icon"] = true,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["font_size"] = 16,
					},
					["switch_tank"] = false,
					["plugins_grow_direction"] = 1,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["switch_damager"] = false,
					["micro_displays_side"] = 2,
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bg_b"] = 0.0941176470588235,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textYMod"] = 1,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["segmentType"] = 2,
								["textFace"] = "Accidental Presidency",
								["textXMod"] = 0,
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textXMod"] = 6,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["timeType"] = 1,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
						},
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
					},
					["switch_tank_in_combat"] = false,
					["bg_alpha"] = 0.183960914611816,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onenter"] = 1,
						["iconstoo"] = true,
						["ignorebars"] = false,
						["onleave"] = 1,
					},
					["show_statusbar"] = false,
					["__was_opened"] = true,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["menu_icons_size"] = 0.850000023841858,
					["hide_in_combat_alpha"] = 0,
					["bars_inverted"] = false,
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["libwindow"] = {
						["y"] = -141.444458007813,
						["x"] = 0,
						["point"] = "TOPLEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["backdrop_texture"] = "Details Ground",
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["bars_sort_direction"] = 1,
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["switch_damager_in_combat"] = false,
					["grab_on_top"] = false,
					["clickthrough_rows"] = false,
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["auto_current"] = true,
					["version"] = 3,
					["attribute_text"] = {
						["enabled"] = true,
						["shadow"] = false,
						["side"] = 1,
						["text_size"] = 12,
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["enable_custom_text"] = false,
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
					},
					["bars_grow_direction"] = 1,
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 206.222229003906,
							["x"] = -603.518524169922,
							["w"] = 310.000061035156,
							["h"] = 157.999984741211,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["desaturated_menu"] = false,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["stretch_button_side"] = 1,
					["ignore_mass_showhide"] = false,
					["switch_all_roles_after_wipe"] = false,
					["show_sidebars"] = false,
					["skin_custom"] = "",
				}, -- [1]
			},
			["segments_amount"] = 18,
			["report_lines"] = 5,
			["pvp_as_group"] = true,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["force_activity_time_pvp"] = true,
			["numerical_system"] = 1,
			["player_details_window"] = {
				["scale"] = 1,
				["bar_texture"] = "Skyline",
				["skin"] = "ElvUI",
			},
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["minimum_combat_time"] = 5,
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["font_sizes"] = {
				["menus"] = 10,
			},
			["memory_threshold"] = 3,
			["deadlog_events"] = 32,
			["overall_flag"] = 16,
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["numerical_system_symbols"] = "auto",
			["trash_auto_remove"] = true,
			["total_abbreviation"] = 2,
			["segments_amount_to_save"] = 18,
			["clear_graphic"] = true,
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["animation_speed_triggertravel"] = 5,
			["options_group_edit"] = true,
			["broadcaster_enabled"] = false,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["text_type"] = 1,
				["minimapPos"] = 220,
				["text_format"] = 3,
				["hide"] = false,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["default_bg_color"] = 0.0941,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["deny_score_messages"] = false,
			["segments_auto_erase"] = 1,
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
			},
			["segments_panic_mode"] = false,
			["standard_skin"] = false,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["overall_clear_logout"] = false,
			["overall_clear_newboss"] = true,
			["report_schema"] = 1,
			["use_scroll"] = false,
			["use_battleground_server_parser"] = false,
			["disable_reset_button"] = false,
			["data_broker_text"] = "",
			["clear_ungrouped"] = true,
			["instances_no_libwindow"] = false,
			["deadlog_limit"] = 16,
			["instances_segments_locked"] = false,
		},
		["Bankofthor-Faerlina"] = {
			["show_arena_role_icon"] = false,
			["capture_real"] = {
				["heal"] = true,
				["spellcast"] = true,
				["miscdata"] = true,
				["aura"] = true,
				["energy"] = true,
				["damage"] = true,
			},
			["row_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["streamer_config"] = {
				["faster_updates"] = false,
				["quick_detection"] = false,
				["reset_spec_cache"] = false,
				["no_alerts"] = false,
				["use_animation_accel"] = true,
				["disable_mythic_dungeon"] = false,
			},
			["all_players_are_group"] = false,
			["use_row_animations"] = true,
			["report_heal_links"] = false,
			["remove_realm_from_name"] = true,
			["minimum_overall_combat_time"] = 10,
			["event_tracker"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["line_height"] = 16,
				["line_color"] = {
					0.1, -- [1]
					0.1, -- [2]
					0.1, -- [3]
					0.3, -- [4]
				},
				["font_shadow"] = "NONE",
				["font_size"] = 10,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = true,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0.16, -- [1]
						0.16, -- [2]
						0.16, -- [3]
						0.47, -- [4]
					},
					["locked"] = false,
					["height"] = 300,
					["width"] = 250,
				},
				["line_texture"] = "Details Serenity",
				["options_frame"] = {
				},
			},
			["report_to_who"] = "",
			["class_specs_coords"] = {
				[62] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[63] = {
					0.375, -- [1]
					0.5, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[250] = {
					0, -- [1]
					0.125, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[251] = {
					0.125, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[252] = {
					0.25, -- [1]
					0.375, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[253] = {
					0.875, -- [1]
					1, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[254] = {
					0, -- [1]
					0.125, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[255] = {
					0.125, -- [1]
					0.25, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[66] = {
					0.125, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[257] = {
					0.5, -- [1]
					0.625, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[258] = {
					0.6328125, -- [1]
					0.75, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[259] = {
					0.75, -- [1]
					0.875, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[260] = {
					0.875, -- [1]
					1, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[577] = {
					0.25, -- [1]
					0.375, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[262] = {
					0.125, -- [1]
					0.25, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[581] = {
					0.375, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[264] = {
					0.375, -- [1]
					0.5, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[265] = {
					0.5, -- [1]
					0.625, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[266] = {
					0.625, -- [1]
					0.75, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[267] = {
					0.75, -- [1]
					0.875, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[268] = {
					0.625, -- [1]
					0.75, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[269] = {
					0.875, -- [1]
					1, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[270] = {
					0.75, -- [1]
					0.875, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[70] = {
					0.251953125, -- [1]
					0.375, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[102] = {
					0.375, -- [1]
					0.5, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[71] = {
					0.875, -- [1]
					1, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[103] = {
					0.5, -- [1]
					0.625, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[72] = {
					0, -- [1]
					0.125, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[104] = {
					0.625, -- [1]
					0.75, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[73] = {
					0.125, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.625, -- [4]
				},
				[64] = {
					0.5, -- [1]
					0.625, -- [2]
					0.125, -- [3]
					0.25, -- [4]
				},
				[105] = {
					0.75, -- [1]
					0.875, -- [2]
					0, -- [3]
					0.125, -- [4]
				},
				[65] = {
					0, -- [1]
					0.125, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[256] = {
					0.375, -- [1]
					0.5, -- [2]
					0.25, -- [3]
					0.375, -- [4]
				},
				[261] = {
					0, -- [1]
					0.125, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
				[263] = {
					0.25, -- [1]
					0.375, -- [2]
					0.375, -- [3]
					0.5, -- [4]
				},
			},
			["profile_save_pos"] = true,
			["tooltip"] = {
				["header_statusbar"] = {
					0.3, -- [1]
					0.3, -- [2]
					0.3, -- [3]
					0.8, -- [4]
					false, -- [5]
					false, -- [6]
					"WorldState Score", -- [7]
				},
				["fontcolor_right"] = {
					1, -- [1]
					0.7, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["line_height"] = 17,
				["tooltip_max_targets"] = 2,
				["icon_size"] = {
					["W"] = 13,
					["H"] = 13,
				},
				["tooltip_max_pets"] = 2,
				["anchor_relative"] = "top",
				["abbreviation"] = 2,
				["anchored_to"] = 1,
				["show_amount"] = false,
				["header_text_color"] = {
					1, -- [1]
					0.9176, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["fontsize"] = 10,
				["background"] = {
					0.196, -- [1]
					0.196, -- [2]
					0.196, -- [3]
					0.8697, -- [4]
				},
				["submenu_wallpaper"] = true,
				["fontsize_title"] = 10,
				["icon_border_texcoord"] = {
					["B"] = 0.921875,
					["L"] = 0.078125,
					["T"] = 0.078125,
					["R"] = 0.921875,
				},
				["commands"] = {
				},
				["tooltip_max_abilities"] = 6,
				["fontface"] = "Friz Quadrata TT",
				["border_color"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["border_texture"] = "Details BarBorder 3",
				["anchor_offset"] = {
					0, -- [1]
					0, -- [2]
				},
				["maximize_method"] = 1,
				["fontshadow"] = false,
				["border_size"] = 14,
				["menus_bg_texture"] = "Interface\\SPELLBOOK\\Spellbook-Page-1",
				["anchor_screen_pos"] = {
					507.7, -- [1]
					-350.5, -- [2]
				},
				["anchor_point"] = "bottom",
				["menus_bg_coords"] = {
					0.309777336120606, -- [1]
					0.924000015258789, -- [2]
					0.213000011444092, -- [3]
					0.279000015258789, -- [4]
				},
				["fontcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["menus_bg_color"] = {
					0.8, -- [1]
					0.8, -- [2]
					0.8, -- [3]
					0.2, -- [4]
				},
			},
			["ps_abbreviation"] = 3,
			["world_combat_is_trash"] = false,
			["update_speed"] = 0.2,
			["bookmark_text_size"] = 11,
			["animation_speed_mintravel"] = 0.45,
			["track_item_level"] = true,
			["windows_fade_in"] = {
				"in", -- [1]
				0.2, -- [2]
			},
			["instances_menu_click_to_open"] = false,
			["overall_clear_newchallenge"] = true,
			["current_dps_meter"] = {
				["enabled"] = false,
				["font_color"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["arena_enabled"] = true,
				["font_shadow"] = "NONE",
				["font_size"] = 18,
				["mythic_dungeon_enabled"] = true,
				["sample_size"] = 5,
				["font_face"] = "Friz Quadrata TT",
				["frame"] = {
					["show_title"] = false,
					["strata"] = "LOW",
					["backdrop_color"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.2, -- [4]
					},
					["locked"] = false,
					["height"] = 65,
					["width"] = 220,
				},
				["update_interval"] = 0.3,
				["options_frame"] = {
				},
			},
			["data_cleanup_logout"] = false,
			["instances_disable_bar_highlight"] = false,
			["trash_concatenate"] = false,
			["color_by_arena_team"] = true,
			["animation_speed"] = 33,
			["disable_stretch_from_toolbar"] = false,
			["disable_lock_ungroup_buttons"] = false,
			["memory_ram"] = 64,
			["disable_window_groups"] = false,
			["instances_suppress_trash"] = 0,
			["time_type_original"] = 2,
			["options_window"] = {
				["scale"] = 1,
			},
			["animation_speed_maxtravel"] = 3,
			["default_bg_alpha"] = 0.5,
			["time_type"] = 2,
			["font_faces"] = {
				["menus"] = "Friz Quadrata TT",
			},
			["death_tooltip_width"] = 350,
			["animate_scroll"] = false,
			["segments_amount"] = 18,
			["instances"] = {
				{
					["__pos"] = {
						["normal"] = {
							["y"] = 119.308654785156,
							["x"] = -455.901275634766,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["hide_in_combat_type"] = 1,
					["menu_icons_size"] = 0.850000023841858,
					["menu_anchor"] = {
						16, -- [1]
						0, -- [2]
						["side"] = 2,
					},
					["bg_r"] = 0.0941176470588235,
					["hide_out_of_combat"] = false,
					["color_buttons"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["toolbar_icon_file"] = "Interface\\AddOns\\Details\\images\\toolbar_icons",
					["skin_custom"] = "",
					["tooltip"] = {
						["n_abilities"] = 3,
						["n_enemies"] = 3,
					},
					["switch_all_roles_in_combat"] = false,
					["clickthrough_toolbaricons"] = false,
					["attribute_text"] = {
						["enabled"] = true,
						["shadow"] = false,
						["side"] = 1,
						["text_size"] = 12,
						["custom_text"] = "{name}",
						["text_face"] = "Accidental Presidency",
						["anchor"] = {
							-18, -- [1]
							3, -- [2]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["enable_custom_text"] = false,
						["show_timer"] = {
							true, -- [1]
							true, -- [2]
							true, -- [3]
						},
					},
					["ignore_mass_showhide"] = false,
					["switch_all_roles_after_wipe"] = false,
					["menu_icons"] = {
						true, -- [1]
						true, -- [2]
						true, -- [3]
						true, -- [4]
						true, -- [5]
						false, -- [6]
						["space"] = -2,
						["shadow"] = false,
					},
					["desaturated_menu"] = false,
					["micro_displays_side"] = 2,
					["window_scale"] = 1,
					["hide_icon"] = true,
					["toolbar_side"] = 1,
					["bg_g"] = 0.0941176470588235,
					["bars_inverted"] = false,
					["switch_healer_in_combat"] = false,
					["color"] = {
						0.0705882352941177, -- [1]
						0.0705882352941177, -- [2]
						0.0705882352941177, -- [3]
						0.639196664094925, -- [4]
					},
					["skin"] = "Minimalistic",
					["following"] = {
						["enabled"] = false,
						["bar_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["switch_healer"] = false,
					["StatusBarSaved"] = {
						["center"] = "DETAILS_STATUSBAR_PLUGIN_CLOCK",
						["right"] = "DETAILS_STATUSBAR_PLUGIN_PDPS",
						["options"] = {
							["DETAILS_STATUSBAR_PLUGIN_PDPS"] = {
								["textYMod"] = 1,
								["textXMod"] = 0,
								["textFace"] = "Accidental Presidency",
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
							},
							["DETAILS_STATUSBAR_PLUGIN_PSEGMENT"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["segmentType"] = 2,
								["textFace"] = "Accidental Presidency",
								["textXMod"] = 0,
								["textAlign"] = 0,
								["textStyle"] = 2,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
							["DETAILS_STATUSBAR_PLUGIN_CLOCK"] = {
								["textColor"] = {
									1, -- [1]
									1, -- [2]
									1, -- [3]
									1, -- [4]
								},
								["textFace"] = "Accidental Presidency",
								["textXMod"] = 6,
								["textAlign"] = 0,
								["textStyle"] = 2,
								["timeType"] = 1,
								["textSize"] = 10,
								["textYMod"] = 1,
							},
						},
						["left"] = "DETAILS_STATUSBAR_PLUGIN_PSEGMENT",
					},
					["switch_tank_in_combat"] = false,
					["version"] = 3,
					["__locked"] = false,
					["menu_alpha"] = {
						["enabled"] = false,
						["onenter"] = 1,
						["iconstoo"] = true,
						["ignorebars"] = false,
						["onleave"] = 1,
					},
					["show_statusbar"] = false,
					["__was_opened"] = true,
					["strata"] = "LOW",
					["clickthrough_incombatonly"] = true,
					["__snap"] = {
					},
					["clickthrough_window"] = false,
					["hide_in_combat_alpha"] = 0,
					["switch_tank"] = false,
					["stretch_button_side"] = 1,
					["libwindow"] = {
						["y"] = 119.308685302734,
						["x"] = 147.617248535156,
						["point"] = "LEFT",
						["scale"] = 1,
					},
					["statusbar_info"] = {
						["alpha"] = 0.3777777777777,
						["overlay"] = {
							0.333333333333333, -- [1]
							0.333333333333333, -- [2]
							0.333333333333333, -- [3]
						},
					},
					["backdrop_texture"] = "Details Ground",
					["bars_grow_direction"] = 1,
					["bg_b"] = 0.0941176470588235,
					["micro_displays_locked"] = true,
					["grab_on_top"] = false,
					["switch_damager_in_combat"] = false,
					["menu_anchor_down"] = {
						16, -- [1]
						-3, -- [2]
					},
					["bg_alpha"] = 0.183960914611816,
					["auto_current"] = true,
					["clickthrough_rows"] = false,
					["row_show_animation"] = {
						["anim"] = "Fade",
						["options"] = {
						},
					},
					["instance_button_anchor"] = {
						-27, -- [1]
						1, -- [2]
					},
					["hide_in_combat"] = false,
					["posicao"] = {
						["normal"] = {
							["y"] = 119.308654785156,
							["x"] = -455.901275634766,
							["w"] = 310.000061035156,
							["h"] = 158.000061035156,
						},
						["solo"] = {
							["y"] = 2,
							["x"] = 1,
							["w"] = 300,
							["h"] = 200,
						},
					},
					["plugins_grow_direction"] = 1,
					["switch_damager"] = false,
					["wallpaper"] = {
						["enabled"] = false,
						["texcoord"] = {
							0, -- [1]
							1, -- [2]
							0, -- [3]
							0.7, -- [4]
						},
						["overlay"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
							1, -- [4]
						},
						["anchor"] = "all",
						["height"] = 114.042518615723,
						["alpha"] = 0.5,
						["width"] = 283.000183105469,
					},
					["total_bar"] = {
						["enabled"] = false,
						["only_in_group"] = true,
						["icon"] = "Interface\\ICONS\\INV_Sigil_Thorim",
						["color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
					},
					["auto_hide_menu"] = {
						["left"] = false,
						["right"] = false,
					},
					["show_sidebars"] = false,
					["row_info"] = {
						["textR_outline"] = false,
						["spec_file"] = "Interface\\AddOns\\Details\\images\\spec_icons_normal",
						["textL_outline"] = false,
						["texture_highlight"] = "Interface\\FriendsFrame\\UI-FriendsList-Highlight",
						["textL_outline_small"] = true,
						["percent_type"] = 1,
						["fixed_text_color"] = {
							1, -- [1]
							1, -- [2]
							1, -- [3]
						},
						["space"] = {
							["right"] = 0,
							["left"] = 0,
							["between"] = 1,
						},
						["texture_background_class_color"] = false,
						["textL_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["font_face_file"] = "Interface\\Addons\\Details\\fonts\\Accidental Presidency.ttf",
						["textL_custom_text"] = "{data1}. {data3}{data2}",
						["models"] = {
							["upper_model"] = "Spells\\AcidBreath_SuperGreen.M2",
							["lower_model"] = "World\\EXPANSION02\\DOODADS\\Coldarra\\COLDARRALOCUS.m2",
							["upper_alpha"] = 0.5,
							["lower_enabled"] = false,
							["lower_alpha"] = 0.1,
							["upper_enabled"] = false,
						},
						["texture_custom_file"] = "Interface\\",
						["texture_file"] = "Interface\\AddOns\\Details\\images\\BantoBar",
						["height"] = 21,
						["backdrop"] = {
							["enabled"] = false,
							["size"] = 12,
							["color"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
								1, -- [4]
							},
							["texture"] = "Details BarBorder 2",
						},
						["icon_file"] = "Interface\\AddOns\\Details\\images\\classes_small",
						["icon_grayscale"] = false,
						["textR_bracket"] = "(",
						["use_spec_icons"] = true,
						["texture_custom"] = "",
						["textL_enable_custom_text"] = false,
						["fixed_texture_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
						},
						["textL_show_number"] = true,
						["textR_show_data"] = {
							true, -- [1]
							true, -- [2]
							false, -- [3]
						},
						["fixed_texture_background_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.150228589773178, -- [4]
						},
						["textR_custom_text"] = "{data1} ({data2}, {data3}%)",
						["texture"] = "BantoBar",
						["textR_enable_custom_text"] = false,
						["texture_background_file"] = "Interface\\AddOns\\Details\\images\\bar4_reverse",
						["texture_background"] = "Details D'ictum (reverse)",
						["textR_outline_small_color"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							1, -- [4]
						},
						["textR_class_colors"] = false,
						["textL_class_colors"] = false,
						["alpha"] = 1,
						["no_icon"] = false,
						["icon_offset"] = {
							0, -- [1]
							0, -- [2]
						},
						["textR_outline_small"] = true,
						["font_face"] = "Accidental Presidency",
						["texture_class_colors"] = true,
						["start_after_icon"] = true,
						["fast_ps_update"] = false,
						["textR_separator"] = "NONE",
						["font_size"] = 16,
					},
					["bars_sort_direction"] = 1,
				}, -- [1]
			},
			["report_lines"] = 5,
			["class_icons_small"] = "Interface\\AddOns\\Details\\images\\classes_small",
			["pvp_as_group"] = true,
			["skin"] = "WoW Interface",
			["override_spellids"] = true,
			["force_activity_time_pvp"] = true,
			["numerical_system"] = 1,
			["player_details_window"] = {
				["scale"] = 1,
				["bar_texture"] = "Skyline",
				["skin"] = "ElvUI",
			},
			["font_sizes"] = {
				["menus"] = 10,
			},
			["minimum_combat_time"] = 5,
			["memory_threshold"] = 3,
			["cloud_capture"] = true,
			["damage_taken_everything"] = false,
			["scroll_speed"] = 2,
			["new_window_size"] = {
				["height"] = 158,
				["width"] = 310,
			},
			["chat_tab_embed"] = {
				["enabled"] = false,
				["y_offset"] = 0,
				["x_offset"] = 0,
				["tab_name"] = "",
				["single_window"] = false,
			},
			["deadlog_events"] = 32,
			["overall_flag"] = 16,
			["close_shields"] = false,
			["class_coords"] = {
				["HUNTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["WARRIOR"] = {
					0, -- [1]
					0.25, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["SHAMAN"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["MAGE"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["PET"] = {
					0.25, -- [1]
					0.49609375, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["DRUID"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["MONK"] = {
					0.5, -- [1]
					0.73828125, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["DEATHKNIGHT"] = {
					0.25, -- [1]
					0.5, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["MONSTER"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNKNOW"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PRIEST"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["ROGUE"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0, -- [3]
					0.25, -- [4]
				},
				["Alliance"] = {
					0.49609375, -- [1]
					0.7421875, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["WARLOCK"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.25, -- [3]
					0.5, -- [4]
				},
				["DEMONHUNTER"] = {
					0.73828126, -- [1]
					1, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["Horde"] = {
					0.7421875, -- [1]
					0.98828125, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["PALADIN"] = {
					0, -- [1]
					0.25, -- [2]
					0.5, -- [3]
					0.75, -- [4]
				},
				["ENEMY"] = {
					0, -- [1]
					0.25, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
				["UNGROUPPLAYER"] = {
					0.5, -- [1]
					0.75, -- [2]
					0.75, -- [3]
					1, -- [4]
				},
			},
			["force_class_icons"] = false,
			["disable_alldisplays_window"] = false,
			["numerical_system_symbols"] = "auto",
			["class_colors"] = {
				["HUNTER"] = {
					0.67, -- [1]
					0.83, -- [2]
					0.45, -- [3]
				},
				["WARRIOR"] = {
					0.78, -- [1]
					0.61, -- [2]
					0.43, -- [3]
				},
				["PALADIN"] = {
					0.96, -- [1]
					0.55, -- [2]
					0.73, -- [3]
				},
				["MAGE"] = {
					0.41, -- [1]
					0.8, -- [2]
					0.94, -- [3]
				},
				["ARENA_YELLOW"] = {
					1, -- [1]
					1, -- [2]
					0.25, -- [3]
				},
				["UNGROUPPLAYER"] = {
					0.4, -- [1]
					0.4, -- [2]
					0.4, -- [3]
				},
				["DRUID"] = {
					1, -- [1]
					0.49, -- [2]
					0.04, -- [3]
				},
				["MONK"] = {
					0, -- [1]
					1, -- [2]
					0.59, -- [3]
				},
				["DEATHKNIGHT"] = {
					0.77, -- [1]
					0.12, -- [2]
					0.23, -- [3]
				},
				["ROGUE"] = {
					1, -- [1]
					0.96, -- [2]
					0.41, -- [3]
				},
				["UNKNOW"] = {
					0.2, -- [1]
					0.2, -- [2]
					0.2, -- [3]
				},
				["PRIEST"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["PET"] = {
					0.3, -- [1]
					0.4, -- [2]
					0.5, -- [3]
				},
				["WARLOCK"] = {
					0.58, -- [1]
					0.51, -- [2]
					0.79, -- [3]
				},
				["ENEMY"] = {
					0.94117, -- [1]
					0, -- [2]
					0.0196, -- [3]
					1, -- [4]
				},
				["DEMONHUNTER"] = {
					0.64, -- [1]
					0.19, -- [2]
					0.79, -- [3]
				},
				["version"] = 1,
				["NEUTRAL"] = {
					1, -- [1]
					1, -- [2]
					0, -- [3]
				},
				["SHAMAN"] = {
					0, -- [1]
					0.44, -- [2]
					0.87, -- [3]
				},
				["ARENA_GREEN"] = {
					0.4, -- [1]
					1, -- [2]
					0.4, -- [3]
				},
			},
			["hotcorner_topleft"] = {
				["hide"] = false,
			},
			["broadcaster_enabled"] = false,
			["clear_graphic"] = true,
			["total_abbreviation"] = 2,
			["segments_auto_erase"] = 1,
			["options_group_edit"] = true,
			["segments_amount_to_save"] = 18,
			["minimap"] = {
				["onclick_what_todo"] = 1,
				["radius"] = 160,
				["text_type"] = 1,
				["minimapPos"] = 220,
				["text_format"] = 3,
				["hide"] = false,
			},
			["instances_amount"] = 5,
			["max_window_size"] = {
				["height"] = 450,
				["width"] = 480,
			},
			["default_bg_color"] = 0.0941,
			["only_pvp_frags"] = false,
			["disable_stretch_button"] = false,
			["deny_score_messages"] = false,
			["animation_speed_triggertravel"] = 5,
			["trash_auto_remove"] = true,
			["segments_panic_mode"] = false,
			["standard_skin"] = false,
			["window_clamp"] = {
				-8, -- [1]
				0, -- [2]
				21, -- [3]
				-14, -- [4]
			},
			["row_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
			["overall_clear_logout"] = false,
			["overall_clear_newboss"] = true,
			["use_scroll"] = false,
			["report_schema"] = 1,
			["use_battleground_server_parser"] = false,
			["clear_ungrouped"] = true,
			["disable_reset_button"] = false,
			["data_broker_text"] = "",
			["instances_no_libwindow"] = false,
			["instances_segments_locked"] = false,
			["deadlog_limit"] = 16,
			["windows_fade_out"] = {
				"out", -- [1]
				0.2, -- [2]
			},
		},
	},
	["boss_mods_timers"] = {
		["encounter_timers_bw"] = {
		},
		["encounter_timers_dbm"] = {
		},
	},
	["disable_talent_feature"] = false,
	["spell_school_cache"] = {
	},
	["deathlog_healingdone_min"] = 1,
	["plater"] = {
		["realtime_dps_enabled"] = false,
		["realtime_dps_size"] = 12,
		["realtime_dps_player_shadow"] = true,
		["damage_taken_enabled"] = false,
		["realtime_dps_player_size"] = 12,
		["damage_taken_size"] = 12,
		["realtime_dps_color"] = {
			1, -- [1]
			1, -- [2]
			0, -- [3]
			1, -- [4]
		},
		["realtime_dps_anchor"] = {
			["y"] = 0,
			["x"] = 0,
			["side"] = 7,
		},
		["damage_taken_shadow"] = true,
		["damage_taken_anchor"] = {
			["y"] = 0,
			["x"] = 0,
			["side"] = 7,
		},
		["damage_taken_color"] = {
			1, -- [1]
			1, -- [2]
			0, -- [3]
			1, -- [4]
		},
		["realtime_dps_player_color"] = {
			1, -- [1]
			1, -- [2]
			0, -- [3]
			1, -- [4]
		},
		["realtime_dps_player_anchor"] = {
			["y"] = 0,
			["x"] = 0,
			["side"] = 7,
		},
		["realtime_dps_player_enabled"] = false,
		["realtime_dps_shadow"] = true,
	},
	["data_sync"] = false,
	["mobs_data"] = {
	},
	["run_code"] = {
		["on_specchanged"] = "\n-- run when the player changes its spec",
		["on_zonechanged"] = "\n-- when the player changes zone, this code will run",
		["on_init"] = "\n-- code to run when Details! initializes, put here code which only will run once\n-- this also will run then the profile is changed\n\n--size of the death log tooltip in the Deaths display (default 350)\nDetails.death_tooltip_width = 350;\n\n--when in arena or battleground, details! silently switch to activity time (goes back to the old setting on leaving, default true)\nDetails.force_activity_time_pvp = true;\n\n--speed of the bar animations (default 33)\nDetails.animation_speed = 33;\n\n--threshold to trigger slow or fast speed (default 0.45)\nDetails.animation_speed_mintravel = 0.45;\n\n--call to update animations\nDetails:RefreshAnimationFunctions();\n\n--max window size, does require a /reload to work (default 480 x 450)\nDetails.max_window_size.width = 480;\nDetails.max_window_size.height = 450;\n\n--use the arena team color as the class color (default true)\nDetails.color_by_arena_team = true;\n\n--use the role icon in the player bar when inside an arena (default false)\nDetails.show_arena_role_icon = false;\n\n--how much time the update warning is shown (default 10)\nDetails.update_warning_timeout = 10;",
		["on_groupchange"] = "\n-- this code runs when the player enter or leave a group",
		["on_leavecombat"] = "\n-- this code runs when the player leave combat",
		["on_entercombat"] = "\n-- this code runs when the player enters in combat",
	},
	["mythic_plus"] = {
		["make_overall_boss_only"] = false,
		["mythicrun_chart_frame"] = {
		},
		["merge_boss_trash"] = true,
		["delay_to_show_graphic"] = 5,
		["always_in_combat"] = false,
		["make_overall_when_done"] = true,
		["delete_trash_after_merge"] = true,
		["show_damage_graphic"] = true,
		["mythicrun_chart_frame_ready"] = {
		},
		["boss_dedicated_segment"] = true,
		["mythicrun_chart_frame_minimized"] = {
		},
		["last_mythicrun_chart"] = {
		},
	},
	["mobs_data_compiled"] = {
	},
	["global_plugin_database"] = {
	},
	["realm_sync"] = true,
	["createauraframe"] = {
	},
	["item_level_pool"] = {
	},
	["switchSaved"] = {
		["slots"] = 4,
		["table"] = {
			{
				["atributo"] = 1,
				["sub_atributo"] = 1,
			}, -- [1]
			{
				["atributo"] = 2,
				["sub_atributo"] = 1,
			}, -- [2]
			{
				["atributo"] = 1,
				["sub_atributo"] = 6,
			}, -- [3]
			{
				["atributo"] = 4,
				["sub_atributo"] = 5,
			}, -- [4]
			{
			}, -- [5]
			{
			}, -- [6]
			{
			}, -- [7]
			{
			}, -- [8]
			{
			}, -- [9]
			{
			}, -- [10]
			{
			}, -- [11]
			{
			}, -- [12]
			{
			}, -- [13]
			{
			}, -- [14]
			{
			}, -- [15]
			{
			}, -- [16]
			{
			}, -- [17]
			{
			}, -- [18]
			{
			}, -- [19]
			{
			}, -- [20]
			{
			}, -- [21]
			{
			}, -- [22]
			{
			}, -- [23]
			{
			}, -- [24]
		},
	},
	["savedCustomSpells"] = {
		{
			3, -- [1]
			"Environment (Falling)", -- [2]
			"Interface\\ICONS\\Spell_Magic_FeatherFall", -- [3]
		}, -- [1]
		{
			1, -- [1]
			"Melee", -- [2]
			"Interface\\ICONS\\INV_Sword_04", -- [3]
		}, -- [2]
		{
			4, -- [1]
			"Environment (Drowning)", -- [2]
			"Interface\\ICONS\\Ability_Suffocate", -- [3]
		}, -- [3]
		{
			5, -- [1]
			"Environment (Fatigue)", -- [2]
			"Interface\\ICONS\\Spell_Arcane_MindMastery", -- [3]
		}, -- [4]
		{
			7, -- [1]
			"Environment (Lava)", -- [2]
			"Interface\\ICONS\\Ability_Rhyolith_Volcano", -- [3]
		}, -- [5]
	},
	["latest_news_saw"] = "v1.13.2.159",
	["update_warning_timeout"] = 10,
	["raid_data"] = {
	},
	["tutorial"] = {
		["bookmark_tutorial"] = false,
		["main_help_button"] = 187,
		["FULL_DELETE_WINDOW"] = true,
		["WINDOW_GROUP_MAKING1"] = true,
		["DETAILS_INFO_TUTORIAL1"] = true,
		["alert_frames"] = {
			false, -- [1]
			false, -- [2]
			false, -- [3]
			false, -- [4]
			false, -- [5]
			false, -- [6]
		},
		["unlock_button"] = 4,
		["ATTRIBUTE_SELECT_TUTORIAL1"] = true,
		["DETAILS_INFO_TUTORIAL2"] = 10,
		["logons"] = 187,
		["version_announce"] = 0,
		["MIN_COMBAT_TIME"] = true,
		["ctrl_click_close_tutorial"] = false,
		["OPTIONS_PANEL_OPENED"] = true,
		["STREAMER_PLUGIN_FIRSTRUN"] = true,
	},
	["always_use_profile_name"] = "",
	["savedStyles"] = {
	},
	["always_use_profile_exception"] = {
	},
	["details_auras"] = {
	},
	["show_totalhitdamage_on_overkill"] = false,
	["savedTimeCaptures"] = {
	},
	["plugin_window_pos"] = {
		["y"] = 21.3332214355469,
		["x"] = 143.012420654297,
		["point"] = "CENTER",
		["scale"] = 1,
	},
	["custom"] = {
		{
			["source"] = false,
			["author"] = "Details!",
			["total_script"] = "				local value, top, total, combat, instance = ...\n				return floor (value)\n			",
			["desc"] = "Show the crowd control amount for each player.",
			["attribute"] = false,
			["script"] = "				local combat, instance_container, instance = ...\n				local total, top, amount = 0, 0, 0\n\n				local misc_actors = combat:GetActorList (DETAILS_ATTRIBUTE_MISC)\n\n				for index, character in ipairs (misc_actors) do\n					if (character.cc_done and character:IsPlayer()) then\n						local cc_done = floor (character.cc_done)\n						instance_container:AddValue (character, cc_done)\n						total = total + cc_done\n						if (cc_done > top) then\n							top = cc_done\n						end\n						amount = amount + 1\n					end\n				end\n\n				return total, top, amount\n			",
			["name"] = "Crowd Control Done",
			["tooltip"] = "				local actor, combat, instance = ...\n				local spells = {}\n				for spellid, spell in pairs (actor.cc_done_spells._ActorTable) do\n				    tinsert (spells, {spellid, spell.counter})\n				end\n\n				table.sort (spells, _detalhes.Sort2)\n\n				for index, spell in ipairs (spells) do\n				    local name, _, icon = GetSpellInfo (spell [1])\n				    GameCooltip:AddLine (name, spell [2])\n				    _detalhes:AddTooltipBackgroundStatusbar()\n				    GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				end\n\n				local targets = {}\n				for playername, amount in pairs (actor.cc_done_targets) do\n				    tinsert (targets, {playername, amount})\n				end\n\n				table.sort (targets, _detalhes.Sort2)\n\n				_detalhes:AddTooltipSpellHeaderText (\"Targets\", \"yellow\", #targets)\n				local class, _, _, _, _, r, g, b = _detalhes:GetClass (actor.nome)\n				_detalhes:AddTooltipHeaderStatusbar (1, 1, 1, 0.6)\n\n				for index, target in ipairs (targets) do\n				    GameCooltip:AddLine (target[1], target [2])\n				    _detalhes:AddTooltipBackgroundStatusbar()\n				    \n				    local class, _, _, _, _, r, g, b = _detalhes:GetClass (target [1])\n				    if (class and class ~= \"UNKNOW\") then\n					local texture, l, r, t, b = _detalhes:GetClassIcon (class)\n					GameCooltip:AddIcon (\"Interface\\\\AddOns\\\\Details\\\\images\\\\classes_small_alpha\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height, l, r, t, b)\n				    else\n					GameCooltip:AddIcon (\"Interface\\\\GossipFrame\\\\IncompleteQuestIcon\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    end\n				    --\n				end\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\ICONS\\Spell_Frost_FreezingBreath",
			["script_version"] = 11,
		}, -- [1]
		{
			["source"] = false,
			["author"] = "Details!",
			["total_script"] = "				local value, top, total, combat, instance = ...\n				return floor (value)\n			",
			["desc"] = "Show the amount of crowd control received for each player.",
			["attribute"] = false,
			["script"] = "				local combat, instance_container, instance = ...\n				local total, top, amt = 0, 0, 0\n\n				local misc_actors = combat:GetActorList (DETAILS_ATTRIBUTE_MISC)\n				DETAILS_CUSTOM_CC_RECEIVED_CACHE = DETAILS_CUSTOM_CC_RECEIVED_CACHE or {}\n				wipe (DETAILS_CUSTOM_CC_RECEIVED_CACHE)\n\n				for index, character in ipairs (misc_actors) do\n				    if (character.cc_done and character:IsPlayer()) then\n					\n					for player_name, amount in pairs (character.cc_done_targets) do\n					    local target = combat (1, player_name) or combat (2, player_name)\n					    if (target and target:IsPlayer()) then\n						instance_container:AddValue (target, amount)\n						total = total + amount\n						if (amount > top) then\n						    top = amount\n						end\n						if (not DETAILS_CUSTOM_CC_RECEIVED_CACHE [player_name]) then\n						    DETAILS_CUSTOM_CC_RECEIVED_CACHE [player_name] = true\n						    amt = amt + 1\n						end\n					    end\n					end\n					\n				    end\n				end\n\n				return total, top, amt\n			",
			["name"] = "Crowd Control Received",
			["tooltip"] = "				local actor, combat, instance = ...\n				local name = actor:name()\n				local spells, from = {}, {}\n				local misc_actors = combat:GetActorList (DETAILS_ATTRIBUTE_MISC)\n\n				for index, character in ipairs (misc_actors) do\n				    if (character.cc_done and character:IsPlayer()) then\n					local on_actor = character.cc_done_targets [name]\n					if (on_actor) then\n					    tinsert (from, {character:name(), on_actor})\n					    \n					    for spellid, spell in pairs (character.cc_done_spells._ActorTable) do\n						\n						local spell_on_actor = spell.targets [name]\n						if (spell_on_actor) then\n						    local has_spell\n						    for index, spell_table in ipairs (spells) do\n							if (spell_table [1] == spellid) then\n							    spell_table [2] = spell_table [2] + spell_on_actor\n							    has_spell = true\n							end\n						    end\n						    if (not has_spell) then\n							tinsert (spells, {spellid, spell_on_actor}) \n						    end\n						end\n						\n					    end            \n					end\n				    end\n				end\n\n				table.sort (from, _detalhes.Sort2)\n				table.sort (spells, _detalhes.Sort2)\n\n				for index, spell in ipairs (spells) do\n				    local name, _, icon = GetSpellInfo (spell [1])\n				    GameCooltip:AddLine (name, spell [2])\n				    _detalhes:AddTooltipBackgroundStatusbar()\n				    GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)    \n				end\n\n				_detalhes:AddTooltipSpellHeaderText (\"From\", \"yellow\", #from)\n				_detalhes:AddTooltipHeaderStatusbar (1, 1, 1, 0.6)\n\n				for index, t in ipairs (from) do\n				    GameCooltip:AddLine (t[1], t[2])\n				    _detalhes:AddTooltipBackgroundStatusbar()\n				    \n				    local class, _, _, _, _, r, g, b = _detalhes:GetClass (t [1])\n				    if (class and class ~= \"UNKNOW\") then\n					local texture, l, r, t, b = _detalhes:GetClassIcon (class)\n					GameCooltip:AddIcon (\"Interface\\\\AddOns\\\\Details\\\\images\\\\classes_small_alpha\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height, l, r, t, b)\n				    else\n					GameCooltip:AddIcon (\"Interface\\\\GossipFrame\\\\IncompleteQuestIcon\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    end     \n				    \n				end\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\ICONS\\Spell_Frost_ChainsOfIce",
			["script_version"] = 3,
		}, -- [2]
		{
			["source"] = false,
			["total_script"] = "local value, top, total, combat, instance = ...\n			local dps = _detalhes:ToK (floor (value) / combat:GetCombatTime())\n			local percent = string.format (\"%.1f\", value/total*100)\n\n			return math.floor (value) .. \" (\" .. dps .. \", \" .. percent .. \"\"",
			["author"] = "Details!",
			["percent_script"] = "				local value, top, total, combat, instance = ...\n				local dps = _detalhes:ToK (floor (value) / combat:GetCombatTime())\n				local percent = string.format (\"%.1f\", value/total*100)\n				return dps .. \", \" .. percent\n			",
			["desc"] = "Show your spells in the window.",
			["attribute"] = false,
			["script"] = "				--get the parameters passed\n				local combat, instance_container, instance = ...\n				--declade the values to return\n				local total, top, amount = 0, 0, 0\n\n				local player\n				local pet_attribute\n				\n				local role = DetailsFramework.UnitGroupRolesAssigned (\"player\")\n				local spec = DetailsFramework.GetSpecialization()\n				role = spec and DetailsFramework.GetSpecializationRole (spec) or role\n\n				if (role == \"DAMAGER\") then\n					player = combat (DETAILS_ATTRIBUTE_DAMAGE, _detalhes.playername)\n					pet_attribute = DETAILS_ATTRIBUTE_DAMAGE\n				elseif (role == \"HEALER\") then    \n					player = combat (DETAILS_ATTRIBUTE_HEAL, _detalhes.playername)\n					pet_attribute = DETAILS_ATTRIBUTE_HEAL\n				else\n					player = combat (DETAILS_ATTRIBUTE_DAMAGE, _detalhes.playername)\n					pet_attribute = DETAILS_ATTRIBUTE_DAMAGE\n				end\n\n				--do the loop\n\n				if (player) then\n					local spells = player:GetSpellList()\n					for spellid, spell in pairs (spells) do\n						instance_container:AddValue (spell, spell.total)\n						total = total + spell.total\n						if (top < spell.total) then\n							top = spell.total\n						end\n						amount = amount + 1\n					end\n				    \n					for _, PetName in ipairs (player.pets) do\n						local pet = combat (pet_attribute, PetName)\n						if (pet) then\n							for spellid, spell in pairs (pet:GetSpellList()) do\n								instance_container:AddValue (spell, spell.total, nil, \" (\" .. PetName:gsub ((\" <.*\"), \"\") .. \")\")\n								total = total + spell.total\n								if (top < spell.total) then\n									top = spell.total\n								end\n								amount = amount + 1\n							end\n						end\n					end\n				end\n\n				--return the values\n				return total, top, amount\n			",
			["name"] = "My Spells",
			["tooltip"] = "				--config:\n				--Background RBG and Alpha:\n				local R, G, B, A = 0, 0, 0, 0.75\n				local R, G, B, A = 0.1960, 0.1960, 0.1960, 0.8697\n				\n				--get the parameters passed\n				local spell, combat, instance = ...\n				\n				--get the cooltip object (we dont use the convencional GameTooltip here)\n				local GC = GameCooltip\n				GC:SetOption (\"YSpacingMod\", 2)\n				local role = DetailsFramework.UnitGroupRolesAssigned (\"player\")\n				\n				if (spell.n_dmg) then\n					\n					local spellschool, schooltext = spell.spellschool, \"\"\n					if (spellschool) then\n						local t = _detalhes.spells_school [spellschool]\n						if (t and t.name) then\n							schooltext = t.formated\n						end\n					end\n					\n					local total_hits = spell.counter\n					local combat_time = instance.showing:GetCombatTime()\n					\n					local debuff_uptime_total, cast_string = \"\", \"\"\n					local misc_actor = instance.showing (4, _detalhes.playername)\n					if (misc_actor) then\n						local debuff_uptime = misc_actor.debuff_uptime_spells and misc_actor.debuff_uptime_spells._ActorTable [spell.id] and misc_actor.debuff_uptime_spells._ActorTable [spell.id].uptime\n						if (debuff_uptime) then\n							debuff_uptime_total = floor (debuff_uptime / instance.showing:GetCombatTime() * 100)\n						end\n						\n						local spell_cast = misc_actor.spell_cast and misc_actor.spell_cast [spell.id]\n						\n						if (not spell_cast and misc_actor.spell_cast) then\n							local spellname = GetSpellInfo (spell.id)\n							for casted_spellid, amount in pairs (misc_actor.spell_cast) do\n								local casted_spellname = GetSpellInfo (casted_spellid)\n								if (casted_spellname == spellname) then\n									spell_cast = amount .. \" (|cFFFFFF00?|r)\"\n								end\n							end\n						end\n						if (not spell_cast) then\n							spell_cast = \"(|cFFFFFF00?|r)\"\n						end\n						cast_string = cast_string .. spell_cast\n					end\n					\n					--Cooltip code\n					GC:AddLine (\"Casts:\", cast_string or \"?\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					if (debuff_uptime_total ~= \"\") then\n						GC:AddLine (\"Uptime:\", (debuff_uptime_total or \"?\") .. \"%\")\n						GC:AddStatusBar (100, 1, R, G, B, A)\n						GC:AddIcon (\"\", 1, 1, 1, 16)\n					end\n					\n					GC:AddLine (\"Hits:\", spell.counter)\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					local average = spell.total / total_hits\n					GC:AddLine (\"Average:\", _detalhes:ToK (average))\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					GC:AddLine (\"E-Dps:\", _detalhes:ToK (spell.total / combat_time))\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					GC:AddLine (\"School:\", schooltext)\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					--GC:AddLine (\" \")\n					\n					GC:AddLine (\"Normal Hits: \", spell.n_amt .. \" (\" ..floor ( spell.n_amt/total_hits*100) .. \"%)\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					local n_average = spell.n_dmg / spell.n_amt\n					local T = (combat_time*spell.n_dmg)/spell.total\n					local P = average/n_average*100\n					T = P*T/100\n					\n					GC:AddLine (\"Average / E-Dps: \",  _detalhes:ToK (n_average) .. \" / \" .. format (\"%.1f\",spell.n_dmg / T ))\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					--GC:AddLine (\" \")\n					\n					GC:AddLine (\"Critical Hits: \", spell.c_amt .. \" (\" ..floor ( spell.c_amt/total_hits*100) .. \"%)\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					if (spell.c_amt > 0) then\n						local c_average = spell.c_dmg/spell.c_amt\n						local T = (combat_time*spell.c_dmg)/spell.total\n						local P = average/c_average*100\n						T = P*T/100\n						local crit_dps = spell.c_dmg / T\n						\n						GC:AddLine (\"Average / E-Dps: \",  _detalhes:ToK (c_average) .. \" / \" .. _detalhes:comma_value (crit_dps))\n					else\n						GC:AddLine (\"Average / E-Dps: \",  \"0 / 0\")    \n					end\n					\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					GC:AddLine (\"Multistrike: \", spell.m_amt .. \" (\" ..floor ( spell.m_amt/total_hits*100) .. \"%)\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n					\n					GC:AddLine (\"On Normal / On Critical:\", spell.m_amt - spell.m_crit .. \"  / \" .. spell.m_crit)\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					GC:AddIcon (\"\", 1, 1, 1, 16)\n					\n				elseif (spell.n_curado) then\n					\n					local spellschool, schooltext = spell.spellschool, \"\"\n					if (spellschool) then\n						local t = _detalhes.spells_school [spellschool]\n						if (t and t.name) then\n							schooltext = t.formated\n						end\n					end\n					\n					local total_hits = spell.counter\n					local combat_time = instance.showing:GetCombatTime()\n					\n					--Cooltip code\n					GC:AddLine (\"Hits:\", spell.counter)\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					local average = spell.total / total_hits\n					GC:AddLine (\"Average:\", _detalhes:ToK (average))\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					GC:AddLine (\"E-Hps:\", _detalhes:ToK (spell.total / combat_time))\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					GC:AddLine (\"School:\", schooltext)\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					--GC:AddLine (\" \")\n					\n					GC:AddLine (\"Normal Hits: \", spell.n_amt .. \" (\" ..floor ( spell.n_amt/total_hits*100) .. \"%)\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					local n_average = spell.n_curado / spell.n_amt\n					local T = (combat_time*spell.n_curado)/spell.total\n					local P = average/n_average*100\n					T = P*T/100\n					\n					GC:AddLine (\"Average / E-Dps: \",  _detalhes:ToK (n_average) .. \" / \" .. format (\"%.1f\",spell.n_curado / T ))\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					--GC:AddLine (\" \")\n					\n					GC:AddLine (\"Critical Hits: \", spell.c_amt .. \" (\" ..floor ( spell.c_amt/total_hits*100) .. \"%)\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					if (spell.c_amt > 0) then\n						local c_average = spell.c_curado/spell.c_amt\n						local T = (combat_time*spell.c_curado)/spell.total\n						local P = average/c_average*100\n						T = P*T/100\n						local crit_dps = spell.c_curado / T\n						\n						GC:AddLine (\"Average / E-Hps: \",  _detalhes:ToK (c_average) .. \" / \" .. _detalhes:comma_value (crit_dps))\n					else\n						GC:AddLine (\"Average / E-Hps: \",  \"0 / 0\")    \n					end\n					\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					--GC:AddLine (\" \")\n					\n					GC:AddLine (\"Multistrike: \", spell.m_amt .. \" (\" ..floor ( spell.m_amt/total_hits*100) .. \"%)\")\n					GC:AddStatusBar (100, 1, R, G, B, A)\n					\n					GC:AddLine (\"On Normal / On Critical:\", spell.m_amt - spell.m_crit .. \"  / \" .. spell.m_crit)\n					GC:AddStatusBar (100, 1, R, G, B, A)\n				end\n\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\CHATFRAME\\UI-ChatIcon-Battlenet",
			["script_version"] = 9,
		}, -- [3]
		{
			["source"] = false,
			["author"] = "Details!",
			["desc"] = "Show the amount of damage applied on targets marked with skull.",
			["tooltip"] = "				--get the parameters passed\n				local actor, combat, instance = ...\n\n				--get the cooltip object (we dont use the convencional GameTooltip here)\n				local GameCooltip = GameCooltip\n\n				--Cooltip code\n				local format_func = Details:GetCurrentToKFunction()\n\n				--Cooltip code\n				local RaidTargets = actor.raid_targets\n\n				local DamageOnStar = RaidTargets [128]\n				if (DamageOnStar) then\n				    --RAID_TARGET_8 is the built-in localized word for 'Skull'.\n				    GameCooltip:AddLine (RAID_TARGET_8 .. \":\", format_func (_, DamageOnStar))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_8\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n			",
			["attribute"] = false,
			["name"] = "Damage On Skull Marked Targets",
			["script"] = "				--get the parameters passed\n				local Combat, CustomContainer, Instance = ...\n				--declade the values to return\n				local total, top, amount = 0, 0, 0\n				\n				--raid target flags: \n				-- 128: skull \n				-- 64: cross\n				-- 32: square\n				-- 16: moon\n				-- 8: triangle\n				-- 4: diamond\n				-- 2: circle\n				-- 1: star\n				\n				--do the loop\n				for _, actor in ipairs (Combat:GetActorList (DETAILS_ATTRIBUTE_DAMAGE)) do\n				    if (actor:IsPlayer()) then\n					if (actor.raid_targets [128]) then\n					    CustomContainer:AddValue (actor, actor.raid_targets [128])\n					end        \n				    end\n				end\n\n				--if not managed inside the loop, get the values of total, top and amount\n				total, top = CustomContainer:GetTotalAndHighestValue()\n				amount = CustomContainer:GetNumActors()\n\n				--return the values\n				return total, top, amount\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\TARGETINGFRAME\\UI-RaidTargetingIcon_8",
			["script_version"] = 4,
		}, -- [4]
		{
			["source"] = false,
			["author"] = "Details!",
			["desc"] = "Show the amount of damage applied on targets marked with any other mark.",
			["tooltip"] = "				--get the parameters passed\n				local actor, combat, instance = ...\n\n				--get the cooltip object\n				local GameCooltip = GameCooltip\n\n				local format_func = Details:GetCurrentToKFunction()\n\n				--Cooltip code\n				local RaidTargets = actor.raid_targets\n\n				local DamageOnStar = RaidTargets [1]\n				if (DamageOnStar) then\n				    GameCooltip:AddLine (RAID_TARGET_1 .. \":\", format_func (_, DamageOnStar))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_1\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n\n				local DamageOnCircle = RaidTargets [2]\n				if (DamageOnCircle) then\n				    GameCooltip:AddLine (RAID_TARGET_2 .. \":\", format_func (_, DamageOnCircle))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_2\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n\n				local DamageOnDiamond = RaidTargets [4]\n				if (DamageOnDiamond) then\n				    GameCooltip:AddLine (RAID_TARGET_3 .. \":\", format_func (_, DamageOnDiamond))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_3\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n\n				local DamageOnTriangle = RaidTargets [8]\n				if (DamageOnTriangle) then\n				    GameCooltip:AddLine (RAID_TARGET_4 .. \":\", format_func (_, DamageOnTriangle))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_4\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n\n				local DamageOnMoon = RaidTargets [16]\n				if (DamageOnMoon) then\n				    GameCooltip:AddLine (RAID_TARGET_5 .. \":\", format_func (_, DamageOnMoon))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_5\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n\n				local DamageOnSquare = RaidTargets [32]\n				if (DamageOnSquare) then\n				    GameCooltip:AddLine (RAID_TARGET_6 .. \":\", format_func (_, DamageOnSquare))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_6\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n\n				local DamageOnCross = RaidTargets [64]\n				if (DamageOnCross) then\n				    GameCooltip:AddLine (RAID_TARGET_7 .. \":\", format_func (_, DamageOnCross))\n				    GameCooltip:AddIcon (\"Interface\\\\TARGETINGFRAME\\\\UI-RaidTargetingIcon_7\", 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				    Details:AddTooltipBackgroundStatusbar()\n				end\n			",
			["attribute"] = false,
			["name"] = "Damage On Other Marked Targets",
			["script"] = "				--get the parameters passed\n				local Combat, CustomContainer, Instance = ...\n				--declade the values to return\n				local total, top, amount = 0, 0, 0\n\n				--do the loop\n				for _, actor in ipairs (Combat:GetActorList (DETAILS_ATTRIBUTE_DAMAGE)) do\n				    if (actor:IsPlayer()) then\n					local total = (actor.raid_targets [1] or 0) --star\n					total = total + (actor.raid_targets [2] or 0) --circle\n					total = total + (actor.raid_targets [4] or 0) --diamond\n					total = total + (actor.raid_targets [8] or 0) --tiangle\n					total = total + (actor.raid_targets [16] or 0) --moon\n					total = total + (actor.raid_targets [32] or 0) --square\n					total = total + (actor.raid_targets [64] or 0) --cross\n					\n					if (total > 0) then\n					    CustomContainer:AddValue (actor, total)\n					end\n				    end\n				end\n\n				--if not managed inside the loop, get the values of total, top and amount\n				total, top = CustomContainer:GetTotalAndHighestValue()\n				amount = CustomContainer:GetNumActors()\n\n				--return the values\n				return total, top, amount\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\TARGETINGFRAME\\UI-RaidTargetingIcon_5",
			["script_version"] = 4,
		}, -- [5]
		{
			["source"] = false,
			["author"] = "Details!",
			["desc"] = "Show who in your raid used a potion during the encounter.",
			["tooltip"] = "			--init:\n			local player, combat, instance = ...\n\n			--get the debuff container for potion of focus\n			local debuff_uptime_container = player.debuff_uptime and player.debuff_uptime_spells and player.debuff_uptime_spells._ActorTable\n			if (debuff_uptime_container) then\n			    local focus_potion = debuff_uptime_container [DETAILS_FOCUS_POTION_ID]\n			    if (focus_potion) then\n				local name, _, icon = GetSpellInfo (DETAILS_FOCUS_POTION_ID)\n				GameCooltip:AddLine (name, 1) --> can use only 1 focus potion (can't be pre-potion)\n				_detalhes:AddTooltipBackgroundStatusbar()\n				GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n			    end\n			end\n\n			--get the buff container for all the others potions\n			local buff_uptime_container = player.buff_uptime and player.buff_uptime_spells and player.buff_uptime_spells._ActorTable\n			if (buff_uptime_container) then\n			    --potion of the jade serpent\n			    local jade_serpent_potion = buff_uptime_container [DETAILS_INT_POTION_ID]\n			    if (jade_serpent_potion) then\n				local name, _, icon = GetSpellInfo (DETAILS_INT_POTION_ID)\n				GameCooltip:AddLine (name, jade_serpent_potion.activedamt)\n				_detalhes:AddTooltipBackgroundStatusbar()\n				GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n			    end\n			    \n			    --potion of mogu power\n			    local mogu_power_potion = buff_uptime_container [DETAILS_STR_POTION_ID]\n			    if (mogu_power_potion) then\n				local name, _, icon = GetSpellInfo (DETAILS_STR_POTION_ID)\n				GameCooltip:AddLine (name, mogu_power_potion.activedamt)\n				_detalhes:AddTooltipBackgroundStatusbar()\n				GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n			    end\n			    \n			    --mana potion\n			    local mana_potion = buff_uptime_container [DETAILS_MANA_POTION_ID]\n			    if (mana_potion) then\n				local name, _, icon = GetSpellInfo (DETAILS_MANA_POTION_ID)\n				GameCooltip:AddLine (name, mana_potion.activedamt)\n				_detalhes:AddTooltipBackgroundStatusbar()\n				GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n			    end\n			    \n			    --prolongued power\n			    local prolongued_power = buff_uptime_container [DETAILS_AGI_POTION_ID]\n			    if (prolongued_power) then\n				local name, _, icon = GetSpellInfo (DETAILS_AGI_POTION_ID)\n				GameCooltip:AddLine (name, prolongued_power.activedamt)\n				_detalhes:AddTooltipBackgroundStatusbar()\n				GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n			    end\n			    \n			    --potion of the mountains\n			    local mountains_potion = buff_uptime_container [DETAILS_STAMINA_POTION_ID]\n			    if (mountains_potion) then\n				local name, _, icon = GetSpellInfo (DETAILS_STAMINA_POTION_ID)\n				GameCooltip:AddLine (name, mountains_potion.activedamt)\n				_detalhes:AddTooltipBackgroundStatusbar()\n				GameCooltip:AddIcon (icon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n			    end\n			end\n		",
			["attribute"] = false,
			["name"] = "Potion Used",
			["script"] = "				--init:\n				local combat, instance_container, instance = ...\n				local total, top, amount = 0, 0, 0\n\n				--get the misc actor container\n				local misc_container = combat:GetActorList ( DETAILS_ATTRIBUTE_MISC )\n\n				--do the loop:\n				for _, player in ipairs ( misc_container ) do \n				    \n				    --only player in group\n				    if (player:IsGroupPlayer()) then\n					\n					local found_potion = false\n					\n					--get the spell debuff uptime container\n					local debuff_uptime_container = player.debuff_uptime and player.debuff_uptime_spells and player.debuff_uptime_spells._ActorTable\n					if (debuff_uptime_container) then\n					    --potion of focus (can't use as pre-potion, so, its amount is always 1\n					    local focus_potion = debuff_uptime_container [DETAILS_FOCUS_POTION_ID]\n\n					    if (focus_potion) then\n						total = total + 1\n						found_potion = true\n						if (top < 1) then\n						    top = 1\n						end\n						--add amount to the player \n						instance_container:AddValue (player, 1)\n					    end\n					end\n					\n					--get the spell buff uptime container\n					local buff_uptime_container = player.buff_uptime and player.buff_uptime_spells and player.buff_uptime_spells._ActorTable\n					if (buff_uptime_container) then\n					    \n					    --potion of the jade serpent\n					    local jade_serpent_potion = buff_uptime_container [DETAILS_INT_POTION_ID]\n					    if (jade_serpent_potion) then\n						local used = jade_serpent_potion.activedamt\n						if (used > 0) then\n						    total = total + used\n						    found_potion = true\n						    if (used > top) then\n							top = used\n						    end\n						    --add amount to the player \n						    instance_container:AddValue (player, used)\n						end\n					    end\n					    \n					    --potion of mogu power\n					    local mogu_power_potion = buff_uptime_container [DETAILS_STR_POTION_ID]\n					    if (mogu_power_potion) then\n						local used = mogu_power_potion.activedamt\n						if (used > 0) then\n						    total = total + used\n						    found_potion = true\n						    if (used > top) then\n							top = used\n						    end\n						    --add amount to the player \n						    instance_container:AddValue (player, used)\n						end\n					    end\n					    \n					    --mana potion\n					    local mana_potion = buff_uptime_container [DETAILS_MANA_POTION_ID]\n					    if (mana_potion) then\n						local used = mana_potion.activedamt\n						if (used > 0) then\n						    total = total + used\n						    found_potion = true\n						    if (used > top) then\n							top = used\n						    end\n						    --add amount to the player \n						    instance_container:AddValue (player, used)\n						end\n					    end\n					    \n					    --potion of prolongued power\n					    local prolongued_power = buff_uptime_container [DETAILS_AGI_POTION_ID]\n					    if (prolongued_power) then\n						local used = prolongued_power.activedamt\n						if (used > 0) then\n						    total = total + used\n						    found_potion = true\n						    if (used > top) then\n							top = used\n						    end\n						    --add amount to the player \n						    instance_container:AddValue (player, used)\n						end\n					    end\n					    \n					    --potion of the mountains\n					    local mountains_potion = buff_uptime_container [DETAILS_STAMINA_POTION_ID]\n					    if (mountains_potion) then\n						local used = mountains_potion.activedamt\n						if (used > 0) then\n						    total = total + used\n						    found_potion = true\n						    if (used > top) then\n							top = used\n						    end\n						    --add amount to the player \n						    instance_container:AddValue (player, used)\n						end\n					    end\n					end\n					\n					if (found_potion) then\n					    amount = amount + 1\n					end    \n				    end\n				end\n\n				--return:\n				return total, top, amount\n				",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\ICONS\\INV_Potion_03",
			["script_version"] = 5,
		}, -- [6]
		{
			["source"] = false,
			["desc"] = "Show who in your raid group used the healthstone or a heal potion.",
			["author"] = "Details! Team",
			["percent_script"] = false,
			["total_script"] = false,
			["attribute"] = false,
			["tooltip"] = "			--get the parameters passed\n			local actor, combat, instance = ...\n			\n			--get the cooltip object (we dont use the convencional GameTooltip here)\n			local GameCooltip = GameCooltip\n			local R, G, B, A = 0, 0, 0, 0.75\n			\n			local hs = actor:GetSpell (6262)\n			if (hs) then\n				GameCooltip:AddLine (select (1, GetSpellInfo(6262)),  _detalhes:ToK(hs.total))\n				GameCooltip:AddIcon (select (3, GetSpellInfo (6262)), 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				GameCooltip:AddStatusBar (100, 1, R, G, B, A)\n			end\n			\n			local pot = actor:GetSpell (DETAILS_HEALTH_POTION_ID)\n			if (pot) then\n				GameCooltip:AddLine (select (1, GetSpellInfo(DETAILS_HEALTH_POTION_ID)),  _detalhes:ToK(pot.total))\n				GameCooltip:AddIcon (select (3, GetSpellInfo (DETAILS_HEALTH_POTION_ID)), 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				GameCooltip:AddStatusBar (100, 1, R, G, B, A)\n			end\n			\n			local pot = actor:GetSpell (DETAILS_REJU_POTION_ID)\n			if (pot) then\n				GameCooltip:AddLine (select (1, GetSpellInfo(DETAILS_REJU_POTION_ID)),  _detalhes:ToK(pot.total))\n				GameCooltip:AddIcon (select (3, GetSpellInfo (DETAILS_REJU_POTION_ID)), 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n				GameCooltip:AddStatusBar (100, 1, R, G, B, A)\n			end\n\n			--Cooltip code\n			",
			["name"] = "Health Potion & Stone",
			["script"] = "			--get the parameters passed\n			local combat, instance_container, instance = ...\n			--declade the values to return\n			local total, top, amount = 0, 0, 0\n			\n			--do the loop\n			local AllHealCharacters = combat:GetActorList (DETAILS_ATTRIBUTE_HEAL)\n			for index, character in ipairs (AllHealCharacters) do\n				local AllSpells = character:GetSpellList()\n				local found = false\n				for spellid, spell in pairs (AllSpells) do\n					if (DETAILS_HEALTH_POTION_LIST [spellid]) then\n						instance_container:AddValue (character, spell.total)\n						total = total + spell.total\n						if (top < spell.total) then\n							top = spell.total\n						end\n						found = true\n					end\n				end\n			\n				if (found) then\n					amount = amount + 1\n				end\n			end\n			--loop end\n			--return the values\n			return total, top, amount\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\ICONS\\INV_Stone_04",
			["script_version"] = 15,
		}, -- [7]
		{
			["source"] = false,
			["tooltip"] = "				\n			",
			["author"] = "Details!",
			["percent_script"] = "				local value, top, total, combat, instance = ...\n				return string.format (\"%.1f\", value/top*100)\n			",
			["desc"] = "Tells how much time each character spent doing damage.",
			["attribute"] = false,
			["total_script"] = "				local value, top, total, combat, instance = ...\n				local minutos, segundos = math.floor (value/60), math.floor (value%60)\n				return minutos .. \"m \" .. segundos .. \"s\"\n			",
			["name"] = "Damage Activity Time",
			["script"] = "				--init:\n				local combat, instance_container, instance = ...\n				local total, amount = 0, 0\n\n				--get the misc actor container\n				local damage_container = combat:GetActorList ( DETAILS_ATTRIBUTE_DAMAGE )\n				\n				--do the loop:\n				for _, player in ipairs ( damage_container ) do \n					if (player.grupo) then\n						local activity = player:Tempo()\n						total = total + activity\n						amount = amount + 1\n						--add amount to the player \n						instance_container:AddValue (player, activity)\n					end\n				end\n				\n				--return:\n				return total, combat:GetCombatTime(), amount\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\Buttons\\UI-MicroStream-Red",
			["script_version"] = 2,
		}, -- [8]
		{
			["source"] = false,
			["tooltip"] = "				\n			",
			["author"] = "Details!",
			["percent_script"] = "				local value, top, total, combat, instance = ...\n				return string.format (\"%.1f\", value/top*100)\n			",
			["desc"] = "Tells how much time each character spent doing healing.",
			["attribute"] = false,
			["total_script"] = "				local value, top, total, combat, instance = ...\n				local minutos, segundos = math.floor (value/60), math.floor (value%60)\n				return minutos .. \"m \" .. segundos .. \"s\"\n			",
			["name"] = "Healing Activity Time",
			["script"] = "				--init:\n				local combat, instance_container, instance = ...\n				local total, top, amount = 0, 0, 0\n\n				--get the misc actor container\n				local damage_container = combat:GetActorList ( DETAILS_ATTRIBUTE_HEAL )\n				\n				--do the loop:\n				for _, player in ipairs ( damage_container ) do \n					if (player.grupo) then\n						local activity = player:Tempo()\n						total = total + activity\n						amount = amount + 1\n						--add amount to the player \n						instance_container:AddValue (player, activity)\n					end\n				end\n				\n				--return:\n				return total, combat:GetCombatTime(), amount\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\Buttons\\UI-MicroStream-Green",
			["script_version"] = 2,
		}, -- [9]
		{
			["source"] = false,
			["author"] = "Details!",
			["desc"] = "Damage done to shields",
			["tooltip"] = "				--get the parameters passed\n				local actor, Combat, instance = ...\n\n				--get the cooltip object (we dont use the convencional GameTooltip here)\n				local GameCooltip = GameCooltip\n\n				--Cooltip code\n				--get the actor total damage absorbed\n				local totalAbsorb = actor.totalabsorbed\n				local format_func = Details:GetCurrentToKFunction()\n\n				--get the damage absorbed by all the actor pets\n				for petIndex, petName in ipairs (actor.pets) do\n				    local pet = Combat :GetActor (1, petName)\n				    if (pet) then\n					totalAbsorb = totalAbsorb + pet.totalabsorbed\n				    end\n				end\n\n				GameCooltip:AddLine (actor:Name(), format_func (_, actor.totalabsorbed))\n				Details:AddTooltipBackgroundStatusbar()\n\n				for petIndex, petName in ipairs (actor.pets) do\n				    local pet = Combat :GetActor (1, petName)\n				    if (pet) then\n					totalAbsorb = totalAbsorb + pet.totalabsorbed\n					\n					GameCooltip:AddLine (petName, format_func (_, pet.totalabsorbed))\n					Details:AddTooltipBackgroundStatusbar()        \n					\n				    end\n				end\n			",
			["attribute"] = false,
			["name"] = "Damage on Shields",
			["script"] = "				--get the parameters passed\n				local Combat, CustomContainer, Instance = ...\n				--declade the values to return\n				local total, top, amount = 0, 0, 0\n\n				--do the loop\n				for index, actor in ipairs (Combat:GetActorList(1)) do\n				    if (actor:IsPlayer()) then\n					\n					--get the actor total damage absorbed\n					local totalAbsorb = actor.totalabsorbed\n					\n					--get the damage absorbed by all the actor pets\n					for petIndex, petName in ipairs (actor.pets) do\n					    local pet = Combat :GetActor (1, petName)\n					    if (pet) then\n						totalAbsorb = totalAbsorb + pet.totalabsorbed\n					    end\n					end\n					\n					--add the value to the actor on the custom container\n					CustomContainer:AddValue (actor, totalAbsorb)        \n					\n				    end\n				end\n				--loop end\n\n				--if not managed inside the loop, get the values of total, top and amount\n				total, top = CustomContainer:GetTotalAndHighestValue()\n				amount = CustomContainer:GetNumActors()\n\n				--return the values\n				return total, top, amount\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\ICONS\\Spell_Holy_PowerWordShield",
			["script_version"] = 2,
		}, -- [10]
		{
			["source"] = false,
			["author"] = "Details!",
			["total_script"] = "				local value, top, total, combat, instance = ...\n\n				--get the time of overall combat\n				local OverallCombatTime = Details:GetCombat (-1):GetCombatTime()\n				\n				--get the time of current combat if the player is in combat\n				if (Details.in_combat) then\n					local CurrentCombatTime = Details:GetCombat (0):GetCombatTime()\n					OverallCombatTime = OverallCombatTime + CurrentCombatTime\n				end\n				\n				--build the string\n				local ToK = Details:GetCurrentToKFunction()\n				local s = ToK (_, value / OverallCombatTime)\n				\n				if (instance.row_info.textR_show_data[3]) then\n					s = ToK (_, value) .. \" (\" .. s .. \", \"\n				else\n					s = ToK (_, value) .. \" (\" .. s\n				end\n				\n				return s\n			",
			["desc"] = "Show overall damage done on the fly.",
			["attribute"] = false,
			["script"] = "				--init:\n				local combat, instance_container, instance = ...\n				local total, top, amount = 0, 0, 0\n				\n				--get the overall combat\n				local OverallCombat = Details:GetCombat (-1)\n				--get the current combat\n				local CurrentCombat = Details:GetCombat (0)\n				\n				--check if the actor list of overall and current combat is valid\n				if (not OverallCombat.GetActorList or not CurrentCombat.GetActorList) then\n					return 0, 0, 0\n				end\n				\n				--get the damage actor container for overall\n				local damage_container_overall = OverallCombat:GetActorList ( DETAILS_ATTRIBUTE_DAMAGE )\n				--get the damage actor container for current\n				local damage_container_current = CurrentCombat:GetActorList ( DETAILS_ATTRIBUTE_DAMAGE )\n				\n				--check if the results from getactorlist is a table and has more than 1 actor on it\n				if (type (damage_container_overall) ~= \"table\") then\n					return 0, 0, 0\n				end\n				\n				--do the loop:\n				for _, player in ipairs ( damage_container_overall ) do \n					--only player in group\n					if (player and player:IsGroupPlayer()) then\n						instance_container:AddValue (player, player.total)\n					end\n				end\n				\n				--does the player is in combat?\n				--if true need to add the data from the current fight as well\n				if (UnitAffectingCombat(\"player\")) then\n					for _, player in ipairs ( damage_container_current ) do \n						--only player in group\n						if (player and player:IsGroupPlayer()) then\n							instance_container:AddValue (player, player.total)        \n						end\n					end\n				end\n				\n				total, top =  instance_container:GetTotalAndHighestValue()\n				amount =  instance_container:GetNumActors()\n				\n				--return:\n				return total, top, amount\n			",
			["name"] = "Dynamic Overall Damage",
			["tooltip"] = "				--get the parameters passed\n				local actor, combat, instance = ...\n				\n				--get the cooltip object (we dont use the convencional GameTooltip here)\n				local GameCooltip = GameCooltip2\n				\n				--Cooltip code\n				--get the overall combat\n				local OverallCombat = Details:GetCombat (-1)\n				--get the current combat\n				local CurrentCombat = Details:GetCombat (0)\n				\n				local AllSpells = {}\n				\n				--overall\n				local player = OverallCombat [1]:GetActor (actor.nome)\n				if (player) then\n					local playerSpells = player:GetSpellList()\n					for spellID, spellTable in pairs (playerSpells) do\n						local spellName, _, spellIcon = Details.GetSpellInfoC  (spellID)\n						AllSpells [spellName] = spellTable.total\n					end\n				end\n				\n				--current\n				local player = CurrentCombat [1]:GetActor (actor.nome)\n				if (player) then\n					local playerSpells = player:GetSpellList()\n					for spellID, spellTable in pairs (playerSpells) do\n						local spellName, _, spellIcon = Details.GetSpellInfoC  (spellID)\n						AllSpells [spellName] = (AllSpells [spellName] or 0) + (spellTable.total or 0)\n					end\n				end\n				\n				local sortedList = {}\n				for spellID, total in pairs (AllSpells) do\n					tinsert (sortedList, {spellID, total})\n				end\n				table.sort (sortedList, Details.Sort2)\n				\n				local format_func = Details:GetCurrentToKFunction()\n				\n				--build the tooltip\n				for i, t in ipairs (sortedList) do\n					local spellID, total = unpack (t)\n					if (total > 1) then\n						local spellName, _, spellIcon = Details.GetSpellInfoC  (spellID)\n						\n						GameCooltip:AddLine (spellName, format_func (_, total))\n						Details:AddTooltipBackgroundStatusbar()\n						GameCooltip:AddIcon (spellIcon, 1, 1, _detalhes.tooltip.line_height, _detalhes.tooltip.line_height)\n					end\n				end\n			",
			["target"] = false,
			["spellid"] = false,
			["icon"] = "Interface\\Buttons\\Spell-Reset",
			["script_version"] = 6,
		}, -- [11]
	},
	["performance_profiles"] = {
		["Dungeon"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["RaidFinder"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["Battleground15"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["Battleground40"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["Mythic"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["Arena"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["Raid30"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
		["Raid15"] = {
			["enabled"] = false,
			["update_speed"] = 1,
			["miscdata"] = true,
			["aura"] = true,
			["heal"] = true,
			["use_row_animations"] = false,
			["energy"] = false,
			["damage"] = true,
		},
	},
	["exit_log"] = {
		"1 - Closing Janela Info.", -- [1]
		"2 - Clearing user place from instances.", -- [2]
		"4 - Reversing switches.", -- [3]
		"6 - Saving Config.", -- [4]
		"7 - Saving Profiles.", -- [5]
		"8 - Saving nicktag cache.", -- [6]
	},
	["report_where"] = "SAY",
	["lastUpdateWarning"] = 1572800039,
	["always_use_profile"] = false,
}
