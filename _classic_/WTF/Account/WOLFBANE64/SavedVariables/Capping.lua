
CappingSettings = {
	["profileKeys"] = {
		["Thorend - Faerlina"] = "Default",
		["Bankofthor - Faerlina"] = "Default",
		["Thorddin - Faerlina"] = "Default",
		["Thorend - Herod"] = "Default",
		["Thorpez - Herod"] = "Default",
		["Thorwynn - Faerlina"] = "Default",
		["Sneakythor - Faerlina"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["position"] = {
				"TOP", -- [1]
				"TOP", -- [2]
				263.11083984375, -- [3]
				-162.376571655273, -- [4]
			},
			["lock"] = true,
		},
	},
}
