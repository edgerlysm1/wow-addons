
GoldSave = {
	["Thorend_Faerlina::Alliance"] = {
		["show"] = true,
		["name"] = "Thorend",
		["gold"] = 39,
	},
	["Thorddin_Faerlina::Alliance"] = {
		["show"] = true,
		["name"] = "Thorddin",
		["gold"] = 50,
	},
	["Thorend_Herod::Horde"] = {
		["show"] = true,
		["name"] = "Thorend",
		["gold"] = 0,
	},
	["Thorpez_Herod::Horde"] = {
		["show"] = true,
		["name"] = "Thorpez",
		["gold"] = 238,
	},
	["Hadren_Blaumeux::Alliance"] = {
		["show"] = true,
		["name"] = "Hadren",
		["gold"] = 0,
	},
	["Sneakythor_Faerlina::Alliance"] = {
		["show"] = true,
		["name"] = "Sneakythor",
		["gold"] = 1896767,
	},
	["Thorwynn_Faerlina::Alliance"] = {
		["show"] = true,
		["name"] = "Thorwynn",
		["gold"] = 431,
	},
	["Bankofthor_Faerlina::Alliance"] = {
		["show"] = true,
		["name"] = "Bankofthor",
		["gold"] = 260,
	},
}
