
NameplateSCTDB = {
	["profileKeys"] = {
		["Thorend - Faerlina"] = "Default",
		["Bankofthor - Faerlina"] = "Default",
		["Thorddin - Faerlina"] = "Default",
		["Thorend - Herod"] = "Default",
		["Thorpez - Herod"] = "Default",
		["Thorwynn - Faerlina"] = "Default",
		["Sneakythor - Faerlina"] = "Default",
	},
	["global"] = {
		["yOffsetPersonal"] = -50,
		["sizing"] = {
			["smallHitsScale"] = 0.75,
		},
		["yOffset"] = 30,
		["formatting"] = {
			["size"] = 25,
		},
		["offTargetFormatting"] = {
			["size"] = 5,
		},
		["animationsPersonal"] = {
			["normal"] = "verticalDown",
			["crit"] = "verticalDown",
			["miss"] = "fountain",
		},
		["font"] = "2002 Bold",
		["animations"] = {
			["autoattack"] = "verticalUp",
			["miss"] = "fountain",
			["ability"] = "verticalUp",
		},
		["personal"] = true,
	},
}
