
AdiBagsDB = {
	["namespaces"] = {
		["ItemLevel"] = {
		},
		["FilterOverride"] = {
			["profiles"] = {
				["Default"] = {
					["version"] = 3,
				},
			},
		},
		["ItemCategory"] = {
		},
		["NewItem"] = {
		},
		["Equipment"] = {
		},
		["AdiBags_TooltipInfo"] = {
		},
		["DataSource"] = {
		},
		["Junk"] = {
		},
		["MoneyFrame"] = {
		},
	},
	["profileKeys"] = {
		["Thorddin - Faerlina"] = "Default",
		["Thorend - Herod"] = "Default",
		["Thorpez - Herod"] = "Default",
		["Thorwynn - Faerlina"] = "Default",
		["Sneakythor - Faerlina"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["positions"] = {
				["anchor"] = {
					["xOffset"] = -277.729248046875,
					["yOffset"] = 178.666290283203,
				},
				["Backpack"] = {
					["xOffset"] = -510.286110040062,
					["yOffset"] = 171.615212518203,
				},
			},
			["positionMode"] = "manual",
			["compactLayout"] = true,
		},
	},
}
