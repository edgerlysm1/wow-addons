
ZygorGuidesViewerClassicSettings = {
	["char"] = {
		["Thorend - Faerlina"] = {
			["lastlogin"] = 1569796706,
			["initialFlightPathsLoaded"] = true,
			["guidename"] = "LEVELING\\Dun Morogh (1-12) [Dwarf & Gnome Starter]",
			["guides_history_ZGV4clear"] = true,
			["step"] = 21,
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["taxis"] = {
				["Ironforge"] = true,
				["translation"] = {
					["enUS"] = {
					},
				},
			},
			["guideTurnInsOnly"] = false,
			["guides_history"] = {
				{
					"LEVELING\\Dun Morogh (1-12) [Dwarf & Gnome Starter]", -- [1]
					21, -- [2]
				}, -- [1]
			},
			["notifications"] = {
			},
			["guidestephistory"] = {
				["LEVELING\\Dun Morogh (1-12) [Dwarf & Gnome Starter]"] = {
					["steps"] = {
						1, -- [1]
						2, -- [2]
						3, -- [3]
						4, -- [4]
						5, -- [5]
						6, -- [6]
						7, -- [7]
						8, -- [8]
						9, -- [9]
						10, -- [10]
						11, -- [11]
						20, -- [12]
					},
					["lasttime"] = 1569796710,
				},
			},
			["badupgrade"] = {
			},
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Dun Morogh (1-12) [Dwarf & Gnome Starter]",
					["step"] = 21,
				}, -- [1]
			},
			["debuglog"] = {
				"17:02:51.537.965> Viewer started. ---------------------------", -- [1]
				"19:26:35.371.820> Viewer started. ---------------------------", -- [2]
				"17:33:36.757.331> Viewer started. ---------------------------", -- [3]
				"18:38:29.099.681> Viewer started. ---------------------------", -- [4]
			},
		},
		["Bankofthor - Faerlina"] = {
			["lastlogin"] = 1569882025,
			["guidename"] = "LEVELING\\Elwynn Forest (1-13) [Human Starter]",
			["guides_history_ZGV4clear"] = true,
			["debuglog"] = {
				"14:57:04.710.998> Viewer started. ---------------------------", -- [1]
				"17:58:10.776.343> Viewer started. ---------------------------", -- [2]
				"17:37:42.713.080> Viewer started. ---------------------------", -- [3]
				"18:20:29.781.401> Viewer started. ---------------------------", -- [4]
			},
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Elwynn Forest (1-13) [Human Starter]",
					["step"] = 1,
				}, -- [1]
			},
			["guideTurnInsOnly"] = false,
			["guides_history"] = {
				{
					"LEVELING\\Elwynn Forest (1-13) [Human Starter]", -- [1]
					1, -- [2]
				}, -- [1]
			},
			["taxis"] = {
				["Stormwind"] = true,
				["translation"] = {
					["enUS"] = {
					},
				},
			},
			["guidestephistory"] = {
				["LEVELING\\Elwynn Forest (1-13) [Human Starter]"] = {
					["steps"] = {
					},
					["lasttime"] = 1569882030,
				},
			},
			["badupgrade"] = {
				["item:25::::::::1:::::::"] = true,
				["item:2362::::::::1:::::::"] = true,
			},
			["initialFlightPathsLoaded"] = true,
			["notifications"] = {
			},
		},
		["Thorddin - Faerlina"] = {
			["step"] = 15,
			["guidename"] = "LEVELING\\Elwynn Forest (1-13) [Human Starter]",
			["guides_history_ZGV4clear"] = true,
			["guideTurnInsOnly"] = false,
			["guides_history"] = {
				{
					"LEVELING\\Elwynn Forest (1-13) [Human Starter]", -- [1]
					15, -- [2]
				}, -- [1]
			},
			["notifications"] = {
			},
			["taxis"] = {
				["Stormwind"] = true,
				["translation"] = {
					["enUS"] = {
					},
				},
			},
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["lastlogin"] = 1589503652,
			["badupgrade"] = {
			},
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Elwynn Forest (1-13) [Human Starter]",
					["step"] = 15,
				}, -- [1]
			},
			["debuglog"] = {
				"20:47:37.078.138> Viewer started. ---------------------------", -- [1]
			},
			["initialFlightPathsLoaded"] = true,
			["guidestephistory"] = {
				["LEVELING\\Elwynn Forest (1-13) [Human Starter]"] = {
					["steps"] = {
						1, -- [1]
						2, -- [2]
						3, -- [3]
						4, -- [4]
						5, -- [5]
						6, -- [6]
						7, -- [7]
						8, -- [8]
						9, -- [9]
						10, -- [10]
						13, -- [11]
					},
					["lasttime"] = 1589503692,
				},
			},
		},
		["Thorend - Herod"] = {
			["lastlogin"] = 1570937425,
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Durotar (1-12) [Orc & Troll Starter]",
					["step"] = 7,
				}, -- [1]
			},
			["guidename"] = "LEVELING\\Durotar (1-12) [Orc & Troll Starter]",
			["guides_history_ZGV4clear"] = true,
			["step"] = 7,
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["taxis"] = {
				["Orgrimmar"] = true,
				["translation"] = {
					["enUS"] = {
					},
				},
			},
			["guideTurnInsOnly"] = false,
			["guides_history"] = {
				{
					"LEVELING\\Durotar (1-12) [Orc & Troll Starter]", -- [1]
					7, -- [2]
				}, -- [1]
			},
			["notifications"] = {
			},
			["guidestephistory"] = {
				["LEVELING\\Durotar (1-12) [Orc & Troll Starter]"] = {
					["steps"] = {
						1, -- [1]
						3, -- [2]
						4, -- [3]
						5, -- [4]
						6, -- [5]
					},
					["lasttime"] = 1570937434,
				},
			},
			["badupgrade"] = {
			},
			["initialFlightPathsLoaded"] = true,
			["debuglog"] = {
				"23:30:29.041.176> Viewer started. ---------------------------", -- [1]
			},
		},
		["Thorpez - Herod"] = {
			["lastlogin"] = 1571455576,
			["initialFlightPathsLoaded"] = true,
			["guidename"] = "LEVELING\\Mulgore (1-12) [Tauren Starter]",
			["guides_history_ZGV4clear"] = true,
			["debuglog"] = {
				"23:37:01.916.132> Viewer started. ---------------------------", -- [1]
				"23:26:19.516.664> Viewer started. ---------------------------", -- [2]
			},
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["taxis"] = {
				["Thunder Bluff"] = true,
				["translation"] = {
					["enUS"] = {
					},
				},
			},
			["guideTurnInsOnly"] = false,
			["guides_history"] = {
				{
					"LEVELING\\Mulgore (1-12) [Tauren Starter]", -- [1]
					20, -- [2]
				}, -- [1]
			},
			["notifications"] = {
			},
			["guidestephistory"] = {
				["LEVELING\\Mulgore (1-12) [Tauren Starter]"] = {
					["steps"] = {
						1, -- [1]
						2, -- [2]
						3, -- [3]
						4, -- [4]
						5, -- [5]
						6, -- [6]
						7, -- [7]
						8, -- [8]
						9, -- [9]
						11, -- [10]
						14, -- [11]
						15, -- [12]
						16, -- [13]
						17, -- [14]
						18, -- [15]
						19, -- [16]
					},
					["lasttime"] = 1571455579,
				},
			},
			["badupgrade"] = {
			},
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Mulgore (1-12) [Tauren Starter]",
					["step"] = 20,
				}, -- [1]
			},
			["step"] = 20,
		},
		["Thorwynn - Faerlina"] = {
			["debuglog"] = {
				"18:54:14.126.713> Viewer started. ---------------------------", -- [1]
				"21:42:33.790.605> Viewer started. ---------------------------", -- [2]
				"22:45:23.894.944> Viewer started. ---------------------------", -- [3]
			},
			["initialFlightPathsLoaded"] = true,
			["guidename"] = "LEVELING\\Elwynn Forest (1-13) [Human Starter]",
			["guides_history_ZGV4clear"] = true,
			["step"] = 45,
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["badupgrade"] = {
				["item:2211::::::::4:::::::"] = true,
				["item:2210::::::::6:::::::"] = true,
				["item:2211::::::::5:::::::"] = true,
				["item:2211::::::::3:::::::"] = true,
				["item:2211::::::::6:::::::"] = true,
				["item:2210::::::::5:::::::"] = true,
				["item:6078::::::::6:::11::::"] = true,
				["item:2211::::::::1:::::::"] = true,
				["item:2211::::::::2:::::::"] = true,
			},
			["guideTurnInsOnly"] = false,
			["lastlogin"] = 1583639119,
			["guidestephistory"] = {
				["LEVELING\\Elwynn Forest (1-13) [Human Starter]"] = {
					["steps"] = {
						1, -- [1]
						2, -- [2]
						3, -- [3]
						4, -- [4]
						5, -- [5]
						6, -- [6]
						7, -- [7]
						8, -- [8]
						9, -- [9]
						10, -- [10]
						13, -- [11]
						15, -- [12]
						18, -- [13]
						21, -- [14]
						22, -- [15]
						23, -- [16]
						24, -- [17]
						25, -- [18]
						26, -- [19]
						27, -- [20]
						28, -- [21]
						29, -- [22]
						30, -- [23]
						31, -- [24]
						32, -- [25]
						33, -- [26]
						34, -- [27]
						35, -- [28]
						36, -- [29]
						37, -- [30]
						38, -- [31]
						39, -- [32]
						40, -- [33]
						41, -- [34]
						42, -- [35]
						43, -- [36]
						44, -- [37]
					},
					["lasttime"] = 1583639124,
				},
			},
			["notifications"] = {
			},
			["taxis"] = {
				["translation"] = {
					["enUS"] = {
					},
				},
				["Stormwind"] = true,
			},
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Elwynn Forest (1-13) [Human Starter]",
					["step"] = 45,
				}, -- [1]
			},
			["guides_history"] = {
				{
					"LEVELING\\Elwynn Forest (1-13) [Human Starter]", -- [1]
					45, -- [2]
				}, -- [1]
			},
		},
		["Sneakythor - Faerlina"] = {
			["guidestephistory"] = {
				["LEVELING\\Winterspring (54-55)"] = {
					["steps"] = {
						1, -- [1]
						2, -- [2]
						3, -- [3]
						4, -- [4]
						5, -- [5]
						6, -- [6]
						7, -- [7]
						8, -- [8]
						9, -- [9]
						10, -- [10]
						11, -- [11]
						12, -- [12]
						13, -- [13]
					},
					["lasttime"] = 1589413214,
				},
			},
			["tabguides"] = {
				{
					["title"] = "LEVELING\\Winterspring (54-55)",
					["step"] = 14,
				}, -- [1]
			},
			["debuglog"] = {
				"20:47:11.619.758> Viewer started. ---------------------------", -- [1]
				"21:33:50.188.917> Viewer started. ---------------------------", -- [2]
				"08:44:50.324.445> Viewer started. ---------------------------", -- [3]
				"08:55:16.577.745> Viewer started. ---------------------------", -- [4]
				"10:42:14.092.237> Viewer started. ---------------------------", -- [5]
				"11:45:01.880.091> Viewer started. ---------------------------", -- [6]
				"12:33:31.226.377> Viewer started. ---------------------------", -- [7]
				"15:11:49.328.474> Viewer started. ---------------------------", -- [8]
				"16:42:17.752.432> Viewer started. ---------------------------", -- [9]
				"17:14:45.292.445> Viewer started. ---------------------------", -- [10]
				"19:16:58.366.864> Viewer started. ---------------------------", -- [11]
				"20:30:28.777.388> Viewer started. ---------------------------", -- [12]
				"09:36:23.365.529> Viewer started. ---------------------------", -- [13]
				"10:45:20.739.018> Viewer started. ---------------------------", -- [14]
				"12:02:58.712.976> Viewer started. ---------------------------", -- [15]
				"12:09:22.328.597> Viewer started. ---------------------------", -- [16]
				"12:19:07.215.499> Viewer started. ---------------------------", -- [17]
				"12:28:25.271.751> Viewer started. ---------------------------", -- [18]
				"12:51:40.069.409> Viewer started. ---------------------------", -- [19]
				"13:28:30.667.175> Viewer started. ---------------------------", -- [20]
				"14:03:06.343.525> Viewer started. ---------------------------", -- [21]
				"15:10:18.097.402> Viewer started. ---------------------------", -- [22]
				"16:40:33.738.919> Viewer started. ---------------------------", -- [23]
				"18:14:02.786.279> Viewer started. ---------------------------", -- [24]
				"08:40:36.448.548> Viewer started. ---------------------------", -- [25]
				"09:44:58.402.529> Viewer started. ---------------------------", -- [26]
				"10:36:35.994.120> Viewer started. ---------------------------", -- [27]
				"13:53:13.244.340> Viewer started. ---------------------------", -- [28]
				"18:22:54.209.353> Viewer started. ---------------------------", -- [29]
				"19:27:37.955.355> Viewer started. ---------------------------", -- [30]
				"20:03:38.436.863> Viewer started. ---------------------------", -- [31]
				"08:44:11.548.699> Viewer started. ---------------------------", -- [32]
				"10:11:06.297.436> Viewer started. ---------------------------", -- [33]
				"11:58:17.203.336> Viewer started. ---------------------------", -- [34]
				"19:07:56.408.267> Viewer started. ---------------------------", -- [35]
				"23:08:01.978.332> Viewer started. ---------------------------", -- [36]
				"08:49:41.503.673> Viewer started. ---------------------------", -- [37]
				"10:52:19.648.796> Viewer started. ---------------------------", -- [38]
				"11:58:33.435.132> Viewer started. ---------------------------", -- [39]
				"12:01:19.014.284> Viewer started. ---------------------------", -- [40]
				"18:31:09.612.129> Viewer started. ---------------------------", -- [41]
				"11:11:58.065.244> Viewer started. ---------------------------", -- [42]
				"14:06:16.994.208> Viewer started. ---------------------------", -- [43]
				"14:25:42.042.323> Viewer started. ---------------------------", -- [44]
				"15:11:03.243.544> Viewer started. ---------------------------", -- [45]
				"08:55:01.975.169> Viewer started. ---------------------------", -- [46]
				"08:57:17.992.292> Viewer started. ---------------------------", -- [47]
				"10:11:30.233.426> Viewer started. ---------------------------", -- [48]
				"12:02:18.744.956> Viewer started. ---------------------------", -- [49]
				"12:58:23.445.640> Viewer started. ---------------------------", -- [50]
				"14:37:20.716.917> Viewer started. ---------------------------", -- [51]
				"16:55:59.802.025> Viewer started. ---------------------------", -- [52]
				"23:54:46.667.076> Viewer started. ---------------------------", -- [53]
				"07:41:48.074.259> Viewer started. ---------------------------", -- [54]
				"10:10:59.606.849> Viewer started. ---------------------------", -- [55]
				"14:52:04.170.376> Viewer started. ---------------------------", -- [56]
				"15:49:24.011.240> Viewer started. ---------------------------", -- [57]
				"18:38:38.669.968> Viewer started. ---------------------------", -- [58]
				"09:18:59.013.222> Viewer started. ---------------------------", -- [59]
				"10:46:47.483.714> Viewer started. ---------------------------", -- [60]
				"12:43:12.626.906> Viewer started. ---------------------------", -- [61]
				"16:16:08.561.782> Viewer started. ---------------------------", -- [62]
				"20:15:10.234.628> Viewer started. ---------------------------", -- [63]
				"09:44:12.212.454> Viewer started. ---------------------------", -- [64]
				"12:55:47.252.502> Viewer started. ---------------------------", -- [65]
				"13:45:43.206.444> Viewer started. ---------------------------", -- [66]
				"16:32:17.632.865> Viewer started. ---------------------------", -- [67]
				"18:28:44.484.260> Viewer started. ---------------------------", -- [68]
				"12:30:32.758.995> Viewer started. ---------------------------", -- [69]
				"16:59:13.968.278> Viewer started. ---------------------------", -- [70]
				"19:16:03.707.966> Viewer started. ---------------------------", -- [71]
				"19:44:28.422.859> Viewer started. ---------------------------", -- [72]
				"21:16:15.557.119> Viewer started. ---------------------------", -- [73]
				"21:17:29.056.314> Viewer started. ---------------------------", -- [74]
				"21:28:08.067.855> Viewer started. ---------------------------", -- [75]
				"21:29:37.495.751> Viewer started. ---------------------------", -- [76]
				"08:27:58.027.292> Viewer started. ---------------------------", -- [77]
				"12:40:58.885.148> Viewer started. ---------------------------", -- [78]
				"20:35:53.232.528> Viewer started. ---------------------------", -- [79]
				"10:00:21.445.743> Viewer started. ---------------------------", -- [80]
				"12:26:12.675.590> Viewer started. ---------------------------", -- [81]
				"21:40:41.862.453> Viewer started. ---------------------------", -- [82]
				"22:45:50.423.069> Viewer started. ---------------------------", -- [83]
				"10:31:08.680.011> Viewer started. ---------------------------", -- [84]
				"12:32:42.858.180> Viewer started. ---------------------------", -- [85]
				"18:30:43.131.841> Viewer started. ---------------------------", -- [86]
				"12:28:20.703.051> Viewer started. ---------------------------", -- [87]
				"12:28:49.007.226> Viewer started. ---------------------------", -- [88]
				"18:41:36.729.093> Viewer started. ---------------------------", -- [89]
				"15:22:20.776.146> Viewer started. ---------------------------", -- [90]
				"19:03:10.222.574> Viewer started. ---------------------------", -- [91]
				"20:18:01.184.129> Viewer started. ---------------------------", -- [92]
				"20:18:32.222.316> Viewer started. ---------------------------", -- [93]
				"20:19:04.788.779> Viewer started. ---------------------------", -- [94]
				"20:19:36.612.881> Viewer started. ---------------------------", -- [95]
				"20:20:19.073.042> Viewer started. ---------------------------", -- [96]
				"20:20:43.923.920> Viewer started. ---------------------------", -- [97]
				"20:30:37.608.868> Viewer started. ---------------------------", -- [98]
				"10:51:05.296.702> Viewer started. ---------------------------", -- [99]
				"16:48:09.147.569> Viewer started. ---------------------------", -- [100]
				"19:37:11.591.981> Viewer started. ---------------------------", -- [101]
				"11:39:16.489.883> Viewer started. ---------------------------", -- [102]
				"14:37:48.566.988> Viewer started. ---------------------------", -- [103]
				"17:06:10.012.478> Viewer started. ---------------------------", -- [104]
				"18:11:48.565.269> Viewer started. ---------------------------", -- [105]
				"19:28:57.995.395> Viewer started. ---------------------------", -- [106]
				"19:39:32.985.377> Viewer started. ---------------------------", -- [107]
				"00:41:29.372.195> Viewer started. ---------------------------", -- [108]
				"00:43:45.716.452> Viewer started. ---------------------------", -- [109]
				"00:56:17.997.733> Viewer started. ---------------------------", -- [110]
				"00:59:07.968.733> Viewer started. ---------------------------", -- [111]
				"01:03:53.616.381> Viewer started. ---------------------------", -- [112]
				"01:14:09.372.162> Viewer started. ---------------------------", -- [113]
				"01:17:00.637.447> Viewer started. ---------------------------", -- [114]
				"18:26:57.595.011> Viewer started. ---------------------------", -- [115]
				"23:30:17.952.367> Viewer started. ---------------------------", -- [116]
				"12:09:51.191.594> Viewer started. ---------------------------", -- [117]
				"17:50:00.047.497> Viewer started. ---------------------------", -- [118]
				"19:27:48.650.510> Viewer started. ---------------------------", -- [119]
				"13:07:09.794.912> Viewer started. ---------------------------", -- [120]
				"19:18:33.434.533> Viewer started. ---------------------------", -- [121]
				"08:08:44.013.101> Viewer started. ---------------------------", -- [122]
				"16:49:12.220.323> Viewer started. ---------------------------", -- [123]
				"19:18:40.698.795> Viewer started. ---------------------------", -- [124]
				"20:46:04.487.653> Viewer started. ---------------------------", -- [125]
				"16:08:48.332.441> Viewer started. ---------------------------", -- [126]
				"16:59:22.695.857> Viewer started. ---------------------------", -- [127]
				"20:32:04.552.675> Viewer started. ---------------------------", -- [128]
				"20:15:51.245.396> Viewer started. ---------------------------", -- [129]
				"13:15:40.692.871> Viewer started. ---------------------------", -- [130]
				"08:11:25.003.161> Viewer started. ---------------------------", -- [131]
				"12:21:10.833.007> Viewer started. ---------------------------", -- [132]
				"12:23:11.939.106> Viewer started. ---------------------------", -- [133]
				"19:26:50.278.449> Viewer started. ---------------------------", -- [134]
				"17:42:26.550.730> Viewer started. ---------------------------", -- [135]
				"18:13:32.087.372> Viewer started. ---------------------------", -- [136]
				"20:01:57.752.050> Viewer started. ---------------------------", -- [137]
				"22:20:42.266.441> Viewer started. ---------------------------", -- [138]
				"20:19:21.389.623> Viewer started. ---------------------------", -- [139]
				"13:44:09.013.190> Viewer started. ---------------------------", -- [140]
				"11:03:45.569.757> Viewer started. ---------------------------", -- [141]
				"14:30:15.808.128> Viewer started. ---------------------------", -- [142]
				"17:12:37.376.614> Viewer started. ---------------------------", -- [143]
				"17:20:09.813.187> Viewer started. ---------------------------", -- [144]
				"16:17:51.957.061> Viewer started. ---------------------------", -- [145]
				"19:40:13.073.188> Viewer started. ---------------------------", -- [146]
			},
			["guidename"] = "LEVELING\\Winterspring (54-55)",
			["guides_history_ZGV4clear"] = true,
			["step"] = 14,
			["SkillsKnown"] = {
				[""] = {
					["max"] = 0,
					["level"] = 0,
					["placeholder"] = true,
					["active"] = false,
				},
			},
			["lastlogin"] = 1589413210,
			["guideTurnInsOnly"] = false,
			["guides_history"] = {
				{
					"LEVELING\\Winterspring (54-55)", -- [1]
					14, -- [2]
				}, -- [1]
				{
					"LEVELING\\Felwood (54-54)", -- [1]
					16, -- [2]
				}, -- [2]
				{
					"LEVELING\\Winterspring (54-54)", -- [1]
					12, -- [2]
				}, -- [3]
				{
					"LEVELING\\Felwood (53-54)", -- [1]
					43, -- [2]
				}, -- [4]
				{
					"LEVELING\\Azshara (53-53)", -- [1]
					12, -- [2]
				}, -- [5]
				{
					"LEVELING\\Un'Goro Crater (51-53)", -- [1]
					89, -- [2]
				}, -- [6]
				{
					"LEVELING\\Blasted Lands (50-51)", -- [1]
					13, -- [2]
				}, -- [7]
				{
					"LEVELING\\The Hinterlands (50-50)", -- [1]
					4, -- [2]
				}, -- [8]
				{
					"LEVELING\\Searing Gorge (47-48)", -- [1]
					72, -- [2]
				}, -- [9]
				{
					"LEVELING\\Tanaris (49-50)", -- [1]
					1, -- [2]
				}, -- [10]
				{
					"LEVELING\\Feralas (44-46)", -- [1]
					32, -- [2]
				}, -- [11]
				{
					"LEVELING\\Tanaris (43-44)", -- [1]
					45, -- [2]
				}, -- [12]
				{
					"LEVELING\\Desolace (42-43)", -- [1]
					16, -- [2]
				}, -- [13]
				{
					"LEVELING\\Stranglethorn Vale (42-42)", -- [1]
					36, -- [2]
				}, -- [14]
				{
					"LEVELING\\Swamp of Sorrows (41-42)", -- [1]
					39, -- [2]
				}, -- [15]
				{
					"LEVELING\\Badlands (40-41)", -- [1]
					49, -- [2]
				}, -- [16]
				{
					"LEVELING\\Stranglethorn Vale (39-40)", -- [1]
					28, -- [2]
				}, -- [17]
				{
					"LEVELING\\Dustwallow Marsh (38-39)", -- [1]
					32, -- [2]
				}, -- [18]
				{
					"LEVELING\\Arathi Highlands (37-38)", -- [1]
					53, -- [2]
				}, -- [19]
				{
					"LEVELING\\Hillsbrad Foothills (37-37)", -- [1]
					17, -- [2]
				}, -- [20]
				{
					"LEVELING\\Stranglethorn Vale (36-37)", -- [1]
					86, -- [2]
				}, -- [21]
				{
					"LEVELING\\Desolace (34-36)", -- [1]
					50, -- [2]
				}, -- [22]
				{
					"LEVELING\\Thousand Needles (34-34)", -- [1]
					27, -- [2]
				}, -- [23]
				{
					"LEVELING\\Stranglethorn Vale (33-34)", -- [1]
					32, -- [2]
				}, -- [24]
				{
					"LEVELING\\Duskwood (33-33)", -- [1]
					10, -- [2]
				}, -- [25]
				{
					"LEVELING\\Hillsbrad Foothills (31-31)", -- [1]
					44, -- [2]
				}, -- [26]
				{
					"LEVELING\\Wetlands (31-33)", -- [1]
					16, -- [2]
				}, -- [27]
				{
					"LEVELING\\Ashenvale (30-31)", -- [1]
					32, -- [2]
				}, -- [28]
				{
					"LEVELING\\Wetlands (30-30)", -- [1]
					4, -- [2]
				}, -- [29]
				{
					"LEVELING\\Duskwood (27-30)", -- [1]
					94, -- [2]
				}, -- [30]
			},
			["badupgrade"] = {
				["item:9834::::::597:1963746688:33:::::::"] = true,
				["item:2941::::::::32:::1::::"] = true,
				["item:10406::::::605:1827851520:39:::1::::"] = true,
				["item:8747::::::::39:::::::"] = true,
				["item:4107::::::::37:::11::::"] = true,
				["item:13087::::::::35:::1::::"] = true,
				["item:15226::::::135:1712930176:36:::1::::"] = true,
				["item:9359::::::::46:::::::"] = true,
				["item:9359::::::::45:::::::"] = true,
				["item:9359::::::::47:::::::"] = true,
				["item:10406::::::605:1827851520:40:::1::::"] = true,
				["item:15294::::::765:1018598784:52:::::::"] = true,
				["item:15294::::::765:1018598784:53:::::::"] = true,
				["item:4114::::::::44:::11::::"] = true,
				["item:8747::::::::32:::::::"] = true,
				["item:8747::::::::40:::::::"] = true,
				["item:9834::::::597:1963746688:34:::::::"] = true,
			},
			["notifications"] = {
			},
			["ignoredguides"] = {
				["LEVELING\\Wetlands (30-30)"] = true,
				["LEVELING\\Feralas (48-49)"] = true,
				["LEVELING\\Wetlands (31-33)"] = true,
				["LEVELING\\Stranglethorn Vale (39-40)"] = true,
				["LEVELING\\Thousand Needles (34-34)"] = true,
			},
			["initialFlightPathsLoaded"] = true,
			["taxis"] = {
				["554:417"] = false,
				["407:472"] = false,
				["Bloodvenom Post"] = false,
				["Morgan's Vigil"] = true,
				["604:809"] = true,
				["Ratchet"] = true,
				["Revantusk Village"] = false,
				["Hammerfall"] = false,
				["416:792"] = false,
				["671:296"] = false,
				["418:790"] = false,
				["Undercity"] = false,
				["Stonard"] = false,
				["612:776"] = true,
				["Grom'gol"] = false,
				["628:443"] = false,
				["555:611"] = false,
				["407:754"] = true,
				["Feathermoon"] = true,
				["Southshore"] = true,
				["557:530"] = false,
				["448:836"] = false,
				["442:693"] = false,
				["416:157"] = true,
				["615:308"] = false,
				["Thorium Point"] = true,
				["Auberdine"] = true,
				["Aerie Peak"] = true,
				["549:734"] = false,
				["512:749"] = true,
				["449:561"] = false,
				["Menethil Harbor"] = true,
				["Ironforge"] = true,
				["554:571"] = false,
				["478:299"] = true,
				["384:244"] = false,
				["313:692"] = true,
				["462:396"] = true,
				["699:162"] = false,
				["translation"] = {
					["enUS"] = {
					},
				},
				["520:224"] = true,
				["390:402"] = false,
				["Camp Taurajo"] = false,
				["530:257"] = true,
				["316:584"] = false,
				["528:610"] = false,
				["606:801"] = false,
				["494:266"] = false,
				["Talonbranch Glade"] = true,
				["497:763"] = true,
				["Marshal's Refuge"] = true,
				["Camp Mojache"] = false,
				["645:232"] = true,
				["Kargath"] = false,
				["631:361"] = false,
				["Moonglade"] = true,
				["442:194"] = false,
				["552:205"] = true,
				["Lakeshire"] = true,
				["537:205"] = false,
				["Flame Crest"] = false,
				["546:253"] = true,
				["Talrendis Point"] = true,
				["570:323"] = true,
				["Freewind Post"] = false,
				["605:549"] = true,
				["Chillwind Camp"] = true,
				["Nijel's Point"] = true,
				["The Sepulcher"] = false,
				["409:373"] = false,
				["Gadgetzan"] = true,
				["Thelsamar"] = true,
				["580:650"] = true,
				["567:641"] = false,
				["Tarren Mill"] = false,
				["Zoram'gar Outpost"] = false,
				["640:232"] = false,
				["507:488"] = true,
				["589:515"] = true,
				["610:400"] = true,
				["Astranaar"] = true,
				["427:251"] = true,
				["Splintertree Post"] = false,
				["605:746"] = false,
				["Crossroads"] = false,
				["508:567"] = true,
				["Refuge Pointe"] = true,
				["Sun Rock Retreat"] = false,
				["Shadowprey Village"] = false,
				["431:929"] = false,
				["396:506"] = true,
				["557:699"] = true,
				["Brackenwall Village"] = false,
				["Theramore"] = true,
				["Thalanaar"] = true,
				["Stonetalon Peak"] = false,
				["Booty Bay"] = true,
				["636:669"] = true,
				["697:160"] = false,
				["Rut'theran Village"] = true,
				["Light's Hope Chapel"] = false,
				["Everlook"] = true,
				["464:304"] = false,
				["Darkshire"] = true,
				["Nethergarde Keep"] = true,
				["Orgrimmar"] = false,
				["Thunder Bluff"] = false,
				["490:440"] = true,
				["482:696"] = true,
				["Stormwind"] = true,
				["505:567"] = false,
				["Sentinel Hill"] = true,
				["Valormok"] = false,
				["Cenarion Hold"] = false,
				["432:672"] = true,
				["433:930"] = true,
			},
		},
	},
	["profileKeys"] = {
		["Thorend - Faerlina"] = "Thorend",
		["Bankofthor - Faerlina"] = "Bankofthor",
		["Thorddin - Faerlina"] = "Thorddin",
		["Thorend - Herod"] = "Thorend",
		["Thorpez - Herod"] = "Thorpez",
		["Thorwynn - Faerlina"] = "Thorwynn",
		["Sneakythor - Faerlina"] = "Sneakythor",
	},
	["global"] = {
		["gii_cache"] = {
			[0] = {
			},
			["item:1685::::::::54:::::::"] = {
				"Troll-hide Bag", -- [1]
				"|cffffffff|Hitem:1685::::::::54:::::::|h[Troll-hide Bag]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133644, -- [10]
				6250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413458,
			},
			["item:13816::::::::54:::::::"] = {
				"Fine Longsword", -- [1]
				"|cff9d9d9d|Hitem:13816::::::::54:::::::|h[Fine Longsword]|h|r", -- [2]
				0, -- [3]
				52, -- [4]
				47, -- [5]
				"Weapon", -- [6]
				"One-Handed Swords", -- [7]
				1, -- [8]
				"INVTYPE_WEAPONMAINHAND", -- [9]
				135328, -- [10]
				10561, -- [11]
				2, -- [12]
				7, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:21377::::::::54:::::::"] = {
				"Deadwood Headdress Feather", -- [1]
				"|cffffffff|Hitem:21377::::::::54:::::::|h[Deadwood Headdress Feather]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				250, -- [8]
				"", -- [9]
				132926, -- [10]
				200, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:3775::::::::54:::::::|h[Crippling Poison]|h|r"] = {
				"Crippling Poison", -- [1]
				"|cffffffff|Hitem:3775::::::::54:::::::|h[Crippling Poison]|h|r", -- [2]
				1, -- [3]
				20, -- [4]
				20, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132274, -- [10]
				13, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413374,
			},
			["item:11078::::::::54:::::::"] = {
				"Relic Coffer Key", -- [1]
				"|cffffffff|Hitem:11078::::::::54:::::::|h[Relic Coffer Key]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Key", -- [6]
				"Key", -- [7]
				20, -- [8]
				"", -- [9]
				134237, -- [10]
				0, -- [11]
				13, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:12891::::::::54:::11::::"] = {
				"Jaron's Pick", -- [1]
				"|cffffffff|Hitem:12891::::::::54:::11::::|h[Jaron's Pick]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134708, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:6948::::::::1:::::::"] = {
				"Hearthstone", -- [1]
				"|cffffffff|Hitem:6948::::::::1:::::::|h[Hearthstone]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				134414, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:15323::::::593:836092160:54:::::::"] = {
				"Percussion Shotgun of the Monkey", -- [1]
				"|cff1eff00|Hitem:15323::::::593:836092160:54:::::::|h[Percussion Shotgun of the Monkey]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				45, -- [5]
				"Weapon", -- [6]
				"Guns", -- [7]
				1, -- [8]
				"INVTYPE_RANGEDRIGHT", -- [9]
				135610, -- [10]
				17791, -- [11]
				2, -- [12]
				3, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:750::::::::3:::::::"] = {
				"Tough Wolf Meat", -- [1]
				"|cffffffff|Hitem:750::::::::3:::::::|h[Tough Wolf Meat]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				133972, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504235,
			},
			["item:19141::::::::54:::11::::"] = {
				"Luffa", -- [1]
				"|cff1eff00|Hitem:19141::::::::54:::11::::|h[Luffa]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_TRINKET", -- [9]
				132911, -- [10]
				16612, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:8952::::::::54:::::::"] = {
				"Roasted Quail", -- [1]
				"|cffffffff|Hitem:8952::::::::54:::::::|h[Roasted Quail]|h|r", -- [2]
				1, -- [3]
				55, -- [4]
				45, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133971, -- [10]
				200, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:44::::::::2:::::::"] = {
				"Squire's Pants", -- [1]
				"|cff9d9d9d|Hitem:44::::::::2:::::::|h[Squire's Pants]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134582, -- [10]
				1, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:5060::::::::54:::::::"] = {
				"Thieves' Tools", -- [1]
				"|cffffffff|Hitem:5060::::::::54:::::::|h[Thieves' Tools]|h|r", -- [2]
				1, -- [3]
				15, -- [4]
				15, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				1, -- [8]
				"", -- [9]
				134065, -- [10]
				0, -- [11]
				7, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:8952::::::::54:::::::|h[Roasted Quail]|h|r"] = {
				"Roasted Quail", -- [1]
				"|cffffffff|Hitem:8952::::::::54:::::::|h[Roasted Quail]|h|r", -- [2]
				1, -- [3]
				55, -- [4]
				45, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133971, -- [10]
				200, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413374,
			},
			["item:13140::::::::54:::::::"] = {
				"Blood Red Key", -- [1]
				"|cffffffff|Hitem:13140::::::::54:::::::|h[Blood Red Key]|h|r", -- [2]
				1, -- [3]
				49, -- [4]
				49, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134235, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:20741::::::::54:::::::|h[Deadwood Ritual Totem]|h|r"] = {
				"Deadwood Ritual Totem", -- [1]
				"|cffffffff|Hitem:20741::::::::54:::::::|h[Deadwood Ritual Totem]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				45, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				136232, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413334,
			},
			["item:8766::::::::54:::::::"] = {
				"Morning Glory Dew", -- [1]
				"|cffffffff|Hitem:8766::::::::54:::::::|h[Morning Glory Dew]|h|r", -- [2]
				1, -- [3]
				55, -- [4]
				45, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				134712, -- [10]
				200, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			[6948] = {
				"Hearthstone", -- [1]
				"|cffffffff|Hitem:6948::::::::3:::::::|h[Hearthstone]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				134414, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504618,
			},
			["|cffffffff|Hitem:13140::::::::54:::::::|h[Blood Red Key]|h|r"] = {
				"Blood Red Key", -- [1]
				"|cffffffff|Hitem:13140::::::::54:::::::|h[Blood Red Key]|h|r", -- [2]
				1, -- [3]
				49, -- [4]
				49, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134235, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413369,
			},
			["|cffffffff|Hitem:11947::::::::54:::::::|h[Filled Cursed Ooze Jar]|h|r"] = {
				"Filled Cursed Ooze Jar", -- [1]
				"|cffffffff|Hitem:11947::::::::54:::::::|h[Filled Cursed Ooze Jar]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134815, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413371,
			},
			["item:11412::::::::54:::11::::"] = {
				"Nagmara's Vial", -- [1]
				"|cffffffff|Hitem:11412::::::::54:::11::::|h[Nagmara's Vial]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134714, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:10742::::::::54:::11::::"] = {
				"Dragonflight Leggings", -- [1]
				"|cff1eff00|Hitem:10742::::::::54:::11::::|h[Dragonflight Leggings]|h|r", -- [2]
				2, -- [3]
				51, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134585, -- [10]
				9589, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:14256::::::::54:::::::"] = {
				"Felcloth", -- [1]
				"|cffffffff|Hitem:14256::::::::54:::::::|h[Felcloth]|h|r", -- [2]
				1, -- [3]
				50, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				132888, -- [10]
				2000, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:12366::::::::54:::11:1:3524:::|h[Thick Yeti Fur]|h|r"] = {
				"Thick Yeti Fur", -- [1]
				"|cffffffff|Hitem:12366::::::::54:::11:1:3524:::|h[Thick Yeti Fur]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134347, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413249,
			},
			["item:12662::::::::54:::::::"] = {
				"Demonic Rune", -- [1]
				"|cff1eff00|Hitem:12662::::::::54:::::::|h[Demonic Rune]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				134417, -- [10]
				600, -- [11]
				7, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:3898::::::::54:::::::|h[Library Scrip]|h|r"] = {
				"Library Scrip", -- [1]
				"|cffffffff|Hitem:3898::::::::54:::::::|h[Library Scrip]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134939, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413346,
			},
			["|cffffffff|Hitem:8831::::::::54:::::::|h[Purple Lotus]|h|r"] = {
				"Purple Lotus", -- [1]
				"|cffffffff|Hitem:8831::::::::54:::::::|h[Purple Lotus]|h|r", -- [2]
				1, -- [3]
				42, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				134198, -- [10]
				300, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413346,
			},
			["item:7073::::::::3:::::::"] = {
				"Broken Fang", -- [1]
				"|cff9d9d9d|Hitem:7073::::::::3:::::::|h[Broken Fang]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133725, -- [10]
				6, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:17781::::::::54:::::::"] = {
				"The Pariah's Instructions", -- [1]
				"|cffffffff|Hitem:17781::::::::54:::::::|h[The Pariah's Instructions]|h|r", -- [2]
				1, -- [3]
				0, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134327, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:2361::::::::1:::::::"] = {
				"Battleworn Hammer", -- [1]
				"|cffffffff|Hitem:2361::::::::1:::::::|h[Battleworn Hammer]|h|r", -- [2]
				1, -- [3]
				2, -- [4]
				1, -- [5]
				"Weapon", -- [6]
				"Two-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				133052, -- [10]
				9, -- [11]
				2, -- [12]
				5, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:2452::::::::54:::::::"] = {
				"Swiftthistle", -- [1]
				"|cffffffff|Hitem:2452::::::::54:::::::|h[Swiftthistle]|h|r", -- [2]
				1, -- [3]
				15, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				134184, -- [10]
				15, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:5396::::::::54:::11::::|h[Key to Searing Gorge]|h|r"] = {
				"Key to Searing Gorge", -- [1]
				"|cffffffff|Hitem:5396::::::::54:::11::::|h[Key to Searing Gorge]|h|r", -- [2]
				1, -- [3]
				0, -- [4]
				0, -- [5]
				"Key", -- [6]
				"Key", -- [7]
				1, -- [8]
				"", -- [9]
				134248, -- [10]
				0, -- [11]
				13, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413367,
			},
			["item:17191::::::::54:::11::::"] = {
				"Scepter of Celebras", -- [1]
				"|cff0070dd|Hitem:17191::::::::54:::11::::|h[Scepter of Celebras]|h|r", -- [2]
				3, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135153, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:3358::::::::54:::::::"] = {
				"Khadgar's Whisker", -- [1]
				"|cffffffff|Hitem:3358::::::::54:::::::|h[Khadgar's Whisker]|h|r", -- [2]
				1, -- [3]
				37, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				134188, -- [10]
				175, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			[750] = {
				"Tough Wolf Meat", -- [1]
				"|cffffffff|Hitem:750::::::::1:::::::|h[Tough Wolf Meat]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				133972, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503847,
			},
			["item:17705:803:::::::54:::11::::"] = {
				"Thrash Blade", -- [1]
				"|cff0070dd|Hitem:17705:803:::::::54:::11::::|h[Thrash Blade]|h|r", -- [2]
				3, -- [3]
				53, -- [4]
				0, -- [5]
				"Weapon", -- [6]
				"One-Handed Swords", -- [7]
				1, -- [8]
				"INVTYPE_WEAPON", -- [9]
				135346, -- [10]
				32854, -- [11]
				2, -- [12]
				7, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:10286::::::::54:::::::"] = {
				"Heart of the Wild", -- [1]
				"|cffffffff|Hitem:10286::::::::54:::::::|h[Heart of the Wild]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				0, -- [5]
				"Reagent", -- [6]
				"Reagent", -- [7]
				10, -- [8]
				"", -- [9]
				134188, -- [10]
				400, -- [11]
				5, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:13820::::::::54:::::::"] = {
				"Clout Mace", -- [1]
				"|cff9d9d9d|Hitem:13820::::::::54:::::::|h[Clout Mace]|h|r", -- [2]
				0, -- [3]
				54, -- [4]
				49, -- [5]
				"Weapon", -- [6]
				"One-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_WEAPONMAINHAND", -- [9]
				133476, -- [10]
				12042, -- [11]
				2, -- [12]
				4, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:11022::::::::54:::::::"] = {
				"Packet of Tharlendris Seeds", -- [1]
				"|cffffffff|Hitem:11022::::::::54:::::::|h[Packet of Tharlendris Seeds]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				136074, -- [10]
				250, -- [11]
				12, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:9533::::::::54:::11::::"] = {
				"Masons Fraternity Ring", -- [1]
				"|cff0070dd|Hitem:9533::::::::54:::11::::|h[Masons Fraternity Ring]|h|r", -- [2]
				3, -- [3]
				47, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_FINGER", -- [9]
				133345, -- [10]
				7092, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:43::::::::3:::::::"] = {
				"Squire's Boots", -- [1]
				"|cffffffff|Hitem:43::::::::3:::::::|h[Squire's Boots]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132535, -- [10]
				1, -- [11]
				4, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:10455::::::::54:::11::::"] = {
				"Chained Essence of Eranikus", -- [1]
				"|cff1eff00|Hitem:10455::::::::54:::11::::|h[Chained Essence of Eranikus]|h|r", -- [2]
				2, -- [3]
				60, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_TRINKET", -- [9]
				135229, -- [10]
				6464, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:21316::::::::54:::11::::"] = {
				"Leggings of the Ursa", -- [1]
				"|cff1eff00|Hitem:21316::::::::54:::11::::|h[Leggings of the Ursa]|h|r", -- [2]
				2, -- [3]
				55, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Plate", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134662, -- [10]
				12407, -- [11]
				4, -- [12]
				4, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:12366::::::::54:::11:1:3524:::"] = {
				"Thick Yeti Fur", -- [1]
				"|cffffffff|Hitem:12366::::::::54:::11:1:3524:::|h[Thick Yeti Fur]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134347, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413249,
			},
			["item:17728::::::::54:::1::::"] = {
				"Albino Crocscale Boots", -- [1]
				"|cff0070dd|Hitem:17728::::::::54:::1::::|h[Albino Crocscale Boots]|h|r", -- [2]
				3, -- [3]
				53, -- [4]
				48, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132539, -- [10]
				12458, -- [11]
				4, -- [12]
				2, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:3914::::::::54:::::::"] = {
				"Journeyman's Backpack", -- [1]
				"|cffffffff|Hitem:3914::::::::54:::::::|h[Journeyman's Backpack]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133629, -- [10]
				6250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413458,
			},
			["item:5646::::::::54:::::::"] = {
				"Vial of Blessed Water", -- [1]
				"|cffffffff|Hitem:5646::::::::54:::::::|h[Vial of Blessed Water]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134856, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:10830::::::::54:::::::"] = {
				"M73 Frag Grenade", -- [1]
				"|cffffffff|Hitem:10830::::::::54:::::::|h[M73 Frag Grenade]|h|r", -- [2]
				1, -- [3]
				53, -- [4]
				0, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				10, -- [8]
				"", -- [9]
				133716, -- [10]
				750, -- [11]
				0, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff9d9d9d|Hitem:3949::::::::54:::::::|h[Twill Pants]|h|r"] = {
				"Twill Pants", -- [1]
				"|cff9d9d9d|Hitem:3949::::::::54:::::::|h[Twill Pants]|h|r", -- [2]
				0, -- [3]
				59, -- [4]
				54, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134586, -- [10]
				6517, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413315,
			},
			["item:11875::::::::54:::11::::"] = {
				"Breezecloud Bracers", -- [1]
				"|cff1eff00|Hitem:11875::::::::54:::11::::|h[Breezecloud Bracers]|h|r", -- [2]
				2, -- [3]
				54, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_WRIST", -- [9]
				132612, -- [10]
				5776, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:15790::::::::54:::::::|h[Studies in Spirit Speaking]|h|r"] = {
				"Studies in Spirit Speaking", -- [1]
				"|cffffffff|Hitem:15790::::::::54:::::::|h[Studies in Spirit Speaking]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				133737, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413387,
			},
			["|cffffffff|Hitem:3928::::::::54:::::::|h[Superior Healing Potion]|h|r"] = {
				"Superior Healing Potion", -- [1]
				"|cffffffff|Hitem:3928::::::::54:::::::|h[Superior Healing Potion]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				35, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				5, -- [8]
				"", -- [9]
				134833, -- [10]
				250, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413375,
			},
			["item:750::::::::2:::::::"] = {
				"Tough Wolf Meat", -- [1]
				"|cffffffff|Hitem:750::::::::2:::::::|h[Tough Wolf Meat]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				133972, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:6070::::::::3:::11:1:3524:::"] = {
				"Wolfskin Bracers", -- [1]
				"|cffffffff|Hitem:6070::::::::3:::11:1:3524:::|h[Wolfskin Bracers]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_WRIST", -- [9]
				132604, -- [10]
				6, -- [11]
				4, -- [12]
				2, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504254,
			},
			["|cffffffff|Hitem:12891::::::::54:::11::::|h[Jaron's Pick]|h|r"] = {
				"Jaron's Pick", -- [1]
				"|cffffffff|Hitem:12891::::::::54:::11::::|h[Jaron's Pick]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134708, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413372,
			},
			["item:15264::::::1139:1874145920:54:::::::"] = {
				"Backbreaker of the Boar", -- [1]
				"|cff1eff00|Hitem:15264::::::1139:1874145920:54:::::::|h[Backbreaker of the Boar]|h|r", -- [2]
				2, -- [3]
				56, -- [4]
				51, -- [5]
				"Weapon", -- [6]
				"Two-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				133053, -- [10]
				41517, -- [11]
				2, -- [12]
				5, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:8953::::::::54:::::::"] = {
				"Deep Fried Plantains", -- [1]
				"|cffffffff|Hitem:8953::::::::54:::::::|h[Deep Fried Plantains]|h|r", -- [2]
				1, -- [3]
				55, -- [4]
				45, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133979, -- [10]
				200, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:5646::::::::54:::::::|h[Vial of Blessed Water]|h|r"] = {
				"Vial of Blessed Water", -- [1]
				"|cffffffff|Hitem:5646::::::::54:::::::|h[Vial of Blessed Water]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134856, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413354,
			},
			["item:2361::::::::2:::::::"] = {
				"Battleworn Hammer", -- [1]
				"|cffffffff|Hitem:2361::::::::2:::::::|h[Battleworn Hammer]|h|r", -- [2]
				1, -- [3]
				2, -- [4]
				1, -- [5]
				"Weapon", -- [6]
				"Two-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				133052, -- [10]
				9, -- [11]
				2, -- [12]
				5, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["|cffffffff|Hitem:3914::::::::54:::::::|h[Journeyman's Backpack]|h|r"] = {
				"Journeyman's Backpack", -- [1]
				"|cffffffff|Hitem:3914::::::::54:::::::|h[Journeyman's Backpack]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133629, -- [10]
				6250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413458,
			},
			["item:15790::::::::54:::::::"] = {
				"Studies in Spirit Speaking", -- [1]
				"|cffffffff|Hitem:15790::::::::54:::::::|h[Studies in Spirit Speaking]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				133737, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:15788::::::::54:::::::|h[Everlook Report]|h|r"] = {
				"Everlook Report", -- [1]
				"|cffffffff|Hitem:15788::::::::54:::::::|h[Everlook Report]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134942, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413367,
			},
			["item:159::::::::2:::::::"] = {
				"Refreshing Spring Water", -- [1]
				"|cffffffff|Hitem:159::::::::2:::::::|h[Refreshing Spring Water]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132794, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:4338::::::::54:::::::"] = {
				"Mageweave Cloth", -- [1]
				"|cffffffff|Hitem:4338::::::::54:::::::|h[Mageweave Cloth]|h|r", -- [2]
				1, -- [3]
				40, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				132892, -- [10]
				250, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:10515::::::::54:::11::::"] = {
				"Torch of Retribution", -- [1]
				"|cffffffff|Hitem:10515::::::::54:::11::::|h[Torch of Retribution]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				135466, -- [10]
				0, -- [11]
				4, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:11078::::::::54:::::::|h[Relic Coffer Key]|h|r"] = {
				"Relic Coffer Key", -- [1]
				"|cffffffff|Hitem:11078::::::::54:::::::|h[Relic Coffer Key]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Key", -- [6]
				"Key", -- [7]
				20, -- [8]
				"", -- [9]
				134237, -- [10]
				0, -- [11]
				13, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413355,
			},
			["item:7077::::::::54:::::::"] = {
				"Heart of Fire", -- [1]
				"|cffffffff|Hitem:7077::::::::54:::::::|h[Heart of Fire]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				0, -- [5]
				"Reagent", -- [6]
				"Reagent", -- [7]
				10, -- [8]
				"", -- [9]
				135819, -- [10]
				400, -- [11]
				5, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:3928::::::::54:::::::"] = {
				"Superior Healing Potion", -- [1]
				"|cffffffff|Hitem:3928::::::::54:::::::|h[Superior Healing Potion]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				35, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				5, -- [8]
				"", -- [9]
				134833, -- [10]
				250, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:9570::::::::3:::::::"] = {
				"Consecrated Letter", -- [1]
				"|cffffffff|Hitem:9570::::::::3:::::::|h[Consecrated Letter]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				133471, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504273,
			},
			["|cff9d9d9d|Hitem:13816::::::::54:::::::|h[Fine Longsword]|h|r"] = {
				"Fine Longsword", -- [1]
				"|cff9d9d9d|Hitem:13816::::::::54:::::::|h[Fine Longsword]|h|r", -- [2]
				0, -- [3]
				52, -- [4]
				47, -- [5]
				"Weapon", -- [6]
				"One-Handed Swords", -- [7]
				1, -- [8]
				"INVTYPE_WEAPONMAINHAND", -- [9]
				135328, -- [10]
				10561, -- [11]
				2, -- [12]
				7, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413318,
			},
			["item:80::::::::3:::11::::"] = {
				"Soft Fur-lined Shoes", -- [1]
				"|cffffffff|Hitem:80::::::::3:::11::::|h[Soft Fur-lined Shoes]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132543, -- [10]
				7, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["|cffffffff|Hitem:6070::::::::3:::11:1:3524:::|h[Wolfskin Bracers]|h|r"] = {
				"Wolfskin Bracers", -- [1]
				"|cffffffff|Hitem:6070::::::::3:::11:1:3524:::|h[Wolfskin Bracers]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_WRIST", -- [9]
				132604, -- [10]
				6, -- [11]
				4, -- [12]
				2, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504255,
			},
			["|cffffffff|Hitem:4338::::::::54:::::::|h[Mageweave Cloth]|h|r"] = {
				"Mageweave Cloth", -- [1]
				"|cffffffff|Hitem:4338::::::::54:::::::|h[Mageweave Cloth]|h|r", -- [2]
				1, -- [3]
				40, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				132892, -- [10]
				250, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413385,
			},
			["item:80::::::::3:::11:1:3524:::"] = {
				"Soft Fur-lined Shoes", -- [1]
				"|cffffffff|Hitem:80::::::::3:::11:1:3524:::|h[Soft Fur-lined Shoes]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132543, -- [10]
				7, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504254,
			},
			["|cffffffff|Hitem:80::::::::3:::11:1:3524:::|h[Soft Fur-lined Shoes]|h|r"] = {
				"Soft Fur-lined Shoes", -- [1]
				"|cffffffff|Hitem:80::::::::3:::11:1:3524:::|h[Soft Fur-lined Shoes]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132543, -- [10]
				7, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504255,
			},
			["item:4536::::::::3:::::::"] = {
				"Shiny Red Apple", -- [1]
				"|cffffffff|Hitem:4536::::::::3:::::::|h[Shiny Red Apple]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133975, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["|cff1eff00|Hitem:2820::::::::54:::11::::|h[Nifty Stopwatch]|h|r"] = {
				"Nifty Stopwatch", -- [1]
				"|cff1eff00|Hitem:2820::::::::54:::11::::|h[Nifty Stopwatch]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_TRINKET", -- [9]
				134376, -- [10]
				4662, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413460,
			},
			["item:8926::::::::54:::::::"] = {
				"Instant Poison IV", -- [1]
				"|cffffffff|Hitem:8926::::::::54:::::::|h[Instant Poison IV]|h|r", -- [2]
				1, -- [3]
				44, -- [4]
				44, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132273, -- [10]
				75, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:755::::::::3:::::::"] = {
				"Melted Candle", -- [1]
				"|cff9d9d9d|Hitem:755::::::::3:::::::|h[Melted Candle]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133750, -- [10]
				1, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:828::::::::3:::::::"] = {
				"Small Blue Pouch", -- [1]
				"|cffffffff|Hitem:828::::::::3:::::::|h[Small Blue Pouch]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133636, -- [10]
				250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:5571::::::::3:::::::"] = {
				"Small Black Pouch", -- [1]
				"|cffffffff|Hitem:5571::::::::3:::::::|h[Small Black Pouch]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133635, -- [10]
				250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:7074::::::::3:::::::"] = {
				"Chipped Claw", -- [1]
				"|cff9d9d9d|Hitem:7074::::::::3:::::::|h[Chipped Claw]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133723, -- [10]
				4, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:4865::::::::3:::::::"] = {
				"Ruined Pelt", -- [1]
				"|cff9d9d9d|Hitem:4865::::::::3:::::::|h[Ruined Pelt]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				134371, -- [10]
				5, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:4114::::::::54:::11::::"] = {
				"Darktide Cape", -- [1]
				"|cff1eff00|Hitem:4114::::::::54:::11::::|h[Darktide Cape]|h|r", -- [2]
				2, -- [3]
				42, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_CLOAK", -- [9]
				133758, -- [10]
				5826, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:2070::::::::3:::::::"] = {
				"Darnassian Bleu", -- [1]
				"|cffffffff|Hitem:2070::::::::3:::::::|h[Darnassian Bleu]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133948, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:2070::::::::1:::::::"] = {
				"Darnassian Bleu", -- [1]
				"|cffffffff|Hitem:2070::::::::1:::::::|h[Darnassian Bleu]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133948, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:12040::::::167:23802240:54:::::::"] = {
				"Forest Pendant of Agility", -- [1]
				"|cff1eff00|Hitem:12040::::::167:23802240:54:::::::|h[Forest Pendant of Agility]|h|r", -- [2]
				2, -- [3]
				38, -- [4]
				33, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_NECK", -- [9]
				133288, -- [10]
				4164, -- [11]
				4, -- [12]
				0, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:6948::::::::3:::::::"] = {
				"Hearthstone", -- [1]
				"|cffffffff|Hitem:6948::::::::3:::::::|h[Hearthstone]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				134414, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:44::::::::3:::::::"] = {
				"Squire's Pants", -- [1]
				"|cff9d9d9d|Hitem:44::::::::3:::::::|h[Squire's Pants]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134582, -- [10]
				1, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:2653::::::::3:::::::"] = {
				"Flimsy Chain Gloves", -- [1]
				"|cff9d9d9d|Hitem:2653::::::::3:::::::|h[Flimsy Chain Gloves]|h|r", -- [2]
				0, -- [3]
				4, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Mail", -- [7]
				1, -- [8]
				"INVTYPE_HAND", -- [9]
				132938, -- [10]
				3, -- [11]
				4, -- [12]
				3, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:1364::::::::3:::::::"] = {
				"Ragged Leather Vest", -- [1]
				"|cff9d9d9d|Hitem:1364::::::::3:::::::|h[Ragged Leather Vest]|h|r", -- [2]
				0, -- [3]
				5, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_CHEST", -- [9]
				135009, -- [10]
				8, -- [11]
				4, -- [12]
				2, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:755::::::::2:::::::"] = {
				"Melted Candle", -- [1]
				"|cff9d9d9d|Hitem:755::::::::2:::::::|h[Melted Candle]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133750, -- [10]
				1, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:2361::::::::3:::::::"] = {
				"Battleworn Hammer", -- [1]
				"|cffffffff|Hitem:2361::::::::3:::::::|h[Battleworn Hammer]|h|r", -- [2]
				1, -- [3]
				2, -- [4]
				1, -- [5]
				"Weapon", -- [6]
				"Two-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				133052, -- [10]
				9, -- [11]
				2, -- [12]
				5, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["|cffffffff|Hitem:11022::::::::54:::::::|h[Packet of Tharlendris Seeds]|h|r"] = {
				"Packet of Tharlendris Seeds", -- [1]
				"|cffffffff|Hitem:11022::::::::54:::::::|h[Packet of Tharlendris Seeds]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				136074, -- [10]
				250, -- [11]
				12, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413350,
			},
			["item:2070::::::::2:::::::"] = {
				"Darnassian Bleu", -- [1]
				"|cffffffff|Hitem:2070::::::::2:::::::|h[Darnassian Bleu]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133948, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:11736::::::::54:::::::"] = {
				"Libram of Resilience", -- [1]
				"|cff1eff00|Hitem:11736::::::::54:::::::|h[Libram of Resilience]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				50, -- [5]
				"Recipe", -- [6]
				"Book", -- [7]
				1, -- [8]
				"", -- [9]
				133737, -- [10]
				0, -- [11]
				9, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:3775::::::::54:::::::"] = {
				"Crippling Poison", -- [1]
				"|cffffffff|Hitem:3775::::::::54:::::::|h[Crippling Poison]|h|r", -- [2]
				1, -- [3]
				20, -- [4]
				20, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132274, -- [10]
				13, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:12793::::::::54:::1::::"] = {
				"Mixologist's Tunic", -- [1]
				"|cff0070dd|Hitem:12793::::::::54:::1::::|h[Mixologist's Tunic]|h|r", -- [2]
				3, -- [3]
				55, -- [4]
				50, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_CHEST", -- [9]
				132716, -- [10]
				19846, -- [11]
				4, -- [12]
				2, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:21377::::::::54:::::::|h[Deadwood Headdress Feather]|h|r"] = {
				"Deadwood Headdress Feather", -- [1]
				"|cffffffff|Hitem:21377::::::::54:::::::|h[Deadwood Headdress Feather]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				250, -- [8]
				"", -- [9]
				132926, -- [10]
				200, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413367,
			},
			["item:2653::::::::2:::::::"] = {
				"Flimsy Chain Gloves", -- [1]
				"|cff9d9d9d|Hitem:2653::::::::2:::::::|h[Flimsy Chain Gloves]|h|r", -- [2]
				0, -- [3]
				4, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Mail", -- [7]
				1, -- [8]
				"INVTYPE_HAND", -- [9]
				132938, -- [10]
				3, -- [11]
				4, -- [12]
				3, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:1364::::::::2:::::::"] = {
				"Ragged Leather Vest", -- [1]
				"|cff9d9d9d|Hitem:1364::::::::2:::::::|h[Ragged Leather Vest]|h|r", -- [2]
				0, -- [3]
				5, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_CHEST", -- [9]
				135009, -- [10]
				8, -- [11]
				4, -- [12]
				2, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:828::::::::2:::::::"] = {
				"Small Blue Pouch", -- [1]
				"|cffffffff|Hitem:828::::::::2:::::::|h[Small Blue Pouch]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133636, -- [10]
				250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:11108::::::::54:::::::"] = {
				"Faded Photograph", -- [1]
				"|cffffffff|Hitem:11108::::::::54:::::::|h[Faded Photograph]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134944, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:3947::::::::54:::::::"] = {
				"Twill Cloak", -- [1]
				"|cff9d9d9d|Hitem:3947::::::::54:::::::|h[Twill Cloak]|h|r", -- [2]
				0, -- [3]
				52, -- [4]
				47, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_CLOAK", -- [9]
				133766, -- [10]
				3258, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:5571::::::::2:::::::"] = {
				"Small Black Pouch", -- [1]
				"|cffffffff|Hitem:5571::::::::2:::::::|h[Small Black Pouch]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133635, -- [10]
				250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:7074::::::::2:::::::"] = {
				"Chipped Claw", -- [1]
				"|cff9d9d9d|Hitem:7074::::::::2:::::::|h[Chipped Claw]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133723, -- [10]
				4, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:14588::::::::54:::::::"] = {
				"Hawkeye's Cord", -- [1]
				"|cff1eff00|Hitem:14588::::::::54:::::::|h[Hawkeye's Cord]|h|r", -- [2]
				2, -- [3]
				36, -- [4]
				31, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_WAIST", -- [9]
				132498, -- [10]
				1989, -- [11]
				4, -- [12]
				2, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:10797::::::::54:::1::::"] = {
				"Firebreather", -- [1]
				"|cff0070dd|Hitem:10797::::::::54:::1::::|h[Firebreather]|h|r", -- [2]
				3, -- [3]
				53, -- [4]
				48, -- [5]
				"Weapon", -- [6]
				"One-Handed Swords", -- [7]
				1, -- [8]
				"INVTYPE_WEAPON", -- [9]
				135279, -- [10]
				35069, -- [11]
				2, -- [12]
				7, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:4865::::::::2:::::::"] = {
				"Ruined Pelt", -- [1]
				"|cff9d9d9d|Hitem:4865::::::::2:::::::|h[Ruined Pelt]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				134371, -- [10]
				5, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:11947::::::::54:::::::"] = {
				"Filled Cursed Ooze Jar", -- [1]
				"|cffffffff|Hitem:11947::::::::54:::::::|h[Filled Cursed Ooze Jar]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134815, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:6948::::::::2:::::::"] = {
				"Hearthstone", -- [1]
				"|cffffffff|Hitem:6948::::::::2:::::::|h[Hearthstone]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				134414, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["|cff1eff00|Hitem:11736::::::::54:::::::|h[Libram of Resilience]|h|r"] = {
				"Libram of Resilience", -- [1]
				"|cff1eff00|Hitem:11736::::::::54:::::::|h[Libram of Resilience]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				50, -- [5]
				"Recipe", -- [6]
				"Book", -- [7]
				1, -- [8]
				"", -- [9]
				133737, -- [10]
				0, -- [11]
				9, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413375,
			},
			["item:43::::::::2:::::::"] = {
				"Squire's Boots", -- [1]
				"|cffffffff|Hitem:43::::::::2:::::::|h[Squire's Boots]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132535, -- [10]
				1, -- [11]
				4, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:3949::::::::54:::::::"] = {
				"Twill Pants", -- [1]
				"|cff9d9d9d|Hitem:3949::::::::54:::::::|h[Twill Pants]|h|r", -- [2]
				0, -- [3]
				59, -- [4]
				54, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134586, -- [10]
				6517, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:7074::::::::1:::::::"] = {
				"Chipped Claw", -- [1]
				"|cff9d9d9d|Hitem:7074::::::::1:::::::|h[Chipped Claw]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133723, -- [10]
				4, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503955,
			},
			["item:13443::::::::54:::::::"] = {
				"Superior Mana Potion", -- [1]
				"|cffffffff|Hitem:13443::::::::54:::::::|h[Superior Mana Potion]|h|r", -- [2]
				1, -- [3]
				51, -- [4]
				41, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				5, -- [8]
				"", -- [9]
				134854, -- [10]
				400, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:15661::::::777:1292254336:54:::::::"] = {
				"Impenetrable Cloak of the Owl", -- [1]
				"|cff1eff00|Hitem:15661::::::777:1292254336:54:::::::|h[Impenetrable Cloak of the Owl]|h|r", -- [2]
				2, -- [3]
				54, -- [4]
				49, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_CLOAK", -- [9]
				133763, -- [10]
				9036, -- [11]
				4, -- [12]
				1, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:7073::::::::1:::::::"] = {
				"Broken Fang", -- [1]
				"|cff9d9d9d|Hitem:7073::::::::1:::::::|h[Broken Fang]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133725, -- [10]
				6, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:17774::::::::54:::11::::"] = {
				"Mark of the Chosen", -- [1]
				"|cff1eff00|Hitem:17774::::::::54:::11::::|h[Mark of the Chosen]|h|r", -- [2]
				2, -- [3]
				48, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_TRINKET", -- [9]
				133441, -- [10]
				6133, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:15380::::::604:1435486720:54:::1::::"] = {
				"Rageclaw Bracers of the Monkey", -- [1]
				"|cff1eff00|Hitem:15380::::::604:1435486720:54:::1::::|h[Rageclaw Bracers of the Monkey]|h|r", -- [2]
				2, -- [3]
				46, -- [4]
				41, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_WRIST", -- [9]
				132615, -- [10]
				4390, -- [11]
				4, -- [12]
				2, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff9d9d9d|Hitem:4787::::::::54:::::::|h[Burning Pitch]|h|r"] = {
				"Burning Pitch", -- [1]
				"|cff9d9d9d|Hitem:4787::::::::54:::::::|h[Burning Pitch]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				132386, -- [10]
				577, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413334,
			},
			["item:4865::::::::1:::::::"] = {
				"Ruined Pelt", -- [1]
				"|cff9d9d9d|Hitem:4865::::::::1:::::::|h[Ruined Pelt]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				134371, -- [10]
				5, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:750::::::::1:::::::"] = {
				"Tough Wolf Meat", -- [1]
				"|cffffffff|Hitem:750::::::::1:::::::|h[Tough Wolf Meat]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				133972, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["|cffffffff|Hitem:12897::::::::54:::::::|h[Second Relic Fragment]|h|r"] = {
				"Second Relic Fragment", -- [1]
				"|cffffffff|Hitem:12897::::::::54:::::::|h[Second Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413374,
			},
			["item:10774::::::::54:::1::::"] = {
				"Fleshhide Shoulders", -- [1]
				"|cff0070dd|Hitem:10774::::::::54:::1::::|h[Fleshhide Shoulders]|h|r", -- [2]
				3, -- [3]
				42, -- [4]
				37, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_SHOULDER", -- [9]
				135059, -- [10]
				5957, -- [11]
				4, -- [12]
				2, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:44::::::::1:::::::"] = {
				"Squire's Pants", -- [1]
				"|cff9d9d9d|Hitem:44::::::::1:::::::|h[Squire's Pants]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				1, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134582, -- [10]
				1, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:43::::::::1:::::::"] = {
				"Squire's Boots", -- [1]
				"|cffffffff|Hitem:43::::::::1:::::::|h[Squire's Boots]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_FEET", -- [9]
				132535, -- [10]
				1, -- [11]
				4, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["|cff1eff00|Hitem:15661::::::777:1292254336:54:::::::|h[Impenetrable Cloak of the Owl]|h|r"] = {
				"Impenetrable Cloak of the Owl", -- [1]
				"|cff1eff00|Hitem:15661::::::777:1292254336:54:::::::|h[Impenetrable Cloak of the Owl]|h|r", -- [2]
				2, -- [3]
				54, -- [4]
				49, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_CLOAK", -- [9]
				133763, -- [10]
				9036, -- [11]
				4, -- [12]
				1, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413380,
			},
			["item:8831::::::::54:::::::"] = {
				"Purple Lotus", -- [1]
				"|cffffffff|Hitem:8831::::::::54:::::::|h[Purple Lotus]|h|r", -- [2]
				1, -- [3]
				42, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				134198, -- [10]
				300, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:12208::::::::54:::::::"] = {
				"Tender Wolf Meat", -- [1]
				"|cffffffff|Hitem:12208::::::::54:::::::|h[Tender Wolf Meat]|h|r", -- [2]
				1, -- [3]
				40, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				10, -- [8]
				"", -- [9]
				133970, -- [10]
				150, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:8984::::::::54:::::::"] = {
				"Deadly Poison III", -- [1]
				"|cffffffff|Hitem:8984::::::::54:::::::|h[Deadly Poison III]|h|r", -- [2]
				1, -- [3]
				46, -- [4]
				46, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132290, -- [10]
				100, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:13321::::::::54:::14::::"] = {
				"Green Mechanostrider", -- [1]
				"|cff0070dd|Hitem:13321::::::::54:::14::::|h[Green Mechanostrider]|h|r", -- [2]
				3, -- [3]
				40, -- [4]
				40, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				132247, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				3, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:6948::::::::54:::::::|h[Hearthstone]|h|r"] = {
				"Hearthstone", -- [1]
				"|cffffffff|Hitem:6948::::::::54:::::::|h[Hearthstone]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				134414, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413387,
			},
			["item:15788::::::::54:::::::"] = {
				"Everlook Report", -- [1]
				"|cffffffff|Hitem:15788::::::::54:::::::|h[Everlook Report]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134942, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:14530::::::::54:::::::"] = {
				"Heavy Runecloth Bandage", -- [1]
				"|cffffffff|Hitem:14530::::::::54:::::::|h[Heavy Runecloth Bandage]|h|r", -- [2]
				1, -- [3]
				58, -- [4]
				0, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133682, -- [10]
				1000, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:5866::::::::54:::::::"] = {
				"Sample of Indurium Ore", -- [1]
				"|cffffffff|Hitem:5866::::::::54:::::::|h[Sample of Indurium Ore]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134575, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:12899::::::::54:::::::"] = {
				"Fourth Relic Fragment", -- [1]
				"|cffffffff|Hitem:12899::::::::54:::::::|h[Fourth Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:20741::::::::54:::::::"] = {
				"Deadwood Ritual Totem", -- [1]
				"|cffffffff|Hitem:20741::::::::54:::::::|h[Deadwood Ritual Totem]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				45, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				136232, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:5396::::::::54:::11::::"] = {
				"Key to Searing Gorge", -- [1]
				"|cffffffff|Hitem:5396::::::::54:::11::::|h[Key to Searing Gorge]|h|r", -- [2]
				1, -- [3]
				0, -- [4]
				0, -- [5]
				"Key", -- [6]
				"Key", -- [7]
				1, -- [8]
				"", -- [9]
				134248, -- [10]
				0, -- [11]
				13, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:15372::::::608:700264448:54:::1::::"] = {
				"Wolf Rider's Gloves of the Monkey", -- [1]
				"|cff1eff00|Hitem:15372::::::608:700264448:54:::1::::|h[Wolf Rider's Gloves of the Monkey]|h|r", -- [2]
				2, -- [3]
				43, -- [4]
				38, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_HAND", -- [9]
				132959, -- [10]
				3383, -- [11]
				4, -- [12]
				2, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:14047::::::::54:::::::"] = {
				"Runecloth", -- [1]
				"|cffffffff|Hitem:14047::::::::54:::::::|h[Runecloth]|h|r", -- [2]
				1, -- [3]
				50, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				132903, -- [10]
				400, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:7146::::::::54:::1::::"] = {
				"The Scarlet Key", -- [1]
				"|cff1eff00|Hitem:7146::::::::54:::1::::|h[The Scarlet Key]|h|r", -- [2]
				2, -- [3]
				0, -- [4]
				0, -- [5]
				"Key", -- [6]
				"Key", -- [7]
				1, -- [8]
				"", -- [9]
				134235, -- [10]
				0, -- [11]
				13, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:14256::::::::54:::::::|h[Felcloth]|h|r"] = {
				"Felcloth", -- [1]
				"|cffffffff|Hitem:14256::::::::54:::::::|h[Felcloth]|h|r", -- [2]
				1, -- [3]
				50, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				132888, -- [10]
				2000, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413386,
			},
			["item:12013::::::688:1250452736:54:::1::::"] = {
				"Desert Ring of the Tiger", -- [1]
				"|cff1eff00|Hitem:12013::::::688:1250452736:54:::1::::|h[Desert Ring of the Tiger]|h|r", -- [2]
				2, -- [3]
				49, -- [4]
				44, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_FINGER", -- [9]
				133356, -- [10]
				4649, -- [11]
				4, -- [12]
				0, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff1eff00|Hitem:11875::::::::54:::11::::|h[Breezecloud Bracers]|h|r"] = {
				"Breezecloud Bracers", -- [1]
				"|cff1eff00|Hitem:11875::::::::54:::11::::|h[Breezecloud Bracers]|h|r", -- [2]
				2, -- [3]
				54, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_WRIST", -- [9]
				132612, -- [10]
				5776, -- [11]
				4, -- [12]
				1, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413380,
			},
			["item:2820::::::::54:::11::::"] = {
				"Nifty Stopwatch", -- [1]
				"|cff1eff00|Hitem:2820::::::::54:::11::::|h[Nifty Stopwatch]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_TRINKET", -- [9]
				134376, -- [10]
				4662, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:159::::::::3:::::::"] = {
				"Refreshing Spring Water", -- [1]
				"|cffffffff|Hitem:159::::::::3:::::::|h[Refreshing Spring Water]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132794, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504496,
			},
			["item:11949::::::::54:::::::"] = {
				"Filled Tainted Ooze Jar", -- [1]
				"|cffffffff|Hitem:11949::::::::54:::::::|h[Filled Tainted Ooze Jar]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134857, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:3898::::::::54:::::::"] = {
				"Library Scrip", -- [1]
				"|cffffffff|Hitem:3898::::::::54:::::::|h[Library Scrip]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134939, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff9d9d9d|Hitem:13820::::::::54:::::::|h[Clout Mace]|h|r"] = {
				"Clout Mace", -- [1]
				"|cff9d9d9d|Hitem:13820::::::::54:::::::|h[Clout Mace]|h|r", -- [2]
				0, -- [3]
				54, -- [4]
				49, -- [5]
				"Weapon", -- [6]
				"One-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_WEAPONMAINHAND", -- [9]
				133476, -- [10]
				12042, -- [11]
				2, -- [12]
				4, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413317,
			},
			["|cff1eff00|Hitem:15264::::::1139:1874145920:54:::::::|h[Backbreaker of the Boar]|h|r"] = {
				"Backbreaker of the Boar", -- [1]
				"|cff1eff00|Hitem:15264::::::1139:1874145920:54:::::::|h[Backbreaker of the Boar]|h|r", -- [2]
				2, -- [3]
				56, -- [4]
				51, -- [5]
				"Weapon", -- [6]
				"Two-Handed Maces", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				133053, -- [10]
				41517, -- [11]
				2, -- [12]
				5, -- [13]
				2, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413366,
			},
			["item:11925::::::::54:::1::::"] = {
				"Ghostshroud", -- [1]
				"|cff0070dd|Hitem:11925::::::::54:::1::::|h[Ghostshroud]|h|r", -- [2]
				3, -- [3]
				57, -- [4]
				52, -- [5]
				"Armor", -- [6]
				"Leather", -- [7]
				1, -- [8]
				"INVTYPE_HEAD", -- [9]
				133143, -- [10]
				16484, -- [11]
				4, -- [12]
				2, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:11949::::::::54:::::::|h[Filled Tainted Ooze Jar]|h|r"] = {
				"Filled Tainted Ooze Jar", -- [1]
				"|cffffffff|Hitem:11949::::::::54:::::::|h[Filled Tainted Ooze Jar]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134857, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413372,
			},
			["|cffffffff|Hitem:5866::::::::54:::::::|h[Sample of Indurium Ore]|h|r"] = {
				"Sample of Indurium Ore", -- [1]
				"|cffffffff|Hitem:5866::::::::54:::::::|h[Sample of Indurium Ore]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134575, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413374,
			},
			["|cffffffff|Hitem:2452::::::::54:::::::|h[Swiftthistle]|h|r"] = {
				"Swiftthistle", -- [1]
				"|cffffffff|Hitem:2452::::::::54:::::::|h[Swiftthistle]|h|r", -- [2]
				1, -- [3]
				15, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				20, -- [8]
				"", -- [9]
				134184, -- [10]
				15, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413384,
			},
			["item:5140::::::::54:::::::"] = {
				"Flash Powder", -- [1]
				"|cffffffff|Hitem:5140::::::::54:::::::|h[Flash Powder]|h|r", -- [2]
				1, -- [3]
				20, -- [4]
				0, -- [5]
				"Reagent", -- [6]
				"Reagent", -- [7]
				20, -- [8]
				"", -- [9]
				134387, -- [10]
				6, -- [11]
				5, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff9d9d9d|Hitem:3947::::::::54:::::::|h[Twill Cloak]|h|r"] = {
				"Twill Cloak", -- [1]
				"|cff9d9d9d|Hitem:3947::::::::54:::::::|h[Twill Cloak]|h|r", -- [2]
				0, -- [3]
				52, -- [4]
				47, -- [5]
				"Armor", -- [6]
				"Cloth", -- [7]
				1, -- [8]
				"INVTYPE_CLOAK", -- [9]
				133766, -- [10]
				3258, -- [11]
				4, -- [12]
				1, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413319,
			},
			["item:12203::::::::54:::::::"] = {
				"Red Wolf Meat", -- [1]
				"|cffffffff|Hitem:12203::::::::54:::::::|h[Red Wolf Meat]|h|r", -- [2]
				1, -- [3]
				30, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				10, -- [8]
				"", -- [9]
				134027, -- [10]
				87, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:1645::::::::54:::::::"] = {
				"Moonberry Juice", -- [1]
				"|cffffffff|Hitem:1645::::::::54:::::::|h[Moonberry Juice]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				35, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132789, -- [10]
				100, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff1eff00|Hitem:7146::::::::54:::1::::|h[The Scarlet Key]|h|r"] = {
				"The Scarlet Key", -- [1]
				"|cff1eff00|Hitem:7146::::::::54:::1::::|h[The Scarlet Key]|h|r", -- [2]
				2, -- [3]
				0, -- [4]
				0, -- [5]
				"Key", -- [6]
				"Key", -- [7]
				1, -- [8]
				"", -- [9]
				134235, -- [10]
				0, -- [11]
				13, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413387,
			},
			["|cffffffff|Hitem:12899::::::::54:::::::|h[Fourth Relic Fragment]|h|r"] = {
				"Fourth Relic Fragment", -- [1]
				"|cffffffff|Hitem:12899::::::::54:::::::|h[Fourth Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413350,
			},
			["item:7073::::::::2:::::::"] = {
				"Broken Fang", -- [1]
				"|cff9d9d9d|Hitem:7073::::::::2:::::::|h[Broken Fang]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				133725, -- [10]
				6, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:12897::::::::54:::::::"] = {
				"Second Relic Fragment", -- [1]
				"|cffffffff|Hitem:12897::::::::54:::::::|h[Second Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:4536::::::::2:::::::"] = {
				"Shiny Red Apple", -- [1]
				"|cffffffff|Hitem:4536::::::::2:::::::|h[Shiny Red Apple]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				133975, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589504140,
			},
			["item:3825::::::::54:::::::"] = {
				"Elixir of Fortitude", -- [1]
				"|cffffffff|Hitem:3825::::::::54:::::::|h[Elixir of Fortitude]|h|r", -- [2]
				1, -- [3]
				35, -- [4]
				25, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				5, -- [8]
				"", -- [9]
				134823, -- [10]
				110, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:159::::::::1:::::::"] = {
				"Refreshing Spring Water", -- [1]
				"|cffffffff|Hitem:159::::::::1:::::::|h[Refreshing Spring Water]|h|r", -- [2]
				1, -- [3]
				5, -- [4]
				1, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132794, -- [10]
				1, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589503918,
			},
			["item:12366::::::::54:::::::"] = {
				"Thick Yeti Fur", -- [1]
				"|cffffffff|Hitem:12366::::::::54:::::::|h[Thick Yeti Fur]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				20, -- [8]
				"", -- [9]
				134347, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413214,
			},
			["item:6948::::::::54:::::::"] = {
				"Hearthstone", -- [1]
				"|cffffffff|Hitem:6948::::::::54:::::::|h[Hearthstone]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				134414, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:13446::::::::54:::::::"] = {
				"Major Healing Potion", -- [1]
				"|cffffffff|Hitem:13446::::::::54:::::::|h[Major Healing Potion]|h|r", -- [2]
				1, -- [3]
				55, -- [4]
				45, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				5, -- [8]
				"", -- [9]
				134834, -- [10]
				1000, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cff0070dd|Hitem:13321::::::::54:::14::::|h[Green Mechanostrider]|h|r"] = {
				"Green Mechanostrider", -- [1]
				"|cff0070dd|Hitem:13321::::::::54:::14::::|h[Green Mechanostrider]|h|r", -- [2]
				3, -- [3]
				40, -- [4]
				40, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				1, -- [8]
				"", -- [9]
				132247, -- [10]
				0, -- [11]
				15, -- [12]
				0, -- [13]
				3, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413387,
			},
			["|cffffffff|Hitem:12898::::::::54:::::::|h[Third Relic Fragment]|h|r"] = {
				"Third Relic Fragment", -- [1]
				"|cffffffff|Hitem:12898::::::::54:::::::|h[Third Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413350,
			},
			["|cffffffff|Hitem:17781::::::::54:::::::|h[The Pariah's Instructions]|h|r"] = {
				"The Pariah's Instructions", -- [1]
				"|cffffffff|Hitem:17781::::::::54:::::::|h[The Pariah's Instructions]|h|r", -- [2]
				1, -- [3]
				0, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				134327, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413386,
			},
			["|cff1eff00|Hitem:21316::::::::54:::11::::|h[Leggings of the Ursa]|h|r"] = {
				"Leggings of the Ursa", -- [1]
				"|cff1eff00|Hitem:21316::::::::54:::11::::|h[Leggings of the Ursa]|h|r", -- [2]
				2, -- [3]
				55, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Plate", -- [7]
				1, -- [8]
				"INVTYPE_LEGS", -- [9]
				134662, -- [10]
				12407, -- [11]
				4, -- [12]
				4, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413380,
			},
			["item:12898::::::::54:::::::"] = {
				"Third Relic Fragment", -- [1]
				"|cffffffff|Hitem:12898::::::::54:::::::|h[Third Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:1685::::::::54:::::::|h[Troll-hide Bag]|h|r"] = {
				"Troll-hide Bag", -- [1]
				"|cffffffff|Hitem:1685::::::::54:::::::|h[Troll-hide Bag]|h|r", -- [2]
				1, -- [3]
				45, -- [4]
				0, -- [5]
				"Container", -- [6]
				"Bag", -- [7]
				1, -- [8]
				"INVTYPE_BAG", -- [9]
				133644, -- [10]
				6250, -- [11]
				1, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413458,
			},
			["|cffffffff|Hitem:5060::::::::54:::::::|h[Thieves' Tools]|h|r"] = {
				"Thieves' Tools", -- [1]
				"|cffffffff|Hitem:5060::::::::54:::::::|h[Thieves' Tools]|h|r", -- [2]
				1, -- [3]
				15, -- [4]
				15, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				1, -- [8]
				"", -- [9]
				134065, -- [10]
				0, -- [11]
				7, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413384,
			},
			["item:4787::::::::54:::::::"] = {
				"Burning Pitch", -- [1]
				"|cff9d9d9d|Hitem:4787::::::::54:::::::|h[Burning Pitch]|h|r", -- [2]
				0, -- [3]
				1, -- [4]
				0, -- [5]
				"Miscellaneous", -- [6]
				"Junk", -- [7]
				5, -- [8]
				"", -- [9]
				132386, -- [10]
				577, -- [11]
				15, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:12203::::::::54:::::::|h[Red Wolf Meat]|h|r"] = {
				"Red Wolf Meat", -- [1]
				"|cffffffff|Hitem:12203::::::::54:::::::|h[Red Wolf Meat]|h|r", -- [2]
				1, -- [3]
				30, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				10, -- [8]
				"", -- [9]
				134027, -- [10]
				87, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413384,
			},
			["|cffffffff|Hitem:12208::::::::54:::::::|h[Tender Wolf Meat]|h|r"] = {
				"Tender Wolf Meat", -- [1]
				"|cffffffff|Hitem:12208::::::::54:::::::|h[Tender Wolf Meat]|h|r", -- [2]
				1, -- [3]
				40, -- [4]
				0, -- [5]
				"Trade Goods", -- [6]
				"Trade Goods", -- [7]
				10, -- [8]
				"", -- [9]
				133970, -- [10]
				150, -- [11]
				7, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413384,
			},
			["|cffffffff|Hitem:10515::::::::54:::11::::|h[Torch of Retribution]|h|r"] = {
				"Torch of Retribution", -- [1]
				"|cffffffff|Hitem:10515::::::::54:::11::::|h[Torch of Retribution]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_2HWEAPON", -- [9]
				135466, -- [10]
				0, -- [11]
				4, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413367,
			},
			["item:11122::::::::54:::11::::"] = {
				"Carrot on a Stick", -- [1]
				"|cff1eff00|Hitem:11122::::::::54:::11::::|h[Carrot on a Stick]|h|r", -- [2]
				2, -- [3]
				50, -- [4]
				0, -- [5]
				"Armor", -- [6]
				"Miscellaneous", -- [7]
				1, -- [8]
				"INVTYPE_TRINKET", -- [9]
				134010, -- [10]
				7162, -- [11]
				4, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["item:12896::::::::54:::::::"] = {
				"First Relic Fragment", -- [1]
				"|cffffffff|Hitem:12896::::::::54:::::::|h[First Relic Fragment]|h|r", -- [2]
				1, -- [3]
				1, -- [4]
				0, -- [5]
				"Quest", -- [6]
				"Quest", -- [7]
				1, -- [8]
				"", -- [9]
				135152, -- [10]
				0, -- [11]
				12, -- [12]
				0, -- [13]
				4, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:3825::::::::54:::::::|h[Elixir of Fortitude]|h|r"] = {
				"Elixir of Fortitude", -- [1]
				"|cffffffff|Hitem:3825::::::::54:::::::|h[Elixir of Fortitude]|h|r", -- [2]
				1, -- [3]
				35, -- [4]
				25, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				5, -- [8]
				"", -- [9]
				134823, -- [10]
				110, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413375,
			},
			["item:5530::::::::54:::::::"] = {
				"Blinding Powder", -- [1]
				"|cffffffff|Hitem:5530::::::::54:::::::|h[Blinding Powder]|h|r", -- [2]
				1, -- [3]
				34, -- [4]
				0, -- [5]
				"Reagent", -- [6]
				"Reagent", -- [7]
				20, -- [8]
				"", -- [9]
				133587, -- [10]
				125, -- [11]
				5, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413252,
			},
			["|cffffffff|Hitem:10830::::::::54:::::::|h[M73 Frag Grenade]|h|r"] = {
				"M73 Frag Grenade", -- [1]
				"|cffffffff|Hitem:10830::::::::54:::::::|h[M73 Frag Grenade]|h|r", -- [2]
				1, -- [3]
				53, -- [4]
				0, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				10, -- [8]
				"", -- [9]
				133716, -- [10]
				750, -- [11]
				0, -- [12]
				0, -- [13]
				1, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413459,
			},
			["|cffffffff|Hitem:8926::::::::54:::::::|h[Instant Poison IV]|h|r"] = {
				"Instant Poison IV", -- [1]
				"|cffffffff|Hitem:8926::::::::54:::::::|h[Instant Poison IV]|h|r", -- [2]
				1, -- [3]
				44, -- [4]
				44, -- [5]
				"Consumable", -- [6]
				"Consumable", -- [7]
				20, -- [8]
				"", -- [9]
				132273, -- [10]
				75, -- [11]
				0, -- [12]
				0, -- [13]
				0, -- [14]
				254, -- [15]
				[17] = false,
				["timestamp"] = 1589413375,
			},
		},
		["sv_version"] = 2,
	},
	["profiles"] = {
		["Thorpez"] = {
			["gold_tooltips_guide"] = 1,
			["gear_active_build"] = 1,
			["load_betaguides"] = true,
			["debug_flags"] = {
				["display"] = {
					["enabled"] = false,
					["color"] = false,
				},
				["lr_initpath_v"] = {
					["enabled"] = false,
					["color"] = false,
				},
			},
			["fullness_search"] = 1,
			["expired_elite_shown"] = false,
			["gold_profitlevel"] = 0.25,
			["autogear_protectheirlooms"] = true,
			["geareffects"] = true,
			["usernamed"] = true,
			["tmp__was_stealth"] = true,
			["gear_selected_build"] = 1,
			["im_always_wait"] = false,
			["load_im"] = true,
			["gold_tooltips_ah"] = 2,
			["show_ui"] = true,
			["Inventory Manager"] = {
				["lastParentName"] = "Blizzard",
				["showText"] = false,
				["isSnapped"] = true,
				["lastParent"] = {
					["PortraitButton"] = {
						["Highlight"] = {
						},
					},
					["Portrait"] = {
					},
					["ClickableTitleFrame"] = {
					},
					["bags"] = {
					},
					["forceExtended"] = false,
					["bagsShown"] = 0,
				},
				["isSnappedSize"] = 169,
				["im_lastWidth"] = 169,
			},
			["load_gold"] = true,
			["load_mail"] = true,
			["stickycolored"] = false,
			["frame_positions"] = {
				["ZygorGuidesViewerPointer_ArrowCtrl"] = {
					"TOP", -- [1]
					nil, -- [2]
					"TOP", -- [3]
					-1.7778377532959, -- [4]
					-81.8111114501953, -- [5]
				},
			},
			["autogear_protectheirlooms_all"] = true,
			["n_nc_numpetguides"] = 5,
			["gold_format_white"] = false,
			["tmp__was_sheened"] = true,
			["im_town_alert"] = false,
			["hide_dev_once"] = false,
			["frame_anchor"] = {
				"RIGHT", -- [1]
				{
					["variablesLoaded"] = true,
					["firstTimeLoaded"] = 1,
				}, -- [2]
				"RIGHT", -- [3]
				-341.086181640625, -- [4]
				169.086273193359, -- [5]
			},
			["dispprimary"] = {
				["showborder"] = true,
			},
			["gold_tooltips_shift"] = true,
		},
		["Bankofthor"] = {
			["gear_active_build"] = 1,
			["load_betaguides"] = true,
			["gold_tooltips_shift"] = true,
			["fullness_search"] = 1,
			["debug_flags"] = {
				["display"] = {
					["enabled"] = false,
					["color"] = false,
				},
				["lr_initpath_v"] = {
					["enabled"] = false,
					["color"] = false,
				},
			},
			["expired_elite_shown"] = false,
			["gold_profitlevel"] = 0.25,
			["geareffects"] = true,
			["usernamed"] = true,
			["tmp__was_stealth"] = true,
			["gear_selected_build"] = 1,
			["autogear_protectheirlooms"] = true,
			["im_always_wait"] = false,
			["tmp__was_sheened"] = true,
			["Inventory Manager"] = {
				["lastParentName"] = "Blizzard",
				["showText"] = false,
				["isSnapped"] = true,
				["lastParent"] = {
					["PortraitButton"] = {
						["Highlight"] = {
						},
					},
					["Portrait"] = {
					},
					["ClickableTitleFrame"] = {
					},
					["bags"] = {
					},
					["forceExtended"] = false,
					["bagsShown"] = 0,
				},
				["isSnappedSize"] = 169,
				["im_lastWidth"] = 169,
			},
			["load_im"] = true,
			["show_ui"] = true,
			["load_mail"] = true,
			["stickycolored"] = false,
			["load_gold"] = true,
			["autogear_protectheirlooms_all"] = true,
			["n_nc_numpetguides"] = 5,
			["gold_format_white"] = false,
			["gold_tooltips_ah"] = 2,
			["im_town_alert"] = false,
			["hide_dev_once"] = false,
			["frame_anchor"] = {
				"RIGHT", -- [1]
				{
					["variablesLoaded"] = true,
					["firstTimeLoaded"] = 1,
				}, -- [2]
				"RIGHT", -- [3]
				-252.000045776367, -- [4]
				176.197525024414, -- [5]
			},
			["dispprimary"] = {
				["showborder"] = true,
			},
			["gold_tooltips_guide"] = 1,
		},
		["Thorddin"] = {
			["gear_active_build"] = 1,
			["gear_selected_build"] = 1,
			["usernamed"] = true,
			["frame_anchor"] = {
				"RIGHT", -- [1]
				nil, -- [2]
				"RIGHT", -- [3]
				-417.332672119141, -- [4]
				10.0741147994995, -- [5]
			},
		},
		["Thorwynn"] = {
			["gold_tooltips_shift"] = true,
			["gmlastsection"] = "Home",
			["autogear_protectheirlooms"] = true,
			["gear_active_build"] = 1,
			["load_betaguides"] = true,
			["frame_anchor"] = {
				"RIGHT", -- [1]
				{
					["variablesLoaded"] = true,
					["firstTimeLoaded"] = 1,
				}, -- [2]
				"RIGHT", -- [3]
				-284.592987060547, -- [4]
				188.049301147461, -- [5]
			},
			["fullness_search"] = 1,
			["tmp__was_stealth"] = true,
			["gear_selected_build"] = 3,
			["im_always_wait"] = false,
			["geareffects"] = true,
			["usernamed"] = true,
			["debug_flags"] = {
				["display"] = {
					["enabled"] = false,
					["color"] = false,
				},
				["lr_initpath_v"] = {
					["enabled"] = false,
					["color"] = false,
				},
			},
			["gold_profitlevel"] = 0.25,
			["Inventory Manager"] = {
				["lastParentName"] = "Blizzard",
				["showText"] = false,
				["isSnapped"] = true,
				["lastParent"] = {
					["PortraitButton"] = {
						["Highlight"] = {
						},
					},
					["Portrait"] = {
					},
					["ClickableTitleFrame"] = {
					},
					["bags"] = {
					},
					["forceExtended"] = false,
					["bagsShown"] = 0,
					["allBags"] = false,
					["isHelpBoxShown"] = true,
					["size"] = 16,
				},
				["isSnappedSize"] = 169,
				["im_lastWidth"] = 169,
			},
			["gear_selected_class"] = 2,
			["gold_tooltips_ah"] = 2,
			["autogear_protectheirlooms_all"] = true,
			["load_im"] = true,
			["show_ui"] = true,
			["load_mail"] = true,
			["stickycolored"] = false,
			["frame_positions"] = {
				["ZygorGuidesViewerPointer_ArrowCtrl"] = {
					"TOP", -- [1]
					nil, -- [2]
					"TOP", -- [3]
					-1.06672132015228, -- [4]
					-85.36669921875, -- [5]
				},
			},
			["load_gold"] = true,
			["n_nc_numpetguides"] = 5,
			["gold_format_white"] = false,
			["tmp__was_sheened"] = true,
			["im_town_alert"] = false,
			["hide_dev_once"] = false,
			["expired_elite_shown"] = false,
			["dispprimary"] = {
				["showborder"] = true,
			},
			["gold_tooltips_guide"] = 1,
		},
		["Sneakythor"] = {
			["arrowscale"] = 0.899999976158142,
			["gear_active_build"] = 1,
			["fullness_search"] = 1,
			["geareffects"] = true,
			["actionbar_anchor"] = {
				"CENTER", -- [1]
				{
					["variablesLoaded"] = true,
					["firstTimeLoaded"] = 1,
				}, -- [2]
				"CENTER", -- [3]
				162.984771728516, -- [4]
				-43.3291091918945, -- [5]
			},
			["gold_profitlevel"] = 0.25,
			["autogear_protectheirlooms"] = true,
			["load_im"] = true,
			["resizeup"] = false,
			["load_gold"] = true,
			["gold_format_white"] = false,
			["fullheight"] = 226.035690307617,
			["expired_elite_shown"] = false,
			["dispprimary"] = {
				["showborder"] = true,
			},
			["gold_tooltips_guide"] = 1,
			["gmlastsection"] = "LEVELING",
			["load_betaguides"] = true,
			["im_always_wait"] = false,
			["autogear_protectheirlooms_all"] = true,
			["windowlocked"] = false,
			["usernamed"] = true,
			["tmp__was_stealth"] = true,
			["gmlasthomeversion"] = 1,
			["gold_tooltips_ah"] = 2,
			["debug_flags"] = {
				["display"] = {
					["enabled"] = false,
					["color"] = false,
				},
				["lr_initpath_v"] = {
					["enabled"] = false,
					["color"] = false,
				},
			},
			["showcountsteps"] = 2,
			["Inventory Manager"] = {
				["lastParentName"] = "Blizzard",
				["showText"] = false,
				["isSnapped"] = true,
				["lastParent"] = {
					["PortraitButton"] = {
						["Highlight"] = {
						},
					},
					["Portrait"] = {
					},
					["isHelpBoxShown"] = true,
					["bags"] = {
					},
					["forceExtended"] = false,
					["allBags"] = false,
					["ClickableTitleFrame"] = {
					},
					["bagsShown"] = 0,
					["size"] = 16,
				},
				["isSnappedSize"] = 169,
				["im_lastWidth"] = 169,
			},
			["fontsecsize"] = 9.9,
			["gear_selected_build"] = 1,
			["show_ui"] = true,
			["stickycolored"] = false,
			["frame_positions"] = {
				["ZygorGuidesViewerPointer_ArrowCtrl"] = {
					"TOP", -- [1]
					nil, -- [2]
					"TOP", -- [3]
					-1.77779114246368, -- [4]
					-66.5222015380859, -- [5]
				},
			},
			["gear_selected_class"] = 4,
			["n_nc_numpetguides"] = 5,
			["load_mail"] = true,
			["tmp__was_sheened"] = true,
			["slidebarconfig"] = {
				["anchor"] = "top",
				["position"] = 1173.13590932252,
			},
			["hide_dev_once"] = false,
			["frame_anchor"] = {
				"RIGHT", -- [1]
				{
					["variablesLoaded"] = true,
					["firstTimeLoaded"] = 1,
				}, -- [2]
				"RIGHT", -- [3]
				-320.983551025391, -- [4]
				117.460456848145, -- [5]
			},
			["im_town_alert"] = false,
			["gold_tooltips_shift"] = true,
		},
		["Thorend"] = {
			["gold_tooltips_shift"] = true,
			["gear_active_build"] = 1,
			["load_betaguides"] = true,
			["tmp__was_stealth"] = true,
			["fullness_search"] = 1,
			["expired_elite_shown"] = false,
			["gear_selected_build"] = 1,
			["gold_tooltips_ah"] = 2,
			["geareffects"] = true,
			["usernamed"] = true,
			["debug_flags"] = {
				["display"] = {
					["enabled"] = false,
					["color"] = false,
				},
				["lr_initpath_v"] = {
					["enabled"] = false,
					["color"] = false,
				},
			},
			["gold_profitlevel"] = 0.25,
			["autogear_protectheirlooms"] = true,
			["load_im"] = true,
			["tmp__was_sheened"] = true,
			["show_ui"] = true,
			["Inventory Manager"] = {
				["lastParentName"] = "Blizzard",
				["showText"] = false,
				["isSnapped"] = true,
				["lastParent"] = {
					["PortraitButton"] = {
						["Highlight"] = {
						},
					},
					["Portrait"] = {
					},
					["ClickableTitleFrame"] = {
					},
					["bags"] = {
					},
					["forceExtended"] = false,
					["bagsShown"] = 0,
				},
				["isSnappedSize"] = 169,
				["im_lastWidth"] = 169,
			},
			["autogear_protectheirlooms_all"] = true,
			["load_mail"] = true,
			["stickycolored"] = false,
			["frame_positions"] = {
				["ZygorGuidesViewerPointer_ArrowCtrl"] = {
					"TOP", -- [1]
					nil, -- [2]
					"TOP", -- [3]
					4.62211656570435, -- [4]
					-91.7667007446289, -- [5]
				},
			},
			["load_gold"] = true,
			["n_nc_numpetguides"] = 5,
			["gold_format_white"] = false,
			["im_always_wait"] = false,
			["im_town_alert"] = false,
			["hide_dev_once"] = false,
			["frame_anchor"] = {
				"RIGHT", -- [1]
				{
					["variablesLoaded"] = true,
					["firstTimeLoaded"] = 1,
				}, -- [2]
				"RIGHT", -- [3]
				-327.654296875, -- [4]
				161.975311279297, -- [5]
			},
			["dispprimary"] = {
				["showborder"] = true,
			},
			["gold_tooltips_guide"] = 1,
		},
	},
}
