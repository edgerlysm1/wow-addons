
KuiNameplatesCoreSaved = {
	["216_HEALTH_TRANSITION"] = true,
	["2162_PERSONAL_FRAME_SIZE_TRANSITION"] = true,
	["profiles"] = {
		["default"] = {
			["health_text"] = true,
			["font_size_normal"] = 12,
			["bar_texture"] = "Blizzard",
			["level_text"] = true,
			["font_face"] = "2002 Bold",
			["castbar_detach_offset"] = 10,
			["castbar_detach_height"] = 22,
			["castbar_detach_combine"] = false,
			["bar_animation"] = 2,
			["name_vertical_offset"] = 4,
			["health_text_friend_dmg"] = 1,
			["auras_side"] = 2,
		},
	},
}
