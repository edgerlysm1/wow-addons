
AtlasLootClassicDB = {
	["global"] = {
		["__addonrevision"] = 1050001,
	},
	["profileKeys"] = {
		["Thorddin - Faerlina"] = "Thorddin - Faerlina",
		["Thorend - Faerlina"] = "Thorend - Faerlina",
		["Bankofthor - Faerlina"] = "Bankofthor - Faerlina",
		["Sneakythor - Faerlina"] = "Sneakythor - Faerlina",
		["Thorend - Herod"] = "Thorend - Herod",
		["Thorpez - Herod"] = "Thorpez - Herod",
		["Thorwynn - Faerlina"] = "Thorwynn - Faerlina",
		["Hadren - Blaumeux"] = "Hadren - Blaumeux",
	},
	["profiles"] = {
		["Thorddin - Faerlina"] = {
			["minimap"] = {
				["minimapPos"] = 196.807042116803,
			},
			["GUI"] = {
				["selected"] = {
					nil, -- [1]
					"Ragefire", -- [2]
					nil, -- [3]
					1, -- [4]
					0, -- [5]
				},
			},
		},
		["Thorend - Faerlina"] = {
		},
		["Bankofthor - Faerlina"] = {
		},
		["Sneakythor - Faerlina"] = {
			["GUI"] = {
				["point"] = {
					nil, -- [1]
					nil, -- [2]
					"CENTER", -- [3]
					101.999992370605, -- [4]
					-1.00000762939453, -- [5]
				},
				["selected"] = {
					nil, -- [1]
					"TheTempleOfAtal'Hakkar", -- [2]
					nil, -- [3]
					1, -- [4]
					0, -- [5]
				},
			},
			["minimap"] = {
				["minimapPos"] = 179.729960048683,
			},
		},
		["Thorend - Herod"] = {
		},
		["Thorpez - Herod"] = {
			["minimap"] = {
				["minimapPos"] = 157.327323760512,
			},
		},
		["Thorwynn - Faerlina"] = {
			["GUI"] = {
				["selected"] = {
					nil, -- [1]
					"TheDeadmines", -- [2]
					nil, -- [3]
					1, -- [4]
					0, -- [5]
				},
			},
			["minimap"] = {
				["minimapPos"] = 167.17734359721,
			},
		},
		["Hadren - Blaumeux"] = {
		},
	},
}
