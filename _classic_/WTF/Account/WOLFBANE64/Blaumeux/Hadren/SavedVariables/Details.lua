
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00499,
							["friendlyfire"] = {
							},
							["damage_from"] = {
							},
							["targets"] = {
								["Young Wolf"] = 45,
							},
							["colocacao"] = 1,
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 45.00499,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 45.00499,
							["classe"] = "PRIEST",
							["serial"] = "Player-4397-00BAB9B8",
							["nome"] = "Hadren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Smite"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Young Wolf"] = 45,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 45,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 45,
										["c_max"] = 0,
										["id"] = "Smite",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["end_time"] = 1568250893,
							["last_dps"] = 8.51399735149442,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.00499,
							["start_time"] = 1568250888,
							["delay"] = 0,
							["last_event"] = 1568250892,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006174,
							["damage_from"] = {
								["Hadren"] = true,
								["Benign"] = true,
								["Porksbane"] = true,
							},
							["targets"] = {
								["Benign"] = 7,
								["Porksbane"] = 9,
							},
							["timeMachine"] = 25,
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 16.006174,
							["on_hold"] = true,
							["dps_started"] = true,
							["total"] = 16.006174,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4400-0-231-299-0000799BBA",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Porksbane"] = 0,
											["Benign"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 3,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Benign"] = 7,
											["Porksbane"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 12,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 12,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 275.006174,
							["start_time"] = 1568250912,
							["delay"] = 1568250945,
							["last_event"] = 1568250945,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 2,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 2,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 2,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Hadren",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Smite"] = 2,
							},
							["serial"] = "Player-4397-00BAB9B8",
							["classe"] = "PRIEST",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Hadren"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1568250888,
				["enemy"] = "Young Wolf",
				["combat_counter"] = 5,
				["playing_solo"] = true,
				["totals"] = {
					60.968049, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 40318.201,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "21:14:48",
				["end_time"] = 40318.201,
				["totals_grupo"] = {
					45, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 2,
				["TotalElapsedCombatTime"] = 40318.201,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Hadren"] = 45.00499,
						}, -- [1]
					},
				},
				["frags"] = {
					["Young Wolf"] = 2,
				},
				["data_fim"] = "21:14:53",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 40312.915,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001002,
							["damage_from"] = {
							},
							["targets"] = {
								["Young Wolf"] = 49,
							},
							["pets"] = {
							},
							["classe"] = "PRIEST",
							["raid_targets"] = {
							},
							["total_without_pet"] = 49.001002,
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["dps_started"] = false,
							["end_time"] = 1568250879,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Hadren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["Smite"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Young Wolf"] = 49,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 49,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 49,
										["c_max"] = 0,
										["id"] = "Smite",
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 49.001002,
							["serial"] = "Player-4397-00BAB9B8",
							["last_dps"] = 11.9894793246681,
							["custom"] = 0,
							["last_event"] = 1568250878,
							["damage_taken"] = 0.001002,
							["start_time"] = 1568250875,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006919,
							["damage_from"] = {
								["Hadren"] = true,
								["Benign"] = true,
								["Porksbane"] = true,
							},
							["targets"] = {
								["Porksbane"] = 4,
								["Benign"] = 2,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6.006919,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 6.006919,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-4400-0-231-299-0000799BAF",
							["nome"] = "Young Wolf",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									["!Melee"] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Porksbane"] = 4,
											["Benign"] = 2,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = "!Melee",
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1568250888,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 83.006919,
							["start_time"] = 1568250877,
							["delay"] = 0,
							["last_event"] = 1568250886,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Hadren",
							["grupo"] = true,
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["spell_cast"] = {
								["Smite"] = 2,
							},
							["serial"] = "Player-4397-00BAB9B8",
							["classe"] = "PRIEST",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Hadren"] = true,
				},
				["CombatStartedAt"] = 40312.915,
				["tempo_start"] = 1568250875,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					54.962455, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					49, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "21:14:39",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Young Wolf",
				["TotalElapsedCombatTime"] = 40304.056,
				["CombatEndedAt"] = 40304.056,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Hadren"] = 49.001002,
						}, -- [1]
					},
				},
				["end_time"] = 40304.056,
				["combat_id"] = 1,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "21:14:35",
				["start_time"] = 40299.969,
				["TimeData"] = {
				},
				["frags"] = {
					["Young Wolf"] = 2,
				},
			}, -- [2]
		},
	},
	["last_version"] = "v1.13.2.153",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1569546847,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Hadren-Blaumeux",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "11",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 2,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 5,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.010001,
					["damage_from"] = {
					},
					["targets"] = {
						["Young Wolf"] = 94,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "PRIEST",
					["raid_targets"] = {
					},
					["total_without_pet"] = 94.010001,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1568250879,
					["friendlyfire_total"] = 0,
					["friendlyfire"] = {
					},
					["nome"] = "Hadren",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["Smite"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 17,
								["targets"] = {
									["Young Wolf"] = 94,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 94,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 94,
								["c_max"] = 0,
								["id"] = "Smite",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 94.010001,
					["serial"] = "Player-4397-00BAB9B8",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 0.010001,
					["start_time"] = 1568250867,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.020172,
					["damage_from"] = {
						["Benign"] = true,
						["Hadren"] = true,
						["Porksbane"] = true,
					},
					["targets"] = {
						["Porksbane"] = 3,
						["Benign"] = 2,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 5.020172,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1568250879,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["nome"] = "Young Wolf",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							["!Melee"] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2,
								["targets"] = {
									["Porksbane"] = 3,
									["Benign"] = 2,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 5,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 5,
								["c_max"] = 0,
								["id"] = "!Melee",
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["friendlyfire_total"] = 0,
					["total"] = 5.020172,
					["serial"] = "Creature-0-4400-0-231-299-0000799BAF",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 118.020172,
					["start_time"] = 1568250869,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["nome"] = "Hadren",
					["grupo"] = true,
					["pets"] = {
					},
					["tipo"] = 4,
					["classe"] = "PRIEST",
					["spell_cast"] = {
						["Smite"] = 4,
					},
					["serial"] = "Player-4397-00BAB9B8",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1568250875,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			132.043387, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "21:14:35",
		["end_time"] = 40318.201,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 5.28600000000006,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "21:14:48",
			}, -- [1]
			{
				["elapsed"] = 4.08700000000681,
				["type"] = 0,
				["name"] = "Young Wolf",
				["clock"] = "21:14:35",
			}, -- [2]
		},
		["totals_grupo"] = {
			94.005992, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "21:14:53",
		["overall_enemy_name"] = "Young Wolf",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 40308.828,
		["TimeData"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 163.95068359375,
					["x"] = -603.518524169922,
					["w"] = 310.000061035156,
					["h"] = 158.000061035156,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
	},
}
