## Interface: 11304
## Title: Titan Panel Classic [|cffeda55fXP|r] |cff00aa001.2.2.11304|r
## Notes: Adds information to Titan Panel about XP earned and time to level
## Author: Titan Panel Development Team (http://www.titanpanel.org)
## SavedVariables: 
## OptionalDeps: 
## Dependencies: TitanClassic
## Version: 1.2.2.11304
## X-Child-Of: TitanClassic
TitanClassicXP.xml
