local ZygorGuidesViewer=ZygorGuidesViewer
if not ZygorGuidesViewer then return end
if UnitFactionGroup("player")~="Horde" then return end
if ZGV:DoMutex("EventsH") then return end
ZygorGuidesViewer.GuideMenuTier = "TRI"
ZygorGuidesViewer:RegisterGuide("Events Guides\\Harvest Festival\\Harvest Festival Quest",{
author="support@zygorguides.com",
condition_end=function() return completedq(8150) end,
description="\nComplete the quest \"Honoring a Hero\" for the Harvest Festival event.",
},[[
step
Reach Level 30 |ding 30
|tip You must be at least level 30 to be able to accept the quest.
|tip Use the Leveling guides to accomplish this.
step
talk Javnir Nashak##15012
accept Honoring a Hero##8150 |goto Durotar 46.10,13.77
step
Follow the path |goto Ashenvale 73.51,63.53 < 15 |only if walking
Continue following the path |goto Ashenvale 75.62,65.88 < 30 |only if walking
Continue following the path |goto Ashenvale 84.20,72.05 < 20 |only if walking
use Grom's Tribute##19851
Place a Tribute at Grom's Monument |q 8150/1 |goto Ashenvale 82.85,79.04
step
Follow the path |goto 84.20,72.05 < 20 |only if walking
Follow the path |goto 73.51,63.53 < 15 |only if walking
talk Javnir Nashak##15012
turnin Honoring a Hero##8150 |goto Durotar 46.10,13.77
step
_Congratulations!_
You Completed the "Harvest Festival" Event
|tip You can now use the food at the Harvest Festival table.
]])
