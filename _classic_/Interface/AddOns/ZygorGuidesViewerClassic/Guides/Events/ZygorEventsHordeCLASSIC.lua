local ZygorGuidesViewer=ZygorGuidesViewer
if not ZygorGuidesViewer then return end
if UnitFactionGroup("player")~="Horde" then return end
if ZGV:DoMutex("EventsH") then return end
ZygorGuidesViewer.GuideMenuTier = "CLA"
ZygorGuidesViewer:RegisterGuide("Events Guides\\Hallow's End\\Hallow's End Quests",{
author="support@zygorguides.com",
condition_end=function() return completedq(1657) end,
description="\nComplete the quests \"Hallow's End Treats for Spoops!\" and \"Stinking Up Southshore\" for the Hallow's End event.",
},[[
step
Reach Level 10 |ding 10
|tip You must be at least level 10 to be able to accept these quests.
|tip Use the Leveling guides to accomplish this.
step
Enter the building |goto Orgrimmar 71.07,23.87 < 5 |walk
talk Spoops##15309
|tip Inside the building.
accept Hallow's End Treats for Spoops!##8312 |goto 71.44,22.80
step
Enter the building |goto 54.03,68.94 < 5 |walk
talk Innkeeper Gryshka##6929
|tip Inside the building.
accept Flexing for Nougat##8359 |goto 54.10,68.40 |condition completedq(8312) or completedq(8359)
step
clicknpc Innkeeper Gryshka##6929
|tip Inside the building.
|tip Target Innkeeper Gryshka and type "/flex" into your chat to perform the Flex emote.
Flex for Inkeeper Gryshka |q 8359/1 |goto 54.10,68.40 |condition completedq(8312) or completedq(8359,1)
step
talk Innkeeper Gryshka##6929
|tip Inside the building.
turnin Flexing for Nougat##8359 |goto 54.10,68.40 |condition completedq(8312) or completedq(8359)
collect Orgrimmar Nougat##20493 |q 8312/1
step
Follow the road south |goto Durotar 46.34,18.51 < 20 |only if walking
Continue following the road south |goto 52.58,45.00 < 20 |only if walking
talk Kali Remik##11814
accept Incoming Gumdrop##8358 |goto 56.12,74.24 |condition completedq(8312) or completedq(8358)
step
clicknpc Kali Remik##11814
|tip Target Kali Remik and type "/train" into your chat to perform the Train emote.
Do the "Train" for Kali Remik |q 8358/1 |goto 56.12,74.24 |condition completedq(8312) or completedq(8358,1)
step
talk Kali Remik##11814
turnin Incoming Gumdrop##8358 |goto 56.12,74.24 |condition completedq(8312) or completedq(8358)
collect Darkspear Gumdrop##20495 |q 8312/2
step
Enter the building |goto Thunder Bluff 44.95,62.12 < 5 |walk
talk Innkeeper Pala##6746
|tip Inside the building.
accept Dancing for Marzipan##8360 |goto 45.81,64.71 |condition completedq(8312) or completedq(8360)
step
clicknpc Innkeeper Pala##6746
|tip Inside the building.
|tip Target Innkeeper Pala and type "/dance" into your chat to perform the Dance emote.
Dance for Inkeeper Pala |q 8360/1 |goto 45.81,64.71 |condition completedq(8312) or completedq(8360,1)
step
talk Innkeeper Pala##6746
|tip Inside the building.
turnin Dancing for Marzipan##8360 |goto 45.81,64.71 |condition completedq(8312) or completedq(8360)
collect Thunder Bluff Marzipan##20497 |q 8312/4
step
talk Innkeeper Norman##6741
accept Chicken Clucking for a Mint##8354 |goto Undercity 67.73,37.88 |condition completedq(8312) or completedq(8354)
step
clicknpc Innkeeper Norman##6741
|tip Target Innkeeper Norman and type "/chicken" into your chat to perform the Chicken emote.
Cluck Like a Chicken for Innkeeper Norman |q 8354/1 |goto 67.73,37.88 |condition completedq(8312) or completedq(8354,1)
step
talk Innkeeper Norman##6741
turnin Chicken Clucking for a Mint##8354 |goto 67.73,37.88 |condition completedq(8312) or completedq(8354)
collect Undercity Mint##20491 |q 8312/3
step
Enter the building |goto Orgrimmar 71.07,23.87 < 5 |walk
talk Spoops##15309
|tip Inside the building.
turnin Hallow's End Treats for Spoops!##8312 |goto 71.44,22.80
step
Reach Level 25 |ding 25
|tip You must be at least level 25 to be able to accept this quest.
|tip Use the Leveling guides to accomplish this.
step
talk Darkcaller Yanka##15197
accept Stinking Up Southshore##1657 |goto Tirisfal Glades 55.57,69.90
step
Avoid Southshore |goto Hillsbrad Foothills 45.12,49.16 < 15 |only if walking
Avoid Southshore guards |goto 46.66,58.05 < 7 |only if walking
use Forsaken Stink Bomb Cluster##20387
|tip Use it while standing here to throw stink bombs on the ground.
Toss Stink Bombs into Southshore |q 1657/1 |goto 48.42,57.86
step
talk Darkcaller Yanka##15197
turnin Stinking Up Southshore##1657 |goto Tirisfal Glades 55.57,69.90
step
_Congratulations!_
You Completed the "Hallow's End" Event
]])
ZygorGuidesViewer:RegisterGuide("Events Guides\\Harvest Festival\\Harvest Festival Quest",{
author="support@zygorguides.com",
condition_end=function() return completedq(8150) end,
description="\nComplete the quest \"Honoring a Hero\" for the Harvest Festival event.",
},[[
step
Reach Level 30 |ding 30
|tip You must be at least level 30 to be able to accept the quest.
|tip Use the Leveling guides to accomplish this.
step
talk Javnir Nashak##15012
accept Honoring a Hero##8150 |goto Durotar 46.10,13.77
step
Follow the path |goto Ashenvale 73.51,63.53 < 15 |only if walking
Continue following the path |goto Ashenvale 75.62,65.88 < 30 |only if walking
Continue following the path |goto Ashenvale 84.20,72.05 < 20 |only if walking
use Grom's Tribute##19851
Place a Tribute at Grom's Monument |q 8150/1 |goto Ashenvale 82.85,79.04
step
Follow the path |goto 84.20,72.05 < 20 |only if walking
Follow the path |goto 73.51,63.53 < 15 |only if walking
talk Javnir Nashak##15012
turnin Honoring a Hero##8150 |goto Durotar 46.10,13.77
step
_Congratulations!_
You Completed the "Harvest Festival" Event
|tip You can now use the food at the Harvest Festival table.
]])
