local ZygorGuidesViewer=ZygorGuidesViewer
if not ZygorGuidesViewer then return end
if UnitFactionGroup("player")~="Alliance" then return end
if ZGV:DoMutex("EventsA") then return end
ZygorGuidesViewer.GuideMenuTier = "CLA"
ZygorGuidesViewer:RegisterGuide("Events Guides\\Hallow's End\\Hallow's End Quests",{
author="support@zygorguides.com",
condition_end=function() return completedq(8373) end,
description="\nComplete the quests \"Hallow's End Treats for Jesper!\" and \"The Power of Pine\" for the Hallow's End event.",
},[[
step
Reach Level 10 |ding 10
|tip You must be at least level 10 to be able to accept these quests.
|tip Use the Leveling guides to accomplish this.
step
Enter the building |goto Stormwind City 47.40,37.30 < 5 |walk
talk Jesper##15310
|tip Inside the building.
accept Hallow's End Treats for Jesper!##8311 |goto 47.63,35.32
step
Enter the building |goto 53.39,64.96 < 5 |walk
talk Innkeeper Allison##6740
|tip Inside the building.
accept Flexing for Nougat##8356 |goto 52.61,65.71 |condition completedq(8311) or completedq(8356)
step
clicknpc Innkeeper Allison##6740
|tip Inside the building.
|tip Target Innkeeper Allison and type "/flex" into your chat to perform the Flex emote.
Flex for Inkeeper Allison |q 8356/1 |goto 52.61,65.71 |condition completedq(8311) or completedq(8356,1)
step
talk Innkeeper Allison##6740
|tip Inside the building.
turnin Flexing for Nougat##8356 |goto 52.61,65.71 |condition completedq(8311) or completedq(8356)
collect Stormwind Nougat##20492 |q 8311/1
step
Enter the building |goto Ironforge 20.57,53.22 < 5 |walk
talk Innkeeper Firebrew##5111
|tip Inside the building.
accept Chicken Clucking for a Mint##8353 |goto 18.16,51.44 |condition completedq(8311) or completedq(8353)
step
clicknpc Innkeeper Firebrew##5111
|tip Inside the building.
|tip Target Innkeeper Firebrew and type "/chicken" into your chat to perform the Chicken emote.
Cluck Like a Chicken for Innkeeper Firebrew |q 8353/1 |goto 18.16,51.44 |condition completedq(8311) or completedq(8353,1)
step
talk Innkeeper Firebrew##5111
|tip Inside the building.
turnin Chicken Clucking for a Mint##8353 |goto 18.16,51.44 |condition completedq(8311) or completedq(8353)
collect Ironforge Mint##20490 |q 8311/3
step
talk Talvash del Kissel##6826
accept Incoming Gumdrop##8355 |goto 36.38,3.62 |condition completedq(8311) or completedq(8355)
step
clicknpc Talvash del Kissel##6826
|tip Target Talvash del Kissel and type "/train" into your chat to perform the Train emote.
Do the "Train" for Talvash |q 8355/1 |goto 36.38,3.62 |condition completedq(8311) or completedq(8355,1)
step
talk Talvash del Kissel##6826
turnin Incoming Gumdrop##8355 |goto 36.38,3.62 |condition completedq(8311) or completedq(8355)
collect Gnomeregan Gumdrop##20494 |q 8311/2
step
talk Innkeeper Saelienne##6735
accept Dancing for Marzipan##8357 |goto Darnassus 67.42,15.65 |condition completedq(8311) or completedq(8357)
step
clicknpc Innkeeper Saelienne##6735
|tip Target Innkeeper Saelienne and type "/dance" into your chat to perform the Dance emote.
Dance for Inkeeper Saelienne |q 8357/1 |goto 67.42,15.65 |condition completedq(8311) or completedq(8357,1)
step
talk Innkeeper Saelienne##6735
turnin Dancing for Marzipan##8357 |goto 67.42,15.65 |condition completedq(8311) or completedq(8357)
collect Darnassus Marzipan##20496 |q 8311/4
step
Enter the building |goto Stormwind City 47.40,37.30 < 5 |walk
talk Jesper##15310
|tip Inside the building.
turnin Hallow's End Treats for Jesper!##8311 |goto 47.63,35.32
step
Reach Level 25 |ding 25
|tip You must be at least level 25 to be able to accept this quest.
|tip Use the Leveling guides to accomplish this.
step
talk Sergeant Hartman##15199
accept The Power of Pine##8373 |goto Hillsbrad Foothills 50.00,57.34
step
use Stink Bomb Cleaner##20604
|tip Use it next to a stink bomb placed by a Horde player.
|tip They look like small metal objects surrounded by orange smoke on the ground around Southshore.
|tip Run around Southshore and look for them, they will most likely be near the outskirts of the town.
Clean Up a Stink Bomb in Southshore |q 8373/1 |goto 48.61,57.82
step
talk Sergeant Hartman##15199
turnin The Power of Pine##8373 |goto 50.00,57.34
step
_Congratulations!_
You Completed the "Hallow's End" Event
]])
ZygorGuidesViewer:RegisterGuide("Events Guides\\Harvest Festival\\Harvest Festival Quest",{
author="support@zygorguides.com",
condition_end=function() return completedq(8149) end,
description="\nComplete the quest \"Honoring a Hero\" for the Harvest Festival event.",
},[[
step
Reach Level 30 |ding 30
|tip You must be at least level 30 to be able to accept the quest.
|tip Use the Leveling guides to accomplish this.
step
_Note:_
|tip While you can accept this quest at level 30, you must go to an area with level 50-52 enemies.
|tip If you are not high enough level to kill enemies of that level, try to find someone to help you.
Click Here to Continue |confirm |q 8149 |future
step
talk Wagner Hammerstrike##15011
accept Honoring a Hero##8149 |goto Dun Morogh 52.60,36.03
step
Follow the path |goto Western Plaguelands 46.61,81.31 < 15 |only if walking
Follow the path up |goto Western Plaguelands 51.62,79.94 < 20 |only if walking
Enter the building |goto Western Plaguelands 52.07,83.22 < 10 |walk
use Uther's Tribute##19850
|tip Inside the building.
Place a Tribute at Uther's Tomb |q 8149/1 |goto 52.14,83.57
step
Follow the path |goto 46.61,81.31 < 15 |only if walking
talk Wagner Hammerstrike##15011
turnin Honoring a Hero##8149 |goto Dun Morogh 52.60,36.03
step
_Congratulations!_
You Completed the "Harvest Festival" Event
|tip You can now use the food at the Harvest Festival table.
]])
