ZGV.Quest_Cache_Alliance = {
	["EVENTS\\Hallow's End\\Hallow's End Quests"] = {
		{ids="8356,8353,8355,8357,8311,8373"},
	},
	["EVENTS\\Harvest Festival\\Harvest Festival Quest"] = {
		{ids="8149"},
	},
	["LEVELING\\Ashenvale (21-22)"] = {
		{ids="1020,991,1007,1009,1023,1008,1024,1033,1025"},
	},
	["LEVELING\\Desolace (34-36)"] = {
		{ids="1453,6141,1437,1465,1458,5561,1382,1459,1387,5501,1454,1438,1439,1440,1455,5741,6161,6027,1456,1384,1370"},
	},
	["LEVELING\\Redridge Mountains (18-20)"] = {
		{ids="1685,1507,1738,1739", cond_if=[[Warlock ]]},
		{ids="244,125,3741,129,118,130,131,119,246,122,92,127,20,89,124,150"},
	},
	["LEVELING\\Tanaris (43-44)"] = {
		{ids="2864,1117,1137,1187,1190,1194,1188,2872,1707,2781,1690,992,8365,2875,8366,2876,2873,1691"},
	},
	["LEVELING\\Burning Steppes (55-56)"] = {
		{ids="3702,4324,4022,4726,4296,3823,4182,3824,4283,3825,4183,4184,4185,4186,4223,4224"},
	},
	["LEVELING\\Ashenvale (24-24)"] = {
		{ids="1134,990,1034,973,1054,1016"},
	},
	["LEVELING\\Teldrassil (1-12) [Night Elf Starter]"] = {
		{ids="3117,6063,6101,6102,6103", cond_if=[[NightElf Hunter ]]},
		{ids="3119,5629", cond_if=[[NightElf Priest ]]},
		{ids="6344,6341,6342,6343", cond_if=[[NightElf ]]},
		{ids="3120,5923,5921,5929,5931,6001", cond_if=[[NightElf Druid ]]},
		{ids="3118", cond_if=[[NightElf Rogue ]]},
		{ids="3116", cond_if=[[NightElf Warrior ]]},
		{ids="456,458,459,4495,916,457,3519,917,920,3521,3522,921,488,2159,928,475,489,476,2438,929,997,918,919,2459,2541,2561,483,932,487,930,933,937,7383,931,486,922,935,940,2519,938,923,2518,2520,2498"},
	},
	["LEVELING\\Eastern Plaguelands (57-58)"] = {
		{ids="5601,5149,6030,5241,6026,5226,6185,5245,5281,6164,5542,5543,5544,5742,5781,6021,5211,5903,5845"},
	},
	["LEVELING\\Azshara (46-46)"] = {
		{ids="5535,5536"},
	},
	["LEVELING\\Extra Zones\\Westfall"] = {
		{ids="6181,6281,6261,6285", cond_if=[[Human ]]},
		{ids="36,109,12,153,64,151,9,22,38,102,13,14,65,132,135,141,142,103,104,155"},
	},
	["LEVELING\\Dun Morogh (1-12) [Dwarf & Gnome Starter]"] = {
		{ids="3112", cond_if=[[Gnome Warrior ]]},
		{ids="3109", cond_if=[[Dwarf Rogue ]]},
		{ids="3107,2997,1646,1647,1648,1778,1779,1783,1784,1785", cond_if=[[Dwarf Paladin ]]},
		{ids="3115,1599,1715,1688,1689", cond_if=[[Gnome Warlock ]]},
		{ids="3108,6064,6084,6085,6086", cond_if=[[Dwarf Hunter ]]},
		{ids="3110,6661,6662,5635", cond_if=[[Dwarf Priest ]]},
		{ids="3106", cond_if=[[Dwarf Warrior ]]},
		{ids="3114", cond_if=[[Gnome Mage ]]},
		{ids="1679,1678", cond_if=[[(Dwarf Warrior) or (Gnome Warrior) ]]},
		{ids="3113", cond_if=[[Gnome Rogue ]]},
		{ids="6387,6391,6388,6392", cond_if=[[Dwarf or Gnome ]]},
		{ids="179,170,233,183,234,3364,3365,182,218,3361,282,420,2160,400,5541,384,317,313,312,318,310,311,319,315,287,412,320,433,432,419,417,413,224,267,414,1339,307,418,416,291"},
	},
	["LEVELING\\Stranglethorn Vale (46-47)"] = {
		{ids="340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="2874,580,1118,603,610,608,197,193,208,611"},
	},
	["LEVELING\\Hillsbrad Foothills (31-33)"] = {
		{ids="536,559,560,561,562,555,564,565,681,2925,689,700,335,293,322,336,563"},
	},
	["LEVELING\\Silithus (58-59)"] = {
		{ids="1124,8275,1125,1126,8280,8277,8318,9415,8308,8281,8278,8284,8304,8285,8279,8323,8282,8287,8283,4513,5210,5181"},
	},
	["LEVELING\\Felwood (54-54)"] = {
		{ids="4101,5159,939,4906,5156,5165"},
	},
	["LEVELING\\Azshara (53-53)"] = {
		{ids="3601,5534,3449"},
	},
	["LEVELING\\Darkshore (20-20)"] = {
		{ids="3765,948,944,949,729,950,5321,5713,967"},
	},
	["LEVELING\\Western Plaguelands (55-55)"] = {
		{ids="3461,4512,5091,6028,5092"},
	},
	["LEVELING\\Winterspring (54-55)"] = {
		{ids="978,4861,4863,979,3783,4864,4842,5083,5082,8464,977,5084,5085"},
	},
	["LEVELING\\Arathi Highlands (38-38)"] = {
		{ids="690,642,659,658,657,660,691,693,651,663,665,662,664,666,668,661,514,1112,1114,1183,1186"},
	},
	["LEVELING\\Western Plaguelands (58-58)"] = {
		{ids="5846,5152,5153,5154,4971,4972,5904,6004,6023,6389,5097,5533,5537,6186,4901,4986,1019,4902,6761,6762,4441,4442"},
	},
	["LEVELING\\Elwynn Forest (1-13) [Human Starter]"] = {
		{ids="3101,2998,1641,1642,1643,1644,1780,1781,1786,1787,1788", cond_if=[[Human Paladin ]]},
		{ids="1685,1688", cond=[[Human Warlock]]},
		{ids="3103,5637", cond_if=[[Human Priest ]]},
		{ids="3104", cond_if=[[Human Mage ]]},
		{ids="3102", cond_if=[[Human Rogue ]]},
		{ids="3105,1598,1689", cond_if=[[Human Warlock ]]},
		{ids="3100,1638,1639,1640,1665", cond_if=[[Human Warrior ]]},
		{ids="783,5261,33,7,18,15,3903,3904,6,3905,21,54,2158,106,111,85,86,84,60,107,47,40,62,35,37,45,5545,71,112,39,76,52,46,59,83,114,87,239,11,61,1097,6661,433,432,419,417,224,267,353,307,418,416"},
	},
	["LEVELING\\Loch Modan (17-18)"] = {
		{ids="272,5061", cond_if=[[NightElf Druid ]]},
		{ids="436,297,257,258,250,199,2038,385,298,301,6661,6662,2041,1338"},
	},
	["LEVELING\\Felwood (53-54)"] = {
		{ids="8460,5155,4421,5157,8462,8461"},
	},
	["LEVELING\\Ashenvale (30-31)"] = {
		{ids="1718,1719", cond_if=[[Warrior ]]},
		{ids="1026,4581,1021,1011,1035,1022,1031,1027,1028,1055,1029,1030,1045,1046,1032,1140,1037,1038"},
	},
	["LEVELING\\Duskwood (27-30)"] = {
		{ids="66,174,175,56,173,177,221,163,165,164,67,5,93,57,68,225,227,222,223,101,148,181,149,154,95,240,157,230,158,58,262,265,266,453,268,323,156,159,133,134,160,251,401,252,269,1274,1241,69,1242,70,72,2923"},
	},
	["LEVELING\\Stranglethorn Vale (39-40)"] = {
		{ids="340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="1115,669,595,196,601,577,597,599,606,607"},
	},
	["LEVELING\\Searing Gorge (47-48)"] = {
		{ids="3367,4449,3441,3368,3442,3371,3443,7701,7723,7724,7727,7728,7729,7722,3452,3453,3454,3462,4451,3463,3481,3181,3182,3201,624,3661,2944"},
	},
	["LEVELING\\Western Plaguelands (56-57)"] = {
		{ids="3701,5215,5216,5021,5022,5048,6182,6183,5217,6184,5050,5051,5219,5220,5222,4984,4985,5223,5225"},
	},
	["LEVELING\\Un'Goro Crater (51-53)"] = {
		{ids="3884", cond_if=[[havequest(3884) or completedq(3884) ]]},
		{ids="162,2641,4493,2661,2662,3444,4243,4244,4245,4289,4290,3844,4291,4503,4141,4501,3882,3883,3881,3884,4284,3845,4492,4491,4301,4292,974,4285,4287,4288,4321,4504,4496,3790,3764,3781"},
	},
	["LEVELING\\Darkshore (21-21)"] = {
		{ids="4733,4732,4731,4730,4740,986,731"},
	},
	["LEVELING\\Blasted Lands (50-51)"] = {
		{ids="3501", cond_if=[[havequest(3501) or completedq(3501) ]]},
		{ids="2601,2603,2581,2583,2585"},
	},
	["LEVELING\\The Hinterlands (50-50)"] = {
		{ids="1189,3841,3842,3843,1452,626,4297,4298,2989,2767,2990,2783,2801"},
	},
	["LEVELING\\Winterspring (54-54)"] = {
		{ids="8465,980,3908,5249,5244,6604,4502,5158"},
	},
	["LEVELING\\Feralas (48-49)"] = {
		{ids="2943,7003,7721,2879,2844,2845,7725,2942,7733,7735"},
	},
	["LEVELING\\Darkshore (12-17)"] = {
		{ids="952", cond_if=[[NightElf ]]},
		{ids="6121,6122,6123,6124,6125,26,29", cond_if=[[NightElf Druid ]]},
		{ids="983,3524,4681,1001,954,955,956,953,963,4723,4811,2118,984,958,4761,957,1002,2138,982,4762,4725,4727,4812,1003,4722,4728,4813,985,965,966,2139,4763,947,1138"},
	},
	["LEVELING\\Dustwallow Marsh (38-39)"] = {
		{ids="1203", cond_if=[[(itemcount(3853) >= 1 or completedq(1203)) ]]},
		{ids="1260,1282,1218,1204,1222,1219,1220,1284,1252,1253,1259,1285,1319,1320,1206,1177,1286,1287"},
	},
	["LEVELING\\Stonetalon Mountains (22-23)"] = {
		{ids="1070,1085,1093,1071,1056"},
	},
	["LEVELING\\Winterspring (59-60)"] = {
		{ids="8798", cond_if=[[havequest(8798) or completedq(8798) ]]},
		{ids="5086,5087,5163,4808,4970,4809,4810"},
	},
	["LEVELING\\The Hinterlands (46-46)"] = {
		{ids="2941,1052,602,1449,1450,1451,485,2877,2880,2988,1467,3448,3450,3451"},
	},
	["LEVELING\\Thousand Needles (34-34)"] = {
		{ids="1100,1179,1110,1104,1105,1176,1175"},
	},
	["LEVELING\\Ashenvale (20-21)"] = {
		{ids="1010,970,945"},
	},
	["LEVELING\\Wetlands (31-31)"] = {
		{ids="290,465,292,472,631,632,633,634"},
	},
	["LEVELING\\Wetlands (24-27)"] = {
		{ids="2360,2359,2607,2608", cond_if=[[Rogue ]]},
		{ids="942,288,305,294,295,299,296,463,276,277,943,279,484,469,464,470,306,281,284,285,275,289,286,471,637,1072,1073,683,686,1075"},
	},
	["LEVELING\\Feralas (44-46)"] = {
		{ids="4124,2866,2867,3130,2869,4125,4127,2870,2871,4129,4130,2766,2969,2970,4131,4135,2821,2982,4265,4266,4281,3022,2939,2940,2972,4267"},
	},
	["LEVELING\\Desolace (42-43)"] = {
		{ids="1373,6134,1374,261,1466"},
	},
	["LEVELING\\Stranglethorn Vale (42-42)"] = {
		{ids="1116,600,621,209,587,604,617,609,628,576,623,1258"},
	},
	["LEVELING\\Swamp of Sorrows (41-42)"] = {
		{ids="1363,1477,1392,1396,1393,1389,1421,1364,1423,1395,1398,1425,1448"},
	},
	["LEVELING\\Badlands (40-41)"] = {
		{ids="707,720,719,718,705,1106,738,1108,713,714,715,710,711,712,734,716,777,778,703,733,732,2500,739"},
	},
	["LEVELING\\Hillsbrad Foothills (37-38)"] = {
		{ids="511,505,510,500,512"},
	},
	["LEVELING\\Stranglethorn Vale (33-34)"] = {
		{ids="340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="583,185,186,190,191,194,210,627,616,1039"},
	},
	["LEVELING\\Stranglethorn Vale (36-37)"] = {
		{ids="340,340,340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341,341,341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339,339,339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338,338,338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342,342,342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="1178,1111,1180,1040,1181,1041,1042,1043,1457,1044,195,5762,187,192,188,198,622,215,200,203,204,328,574,329,330,331,605,201,189,213,1182,578,575"},
	},
	["LEVELING\\Duskwood (33-33)"] = {
		{ids="74,75,1243,78,79,80,97,98"},
	},
	["LEVELING\\Wetlands (30-30)"] = {
		{ids="270,321,324"},
	},
	["LEVELING\\Darkshore (23-24)"] = {
		{ids="995", cond_if=[[havequest(995) or completedq(995) ]]},
		{ids="994", cond_if=[[havequest(994) or completedq(994) ]]},
		{ids="2098,2078,951,993,741"},
	},
	["LEVELING\\Redridge Mountains (27-27)"] = {
		{ids="34,219,128,91,180"},
	},
	["LEVELING\\Tanaris (49-50)"] = {
		{ids="625,4450,3445,1560,3520,2605,2606,82,3362,5863,3161,351,10,110,113"},
	},
}
ZGV.Quest_Cache_Horde = {
	["EVENTS\\Hallow's End\\Hallow's End Quests"] = {
		{ids="8359,8358,8360,8354,8312,1657"},
	},
	["EVENTS\\Harvest Festival\\Harvest Festival Quest"] = {
		{ids="8150"},
	},
	["LEVELING\\Ashenvale (25-27)"] = {
		{ids="2460,2458,2478,2479,2480", cond_if=[[Rogue ]]},
		{ids="6641,25,1918,23,6544,6503,24,6441,824,6482,216,6462"},
	},
	["LEVELING\\Hillsbrad Foothills (29-30)"] = {
		{ids="493,494,501,509,552,502,527,567"},
	},
	["LEVELING\\Desolace (32-34)"] = {
		{ids="1145,1431,2,5561,1362,5361,1432,1433,1365,1368,5386,1435,1480,1434,1481,5741,6161,6027,1482,1484,1366,5501,5381,1370,6143,6142"},
	},
	["LEVELING\\Dustwallow Marsh (43-44)"] = {
		{ids="1273,1169,1170,1171,1261,1276,1205"},
	},
	["LEVELING\\Extra Zones\\Silverpine Forest"] = {
		{ids="445,6321,6323,6322,6324", cond_if=[[Scourge ]]},
		{ids="435,449,3221,421,428,429,437,422,447,1359,477,438,430,425,439,478,423,481,482,424,479,440,441,264,460,461,443,530,444,491"},
	},
	["LEVELING\\Durotar (1-12) [Orc & Troll Starter]"] = {
		{ids="3086", cond_if=[[Troll Mage ]]},
		{ids="1505,1498", cond_if=[[Warrior ]]},
		{ids="3083", cond_if=[[Troll Rogue ]]},
		{ids="1516,1517,1518,2983,1524,1525,1526,1527", cond_if=[[Shaman ]]},
		{ids="2383", cond_if=[[Orc Warrior ]]},
		{ids="3090,1485,1499", cond_if=[[Orc Warlock ]]},
		{ids="3084", cond_if=[[Troll Shaman ]]},
		{ids="792", cond_if=[[not Orc Warlock ]]},
		{ids="3089", cond_if=[[Orc Shaman ]]},
		{ids="6062,6083,6082,6081", cond_if=[[Hunter ]]},
		{ids="3082", cond_if=[[Troll Hunter ]]},
		{ids="1506,1501,1474", cond_if=[[Warlock ]]},
		{ids="3087", cond_if=[[Orc Hunter ]]},
		{ids="3088", cond_if=[[Orc Rogue ]]},
		{ids="3085,5652", cond_if=[[Troll Priest ]]},
		{ids="3065", cond_if=[[Troll Warrior ]]},
		{ids="4641,788,4402,5441,790,789,804,6394,794,805,823,2161,784,830,825,791,786,818,808,826,817,815,837,834,835,831,813,812,816,806,828,827,829,5726,5727"},
	},
	["LEVELING\\Winterspring (59-60)"] = {
		{ids="8798", cond_if=[[havequest(8798) or completedq(8798) ]]},
		{ids="5086,5087,5163,4741,4809,4721,4882,4883,4810"},
	},
	["LEVELING\\Stranglethorn Vale (42-43)"] = {
		{ids="669,1116,628,600,621,209,587,604,573,598,617,609,576"},
	},
	["LEVELING\\Stonetalon Mountains (23-25)"] = {
		{ids="6421,6301,1087,1095,1096,1058,1068"},
	},
	["LEVELING\\Thousand Needles (38-38)"] = {
		{ids="1146,1112,1114,1183,1186,1147,1148"},
	},
	["LEVELING\\Azshara (46-46)"] = {
		{ids="5535,5536,3504,232,238"},
	},
	["LEVELING\\Alterac Mountains (37-38)"] = {
		{ids="553,544,556,545,557,1164"},
	},
	["LEVELING\\The Hinterlands (51-52)"] = {
		{ids="626,7843,7840,7815,7816,3562,3563,3564,2767,580"},
	},
	["LEVELING\\Desolace (44-44)"] = {
		{ids="1373,1488,6134,1374,5581"},
	},
	["LEVELING\\Feralas (49-50)"] = {
		{ids="3123,3124,3125,3126,7003,7721,7725,3127,3129,7734,7738,3062,3063"},
	},
	["LEVELING\\Western Plaguelands (58-58)"] = {
		{ids="5236,5901,5050,5051,5902,6004,6023,5152,5153,5154,4971,4972,6390,5098,838,964,4987,1004,1123,4120,7492"},
	},
	["LEVELING\\Eastern Plaguelands (57-58)"] = {
		{ids="5601,5149,6030,5241,6026,5281,6164,5542,5543,5544,5742,6022,6042,5781,6021,5211,5845,5846,5023,5049"},
	},
	["LEVELING\\Western Plaguelands (56-57)"] = {
		{ids="4293,4294,5094,6029,5021,5096,5228,5229,5230,5231,5232,5233,5234,4984,4985,5235"},
	},
	["LEVELING\\Winterspring (56-56)"] = {
		{ids="5085,977"},
	},
	["LEVELING\\Tanaris (50-51)"] = {
		{ids="243,4450,3380,1560,3520,2605,379,2606,82,3362,5863,3161,351,654,10,110,113"},
	},
	["LEVELING\\Thousand Needles (25-25)"] = {
		{ids="5881,4542,4841"},
	},
	["LEVELING\\Burning Steppes (54-54)"] = {
		{ids="4324,4022,3821,4726,4296,4061,3822,4062"},
	},
	["LEVELING\\The Barrens (22-23)"] = {
		{ids="1534,220,63,100,96", cond_if=[[Shaman ]]},
		{ids="822", cond_if=[[_G.GetCurrentRegionName()~='EU' ]]},
		{ids="1094,1069,843,893,884,885,879,906,868"},
	},
	["LEVELING\\Stonetalon Mountains (15-16)"] = {
		{ids="6548,6629,6523"},
	},
	["LEVELING\\Felwood (55-56)"] = {
		{ids="4521,4102,5155,5156,8460,4505,6162,4506,5157,5084,8462"},
	},
	["LEVELING\\Felwood (54-55)"] = {
		{ids="8461"},
	},
	["LEVELING\\Azshara (54-54)"] = {
		{ids="3505,3601,3506,5534,4300,3507"},
	},
	["LEVELING\\The Hinterlands (46-48)"] = {
		{ids="650,485,77,2742,7839,7844,7841,7842,7828,7829,7830,2933,1429,2934,2995,2782"},
	},
	["LEVELING\\Silithus (58-59)"] = {
		{ids="1124,1125,8276,1126,8280,8277,8318,9416,8308,8281,8278,8284,8304,8285,8279,8323,8282,8287,8283,4642,5210,5181,5527"},
	},
	["LEVELING\\Swamp of Sorrows (42-42)"] = {
		{ids="1420,1426,1427,1430"},
	},
	["LEVELING\\Feralas (45-46)"] = {
		{ids="2862,2987,2973,2975,2978,2863,2902,2903,2974,2766,2822,7730,7731,2979,2980,7732,649,2976,3121,1262,3002,3122,3128"},
	},
	["LEVELING\\Swamp of Sorrows (39-40)"] = {
		{ids="1372,1392,698,1393,1389,1424,699,1422,1184"},
	},
	["LEVELING\\Swamp of Sorrows (49-49)"] = {
		{ids="1444,2784,624,2621,2622,1428,2623,2801"},
	},
	["LEVELING\\Un'Goro Crater (52-54)"] = {
		{ids="3884", cond_if=[[havequest(3884) or completedq(3884) ]]},
		{ids="2641,4494,2661,2662,3444,4243,4244,4245,4289,4290,3844,4291,4503,4501,3882,3883,3881,3884,4284,4145,3845,4492,4491,4301,4292,974,4285,4287,4288,4321,4504,4496,3761,3782,4502,4147,4133"},
	},
	["LEVELING\\Azshara (51-51)"] = {
		{ids="3517,3561,3565,3518,32,81,3541,3542,3568,3569,864"},
	},
	["LEVELING\\The Barrens (25-25)"] = {
		{ids="846,849"},
	},
	["LEVELING\\Winterspring (55-55)"] = {
		{ids="8465,980,3908,4808,3783,4842,5082,5083,8464"},
	},
	["LEVELING\\The Barrens (12-15)"] = {
		{ids="6126,6127,6128,6129,6130", cond_if=[[Druid ]]},
		{ids="809,6365,6384,6385,6386", cond_if=[[Orc or Troll ]]},
		{ids="840,842,844,871,845,1492,819,887,895,890,892,872,5041,850,855"},
	},
	["LEVELING\\Tirisfal Glades (1-12) [Undead Starter]"] = {
		{ids="3098", cond_if=[[Scourge Mage ]]},
		{ids="3097,5658", cond_if=[[Scourge Priest ]]},
		{ids="3096", cond_if=[[Scourge Rogue ]]},
		{ids="3099,1470,1478,1473,1471", cond_if=[[Scourge Warlock ]]},
		{ids="3095,1818,1819,1820", cond_if=[[Scourge Warrior ]]},
		{ids="363,364,376,3901,3902,380,6395,381,382,5481,383,8,367,404,590,354,362,361,375,355,427,365,426,407,398,358,368,5482,374,370,359,360,371,369,492,356,372"},
	},
	["LEVELING\\Dustwallow Marsh (38-39)"] = {
		{ids="1203", cond_if=[[havequest(1203) or completedq(1203) ]]},
		{ids="1136,1218,1201,1238,1322,1323,1177,1206,1202,1239,1270"},
	},
	["LEVELING\\The Barrens (16-21)"] = {
		{ids="26,28,30,31", cond_if=[[Druid ]]},
		{ids="1507,1508,1509,1510,1511,1515,1512,1474", cond_if=[[Warlock ]]},
		{ids="848,867,903,870,869,853,4921,894,900,901,858,875,881,888,902,863,896,865,877,880,851,3921,3922,6541,876,3281,905,3261,883,1130,1489,1490,821,891,882,907,878,5052,913,899,852"},
	},
	["LEVELING\\Mulgore (1-12) [Tauren Starter]"] = {
		{ids="3094,5926,5922,5930,5932,6002", cond_if=[[Tauren Druid ]]},
		{ids="748,754,756,758,759,760,854", cond_if=[[Tauren ]]},
		{ids="3092,6061,6087,6089", cond_if=[[Tauren Hunter ]]},
		{ids="3091,1505,1498", cond_if=[[Tauren Warrior ]]},
		{ids="3093,1519,1520,1521,2984,1524,1525,1526,1527", cond_if=[[Tauren Shaman ]]},
		{ids="752,747,753,755,750,780,3376,757,763,1656,767,761,745,743,771,749,772,766,751,773,833,746,764,765,775,776,744,861,886,860"},
	},
	["LEVELING\\Arathi Highlands (40-40)"] = {
		{ids="663,665,662,664,666,668"},
	},
	["LEVELING\\Dustwallow Marsh (49-49)"] = {
		{ids="625,1172,1173"},
	},
	["LEVELING\\Thousand Needles (31-32)"] = {
		{ids="1361,1110,1104,1105,1176,1175"},
	},
	["LEVELING\\Blasted Lands (52-52)"] = {
		{ids="3501", cond_if=[[havequest(3501) or completedq(3501) ]]},
		{ids="2601,2603,2581,2583,2585"},
	},
	["LEVELING\\Stonetalon Mountains (21-22)"] = {
		{ids="1060,1483,1093,6401,6461,1062"},
	},
	["LEVELING\\Searing Gorge (48-49)"] = {
		{ids="4449,3441,3442,3443,7701,7723,7724,7727,7728,7729,7722,3452,3453,3454,3462,4451,3463,3481"},
	},
	["LEVELING\\Arathi Highlands (36-37)"] = {
		{ids="638,642,651,676,671,701,702,847,677,678"},
	},
	["LEVELING\\Thousand Needles (28-29)"] = {
		{ids="1196,1149,4821,1197,4865,5062,5064,5088,4770,4881,4966,4904,5147,4767,1131"},
	},
	["LEVELING\\Stranglethorn Vale (48-48)"] = {
		{ids="340,340,340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341,341,341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339,339,339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338,338,338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342,342,342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="2932,197,193,208,1118,2874,608"},
	},
	["LEVELING\\Badlands (40-42)"] = {
		{ids="705,1106,1108,713,714,710,711,2258,782,1419,712,734,777,778,715,716,703"},
	},
	["LEVELING\\Stranglethorn Vale (39-39)"] = {
		{ids="340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="1115,1240,572,629,196,577,595,606,597,607,599,571"},
	},
	["LEVELING\\Tanaris (44-45)"] = {
		{ids="2864,1117,1137,1187,1190,1194,1188,2872,1707,2781,1690,992,8365,2875,8366,2876,2873,1691"},
	},
	["LEVELING\\Stranglethorn Vale (34-36)"] = {
		{ids="340,340,340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341,341,341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339,339,339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338,338,338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342,342,342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="1180,1181,568,581,596,187,195,5762,5763,188,192,605,189,213,201,1182,575,569,570,582"},
	},
	["LEVELING\\Dustwallow Marsh (34-34)"] = {
		{ids="1268,1269,1251,1321,1178,1111"},
	},
	["LEVELING\\Stranglethorn Vale (31-31)"] = {
		{ids="340", cond_if=[[((itemcount(2734) >= 1) and (itemcount(2735) >= 1) and (itemcount(2738) >= 1) and (itemcount(2740) >= 1) and (not completedq(340))) ]]},
		{ids="341", cond_if=[[((itemcount(2742) >= 1) and (itemcount(2744) >= 1) and (itemcount(2745) >= 1) and (itemcount(2748) >= 1) and (not completedq(341))) ]]},
		{ids="339", cond_if=[[((itemcount(2725) >= 1) and (itemcount(2728) >= 1) and (itemcount(2730) >= 1) and (itemcount(2732) >= 1) and (not completedq(339))) ]]},
		{ids="338", cond_if=[[((itemcount(2756) >= 1) and (itemcount(2757) >= 1) and (itemcount(2758) >= 1) and (itemcount(2759) >= 1) and (not completedq(338))) ]]},
		{ids="342", cond_if=[[((itemcount(2749) >= 1) and (itemcount(2750) >= 1) and (itemcount(2751) >= 1) and (not completedq(342))) ]]},
		{ids="583,185,186,190,191,194"},
	},
	["LEVELING\\Stonetalon Mountains (27-28)"] = {
		{ids="6393,6282,6381,6283,1195"},
	},
	["LEVELING\\Arathi Highlands (30-31)"] = {
		{ids="1531", cond_if=[[Shaman ]]},
		{ids="1718,1719", cond_if=[[Warrior ]]},
		{ids="655,672,674,675"},
	},
	["LEVELING\\Ashenvale (22-22)"] = {
		{ids="6542,742,6562,6442"},
		{ids="1528,1530,1535,1536", cond_if=[[Shaman ]]},
	},
}
