## Interface: 11304
## Title: Titan Panel Classic [|cffeda55fLootType|r] |cff00aa001.2.2.11304|r
## Notes: Adds group loot and instance difficulty information to Titan Panel
## Author: Titan Panel Development Team (http://www.titanpanel.org)
## SavedVariables: TitanClassicLootTypeSaved
## OptionalDeps: 
## Dependencies: TitanClassic
## Version: 1.2.2.11304
## X-Child-Of: TitanClassic
TitanClassicLootType.xml
