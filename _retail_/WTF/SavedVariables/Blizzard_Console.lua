
Blizzard_Console_SavedVars = {
	["version"] = 3,
	["messageHistory"] = {
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [1]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [2]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [3]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [4]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [5]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [6]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [7]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [8]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [9]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [10]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [11]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [12]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [13]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [14]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [15]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [16]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [17]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [18]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [19]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [20]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [21]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [22]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [23]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [24]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [25]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [26]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [27]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [28]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [29]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [30]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [31]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [32]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [33]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [34]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [35]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [36]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [37]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [38]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [39]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [40]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [41]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [42]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [43]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [44]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [45]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [46]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [47]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [48]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [49]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [50]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [51]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [52]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 17 entitlements.", -- [1]
			0, -- [2]
		}, -- [53]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [54]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [55]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [56]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [57]
		{
			"Proficiency in item class 2 set to 0x00000080", -- [1]
			0, -- [2]
		}, -- [58]
		{
			"Proficiency in item class 2 set to 0x00000081", -- [1]
			0, -- [2]
		}, -- [59]
		{
			"Proficiency in item class 2 set to 0x00000091", -- [1]
			0, -- [2]
		}, -- [60]
		{
			"Proficiency in item class 2 set to 0x00000191", -- [1]
			0, -- [2]
		}, -- [61]
		{
			"Proficiency in item class 2 set to 0x000001b1", -- [1]
			0, -- [2]
		}, -- [62]
		{
			"Proficiency in item class 2 set to 0x000001b3", -- [1]
			0, -- [2]
		}, -- [63]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [64]
		{
			"Proficiency in item class 2 set to 0x000041b3", -- [1]
			0, -- [2]
		}, -- [65]
		{
			"Proficiency in item class 2 set to 0x000041f3", -- [1]
			0, -- [2]
		}, -- [66]
		{
			"Proficiency in item class 4 set to 0x00000031", -- [1]
			0, -- [2]
		}, -- [67]
		{
			"Proficiency in item class 4 set to 0x00000039", -- [1]
			0, -- [2]
		}, -- [68]
		{
			"Proficiency in item class 4 set to 0x0000003d", -- [1]
			0, -- [2]
		}, -- [69]
		{
			"Proficiency in item class 4 set to 0x0000003f", -- [1]
			0, -- [2]
		}, -- [70]
		{
			"Proficiency in item class 4 set to 0x0000007f", -- [1]
			0, -- [2]
		}, -- [71]
		{
			"Proficiency in item class 2 set to 0x000041f3", -- [1]
			0, -- [2]
		}, -- [72]
		{
			"Proficiency in item class 4 set to 0x0000007f", -- [1]
			0, -- [2]
		}, -- [73]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [74]
		{
			"Time set to 4/26/2021 (Mon) 15:19", -- [1]
			0, -- [2]
		}, -- [75]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [76]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [77]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [78]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [79]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [80]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [81]
		{
			"Skill 124 increased from 75 to 95", -- [1]
			0, -- [2]
		}, -- [82]
		{
			"Skill 183 increased from 75 to 95", -- [1]
			0, -- [2]
		}, -- [83]
		{
			"Skill 800 increased from 75 to 95", -- [1]
			0, -- [2]
		}, -- [84]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [85]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [86]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [87]
		{
			"Skill 124 increased from 95 to 80", -- [1]
			0, -- [2]
		}, -- [88]
		{
			"Skill 183 increased from 95 to 80", -- [1]
			0, -- [2]
		}, -- [89]
		{
			"Skill 800 increased from 95 to 80", -- [1]
			0, -- [2]
		}, -- [90]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [91]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [92]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [93]
		{
			"Skill 124 increased from 80 to 95", -- [1]
			0, -- [2]
		}, -- [94]
		{
			"Skill 183 increased from 80 to 95", -- [1]
			0, -- [2]
		}, -- [95]
		{
			"Skill 800 increased from 80 to 95", -- [1]
			0, -- [2]
		}, -- [96]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [97]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [98]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [99]
		{
			"Skill 124 increased from 95 to 80", -- [1]
			0, -- [2]
		}, -- [100]
		{
			"Skill 183 increased from 95 to 80", -- [1]
			0, -- [2]
		}, -- [101]
		{
			"Skill 800 increased from 95 to 80", -- [1]
			0, -- [2]
		}, -- [102]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [103]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [104]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [105]
		{
			"Skill 124 increased from 80 to 95", -- [1]
			0, -- [2]
		}, -- [106]
		{
			"Skill 183 increased from 80 to 95", -- [1]
			0, -- [2]
		}, -- [107]
		{
			"Skill 800 increased from 80 to 95", -- [1]
			0, -- [2]
		}, -- [108]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [109]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [110]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [111]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [112]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [113]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [114]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [115]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [116]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [117]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [118]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [119]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [120]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [121]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [122]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [123]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [124]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [125]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [126]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [127]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [128]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [129]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [130]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [131]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [132]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [133]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [134]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [135]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [136]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [137]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [138]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [139]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [140]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [141]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [142]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [143]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [144]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [145]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [146]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [147]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [148]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [149]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [150]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [151]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [152]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [153]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [154]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [155]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [156]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [157]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [158]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [159]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [160]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [161]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [162]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [163]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [164]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [165]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [166]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [167]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [168]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [169]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [170]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [171]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [172]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [173]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [174]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [175]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [176]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [177]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [178]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [179]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [180]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [181]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [182]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [183]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [184]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [185]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [186]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [187]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [188]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [189]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [190]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [191]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [192]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [193]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [194]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [195]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [196]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [197]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [198]
		{
			"Proficiency in item class 2 set to 0x00000010", -- [1]
			0, -- [2]
		}, -- [199]
		{
			"Proficiency in item class 2 set to 0x00000410", -- [1]
			0, -- [2]
		}, -- [200]
		{
			"Proficiency in item class 2 set to 0x00008410", -- [1]
			0, -- [2]
		}, -- [201]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [202]
		{
			"Proficiency in item class 2 set to 0x0000c410", -- [1]
			0, -- [2]
		}, -- [203]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [204]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [205]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [206]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [207]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [208]
		{
			"Time set to 4/28/2021 (Wed) 16:50", -- [1]
			0, -- [2]
		}, -- [209]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [210]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [211]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [212]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [213]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [214]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [215]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [216]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [217]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [218]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [219]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [220]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [221]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [222]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [223]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [224]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [225]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [226]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [227]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [228]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [229]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [230]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [231]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [232]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [233]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [234]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [235]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [236]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [237]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [238]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [239]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [240]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [241]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [242]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [243]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [244]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [245]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [246]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [247]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [248]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [249]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [250]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [251]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [252]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [253]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [254]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [255]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [256]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [257]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [258]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [259]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [260]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [261]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [262]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [263]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [264]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [265]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [266]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [267]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [268]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [269]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [270]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [271]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [272]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [273]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [274]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [275]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [276]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [277]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [278]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [279]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [280]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [281]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [282]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [283]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [284]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [285]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [286]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [287]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [288]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [289]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [290]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [291]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [292]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [293]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [294]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [295]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [296]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [297]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [298]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [299]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [300]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [301]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [302]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [303]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [304]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [305]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [306]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [307]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [308]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [309]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [310]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [311]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [312]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [313]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [314]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [315]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [316]
		{
			"Time set to 4/28/2021 (Wed) 18:38", -- [1]
			0, -- [2]
		}, -- [317]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [318]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [319]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [320]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [321]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [322]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [323]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [324]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [325]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [326]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [327]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [328]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [329]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [330]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [331]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [332]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [333]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [334]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [335]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [336]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [337]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [338]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [339]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [340]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [341]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [342]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [343]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [344]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [345]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [346]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [347]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [348]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [349]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [350]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [351]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [352]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [353]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [354]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [355]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [356]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [357]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [358]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [359]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [360]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [361]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [362]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [363]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [364]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [365]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [366]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [367]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [368]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [369]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [370]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [371]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [372]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [373]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [374]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [375]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [376]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [377]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [378]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [379]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [380]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [381]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [382]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [383]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [384]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [385]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [386]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [387]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [388]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [389]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [390]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [391]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [392]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [393]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [394]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [395]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [396]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [397]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [398]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [399]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [400]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [401]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [402]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [403]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [404]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [405]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [406]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [407]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [408]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [409]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [410]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [411]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [412]
		{
			"Proficiency in item class 2 set to 0x00000010", -- [1]
			0, -- [2]
		}, -- [413]
		{
			"Proficiency in item class 2 set to 0x00000410", -- [1]
			0, -- [2]
		}, -- [414]
		{
			"Proficiency in item class 2 set to 0x00008410", -- [1]
			0, -- [2]
		}, -- [415]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [416]
		{
			"Proficiency in item class 2 set to 0x0000c410", -- [1]
			0, -- [2]
		}, -- [417]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [418]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [419]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [420]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [421]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [422]
		{
			"Time set to 4/29/2021 (Thu) 16:46", -- [1]
			0, -- [2]
		}, -- [423]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [424]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [425]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [426]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [427]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [428]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [429]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [430]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [431]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [432]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [433]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [434]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [435]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [436]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [437]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [438]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [439]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [440]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [441]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [442]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [443]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [444]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [445]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [446]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [447]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [448]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [449]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [450]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [451]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [452]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [453]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [454]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [455]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [456]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [457]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [458]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [459]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [460]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [461]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [462]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [463]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [464]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [465]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [466]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [467]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [468]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [469]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [470]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [471]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [472]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [473]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [474]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [475]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [476]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [477]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [478]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [479]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [480]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [481]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [482]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [483]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [484]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [485]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [486]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [487]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [488]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [489]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [490]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [491]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [492]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [493]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [494]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [495]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [496]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [497]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [498]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [499]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [500]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [501]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [502]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [503]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [504]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [505]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [506]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [507]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [508]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [509]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [510]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [511]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [512]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [513]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [514]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [515]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [516]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [517]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [518]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [519]
		{
			"Proficiency in item class 2 set to 0x00000010", -- [1]
			0, -- [2]
		}, -- [520]
		{
			"Proficiency in item class 2 set to 0x00000410", -- [1]
			0, -- [2]
		}, -- [521]
		{
			"Proficiency in item class 2 set to 0x00008410", -- [1]
			0, -- [2]
		}, -- [522]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [523]
		{
			"Proficiency in item class 2 set to 0x0000c410", -- [1]
			0, -- [2]
		}, -- [524]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [525]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [526]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [527]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [528]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [529]
		{
			"Time set to 4/29/2021 (Thu) 16:54", -- [1]
			0, -- [2]
		}, -- [530]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [531]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [532]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [533]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [534]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [535]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [536]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [537]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [538]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [539]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [540]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [541]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [542]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [543]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [544]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [545]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [546]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [547]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [548]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [549]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [550]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [551]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [552]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [553]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [554]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [555]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [556]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [557]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [558]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [559]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [560]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [561]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [562]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [563]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [564]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [565]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [566]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [567]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [568]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [569]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [570]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [571]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [572]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [573]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [574]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [575]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [576]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [577]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [578]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [579]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [580]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [581]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [582]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [583]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [584]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [585]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [586]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [587]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [588]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [589]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [590]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [591]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [592]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [593]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [594]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [595]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [596]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [597]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [598]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [599]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [600]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [601]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [602]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [603]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [604]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [605]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [606]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [607]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [608]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [609]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [610]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [611]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [612]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [613]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [614]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [615]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [616]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [617]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [618]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [619]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [620]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [621]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [622]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [623]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [624]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [625]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [626]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [627]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [628]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [629]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [630]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [631]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [632]
		{
			"Time set to 4/29/2021 (Thu) 18:28", -- [1]
			0, -- [2]
		}, -- [633]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [634]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [635]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [636]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [637]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [638]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [639]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [640]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [641]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [642]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [643]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [644]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [645]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [646]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [647]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [648]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [649]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [650]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [651]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [652]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [653]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [654]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [655]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [656]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [657]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [658]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [659]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [660]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [661]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [662]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [663]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [664]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [665]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [666]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [667]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [668]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [669]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [670]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [671]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [672]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [673]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [674]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [675]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [676]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [677]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [678]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [679]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [680]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [681]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [682]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [683]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [684]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [685]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [686]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [687]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [688]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [689]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [690]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [691]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [692]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [693]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [694]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [695]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [696]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [697]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [698]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [699]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [700]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [701]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [702]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [703]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [704]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [705]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [706]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [707]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [708]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [709]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [710]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [711]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [712]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [713]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [714]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [715]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [716]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [717]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [718]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [719]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [720]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [721]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [722]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [723]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [724]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [725]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [726]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [727]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [728]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [729]
		{
			"Proficiency in item class 2 set to 0x00000010", -- [1]
			0, -- [2]
		}, -- [730]
		{
			"Proficiency in item class 2 set to 0x00000410", -- [1]
			0, -- [2]
		}, -- [731]
		{
			"Proficiency in item class 2 set to 0x00008410", -- [1]
			0, -- [2]
		}, -- [732]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [733]
		{
			"Proficiency in item class 2 set to 0x0000c410", -- [1]
			0, -- [2]
		}, -- [734]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [735]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [736]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [737]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [738]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [739]
		{
			"Time set to 5/3/2021 (Mon) 14:51", -- [1]
			0, -- [2]
		}, -- [740]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [741]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [742]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [743]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [744]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [745]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [746]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [747]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [748]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [749]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [750]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [751]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [752]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [753]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [754]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [755]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [756]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [757]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [758]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [759]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [760]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [761]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [762]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [763]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [764]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [765]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [766]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [767]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [768]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [769]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [770]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [771]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [772]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [773]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [774]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [775]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [776]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [777]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [778]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [779]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [780]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [781]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [782]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [783]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [784]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [785]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [786]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [787]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [788]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [789]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [790]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [791]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [792]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [793]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [794]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [795]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [796]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [797]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [798]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [799]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [800]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [801]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [802]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [803]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [804]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [805]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [806]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [807]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [808]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [809]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [810]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [811]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [812]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [813]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [814]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [815]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [816]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [817]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [818]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [819]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [820]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [821]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [822]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [823]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [824]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [825]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [826]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [827]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [828]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [829]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [830]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [831]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [832]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [833]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [834]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [835]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [836]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [837]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [838]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [839]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [840]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [841]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [842]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [843]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [844]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [845]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [846]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [847]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [848]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [849]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [850]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [851]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [852]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [853]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [854]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [855]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [856]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [857]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [858]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [859]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [860]
		{
			"Proficiency in item class 2 set to 0x00000010", -- [1]
			0, -- [2]
		}, -- [861]
		{
			"Proficiency in item class 2 set to 0x00000410", -- [1]
			0, -- [2]
		}, -- [862]
		{
			"Proficiency in item class 2 set to 0x00008410", -- [1]
			0, -- [2]
		}, -- [863]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [864]
		{
			"Proficiency in item class 2 set to 0x0000c410", -- [1]
			0, -- [2]
		}, -- [865]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [866]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [867]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [868]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [869]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [870]
		{
			"Time set to 5/5/2021 (Wed) 16:48", -- [1]
			0, -- [2]
		}, -- [871]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [872]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [873]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [874]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [875]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [876]
		{
			"Weather changed to 2, intensity 0.920998\n", -- [1]
			0, -- [2]
		}, -- [877]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [878]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [879]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [880]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [881]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [882]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [883]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [884]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [885]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [886]
		{
			"Multithreaded rendering enabled.", -- [1]
			0, -- [2]
		}, -- [887]
		{
			"Multithreaded BeginDraw enabled.", -- [1]
			0, -- [2]
		}, -- [888]
		{
			"Multithread shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [889]
		{
			"Multithreaded prepass enabled.", -- [1]
			0, -- [2]
		}, -- [890]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [891]
		{
			"Multithreaded opaque pass enabled.", -- [1]
			0, -- [2]
		}, -- [892]
		{
			"Multithreaded alpha M2 pass enabled.", -- [1]
			0, -- [2]
		}, -- [893]
		{
			"Multithreaded opaque WMO pass enabled.", -- [1]
			0, -- [2]
		}, -- [894]
		{
			"Multithreaded terrain pass enabled.", -- [1]
			0, -- [2]
		}, -- [895]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [896]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [897]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [898]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [899]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [900]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [901]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [902]
		{
			"Volume fog enabled.", -- [1]
			0, -- [2]
		}, -- [903]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [904]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [905]
		{
			"Spell Clutter set to dynamic", -- [1]
			0, -- [2]
		}, -- [906]
		{
			"Shadow mode changed to 4 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [907]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [908]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [909]
		{
			"Shadow RT mode changed to 0 (Disabled)", -- [1]
			0, -- [2]
		}, -- [910]
		{
			"maxLightCount must be in range 0 to 32.", -- [1]
			0, -- [2]
		}, -- [911]
		{
			"CVar 'maxLightCount' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [912]
		{
			"SSAO mode set to 4", -- [1]
			0, -- [2]
		}, -- [913]
		{
			"SSAO type set to 0", -- [1]
			0, -- [2]
		}, -- [914]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [915]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [916]
		{
			"Texture filtering mode updated.", -- [1]
			0, -- [2]
		}, -- [917]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [918]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [919]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [920]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [921]
		{
			"Resample quality changed to 1", -- [1]
			0, -- [2]
		}, -- [922]
		{
			"MSAA set to 4 color samples, 4 coverage samples", -- [1]
			0, -- [2]
		}, -- [923]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [924]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [925]
		{
			"CVar 'vrsWorldGeo' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [926]
		{
			"Variable Rate Shading not supported on this hardware", -- [1]
			0, -- [2]
		}, -- [927]
		{
			"CVar 'vrsParticles' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [928]
		{
			"Particulate volumes enabled.", -- [1]
			0, -- [2]
		}, -- [929]
		{
			"dynamicLod enabled", -- [1]
			0, -- [2]
		}, -- [930]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [931]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [932]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [933]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [934]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [935]
		{
			"Full memory crash dump disabled", -- [1]
			0, -- [2]
		}, -- [936]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [937]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [938]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [939]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [940]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [941]
		{
			"CVar 'Sound_AmbienceHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [942]
		{
			"CVar 'Sound_AllyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [943]
		{
			"CVar 'Sound_EnemyPlayerHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [944]
		{
			"CVar 'Sound_NPCHighpassDSPCutoff' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [945]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"us.actual.battle.net\" loginPortal=\"us.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [946]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [947]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [948]
		{
			"[IBN_Login] Attempting logonhost=\"us.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [949]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [950]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [951]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [952]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [953]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [954]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [955]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [956]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [957]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [958]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"5\"", -- [1]
			0, -- [2]
		}, -- [959]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [960]
		{
			"[IBN_Login] Joining realmsubRegion=\"1-1-89\" realmAddress=\"1-4-26\"", -- [1]
			0, -- [2]
		}, -- [961]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [962]
		{
			"NetClient::HandleConnect()\n", -- [1]
			0, -- [2]
		}, -- [963]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [964]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [965]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [966]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [967]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [968]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_NETWORK_MODULE_SOCKET_CLOSED (1016)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [969]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [970]
		{
			"[WowEntitlements] [BNetAccount-0-00000004BEF0] [WowAccount-0-0000006E9E0F] Initialized with 18 entitlements.", -- [1]
			0, -- [2]
		}, -- [971]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [972]
		{
			"Game table failed consistency check: ChallengeModeHealth. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [973]
		{
			"Game table failed consistency check: ChallengeModeDamage. rows: 50, expectedRows: 51, columns: 1, expectedColumns: 1\n", -- [1]
			3, -- [2]
		}, -- [974]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [975]
		{
			"Proficiency in item class 2 set to 0x00000010", -- [1]
			0, -- [2]
		}, -- [976]
		{
			"Proficiency in item class 2 set to 0x00000410", -- [1]
			0, -- [2]
		}, -- [977]
		{
			"Proficiency in item class 2 set to 0x00008410", -- [1]
			0, -- [2]
		}, -- [978]
		{
			"Proficiency in item class 4 set to 0x00000021", -- [1]
			0, -- [2]
		}, -- [979]
		{
			"Proficiency in item class 2 set to 0x0000c410", -- [1]
			0, -- [2]
		}, -- [980]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [981]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [982]
		{
			"Proficiency in item class 2 set to 0x0008c410", -- [1]
			0, -- [2]
		}, -- [983]
		{
			"Proficiency in item class 4 set to 0x00000023", -- [1]
			0, -- [2]
		}, -- [984]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [985]
		{
			"Time set to 5/6/2021 (Thu) 16:46", -- [1]
			0, -- [2]
		}, -- [986]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [987]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [988]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [989]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [990]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [991]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [992]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [993]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [994]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [995]
		{
			"GameTimeSync: delta=0, differential=1, HoursAndMinutes=1066", -- [1]
			0, -- [2]
		}, -- [996]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [997]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [998]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [999]
		{
			"Weather changed to 4, intensity 1.000000\n", -- [1]
			0, -- [2]
		}, -- [1000]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [1001]
	},
	["height"] = 299.9999694824219,
	["fontHeight"] = 14,
	["isShown"] = false,
	["commandHistory"] = {
		"ExportInterfaceFiles code", -- [1]
		"ExportInterfaceFiles art", -- [2]
		"ExportInterfaceFiles code", -- [3]
	},
}
