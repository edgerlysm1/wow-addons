
PawnOptions = {
	["LastVersion"] = 2.0405,
	["ItemLevels"] = {
		{
			["ID"] = 38661,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38661::::::::18:251::75:1:6691:1:9:10:::|h[Greathelm of the Scourge Champion]|h|r",
		}, -- [1]
		{
			["ID"] = 38662,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38662::::::::18:251::75:1:6691:1:9:10:::|h[Bladed Ebon Amulet]|h|r",
		}, -- [2]
		{
			["ID"] = 38663,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38663::::::::18:251::75:1:6691:1:9:10:::|h[Blood-Soaked Saronite Plated Spaulders]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 38665,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38665::::::::18:251::75:1:6691:1:9:10:::|h[Saronite War Plate]|h|r",
		}, -- [5]
		{
			["ID"] = 158284,
			["Level"] = 22,
			["Link"] = "|cffa335ee|Hitem:158284::::::::18:251::11:2:4790:4792:2:9:15:28:186:::|h[Sea Raider's Greatbelt]|h|r",
		}, -- [6]
		{
			["ID"] = 38669,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38669::::::::18:251::75:1:6691:1:9:10:::|h[Engraved Saronite Legplates]|h|r",
		}, -- [7]
		{
			["ID"] = 38670,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38670::::::::18:251::75:1:6691:1:9:10:::|h[Greaves of the Slaughter]|h|r",
		}, -- [8]
		{
			["ID"] = 38666,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38666::::::::18:251::75:1:6691:1:9:10:::|h[Plated Saronite Bracers]|h|r",
		}, -- [9]
		{
			["ID"] = 38667,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38667::::::::18:251::75:1:6691:1:9:10:::|h[Bloodbane's Gauntlets of Command]|h|r",
		}, -- [10]
		{
			["ID"] = 160261,
			["Level"] = 22,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffa335ee|Hitem:160261::::::::18:251::11:2:4811:4815:2:9:15:28:186:::|h[Soul of the Sea]|h|r",
		}, -- [11]
		{
			["ID"] = 160261,
			["Level"] = 22,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:160261::::::::19:251::11:2:4811:4815:2:9:15:28:186:::|h[Soul of the Sea]|h|r",
		}, -- [12]
		{
			["ID"] = 38674,
			["Level"] = 14,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:38674::::::::18:251::75:1:6691:1:9:10:::|h[Soul Harvester's Charm]|h|r",
		}, -- [13]
		{
			["ID"] = 38675,
			["Level"] = 14,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff0070dd|Hitem:38675::::::::18:251::75:1:6691:1:9:10:::|h[Signet of the Dark Brotherhood]|h|r",
		}, -- [14]
		{
			["ID"] = 38664,
			["Level"] = 14,
			["Link"] = "|cff0070dd|Hitem:38664::::::::18:251::75:1:6691:1:9:10:::|h[Sky Darkener's Shroud of the Unholy]|h|r",
		}, -- [15]
		{
			["ID"] = 122365,
			["Level"] = 23,
			["Link"] = "|cff00ccff|Hitem:122365::::::::19:251:::1:5805::::|h[Reforged Truesilver Champion]|h|r",
		}, -- [16]
	},
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Thornoxnun-Sargeras",
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DEATHKNIGHT",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
