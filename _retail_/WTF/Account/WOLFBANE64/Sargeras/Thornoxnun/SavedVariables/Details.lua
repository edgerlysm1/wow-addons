
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 47,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007999,
							["spec"] = 251,
							["damage_from"] = {
							},
							["targets"] = {
								["Bleeding Hollow Hatchet"] = 265,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["end_time"] = 1605671437,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 265.007999,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["total"] = 265.007999,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 50,
										["targets"] = {
											["Bleeding Hollow Hatchet"] = 99,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 99,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 99,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Bleeding Hollow Hatchet"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 146,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Bleeding Hollow Hatchet"] = 146,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 146,
										["c_max"] = 146,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 146,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 70.74426027764486,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.007999,
							["start_time"] = 1605671434,
							["delay"] = 0,
							["last_event"] = 1605671437,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 8.006956,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Jïmmÿ"] = true,
								["Kokiria-Turalyon"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Jïmmÿ"] = 76,
								["Kokiria-Turalyon"] = 31,
							},
							["pets"] = {
							},
							["timeMachine"] = 120,
							["fight_component"] = true,
							["aID"] = "78510",
							["raid_targets"] = {
							},
							["total_without_pet"] = 107.006956,
							["monster"] = true,
							["dps_started"] = true,
							["total"] = 107.006956,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Bleeding Hollow Hatchet",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Jïmmÿ"] = 57,
											["Kokiria-Turalyon"] = 13,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 70,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 7,
										["DODGE"] = 1,
										["total"] = 70,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 13,
										["m_crit"] = 0,
										["a_amt"] = 2,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[163598] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Jïmmÿ"] = 19,
											["Kokiria-Turalyon"] = 18,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 37,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 37,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 163598,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 18,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78510-0000349A05",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671449,
							["damage_taken"] = 525.006956,
							["start_time"] = 1605671434,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 47,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 47,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 30.006576,
							["resource"] = 0.006576,
							["targets"] = {
								["Thornoxnun"] = 30,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.006576,
							["total"] = 30.006576,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49184] = {
										["total"] = 10,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006576,
							["last_event"] = 1605671436,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.006576,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 47,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 6,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 2,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49020] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671437,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 47,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49844.605,
				["tempo_start"] = 1605671434,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 69,
				["playing_solo"] = true,
				["totals"] = {
					371.9731689999999, -- [1]
					-0.01003599999999949, -- [2]
					{
						-0.008833, -- [1]
						[0] = -0.002164,
						["alternatepower"] = 0,
						[3] = -0.007567,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					265, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:50:38",
				["cleu_timeline"] = {
				},
				["enemy"] = "Bleeding Hollow Hatchet",
				["TotalElapsedCombatTime"] = 3.745999999999185,
				["CombatEndedAt"] = 49848.351,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 265.007999,
						}, -- [1]
					},
				},
				["end_time"] = 49848.351,
				["combat_id"] = 47,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:50:34",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Bleeding Hollow Hatchet"] = 1,
				},
				["start_time"] = 49844.605,
				["TimeData"] = {
				},
				["contra"] = "Bleeding Hollow Hatchet",
			}, -- [1]
			{
				{
					["combatId"] = 46,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008681,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 430,
							},
							["pets"] = {
							},
							["spec"] = 251,
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 430.008681,
							["end_time"] = 1605671429,
							["colocacao"] = 1,
							["dps_started"] = false,
							["total"] = 430.008681,
							["classe"] = "DEATHKNIGHT",
							["aID"] = "76-0B2B0B98",
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 49,
										["targets"] = {
											["Bleeding Hollow Savage"] = 97,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 97,
										["n_min"] = 48,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 97,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Bleeding Hollow Savage"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Bleeding Hollow Savage"] = 72,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 72,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 72,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325464] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 140,
										["g_amt"] = 0,
										["n_max"] = 71,
										["targets"] = {
											["Bleeding Hollow Savage"] = 211,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 71,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 211,
										["c_max"] = 140,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 140,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Bleeding Hollow Savage"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 30,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["on_hold"] = false,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 55.76561807810144,
							["custom"] = 0,
							["last_event"] = 1605671428,
							["damage_taken"] = 63.008681,
							["start_time"] = 1605671424,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.007483,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Palios-Azshara"] = true,
								["Zexu"] = true,
								["Bleeding Hollow Savage"] = true,
								["Rhodana-Terokkar"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Rezêc-Dalaran"] = 19,
								["Bleeding Hollow Savage"] = 48,
								["Zexu"] = 56,
								["Thornoxnun"] = 63,
							},
							["end_time"] = 1605671434,
							["pets"] = {
							},
							["fight_component"] = true,
							["aID"] = "78507",
							["raid_targets"] = {
							},
							["total_without_pet"] = 186.007483,
							["monster"] = true,
							["dps_started"] = false,
							["total"] = 186.007483,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Bleeding Hollow Savage",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Rezêc-Dalaran"] = 19,
											["Zexu"] = 56,
											["Thornoxnun"] = 63,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 138,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 138,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 8,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[163586] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Bleeding Hollow Savage"] = 48,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 48,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 48,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 163586,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-0000B499FB",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671430,
							["damage_taken"] = 985.007483,
							["start_time"] = 1605671421,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 46,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-0B2B0B98",
							["totalover"] = 0.007088,
							["total_without_pet"] = 1.007088,
							["total"] = 1.007088,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0B2B0B98",
							["totalabsorb"] = 0.007088,
							["last_hps"] = 0,
							["targets"] = {
								["Thornoxnun"] = 1,
							},
							["totalover_without_pet"] = 0.007088,
							["healing_taken"] = 1.007088,
							["fight_component"] = true,
							["end_time"] = 1605671429,
							["healing_from"] = {
								["Thornoxnun"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[291843] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 1,
										["targets"] = {
											["Thornoxnun"] = 1,
										},
										["n_min"] = 1,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 1,
										["c_max"] = 0,
										["id"] = 291843,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 1,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1605671427,
							["spec"] = 251,
							["custom"] = 0,
							["tipo"] = 2,
							["classe"] = "DEATHKNIGHT",
							["totaldenied"] = 0.007088,
							["delay"] = 0,
							["last_event"] = 1605671427,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 46,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 30.006901,
							["resource"] = 0.006901,
							["targets"] = {
								["Thornoxnun"] = 30,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.006901,
							["fight_component"] = true,
							["total"] = 30.006901,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49184] = {
										["total"] = 10,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006901,
							["last_event"] = 1605671427,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.006901,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 46,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 25,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[291843] = {
										["activedamt"] = 1,
										["id"] = 291843,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 2,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 2,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 4,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49143] = 2,
								[49020] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671429,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Bleeding Hollow Savage",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[163586] = 2,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-00003499FB",
							["aID"] = "78507",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 46,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1605671421,
				["enemy"] = "Bleeding Hollow Savage",
				["combat_counter"] = 68,
				["playing_solo"] = true,
				["totals"] = {
					615.9692510000001, -- [1]
					0.997368999999992, -- [2]
					{
						-0.001087, -- [1]
						[0] = -0.003955,
						["alternatepower"] = 0,
						[3] = -0.001255999999997925,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 49839.748,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:50:22",
				["end_time"] = 49839.748,
				["totals_grupo"] = {
					430, -- [1]
					1, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 46,
				["TotalElapsedCombatTime"] = 49839.748,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thornoxnun"] = 1.007088,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 430.008681,
						}, -- [1]
					},
				},
				["frags"] = {
					["Bleeding Hollow Savage"] = 3,
				},
				["data_fim"] = "22:50:29",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 49832.037,
				["contra"] = "Bleeding Hollow Savage",
				["TimeData"] = {
				},
			}, -- [2]
			{
				{
					["combatId"] = 45,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00332,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 435,
							},
							["pets"] = {
							},
							["total"] = 435.00332,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 435.00332,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671419,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 52,
										["targets"] = {
											["Bleeding Hollow Savage"] = 100,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 100,
										["n_min"] = 48,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 100,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Bleeding Hollow Savage"] = 19,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 19,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 19,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Bleeding Hollow Savage"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 30,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 144,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Bleeding Hollow Savage"] = 216,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 72,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 216,
										["c_max"] = 144,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 144,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Bleeding Hollow Savage"] = 70,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 70,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 70,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 69.91374477658151,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 59.00332,
							["start_time"] = 1605671413,
							["delay"] = 0,
							["last_event"] = 1605671418,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.001854,
							["total"] = 110.001854,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
								["Palios-Azshara"] = true,
								["Zexu"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 8,
								["Palios-Azshara"] = 10,
								["Zexu"] = 33,
								["Thornoxnun"] = 59,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78507",
							["raid_targets"] = {
							},
							["total_without_pet"] = 110.001854,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671421,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Bleeding Hollow Savage",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Palios-Azshara"] = 10,
											["Zexu"] = 33,
											["Thornoxnun"] = 59,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 102,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 102,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[163586] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Bleeding Hollow Savage"] = 8,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 8,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 8,
										["c_max"] = 0,
										["id"] = 163586,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78507-00003499F0",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671420,
							["damage_taken"] = 1318.001854,
							["start_time"] = 1605671413,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 45,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 45,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 30.003142,
							["resource"] = 0.003142,
							["targets"] = {
								["Thornoxnun"] = 30,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.003142,
							["total"] = 30.003142,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49184] = {
										["total"] = 10,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003142,
							["last_event"] = 1605671418,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.003142,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 45,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 15,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 4,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49143] = 1,
								[49020] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671419,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Bleeding Hollow Savage",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[163586] = 2,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-00003499F0",
							["aID"] = "78507",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 45,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49831.213,
				["tempo_start"] = 1605671413,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 67,
				["playing_solo"] = true,
				["totals"] = {
					544.9872359999999, -- [1]
					-0.01155699999999627, -- [2]
					{
						-0.004591, -- [1]
						[0] = -0.002296,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					435, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:50:20",
				["cleu_timeline"] = {
				},
				["enemy"] = "Bleeding Hollow Savage",
				["TotalElapsedCombatTime"] = 49830.027,
				["CombatEndedAt"] = 49830.027,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 435.00332,
						}, -- [1]
					},
				},
				["end_time"] = 49830.027,
				["combat_id"] = 45,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:50:14",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Bleeding Hollow Savage"] = 3,
				},
				["start_time"] = 49823.805,
				["TimeData"] = {
				},
				["contra"] = "Bleeding Hollow Savage",
			}, -- [3]
			{
				{
					["combatId"] = 44,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004309,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 420,
							},
							["pets"] = {
							},
							["total"] = 420.004309,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 420.004309,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671408,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 52,
										["targets"] = {
											["Bleeding Hollow Savage"] = 152,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 152,
										["n_min"] = 48,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 152,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Bleeding Hollow Savage"] = 70,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 70,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 70,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[55095] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 60,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Bleeding Hollow Savage"] = 90,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 90,
										["c_max"] = 60,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 60,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Bleeding Hollow Savage"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[196771] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Bleeding Hollow Savage"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 16,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 196771,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Bleeding Hollow Savage"] = 72,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 72,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 72,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 41.02405831215099,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 90.004309,
							["start_time"] = 1605671398,
							["delay"] = 0,
							["last_event"] = 1605671408,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.003376,
							["total"] = 213.003376,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
								["Zexu"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 24,
								["Zexu"] = 99,
								["Thornoxnun"] = 90,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78507",
							["raid_targets"] = {
							},
							["total_without_pet"] = 213.003376,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671413,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Bleeding Hollow Savage",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Zexu"] = 99,
											["Thornoxnun"] = 90,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 189,
										["n_min"] = 13,
										["g_dmg"] = 0,
										["counter"] = 13,
										["DODGE"] = 1,
										["total"] = 189,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 12,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[163586] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Bleeding Hollow Savage"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = 163586,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78507-00003499E3",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671412,
							["damage_taken"] = 1036.003376,
							["start_time"] = 1605671398,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 44,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 44,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 50.006247,
							["resource"] = 0.006247,
							["targets"] = {
								["Thornoxnun"] = 50,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.006247,
							["total"] = 50.006247,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 40,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 40,
										},
										["counter"] = 2,
									},
									[49184] = {
										["total"] = 10,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006247,
							["last_event"] = 1605671413,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.006247,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 44,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[211793] = {
										["activedamt"] = 0,
										["id"] = 211793,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 30,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 2,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[196770] = {
										["activedamt"] = 1,
										["id"] = 196770,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[291843] = {
										["activedamt"] = 1,
										["id"] = 291843,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 10,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49020] = 1,
								[49143] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671408,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Bleeding Hollow Savage",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[163586] = 2,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-00003499E3",
							["aID"] = "78507",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 44,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49820.265,
				["tempo_start"] = 1605671398,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 66,
				["playing_solo"] = true,
				["totals"] = {
					632.980566, -- [1]
					-0.004115999999996234, -- [2]
					{
						-0.002862, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 50,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					420, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 50,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:50:09",
				["cleu_timeline"] = {
				},
				["enemy"] = "Bleeding Hollow Savage",
				["TotalElapsedCombatTime"] = 49819.425,
				["CombatEndedAt"] = 49819.425,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 420.004309,
						}, -- [1]
					},
				},
				["end_time"] = 49819.425,
				["combat_id"] = 44,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:49:59",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Bleeding Hollow Savage"] = 2,
				},
				["start_time"] = 49809.187,
				["TimeData"] = {
				},
				["contra"] = "Bleeding Hollow Savage",
			}, -- [4]
			{
				{
					["combatId"] = 43,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001627,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 441,
							},
							["pets"] = {
							},
							["total"] = 441.001627,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 441.001627,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671396,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 52,
										["targets"] = {
											["Bleeding Hollow Savage"] = 101,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 101,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 101,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Bleeding Hollow Savage"] = 70,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 70,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 70,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Bleeding Hollow Savage"] = 60,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 60,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 60,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Bleeding Hollow Savage"] = 19,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 19,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 19,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[196771] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 16,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Bleeding Hollow Savage"] = 48,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 32,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 48,
										["c_max"] = 16,
										["spellschool"] = 16,
										["id"] = 196771,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 16,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Bleeding Hollow Savage"] = 143,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 143,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 143,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 58.62824076040112,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 29.001627,
							["start_time"] = 1605671388,
							["delay"] = 0,
							["last_event"] = 1605671395,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.00453,
							["total"] = 93.00453,
							["damage_from"] = {
								["Zexu"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Zexu"] = 64,
								["Thornoxnun"] = 29,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78507",
							["raid_targets"] = {
							},
							["total_without_pet"] = 93.00453,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671396,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Bleeding Hollow Savage",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 29,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Zexu"] = 64,
											["Thornoxnun"] = 29,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 64,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 6,
										["DODGE"] = 1,
										["total"] = 93,
										["c_max"] = 29,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 29,
									}, -- [1]
									[163586] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 163586,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78507-000034988E",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671395,
							["damage_taken"] = 824.0045299999999,
							["start_time"] = 1605671388,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 43,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-0B2B0B98",
							["totalover"] = 0.008137,
							["total_without_pet"] = 2.008137,
							["total"] = 2.008137,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0B2B0B98",
							["totalabsorb"] = 0.008137,
							["last_hps"] = 0,
							["targets"] = {
								["Thornoxnun"] = 2,
							},
							["totalover_without_pet"] = 0.008137,
							["healing_taken"] = 2.008137,
							["fight_component"] = true,
							["end_time"] = 1605671396,
							["healing_from"] = {
								["Thornoxnun"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[291843] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 1,
										["targets"] = {
											["Thornoxnun"] = 2,
										},
										["n_min"] = 1,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 2,
										["c_max"] = 0,
										["id"] = 291843,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 2,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1605671393,
							["spec"] = 251,
							["custom"] = 0,
							["tipo"] = 2,
							["classe"] = "DEATHKNIGHT",
							["totaldenied"] = 0.008137,
							["delay"] = 0,
							["last_event"] = 1605671395,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 43,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 30.00466,
							["resource"] = 0.00466,
							["targets"] = {
								["Thornoxnun"] = 30,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.00466,
							["fight_component"] = true,
							["total"] = 30.00466,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[196770] = {
										["total"] = 10,
										["id"] = 196770,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00466,
							["last_event"] = 1605671393,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.00466,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 43,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[211793] = {
										["activedamt"] = 0,
										["id"] = 211793,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[55095] = {
										["activedamt"] = -1,
										["id"] = 55095,
										["targets"] = {
										},
										["actived_at"] = 1605671395,
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 27,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[291843] = {
										["activedamt"] = 1,
										["id"] = 291843,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[196770] = {
										["activedamt"] = 1,
										["id"] = 196770,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 4,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[196770] = 1,
								[49020] = 1,
								[49143] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671396,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Bleeding Hollow Savage",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[163586] = 2,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-000034993A",
							["aID"] = "78507",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 43,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49807.586,
				["tempo_start"] = 1605671388,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 65,
				["playing_solo"] = true,
				["totals"] = {
					533.976299, -- [1]
					1.995146000000005, -- [2]
					{
						-0.005473, -- [1]
						[0] = -0.003529,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					441, -- [1]
					2, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:49:57",
				["cleu_timeline"] = {
				},
				["enemy"] = "Bleeding Hollow Savage",
				["TotalElapsedCombatTime"] = 7.522000000004482,
				["CombatEndedAt"] = 49806.8,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thornoxnun"] = 2.008137,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 441.001627,
						}, -- [1]
					},
				},
				["end_time"] = 49806.8,
				["combat_id"] = 43,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:49:49",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Bleeding Hollow Savage"] = 2,
				},
				["start_time"] = 49799.278,
				["TimeData"] = {
				},
				["contra"] = "Bleeding Hollow Savage",
			}, -- [5]
			{
				{
					["combatId"] = 42,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006223,
							["damage_from"] = {
								["Bleeding Hollow Savage"] = true,
							},
							["targets"] = {
								["Bleeding Hollow Savage"] = 438,
							},
							["pets"] = {
							},
							["total"] = 438.006223,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 438.006223,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671382,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 105,
										["g_amt"] = 0,
										["n_max"] = 49,
										["targets"] = {
											["Bleeding Hollow Savage"] = 154,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 49,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 154,
										["c_max"] = 105,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 105,
									}, -- [1]
									[49184] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 41,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Bleeding Hollow Savage"] = 41,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 41,
										["c_max"] = 41,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 41,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Bleeding Hollow Savage"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 30,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 73,
										["targets"] = {
											["Bleeding Hollow Savage"] = 143,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 143,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 143,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Bleeding Hollow Savage"] = 70,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 70,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 70,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 77.0866284758793,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 15.006223,
							["start_time"] = 1605671376,
							["delay"] = 0,
							["last_event"] = 1605671381,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.005334,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Zexu"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Zexu"] = 32,
								["Thornoxnun"] = 15,
							},
							["fight_component"] = true,
							["pets"] = {
							},
							["end_time"] = 1605671388,
							["aID"] = "78507",
							["raid_targets"] = {
							},
							["total_without_pet"] = 47.005334,
							["monster"] = true,
							["dps_started"] = false,
							["total"] = 47.005334,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Bleeding Hollow Savage",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Zexu"] = 32,
											["Thornoxnun"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 47,
										["DODGE"] = 1,
										["n_min"] = 15,
										["g_dmg"] = 0,
										["counter"] = 5,
										["MISS"] = 1,
										["total"] = 47,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[163586] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 163586,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-00003499CC",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671382,
							["damage_taken"] = 875.005334,
							["start_time"] = 1605671379,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 42,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 42,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 60.002026,
							["resource"] = 0.002026,
							["targets"] = {
								["Thornoxnun"] = 60,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.002026,
							["total"] = 60.002026,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 40,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 40,
										},
										["counter"] = 2,
									},
									[49184] = {
										["total"] = 20,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 2,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002026,
							["last_event"] = 1605671388,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.002026,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 42,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 8,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 3,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49020] = 2,
								[49143] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671382,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Bleeding Hollow Savage",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[163586] = 2,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78507-00003499CC",
							["aID"] = "78507",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 42,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1605671376,
				["enemy"] = "Bleeding Hollow Savage",
				["combat_counter"] = 64,
				["playing_solo"] = true,
				["totals"] = {
					484.998753, -- [1]
					-0.006448000000006005, -- [2]
					{
						-0.001021, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 60,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 49792.929,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:49:37",
				["end_time"] = 49792.929,
				["totals_grupo"] = {
					438, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 60,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 42,
				["TotalElapsedCombatTime"] = 49792.929,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 438.006223,
						}, -- [1]
					},
				},
				["frags"] = {
					["Bleeding Hollow Savage"] = 2,
				},
				["data_fim"] = "22:49:43",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 49787.247,
				["contra"] = "Bleeding Hollow Savage",
				["TimeData"] = {
				},
			}, -- [6]
			{
				{
					["combatId"] = 41,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00195,
							["damage_from"] = {
							},
							["targets"] = {
								["Tormented Soul"] = 197,
								["Iron Grunt"] = 30,
							},
							["pets"] = {
							},
							["total"] = 227.00195,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 227.00195,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671254,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 53,
										["targets"] = {
											["Tormented Soul"] = 53,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 53,
										["n_min"] = 53,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 53,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Iron Grunt"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 30,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 144,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Tormented Soul"] = 144,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 144,
										["c_max"] = 144,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 144,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 63.49704895110065,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.00195,
							["start_time"] = 1605671251,
							["delay"] = 0,
							["last_event"] = 1605671253,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.005518,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Racy"] = true,
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
								["Thrend"] = true,
								["Mumper"] = true,
								["Etubrute"] = true,
								["Caldia-Stormrage"] = true,
								["Agios Lumen"] = true,
								["Bonesaw"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 160,
								["Plainsmender Darragh"] = 666,
								["Barbery the Crazy Cat Lady"] = 109,
								["Racy"] = 401,
								["Corselle"] = 239,
								["Starcaller Astrylian"] = 196,
								["Bonesaw"] = 455,
								["Ryii the Shameless"] = 71,
								["Arnold Croman"] = 202,
								["High Warlord Shoju"] = 383,
								["Maelgwyn"] = 669,
								["Qiana Moonshadow"] = 88,
								["Lupas"] = 61,
								["Olin Umberhide"] = 123,
								["Kengtus Pranch the Patient"] = 273,
								["Johnny Oshimo"] = 445,
								["Turkina"] = 62,
								["Moonalli"] = 527,
								["Durphorn the Bullheaded"] = 139,
								["Malothas"] = 75,
								["Tore"] = 62,
								["Yoori"] = 158,
								["Moriccalas"] = 197,
								["Etubrute"] = 500,
								["Mumper"] = 300,
								["Salty Futz"] = 190,
								["Pazo Stonehoof"] = 366,
								["Nevo"] = 169,
								["Miserain Starsorrow"] = 436,
								["Agios Lumen"] = 245,
								["Ariok"] = 157,
								["Northpaul"] = 278,
							},
							["fight_component"] = true,
							["pets"] = {
							},
							["end_time"] = 1605671376,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 8402.005518,
							["monster"] = true,
							["dps_started"] = false,
							["total"] = 8402.005518,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 48,
										["b_amt"] = 6,
										["c_dmg"] = 1161,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Challe Tebrilinde"] = 160,
											["Plainsmender Darragh"] = 666,
											["Barbery the Crazy Cat Lady"] = 109,
											["Racy"] = 401,
											["Corselle"] = 239,
											["Starcaller Astrylian"] = 196,
											["Bonesaw"] = 455,
											["Ryii the Shameless"] = 71,
											["Arnold Croman"] = 202,
											["Miserain Starsorrow"] = 436,
											["Maelgwyn"] = 669,
											["Qiana Moonshadow"] = 88,
											["Lupas"] = 61,
											["Olin Umberhide"] = 123,
											["Kengtus Pranch the Patient"] = 273,
											["Johnny Oshimo"] = 445,
											["Turkina"] = 62,
											["Moonalli"] = 527,
											["Durphorn the Bullheaded"] = 139,
											["Malothas"] = 75,
											["Tore"] = 62,
											["Yoori"] = 158,
											["Moriccalas"] = 197,
											["Etubrute"] = 500,
											["Mumper"] = 300,
											["Salty Futz"] = 190,
											["Pazo Stonehoof"] = 366,
											["Nevo"] = 169,
											["High Warlord Shoju"] = 383,
											["Agios Lumen"] = 245,
											["Ariok"] = 157,
											["Northpaul"] = 278,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7241,
										["a_amt"] = 0,
										["MISS"] = 22,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 714,
										["DODGE"] = 13,
										["total"] = 8402,
										["c_max"] = 42,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 17,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 50,
										["n_amt"] = 614,
										["r_amt"] = 0,
										["c_min"] = 17,
									}, -- [1]
								},
							},
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-0000347CE2",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671321,
							["damage_taken"] = 8386.005518,
							["start_time"] = 1605671305,
							["delay"] = 1605671321,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.004577,
							["damage_from"] = {
								["Zexu"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
							},
							["total"] = 0.004577,
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["aID"] = "82647",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.004577,
							["fight_component"] = true,
							["monster"] = true,
							["dps_started"] = false,
							["end_time"] = 1605671254,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Tormented Soul",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[167951] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 167951,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-82647-0001B49944",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 301.004577,
							["start_time"] = 1605671254,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [3]
					},
				}, -- [1]
				{
					["combatId"] = 41,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 41,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 20.002672,
							["resource"] = 0.002672,
							["targets"] = {
								["Thornoxnun"] = 20,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.002672,
							["total"] = 20.002672,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002672,
							["last_event"] = 1605671253,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.002672,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 41,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = -1,
										["id"] = 55095,
										["targets"] = {
										},
										["actived_at"] = 1605671252,
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 3,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 0,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49020] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671254,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Tormented Soul",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[167951] = 1,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-82647-0001B49944",
							["aID"] = "82647",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 41,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49784.388,
				["tempo_start"] = 1605671251,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 63,
				["playing_solo"] = true,
				["totals"] = {
					8628.74607, -- [1]
					-0.029605999999989, -- [2]
					{
						-0.01716600000000312, -- [1]
						[0] = -0.014588,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 20,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					227, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 20,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:47:35",
				["cleu_timeline"] = {
				},
				["enemy"] = "Tormented Soul",
				["TotalElapsedCombatTime"] = 3.47099999999773,
				["CombatEndedAt"] = 49665.231,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 227.00195,
						}, -- [1]
					},
				},
				["end_time"] = 49665.231,
				["combat_id"] = 41,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:47:31",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Iron Grunt"] = 1,
				},
				["start_time"] = 49661.656,
				["TimeData"] = {
				},
				["contra"] = "Tormented Soul",
			}, -- [7]
			{
				{
					["combatId"] = 40,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006911,
							["end_time"] = 1605671250,
							["damage_from"] = {
							},
							["targets"] = {
								["Iron Grunt"] = 30,
							},
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["colocacao"] = 1,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 30.006911,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 30.006911,
							["aID"] = "76-0B2B0B98",
							["spec"] = 251,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Iron Grunt"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 30,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 34.06005788871551,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.006911,
							["start_time"] = 1605671249,
							["delay"] = 0,
							["last_event"] = 1605671249,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.005677,
							["total"] = 99.00567699999999,
							["damage_from"] = {
								["Moriccalas"] = true,
								["Bonesaw"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Johnny Oshimo"] = 18,
								["Maelgwyn"] = 25,
								["Qiana Moonshadow"] = 8,
								["Racy"] = 11,
								["Bonesaw"] = 11,
								["Ariok"] = 15,
								["Northpaul"] = 11,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 99.00567699999999,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671250,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Johnny Oshimo"] = 18,
											["Maelgwyn"] = 25,
											["Qiana Moonshadow"] = 8,
											["Racy"] = 11,
											["Bonesaw"] = 11,
											["Ariok"] = 15,
											["Northpaul"] = 11,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 99,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 99,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 8,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0002B49855",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671250,
							["damage_taken"] = 314.005677,
							["start_time"] = 1605671250,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 40,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 40,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 40,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["buff_uptime"] = 1,
							["aID"] = "76-0B2B0B98",
							["classe"] = "DEATHKNIGHT",
							["last_event"] = 1605671250,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 40,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["tempo_start"] = 1605671249,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Iron Grunt",
				["combat_counter"] = 62,
				["overall_added"] = true,
				["totals"] = {
					128.948901, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:47:30",
				["end_time"] = 49661.322,
				["combat_id"] = 40,
				["cleu_timeline"] = {
				},
				["totals_grupo"] = {
					30, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 30.006911,
						}, -- [1]
					},
				},
				["frags"] = {
					["Iron Grunt"] = 1,
				},
				["data_fim"] = "22:47:31",
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 49660.307,
				["contra"] = "Iron Grunt",
				["TimeData"] = {
				},
			}, -- [8]
			{
				{
					["combatId"] = 39,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003031,
							["damage_from"] = {
								["Iron Grunt"] = true,
							},
							["targets"] = {
								["Iron Grunt"] = 717,
							},
							["pets"] = {
							},
							["total"] = 717.003031,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 717.003031,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671249,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 58,
										["targets"] = {
											["Iron Grunt"] = 113,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 113,
										["n_min"] = 55,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 113,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Iron Grunt"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 34,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Iron Grunt"] = 140,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 140,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 140,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 81,
										["targets"] = {
											["Iron Grunt"] = 160,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 160,
										["n_min"] = 79,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 160,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[55095] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 60,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Iron Grunt"] = 270,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 210,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 270,
										["c_max"] = 60,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 60,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 71.78644683616872,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 12.003031,
							["start_time"] = 1605671228,
							["delay"] = 0,
							["last_event"] = 1605671246,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.001396,
							["total"] = 3875.001396,
							["damage_from"] = {
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
								["Rainiara the Kingslayer"] = true,
								["Mumper"] = true,
								["Agios Lumen"] = true,
								["Bonesaw"] = true,
								["Astraphobia-Llane"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 163,
								["Salty Futz"] = 86,
								["High Warlord Shoju"] = 155,
								["Corselle"] = 207,
								["Starcaller Astrylian"] = 92,
								["Bonesaw"] = 395,
								["Pazo Stonehoof"] = 97,
								["Maelgwyn"] = 205,
								["Qiana Moonshadow"] = 122,
								["Olin Umberhide"] = 57,
								["Kengtus Pranch the Patient"] = 184,
								["Johnny Oshimo"] = 152,
								["Plainsmender Darragh"] = 69,
								["Thornoxnun"] = 12,
								["Malothas"] = 207,
								["Durphorn the Bullheaded"] = 72,
								["Tore"] = 56,
								["Moriccalas"] = 165,
								["Etubrute"] = 151,
								["Mumper"] = 80,
								["Agios Lumen"] = 89,
								["Racy"] = 115,
								["Miserain Starsorrow"] = 408,
								["Moonalli"] = 90,
								["Yoori"] = 186,
								["Ariok"] = 151,
								["Northpaul"] = 109,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 3875.001396,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671249,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 21,
										["b_amt"] = 1,
										["c_dmg"] = 537,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Challe Tebrilinde"] = 163,
											["Salty Futz"] = 86,
											["High Warlord Shoju"] = 155,
											["Corselle"] = 207,
											["Starcaller Astrylian"] = 92,
											["Bonesaw"] = 395,
											["Pazo Stonehoof"] = 97,
											["Maelgwyn"] = 205,
											["Qiana Moonshadow"] = 122,
											["Olin Umberhide"] = 57,
											["Kengtus Pranch the Patient"] = 184,
											["Johnny Oshimo"] = 152,
											["Plainsmender Darragh"] = 69,
											["Malothas"] = 207,
											["Durphorn the Bullheaded"] = 72,
											["Tore"] = 56,
											["Moriccalas"] = 165,
											["Etubrute"] = 151,
											["Mumper"] = 80,
											["Agios Lumen"] = 89,
											["Racy"] = 115,
											["Miserain Starsorrow"] = 408,
											["Moonalli"] = 90,
											["Yoori"] = 186,
											["Ariok"] = 151,
											["Northpaul"] = 109,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3326,
										["a_amt"] = 0,
										["MISS"] = 7,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 324,
										["DODGE"] = 10,
										["total"] = 3863,
										["c_max"] = 42,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 5,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 8,
										["n_amt"] = 281,
										["r_amt"] = 0,
										["c_min"] = 16,
									}, -- [1]
									[157843] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Thornoxnun"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = 157843,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0003349855",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671249,
							["damage_taken"] = 8109.001396,
							["start_time"] = 1605671228,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 39,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 39,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 40.005534,
							["resource"] = 0.005534,
							["targets"] = {
								["Thornoxnun"] = 40,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.005534,
							["total"] = 40.005534,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 40,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 40,
										},
										["counter"] = 2,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.005534,
							["last_event"] = 1605671231,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.005534,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 39,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = 1,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 15,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 10,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49020] = 2,
								[49143] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671238,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Iron Grunt",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[157843] = 1,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-0002B49855",
							["aID"] = "78883",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 39,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49639.35,
				["tempo_start"] = 1605671228,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 61,
				["playing_solo"] = true,
				["totals"] = {
					4591.821139999999, -- [1]
					-0.07331900000007985, -- [2]
					{
						0, -- [1]
						[0] = -0.004904,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 40,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					717, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 40,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:47:19",
				["cleu_timeline"] = {
				},
				["enemy"] = "Iron Grunt",
				["TotalElapsedCombatTime"] = 9.988000000004831,
				["CombatEndedAt"] = 49649.338,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 627.003031,
						}, -- [1]
					},
				},
				["end_time"] = 49649.338,
				["combat_id"] = 39,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:47:09",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Shadowmoon Ritualist"] = 1,
					["Iron Grunt"] = 9,
				},
				["start_time"] = 49639.35,
				["TimeData"] = {
				},
				["contra"] = "Iron Grunt",
			}, -- [9]
			{
				{
					["combatId"] = 38,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007189,
							["damage_from"] = {
							},
							["targets"] = {
								["Iron Grunt"] = 355,
							},
							["pets"] = {
							},
							["total"] = 355.007189,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 355.007189,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671192,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 50,
										["targets"] = {
											["Iron Grunt"] = 99,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 99,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 99,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[49184] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 41,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Iron Grunt"] = 41,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 41,
										["c_max"] = 41,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 41,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Iron Grunt"] = 72,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 72,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 72,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Iron Grunt"] = 143,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 143,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 143,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 52.00808511572686,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.007189,
							["start_time"] = 1605671185,
							["delay"] = 0,
							["last_event"] = 1605671191,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002082,
							["total"] = 7577.002082,
							["damage_from"] = {
								["Archon"] = true,
								["Tengery"] = true,
								["Etubrute"] = true,
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
								["Ed"] = true,
								["Thrend"] = true,
								["Bonesaw"] = true,
								["Mumper"] = true,
								["Alya"] = true,
								["Agios Lumen"] = true,
								["Rhodana-Terokkar"] = true,
								["Northpaul"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 115,
								["Plainsmender Darragh"] = 401,
								["High Warlord Shoju"] = 182,
								["Rhodana-Terokkar"] = 60,
								["Turkina"] = 126,
								["Thrend"] = 266,
								["Olin Umberhide"] = 317,
								["Kengtus Pranch the Patient"] = 106,
								["Johnny Oshimo"] = 262,
								["Moriccalas"] = 263,
								["Nevo"] = 99,
								["Yoori"] = 271,
								["Durphorn the Bullheaded"] = 81,
								["Lupas"] = 92,
								["Salty Futz"] = 100,
								["Monishot"] = 29,
								["Racy"] = 215,
								["Corselle"] = 298,
								["Starcaller Astrylian"] = 52,
								["Bonesaw"] = 435,
								["Maelgwyn"] = 427,
								["Tore"] = 94,
								["Moonalli"] = 56,
								["Malothas"] = 348,
								["Tengery"] = 25,
								["Miserain Starsorrow"] = 618,
								["Barbery the Crazy Cat Lady"] = 144,
								["Etubrute"] = 364,
								["Qiana Moonshadow"] = 97,
								["Pazo Stonehoof"] = 155,
								["Ryii the Shameless"] = 329,
								["Mumper"] = 145,
								["Arnold Croman"] = 493,
								["Agios Lumen"] = 187,
								["Ariok"] = 144,
								["Northpaul"] = 181,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 7577.002082,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671228,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 36,
										["b_amt"] = 3,
										["c_dmg"] = 768,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Challe Tebrilinde"] = 115,
											["Miserain Starsorrow"] = 618,
											["High Warlord Shoju"] = 182,
											["Rhodana-Terokkar"] = 60,
											["Turkina"] = 126,
											["Thrend"] = 266,
											["Olin Umberhide"] = 317,
											["Kengtus Pranch the Patient"] = 106,
											["Johnny Oshimo"] = 262,
											["Moriccalas"] = 263,
											["Nevo"] = 99,
											["Yoori"] = 271,
											["Durphorn the Bullheaded"] = 81,
											["Lupas"] = 92,
											["Salty Futz"] = 100,
											["Monishot"] = 29,
											["Racy"] = 215,
											["Corselle"] = 298,
											["Starcaller Astrylian"] = 52,
											["Bonesaw"] = 435,
											["Maelgwyn"] = 427,
											["Tore"] = 94,
											["Moonalli"] = 56,
											["Malothas"] = 348,
											["Plainsmender Darragh"] = 401,
											["Tengery"] = 25,
											["Barbery the Crazy Cat Lady"] = 144,
											["Etubrute"] = 364,
											["Qiana Moonshadow"] = 97,
											["Pazo Stonehoof"] = 155,
											["Ryii the Shameless"] = 329,
											["Mumper"] = 145,
											["Arnold Croman"] = 493,
											["Agios Lumen"] = 187,
											["Ariok"] = 144,
											["Northpaul"] = 181,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6809,
										["a_amt"] = 0,
										["MISS"] = 19,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 723,
										["DODGE"] = 30,
										["total"] = 7577,
										["c_max"] = 29,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 16,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 18,
										["n_amt"] = 622,
										["r_amt"] = 0,
										["c_min"] = 16,
									}, -- [1]
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0000349879",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671228,
							["damage_taken"] = 8911.002081999999,
							["start_time"] = 1605671185,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 38,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 38,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 40.004449,
							["resource"] = 0.004449,
							["targets"] = {
								["Thornoxnun"] = 40,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.004449,
							["total"] = 40.004449,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49184] = {
										["total"] = 20,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 2,
									},
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.004449,
							["last_event"] = 1605671228,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.004449,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 38,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 12,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 3,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49143] = 1,
								[49020] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671192,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 38,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1605671185,
				["enemy"] = "Iron Grunt",
				["combat_counter"] = 60,
				["playing_solo"] = true,
				["totals"] = {
					7931.744413000002, -- [1]
					-0.01532600000000883, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = -0.002396000000004506,
						[6] = 39.992092,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 49602.905,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:46:26",
				["end_time"] = 49602.905,
				["totals_grupo"] = {
					355, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 40,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 38,
				["TotalElapsedCombatTime"] = 49602.905,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 355.007189,
						}, -- [1]
					},
				},
				["frags"] = {
					["Iron Grunt"] = 3,
				},
				["data_fim"] = "22:46:33",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 49596.079,
				["contra"] = "Iron Grunt",
				["TimeData"] = {
				},
			}, -- [10]
			{
				{
					["combatId"] = 37,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008993,
							["damage_from"] = {
							},
							["targets"] = {
								["Iron Grunt"] = 402,
							},
							["pets"] = {
							},
							["total"] = 402.008993,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 402.008993,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671180,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 51,
										["targets"] = {
											["Iron Grunt"] = 149,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 149,
										["n_min"] = 48,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 149,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Iron Grunt"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Iron Grunt"] = 72,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 72,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 72,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Iron Grunt"] = 90,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 90,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 90,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 71,
										["targets"] = {
											["Iron Grunt"] = 71,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 71,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 71,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 42.13930744234416,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.008993,
							["start_time"] = 1605671171,
							["delay"] = 0,
							["last_event"] = 1605671180,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002142,
							["total"] = 2727.002142,
							["damage_from"] = {
								["Archon"] = true,
								["Tengery"] = true,
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
								["Arnold Croman"] = true,
								["Thrend"] = true,
								["Mumper"] = true,
							},
							["targets"] = {
								["Lupas"] = 67,
								["Plainsmender Darragh"] = 172,
								["Monishot"] = 91,
								["Corselle"] = 134,
								["Starcaller Astrylian"] = 9,
								["Bonesaw"] = 185,
								["Arnold Croman"] = 196,
								["Thrend"] = 186,
								["Maelgwyn"] = 151,
								["Tore"] = 68,
								["Olin Umberhide"] = 72,
								["Johnny Oshimo"] = 100,
								["Mumper"] = 33,
								["Malothas"] = 174,
								["Tengery"] = 43,
								["Turkina"] = 54,
								["Moriccalas"] = 60,
								["Salty Futz"] = 84,
								["Durphorn the Bullheaded"] = 57,
								["Pazo Stonehoof"] = 73,
								["Miserain Starsorrow"] = 175,
								["Nevo"] = 74,
								["Barbery the Crazy Cat Lady"] = 85,
								["Yoori"] = 120,
								["Ryii the Shameless"] = 175,
								["Northpaul"] = 89,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 2727.002142,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671185,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 15,
										["b_amt"] = 3,
										["c_dmg"] = 309,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Lupas"] = 67,
											["Plainsmender Darragh"] = 172,
											["Monishot"] = 91,
											["Corselle"] = 134,
											["Starcaller Astrylian"] = 9,
											["Bonesaw"] = 185,
											["Arnold Croman"] = 196,
											["Thrend"] = 186,
											["Maelgwyn"] = 151,
											["Tore"] = 68,
											["Olin Umberhide"] = 72,
											["Johnny Oshimo"] = 100,
											["Mumper"] = 33,
											["Malothas"] = 174,
											["Tengery"] = 43,
											["Turkina"] = 54,
											["Moriccalas"] = 60,
											["Durphorn the Bullheaded"] = 57,
											["Salty Futz"] = 84,
											["Pazo Stonehoof"] = 73,
											["Miserain Starsorrow"] = 175,
											["Nevo"] = 74,
											["Barbery the Crazy Cat Lady"] = 85,
											["Yoori"] = 120,
											["Ryii the Shameless"] = 175,
											["Northpaul"] = 89,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2418,
										["DODGE"] = 7,
										["MISS"] = 4,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 266,
										["a_amt"] = 0,
										["total"] = 2727,
										["c_max"] = 28,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 6,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 19,
										["n_amt"] = 234,
										["r_amt"] = 0,
										["c_min"] = 15,
									}, -- [1]
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0000B49864",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671185,
							["damage_taken"] = 2764.002142,
							["start_time"] = 1605671171,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 37,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 37,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 40.0026,
							["resource"] = 0.0026,
							["targets"] = {
								["Thornoxnun"] = 40,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.0026,
							["total"] = 40.0026,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 40,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 40,
										},
										["counter"] = 2,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.0026,
							["last_event"] = 1605671185,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.0026,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 37,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[55095] = {
										["activedamt"] = -1,
										["id"] = 55095,
										["targets"] = {
										},
										["actived_at"] = 1605671180,
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 22,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 0,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49143] = 1,
								[49020] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671180,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 37,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49596.079,
				["tempo_start"] = 1605671171,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 59,
				["playing_solo"] = true,
				["totals"] = {
					3128.863708000001, -- [1]
					-0.04596600000002039, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 39.997445,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					402, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 40,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:46:21",
				["cleu_timeline"] = {
				},
				["enemy"] = "Iron Grunt",
				["TotalElapsedCombatTime"] = 9.540000000000873,
				["CombatEndedAt"] = 49591.135,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 402.008993,
						}, -- [1]
					},
				},
				["end_time"] = 49591.135,
				["combat_id"] = 37,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:46:11",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Shadowmoon Ritualist"] = 1,
					["Iron Grunt"] = 4,
				},
				["start_time"] = 49581.595,
				["TimeData"] = {
				},
				["contra"] = "Iron Grunt",
			}, -- [11]
			{
				{
					["combatId"] = 36,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006477,
							["damage_from"] = {
							},
							["targets"] = {
								["Iron Grunt"] = 249,
							},
							["pets"] = {
							},
							["total"] = 249.006477,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 249.006477,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671168,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 53,
										["targets"] = {
											["Iron Grunt"] = 104,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 104,
										["n_min"] = 51,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 104,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 71,
										["targets"] = {
											["Iron Grunt"] = 71,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 71,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 71,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 74,
										["targets"] = {
											["Iron Grunt"] = 74,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 74,
										["n_min"] = 74,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 74,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 63.92977586654805,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.006477,
							["start_time"] = 1605671164,
							["delay"] = 0,
							["last_event"] = 1605671167,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.005854,
							["total"] = 1032.005854,
							["damage_from"] = {
								["Arnold Croman"] = true,
								["Tengery"] = true,
								["Thrend"] = true,
								["Mumper"] = true,
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Lupas"] = 28,
								["Salty Futz"] = 31,
								["Monishot"] = 32,
								["Corselle"] = 61,
								["Starcaller Astrylian"] = 18,
								["Bonesaw"] = 98,
								["Arnold Croman"] = 72,
								["Thrend"] = 10,
								["Maelgwyn"] = 69,
								["Tore"] = 19,
								["Olin Umberhide"] = 45,
								["Johnny Oshimo"] = 42,
								["Malothas"] = 69,
								["Mumper"] = 12,
								["Turkina"] = 28,
								["Moriccalas"] = 67,
								["Durphorn the Bullheaded"] = 32,
								["Pazo Stonehoof"] = 30,
								["Plainsmender Darragh"] = 48,
								["Miserain Starsorrow"] = 31,
								["Nevo"] = 12,
								["Barbery the Crazy Cat Lady"] = 22,
								["Yoori"] = 49,
								["Ryii the Shameless"] = 68,
								["Northpaul"] = 39,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1032.005854,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671171,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 4,
										["b_amt"] = 0,
										["c_dmg"] = 75,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Lupas"] = 28,
											["Plainsmender Darragh"] = 48,
											["Monishot"] = 32,
											["Corselle"] = 61,
											["Starcaller Astrylian"] = 18,
											["Bonesaw"] = 98,
											["Arnold Croman"] = 72,
											["Thrend"] = 10,
											["Maelgwyn"] = 69,
											["Tore"] = 19,
											["Olin Umberhide"] = 45,
											["Johnny Oshimo"] = 42,
											["Malothas"] = 69,
											["Mumper"] = 12,
											["Durphorn the Bullheaded"] = 32,
											["Moriccalas"] = 67,
											["Pazo Stonehoof"] = 30,
											["Ryii the Shameless"] = 68,
											["Turkina"] = 28,
											["Barbery the Crazy Cat Lady"] = 22,
											["Nevo"] = 12,
											["Miserain Starsorrow"] = 31,
											["Yoori"] = 49,
											["Salty Futz"] = 31,
											["Northpaul"] = 39,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 957,
										["a_amt"] = 0,
										["MISS"] = 7,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 112,
										["DODGE"] = 4,
										["total"] = 1032,
										["c_max"] = 21,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 4,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 93,
										["r_amt"] = 0,
										["c_min"] = 17,
									}, -- [1]
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0000349864",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671170,
							["damage_taken"] = 1139.005854,
							["start_time"] = 1605671164,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 36,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 36,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 30.001919,
							["resource"] = 0.001919,
							["targets"] = {
								["Thornoxnun"] = 30,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.001919,
							["total"] = 30.001919,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 20,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 20,
										},
										["counter"] = 1,
									},
									[49184] = {
										["total"] = 10,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.001919,
							["last_event"] = 1605671171,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.001919,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 36,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thornoxnun",
							["buff_uptime_targets"] = {
							},
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49020] = 1,
							},
							["buff_uptime"] = 12,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["tipo"] = 4,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0B2B0B98",
							["last_event"] = 1605671168,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 36,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1605671164,
				["enemy"] = "Iron Grunt",
				["combat_counter"] = 58,
				["playing_solo"] = true,
				["totals"] = {
					1280.865724, -- [1]
					-0.1201699999997139, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 49578.96,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:46:05",
				["end_time"] = 49578.96,
				["totals_grupo"] = {
					249, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 30,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 36,
				["TotalElapsedCombatTime"] = 49578.96,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 249.006477,
						}, -- [1]
					},
				},
				["frags"] = {
					["Iron Grunt"] = 3,
				},
				["data_fim"] = "22:46:09",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 49575.065,
				["contra"] = "Iron Grunt",
				["TimeData"] = {
				},
			}, -- [12]
			{
				{
					["combatId"] = 35,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007697,
							["damage_from"] = {
								["Warsong Commander"] = true,
							},
							["targets"] = {
								["Warsong Commander"] = 595,
							},
							["pets"] = {
							},
							["total"] = 595.007697,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 595.007697,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671159,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 202,
										["g_amt"] = 0,
										["n_max"] = 49,
										["targets"] = {
											["Warsong Commander"] = 251,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 49,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 251,
										["c_max"] = 104,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 98,
									}, -- [1]
									[325461] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 74,
										["targets"] = {
											["Warsong Commander"] = 146,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 146,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 146,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Warsong Commander"] = 60,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 60,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 60,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Warsong Commander"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[196771] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Warsong Commander"] = 48,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 48,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 48,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 196771,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Warsong Commander"] = 70,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 70,
										["n_min"] = 70,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 70,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 69.36438528793508,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 34.007697,
							["start_time"] = 1605671151,
							["delay"] = 0,
							["last_event"] = 1605671158,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002811,
							["total"] = 34.002811,
							["damage_from"] = {
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Thornoxnun"] = 34,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "83538",
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.002811,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671159,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Warsong Commander",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 13,
										["targets"] = {
											["Thornoxnun"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 4,
										["MISS"] = 1,
										["total"] = 34,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-83538-000134986D",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671158,
							["damage_taken"] = 595.0028110000001,
							["start_time"] = 1605671153,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 35,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 35,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 50.006622,
							["resource"] = 0.006622,
							["targets"] = {
								["Thornoxnun"] = 50,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.006622,
							["total"] = 50.006622,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[196770] = {
										["total"] = 10,
										["id"] = 196770,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[49020] = {
										["total"] = 40,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 40,
										},
										["counter"] = 2,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006622,
							["last_event"] = 1605671155,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.006622,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 35,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[211793] = {
										["activedamt"] = 0,
										["id"] = 211793,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[55095] = {
										["activedamt"] = -1,
										["id"] = 55095,
										["targets"] = {
										},
										["actived_at"] = 1605671158,
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 18,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[196770] = {
										["activedamt"] = 1,
										["id"] = 196770,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 6,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[196770] = 1,
								[49020] = 2,
								[49143] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671159,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 35,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49575.065,
				["tempo_start"] = 1605671151,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 57,
				["playing_solo"] = true,
				["totals"] = {
					628.8460579999991, -- [1]
					-0.0319300000000044, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 49.997011,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					595, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 50,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:46:00",
				["cleu_timeline"] = {
				},
				["enemy"] = "Warsong Commander",
				["TotalElapsedCombatTime"] = 49570.447,
				["CombatEndedAt"] = 49570.447,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 595.007697,
						}, -- [1]
					},
				},
				["end_time"] = 49570.447,
				["combat_id"] = 35,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:45:52",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Iron Grunt"] = 7,
					["Shadowmoon Ritualist"] = 1,
					["Warsong Commander"] = 1,
				},
				["start_time"] = 49561.869,
				["TimeData"] = {
				},
				["contra"] = "Warsong Commander",
			}, -- [13]
			{
				{
					["combatId"] = 34,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00705,
							["damage_from"] = {
								["Iron Grunt"] = true,
							},
							["targets"] = {
								["Iron Grunt"] = 1355,
							},
							["pets"] = {
							},
							["total"] = 1355.00705,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1355.00705,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671120,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 58,
										["targets"] = {
											["Iron Grunt"] = 259,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 259,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 259,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[325461] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 144,
										["g_amt"] = 0,
										["n_max"] = 84,
										["targets"] = {
											["Iron Grunt"] = 301,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 157,
										["n_min"] = 73,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 301,
										["c_max"] = 144,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 144,
									},
									[55095] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 188,
										["g_amt"] = 0,
										["n_max"] = 34,
										["targets"] = {
											["Iron Grunt"] = 414,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 10,
										["total"] = 414,
										["c_max"] = 68,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 60,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Iron Grunt"] = 54,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 54,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 54,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[196771] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 32,
										["g_amt"] = 0,
										["n_max"] = 9,
										["targets"] = {
											["Iron Grunt"] = 107,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 75,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 107,
										["c_max"] = 16,
										["spellschool"] = 16,
										["id"] = 196771,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 9,
										["r_amt"] = 0,
										["c_min"] = 16,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 81,
										["targets"] = {
											["Iron Grunt"] = 220,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 220,
										["n_min"] = 69,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 220,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 24.08772954331143,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 50.00705,
							["start_time"] = 1605671085,
							["delay"] = 1605671095,
							["last_event"] = 1605671118,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.006409,
							["total"] = 15782.006409,
							["damage_from"] = {
								["Gravywave-Stormrage"] = true,
								["Zexu"] = true,
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Bonesaw"] = true,
								["Holyhoe-Arygos"] = true,
								["Thrend"] = true,
								["Yalrashea"] = true,
								["Mumper"] = true,
								["Musclemago-Arygos"] = true,
								["Archon"] = true,
								["Izzad-Arygos"] = true,
								["Gruffhorn"] = true,
								["Dagger"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
								["Etubrute"] = true,
								["Tengery"] = true,
								["Palios-Azshara"] = true,
								["Ed"] = true,
								["Rainiara the Kingslayer"] = true,
								["Agios Lumen"] = true,
								["Irondor-Kel'Thuzad"] = true,
								["Arnold Croman"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 677,
								["Miserain Starsorrow"] = 852,
								["High Warlord Shoju"] = 188,
								["Arnold Croman"] = 439,
								["Thrend"] = 530,
								["Yalrashea"] = 18,
								["Elaynea Welton the Wind and Sea"] = 318,
								["Olin Umberhide"] = 638,
								["Kengtus Pranch the Patient"] = 729,
								["Johnny Oshimo"] = 367,
								["Izzad-Arygos"] = 52,
								["Moriccalas"] = 258,
								["Nevo"] = 413,
								["Roague"] = 319,
								["Irondor-Kel'Thuzad"] = 69,
								["Durphorn the Bullheaded"] = 267,
								["Lupas"] = 253,
								["Salty Futz"] = 343,
								["Barbery the Crazy Cat Lady"] = 369,
								["Corselle"] = 626,
								["Starcaller Astrylian"] = 625,
								["Bonesaw"] = 878,
								["Ryii the Shameless"] = 783,
								["Tore"] = 294,
								["Musclemago-Arygos"] = 15,
								["Moonalli"] = 305,
								["Zexu"] = 15,
								["Mumper"] = 146,
								["Malothas"] = 516,
								["Gravywave-Stormrage"] = 12,
								["Monishot"] = 327,
								["Qiana Moonshadow"] = 271,
								["Thornoxnun"] = 50,
								["Plainsmender Darragh"] = 598,
								["Yoori"] = 715,
								["Turkina"] = 347,
								["Pazo Stonehoof"] = 375,
								["Etubrute"] = 484,
								["Agios Lumen"] = 549,
								["Ariok"] = 462,
								["Northpaul"] = 290,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 15782.006409,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671151,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 73,
										["b_amt"] = 13,
										["c_dmg"] = 1530,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Challe Tebrilinde"] = 677,
											["Miserain Starsorrow"] = 852,
											["High Warlord Shoju"] = 188,
											["Arnold Croman"] = 439,
											["Thrend"] = 530,
											["Elaynea Welton the Wind and Sea"] = 318,
											["Olin Umberhide"] = 638,
											["Kengtus Pranch the Patient"] = 729,
											["Johnny Oshimo"] = 367,
											["Izzad-Arygos"] = 40,
											["Moriccalas"] = 258,
											["Nevo"] = 413,
											["Yoori"] = 715,
											["Irondor-Kel'Thuzad"] = 69,
											["Durphorn the Bullheaded"] = 267,
											["Lupas"] = 253,
											["Salty Futz"] = 343,
											["Barbery the Crazy Cat Lady"] = 369,
											["Corselle"] = 626,
											["Starcaller Astrylian"] = 625,
											["Bonesaw"] = 878,
											["Ryii the Shameless"] = 783,
											["Tore"] = 294,
											["Musclemago-Arygos"] = 15,
											["Moonalli"] = 305,
											["Mumper"] = 146,
											["Malothas"] = 500,
											["Gravywave-Stormrage"] = 12,
											["Monishot"] = 327,
											["Thornoxnun"] = 19,
											["Etubrute"] = 484,
											["Qiana Moonshadow"] = 271,
											["Plainsmender Darragh"] = 598,
											["Turkina"] = 347,
											["Roague"] = 319,
											["Pazo Stonehoof"] = 375,
											["Agios Lumen"] = 549,
											["Ariok"] = 462,
											["Northpaul"] = 290,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14160,
										["a_amt"] = 0,
										["DODGE"] = 43,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 1538,
										["MISS"] = 53,
										["total"] = 15690,
										["c_max"] = 37,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 28,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 109,
										["n_amt"] = 1341,
										["r_amt"] = 0,
										["c_min"] = 11,
									}, -- [1]
									[157843] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Zexu"] = 15,
											["Malothas"] = 16,
											["Izzad-Arygos"] = 12,
											["Yalrashea"] = 18,
											["Thornoxnun"] = 31,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 92,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 92,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 157843,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 4,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0005B49854",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671151,
							["damage_taken"] = 22110.006409,
							["start_time"] = 1605671064,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 34,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-0B2B0B98",
							["totalover"] = 0.004279,
							["total_without_pet"] = 2.004279,
							["total"] = 2.004279,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0B2B0B98",
							["totalabsorb"] = 0.004279,
							["last_hps"] = 0,
							["targets"] = {
								["Thornoxnun"] = 2,
							},
							["totalover_without_pet"] = 0.004279,
							["healing_taken"] = 2.004279,
							["fight_component"] = true,
							["end_time"] = 1605671120,
							["healing_from"] = {
								["Thornoxnun"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[291843] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 1,
										["targets"] = {
											["Thornoxnun"] = 2,
										},
										["n_min"] = 1,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 2,
										["c_max"] = 0,
										["id"] = 291843,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 2,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1605671117,
							["spec"] = 251,
							["custom"] = 0,
							["tipo"] = 2,
							["classe"] = "DEATHKNIGHT",
							["totaldenied"] = 0.004279,
							["delay"] = 1605671069,
							["last_event"] = 1605671069,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 34,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 90.003196,
							["resource"] = 0.003196,
							["targets"] = {
								["Thornoxnun"] = 90,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.003196,
							["fight_component"] = true,
							["total"] = 90.003196,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49020] = {
										["total"] = 60,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 60,
										},
										["counter"] = 3,
									},
									[49184] = {
										["total"] = 30,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 30,
										},
										["counter"] = 3,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003196,
							["last_event"] = 1605671151,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.003196,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 34,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[211793] = {
										["activedamt"] = 0,
										["id"] = 211793,
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 3,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 25,
										["appliedamt"] = 3,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 92,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[52424] = {
										["activedamt"] = 1,
										["id"] = 52424,
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 57,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 2,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 2,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[291843] = {
										["activedamt"] = 1,
										["id"] = 291843,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[196770] = {
										["activedamt"] = 1,
										["id"] = 196770,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 36,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 2,
								[49020] = 3,
								[49143] = 3,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671120,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Iron Grunt",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[157843] = 4,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-0005B49854",
							["aID"] = "78883",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 34,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49561.869,
				["tempo_start"] = 1605671063,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 56,
				["playing_solo"] = true,
				["totals"] = {
					17136.73164100002, -- [1]
					1.901649999998579, -- [2]
					{
						-0.01835399999999446, -- [1]
						[0] = -0.02923300000000321,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 89.998893,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					1355, -- [1]
					2, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 90,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:45:20",
				["cleu_timeline"] = {
				},
				["enemy"] = "Iron Grunt",
				["TotalElapsedCombatTime"] = 56.13100000000122,
				["CombatEndedAt"] = 49530.592,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thornoxnun"] = 2.004279,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 1355.00705,
						}, -- [1]
					},
				},
				["end_time"] = 49530.592,
				["combat_id"] = 34,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:44:24",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Iron Grunt"] = 33,
					["Shadowmoon Ritualist"] = 7,
					["Earthbind Totem"] = 1,
				},
				["start_time"] = 49474.339,
				["TimeData"] = {
				},
				["contra"] = "Iron Grunt",
			}, -- [14]
			{
				{
					["combatId"] = 33,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008081,
							["damage_from"] = {
								["Iron Grunt"] = true,
							},
							["targets"] = {
								["Iron Grunt"] = 769,
							},
							["pets"] = {
							},
							["total"] = 769.008081,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 769.008081,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671055,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 52,
										["targets"] = {
											["Iron Grunt"] = 153,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 153,
										["n_min"] = 50,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 153,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[325461] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 292,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Iron Grunt"] = 292,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 292,
										["c_max"] = 149,
										["spellschool"] = 1,
										["id"] = 325461,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 143,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 30,
										["targets"] = {
											["Iron Grunt"] = 90,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 90,
										["n_min"] = 30,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 90,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 55095,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Iron Grunt"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 34,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 49184,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[196771] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 17,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Iron Grunt"] = 57,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 40,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 57,
										["c_max"] = 17,
										["spellschool"] = 16,
										["id"] = 196771,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 17,
									},
									[325464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Iron Grunt"] = 143,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 143,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 143,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 325464,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 54.2586665490626,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 58.008081,
							["start_time"] = 1605671040,
							["delay"] = 0,
							["last_event"] = 1605671054,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.002237,
							["total"] = 5323.002237000001,
							["damage_from"] = {
								["Tengery"] = true,
								["Corselle"] = true,
								["Starcaller Astrylian"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
								["Ed"] = true,
								["Thrend"] = true,
								["Etubrute"] = true,
								["Mumper"] = true,
								["Musclemago-Arygos"] = true,
								["Agios Lumen"] = true,
								["Alya"] = true,
								["Arnold Croman"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 246,
								["Plainsmender Darragh"] = 220,
								["High Warlord Shoju"] = 102,
								["Pazo Stonehoof"] = 89,
								["Thrend"] = 112,
								["Elaynea Welton the Wind and Sea"] = 60,
								["Olin Umberhide"] = 278,
								["Kengtus Pranch the Patient"] = 221,
								["Johnny Oshimo"] = 83,
								["Moriccalas"] = 86,
								["Nevo"] = 51,
								["Roague"] = 129,
								["Durphorn the Bullheaded"] = 92,
								["Salty Futz"] = 96,
								["Barbery the Crazy Cat Lady"] = 118,
								["Racy"] = 86,
								["Corselle"] = 250,
								["Starcaller Astrylian"] = 54,
								["Bonesaw"] = 167,
								["Maelgwyn"] = 168,
								["Qiana Moonshadow"] = 112,
								["Musclemago-Arygos"] = 48,
								["Moonalli"] = 264,
								["Miserain Starsorrow"] = 278,
								["Thornoxnun"] = 58,
								["Mumper"] = 18,
								["Etubrute"] = 200,
								["Arnold Croman"] = 259,
								["Ryii the Shameless"] = 226,
								["Tore"] = 114,
								["Turkina"] = 89,
								["Yoori"] = 204,
								["Agios Lumen"] = 417,
								["Ariok"] = 168,
								["Northpaul"] = 160,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 5323.002237000001,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605671063,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 24,
										["b_amt"] = 10,
										["c_dmg"] = 513,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Challe Tebrilinde"] = 246,
											["Plainsmender Darragh"] = 220,
											["High Warlord Shoju"] = 102,
											["Pazo Stonehoof"] = 89,
											["Thrend"] = 112,
											["Elaynea Welton the Wind and Sea"] = 60,
											["Olin Umberhide"] = 278,
											["Kengtus Pranch the Patient"] = 221,
											["Johnny Oshimo"] = 83,
											["Moriccalas"] = 86,
											["Nevo"] = 51,
											["Roague"] = 129,
											["Durphorn the Bullheaded"] = 92,
											["Salty Futz"] = 96,
											["Barbery the Crazy Cat Lady"] = 118,
											["Racy"] = 86,
											["Corselle"] = 250,
											["Starcaller Astrylian"] = 54,
											["Bonesaw"] = 167,
											["Maelgwyn"] = 168,
											["Qiana Moonshadow"] = 112,
											["Musclemago-Arygos"] = 48,
											["Moonalli"] = 264,
											["Miserain Starsorrow"] = 278,
											["Thornoxnun"] = 58,
											["Mumper"] = 18,
											["Etubrute"] = 200,
											["Arnold Croman"] = 259,
											["Ryii the Shameless"] = 226,
											["Tore"] = 114,
											["Turkina"] = 89,
											["Yoori"] = 204,
											["Agios Lumen"] = 417,
											["Ariok"] = 168,
											["Northpaul"] = 160,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4810,
										["a_amt"] = 0,
										["DODGE"] = 10,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 509,
										["MISS"] = 7,
										["total"] = 5323,
										["c_max"] = 38,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 11,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 62,
										["n_amt"] = 457,
										["r_amt"] = 0,
										["c_min"] = 16,
									}, -- [1]
									[157843] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thornoxnun"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 157843,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3886-1265-814-78883-0001349878",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671063,
							["damage_taken"] = 5803.002237000001,
							["start_time"] = 1605671041,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 33,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 33,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 60.008838,
							["resource"] = 0.008838,
							["targets"] = {
								["Thornoxnun"] = 60,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.008838,
							["total"] = 60.008838,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[49184] = {
										["total"] = 10,
										["id"] = 49184,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[196770] = {
										["total"] = 10,
										["id"] = 196770,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
									[49020] = {
										["total"] = 40,
										["id"] = 49020,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 40,
										},
										["counter"] = 2,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.008838,
							["last_event"] = 1605671063,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.008838,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 33,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[211793] = {
										["activedamt"] = 0,
										["id"] = 211793,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 3,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[55095] = {
										["activedamt"] = 0,
										["id"] = 55095,
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 22,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 15,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[196770] = {
										["activedamt"] = 1,
										["id"] = 196770,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[194879] = {
										["activedamt"] = 1,
										["id"] = 194879,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 17,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49020] = 2,
								[49143] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671055,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["tipo"] = 4,
							["nome"] = "Iron Grunt",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[157843] = 1,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-0001349878",
							["aID"] = "78883",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 33,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49451.534,
				["tempo_start"] = 1605671040,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 55,
				["playing_solo"] = true,
				["totals"] = {
					6091.775553000003, -- [1]
					-0.07311100000025306, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 59.9923,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					769, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 60,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:44:15",
				["cleu_timeline"] = {
				},
				["enemy"] = "Iron Grunt",
				["TotalElapsedCombatTime"] = 14.1730000000025,
				["CombatEndedAt"] = 49465.707,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 769.008081,
						}, -- [1]
					},
				},
				["end_time"] = 49465.707,
				["combat_id"] = 33,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:44:01",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Warsong Commander"] = 1,
					["Iron Grunt"] = 8,
				},
				["start_time"] = 49451.534,
				["TimeData"] = {
				},
				["contra"] = "Iron Grunt",
			}, -- [15]
			{
				{
					["combatId"] = 32,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005653,
							["damage_from"] = {
							},
							["targets"] = {
								["Iron Grunt"] = 16,
							},
							["pets"] = {
							},
							["spec"] = 251,
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 16.005653,
							["end_time"] = 1605671038,
							["colocacao"] = 1,
							["dps_started"] = false,
							["total"] = 16.005653,
							["classe"] = "DEATHKNIGHT",
							["aID"] = "76-0B2B0B98",
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[196771] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Iron Grunt"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 8,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 16,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 196771,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["on_hold"] = false,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 8.26311461019915,
							["custom"] = 0,
							["last_event"] = 1605671037,
							["damage_taken"] = 0.005653,
							["start_time"] = 1605671036,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.002358,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Thrend"] = true,
								["Mumper"] = true,
								["Corselle"] = true,
								["Agios Lumen"] = true,
								["Moriccalas"] = true,
								["Thornoxnun"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 50,
								["Salty Futz"] = 21,
								["Barbery the Crazy Cat Lady"] = 23,
								["Racy"] = 18,
								["Corselle"] = 43,
								["Bonesaw"] = 34,
								["Arnold Croman"] = 56,
								["Thrend"] = 84,
								["Ryii the Shameless"] = 49,
								["Qiana Moonshadow"] = 17,
								["Olin Umberhide"] = 46,
								["Moonalli"] = 45,
								["Johnny Oshimo"] = 19,
								["Kengtus Pranch the Patient"] = 10,
								["Tore"] = 11,
								["High Warlord Shoju"] = 21,
								["Pazo Stonehoof"] = 31,
								["Yoori"] = 35,
								["Moriccalas"] = 32,
								["Etubrute"] = 51,
								["Maelgwyn"] = 39,
								["Miserain Starsorrow"] = 32,
								["Plainsmender Darragh"] = 45,
								["Agios Lumen"] = 12,
								["Northpaul"] = 23,
								["Roague"] = 10,
								["Ariok"] = 27,
								["Durphorn the Bullheaded"] = 15,
							},
							["end_time"] = 1605671040,
							["pets"] = {
							},
							["fight_component"] = true,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 899.002358,
							["monster"] = true,
							["dps_started"] = false,
							["total"] = 899.002358,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 5,
										["b_amt"] = 2,
										["c_dmg"] = 115,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Challe Tebrilinde"] = 50,
											["Miserain Starsorrow"] = 32,
											["Barbery the Crazy Cat Lady"] = 23,
											["Racy"] = 18,
											["Corselle"] = 43,
											["Bonesaw"] = 34,
											["Arnold Croman"] = 56,
											["Thrend"] = 84,
											["Ryii the Shameless"] = 49,
											["Tore"] = 11,
											["Olin Umberhide"] = 46,
											["Moonalli"] = 45,
											["Johnny Oshimo"] = 19,
											["High Warlord Shoju"] = 21,
											["Pazo Stonehoof"] = 31,
											["Kengtus Pranch the Patient"] = 10,
											["Yoori"] = 35,
											["Maelgwyn"] = 39,
											["Moriccalas"] = 32,
											["Etubrute"] = 51,
											["Plainsmender Darragh"] = 45,
											["Salty Futz"] = 21,
											["Agios Lumen"] = 12,
											["Qiana Moonshadow"] = 17,
											["Northpaul"] = 23,
											["Roague"] = 10,
											["Ariok"] = 27,
											["Durphorn the Bullheaded"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 784,
										["DODGE"] = 2,
										["a_amt"] = 0,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 87,
										["MISS"] = 3,
										["total"] = 899,
										["c_max"] = 34,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 3,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 12,
										["n_amt"] = 74,
										["r_amt"] = 0,
										["c_min"] = 15,
									}, -- [1]
								},
							},
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-000034986D",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671040,
							["damage_taken"] = 850.002358,
							["start_time"] = 1605671037,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 32,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 32,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 32,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[211793] = {
										["activedamt"] = 0,
										["id"] = 211793,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 4,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[196770] = {
										["activedamt"] = 1,
										["id"] = 196770,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 2,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1605671038,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["serial"] = "Player-76-0B2B0B98",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 32,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49447.53,
				["tempo_start"] = 1605671036,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 54,
				["playing_solo"] = true,
				["totals"] = {
					914.8309500000005, -- [1]
					-0.013353, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					16, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:43:59",
				["cleu_timeline"] = {
				},
				["enemy"] = "Iron Grunt",
				["TotalElapsedCombatTime"] = 1.937000000005355,
				["CombatEndedAt"] = 49449.467,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 16.005653,
						}, -- [1]
					},
				},
				["end_time"] = 49449.467,
				["combat_id"] = 32,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:43:57",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Iron Grunt"] = 1,
				},
				["start_time"] = 49447.53,
				["TimeData"] = {
				},
				["contra"] = "Iron Grunt",
			}, -- [16]
			{
				{
					["combatId"] = 31,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001353,
							["damage_from"] = {
							},
							["targets"] = {
								["Iron Grunt"] = 46,
							},
							["pets"] = {
							},
							["total"] = 46.001353,
							["on_hold"] = false,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 46.001353,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1605671014,
							["aID"] = "76-0B2B0B98",
							["friendlyfire_total"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 46,
										["targets"] = {
											["Iron Grunt"] = 46,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 46,
										["n_min"] = 46,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 46,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 24.02159425586372,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.001353,
							["start_time"] = 1605671012,
							["delay"] = 0,
							["last_event"] = 1605671012,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.001396,
							["friendlyfire"] = {
							},
							["damage_from"] = {
								["Lunaría-Turalyon"] = true,
								["Corselle"] = true,
								["Kung Din"] = true,
								["Thrend"] = true,
								["Kyrthora-Hellscream"] = true,
								["Mumper"] = true,
								["Rainiara the Kingslayer"] = true,
								["Moriccalas"] = true,
								["Etubrute"] = true,
								["Durphorn the Bullheaded"] = true,
								["Ed"] = true,
								["Gclown"] = true,
								["Rezêc-Dalaran"] = true,
								["Thornoxnun"] = true,
								["Agios Lumen"] = true,
								["Kokiria-Turalyon"] = true,
								["Rakira"] = true,
							},
							["targets"] = {
								["Challe Tebrilinde"] = 247,
								["Plainsmender Darragh"] = 227,
								["Lunaría-Turalyon"] = 10,
								["High Warlord Shoju"] = 130,
								["Gclown"] = 9,
								["Arnold Croman"] = 51,
								["Thrend"] = 162,
								["Olin Umberhide"] = 248,
								["Kengtus Pranch the Patient"] = 275,
								["Johnny Oshimo"] = 113,
								["Moriccalas"] = 149,
								["Yoori"] = 236,
								["Durphorn the Bullheaded"] = 120,
								["Salty Futz"] = 97,
								["Barbery the Crazy Cat Lady"] = 150,
								["Racy"] = 114,
								["Corselle"] = 229,
								["Bonesaw"] = 142,
								["Ryii the Shameless"] = 223,
								["Qiana Moonshadow"] = 81,
								["Moonalli"] = 232,
								["Mumper"] = 57,
								["Miserain Starsorrow"] = 314,
								["Kokiria-Turalyon"] = 12,
								["Etubrute"] = 167,
								["Pazo Stonehoof"] = 96,
								["Maelgwyn"] = 244,
								["Tore"] = 105,
								["Roague"] = 105,
								["Northpaul"] = 59,
								["Agios Lumen"] = 112,
								["Ariok"] = 165,
								["Rakira"] = 26,
							},
							["fight_component"] = true,
							["pets"] = {
							},
							["end_time"] = 1605671036,
							["aID"] = "78883",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4707.001396,
							["monster"] = true,
							["dps_started"] = false,
							["total"] = 4707.001396,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Iron Grunt",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 21,
										["b_amt"] = 7,
										["c_dmg"] = 461,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Challe Tebrilinde"] = 247,
											["Miserain Starsorrow"] = 314,
											["Barbery the Crazy Cat Lady"] = 150,
											["High Warlord Shoju"] = 130,
											["Corselle"] = 229,
											["Bonesaw"] = 142,
											["Pazo Stonehoof"] = 96,
											["Thrend"] = 162,
											["Maelgwyn"] = 244,
											["Tore"] = 105,
											["Qiana Moonshadow"] = 81,
											["Olin Umberhide"] = 248,
											["Moonalli"] = 232,
											["Johnny Oshimo"] = 113,
											["Roague"] = 105,
											["Ryii the Shameless"] = 223,
											["Salty Futz"] = 97,
											["Plainsmender Darragh"] = 227,
											["Racy"] = 114,
											["Agios Lumen"] = 112,
											["Moriccalas"] = 149,
											["Etubrute"] = 167,
											["Northpaul"] = 59,
											["Mumper"] = 57,
											["Kengtus Pranch the Patient"] = 275,
											["Arnold Croman"] = 51,
											["Durphorn the Bullheaded"] = 120,
											["Yoori"] = 236,
											["Ariok"] = 165,
											["Rakira"] = 26,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4215,
										["MISS"] = 19,
										["a_amt"] = 0,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 467,
										["DODGE"] = 13,
										["total"] = 4676,
										["c_max"] = 32,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 10,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 42,
										["n_amt"] = 404,
										["r_amt"] = 0,
										["c_min"] = 16,
									}, -- [1]
									[157843] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Gclown"] = 9,
											["Lunaría-Turalyon"] = 10,
											["Kokiria-Turalyon"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 31,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 31,
										["c_max"] = 0,
										["id"] = 157843,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 12,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-0005349854",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605671036,
							["damage_taken"] = 6624.001396,
							["start_time"] = 1605671012,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 31,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 31,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 10.003803,
							["resource"] = 0.003803,
							["targets"] = {
								["Thornoxnun"] = 10,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["aID"] = "76-0B2B0B98",
							["passiveover"] = 0.003803,
							["total"] = 10.003803,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[196770] = {
										["total"] = 10,
										["id"] = 196770,
										["totalover"] = 0,
										["targets"] = {
											["Thornoxnun"] = 10,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 251,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003803,
							["last_event"] = 1605671036,
							["classe"] = "DEATHKNIGHT",
							["tipo"] = 3,
							["serial"] = "Player-76-0B2B0B98",
							["totalover"] = 0.003803,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 31,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thornoxnun",
							["spec"] = 251,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 2,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["last_event"] = 1605671014,
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[2479] = {
										["activedamt"] = 1,
										["id"] = 2479,
										["targets"] = {
										},
										["actived_at"] = 1605671012,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[167410] = {
										["activedamt"] = 1,
										["id"] = 167410,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0B2B0B98",
							["classe"] = "DEATHKNIGHT",
						}, -- [1]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Iron Grunt",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[157843] = 1,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3886-1265-814-78883-000034985C",
							["aID"] = "78883",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 31,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["CombatStartedAt"] = 49422.831,
				["tempo_start"] = 1605671012,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 53,
				["playing_solo"] = true,
				["totals"] = {
					4752.78532, -- [1]
					-0.005286999999999154, -- [2]
					{
						-0.01629600000000053, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 9.997676999999996,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					46, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 10,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "22:43:34",
				["cleu_timeline"] = {
				},
				["enemy"] = "Iron Grunt",
				["TotalElapsedCombatTime"] = 1.830000000001746,
				["CombatEndedAt"] = 49424.661,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 46.001353,
						}, -- [1]
					},
				},
				["end_time"] = 49424.661,
				["combat_id"] = 31,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "22:43:32",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Iron Grunt"] = 1,
				},
				["start_time"] = 49422.746,
				["TimeData"] = {
				},
				["contra"] = "Iron Grunt",
			}, -- [17]
			{
				{
					["combatId"] = 30,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005533,
							["damage_from"] = {
								["Ironmarch Warsmith"] = true,
								["Ironmarch Warcaster"] = true,
								["Ironmarch Legionnaire"] = true,
								["Ironmarch Scorcher"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["spec"] = 251,
							["aID"] = "76-0B2B0B98",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.005533,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605670970,
							["friendlyfire_total"] = 0,
							["classe"] = "DEATHKNIGHT",
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["grupo"] = true,
							["total"] = 0.005533,
							["serial"] = "Player-76-0B2B0B98",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 961.005533,
							["start_time"] = 1605670970,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.007659,
							["total"] = 560.007659,
							["damage_from"] = {
								["Irondor-Kel'Thuzad"] = true,
							},
							["targets"] = {
								["Irondor-Kel'Thuzad"] = 247,
								["Thornoxnun"] = 313,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78667",
							["raid_targets"] = {
							},
							["total_without_pet"] = 560.007659,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605670970,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Ironmarch Legionnaire",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 63,
										["targets"] = {
											["Irondor-Kel'Thuzad"] = 123,
											["Thornoxnun"] = 63,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 186,
										["n_min"] = 41,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 186,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[169426] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Irondor-Kel'Thuzad"] = 0,
											["Thornoxnun"] = 1,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1,
										["c_max"] = 0,
										["IMMUNE"] = 1,
										["id"] = 169426,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 4,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[169429] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Irondor-Kel'Thuzad"] = 3,
											["Thornoxnun"] = 3,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 169429,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 3,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[159857] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 125,
										["targets"] = {
											["Irondor-Kel'Thuzad"] = 121,
											["Thornoxnun"] = 246,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 367,
										["n_min"] = 121,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 367,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 159857,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[159847] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 159847,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 3,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3883-0-771-78667-0000348DF6",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605670947,
							["damage_taken"] = 406.007659,
							["start_time"] = 1605670957,
							["delay"] = 1605670947,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.003801,
							["total"] = 444.003801,
							["damage_from"] = {
							},
							["targets"] = {
								["Thornoxnun"] = 444,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78670",
							["raid_targets"] = {
							},
							["total_without_pet"] = 444.003801,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605670970,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Ironmarch Warcaster",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[169414] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 76,
										["targets"] = {
											["Thornoxnun"] = 444,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 444,
										["n_min"] = 73,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 444,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 169414,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 5,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3883-0-771-78670-00003497A2",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605670939,
							["damage_taken"] = 0.003801,
							["start_time"] = 1605670963,
							["delay"] = 1605670939,
							["tipo"] = 1,
						}, -- [3]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.005111,
							["total"] = 226.005111,
							["damage_from"] = {
								["Irondor-Kel'Thuzad"] = true,
							},
							["targets"] = {
								["Irondor-Kel'Thuzad"] = 51,
								["Thornoxnun"] = 175,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "78674",
							["raid_targets"] = {
							},
							["total_without_pet"] = 226.005111,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605670970,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Ironmarch Scorcher",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 51,
										["targets"] = {
											["Irondor-Kel'Thuzad"] = 51,
											["Thornoxnun"] = 175,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 42,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[176333] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 176333,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3883-0-771-78674-0000348F3B",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605670941,
							["damage_taken"] = 10.005111,
							["start_time"] = 1605670962,
							["delay"] = 1605670941,
							["tipo"] = 1,
						}, -- [4]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.004065,
							["total"] = 29.004065,
							["damage_from"] = {
							},
							["targets"] = {
								["Thornoxnun"] = 29,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["aID"] = "77653",
							["raid_targets"] = {
							},
							["total_without_pet"] = 29.004065,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1605670970,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Ironmarch Warsmith",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[159949] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 29,
										["targets"] = {
											["Thornoxnun"] = 29,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 29,
										["n_min"] = 29,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 29,
										["c_max"] = 0,
										["id"] = 159949,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["serial"] = "Creature-0-3883-0-771-77653-0000347CE2",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1605670953,
							["damage_taken"] = 0.004065,
							["start_time"] = 1605670969,
							["delay"] = 1605670953,
							["tipo"] = 1,
						}, -- [5]
					},
				}, -- [1]
				{
					["combatId"] = 30,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-0B2B0B98",
							["totalover"] = 0.005194,
							["total_without_pet"] = 19.005194,
							["total"] = 19.005194,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0B2B0B98",
							["totalabsorb"] = 0.005194,
							["last_hps"] = 0,
							["targets"] = {
								["Thornoxnun"] = 19,
							},
							["totalover_without_pet"] = 0.005194,
							["healing_taken"] = 19.005194,
							["fight_component"] = true,
							["end_time"] = 1605670970,
							["healing_from"] = {
								["Thornoxnun"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thornoxnun",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[291843] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 4,
										["targets"] = {
											["Thornoxnun"] = 19,
										},
										["n_min"] = 1,
										["counter"] = 8,
										["overheal"] = 0,
										["total"] = 19,
										["c_max"] = 0,
										["id"] = 291843,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 19,
										["n_amt"] = 8,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1605670954,
							["spec"] = 251,
							["custom"] = 0,
							["tipo"] = 2,
							["classe"] = "DEATHKNIGHT",
							["totaldenied"] = 0.005194,
							["delay"] = 1605670950,
							["last_event"] = 1605670950,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 30,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 30,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 251,
							["grupo"] = true,
							["nome"] = "Thornoxnun",
							["buff_uptime"] = 65,
							["pets"] = {
							},
							["aID"] = "76-0B2B0B98",
							["tipo"] = 4,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[101568] = {
										["activedamt"] = 1,
										["id"] = 101568,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281888] = {
										["activedamt"] = 1,
										["id"] = 281888,
										["targets"] = {
										},
										["uptime"] = 37,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[291843] = {
										["activedamt"] = 2,
										["id"] = 291843,
										["targets"] = {
										},
										["uptime"] = 16,
										["appliedamt"] = 2,
										["refreshamt"] = 3,
										["actived"] = false,
										["counter"] = 0,
									},
									[52419] = {
										["activedamt"] = 1,
										["id"] = 52419,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0B2B0B98",
							["last_event"] = 1605670970,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Ironmarch Legionnaire",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[169426] = 4,
								[169429] = 3,
								[159847] = 3,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3883-0-771-78667-00003497A2",
							["aID"] = "78667",
						}, -- [2]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Ironmarch Warcaster",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[169414] = 5,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3883-0-771-78670-0000B47E03",
							["aID"] = "78670",
						}, -- [3]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Ironmarch Scorcher",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[176333] = 2,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3883-0-771-78674-00003483CB",
							["aID"] = "78674",
						}, -- [4]
						{
							["flag_original"] = 2632,
							["tipo"] = 4,
							["nome"] = "Ironmarch Warsmith",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[159949] = 1,
							},
							["monster"] = true,
							["last_event"] = 0,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3883-0-771-77653-0000347CE2",
							["aID"] = "77653",
						}, -- [5]
					},
				}, -- [4]
				{
					["combatId"] = 30,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thornoxnun"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1605670933,
				["enemy"] = "Ironmarch Warcaster",
				["combat_counter"] = 52,
				["playing_solo"] = true,
				["totals"] = {
					1258.804158999998, -- [1]
					18.991418, -- [2]
					{
						0, -- [1]
						[0] = -0.006244,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 49380.948,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "22:42:14",
				["end_time"] = 49380.948,
				["totals_grupo"] = {
					0, -- [1]
					19, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 30,
				["TotalElapsedCombatTime"] = 49380.948,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thornoxnun"] = 19.005194,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thornoxnun"] = 0.005533,
						}, -- [1]
					},
				},
				["frags"] = {
					["Ironmarch Grunt"] = 8,
				},
				["data_fim"] = "22:42:51",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 49344.134,
				["contra"] = "Ironmarch Warcaster",
				["TimeData"] = {
				},
			}, -- [18]
		},
	},
	["combat_counter"] = 69,
	["character_data"] = {
		["logons"] = 10,
	},
	["tabela_instancias"] = {
	},
	["coach"] = {
		["enabled"] = false,
	},
	["local_instances_config"] = {
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[2] = 2,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 60.2529296875,
					["x"] = -1016.867584228516,
					["w"] = 264.3997497558594,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[4] = 1,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -80.34707641601562,
					["x"] = -1016.867492675781,
					["w"] = 264.3999328613281,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [2]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["isLocked"] = false,
			["is_open"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -147.2000732421875,
					["x"] = -238.400390625,
					["w"] = 320.0000305175781,
					["h"] = 130.0000610351563,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [3]
	},
	["cached_talents"] = {
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["active_profile"] = "Default",
	["last_realversion"] = 144,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["plugin_database"] = {
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["enabled"] = true,
			["dps"] = 0,
			["level"] = 1,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["last_section_selected"] = "main",
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				["last_selected"] = 1,
			},
			["options"] = {
				["show_method"] = 4,
				["auto_create"] = true,
				["window_scale"] = 1,
			},
		},
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
		["DETAILS_PLUGIN_STREAM_OVERLAY"] = {
			["font_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["is_first_run"] = true,
			["arrow_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				0.5, -- [4]
			},
			["main_frame_size"] = {
				300, -- [1]
				500.000030517578, -- [2]
			},
			["minimap"] = {
				["minimapPos"] = 160,
				["radius"] = 160,
				["hide"] = false,
			},
			["arrow_anchor_x"] = 0,
			["row_texture"] = "Details Serenity",
			["arrow_anchor_y"] = 0,
			["main_frame_locked"] = false,
			["row_color"] = {
				0.1, -- [1]
				0.1, -- [2]
				0.1, -- [3]
				0.4, -- [4]
			},
			["enabled"] = false,
			["arrow_size"] = 10,
			["font_size"] = 10,
			["row_spacement"] = 21,
			["main_frame_color"] = {
				0, -- [1]
				0, -- [2]
				0, -- [3]
				0.2, -- [4]
			},
			["main_frame_strata"] = "LOW",
			["arrow_texture"] = "Interface\\CHATFRAME\\ChatFrameExpandArrow",
			["use_spark"] = true,
			["y"] = 5.340576171875e-05,
			["x"] = 9.1552734375e-05,
			["font_face"] = "Friz Quadrata TT",
			["per_second"] = {
				["enabled"] = false,
				["point"] = "CENTER",
				["scale"] = 1,
				["font_shadow"] = true,
				["y"] = 0,
				["x"] = 0,
				["attribute_type"] = 1,
				["update_speed"] = 0.05,
				["size"] = 32,
			},
			["author"] = "Details! Team",
			["point"] = "CENTER",
			["row_height"] = 20,
			["scale"] = 1,
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["author"] = "Details! Team",
			["realm_history"] = {
			},
			["realm_lastamt"] = 0,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
	},
	["on_death_menu"] = true,
	["ignore_nicktag"] = false,
	["nick_tag_cache"] = {
		["nextreset"] = 1606177083,
		["last_version"] = 12,
	},
	["last_day"] = "17",
	["last_version"] = "v9.0.1.7950",
	["combat_id"] = 47,
	["savedStyles"] = {
	},
	["last_instance_time"] = 0,
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.114981,
					["damage_from"] = {
						["Bleeding Hollow Savage"] = true,
						["Ironmarch Warcaster"] = true,
						["Ironmarch Legionnaire"] = true,
						["Snickerfang Hyena"] = true,
						["Warsong Commander"] = true,
						["Iron Grunt"] = true,
						["Ironmarch Warsmith"] = true,
						["Ironmarch Scorcher"] = true,
					},
					["targets"] = {
						["Training Dummy"] = 10267,
						["Iron Grunt"] = 3879,
						["Snickerfang Hyena"] = 513,
						["Bleeding Hollow Hatchet"] = 265,
						["Bleeding Hollow Savage"] = 2164,
						["Tormented Soul"] = 197,
						["Warsong Commander"] = 595,
					},
					["end_time"] = 1604970833,
					["pets"] = {
					},
					["delay"] = 0,
					["classe"] = "DEATHKNIGHT",
					["raid_targets"] = {
					},
					["total_without_pet"] = 17880.114981,
					["on_hold"] = false,
					["damage_taken"] = 1466.114981,
					["dps_started"] = false,
					["total"] = 17880.114981,
					["friendlyfire_total"] = 0,
					["aID"] = "76-0B2B0B98",
					["nome"] = "Thornoxnun",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 9,
								["b_amt"] = 0,
								["c_dmg"] = 878,
								["g_amt"] = 0,
								["n_max"] = 58,
								["targets"] = {
									["Training Dummy"] = 2194,
									["Iron Grunt"] = 923,
									["Snickerfang Hyena"] = 140,
									["Bleeding Hollow Hatchet"] = 99,
									["Bleeding Hollow Savage"] = 604,
									["Tormented Soul"] = 53,
									["Warsong Commander"] = 251,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3386,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 78,
								["total"] = 4264,
								["c_max"] = 105,
								["id"] = 1,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 69,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[325461] = {
								["c_amt"] = 8,
								["b_amt"] = 0,
								["c_dmg"] = 1142,
								["g_amt"] = 0,
								["n_max"] = 84,
								["targets"] = {
									["Training Dummy"] = 1355,
									["Iron Grunt"] = 1042,
									["Snickerfang Hyena"] = 135,
									["Bleeding Hollow Hatchet"] = 146,
									["Bleeding Hollow Savage"] = 571,
									["Tormented Soul"] = 144,
									["Warsong Commander"] = 146,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2397,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 42,
								["total"] = 3539,
								["c_max"] = 149,
								["id"] = 325461,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 34,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[55095] = {
								["c_amt"] = 26,
								["b_amt"] = 0,
								["c_dmg"] = 1505,
								["g_amt"] = 0,
								["n_max"] = 34,
								["targets"] = {
									["Training Dummy"] = 4396,
									["Iron Grunt"] = 834,
									["Snickerfang Hyena"] = 85,
									["Bleeding Hollow Savage"] = 240,
									["Warsong Commander"] = 60,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 4110,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 172,
								["total"] = 5615,
								["c_max"] = 68,
								["id"] = 55095,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 146,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[49184] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 104,
								["g_amt"] = 0,
								["n_max"] = 20,
								["targets"] = {
									["Training Dummy"] = 256,
									["Iron Grunt"] = 183,
									["Snickerfang Hyena"] = 19,
									["Bleeding Hollow Hatchet"] = 20,
									["Bleeding Hollow Savage"] = 119,
									["Warsong Commander"] = 20,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 513,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 35,
								["total"] = 617,
								["c_max"] = 41,
								["id"] = 49184,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 32,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[325464] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 674,
								["g_amt"] = 0,
								["n_max"] = 81,
								["targets"] = {
									["Training Dummy"] = 1478,
									["Iron Grunt"] = 717,
									["Snickerfang Hyena"] = 134,
									["Bleeding Hollow Savage"] = 566,
									["Warsong Commander"] = 70,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2291,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 38,
								["total"] = 2965,
								["c_max"] = 140,
								["id"] = 325464,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 33,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[196771] = {
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 65,
								["g_amt"] = 0,
								["n_max"] = 9,
								["targets"] = {
									["Iron Grunt"] = 180,
									["Bleeding Hollow Savage"] = 64,
									["Warsong Commander"] = 48,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 227,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 32,
								["total"] = 292,
								["c_max"] = 17,
								["id"] = 196771,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 28,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[52212] = {
								["c_amt"] = 18,
								["b_amt"] = 0,
								["c_dmg"] = 144,
								["g_amt"] = 0,
								["n_max"] = 4,
								["targets"] = {
									["Training Dummy"] = 588,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 444,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 132,
								["total"] = 588,
								["c_max"] = 8,
								["id"] = 52212,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 114,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["grupo"] = true,
					["spec"] = 251,
					["last_dps"] = 0,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1604970542,
					["serial"] = "Player-76-0B2B0B98",
					["tipo"] = 1,
				}, -- [1]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.007763,
					["damage_from"] = {
						["Synical"] = true,
						["Thornoxnun"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["last_dps"] = 0,
					["classe"] = "UNKNOW",
					["aID"] = "126781",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.007763,
					["delay"] = 0,
					["fight_component"] = true,
					["end_time"] = 1604970833,
					["friendlyfire_total"] = 0,
					["damage_taken"] = 10449.007763,
					["nome"] = "Training Dummy",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 2,
					},
					["tipo"] = 1,
					["on_hold"] = false,
					["total"] = 0.007763,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1604970830,
					["serial"] = "Creature-0-3134-1643-28380-126781-0000A72E32",
					["dps_started"] = false,
				}, -- [2]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.012139,
					["damage_from"] = {
						["Thornoxnun"] = true,
					},
					["targets"] = {
						["Thornoxnun"] = 95,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 95.01213899999999,
					["classe"] = "UNKNOW",
					["end_time"] = 1605670916,
					["dps_started"] = false,
					["total"] = 95.01213899999999,
					["fight_component"] = true,
					["aID"] = "5985",
					["nome"] = "Snickerfang Hyena",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 28,
								["targets"] = {
									["Thornoxnun"] = 53,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 53,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 53,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[82797] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 42,
								["targets"] = {
									["Thornoxnun"] = 42,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 42,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 42,
								["c_max"] = 0,
								["id"] = 82797,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[3149] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 3149,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3883-0-771-5985-0000349754",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670906,
					["delay"] = 0,
					["damage_taken"] = 513.012139,
				}, -- [3]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.006573000000000001,
					["damage_from"] = {
					},
					["targets"] = {
						["Thornoxnun"] = 444,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 444.006573,
					["classe"] = "UNKNOW",
					["end_time"] = 1605670971,
					["dps_started"] = false,
					["total"] = 444.006573,
					["fight_component"] = true,
					["aID"] = "78670",
					["nome"] = "Ironmarch Warcaster",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							[169414] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 76,
								["targets"] = {
									["Thornoxnun"] = 444,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 444,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 444,
								["c_max"] = 0,
								["id"] = 169414,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 5,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3883-0-771-78670-00003497A2",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670961,
					["delay"] = 0,
					["damage_taken"] = 0.006573000000000001,
				}, -- [4]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.016114,
					["damage_from"] = {
						["Irondor-Kel'Thuzad"] = true,
					},
					["targets"] = {
						["Irondor-Kel'Thuzad"] = 247,
						["Thornoxnun"] = 313,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 560.016114,
					["classe"] = "UNKNOW",
					["end_time"] = 1605670971,
					["dps_started"] = false,
					["total"] = 560.016114,
					["fight_component"] = true,
					["aID"] = "78667",
					["nome"] = "Ironmarch Legionnaire",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 63,
								["targets"] = {
									["Irondor-Kel'Thuzad"] = 123,
									["Thornoxnun"] = 63,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 186,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 186,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[169429] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Irondor-Kel'Thuzad"] = 3,
									["Thornoxnun"] = 3,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 6,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 6,
								["c_max"] = 0,
								["id"] = 169429,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 3,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[159847] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 159847,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 3,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[159857] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 125,
								["targets"] = {
									["Irondor-Kel'Thuzad"] = 121,
									["Thornoxnun"] = 246,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 367,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 367,
								["c_max"] = 0,
								["id"] = 159857,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[169426] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 1,
								["targets"] = {
									["Irondor-Kel'Thuzad"] = 0,
									["Thornoxnun"] = 1,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 1,
								["c_max"] = 0,
								["IMMUNE"] = 1,
								["id"] = 169426,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3883-0-771-78667-0000348DF6",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670955,
					["delay"] = 0,
					["damage_taken"] = 406.016114,
				}, -- [5]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.012216,
					["damage_from"] = {
						["Irondor-Kel'Thuzad"] = true,
					},
					["targets"] = {
						["Irondor-Kel'Thuzad"] = 51,
						["Thornoxnun"] = 175,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 226.012216,
					["classe"] = "UNKNOW",
					["end_time"] = 1605670971,
					["dps_started"] = false,
					["total"] = 226.012216,
					["fight_component"] = true,
					["aID"] = "78674",
					["nome"] = "Ironmarch Scorcher",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 51,
								["targets"] = {
									["Irondor-Kel'Thuzad"] = 51,
									["Thornoxnun"] = 175,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 226,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 226,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[176333] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 176333,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3883-0-771-78674-0000348F3B",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670960,
					["delay"] = 0,
					["damage_taken"] = 10.012216,
				}, -- [6]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.007778,
					["damage_from"] = {
					},
					["targets"] = {
						["Thornoxnun"] = 29,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 29.007778,
					["classe"] = "UNKNOW",
					["end_time"] = 1605670971,
					["dps_started"] = false,
					["total"] = 29.007778,
					["fight_component"] = true,
					["aID"] = "77653",
					["nome"] = "Ironmarch Warsmith",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							[159949] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 29,
								["targets"] = {
									["Thornoxnun"] = 29,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 29,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 29,
								["c_max"] = 0,
								["id"] = 159949,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3883-0-771-77653-0000347CE2",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670967,
					["delay"] = 0,
					["damage_taken"] = 0.007778,
				}, -- [7]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.042233,
					["on_hold"] = false,
					["damage_from"] = {
						["Gravywave-Stormrage"] = true,
						["Lunaría-Turalyon"] = true,
						["Corselle"] = true,
						["Starcaller Astrylian"] = true,
						["Bonesaw"] = true,
						["Ed"] = true,
						["Thrend"] = true,
						["Mumper"] = true,
						["Musclemago-Arygos"] = true,
						["Astraphobia-Llane"] = true,
						["Rakira"] = true,
						["Rainiara the Kingslayer"] = true,
						["Palios-Azshara"] = true,
						["Izzad-Arygos"] = true,
						["Tengery"] = true,
						["Holyhoe-Arygos"] = true,
						["Moriccalas"] = true,
						["Etubrute"] = true,
						["Zexu"] = true,
						["Arnold Croman"] = true,
						["Alya"] = true,
						["Thornoxnun"] = true,
						["Kung Din"] = true,
						["Agios Lumen"] = true,
						["Irondor-Kel'Thuzad"] = true,
						["Durphorn the Bullheaded"] = true,
					},
					["targets"] = {
						["Challe Tebrilinde"] = 843,
						["Miserain Starsorrow"] = 1318,
						["Lunaría-Turalyon"] = 10,
						["High Warlord Shoju"] = 331,
						["Arnold Croman"] = 644,
						["Thrend"] = 599,
						["Elaynea Welton the Wind and Sea"] = 254,
						["Olin Umberhide"] = 684,
						["Kengtus Pranch the Patient"] = 803,
						["Johnny Oshimo"] = 662,
						["Izzad-Arygos"] = 10,
						["Moriccalas"] = 428,
						["Nevo"] = 395,
						["Yoori"] = 967,
						["Irondor-Kel'Thuzad"] = 69,
						["Durphorn the Bullheaded"] = 415,
						["Lupas"] = 288,
						["Salty Futz"] = 473,
						["Barbery the Crazy Cat Lady"] = 466,
						["Racy"] = 166,
						["Corselle"] = 984,
						["Starcaller Astrylian"] = 443,
						["Bonesaw"] = 1194,
						["Ryii the Shameless"] = 944,
						["Qiana Moonshadow"] = 393,
						["Tengery"] = 68,
						["Musclemago-Arygos"] = 15,
						["Moonalli"] = 492,
						["Monishot"] = 294,
						["Gravywave-Stormrage"] = 12,
						["Turkina"] = 344,
						["Zexu"] = 15,
						["Malothas"] = 688,
						["Mumper"] = 174,
						["Thornoxnun"] = 120,
						["Plainsmender Darragh"] = 764,
						["Etubrute"] = 654,
						["Roague"] = 337,
						["Pazo Stonehoof"] = 551,
						["Maelgwyn"] = 540,
						["Tore"] = 414,
						["Rakira"] = 8,
						["Agios Lumen"] = 694,
						["Ariok"] = 635,
						["Northpaul"] = 533,
					},
					["pets"] = {
					},
					["end_time"] = 1605671014,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 21135.042233,
					["fight_component"] = true,
					["dps_started"] = false,
					["total"] = 21135.042233,
					["aID"] = "78883",
					["monster"] = true,
					["nome"] = "Iron Grunt",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 100,
								["b_amt"] = 12,
								["c_dmg"] = 2128,
								["g_amt"] = 0,
								["n_max"] = 21,
								["targets"] = {
									["Challe Tebrilinde"] = 843,
									["Miserain Starsorrow"] = 1318,
									["High Warlord Shoju"] = 331,
									["Arnold Croman"] = 644,
									["Thrend"] = 599,
									["Elaynea Welton the Wind and Sea"] = 254,
									["Olin Umberhide"] = 684,
									["Kengtus Pranch the Patient"] = 803,
									["Johnny Oshimo"] = 662,
									["Izzad-Arygos"] = 10,
									["Moriccalas"] = 428,
									["Nevo"] = 395,
									["Yoori"] = 967,
									["Irondor-Kel'Thuzad"] = 69,
									["Durphorn the Bullheaded"] = 415,
									["Lupas"] = 288,
									["Salty Futz"] = 473,
									["Barbery the Crazy Cat Lady"] = 466,
									["Racy"] = 166,
									["Corselle"] = 984,
									["Starcaller Astrylian"] = 443,
									["Bonesaw"] = 1194,
									["Ryii the Shameless"] = 944,
									["Mumper"] = 174,
									["Musclemago-Arygos"] = 15,
									["Moonalli"] = 492,
									["Tengery"] = 44,
									["Monishot"] = 294,
									["Gravywave-Stormrage"] = 12,
									["Turkina"] = 344,
									["Malothas"] = 688,
									["Thornoxnun"] = 77,
									["Plainsmender Darragh"] = 764,
									["Roague"] = 337,
									["Etubrute"] = 654,
									["Pazo Stonehoof"] = 551,
									["Maelgwyn"] = 540,
									["Tore"] = 414,
									["Qiana Moonshadow"] = 393,
									["Rakira"] = 8,
									["Agios Lumen"] = 694,
									["Ariok"] = 635,
									["Northpaul"] = 533,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 18915,
								["a_amt"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2040,
								["DODGE"] = 62,
								["total"] = 21043,
								["c_max"] = 42,
								["MISS"] = 63,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 44,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 92,
								["n_amt"] = 1771,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[157843] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 18,
								["targets"] = {
									["Zexu"] = 15,
									["Tengery"] = 24,
									["Lunaría-Turalyon"] = 10,
									["Thornoxnun"] = 43,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 92,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 92,
								["c_max"] = 0,
								["MISS"] = 2,
								["id"] = 157843,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 9,
								["b_dmg"] = 0,
								["n_amt"] = 7,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3886-1265-814-78883-0005349854",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670897,
					["delay"] = 0,
					["damage_taken"] = 36725.042233,
				}, -- [8]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.027038,
					["on_hold"] = false,
					["damage_from"] = {
						["Gclown"] = true,
						["Tengery"] = true,
						["Thornoxnun"] = true,
					},
					["targets"] = {
						["Gclown"] = 49,
						["Tengery"] = 17,
						["Thornoxnun"] = 34,
					},
					["classe"] = "UNKNOW",
					["pets"] = {
					},
					["fight_component"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 100.027038,
					["end_time"] = 1605671014,
					["dps_started"] = false,
					["total"] = 100.027038,
					["aID"] = "83538",
					["monster"] = true,
					["nome"] = "Warsong Commander",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 13,
								["targets"] = {
									["Gclown"] = 30,
									["Tengery"] = 17,
									["Thornoxnun"] = 34,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 81,
								["a_amt"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["DODGE"] = 1,
								["total"] = 81,
								["c_max"] = 0,
								["MISS"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 8,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[80149] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 14,
								["targets"] = {
									["Gclown"] = 14,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 14,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 14,
								["c_max"] = 0,
								["id"] = 80149,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[166762] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5,
								["targets"] = {
									["Gclown"] = 5,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 5,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 5,
								["c_max"] = 0,
								["id"] = 166762,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3886-1265-814-83538-00003495DF",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605670991,
					["delay"] = 0,
					["damage_taken"] = 1230.027038,
				}, -- [9]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.008851000000000001,
					["damage_from"] = {
						["Zexu"] = true,
						["Thornoxnun"] = true,
					},
					["targets"] = {
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.008851000000000001,
					["classe"] = "UNKNOW",
					["end_time"] = 1605671255,
					["dps_started"] = false,
					["total"] = 0.008851000000000001,
					["fight_component"] = true,
					["aID"] = "82647",
					["nome"] = "Tormented Soul",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							[167951] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 167951,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3886-1265-814-82647-0001B49944",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605671252,
					["delay"] = 0,
					["damage_taken"] = 229.008851,
				}, -- [10]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.030698,
					["on_hold"] = false,
					["damage_from"] = {
						["Palios-Azshara"] = true,
						["Zexu"] = true,
						["Bleeding Hollow Savage"] = true,
						["Rezêc-Dalaran"] = true,
						["Rhodana-Terokkar"] = true,
						["Thornoxnun"] = true,
					},
					["targets"] = {
						["Palios-Azshara"] = 10,
						["Zexu"] = 221,
						["Bleeding Hollow Savage"] = 80,
						["Rezêc-Dalaran"] = 54,
						["Thornoxnun"] = 256,
					},
					["end_time"] = 1605671383,
					["pets"] = {
					},
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 621.030698,
					["fight_component"] = true,
					["dps_started"] = false,
					["total"] = 621.030698,
					["aID"] = "78507",
					["monster"] = true,
					["nome"] = "Bleeding Hollow Savage",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 29,
								["g_amt"] = 0,
								["n_max"] = 20,
								["targets"] = {
									["Rezêc-Dalaran"] = 54,
									["Palios-Azshara"] = 10,
									["Zexu"] = 221,
									["Thornoxnun"] = 256,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 512,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 37,
								["MISS"] = 1,
								["total"] = 541,
								["c_max"] = 29,
								["DODGE"] = 3,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 32,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[163586] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 8,
								["targets"] = {
									["Bleeding Hollow Savage"] = 80,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 80,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 10,
								["total"] = 80,
								["c_max"] = 0,
								["id"] = 163586,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 11,
								["b_dmg"] = 0,
								["n_amt"] = 10,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3886-1265-814-78507-00003499CC",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605671344,
					["delay"] = 0,
					["damage_taken"] = 4715.030698,
				}, -- [11]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.024046,
					["on_hold"] = false,
					["damage_from"] = {
						["Palios-Azshara"] = true,
						["Thornoxnun"] = true,
						["Kokiria-Turalyon"] = true,
					},
					["targets"] = {
						["Kokiria-Turalyon"] = 31,
						["Palios-Azshara"] = 27,
					},
					["classe"] = "UNKNOW",
					["pets"] = {
					},
					["fight_component"] = true,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 58.024046,
					["end_time"] = 1605671397,
					["dps_started"] = false,
					["total"] = 58.024046,
					["aID"] = "78510",
					["monster"] = true,
					["nome"] = "Bleeding Hollow Hatchet",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 10,
								["targets"] = {
									["Kokiria-Turalyon"] = 13,
									["Palios-Azshara"] = 27,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 40,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["DODGE"] = 1,
								["total"] = 40,
								["c_max"] = 0,
								["MISS"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 13,
								["m_crit"] = 0,
								["a_amt"] = 2,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[163598] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 18,
								["targets"] = {
									["Kokiria-Turalyon"] = 18,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 18,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 18,
								["c_max"] = 0,
								["id"] = 163598,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 18,
								["m_crit"] = 0,
								["a_amt"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-3886-1265-814-78510-00003499D6",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605671385,
					["delay"] = 0,
					["damage_taken"] = 984.024046,
				}, -- [12]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["targets_overheal"] = {
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["classe"] = "DEATHKNIGHT",
					["totalover"] = 0.028212,
					["total_without_pet"] = 24.028212,
					["total"] = 24.028212,
					["targets_absorbs"] = {
					},
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-0B2B0B98",
					["totalabsorb"] = 0.028212,
					["last_hps"] = 0,
					["targets"] = {
						["Thornoxnun"] = 24,
					},
					["totalover_without_pet"] = 0.028212,
					["healing_taken"] = 24.028212,
					["fight_component"] = true,
					["end_time"] = 1605670971,
					["start_time"] = 1605670944,
					["healing_from"] = {
						["Thornoxnun"] = true,
					},
					["nome"] = "Thornoxnun",
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[291843] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 4,
								["targets"] = {
									["Thornoxnun"] = 24,
								},
								["n_min"] = 0,
								["counter"] = 13,
								["overheal"] = 0,
								["total"] = 24,
								["c_max"] = 0,
								["id"] = 291843,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 24,
								["n_amt"] = 13,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["grupo"] = true,
					["heal_enemy_amt"] = 0,
					["aID"] = "76-0B2B0B98",
					["custom"] = 0,
					["tipo"] = 2,
					["spec"] = 251,
					["totaldenied"] = 0.028212,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 1000.080699,
					["resource"] = 0.085399,
					["targets"] = {
						["Thornoxnun"] = 550,
					},
					["pets"] = {
					},
					["powertype"] = 6,
					["aID"] = "76-0B2B0B98",
					["passiveover"] = 0.006432,
					["fight_component"] = true,
					["total"] = 1000.080699,
					["nome"] = "Thornoxnun",
					["spells"] = {
						["_ActorTable"] = {
							[43265] = {
								["total"] = 40,
								["id"] = 43265,
								["totalover"] = 0,
								["targets"] = {
									["Thornoxnun"] = 0,
								},
								["counter"] = 4,
							},
							[196770] = {
								["total"] = 20,
								["id"] = 196770,
								["totalover"] = 0,
								["targets"] = {
									["Thornoxnun"] = 20,
								},
								["counter"] = 2,
							},
							[49020] = {
								["total"] = 800,
								["id"] = 49020,
								["totalover"] = 0,
								["targets"] = {
									["Thornoxnun"] = 440,
								},
								["counter"] = 40,
							},
							[49184] = {
								["total"] = 140,
								["id"] = 49184,
								["totalover"] = 0,
								["targets"] = {
									["Thornoxnun"] = 90,
								},
								["counter"] = 14,
							},
						},
						["tipo"] = 7,
					},
					["grupo"] = true,
					["spec"] = 251,
					["classe"] = "DEATHKNIGHT",
					["last_event"] = 0,
					["tipo"] = 3,
					["alternatepower"] = 0.080699,
					["flag_original"] = 1297,
					["serial"] = "Player-76-0B2B0B98",
					["totalover"] = 0.006432,
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["debuff_uptime_spells"] = {
						["_ActorTable"] = {
							[55095] = {
								["actived_at"] = 8028355900,
								["counter"] = 0,
								["activedamt"] = -4,
								["appliedamt"] = 22,
								["id"] = 55095,
								["uptime"] = 197,
								["targets"] = {
								},
								["refreshamt"] = 5,
							},
							[211793] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 10,
								["id"] = 211793,
								["uptime"] = 35,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["buff_uptime"] = 507,
					["aID"] = "76-0B2B0B98",
					["buff_uptime_spells"] = {
						["_ActorTable"] = {
							[52424] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 52424,
								["uptime"] = 11,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[291843] = {
								["refreshamt"] = 3,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 291843,
								["uptime"] = 29,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[196770] = {
								["refreshamt"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 196770,
								["uptime"] = 27,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[52419] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 52419,
								["uptime"] = 10,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[101568] = {
								["refreshamt"] = 5,
								["activedamt"] = 11,
								["appliedamt"] = 11,
								["id"] = 101568,
								["uptime"] = 50,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[281888] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 281888,
								["uptime"] = 37,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[188290] = {
								["counter"] = 0,
								["activedamt"] = 4,
								["appliedamt"] = 4,
								["id"] = 188290,
								["uptime"] = 36,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[194879] = {
								["counter"] = 0,
								["activedamt"] = 27,
								["appliedamt"] = 27,
								["id"] = 194879,
								["uptime"] = 148,
								["targets"] = {
								},
								["refreshamt"] = 13,
							},
							[167410] = {
								["refreshamt"] = 0,
								["activedamt"] = 17,
								["appliedamt"] = 17,
								["id"] = 167410,
								["uptime"] = 159,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[2479] = {
								["refreshamt"] = 0,
								["appliedamt"] = 1,
								["activedamt"] = 1,
								["uptime"] = 0,
								["id"] = 2479,
								["actived_at"] = 1605671012,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["fight_component"] = true,
					["debuff_uptime"] = 232,
					["nome"] = "Thornoxnun",
					["spec"] = 251,
					["grupo"] = true,
					["spell_cast"] = {
						[49020] = 40,
						[49143] = 37,
						[43265] = 4,
						[49184] = 14,
						[196770] = 2,
					},
					["classe"] = "DEATHKNIGHT",
					["tipo"] = 4,
					["last_event"] = 0,
					["pets"] = {
					},
					["buff_uptime_targets"] = {
					},
					["serial"] = "Player-76-0B2B0B98",
					["debuff_uptime_targets"] = {
					},
				}, -- [1]
				{
					["monster"] = true,
					["tipo"] = 4,
					["spell_cast"] = {
						[82797] = 1,
						[3149] = 1,
					},
					["nome"] = "Snickerfang Hyena",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 68168,
					["classe"] = "UNKNOW",
					["aID"] = "5985",
					["serial"] = "Creature-0-3883-0-771-5985-0000349754",
					["last_event"] = 0,
				}, -- [2]
				{
					["monster"] = true,
					["tipo"] = 4,
					["spell_cast"] = {
						[169429] = 3,
						[159847] = 3,
						[169426] = 4,
					},
					["nome"] = "Ironmarch Legionnaire",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 2632,
					["classe"] = "UNKNOW",
					["aID"] = "78667",
					["serial"] = "Creature-0-3883-0-771-78667-00003497A2",
					["last_event"] = 0,
				}, -- [3]
				{
					["monster"] = true,
					["tipo"] = 4,
					["spell_cast"] = {
						[169414] = 5,
					},
					["nome"] = "Ironmarch Warcaster",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 2632,
					["classe"] = "UNKNOW",
					["aID"] = "78670",
					["serial"] = "Creature-0-3883-0-771-78670-0000B47E03",
					["last_event"] = 0,
				}, -- [4]
				{
					["monster"] = true,
					["tipo"] = 4,
					["spell_cast"] = {
						[176333] = 2,
					},
					["nome"] = "Ironmarch Scorcher",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 2632,
					["classe"] = "UNKNOW",
					["aID"] = "78674",
					["serial"] = "Creature-0-3883-0-771-78674-00003483CB",
					["last_event"] = 0,
				}, -- [5]
				{
					["monster"] = true,
					["tipo"] = 4,
					["spell_cast"] = {
						[159949] = 1,
					},
					["nome"] = "Ironmarch Warsmith",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 2632,
					["classe"] = "UNKNOW",
					["aID"] = "77653",
					["serial"] = "Creature-0-3883-0-771-77653-0000347CE2",
					["last_event"] = 0,
				}, -- [6]
				{
					["flag_original"] = 2632,
					["monster"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Iron Grunt",
					["pets"] = {
					},
					["tipo"] = 4,
					["spell_cast"] = {
						[157843] = 9,
					},
					["last_event"] = 0,
					["fight_component"] = true,
					["serial"] = "Creature-0-3886-1265-814-78883-000034985C",
					["aID"] = "78883",
				}, -- [7]
				{
					["monster"] = true,
					["tipo"] = 4,
					["spell_cast"] = {
						[167951] = 1,
					},
					["nome"] = "Tormented Soul",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 68168,
					["classe"] = "UNKNOW",
					["aID"] = "82647",
					["serial"] = "Creature-0-3886-1265-814-82647-0001B49944",
					["last_event"] = 0,
				}, -- [8]
				{
					["flag_original"] = 68168,
					["monster"] = true,
					["nome"] = "Bleeding Hollow Savage",
					["aID"] = "78507",
					["pets"] = {
					},
					["spell_cast"] = {
						[163586] = 11,
					},
					["fight_component"] = true,
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["serial"] = "Creature-0-3886-1265-814-78507-00003499CC",
					["tipo"] = 4,
				}, -- [9]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1604970687,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["cleu_timeline"] = {
		},
		["combat_counter"] = 48,
		["totals"] = {
			52101.26519699998, -- [1]
			1590.026197000003, -- [2]
			{
				0.01128199999999673, -- [1]
				[0] = -0.006163000000005192,
				["alternatepower"] = 0,
				[6] = 1000.060993,
				[3] = 22.013048,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "20:11:28",
		["end_time"] = 49848.351,
		["totals_grupo"] = {
			17880.111285, -- [1]
			24.024698, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 1000.074267,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["segments_added"] = {
			{
				["elapsed"] = 3.745999999999185,
				["type"] = 0,
				["name"] = "Bleeding Hollow Hatchet",
				["clock"] = "22:50:34",
			}, -- [1]
			{
				["elapsed"] = 7.710999999995693,
				["type"] = 0,
				["name"] = "Bleeding Hollow Savage",
				["clock"] = "22:50:22",
			}, -- [2]
			{
				["elapsed"] = 6.222000000001572,
				["type"] = 0,
				["name"] = "Bleeding Hollow Savage",
				["clock"] = "22:50:14",
			}, -- [3]
			{
				["elapsed"] = 10.23800000000483,
				["type"] = 0,
				["name"] = "Bleeding Hollow Savage",
				["clock"] = "22:49:59",
			}, -- [4]
			{
				["elapsed"] = 7.522000000004482,
				["type"] = 0,
				["name"] = "Bleeding Hollow Savage",
				["clock"] = "22:49:49",
			}, -- [5]
			{
				["elapsed"] = 5.682000000000699,
				["type"] = 0,
				["name"] = "Bleeding Hollow Savage",
				["clock"] = "22:49:37",
			}, -- [6]
			{
				["elapsed"] = 3.57499999999709,
				["type"] = 0,
				["name"] = "Tormented Soul",
				["clock"] = "22:47:31",
			}, -- [7]
			{
				["elapsed"] = 1.014999999999418,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:47:30",
			}, -- [8]
			{
				["elapsed"] = 9.988000000004831,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:47:09",
			}, -- [9]
			{
				["elapsed"] = 6.826000000000931,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:46:26",
			}, -- [10]
			{
				["elapsed"] = 9.540000000000873,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:46:11",
			}, -- [11]
			{
				["elapsed"] = 3.894999999996799,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:46:05",
			}, -- [12]
			{
				["elapsed"] = 8.578000000001339,
				["type"] = 0,
				["name"] = "Warsong Commander",
				["clock"] = "22:45:52",
			}, -- [13]
			{
				["elapsed"] = 56.25300000000425,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:44:24",
			}, -- [14]
			{
				["elapsed"] = 14.1730000000025,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:44:01",
			}, -- [15]
			{
				["elapsed"] = 1.937000000005355,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:43:57",
			}, -- [16]
			{
				["elapsed"] = 1.915000000000873,
				["type"] = 0,
				["name"] = "Iron Grunt",
				["clock"] = "22:43:32",
			}, -- [17]
			{
				["elapsed"] = 36.81400000000576,
				["type"] = 0,
				["name"] = "Ironmarch Warcaster",
				["clock"] = "22:42:14",
			}, -- [18]
			{
				["elapsed"] = 8.362999999997555,
				["type"] = 0,
				["name"] = "Snickerfang Hyena",
				["clock"] = "22:41:47",
			}, -- [19]
			{
				["elapsed"] = 145.4069999999992,
				["type"] = 0,
				["name"] = "Training Dummy",
				["clock"] = "20:11:28",
			}, -- [20]
		},
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = "22:50:38",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["frags"] = {
		},
		["start_time"] = 49498.95099999998,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["cleu_events"] = {
			["n"] = 1,
		},
	},
	["force_font_outline"] = "",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-99-0D07FE11"] = 72,
		["Player-77-0E16879F"] = 66,
		["Player-60-0E168BE9"] = 65,
		["Player-76-0B2E9164"] = 62,
		["Player-76-0A06C5E2"] = 577,
		["Player-3683-0AC637CF"] = 577,
		["Player-60-0E16768D"] = 71,
		["Player-76-0B2B0B98"] = 251,
	},
}
