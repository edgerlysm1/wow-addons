Version: 1
Frame: AllTheThings-Window-CurrentInstance
FrameLevel: 1
Anchor: TOPLEFT
X: 4
Y: -11
W: 262
H: 92
Frame: XPerl_RaidMonitor_Anchor
FrameLevel: 1
Anchor: CENTER
X: -20
Y: -10
W: 200
H: 80
Frame: XPerl_AggroAnchor
FrameLevel: 1
Anchor: TOP
X: 81
Y: -185
W: 32
H: 32
Frame: XPerl_Assists_FrameAnchor
FrameLevel: 1
Anchor: CENTER
X: -20
Y: -10
W: 330
H: 124
Frame: XPerl_MTList_Anchor
FrameLevel: 1
Anchor: CENTER
X: -20
Y: 25
W: 206
H: 11
Frame: XPerl_Raid_Title12
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title11
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title10
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title9
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title8
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title7
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title6
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -192
W: 80
H: 10
Frame: XPerl_Raid_Title5
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title4
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title3
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title2
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_Raid_Title1
FrameLevel: 1
Anchor: CENTER
X: -189
Y: -221
W: 80
H: 10
Frame: XPerl_RosterTextAnchor
FrameLevel: 1
Anchor: CENTER
X: -20
Y: -21
W: 350
H: 250
Frame: XPerl_CheckAnchor
FrameLevel: 1
Anchor: CENTER
X: -20
Y: -10
W: 500
H: 240
Frame: XPerl_AdminFrameAnchor
FrameLevel: 1
Anchor: CENTER
X: -20
Y: -10
W: 140
H: 150
Frame: XPerl_Player
FrameLevel: 1
Anchor: BOTTOM
X: -127
Y: 218
W: 220
H: 72
Frame: XPerl_Party_Anchor
FrameLevel: 1
Anchor: LEFT
X: 312
Y: 189
W: 100
H: 60
Frame: AllTheThings-Window-Prime
FrameLevel: 1
Anchor: CENTER
X: 0
Y: 0
W: 300
H: 300
Frame: XPerl_Focus
FrameLevel: 1
Anchor: RIGHT
X: -268
Y: -240
W: 220
H: 62
Frame: XPerl_Player_Pet
FrameLevel: 1
Anchor: BOTTOMLEFT
X: 446
Y: 251
W: 140
H: 50
Frame: XPerl_PetTarget
FrameLevel: 1
Anchor: BOTTOM
X: -343
Y: 245
W: 160
H: 60
Frame: DBMMinimapButton
FrameLevel: 8
Anchor: TOPRIGHT
X: 4
Y: -83
W: 32
H: 32
Frame: XPerl_TargetTarget
FrameLevel: 1
Anchor: BOTTOM
X: 118
Y: 187
W: 160
H: 60
Frame: XPerl_Target
FrameLevel: 1
Anchor: BOTTOM
X: 146
Y: 228
W: 220
H: 62
