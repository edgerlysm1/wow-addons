
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005637,
							["damage_from"] = {
								["Goblin Assassin"] = true,
								["Blackrock Spy"] = true,
								["Blackrock Invader"] = true,
							},
							["targets"] = {
								["Blackrock Spy"] = 117,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 117.005637,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1518397047,
							["friendlyfire_total"] = 0,
							["spec"] = 259,
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 22,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Blackrock Spy"] = 52,
											["Blackrock Invader"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 52,
										["c_max"] = 15,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 7,
									}, -- [1]
									[1752] = {
										["c_amt"] = 0,
										["b_amt"] = 1,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 25,
										["targets"] = {
											["Blackrock Spy"] = 65,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 65,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 65,
										["c_max"] = 0,
										["id"] = 1752,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 16,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 117.005637,
							["serial"] = "Player-76-09A7997E",
							["last_dps"] = 18.4551477917236,
							["custom"] = 0,
							["last_event"] = 1518397352,
							["damage_taken"] = 39.005637,
							["start_time"] = 1518397041,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 4,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorb"] = 0.00439,
							["last_hps"] = 0,
							["targets_overheal"] = {
							},
							["healing_from"] = {
								["Thorvinn"] = true,
							},
							["targets"] = {
								["Thorvinn"] = 6,
							},
							["targets_absorbs"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["totalover_without_pet"] = 0.00439,
							["healing_taken"] = 6.00439,
							["totalover"] = 0.00439,
							["total_without_pet"] = 6.00439,
							["end_time"] = 1518397047,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 6.00439,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1518397046,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorvinn"] = 6,
										},
										["n_min"] = 3,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 6,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["heal_enemy"] = {
							},
							["serial"] = "Player-76-09A7997E",
							["custom"] = 0,
							["tipo"] = 2,
							["on_hold"] = false,
							["totaldenied"] = 0.00439,
							["delay"] = 0,
							["last_event"] = 1518397046,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 4,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 7.001456,
							["resource"] = 0.001456,
							["targets"] = {
								["Thorvinn"] = 7,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 7.001456,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1518397352,
							["alternatepower"] = 0.001456,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[1752] = {
										["id"] = 1752,
										["total"] = 7,
										["targets"] = {
											["Thorvinn"] = 7,
										},
										["counter"] = 7,
									},
								},
							},
							["serial"] = "Player-76-09A7997E",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 4,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1297,
							["cc_break_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									["BUFF"] = {
										["cc_break_oque"] = {
											[92857] = 1,
										},
										["id"] = "BUFF",
										["cc_break"] = 1,
										["targets"] = {
											["Blackrock Spy"] = 1,
										},
										["counter"] = 0,
									},
									[1752] = {
										["cc_break_oque"] = {
											[80676] = 1,
											[92857] = 1,
										},
										["id"] = 1752,
										["cc_break"] = 2,
										["targets"] = {
											["Blackrock Spy"] = 2,
										},
										["counter"] = 0,
									},
								},
							},
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["cc_break"] = 3.00361,
							["spell_cast"] = {
								[1752] = 3,
							},
							["cc_break_oque"] = {
								[92857] = 2,
								[80676] = 1,
							},
							["pets"] = {
							},
							["tipo"] = 4,
							["last_event"] = 1518397089,
							["cc_break_targets"] = {
								["Blackrock Spy"] = 3,
							},
							["serial"] = "Player-76-09A7997E",
							["classe"] = "ROGUE",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorvinn"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Blackrock Spy",
				["combat_counter"] = 12,
				["playing_solo"] = true,
				["totals"] = {
					116.954316, -- [1]
					6, -- [2]
					{
						0, -- [1]
						[0] = 6.993412,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 3,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorvinn"] = {
						{
							true, -- [1]
							1, -- [2]
							5, -- [3]
							1518397081.79, -- [4]
							300, -- [5]
							"Blackrock Spy", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							1, -- [2]
							6, -- [3]
							1518397206.02, -- [4]
							336, -- [5]
							"Goblin Assassin", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							1, -- [2]
							5, -- [3]
							1518397316.335, -- [4]
							336, -- [5]
							"Blackrock Invader", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							1, -- [2]
							6, -- [3]
							1518397353.474, -- [4]
							336, -- [5]
							"Blackrock Invader", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 5,
					},
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorvinn"] = 161.102182,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorvinn"] = 4103.130718,
						}, -- [1]
					},
				},
				["end_time"] = 364006.982,
				["combat_id"] = 4,
				["instance_type"] = "none",
				["resincked"] = true,
				["frags"] = {
					["Blackrock Spy"] = 1,
				},
				["data_fim"] = "19:57:28",
				["data_inicio"] = "19:57:22",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					117, -- [1]
					6, -- [2]
					{
						0, -- [1]
						[0] = 7,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 3,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 364000.642,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003578,
							["damage_from"] = {
								["Blackrock Worg"] = true,
							},
							["targets"] = {
								["Blackrock Worg"] = 75,
							},
							["total"] = 75.003578,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 75.003578,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1518397001,
							["friendlyfire_total"] = 0,
							["spec"] = 259,
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Blackrock Worg"] = 32,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 32,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 32,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1752] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 23,
										["targets"] = {
											["Blackrock Worg"] = 43,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 43,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 43,
										["c_max"] = 0,
										["id"] = 1752,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09A7997E",
							["last_dps"] = 14.0062704015427,
							["custom"] = 0,
							["last_event"] = 1518397000,
							["damage_taken"] = 12.003578,
							["start_time"] = 1518396996,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 3,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorvinn"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 0.007131,
							["total_without_pet"] = 6.007131,
							["total"] = 6.007131,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A7997E",
							["totalabsorb"] = 0.007131,
							["last_hps"] = 0,
							["targets"] = {
								["Thorvinn"] = 6,
							},
							["totalover_without_pet"] = 0.007131,
							["healing_taken"] = 6.007131,
							["fight_component"] = true,
							["end_time"] = 1518397001,
							["targets_overheal"] = {
							},
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorvinn"] = 6,
										},
										["n_min"] = 3,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 6,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1518397000,
							["custom"] = 0,
							["last_event"] = 1518397000,
							["spec"] = 259,
							["totaldenied"] = 0.007131,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 0.002261,
							["resource"] = 0.002261,
							["targets"] = {
								["Thorvinn"] = 0,
							},
							["pets"] = {
							},
							["powertype"] = 1,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 0.002261,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1518396999,
							["alternatepower"] = 0.002261,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[1752] = {
										["id"] = 1752,
										["total"] = 0,
										["targets"] = {
											["Thorvinn"] = 0,
										},
										["counter"] = 2,
									},
								},
							},
							["serial"] = "Player-76-09A7997E",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["tipo"] = 4,
							["spell_cast"] = {
								[1752] = 2,
							},
							["serial"] = "Player-76-09A7997E",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorvinn"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Blackrock Worg",
				["combat_counter"] = 11,
				["playing_solo"] = true,
				["totals"] = {
					74.987161, -- [1]
					6, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorvinn"] = 6.007131,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorvinn"] = 75.003578,
						}, -- [1]
					},
				},
				["end_time"] = 363960.817,
				["combat_id"] = 3,
				["instance_type"] = "none",
				["frags"] = {
					["Blackrock Worg"] = 1,
				},
				["data_fim"] = "19:56:42",
				["data_inicio"] = "19:56:37",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					75, -- [1]
					6, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 363955.462,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [2]
			{
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002714,
							["damage_from"] = {
								["Blackrock Worg"] = true,
							},
							["targets"] = {
								["Blackrock Worg"] = 92,
							},
							["total"] = 92.002714,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 92.002714,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1518396967,
							["friendlyfire_total"] = 0,
							["spec"] = 259,
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 1,
										["c_dmg"] = 8,
										["g_amt"] = 0,
										["n_max"] = 8,
										["targets"] = {
											["Blackrock Worg"] = 38,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 38,
										["c_max"] = 8,
										["MISS"] = 4,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 4,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 8,
									}, -- [1]
									[1752] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 27,
										["targets"] = {
											["Blackrock Worg"] = 54,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 54,
										["n_min"] = 27,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 54,
										["c_max"] = 0,
										["id"] = 1752,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09A7997E",
							["last_dps"] = 12.5893149972304,
							["custom"] = 0,
							["last_event"] = 1518396995,
							["damage_taken"] = 20.002714,
							["start_time"] = 1518396960,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 2,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorvinn"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 0.008737,
							["total_without_pet"] = 6.008737,
							["total"] = 6.008737,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A7997E",
							["totalabsorb"] = 0.008737,
							["last_hps"] = 0,
							["targets"] = {
								["Thorvinn"] = 6,
							},
							["totalover_without_pet"] = 0.008737,
							["healing_taken"] = 6.008737,
							["fight_component"] = true,
							["end_time"] = 1518396967,
							["targets_overheal"] = {
							},
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorvinn"] = 6,
										},
										["n_min"] = 3,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 6,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1518396967,
							["custom"] = 0,
							["last_event"] = 1518396967,
							["spec"] = 259,
							["totaldenied"] = 0.008737,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 2,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 2.003818,
							["resource"] = 0.003818,
							["targets"] = {
								["Thorvinn"] = 2,
							},
							["pets"] = {
							},
							["powertype"] = 1,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 2.003818,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1518396982,
							["alternatepower"] = 0.003818,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[1752] = {
										["id"] = 1752,
										["total"] = 2,
										["targets"] = {
											["Thorvinn"] = 2,
										},
										["counter"] = 4,
									},
								},
							},
							["serial"] = "Player-76-09A7997E",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 2,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["tipo"] = 4,
							["spell_cast"] = {
								[1752] = 2,
							},
							["serial"] = "Player-76-09A7997E",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorvinn"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Blackrock Worg",
				["combat_counter"] = 7,
				["playing_solo"] = true,
				["totals"] = {
					91.990378, -- [1]
					6, -- [2]
					{
						0, -- [1]
						[0] = 2,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorvinn"] = 21.02915,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorvinn"] = 336.022463,
						}, -- [1]
					},
				},
				["end_time"] = 363926.732,
				["combat_id"] = 2,
				["instance_type"] = "none",
				["resincked"] = true,
				["frags"] = {
					["Blackrock Worg"] = 1,
				},
				["data_fim"] = "19:56:08",
				["data_inicio"] = "19:56:01",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					92, -- [1]
					6, -- [2]
					{
						0, -- [1]
						[0] = 2,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 363919.424,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [3]
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005054,
							["spec"] = 259,
							["damage_from"] = {
								["Blackrock Worg"] = true,
							},
							["targets"] = {
								["Blackrock Worg"] = 98,
							},
							["pets"] = {
							},
							["colocacao"] = 1,
							["end_time"] = 1518396956,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 98.005054,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["total"] = 98.005054,
							["friendlyfire_total"] = 0,
							["on_hold"] = false,
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Blackrock Worg"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1752] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 27,
										["targets"] = {
											["Blackrock Worg"] = 76,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 76,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 76,
										["c_max"] = 0,
										["id"] = 1752,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09A7997E",
							["last_dps"] = 34.1481024390798,
							["custom"] = 0,
							["last_event"] = 1518396956,
							["damage_taken"] = 3.005054,
							["start_time"] = 1518396953,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorvinn"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 3.001832,
							["total_without_pet"] = 3.001832,
							["total"] = 3.001832,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A7997E",
							["totalabsorb"] = 0.001832,
							["last_hps"] = 0,
							["targets"] = {
								["Thorvinn"] = 3,
							},
							["totalover_without_pet"] = 0.001832,
							["healing_taken"] = 3.001832,
							["fight_component"] = true,
							["end_time"] = 1518396956,
							["targets_overheal"] = {
								["Thorvinn"] = 3,
							},
							["nome"] = "Thorvinn",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorvinn"] = 3,
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorvinn"] = 3,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 3,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 3,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1518396956,
							["custom"] = 0,
							["last_event"] = 1518396956,
							["spec"] = 259,
							["totaldenied"] = 0.001832,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.003815,
							["resource"] = 0.003815,
							["targets"] = {
								["Thorvinn"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 3.003815,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1518396956,
							["alternatepower"] = 0.003815,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[1752] = {
										["id"] = 1752,
										["total"] = 3,
										["targets"] = {
											["Thorvinn"] = 3,
										},
										["counter"] = 3,
									},
								},
							},
							["serial"] = "Player-76-09A7997E",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorvinn",
							["spec"] = 259,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["tipo"] = 4,
							["spell_cast"] = {
								[1752] = 3,
							},
							["serial"] = "Player-76-09A7997E",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorvinn"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Blackrock Worg",
				["combat_counter"] = 6,
				["playing_solo"] = true,
				["totals"] = {
					97.9931, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorvinn"] = 3.001832,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorvinn"] = 98.005054,
						}, -- [1]
					},
				},
				["end_time"] = 363915.787,
				["combat_id"] = 1,
				["instance_type"] = "none",
				["frags"] = {
					["Blackrock Worg"] = 1,
				},
				["data_fim"] = "19:55:57",
				["data_inicio"] = "19:55:54",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					98, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 363912.834,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [4]
		},
	},
	["combat_counter"] = 36,
	["character_data"] = {
		["logons"] = 2,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -17.1825256347656,
					["x"] = -608.020317077637,
					["w"] = 184.937026977539,
					["h"] = 132.397598266602,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09A7997E"] = {
		},
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["active_profile"] = "Thorvinn-Sargeras",
	["last_realversion"] = 128,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["enabled"] = true,
			["dps"] = 0,
			["level"] = 1,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["author"] = "Details! Team",
			["realm_history"] = {
			},
			["realm_lastamt"] = 0,
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				["last_selected"] = 1,
			},
			["options"] = {
				["show_method"] = 4,
				["auto_create"] = true,
				["window_scale"] = 1,
			},
		},
	},
	["nick_tag_cache"] = {
		["nextreset"] = 1519692661,
		["last_version"] = 9,
	},
	["ignore_nicktag"] = false,
	["last_day"] = "11",
	["last_version"] = "v7.3.0.5198",
	["combat_id"] = 4,
	["savedStyles"] = {
	},
	["last_instance_time"] = 0,
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 5,
		["totals"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["totals_grupo"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["data_inicio"] = 0,
		["frags"] = {
		},
		["data_fim"] = 0,
		["CombatSkillCache"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["start_time"] = 0,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["player_last_events"] = {
		},
	},
	["force_font_outline"] = "",
	["SoloTablesSaved"] = {
		["LastSelected"] = "DETAILS_PLUGIN_DAMAGE_RANK",
		["Mode"] = 1,
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09A7997E"] = 259,
	},
}
