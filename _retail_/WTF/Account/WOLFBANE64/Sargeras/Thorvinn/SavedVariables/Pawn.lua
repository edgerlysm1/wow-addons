
PawnOptions = {
	["LastVersion"] = 2.0217,
	["LastPlayerFullName"] = "Thorvinn-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "ROGUE",
	["LastAdded"] = 1,
}
