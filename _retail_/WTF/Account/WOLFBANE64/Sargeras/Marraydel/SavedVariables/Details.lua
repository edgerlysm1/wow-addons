
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002572,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Dummy"] = 5389,
							},
							["total"] = 5389.002571999999,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 5389.002571999999,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1578880927,
							["friendlyfire_total"] = 0,
							["spec"] = 71,
							["nome"] = "Marraydel",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 6,
										["b_amt"] = 0,
										["c_dmg"] = 672,
										["g_amt"] = 0,
										["n_max"] = 58,
										["targets"] = {
											["Training Dummy"] = 1713,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1041,
										["n_min"] = 52,
										["g_dmg"] = 0,
										["counter"] = 25,
										["total"] = 1713,
										["c_max"] = 117,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 19,
										["r_amt"] = 0,
										["c_min"] = 107,
									}, -- [1]
									[12294] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 342,
										["g_amt"] = 0,
										["n_max"] = 171,
										["targets"] = {
											["Training Dummy"] = 2162,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1820,
										["n_min"] = 142,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 2162,
										["c_max"] = 342,
										["id"] = 12294,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 12,
										["r_amt"] = 0,
										["c_min"] = 342,
									},
									[126664] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Training Dummy"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = 126664,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[7384] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 85,
										["targets"] = {
											["Training Dummy"] = 425,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 425,
										["n_min"] = 85,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 425,
										["c_max"] = 0,
										["id"] = 7384,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 71,
										["targets"] = {
											["Training Dummy"] = 1065,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1065,
										["n_min"] = 71,
										["g_dmg"] = 0,
										["counter"] = 15,
										["total"] = 1065,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 15,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A4AB170",
							["last_dps"] = 58.58311941645107,
							["custom"] = 0,
							["last_event"] = 1578880922,
							["damage_taken"] = 0.002572,
							["start_time"] = 1578880835,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.004481,
							["damage_from"] = {
								["Marraydel"] = true,
								["Icyy"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.004481,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 0.004481,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3019-1643-11633-126781-0000151245",
							["nome"] = "Training Dummy",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1578880927,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 53172.004481,
							["start_time"] = 1578880927,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 2,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 2,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 20.008858,
							["resource"] = 0.008858,
							["targets"] = {
								["Marraydel"] = 20,
							},
							["pets"] = {
							},
							["powertype"] = 1,
							["classe"] = "WARRIOR",
							["passiveover"] = 0.008858,
							["total"] = 20.008858,
							["nome"] = "Marraydel",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[100] = {
										["total"] = 20,
										["id"] = 100,
										["totalover"] = 0,
										["targets"] = {
											["Marraydel"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["flag_original"] = 1297,
							["alternatepower"] = 0.008858,
							["last_event"] = 1578880912,
							["spec"] = 71,
							["tipo"] = 3,
							["serial"] = "Player-76-0A4AB170",
							["totalover"] = 0.008858,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 2,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[105771] = {
										["activedamt"] = -1,
										["id"] = 105771,
										["targets"] = {
										},
										["uptime"] = 92,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[115804] = {
										["activedamt"] = 1,
										["id"] = 115804,
										["targets"] = {
										},
										["uptime"] = 89,
										["appliedamt"] = 3,
										["refreshamt"] = 10,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[7384] = {
										["activedamt"] = 5,
										["id"] = 7384,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 5,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 181,
							["nome"] = "Marraydel",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[12294] = 13,
								[7384] = 5,
								[100] = 1,
								[126664] = 1,
								[1464] = 15,
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1578880922,
							["tipo"] = 4,
							["debuff_uptime_targets"] = {
							},
							["serial"] = "Player-76-0A4AB170",
							["buff_uptime"] = 8,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Marraydel"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1578880835,
				["enemy"] = "Training Dummy",
				["combat_counter"] = 5,
				["playing_solo"] = true,
				["totals"] = {
					5388.994441999996, -- [1]
					-0.045601, -- [2]
					{
						20, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = -0.002456999999992604,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 37908.646,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "21:00:35",
				["end_time"] = 37908.646,
				["totals_grupo"] = {
					5389, -- [1]
					0, -- [2]
					{
						20, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 2,
				["TotalElapsedCombatTime"] = 37908.646,
				["frags_need_refresh"] = false,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Marraydel"] = 5389.002571999999,
						}, -- [1]
					},
				},
				["frags"] = {
				},
				["data_fim"] = "21:02:07",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 37816.657,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002779,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Dummy"] = 177,
							},
							["total"] = 177.002779,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 177.002779,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1578880819,
							["friendlyfire_total"] = 0,
							["spec"] = 71,
							["nome"] = "Marraydel",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 58,
										["targets"] = {
											["Training Dummy"] = 165,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 165,
										["n_min"] = 52,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 165,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[126664] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Training Dummy"] = 12,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 12,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = 126664,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A4AB170",
							["last_dps"] = 12.86918561872833,
							["custom"] = 0,
							["last_event"] = 1578880813,
							["damage_taken"] = 0.002779,
							["start_time"] = 1578880805,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00461,
							["damage_from"] = {
								["Marraydel"] = true,
								["Icyy"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.00461,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 0.00461,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3019-1643-11633-126781-0000151245",
							["nome"] = "Training Dummy",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1578880819,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 12517.00461,
							["start_time"] = 1578880819,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 20.003149,
							["resource"] = 0.003149,
							["targets"] = {
								["Marraydel"] = 20,
							},
							["pets"] = {
							},
							["powertype"] = 1,
							["classe"] = "WARRIOR",
							["passiveover"] = 0.003149,
							["total"] = 20.003149,
							["nome"] = "Marraydel",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[100] = {
										["total"] = 20,
										["id"] = 100,
										["totalover"] = 0,
										["targets"] = {
											["Marraydel"] = 20,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003149,
							["last_event"] = 1578880834,
							["spec"] = 71,
							["tipo"] = 3,
							["serial"] = "Player-76-0A4AB170",
							["totalover"] = 0.003149,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["debuff_uptime"] = 0,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[105771] = {
										["activedamt"] = -1,
										["id"] = 105771,
										["targets"] = {
										},
										["actived_at"] = 1578880806,
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime_targets"] = {
							},
							["spec"] = 71,
							["grupo"] = true,
							["nome"] = "Marraydel",
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 0,
							["classe"] = "WARRIOR",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[109128] = {
										["activedamt"] = 0,
										["id"] = 109128,
										["targets"] = {
										},
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0A4AB170",
							["last_event"] = 1578880811,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Marraydel"] = true,
				},
				["CombatStartedAt"] = 37815.772,
				["tempo_start"] = 1578880805,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					176.9971989999995, -- [1]
					-0.006123, -- [2]
					{
						20, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = -0.007632000000000971,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					177, -- [1]
					0, -- [2]
					{
						20, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["data_fim"] = "21:00:19",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Dummy",
				["TotalElapsedCombatTime"] = 37800.479,
				["CombatEndedAt"] = 37800.479,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Marraydel"] = 177.002779,
						}, -- [1]
					},
				},
				["end_time"] = 37800.479,
				["combat_id"] = 1,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "21:00:05",
				["start_time"] = 37786.725,
				["TimeData"] = {
				},
				["frags"] = {
				},
			}, -- [2]
		},
	},
	["last_version"] = "v8.2.5.7227",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1580176712,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Default",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "12",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 2,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 5,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.009242,
					["damage_from"] = {
					},
					["targets"] = {
						["Training Dummy"] = 5566,
					},
					["spec"] = 71,
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "WARRIOR",
					["raid_targets"] = {
					},
					["total_without_pet"] = 5566.009241999999,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1578880819,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Marraydel",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 6,
								["b_amt"] = 0,
								["c_dmg"] = 672,
								["g_amt"] = 0,
								["n_max"] = 58,
								["targets"] = {
									["Training Dummy"] = 1878,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1206,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 28,
								["total"] = 1878,
								["c_max"] = 117,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 22,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[12294] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 342,
								["g_amt"] = 0,
								["n_max"] = 171,
								["targets"] = {
									["Training Dummy"] = 2162,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1820,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 2162,
								["c_max"] = 342,
								["id"] = 12294,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 12,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[126664] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 12,
								["targets"] = {
									["Training Dummy"] = 36,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 36,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 36,
								["c_max"] = 0,
								["id"] = 126664,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[1464] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 71,
								["targets"] = {
									["Training Dummy"] = 1065,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1065,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 15,
								["total"] = 1065,
								["c_max"] = 0,
								["id"] = 1464,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 15,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[7384] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 85,
								["targets"] = {
									["Training Dummy"] = 425,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 425,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 425,
								["c_max"] = 0,
								["id"] = 7384,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 5566.009241999999,
					["serial"] = "Player-76-0A4AB170",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 0.009242,
					["start_time"] = 1578880710,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.016238,
					["damage_from"] = {
						["Marraydel"] = true,
						["Icyy"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.016238,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1578880819,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["nome"] = "Training Dummy",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["friendlyfire_total"] = 0,
					["total"] = 0.016238,
					["serial"] = "Creature-0-3019-1643-11633-126781-0000151245",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 49369.016238,
					["start_time"] = 1578880816,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 20.017001,
					["resource"] = 0.017001,
					["targets"] = {
						["Marraydel"] = 20,
					},
					["pets"] = {
					},
					["powertype"] = 1,
					["classe"] = "WARRIOR",
					["passiveover"] = 0.008143,
					["total"] = 20.017001,
					["tipo"] = 3,
					["nome"] = "Marraydel",
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[100] = {
								["total"] = 20,
								["id"] = 100,
								["totalover"] = 0,
								["targets"] = {
									["Marraydel"] = 20,
								},
								["counter"] = 1,
							},
						},
					},
					["grupo"] = true,
					["totalover"] = 0.008143,
					["flag_original"] = 1297,
					["alternatepower"] = 0.017001,
					["last_event"] = 0,
					["spec"] = 71,
					["serial"] = "Player-76-0A4AB170",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["debuff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[105771] = {
								["refreshamt"] = 0,
								["appliedamt"] = 1,
								["activedamt"] = -2,
								["uptime"] = 92,
								["id"] = 105771,
								["actived_at"] = 1578880806,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[115804] = {
								["refreshamt"] = 10,
								["activedamt"] = 1,
								["appliedamt"] = 3,
								["id"] = 115804,
								["uptime"] = 89,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["buff_uptime"] = 8,
					["classe"] = "WARRIOR",
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[109128] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 0,
								["id"] = 109128,
								["uptime"] = 0,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[7384] = {
								["refreshamt"] = 0,
								["activedamt"] = 5,
								["appliedamt"] = 5,
								["id"] = 7384,
								["uptime"] = 8,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["debuff_uptime"] = 181,
					["buff_uptime_targets"] = {
					},
					["spec"] = 71,
					["grupo"] = true,
					["spell_cast"] = {
						[12294] = 13,
						[7384] = 5,
						[100] = 1,
						[126664] = 1,
						[1464] = 15,
					},
					["debuff_uptime_targets"] = {
					},
					["tipo"] = 4,
					["nome"] = "Marraydel",
					["pets"] = {
					},
					["serial"] = "Player-76-0A4AB170",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1578880805,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			49369.02280100001, -- [1]
			0.051724, -- [2]
			{
				20.008858, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 330.010089,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "21:00:05",
		["end_time"] = 37908.646,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 91.9890000000014,
				["type"] = 0,
				["name"] = "Training Dummy",
				["clock"] = "21:00:35",
			}, -- [1]
			{
				["elapsed"] = 13.75400000000082,
				["type"] = 0,
				["name"] = "Training Dummy",
				["clock"] = "21:00:05",
			}, -- [2]
		},
		["totals_grupo"] = {
			5566.005351, -- [1]
			0, -- [2]
			{
				20.008858, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "21:02:07",
		["overall_enemy_name"] = "Training Dummy",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 37802.903,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["is_open"] = true,
			["isLocked"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[2] = 2,
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 95.35137939453125,
					["x"] = -549.5246124267578,
					["w"] = 170.7061767578125,
					["h"] = 116.0994491577148,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["is_open"] = true,
			["isLocked"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[4] = 1,
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -40.74807739257813,
					["x"] = -549.5246124267578,
					["w"] = 170.7061614990234,
					["h"] = 116.0995025634766,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [2]
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A415186"] = 251,
		["Player-76-0A4AB170"] = 71,
		["Player-76-09BFC894"] = 264,
	},
}
