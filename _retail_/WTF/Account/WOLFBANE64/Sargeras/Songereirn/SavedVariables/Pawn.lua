
PawnOptions = {
	["LastVersion"] = 2.0218,
	["LastPlayerFullName"] = "Songereirn-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DEATHKNIGHT",
	["LastAdded"] = 1,
}
