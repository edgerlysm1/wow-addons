
PawnOptions = {
	["LastVersion"] = 2.0324,
	["LastPlayerFullName"] = "Senithor-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 112664,
			["Level"] = 120,
			["Link"] = "|cff0070dd|Hitem:112664::::::::91:265::11::::|h[Voidgazer Cap]|h|r",
		}, -- [1]
		{
			["ID"] = 89068,
			["Level"] = 116,
			["Link"] = "|cffa335ee|Hitem:89068::::::::91:265::::::|h[Wire of the Wakener]|h|r",
		}, -- [2]
		{
			["ID"] = 112670,
			["Level"] = 120,
			["Link"] = "|cff0070dd|Hitem:112670::::::::91:265::11::::|h[Quarrier's Mantle]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 112440,
			["Level"] = 119,
			["Link"] = "|cff0070dd|Hitem:112440::::::::91:265::11::::|h[Slavebreaker Robes]|h|r",
		}, -- [5]
		{
			["ID"] = 81058,
			["Level"] = 116,
			["Link"] = "|cff0070dd|Hitem:81058::::::::91:265::::::|h[Girdle of Endemic Anger]|h|r",
		}, -- [6]
		{
			["ID"] = 112434,
			["Level"] = 119,
			["Link"] = "|cff0070dd|Hitem:112434::::::::91:265::11::::|h[Hollowheart Pantaloons]|h|r",
		}, -- [7]
		{
			["ID"] = 90913,
			["Level"] = 116,
			["Link"] = "|cffa335ee|Hitem:90913:4429:::::::91:265::::::|h[Sandals of the Shadow]|h|r",
		}, -- [8]
		{
			["ID"] = 95136,
			["Level"] = 122,
			["Link"] = "|cffa335ee|Hitem:95136::::::::91:265::::::|h[Troll-Burner Bracers]|h|r",
		}, -- [9]
		{
			["ID"] = 86709,
			["Level"] = 116,
			["Link"] = "|cffa335ee|Hitem:86709::::::::91:265::::::|h[Sha-Skin Gloves]|h|r",
		}, -- [10]
		{
			["ID"] = 113082,
			["Level"] = 120,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:113082::::::::91:265::::::|h[Precious Bloodthorn Loop]|h|r",
		}, -- [11]
		{
			["ID"] = 86814,
			["Level"] = 116,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:86814::::::::91:265::::::|h[Fragment of Fear Made Flesh]|h|r",
		}, -- [12]
		{
			["ID"] = 86907,
			["Level"] = 116,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:86907::::::::91:265::::::|h[Essence of Terror]|h|r",
		}, -- [13]
		{
			["ID"] = 89081,
			["Level"] = 116,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:89081::::::::91:265::::::|h[Blossom of Pure Snow]|h|r",
		}, -- [14]
		{
			["ID"] = 86840,
			["Level"] = 116,
			["Link"] = "|cffa335ee|Hitem:86840::::::::91:265::::::|h[Stormwake Mistcloak]|h|r",
		}, -- [15]
		{
			["ID"] = 108918,
			["Level"] = 120,
			["Link"] = "|cff1eff00|Hitem:108918::::::::91:265::11::::|h[Karabor Arcanist Rod]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "WARLOCK",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
