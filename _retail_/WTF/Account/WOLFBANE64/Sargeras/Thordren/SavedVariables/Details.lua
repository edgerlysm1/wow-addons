
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 3537,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003845,
							["total"] = 3132.003845,
							["damage_from"] = {
								["Weald Stinger"] = true,
							},
							["targets"] = {
								["Weald Stinger"] = 3132,
							},
							["pets"] = {
							},
							["spec"] = 63,
							["colocacao"] = 1,
							["classe"] = "MAGE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 3132.003845,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674882,
							["friendlyfire_total"] = 0,
							["aID"] = "76-09A108A3",
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Weald Stinger"] = 46,
										},
										["n_dmg"] = 46,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 46,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[11366] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 556,
										["targets"] = {
											["Weald Stinger"] = 1112,
										},
										["n_dmg"] = 1112,
										["n_min"] = 556,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1112,
										["c_max"] = 0,
										["id"] = 11366,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[235314] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 50,
										["targets"] = {
											["Weald Stinger"] = 50,
										},
										["n_dmg"] = 50,
										["n_min"] = 50,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 50,
										["c_max"] = 0,
										["id"] = 235314,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[108853] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 1924,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Weald Stinger"] = 1924,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1924,
										["c_max"] = 644,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 637,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 747.8519209646428,
							["custom"] = 0,
							["last_event"] = 1618674881,
							["damage_taken"] = 99.003845,
							["start_time"] = 1618674877,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 99.008338,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 99,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 99.008338,
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1618674882,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Weald Stinger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 99,
										["targets"] = {
											["Thordren"] = 99,
										},
										["n_dmg"] = 99,
										["n_min"] = 99,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 99,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "85807",
							["total"] = 99.008338,
							["serial"] = "Creature-0-4215-1116-69-85807-00007AF6D2",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674878,
							["damage_taken"] = 3132.008338,
							["start_time"] = 1618674878,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 3537,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 0.003588,
							["total_without_pet"] = 99.003588,
							["total"] = 99.003588,
							["targets_absorbs"] = {
								["Thordren"] = 99,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 99.003588,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 99,
							},
							["totalover_without_pet"] = 0.003588,
							["healing_taken"] = 99.003588,
							["fight_component"] = true,
							["end_time"] = 1618674882,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[235313] = {
										["c_amt"] = 0,
										["totalabsorb"] = 99,
										["targets_overheal"] = {
										},
										["n_max"] = 99,
										["targets"] = {
											["Thordren"] = 99,
										},
										["n_min"] = 99,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 99,
										["c_max"] = 0,
										["id"] = 235313,
										["targets_absorbs"] = {
											["Thordren"] = 99,
										},
										["n_amt"] = 1,
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 99,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674878,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.003588,
							["delay"] = 0,
							["last_event"] = 1618674878,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3537,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 138.008798,
							["resource"] = 0.008798,
							["targets"] = {
								["Thordren"] = 138,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.008798,
							["fight_component"] = true,
							["total"] = 138.008798,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 138,
										["id"] = 59914,
										["totalover"] = 19,
										["targets"] = {
											["Thordren"] = 138,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.008798,
							["last_event"] = 1618674881,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 19.008798,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3537,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 3,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 22,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 2,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[235313] = {
										["activedamt"] = 1,
										["id"] = 235313,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 1,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 4,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[11366] = 2,
								[108853] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674882,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 3537,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7709.497,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4839,
				["playing_solo"] = true,
				["totals"] = {
					3231, -- [1]
					99, -- [2]
					{
						0, -- [1]
						[0] = 138,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:54:42",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Weald Stinger",
				["TotalElapsedCombatTime"] = 4.777000000000044,
				["CombatEndedAt"] = 7714.274,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:54:37",
				["end_time"] = 7714.274,
				["combat_id"] = 3537,
				["tempo_start"] = 1618674877,
				["frags"] = {
					["Weald Stinger"] = 1,
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["totals_grupo"] = {
					3132, -- [1]
					99, -- [2]
					{
						0, -- [1]
						[0] = 138,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["CombatSkillCache"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 99.003588,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 3132.003845,
						}, -- [1]
					},
				},
				["start_time"] = 7709.386,
				["TimeData"] = {
				},
				["spells_cast_timeline"] = {
				},
			}, -- [1]
			{
				{
					["combatId"] = 3536,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008876,
							["damage_from"] = {
							},
							["targets"] = {
								["Weald Stinger"] = 2782,
							},
							["pets"] = {
							},
							["total"] = 2782.008876,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 2782.008876,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674863,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[108853] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 640,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Weald Stinger"] = 640,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 640,
										["c_max"] = 640,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 640,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[257542] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 368,
										["targets"] = {
											["Weald Stinger"] = 368,
										},
										["n_dmg"] = 368,
										["n_min"] = 368,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 368,
										["c_max"] = 0,
										["id"] = 257542,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Weald Stinger"] = 18,
										},
										["n_dmg"] = 18,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Weald Stinger"] = 4,
										},
										["n_dmg"] = 4,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["spellschool"] = 48,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[11366] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1085,
										["g_amt"] = 0,
										["n_max"] = 545,
										["targets"] = {
											["Weald Stinger"] = 1630,
										},
										["n_dmg"] = 545,
										["n_min"] = 545,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1630,
										["c_max"] = 1085,
										["id"] = 11366,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 1085,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 28,
										["targets"] = {
											["Weald Stinger"] = 122,
										},
										["n_dmg"] = 122,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 122,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 5,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 416.1568999251808,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.008876,
							["start_time"] = 1618674856,
							["delay"] = 0,
							["last_event"] = 1618674862,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002242,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.002242,
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1618674863,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Weald Stinger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["aID"] = "85807",
							["total"] = 0.002242,
							["serial"] = "Creature-0-4215-1116-69-85807-00007AF6BC",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 2782.002242,
							["start_time"] = 1618674863,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 3536,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 3536,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 0.002636,
							["resource"] = 0.002636,
							["targets"] = {
								["Thordren"] = 0,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.002636,
							["total"] = 0.002636,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 0,
										["id"] = 59914,
										["totalover"] = 157,
										["targets"] = {
											["Thordren"] = 0,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002636,
							["last_event"] = 1618674862,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 157.002636,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3536,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 0,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 35,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 1,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[256374] = {
										["activedamt"] = 1,
										["id"] = 256374,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[235313] = {
										["activedamt"] = 1,
										["id"] = 235313,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 1,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["debuff_uptime"] = 11,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[11366] = 1,
								[257541] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674863,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 3536,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7688.795,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4838,
				["playing_solo"] = true,
				["totals"] = {
					2782, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:54:23",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Weald Stinger",
				["TotalElapsedCombatTime"] = 6.6850000000004,
				["CombatEndedAt"] = 7695.480000000001,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:54:16",
				["end_time"] = 7695.480000000001,
				["combat_id"] = 3536,
				["tempo_start"] = 1618674856,
				["frags"] = {
					["Weald Stinger"] = 1,
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["totals_grupo"] = {
					2782, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["CombatSkillCache"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 2782.008876,
						}, -- [1]
					},
				},
				["start_time"] = 7688.795,
				["TimeData"] = {
				},
				["spells_cast_timeline"] = {
				},
			}, -- [2]
			{
				{
					["combatId"] = 3535,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 391.005077,
							["total"] = 1757.005077,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 1757,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1757.005077,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674828,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Thorny Stabber",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 160,
										["targets"] = {
											["Thordren"] = 1757,
										},
										["n_dmg"] = 1757,
										["n_min"] = 109,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 1757,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 13,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "80744",
							["serial"] = "Creature-0-4215-1116-69-80744-00007AF871",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674826,
							["damage_taken"] = 5275.005077,
							["start_time"] = 1618674808,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001918,
							["damage_from"] = {
								["Thorny Leafling"] = true,
								["Thorny Stabber"] = true,
							},
							["targets"] = {
								["Thorny Leafling"] = 9243,
								["Twilight Wasp"] = 7,
								["Thorny Stabber"] = 5275,
							},
							["pets"] = {
							},
							["total"] = 14525.001918,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 14525.001918,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674828,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[235314] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 296,
										["g_amt"] = 0,
										["n_max"] = 52,
										["targets"] = {
											["Thorny Stabber"] = 197,
											["Thorny Leafling"] = 800,
										},
										["n_dmg"] = 701,
										["n_min"] = 49,
										["g_dmg"] = 0,
										["counter"] = 17,
										["total"] = 997,
										["c_max"] = 100,
										["id"] = 235314,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 98,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 14,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2120] = {
										["c_amt"] = 7,
										["b_amt"] = 0,
										["c_dmg"] = 3414,
										["g_amt"] = 0,
										["n_max"] = 250,
										["targets"] = {
											["Thorny Stabber"] = 724,
											["Thorny Leafling"] = 5143,
										},
										["n_dmg"] = 2453,
										["n_min"] = 240,
										["g_dmg"] = 0,
										["counter"] = 17,
										["total"] = 5867,
										["c_max"] = 497,
										["id"] = 2120,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 480,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 10,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 13,
										["b_amt"] = 0,
										["c_dmg"] = 707,
										["g_amt"] = 0,
										["n_max"] = 28,
										["targets"] = {
											["Thorny Stabber"] = 317,
											["Thorny Leafling"] = 1467,
										},
										["n_dmg"] = 1077,
										["n_min"] = 25,
										["g_dmg"] = 0,
										["counter"] = 54,
										["total"] = 1784,
										["c_max"] = 56,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 53,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 41,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[257542] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 733,
										["g_amt"] = 0,
										["n_max"] = 212,
										["targets"] = {
											["Thorny Stabber"] = 733,
											["Thorny Leafling"] = 424,
										},
										["n_dmg"] = 424,
										["n_min"] = 212,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1157,
										["c_max"] = 733,
										["id"] = 257542,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 733,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[108853] = {
										["c_amt"] = 4,
										["b_amt"] = 0,
										["c_dmg"] = 2522,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Thorny Stabber"] = 2522,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 2522,
										["c_max"] = 641,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 624,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 62,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Thorny Leafling"] = 118,
										},
										["n_dmg"] = 56,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 118,
										["c_max"] = 38,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 24,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Thorny Leafling"] = 349,
											["Twilight Wasp"] = 7,
											["Thorny Stabber"] = 81,
										},
										["n_dmg"] = 437,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 55,
										["total"] = 437,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 55,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[31661] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 937,
										["g_amt"] = 0,
										["n_max"] = 238,
										["targets"] = {
											["Thorny Stabber"] = 701,
											["Thorny Leafling"] = 942,
										},
										["n_dmg"] = 706,
										["n_min"] = 231,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 1643,
										["c_max"] = 474,
										["id"] = 31661,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 463,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 772.6886859240437,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 4497.001918,
							["start_time"] = 1618674811,
							["delay"] = 0,
							["last_event"] = 1618674827,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 1029.00265,
							["total"] = 2740.00265,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 2740,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 2740.00265,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674828,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Thorny Leafling",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 212,
										["g_amt"] = 0,
										["n_max"] = 138,
										["targets"] = {
											["Thordren"] = 2740,
										},
										["n_dmg"] = 2528,
										["n_min"] = 51,
										["g_dmg"] = 0,
										["counter"] = 40,
										["total"] = 2740,
										["c_max"] = 109,
										["id"] = 1,
										["r_dmg"] = 0,
										["DODGE"] = 1,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 72,
										["c_min"] = 103,
										["successful_casted"] = 0,
										["a_amt"] = 1,
										["n_amt"] = 37,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[174732] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 174732,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 6,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["aID"] = "85809",
							["serial"] = "Creature-0-4215-1116-69-85809-00007AF854",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674823,
							["damage_taken"] = 9243.00265,
							["start_time"] = 1618674808,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [3]
						{
							["flag_original"] = 2600,
							["totalabsorbed"] = 0.002877,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.002877,
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1618674828,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Twilight Wasp",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["aID"] = "88427",
							["total"] = 0.002877,
							["serial"] = "Creature-0-4215-1116-69-88427-00007AFF64",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 7.002877,
							["start_time"] = 1618674828,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [4]
					},
				}, -- [1]
				{
					["combatId"] = 3535,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 0.008362,
							["total_without_pet"] = 2169.008362,
							["total"] = 2169.008362,
							["targets_absorbs"] = {
								["Thordren"] = 1462,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 1462.008362,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 2169,
							},
							["totalover_without_pet"] = 0.008362,
							["healing_taken"] = 2169.008362,
							["fight_component"] = true,
							["end_time"] = 1618674828,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 236,
										["targets"] = {
											["Thordren"] = 707,
										},
										["n_min"] = 235,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 707,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 707,
										["totaldenied"] = 0,
										["n_amt"] = 3,
										["absorbed"] = 0,
									},
									[235313] = {
										["c_amt"] = 0,
										["totalabsorb"] = 1462,
										["targets_overheal"] = {
										},
										["n_max"] = 154,
										["targets"] = {
											["Thordren"] = 1462,
										},
										["n_min"] = 42,
										["counter"] = 18,
										["overheal"] = 0,
										["total"] = 1462,
										["c_max"] = 0,
										["id"] = 235313,
										["targets_absorbs"] = {
											["Thordren"] = 1462,
										},
										["n_amt"] = 18,
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 1462,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674808,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.008362,
							["delay"] = 0,
							["last_event"] = 1618674823,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3535,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 234.007899,
							["resource"] = 0.007899,
							["targets"] = {
								["Thordren"] = 234,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.007899,
							["fight_component"] = true,
							["total"] = 234.007899,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 234,
										["id"] = 59914,
										["totalover"] = 80,
										["targets"] = {
											["Thordren"] = 234,
										},
										["counter"] = 2,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007899,
							["last_event"] = 1618674827,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 80.007899,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3535,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[31661] = {
										["activedamt"] = 0,
										["id"] = 31661,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 5,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[2120] = {
										["activedamt"] = 6,
										["id"] = 2120,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 9,
										["refreshamt"] = 8,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 1,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 15,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[12654] = {
										["activedamt"] = 7,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 10,
										["refreshamt"] = 13,
										["actived"] = false,
										["counter"] = 0,
									},
									[171389] = {
										["activedamt"] = 0,
										["id"] = 171389,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 71,
							["cc_done_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[31661] = {
										["id"] = 31661,
										["targets"] = {
											["Thorny Leafling"] = 3,
											["Thorny Stabber"] = 2,
										},
										["counter"] = 5,
									},
								},
							},
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 20,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 2,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 20,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 20,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[229376] = {
										["activedamt"] = 1,
										["id"] = 229376,
										["targets"] = {
										},
										["actived_at"] = 1618674808,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[235313] = {
										["activedamt"] = 1,
										["id"] = 235313,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 2,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 45,
							["debuff_uptime_targets"] = {
							},
							["cc_done"] = 5.00528,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[108853] = 4,
								[2120] = 2,
								[257541] = 1,
								[171389] = 1,
								[235313] = 1,
								[31661] = 1,
							},
							["cc_done_targets"] = {
								["Thorny Leafling"] = 3,
								["Thorny Stabber"] = 2,
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674828,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
						{
							["monster"] = true,
							["nome"] = "Thorny Leafling",
							["flag_original"] = 2632,
							["spell_cast"] = {
								[174732] = 6,
							},
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["last_event"] = 0,
							["pets"] = {
							},
							["tipo"] = 4,
							["serial"] = "Creature-0-4215-1116-69-85809-00007AFEE4",
							["aID"] = "85809",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 3535,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["tempo_start"] = 1618674808,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4837,
				["playing_solo"] = true,
				["totals"] = {
					19022, -- [1]
					2169, -- [2]
					{
						0, -- [1]
						[0] = 234,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					14525, -- [1]
					2169, -- [2]
					{
						0, -- [1]
						[0] = 234,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:53:48",
				["cleu_timeline"] = {
				},
				["enemy"] = "Thorny Stabber",
				["TotalElapsedCombatTime"] = 7660.705,
				["CombatEndedAt"] = 7660.705,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 2169.008362,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 14525.001918,
						}, -- [1]
					},
				},
				["end_time"] = 7660.705,
				["combat_id"] = 3535,
				["cleu_events"] = {
					["n"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["player_last_events"] = {
				},
				["data_inicio"] = "11:53:29",
				["CombatSkillCache"] = {
				},
				["frags"] = {
					["Twilight Wasp"] = 1,
					["Thorny Stabber"] = 2,
				},
				["start_time"] = 7641.208000000001,
				["TimeData"] = {
				},
				["contra"] = "Thorny Stabber",
			}, -- [3]
			{
				{
					["combatId"] = 3534,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006079,
							["damage_from"] = {
								["Weald Stinger"] = true,
								["Thorny Leafling"] = true,
								["Thorny Stabber"] = true,
							},
							["targets"] = {
								["Weald Stinger"] = 3049,
								["Thorny Leafling"] = 2737,
								["Thorny Stabber"] = 5517,
							},
							["pets"] = {
							},
							["total"] = 11303.006079,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 11303.006079,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674791,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[108853] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 2443,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Weald Stinger"] = 650,
											["Thorny Stabber"] = 1793,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 2443,
										["c_max"] = 903,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 650,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2120] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 350,
										["targets"] = {
											["Thorny Leafling"] = 690,
											["Thorny Stabber"] = 1039,
										},
										["n_dmg"] = 1729,
										["n_min"] = 342,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 1729,
										["c_max"] = 0,
										["id"] = 2120,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 5,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[11366] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1110,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Weald Stinger"] = 1110,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1110,
										["c_max"] = 1110,
										["id"] = 11366,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 1110,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[257542] = {
										["c_amt"] = 4,
										["b_amt"] = 0,
										["c_dmg"] = 2611,
										["g_amt"] = 0,
										["n_max"] = 508,
										["targets"] = {
											["Weald Stinger"] = 1099,
											["Thorny Leafling"] = 1560,
											["Thorny Stabber"] = 2235,
										},
										["n_dmg"] = 2283,
										["n_min"] = 227,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 4894,
										["c_max"] = 1000,
										["id"] = 257542,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 452,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 7,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Thorny Stabber"] = 54,
										},
										["n_dmg"] = 54,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 54,
										["c_max"] = 0,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 45,
										["targets"] = {
											["Weald Stinger"] = 89,
											["Thorny Leafling"] = 111,
											["Thorny Stabber"] = 129,
										},
										["n_dmg"] = 329,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 40,
										["total"] = 329,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["spellschool"] = 48,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 40,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 25,
										["targets"] = {
											["Weald Stinger"] = 101,
											["Thorny Leafling"] = 148,
											["Thorny Stabber"] = 191,
										},
										["n_dmg"] = 440,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 27,
										["total"] = 440,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 27,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 151,
										["g_amt"] = 0,
										["n_max"] = 39,
										["targets"] = {
											["Thorny Stabber"] = 76,
											["Thorny Leafling"] = 228,
										},
										["n_dmg"] = 153,
										["n_min"] = 38,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 304,
										["c_max"] = 77,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 74,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 954.4039583719731,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 1421.006079,
							["start_time"] = 1618674779,
							["delay"] = 0,
							["last_event"] = 1618674791,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.008452,
							["total"] = 826.008452,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 826,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 826.008452,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674791,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Thorny Stabber",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 316,
										["g_amt"] = 0,
										["n_max"] = 156,
										["targets"] = {
											["Thordren"] = 826,
										},
										["n_dmg"] = 510,
										["n_min"] = 117,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 826,
										["c_max"] = 316,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 316,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "80744",
							["serial"] = "Creature-0-4215-1116-69-80744-00007AF7D8",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674786,
							["damage_taken"] = 5517.008452,
							["start_time"] = 1618674781,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.00489,
							["total"] = 363.00489,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 363,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 363.00489,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674791,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Thorny Leafling",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 253,
										["g_amt"] = 0,
										["n_max"] = 56,
										["targets"] = {
											["Thordren"] = 363,
										},
										["n_dmg"] = 110,
										["n_min"] = 54,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 363,
										["c_max"] = 140,
										["id"] = 1,
										["r_dmg"] = 0,
										["DODGE"] = 1,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 113,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[174732] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 174732,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 2,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["aID"] = "85809",
							["serial"] = "Creature-0-4215-1116-69-85809-00007AF6A4",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674808,
							["damage_taken"] = 2737.00489,
							["start_time"] = 1618674783,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [3]
						{
							["flag_original"] = 2600,
							["totalabsorbed"] = 0.005516,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 232,
							},
							["pets"] = {
							},
							["total"] = 232.005516,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 232.005516,
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1618674791,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Weald Stinger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 120,
										["targets"] = {
											["Thordren"] = 232,
										},
										["n_dmg"] = 232,
										["n_min"] = 112,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 232,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "85807",
							["serial"] = "Creature-0-4215-1116-69-85807-00007AF7BA",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674788,
							["damage_taken"] = 3049.005516,
							["start_time"] = 1618674786,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [4]
					},
				}, -- [1]
				{
					["combatId"] = 3534,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thordren"] = 129,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 129.003679,
							["total_without_pet"] = 364.003679,
							["total"] = 364.003679,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.003679,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 364,
							},
							["totalover_without_pet"] = 0.003679,
							["healing_taken"] = 364.003679,
							["fight_component"] = true,
							["end_time"] = 1618674791,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 118,
										},
										["n_max"] = 235,
										["targets"] = {
											["Thordren"] = 352,
										},
										["n_min"] = 117,
										["counter"] = 2,
										["overheal"] = 118,
										["total"] = 352,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 352,
										["totaldenied"] = 0,
										["n_amt"] = 2,
										["absorbed"] = 0,
									},
									[259760] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 11,
										},
										["n_max"] = 12,
										["targets"] = {
											["Thordren"] = 12,
										},
										["n_min"] = 12,
										["counter"] = 2,
										["overheal"] = 11,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = 259760,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 12,
										["totaldenied"] = 0,
										["n_amt"] = 2,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674781,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.003679,
							["delay"] = 0,
							["last_event"] = 1618674788,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3534,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 140.007119,
							["resource"] = 0.007119,
							["targets"] = {
								["Thordren"] = 140,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.007119,
							["fight_component"] = true,
							["total"] = 140.007119,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 140,
										["id"] = 59914,
										["totalover"] = 331,
										["targets"] = {
											["Thordren"] = 140,
										},
										["counter"] = 3,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007119,
							["last_event"] = 1618674791,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 331.007119,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3534,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 2,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 5,
										["refreshamt"] = 14,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 0,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[2120] = {
										["activedamt"] = 1,
										["id"] = 2120,
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 3,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 63,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[116014] = {
										["activedamt"] = 1,
										["id"] = 116014,
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 3,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 3,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[256374] = {
										["activedamt"] = 1,
										["id"] = 256374,
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 2,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 30,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[11366] = 1,
								[108853] = 2,
								[2120] = 2,
								[257541] = 3,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674791,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
						{
							["monster"] = true,
							["nome"] = "Thorny Leafling",
							["flag_original"] = 2632,
							["spell_cast"] = {
								[174732] = 2,
							},
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["last_event"] = 0,
							["pets"] = {
							},
							["tipo"] = 4,
							["serial"] = "Creature-0-4215-1116-69-85809-00007AF6A4",
							["aID"] = "85809",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 3534,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7638.781,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4836,
				["playing_solo"] = true,
				["totals"] = {
					12724, -- [1]
					364, -- [2]
					{
						0, -- [1]
						[0] = 140,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:53:11",
				["cleu_timeline"] = {
				},
				["enemy"] = "Thorny Stabber",
				["TotalElapsedCombatTime"] = 7623.784000000001,
				["CombatEndedAt"] = 7623.784000000001,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:53:00",
				["end_time"] = 7623.784000000001,
				["combat_id"] = 3534,
				["tempo_start"] = 1618674779,
				["cleu_events"] = {
					["n"] = 1,
				},
				["frags"] = {
					["Weald Stinger"] = 1,
					["Thorny Stabber"] = 2,
				},
				["totals_grupo"] = {
					11303, -- [1]
					364, -- [2]
					{
						0, -- [1]
						[0] = 140,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 364.003679,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 11303.006079,
						}, -- [1]
					},
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 7611.941,
				["contra"] = "Thorny Stabber",
				["TimeData"] = {
				},
			}, -- [4]
			{
				{
					["combatId"] = 3533,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008911,
							["damage_from"] = {
								["Podling Scavenger"] = true,
							},
							["targets"] = {
								["Podling Scavenger"] = 4104,
							},
							["pets"] = {
							},
							["total"] = 4104.008911,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4104.008911,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674722,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[235314] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 139,
										["g_amt"] = 0,
										["n_max"] = 70,
										["targets"] = {
											["Podling Scavenger"] = 763,
										},
										["n_dmg"] = 624,
										["n_min"] = 67,
										["g_dmg"] = 0,
										["counter"] = 10,
										["total"] = 763,
										["c_max"] = 139,
										["id"] = 235314,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 139,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 9,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2120] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 344,
										["targets"] = {
											["Podling Scavenger"] = 1355,
										},
										["n_dmg"] = 1355,
										["n_min"] = 334,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1355,
										["c_max"] = 0,
										["id"] = 2120,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[122] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 25,
										["targets"] = {
											["Podling Scavenger"] = 50,
										},
										["n_dmg"] = 50,
										["n_min"] = 25,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 50,
										["c_max"] = 0,
										["id"] = 122,
										["r_dmg"] = 0,
										["spellschool"] = 16,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 74,
										["g_amt"] = 0,
										["n_max"] = 38,
										["targets"] = {
											["Podling Scavenger"] = 148,
										},
										["n_dmg"] = 74,
										["n_min"] = 36,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 148,
										["c_max"] = 74,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 74,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 9,
										["targets"] = {
											["Podling Scavenger"] = 25,
										},
										["n_dmg"] = 25,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 25,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 5,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[108853] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1763,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Podling Scavenger"] = 1763,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1763,
										["c_max"] = 889,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 874,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 558.3685593197002,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 603.008911,
							["start_time"] = 1618674715,
							["delay"] = 0,
							["last_event"] = 1618674721,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 494.005823,
							["total"] = 603.005823,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 603,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 603.005823,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674722,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Podling Scavenger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 63,
										["targets"] = {
											["Thordren"] = 603,
										},
										["n_dmg"] = 603,
										["n_min"] = 43,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 603,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 46,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 1,
										["n_amt"] = 11,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "84402",
							["serial"] = "Creature-0-4215-1116-69-84402-00007AFFE6",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674721,
							["damage_taken"] = 4104.005823,
							["start_time"] = 1618674715,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 3533,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 0.008378,
							["total_without_pet"] = 809.008378,
							["total"] = 809.008378,
							["targets_absorbs"] = {
								["Thordren"] = 573,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 573.008378,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 809,
							},
							["totalover_without_pet"] = 0.008378,
							["healing_taken"] = 809.008378,
							["fight_component"] = true,
							["end_time"] = 1618674722,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[235313] = {
										["c_amt"] = 0,
										["totalabsorb"] = 573,
										["targets_overheal"] = {
										},
										["n_max"] = 63,
										["targets"] = {
											["Thordren"] = 573,
										},
										["n_min"] = 16,
										["counter"] = 11,
										["overheal"] = 0,
										["total"] = 573,
										["c_max"] = 0,
										["id"] = 235313,
										["targets_absorbs"] = {
											["Thordren"] = 573,
										},
										["n_amt"] = 11,
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 573,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 236,
										["targets"] = {
											["Thordren"] = 236,
										},
										["n_min"] = 236,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 236,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 236,
										["totaldenied"] = 0,
										["n_amt"] = 1,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674715,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.008378,
							["delay"] = 0,
							["last_event"] = 1618674721,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3533,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 216.002617,
							["resource"] = 0.002617,
							["targets"] = {
								["Thordren"] = 216,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.002617,
							["fight_component"] = true,
							["total"] = 216.002617,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 216,
										["id"] = 59914,
										["totalover"] = 219,
										["targets"] = {
											["Thordren"] = 216,
										},
										["counter"] = 3,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002617,
							["last_event"] = 1618674721,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 219.002617,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3533,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 3,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
									[122] = {
										["activedamt"] = 0,
										["id"] = 122,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[2120] = {
										["activedamt"] = 0,
										["id"] = 2120,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 2,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 37,
							["cc_done_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[122] = {
										["id"] = 122,
										["targets"] = {
											["Podling Scavenger"] = 2,
										},
										["counter"] = 2,
									},
								},
							},
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 1,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 1,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[235313] = {
										["activedamt"] = 1,
										["id"] = 235313,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[116014] = {
										["activedamt"] = 1,
										["id"] = 116014,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 9,
							["cc_done_targets"] = {
								["Podling Scavenger"] = 2,
							},
							["cc_done"] = 2.005082,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[108853] = 2,
								[2120] = 2,
								[122] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674722,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 3533,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7611.941,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4835,
				["playing_solo"] = true,
				["totals"] = {
					4707, -- [1]
					809, -- [2]
					{
						0, -- [1]
						[0] = 216,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:52:02",
				["cleu_timeline"] = {
				},
				["enemy"] = "Podling Scavenger",
				["TotalElapsedCombatTime"] = 7554.694,
				["CombatEndedAt"] = 7554.694,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:51:55",
				["end_time"] = 7554.694,
				["combat_id"] = 3533,
				["tempo_start"] = 1618674715,
				["cleu_events"] = {
					["n"] = 1,
				},
				["frags"] = {
					["Podling Scavenger"] = 3,
				},
				["totals_grupo"] = {
					4104, -- [1]
					809, -- [2]
					{
						0, -- [1]
						[0] = 216,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 809.008378,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 4104.008911,
						}, -- [1]
					},
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 7547.344,
				["contra"] = "Podling Scavenger",
				["TimeData"] = {
				},
			}, -- [5]
			{
				{
					["combatId"] = 3532,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003252,
							["damage_from"] = {
								["Podling Scavenger"] = true,
								["Harvester Ommru"] = true,
							},
							["targets"] = {
								["Harvester Ommru"] = 14190,
								["Podling Scavenger"] = 3629,
							},
							["pets"] = {
							},
							["total"] = 17819.003252,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 17819.003252,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674699,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[257542] = {
										["c_amt"] = 5,
										["b_amt"] = 0,
										["c_dmg"] = 2164,
										["g_amt"] = 0,
										["n_max"] = 361,
										["targets"] = {
											["Podling Scavenger"] = 2327,
											["Harvester Ommru"] = 1810,
										},
										["n_dmg"] = 1973,
										["n_min"] = 175,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 4137,
										["c_max"] = 730,
										["id"] = 257542,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 354,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 8,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[108853] = {
										["c_amt"] = 6,
										["b_amt"] = 0,
										["c_dmg"] = 3824,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Harvester Ommru"] = 3824,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 3824,
										["c_max"] = 647,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 621,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 149,
										["g_amt"] = 0,
										["n_max"] = 74,
										["targets"] = {
											["Harvester Ommru"] = 391,
										},
										["n_dmg"] = 242,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 391,
										["c_max"] = 149,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 149,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[226757] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 40,
										["g_amt"] = 0,
										["n_max"] = 7,
										["targets"] = {
											["Harvester Ommru"] = 94,
										},
										["n_dmg"] = 54,
										["n_min"] = 5,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 94,
										["c_max"] = 14,
										["id"] = 226757,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 12,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 8,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2120] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 474,
										["g_amt"] = 0,
										["n_max"] = 244,
										["targets"] = {
											["Podling Scavenger"] = 718,
											["Harvester Ommru"] = 237,
										},
										["n_dmg"] = 481,
										["n_min"] = 237,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 955,
										["c_max"] = 474,
										["id"] = 2120,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 474,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 53,
										["g_amt"] = 0,
										["n_max"] = 26,
										["targets"] = {
											["Podling Scavenger"] = 78,
											["Harvester Ommru"] = 130,
										},
										["n_dmg"] = 155,
										["n_min"] = 25,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 208,
										["c_max"] = 53,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 53,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[235314] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 50,
										["targets"] = {
											["Harvester Ommru"] = 100,
										},
										["n_dmg"] = 100,
										["n_min"] = 50,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 100,
										["c_max"] = 0,
										["id"] = 235314,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 31,
										["targets"] = {
											["Harvester Ommru"] = 148,
										},
										["n_dmg"] = 148,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 25,
										["total"] = 148,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["spellschool"] = 48,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 25,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 52,
										["targets"] = {
											["Harvester Ommru"] = 1177,
											["Podling Scavenger"] = 506,
										},
										["n_dmg"] = 1683,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 41,
										["total"] = 1683,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 41,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[11366] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 3289,
										["g_amt"] = 0,
										["n_max"] = 541,
										["targets"] = {
											["Harvester Ommru"] = 3830,
										},
										["n_dmg"] = 541,
										["n_min"] = 541,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 3830,
										["c_max"] = 1109,
										["id"] = 11366,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 1081,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2948] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Harvester Ommru"] = 72,
										},
										["n_dmg"] = 72,
										["n_min"] = 72,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 72,
										["c_max"] = 0,
										["id"] = 2948,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[133] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1171,
										["g_amt"] = 0,
										["n_max"] = 305,
										["targets"] = {
											["Harvester Ommru"] = 2377,
										},
										["n_dmg"] = 1206,
										["n_min"] = 293,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 2377,
										["c_max"] = 589,
										["id"] = 133,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 582,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 560.3636357118153,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 3023.003252,
							["start_time"] = 1618674667,
							["delay"] = 0,
							["last_event"] = 1618674698,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 871.008418,
							["total"] = 2748.008418,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 2748,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 2748.008418,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674699,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Harvester Ommru",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 272,
										["targets"] = {
											["Thordren"] = 1592,
										},
										["n_dmg"] = 1592,
										["n_min"] = 195,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 1592,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["MISS"] = 1,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 7,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[169571] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 370,
										["targets"] = {
											["Thordren"] = 1089,
										},
										["n_dmg"] = 1089,
										["n_min"] = 350,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1089,
										["c_max"] = 0,
										["id"] = 169571,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 3,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[173861] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 67,
										["targets"] = {
											["Thordren"] = 67,
										},
										["n_dmg"] = 67,
										["n_min"] = 67,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 67,
										["c_max"] = 0,
										["id"] = 173861,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["aID"] = "84373",
							["serial"] = "Creature-0-4215-1116-69-84373-00007AFF00",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674697,
							["damage_taken"] = 14190.008418,
							["start_time"] = 1618674673,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 63.00333,
							["total"] = 275.00333,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 275,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 275.00333,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674699,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Podling Scavenger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 63,
										["targets"] = {
											["Thordren"] = 275,
										},
										["n_dmg"] = 275,
										["n_min"] = 46,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 275,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 5,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "84402",
							["serial"] = "Creature-0-4215-1116-69-84402-00017AFF6A",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674715,
							["damage_taken"] = 3629.00333,
							["start_time"] = 1618674697,
							["delay"] = 1618674673,
							["tipo"] = 1,
						}, -- [3]
					},
				}, -- [1]
				{
					["combatId"] = 3532,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thordren"] = 62,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 62.003268,
							["total_without_pet"] = 1529.003268,
							["total"] = 1529.003268,
							["targets_absorbs"] = {
								["Thordren"] = 871,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 871.0032679999999,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 1529,
							},
							["totalover_without_pet"] = 0.003268,
							["healing_taken"] = 1529.003268,
							["fight_component"] = true,
							["end_time"] = 1618674699,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[235313] = {
										["c_amt"] = 0,
										["totalabsorb"] = 871,
										["targets_overheal"] = {
										},
										["n_max"] = 350,
										["targets"] = {
											["Thordren"] = 871,
										},
										["n_min"] = 249,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 871,
										["c_max"] = 0,
										["id"] = 235313,
										["targets_absorbs"] = {
											["Thordren"] = 871,
										},
										["n_amt"] = 3,
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 871,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
									[259760] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 12,
										["targets"] = {
											["Thordren"] = 12,
										},
										["n_min"] = 12,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = 259760,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 12,
										["totaldenied"] = 0,
										["n_amt"] = 1,
										["absorbed"] = 0,
									},
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 62,
										},
										["n_max"] = 236,
										["targets"] = {
											["Thordren"] = 646,
										},
										["n_min"] = 174,
										["counter"] = 3,
										["overheal"] = 62,
										["total"] = 646,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 646,
										["totaldenied"] = 0,
										["n_amt"] = 3,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674672,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.003268,
							["delay"] = 0,
							["last_event"] = 1618674697,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3532,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 369.002393,
							["resource"] = 0.002393,
							["targets"] = {
								["Thordren"] = 369,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.002393,
							["fight_component"] = true,
							["total"] = 369.002393,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 369,
										["id"] = 59914,
										["totalover"] = 211,
										["targets"] = {
											["Thordren"] = 369,
										},
										["counter"] = 4,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002393,
							["last_event"] = 1618674698,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 211.002393,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3532,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 31,
										["appliedamt"] = 4,
										["refreshamt"] = 29,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 0,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 22,
										["appliedamt"] = 2,
										["refreshamt"] = 3,
										["actived"] = false,
										["counter"] = 0,
									},
									[226757] = {
										["activedamt"] = 0,
										["id"] = 226757,
										["targets"] = {
										},
										["uptime"] = 20,
										["appliedamt"] = 1,
										["refreshamt"] = 5,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 143,
							["aID"] = "76-09A108A3",
							["interrupt_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[2139] = {
										["id"] = 2139,
										["interrompeu_oque"] = {
											[173861] = 1,
										},
										["targets"] = {
											["Harvester Ommru"] = 1,
										},
										["counter"] = 1,
									},
								},
							},
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[157644] = {
										["activedamt"] = 2,
										["id"] = 157644,
										["targets"] = {
										},
										["uptime"] = 11,
										["appliedamt"] = 2,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 32,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 8,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 8,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 4,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 4,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[235313] = {
										["activedamt"] = 1,
										["id"] = 235313,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 32,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 32,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236060] = {
										["activedamt"] = 1,
										["id"] = 236060,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[256374] = {
										["activedamt"] = 1,
										["id"] = 256374,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["interrompeu_oque"] = {
								[173861] = 1,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 73,
							["interrupt_targets"] = {
								["Harvester Ommru"] = 1,
							},
							["interrupt"] = 1.003357,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[108853] = 5,
								[2120] = 1,
								[257541] = 4,
								[2948] = 1,
								[235313] = 1,
								[2139] = 1,
								[11366] = 3,
								[133] = 6,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674699,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
						{
							["monster"] = true,
							["nome"] = "Harvester Ommru",
							["flag_original"] = 68168,
							["spell_cast"] = {
								[169571] = 3,
								[173861] = 1,
							},
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["last_event"] = 0,
							["pets"] = {
							},
							["tipo"] = 4,
							["serial"] = "Creature-0-4215-1116-69-84373-00007AFF00",
							["aID"] = "84373",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 3532,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7546.108,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4834,
				["playing_solo"] = true,
				["totals"] = {
					20841.994945, -- [1]
					1529, -- [2]
					{
						0, -- [1]
						[0] = 369,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 1,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:51:40",
				["cleu_timeline"] = {
				},
				["enemy"] = "Harvester Ommru",
				["TotalElapsedCombatTime"] = 7531.83,
				["CombatEndedAt"] = 7531.83,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:51:08",
				["end_time"] = 7531.83,
				["combat_id"] = 3532,
				["tempo_start"] = 1618674667,
				["cleu_events"] = {
					["n"] = 1,
				},
				["frags"] = {
					["Podling Scavenger"] = 3,
					["Harvester Ommru"] = 1,
				},
				["totals_grupo"] = {
					17819, -- [1]
					1529, -- [2]
					{
						0, -- [1]
						[0] = 369,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 1,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 1529.003268,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 17819.003252,
						}, -- [1]
					},
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 7500.031,
				["contra"] = "Harvester Ommru",
				["TimeData"] = {
				},
			}, -- [6]
			{
				{
					["combatId"] = 3531,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007597,
							["damage_from"] = {
								["Podling Scavenger"] = true,
							},
							["targets"] = {
								["Podling Scavenger"] = 3702,
								["Aggressive Growth"] = 631,
							},
							["pets"] = {
							},
							["total"] = 4333.007597,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4333.007597,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674632,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[31661] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 230,
										["targets"] = {
											["Podling Scavenger"] = 456,
										},
										["n_dmg"] = 456,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 456,
										["c_max"] = 0,
										["id"] = 31661,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2120] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 977,
										["g_amt"] = 0,
										["n_max"] = 247,
										["targets"] = {
											["Podling Scavenger"] = 1224,
										},
										["n_dmg"] = 247,
										["n_min"] = 247,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1224,
										["c_max"] = 494,
										["id"] = 2120,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 483,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 38,
										["targets"] = {
											["Podling Scavenger"] = 76,
										},
										["n_dmg"] = 76,
										["n_min"] = 38,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 76,
										["c_max"] = 0,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2948] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 72,
										["targets"] = {
											["Podling Scavenger"] = 283,
										},
										["n_dmg"] = 283,
										["n_min"] = 69,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 283,
										["c_max"] = 0,
										["id"] = 2948,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 2,
										["targets"] = {
											["Podling Scavenger"] = 24,
										},
										["n_dmg"] = 24,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 18,
										["total"] = 24,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["spellschool"] = 48,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 18,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 106,
										["g_amt"] = 0,
										["n_max"] = 26,
										["targets"] = {
											["Podling Scavenger"] = 209,
										},
										["n_dmg"] = 103,
										["n_min"] = 25,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 209,
										["c_max"] = 53,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 53,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 12,
										["targets"] = {
											["Podling Scavenger"] = 160,
										},
										["n_dmg"] = 160,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 22,
										["total"] = 160,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 22,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[108853] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 1901,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Podling Scavenger"] = 1270,
											["Aggressive Growth"] = 631,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1901,
										["c_max"] = 643,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 627,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 262.4633591980129,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 768.007597,
							["start_time"] = 1618674616,
							["delay"] = 0,
							["last_event"] = 1618674632,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.003453,
							["total"] = 768.003453,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 768,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 768.003453,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674632,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Podling Scavenger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 212,
										["g_amt"] = 0,
										["n_max"] = 64,
										["targets"] = {
											["Thordren"] = 768,
										},
										["n_dmg"] = 556,
										["n_min"] = 44,
										["g_dmg"] = 0,
										["counter"] = 12,
										["total"] = 768,
										["c_max"] = 114,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 98,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 10,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[169519] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 169519,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 2,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["aID"] = "84402",
							["serial"] = "Creature-0-4215-1116-69-84402-00017AFF30",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674631,
							["damage_taken"] = 3702.003453,
							["start_time"] = 1618674615,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002426,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.002426,
							["fight_component"] = true,
							["monster"] = true,
							["dps_started"] = false,
							["end_time"] = 1618674632,
							["friendlyfire_total"] = 0,
							["aID"] = "",
							["nome"] = "Aggressive Growth",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["total"] = 0.002426,
							["serial"] = "Vehicle-0-4215-1116-69-84451-00007AFE81",
							["friendlyfire"] = {
							},
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 631.002426,
							["start_time"] = 1618674632,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [3]
					},
				}, -- [1]
				{
					["combatId"] = 3531,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thordren"] = 187,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 187.004602,
							["total_without_pet"] = 298.004602,
							["total"] = 298.004602,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.004602,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 298,
							},
							["totalover_without_pet"] = 0.004602,
							["healing_taken"] = 298.004602,
							["fight_component"] = true,
							["end_time"] = 1618674632,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 187,
										},
										["n_max"] = 236,
										["targets"] = {
											["Thordren"] = 286,
										},
										["n_min"] = 50,
										["counter"] = 2,
										["overheal"] = 187,
										["total"] = 286,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 286,
										["totaldenied"] = 0,
										["n_amt"] = 2,
										["absorbed"] = 0,
									},
									[259760] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 12,
										["targets"] = {
											["Thordren"] = 12,
										},
										["n_min"] = 12,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 12,
										["c_max"] = 0,
										["id"] = 259760,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 12,
										["totaldenied"] = 0,
										["n_amt"] = 1,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674626,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.004602,
							["delay"] = 1618674615,
							["last_event"] = 1618674626,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3531,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 291.001074,
							["resource"] = 0.001074,
							["targets"] = {
								["Thordren"] = 291,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.001074,
							["fight_component"] = true,
							["total"] = 291.001074,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 291,
										["id"] = 59914,
										["totalover"] = 289,
										["targets"] = {
											["Thordren"] = 291,
										},
										["counter"] = 4,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.001074,
							["last_event"] = 1618674632,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 289.001074,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3531,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 14,
										["appliedamt"] = 3,
										["refreshamt"] = 4,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 0,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 7,
										["appliedamt"] = 1,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[2120] = {
										["activedamt"] = 0,
										["id"] = 2120,
										["targets"] = {
										},
										["uptime"] = 9,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[31661] = {
										["activedamt"] = 0,
										["id"] = 31661,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 71,
							["cc_done_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[31661] = {
										["id"] = 31661,
										["targets"] = {
											["Podling Scavenger"] = 2,
										},
										["counter"] = 2,
									},
								},
							},
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 17,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 2,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 3,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 17,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 17,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236060] = {
										["activedamt"] = 1,
										["id"] = 236060,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 3,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 1,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[256374] = {
										["activedamt"] = 1,
										["id"] = 256374,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 34,
							["cc_done_targets"] = {
								["Podling Scavenger"] = 2,
							},
							["cc_done"] = 2.003723,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[31661] = 1,
								[108853] = 3,
								[2120] = 1,
								[2948] = 4,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674632,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
						{
							["monster"] = true,
							["nome"] = "Podling Scavenger",
							["flag_original"] = 2632,
							["spell_cast"] = {
								[169519] = 2,
							},
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["last_event"] = 0,
							["pets"] = {
							},
							["tipo"] = 4,
							["serial"] = "Creature-0-4215-1116-69-84402-00007AFFA9",
							["aID"] = "84402",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 3531,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7499.58,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4833,
				["playing_solo"] = true,
				["totals"] = {
					5101, -- [1]
					298, -- [2]
					{
						0, -- [1]
						[0] = 291,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:50:32",
				["cleu_timeline"] = {
				},
				["enemy"] = "Podling Scavenger",
				["TotalElapsedCombatTime"] = 7464.458000000001,
				["CombatEndedAt"] = 7464.458000000001,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:50:16",
				["end_time"] = 7464.458000000001,
				["combat_id"] = 3531,
				["tempo_start"] = 1618674615,
				["cleu_events"] = {
					["n"] = 1,
				},
				["frags"] = {
					["Podling Scavenger"] = 3,
				},
				["totals_grupo"] = {
					4333, -- [1]
					298, -- [2]
					{
						0, -- [1]
						[0] = 291,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 298.004602,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 4333.007597,
						}, -- [1]
					},
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 7447.949000000001,
				["contra"] = "Podling Scavenger",
				["TimeData"] = {
				},
			}, -- [7]
			{
				{
					["combatId"] = 3530,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.001643,
							["total"] = 4068.001643,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 4068,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4068.001643,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674514,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Crimson Mandragora",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 288,
										["targets"] = {
											["Thordren"] = 2188,
										},
										["n_dmg"] = 2188,
										["n_min"] = 193,
										["g_dmg"] = 0,
										["counter"] = 9,
										["total"] = 2188,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 9,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[161533] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 353,
										["targets"] = {
											["Thordren"] = 1043,
										},
										["n_dmg"] = 1043,
										["n_min"] = 340,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1043,
										["c_max"] = 0,
										["id"] = 161533,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 3,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[167129] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 309,
										["targets"] = {
											["Thordren"] = 837,
										},
										["n_dmg"] = 837,
										["n_min"] = 233,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 837,
										["c_max"] = 0,
										["id"] = 167129,
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[161299] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 161299,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["aID"] = "84391",
							["serial"] = "Creature-0-4215-1116-69-84391-00007AFF26",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674512,
							["damage_taken"] = 7270.001643,
							["start_time"] = 1618674487,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006926,
							["damage_from"] = {
								["Crimson Mandragora"] = true,
							},
							["targets"] = {
								["Crimson Mandragora"] = 7270,
							},
							["pets"] = {
							},
							["total"] = 7270.006926,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 7270.006926,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674514,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[108853] = {
										["c_amt"] = 4,
										["b_amt"] = 0,
										["c_dmg"] = 2542,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Crimson Mandragora"] = 2542,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 2542,
										["c_max"] = 645,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 621,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[133] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 299,
										["targets"] = {
											["Crimson Mandragora"] = 299,
										},
										["n_dmg"] = 299,
										["n_min"] = 299,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 299,
										["c_max"] = 0,
										["id"] = 133,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 53,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Crimson Mandragora"] = 53,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 53,
										["c_max"] = 53,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 53,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[226757] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 12,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Crimson Mandragora"] = 31,
										},
										["n_dmg"] = 19,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 31,
										["c_max"] = 12,
										["id"] = 226757,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 12,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Crimson Mandragora"] = 104,
										},
										["n_dmg"] = 104,
										["n_min"] = 12,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 104,
										["c_max"] = 0,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[11366] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 2170,
										["g_amt"] = 0,
										["n_max"] = 546,
										["targets"] = {
											["Crimson Mandragora"] = 3794,
										},
										["n_dmg"] = 1624,
										["n_min"] = 532,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 3794,
										["c_max"] = 1093,
										["id"] = 11366,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 1077,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 35,
										["targets"] = {
											["Crimson Mandragora"] = 447,
										},
										["n_dmg"] = 447,
										["n_min"] = 24,
										["g_dmg"] = 0,
										["counter"] = 16,
										["total"] = 447,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 16,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 280.9881701387591,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 4068.006926,
							["start_time"] = 1618674496,
							["delay"] = 0,
							["last_event"] = 1618674513,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 3530,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 0.005138,
							["total_without_pet"] = 1652.005138,
							["total"] = 1652.005138,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.005138,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 1652,
							},
							["totalover_without_pet"] = 0.005138,
							["healing_taken"] = 1652.005138,
							["fight_component"] = true,
							["end_time"] = 1618674514,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 3,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 236,
										["targets"] = {
											["Thordren"] = 1652,
										},
										["n_min"] = 236,
										["counter"] = 4,
										["overheal"] = 0,
										["total"] = 1652,
										["c_max"] = 472,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 472,
										["c_curado"] = 1416,
										["n_curado"] = 236,
										["totaldenied"] = 0,
										["n_amt"] = 1,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674487,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.005138,
							["delay"] = 0,
							["last_event"] = 1618674508,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3530,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 145.005192,
							["resource"] = 0.005192,
							["targets"] = {
								["Thordren"] = 145,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.005192,
							["fight_component"] = true,
							["total"] = 145.005192,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 145,
										["id"] = 59914,
										["totalover"] = 0,
										["targets"] = {
											["Thordren"] = 145,
										},
										["counter"] = 1,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.005192,
							["last_event"] = 1618674513,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 0.005192,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3530,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 17,
										["appliedamt"] = 1,
										["refreshamt"] = 8,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 0,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 15,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[226757] = {
										["activedamt"] = 0,
										["id"] = 226757,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 103,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 27,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 4,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 8,
										["appliedamt"] = 4,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 27,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 27,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 2,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[157644] = {
										["activedamt"] = 1,
										["id"] = 157644,
										["targets"] = {
										},
										["uptime"] = 13,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 40,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[30449] = 2,
								[11366] = 5,
								[133] = 1,
								[108853] = 4,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674514,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
						{
							["monster"] = true,
							["nome"] = "Crimson Mandragora",
							["flag_original"] = 68168,
							["spell_cast"] = {
								[161533] = 3,
								[161299] = 1,
							},
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["last_event"] = 0,
							["pets"] = {
							},
							["tipo"] = 4,
							["serial"] = "Creature-0-4215-1116-69-84391-00007AFF26",
							["aID"] = "84391",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 3530,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7446.230000000001,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4832,
				["playing_solo"] = true,
				["totals"] = {
					11338, -- [1]
					1652, -- [2]
					{
						0, -- [1]
						[0] = 145,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:48:34",
				["cleu_timeline"] = {
				},
				["enemy"] = "Crimson Mandragora",
				["TotalElapsedCombatTime"] = 7346.462,
				["CombatEndedAt"] = 7346.462,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:48:07",
				["end_time"] = 7346.462,
				["combat_id"] = 3530,
				["tempo_start"] = 1618674487,
				["cleu_events"] = {
					["n"] = 1,
				},
				["frags"] = {
					["Crimson Mandragora"] = 1,
				},
				["totals_grupo"] = {
					7270, -- [1]
					1652, -- [2]
					{
						0, -- [1]
						[0] = 145,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 1652.005138,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 7270.006926,
						}, -- [1]
					},
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 7319.765,
				["contra"] = "Crimson Mandragora",
				["TimeData"] = {
				},
			}, -- [8]
			{
				{
					["combatId"] = 3529,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00795,
							["damage_from"] = {
								["Podling Scavenger"] = true,
								["Podling Trapper"] = true,
							},
							["targets"] = {
								["Podling Scavenger"] = 3486,
								["Podling Trapper"] = 1874,
							},
							["pets"] = {
							},
							["total"] = 5360.00795,
							["on_hold"] = false,
							["aID"] = "76-09A108A3",
							["raid_targets"] = {
							},
							["total_without_pet"] = 5360.00795,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1618674478,
							["classe"] = "MAGE",
							["friendlyfire_total"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[108853] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 1844,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Podling Trapper"] = 1844,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1844,
										["c_max"] = 616,
										["id"] = 108853,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 612,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[2120] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 925,
										["g_amt"] = 0,
										["n_max"] = 236,
										["targets"] = {
											["Podling Scavenger"] = 2307,
										},
										["n_dmg"] = 1382,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 2307,
										["c_max"] = 463,
										["id"] = 2120,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 462,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Podling Scavenger"] = 18,
										},
										["n_dmg"] = 18,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = 280286,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[205345] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 25,
										["targets"] = {
											["Podling Scavenger"] = 75,
										},
										["n_dmg"] = 75,
										["n_min"] = 25,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 75,
										["c_max"] = 0,
										["id"] = 205345,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 21,
										["targets"] = {
											["Podling Scavenger"] = 84,
											["Podling Trapper"] = 2,
										},
										["n_dmg"] = 86,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 15,
										["total"] = 86,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["spellschool"] = 48,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 15,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 14,
										["targets"] = {
											["Podling Scavenger"] = 127,
											["Podling Trapper"] = 28,
										},
										["n_dmg"] = 155,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 31,
										["total"] = 155,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 31,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[31661] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 432,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Podling Scavenger"] = 875,
										},
										["n_dmg"] = 443,
										["n_min"] = 217,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 875,
										["c_max"] = 432,
										["id"] = 31661,
										["r_dmg"] = 0,
										["spellschool"] = 4,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 432,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["serial"] = "Player-76-09A108A3",
							["last_dps"] = 251.4664766596234,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 1137.00795,
							["start_time"] = 1618674459,
							["delay"] = 0,
							["last_event"] = 1618674477,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.006922,
							["total"] = 871.006922,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 871,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 871.006922,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674478,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Podling Scavenger",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 65,
										["targets"] = {
											["Thordren"] = 672,
										},
										["n_dmg"] = 672,
										["n_min"] = 46,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 672,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["MISS"] = 1,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 12,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[169519] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 169519,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 2,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
									[169522] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 100,
										["targets"] = {
											["Thordren"] = 199,
										},
										["n_dmg"] = 199,
										["n_min"] = 99,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 199,
										["c_max"] = 0,
										["id"] = 169522,
										["r_dmg"] = 0,
										["spellschool"] = 8,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
							},
							["aID"] = "84402",
							["serial"] = "Creature-0-4215-1116-69-84402-00007AF674",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674476,
							["damage_taken"] = 3486.006922,
							["start_time"] = 1618674463,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.00765,
							["total"] = 266.00765,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 266,
							},
							["pets"] = {
							},
							["monster"] = true,
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 266.00765,
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1618674478,
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["nome"] = "Podling Trapper",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 146,
										["targets"] = {
											["Thordren"] = 266,
										},
										["n_dmg"] = 266,
										["n_min"] = 120,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 266,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["MISS"] = 1,
										["spellschool"] = 1,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["b_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
							},
							["aID"] = "84862",
							["serial"] = "Creature-0-4215-1116-69-84862-00007ABDE6",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 1618674460,
							["damage_taken"] = 1874.00765,
							["start_time"] = 1618674473,
							["delay"] = 1618674460,
							["tipo"] = 1,
						}, -- [3]
					},
				}, -- [1]
				{
					["combatId"] = 3529,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thordren"] = 158,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "MAGE",
							["totalover"] = 158.003772,
							["total_without_pet"] = 562.003772,
							["total"] = 562.003772,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.003772,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 562,
							},
							["totalover_without_pet"] = 0.003772,
							["healing_taken"] = 562.003772,
							["fight_component"] = true,
							["end_time"] = 1618674478,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["heal_enemy_amt"] = 0,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 146,
										},
										["n_max"] = 236,
										["targets"] = {
											["Thordren"] = 562,
										},
										["n_min"] = 120,
										["counter"] = 3,
										["overheal"] = 146,
										["total"] = 562,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 562,
										["totaldenied"] = 0,
										["n_amt"] = 3,
										["absorbed"] = 0,
									},
									[259760] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 12,
										},
										["n_max"] = 0,
										["targets"] = {
											["Thordren"] = 0,
										},
										["n_min"] = 0,
										["counter"] = 1,
										["overheal"] = 12,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 259760,
										["targets_absorbs"] = {
										},
										["c_min"] = 0,
										["c_curado"] = 0,
										["n_curado"] = 0,
										["totaldenied"] = 0,
										["n_amt"] = 1,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["start_time"] = 1618674456,
							["spec"] = 63,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["totaldenied"] = 0.003772,
							["delay"] = 0,
							["last_event"] = 1618674472,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3529,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 361.003284,
							["resource"] = 0.003284,
							["targets"] = {
								["Thordren"] = 361,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.003284,
							["fight_component"] = true,
							["total"] = 361.003284,
							["nome"] = "Thordren",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[59914] = {
										["total"] = 361,
										["id"] = 59914,
										["totalover"] = 219,
										["targets"] = {
											["Thordren"] = 361,
										},
										["counter"] = 4,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 63,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003284,
							["last_event"] = 1618674477,
							["aID"] = "76-09A108A3",
							["tipo"] = 3,
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 219.003284,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3529,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[12654] = {
										["activedamt"] = 0,
										["id"] = 12654,
										["targets"] = {
										},
										["uptime"] = 14,
										["appliedamt"] = 4,
										["refreshamt"] = 4,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["activedamt"] = 0,
										["id"] = 280286,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[2120] = {
										["activedamt"] = 0,
										["id"] = 2120,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 3,
										["refreshamt"] = 3,
										["actived"] = false,
										["counter"] = 0,
									},
									[31661] = {
										["activedamt"] = 0,
										["id"] = 31661,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 3,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 63,
							["cc_done_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[31661] = {
										["id"] = 31661,
										["targets"] = {
											["Podling Scavenger"] = 3,
										},
										["counter"] = 3,
									},
								},
							},
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[269083] = {
										["activedamt"] = 1,
										["id"] = 269083,
										["targets"] = {
										},
										["uptime"] = 22,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[48107] = {
										["activedamt"] = 1,
										["id"] = 48107,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186401] = {
										["activedamt"] = 1,
										["id"] = 186401,
										["targets"] = {
										},
										["uptime"] = 22,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[256374] = {
										["activedamt"] = 1,
										["id"] = 256374,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[229376] = {
										["activedamt"] = 1,
										["id"] = 229376,
										["targets"] = {
										},
										["actived_at"] = 1618674456,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[48108] = {
										["activedamt"] = 1,
										["id"] = 48108,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[1459] = {
										["activedamt"] = 1,
										["id"] = 1459,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 34,
							["cc_done_targets"] = {
								["Podling Scavenger"] = 3,
							},
							["cc_done"] = 3.001919,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[31661] = 1,
								[108853] = 3,
								[2120] = 3,
								[1459] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618674478,
							["pets"] = {
							},
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["tipo"] = 4,
						}, -- [1]
						{
							["monster"] = true,
							["nome"] = "Podling Scavenger",
							["flag_original"] = 2632,
							["spell_cast"] = {
								[169519] = 2,
							},
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["last_event"] = 0,
							["pets"] = {
							},
							["tipo"] = 4,
							["serial"] = "Creature-0-4215-1116-69-84402-00017AF674",
							["aID"] = "84402",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 3529,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7318.532,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4831,
				["playing_solo"] = true,
				["totals"] = {
					6497, -- [1]
					562, -- [2]
					{
						0, -- [1]
						[0] = 361,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "11:47:58",
				["cleu_timeline"] = {
				},
				["enemy"] = "Podling Trapper",
				["TotalElapsedCombatTime"] = 7310.385,
				["CombatEndedAt"] = 7310.385,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:47:37",
				["end_time"] = 7310.385,
				["combat_id"] = 3529,
				["tempo_start"] = 1618674456,
				["cleu_events"] = {
					["n"] = 1,
				},
				["frags"] = {
					["Podling Scavenger"] = 3,
					["Podling Trapper"] = 1,
				},
				["totals_grupo"] = {
					5360, -- [1]
					562, -- [2]
					{
						0, -- [1]
						[0] = 361,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 562.003772,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thordren"] = 5360.00795,
						}, -- [1]
					},
				},
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 7289.07,
				["contra"] = "Podling Trapper",
				["TimeData"] = {
				},
			}, -- [9]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3528,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004339,
							["total"] = 3757.004339,
							["damage_from"] = {
								["Midnight Kaliri"] = true,
							},
							["targets"] = {
								["Midnight Kaliri"] = 3757,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["last_dps"] = 310.0606040274412,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 3757.004339,
							["last_event"] = 1618530279,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1618530279,
							["aID"] = "76-09A108A3",
							["damage_taken"] = 1491.004339,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["custom"] = 0,
							["spells"] = {
								["_ActorTable"] = {
									[108853] = {
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 1821,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Midnight Kaliri"] = 1821,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1821,
										["c_max"] = 609,
										["id"] = 108853,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 606,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[2948] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 69,
										["targets"] = {
											["Midnight Kaliri"] = 135,
										},
										["n_dmg"] = 135,
										["n_min"] = 66,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 135,
										["c_max"] = 0,
										["id"] = 2948,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 31,
										["targets"] = {
											["Midnight Kaliri"] = 39,
										},
										["n_dmg"] = 39,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 39,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 7,
										["extra"] = {
										},
										["spellschool"] = 48,
									},
									[11366] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1054,
										["g_amt"] = 0,
										["n_max"] = 537,
										["targets"] = {
											["Midnight Kaliri"] = 1591,
										},
										["n_dmg"] = 537,
										["n_min"] = 537,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1591,
										["c_max"] = 1054,
										["id"] = 11366,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 1054,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 31,
										["targets"] = {
											["Midnight Kaliri"] = 171,
										},
										["n_dmg"] = 171,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 171,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
								},
								["tipo"] = 2,
							},
							["colocacao"] = 1,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1618530272,
							["serial"] = "Player-76-09A108A3",
							["on_hold"] = false,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.003636,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 1491,
							},
							["pets"] = {
							},
							["delay"] = 0,
							["aID"] = "87671",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1491.003636,
							["last_dps"] = 0,
							["tipo"] = 1,
							["monster"] = true,
							["total"] = 1491.003636,
							["classe"] = "UNKNOW",
							["end_time"] = 1618530279,
							["nome"] = "Midnight Kaliri",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 372,
										["g_amt"] = 0,
										["n_max"] = 201,
										["targets"] = {
											["Thordren"] = 1298,
										},
										["n_dmg"] = 926,
										["n_min"] = 159,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 1298,
										["c_max"] = 372,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 372,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 5,
										["extra"] = {
										},
										["spellschool"] = 1,
									}, -- [1]
									[163716] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 193,
										["targets"] = {
											["Thordren"] = 193,
										},
										["n_dmg"] = 193,
										["n_min"] = 193,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 193,
										["c_max"] = 0,
										["id"] = 163716,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["extra"] = {
										},
									},
								},
								["tipo"] = 2,
							},
							["fight_component"] = true,
							["damage_taken"] = 3757.003636,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["custom"] = 0,
							["last_event"] = 1618530277,
							["friendlyfire"] = {
							},
							["start_time"] = 1618530267,
							["serial"] = "Creature-0-4215-1116-50-87671-000078C8A1",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3528,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["heal_enemy_amt"] = 0,
							["totalover"] = 77.002237,
							["total_without_pet"] = 631.002237,
							["total"] = 631.002237,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.002237,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 631,
							},
							["totalover_without_pet"] = 0.002237,
							["healing_taken"] = 631.002237,
							["fight_component"] = true,
							["end_time"] = 1618530279,
							["targets_overheal"] = {
								["Thordren"] = 77,
							},
							["last_event"] = 1618530273,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 1,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 77,
										},
										["n_max"] = 159,
										["targets"] = {
											["Thordren"] = 631,
										},
										["n_min"] = 159,
										["counter"] = 2,
										["overheal"] = 77,
										["total"] = 631,
										["c_max"] = 472,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_min"] = 472,
										["c_curado"] = 472,
										["n_amt"] = 1,
										["n_curado"] = 159,
										["totaldenied"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["classe"] = "MAGE",
							["totaldenied"] = 0.002237,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["start_time"] = 1618530267,
							["delay"] = 0,
							["spec"] = 63,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3528,
					["_ActorTable"] = {
						{
							["received"] = 70.001164,
							["resource"] = 0.001164,
							["targets"] = {
								["Thordren"] = 70,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.001164,
							["fight_component"] = true,
							["total"] = 70.001164,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 70,
										["id"] = 59914,
										["totalover"] = 75,
										["targets"] = {
											["Thordren"] = 70,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 75.001164,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1618530279,
							["aID"] = "76-09A108A3",
							["alternatepower"] = 0.001164,
							["serial"] = "Player-76-09A108A3",
							["spec"] = 63,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3528,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[12654] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 5,
										["id"] = 12654,
										["uptime"] = 7,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 47,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269083] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 269083,
										["uptime"] = 12,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48107] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = 48107,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									[186401] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 186401,
										["uptime"] = 12,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[256374] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 256374,
										["uptime"] = 5,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[289982] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 289982,
										["uptime"] = 12,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48108] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = 48108,
										["uptime"] = 0,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									[236060] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 1,
										["id"] = 236060,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 7,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[108853] = 3,
								[2948] = 2,
								[11366] = 2,
							},
							["tipo"] = 4,
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618530279,
							["classe"] = "MAGE",
							["pets"] = {
							},
							["serial"] = "Player-76-09A108A3",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["monster"] = true,
							["last_event"] = 0,
							["nome"] = "Midnight Kaliri",
							["aID"] = "87671",
							["spell_cast"] = {
								[163716] = 1,
							},
							["fight_component"] = true,
							["tipo"] = 4,
							["classe"] = "UNKNOW",
							["pets"] = {
							},
							["serial"] = "Creature-0-4215-1116-50-87671-000078C8A1",
							["flag_original"] = 68168,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3528,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 7287.563,
				["tempo_start"] = 1618530267,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					5247.906065000001, -- [1]
					630.98641, -- [2]
					{
						-0.005634999999998058, -- [1]
						[0] = 69.98911100000001,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = -0.003462,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					3757, -- [1]
					631, -- [2]
					{
						0, -- [1]
						[0] = 70,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "19:44:40",
				["cleu_timeline"] = {
				},
				["enemy"] = "Midnight Kaliri",
				["TotalElapsedCombatTime"] = 1.213999999999942,
				["CombatEndedAt"] = 14745.045,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thordren"] = 3757.004339,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 631.002237,
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 14704.853,
				["combat_id"] = 3528,
				["overall_added"] = true,
				["TimeData"] = {
				},
				["frags"] = {
					["Midnight Kaliri"] = 1,
				},
				["combat_counter"] = 4829,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "19:44:27",
				["start_time"] = 14692.736,
				["contra"] = "Midnight Kaliri",
				["spells_cast_timeline"] = {
				},
			}, -- [10]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3527,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001672,
							["damage_from"] = {
							},
							["targets"] = {
								["Shadowmoon Stalker"] = 2931,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["last_dps"] = 1173.809239888153,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 2931.001672,
							["last_event"] = 1618530128,
							["delay"] = 0,
							["dps_started"] = false,
							["total"] = 2931.001672,
							["aID"] = "76-09A108A3",
							["damage_taken"] = 0.001672,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 50,
										["targets"] = {
											["Shadowmoon Stalker"] = 113,
										},
										["n_dmg"] = 113,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 113,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["extra"] = {
										},
										["spellschool"] = 48,
									},
									[108853] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1236,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Shadowmoon Stalker"] = 1236,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1236,
										["c_max"] = 623,
										["id"] = 108853,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 613,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[11366] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1036,
										["g_amt"] = 0,
										["n_max"] = 520,
										["targets"] = {
											["Shadowmoon Stalker"] = 1556,
										},
										["n_dmg"] = 520,
										["n_min"] = 520,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1556,
										["c_max"] = 1036,
										["id"] = 11366,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 1036,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 26,
										["targets"] = {
											["Shadowmoon Stalker"] = 26,
										},
										["n_dmg"] = 26,
										["n_min"] = 26,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 26,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1618530129,
							["custom"] = 0,
							["spec"] = 63,
							["colocacao"] = 1,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1618530126,
							["serial"] = "Player-76-09A108A3",
							["on_hold"] = false,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.001365,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
							},
							["delay"] = 0,
							["pets"] = {
							},
							["last_dps"] = 0,
							["tipo"] = 1,
							["aID"] = "82308",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.001365,
							["dps_started"] = false,
							["fight_component"] = true,
							["end_time"] = 1618530129,
							["damage_taken"] = 2931.001365,
							["total"] = 0.001365,
							["nome"] = "Shadowmoon Stalker",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["monster"] = true,
							["friendlyfire"] = {
							},
							["custom"] = 0,
							["last_event"] = 0,
							["on_hold"] = false,
							["start_time"] = 1618530129,
							["serial"] = "Creature-0-4215-1116-50-82308-000275C45C",
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3527,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3527,
					["_ActorTable"] = {
						{
							["received"] = 138.007736,
							["resource"] = 0.007736,
							["targets"] = {
								["Thordren"] = 138,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.007736,
							["total"] = 138.007736,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 138,
										["id"] = 59914,
										["totalover"] = 7,
										["targets"] = {
											["Thordren"] = 138,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 7.007736,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1618530128,
							["aID"] = "76-09A108A3",
							["alternatepower"] = 0.007736,
							["serial"] = "Player-76-09A108A3",
							["spec"] = 63,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3527,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[12654] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 2,
										["id"] = 12654,
										["uptime"] = 2,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[280286] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 280286,
										["uptime"] = 1,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 12,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269083] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 269083,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[289982] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 289982,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[186401] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 186401,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[256374] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 256374,
										["uptime"] = 2,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48108] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 48108,
										["uptime"] = 0,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48107] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 48107,
										["uptime"] = 1,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["debuff_uptime"] = 3,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[108853] = 1,
								[11366] = 1,
							},
							["tipo"] = 4,
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618530129,
							["classe"] = "MAGE",
							["pets"] = {
							},
							["serial"] = "Player-76-09A108A3",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3527,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 14690.26,
				["tempo_start"] = 1618530126,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					2930.977744999999, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 137.997991,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					2931, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 138,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "19:42:09",
				["cleu_timeline"] = {
				},
				["enemy"] = "Shadowmoon Stalker",
				["TotalElapsedCombatTime"] = 6.166999999999462,
				["CombatEndedAt"] = 14675.687,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thordren"] = 2931.001672,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 14554.449,
				["combat_id"] = 3527,
				["overall_added"] = true,
				["TimeData"] = {
				},
				["frags"] = {
					["Shadowmoon Stalker"] = 1,
				},
				["combat_counter"] = 4828,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "19:42:07",
				["start_time"] = 14551.952,
				["contra"] = "Shadowmoon Stalker",
				["spells_cast_timeline"] = {
				},
			}, -- [11]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3526,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002226,
							["damage_from"] = {
							},
							["targets"] = {
								["Shadowmoon Stalker"] = 3098,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["last_dps"] = 1205.917565589956,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 3098.002226,
							["last_event"] = 1618530118,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1618530119,
							["aID"] = "76-09A108A3",
							["damage_taken"] = 0.002226,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["total"] = 3098.002226,
							["custom"] = 0,
							["spells"] = {
								["_ActorTable"] = {
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 26,
										["targets"] = {
											["Shadowmoon Stalker"] = 52,
										},
										["n_dmg"] = 52,
										["n_min"] = 26,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 52,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[108853] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 863,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Shadowmoon Stalker"] = 863,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 863,
										["c_max"] = 863,
										["id"] = 108853,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 863,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[11366] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1450,
										["g_amt"] = 0,
										["n_max"] = 732,
										["targets"] = {
											["Shadowmoon Stalker"] = 2182,
										},
										["n_dmg"] = 732,
										["n_min"] = 732,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2182,
										["c_max"] = 1450,
										["id"] = 11366,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 1450,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1,
										["targets"] = {
											["Shadowmoon Stalker"] = 1,
										},
										["n_dmg"] = 1,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 48,
									},
								},
								["tipo"] = 2,
							},
							["colocacao"] = 1,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1618530115,
							["serial"] = "Player-76-09A108A3",
							["on_hold"] = false,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002446,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
							},
							["delay"] = 0,
							["pets"] = {
							},
							["last_dps"] = 0,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.002446,
							["end_time"] = 1618530119,
							["fight_component"] = true,
							["total"] = 0.002446,
							["damage_taken"] = 3098.002446,
							["on_hold"] = false,
							["nome"] = "Shadowmoon Stalker",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["aID"] = "82308",
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1618530119,
							["serial"] = "Creature-0-4215-1116-50-82308-0007F5C463",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3526,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3526,
					["_ActorTable"] = {
						{
							["received"] = 56.003215,
							["resource"] = 0.003215,
							["targets"] = {
								["Thordren"] = 56,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.003215,
							["total"] = 56.003215,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 56,
										["id"] = 59914,
										["totalover"] = 89,
										["targets"] = {
											["Thordren"] = 56,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 89.003215,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1618530118,
							["aID"] = "76-09A108A3",
							["alternatepower"] = 0.003215,
							["serial"] = "Player-76-09A108A3",
							["spec"] = 63,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3526,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[12654] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 1,
										["id"] = 12654,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 21,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[116014] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 116014,
										["uptime"] = 4,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[289982] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 289982,
										["uptime"] = 4,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[186401] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 186401,
										["uptime"] = 4,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[256374] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 256374,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[269083] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 269083,
										["uptime"] = 4,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48108] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 48108,
										["uptime"] = 1,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48107] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 48107,
										["uptime"] = 1,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["debuff_uptime"] = 3,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[108853] = 1,
								[11366] = 1,
							},
							["tipo"] = 4,
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618530119,
							["classe"] = "MAGE",
							["pets"] = {
							},
							["serial"] = "Player-76-09A108A3",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3526,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 14551.952,
				["tempo_start"] = 1618530115,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					3098, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 56,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					3098, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 56,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "19:41:59",
				["cleu_timeline"] = {
				},
				["enemy"] = "Shadowmoon Stalker",
				["TotalElapsedCombatTime"] = 14544.723,
				["CombatEndedAt"] = 14544.723,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thordren"] = 3098.002226,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 14544.723,
				["combat_id"] = 3526,
				["overall_added"] = true,
				["TimeData"] = {
				},
				["frags"] = {
					["Shadowmoon Stalker"] = 1,
				},
				["combat_counter"] = 4827,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "19:41:56",
				["start_time"] = 14541.245,
				["contra"] = "Shadowmoon Stalker",
				["spells_cast_timeline"] = {
				},
			}, -- [12]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3525,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007176,
							["damage_from"] = {
								["Shadowmoon Stalker"] = true,
							},
							["targets"] = {
								["Shadowmoon Stalker"] = 2826,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["last_dps"] = 344.7190992924969,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 2826.007176,
							["last_event"] = 1618529931,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1618529931,
							["aID"] = "76-09A108A3",
							["damage_taken"] = 1178.007176,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["total"] = 2826.007176,
							["custom"] = 0,
							["spells"] = {
								["_ActorTable"] = {
									[235314] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 94,
										["g_amt"] = 0,
										["n_max"] = 49,
										["targets"] = {
											["Shadowmoon Stalker"] = 240,
										},
										["n_dmg"] = 146,
										["n_min"] = 48,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 240,
										["c_max"] = 94,
										["id"] = 235314,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 94,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[133] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 578,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Shadowmoon Stalker"] = 578,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 578,
										["c_max"] = 578,
										["id"] = 133,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 578,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[2948] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 69,
										["targets"] = {
											["Shadowmoon Stalker"] = 135,
										},
										["n_dmg"] = 135,
										["n_min"] = 66,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 135,
										["c_max"] = 0,
										["id"] = 2948,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 2,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[12654] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 24,
										["targets"] = {
											["Shadowmoon Stalker"] = 63,
										},
										["n_dmg"] = 63,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 63,
										["c_max"] = 0,
										["id"] = 12654,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 7,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 29,
										["targets"] = {
											["Shadowmoon Stalker"] = 99,
										},
										["n_dmg"] = 99,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 10,
										["total"] = 99,
										["c_max"] = 0,
										["id"] = 259756,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 10,
										["extra"] = {
										},
										["spellschool"] = 48,
									},
									[11366] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 513,
										["targets"] = {
											["Shadowmoon Stalker"] = 513,
										},
										["n_dmg"] = 513,
										["n_min"] = 513,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 513,
										["c_max"] = 0,
										["id"] = 11366,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
									[108853] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1198,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Shadowmoon Stalker"] = 1198,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1198,
										["c_max"] = 602,
										["id"] = 108853,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 596,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["extra"] = {
										},
										["spellschool"] = 4,
									},
								},
								["tipo"] = 2,
							},
							["colocacao"] = 1,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1618529923,
							["serial"] = "Player-76-09A108A3",
							["on_hold"] = false,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 1178.006076,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 1178,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["last_dps"] = 0,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1178.006076,
							["end_time"] = 1618529931,
							["monster"] = true,
							["total"] = 1178.006076,
							["damage_taken"] = 2826.006076,
							["friendlyfire_total"] = 0,
							["nome"] = "Shadowmoon Stalker",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 228,
										["targets"] = {
											["Thordren"] = 649,
										},
										["n_dmg"] = 649,
										["n_min"] = 196,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 649,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 3,
										["extra"] = {
										},
										["spellschool"] = 1,
									}, -- [1]
									[131944] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 529,
										["targets"] = {
											["Thordren"] = 529,
										},
										["n_dmg"] = 529,
										["n_min"] = 529,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 529,
										["c_max"] = 0,
										["id"] = 131944,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["extra"] = {
										},
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["aID"] = "82308",
							["custom"] = 0,
							["last_event"] = 1618529930,
							["on_hold"] = false,
							["start_time"] = 1618529926,
							["serial"] = "Creature-0-4215-1116-50-82308-000275C465",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3525,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["heal_enemy_amt"] = 0,
							["totalover"] = 0.001587,
							["total_without_pet"] = 1178.001587,
							["total"] = 1178.001587,
							["targets_absorbs"] = {
								["Thordren"] = 1178,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 1178.001587,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 1178,
							},
							["totalover_without_pet"] = 0.001587,
							["healing_taken"] = 1178.001587,
							["fight_component"] = true,
							["end_time"] = 1618529931,
							["targets_overheal"] = {
							},
							["last_event"] = 1618529930,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[235313] = {
										["c_amt"] = 0,
										["totalabsorb"] = 1178,
										["targets_overheal"] = {
										},
										["n_max"] = 529,
										["targets"] = {
											["Thordren"] = 1178,
										},
										["n_min"] = 196,
										["counter"] = 4,
										["overheal"] = 0,
										["total"] = 1178,
										["c_max"] = 0,
										["id"] = 235313,
										["targets_absorbs"] = {
											["Thordren"] = 1178,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["n_curado"] = 1178,
										["totaldenied"] = 0,
										["n_amt"] = 4,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["classe"] = "MAGE",
							["totaldenied"] = 0.001587,
							["custom"] = 0,
							["tipo"] = 2,
							["aID"] = "76-09A108A3",
							["start_time"] = 1618529926,
							["delay"] = 0,
							["spec"] = 63,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3525,
					["_ActorTable"] = {
						{
							["received"] = 145.004035,
							["resource"] = 0.004035,
							["targets"] = {
								["Thordren"] = 145,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MAGE",
							["passiveover"] = 0.004035,
							["fight_component"] = true,
							["total"] = 145.004035,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 145,
										["id"] = 59914,
										["totalover"] = 0,
										["targets"] = {
											["Thordren"] = 145,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 0.004035,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1618529931,
							["aID"] = "76-09A108A3",
							["alternatepower"] = 0.004035,
							["serial"] = "Player-76-09A108A3",
							["spec"] = 63,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3525,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[12654] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 4,
										["id"] = 12654,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 43,
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269083] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 269083,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[289982] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 289982,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[186401] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 186401,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[256374] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 256374,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[48107] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = 48107,
										["uptime"] = 2,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									[48108] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 48108,
										["uptime"] = 1,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[235313] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 235313,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 8,
							["nome"] = "Thordren",
							["spec"] = 63,
							["grupo"] = true,
							["spell_cast"] = {
								[2948] = 1,
								[11366] = 1,
								[133] = 1,
								[108853] = 2,
							},
							["tipo"] = 4,
							["buff_uptime_targets"] = {
							},
							["last_event"] = 1618529931,
							["classe"] = "MAGE",
							["pets"] = {
							},
							["serial"] = "Player-76-09A108A3",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["monster"] = true,
							["last_event"] = 0,
							["nome"] = "Shadowmoon Stalker",
							["aID"] = "82308",
							["spell_cast"] = {
								[131944] = 1,
							},
							["fight_component"] = true,
							["tipo"] = 4,
							["classe"] = "UNKNOW",
							["pets"] = {
							},
							["serial"] = "Creature-0-4215-1116-50-82308-000275C465",
							["flag_original"] = 68168,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3525,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["CombatStartedAt"] = 14540.48,
				["tempo_start"] = 1618529923,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					4004, -- [1]
					1178, -- [2]
					{
						0, -- [1]
						[0] = 145,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					2826, -- [1]
					1178, -- [2]
					{
						0, -- [1]
						[0] = 145,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "19:38:52",
				["cleu_timeline"] = {
				},
				["enemy"] = "Shadowmoon Stalker",
				["TotalElapsedCombatTime"] = 8.19800000000032,
				["CombatEndedAt"] = 14357.064,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thordren"] = 2826.007176,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 1178.001587,
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 14357.064,
				["combat_id"] = 3525,
				["overall_added"] = true,
				["TimeData"] = {
				},
				["frags"] = {
					["Shadowmoon Stalker"] = 1,
				},
				["combat_counter"] = 4826,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "19:38:44",
				["start_time"] = 14348.866,
				["contra"] = "Shadowmoon Stalker",
				["spells_cast_timeline"] = {
				},
			}, -- [13]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3524,
					["_ActorTable"] = {
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.008859,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 359,
							},
							["delay"] = 1618529853,
							["pets"] = {
							},
							["last_dps"] = 0,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 359.008859,
							["end_time"] = 1618529882,
							["monster"] = true,
							["total"] = 359.008859,
							["damage_taken"] = 174.008859,
							["friendlyfire_total"] = 0,
							["nome"] = "Ancient Pearltusk",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 359,
										["targets"] = {
											["Thordren"] = 359,
										},
										["n_dmg"] = 359,
										["n_min"] = 359,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 359,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 1,
									}, -- [1]
									[158207] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 158207,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 4,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["extra"] = {
										},
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["aID"] = "82452",
							["custom"] = 0,
							["last_event"] = 1618529853,
							["on_hold"] = false,
							["start_time"] = 1618529881,
							["serial"] = "Creature-0-4215-1116-50-82452-000078B77D",
							["fight_component"] = true,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002663,
							["damage_from"] = {
								["Environment (Falling)"] = true,
								["Ancient Pearltusk"] = true,
							},
							["targets"] = {
								["Ancient Pearltusk"] = 174,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["last_dps"] = 7.333838953047114,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 174.002663,
							["last_event"] = 1618529867,
							["delay"] = 1618529867,
							["dps_started"] = false,
							["end_time"] = 1618529882,
							["aID"] = "76-09A108A3",
							["damage_taken"] = 375.002663,
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["total"] = 174.002663,
							["custom"] = 0,
							["spells"] = {
								["_ActorTable"] = {
									[122] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 90,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Ancient Pearltusk"] = 90,
										},
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 90,
										["c_max"] = 45,
										["id"] = 122,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 45,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 0,
										["extra"] = {
										},
										["spellschool"] = 16,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 19,
										["targets"] = {
											["Ancient Pearltusk"] = 84,
										},
										["n_dmg"] = 84,
										["n_min"] = 11,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 84,
										["c_max"] = 0,
										["id"] = 280286,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 5,
										["extra"] = {
										},
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["colocacao"] = 1,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1618529869,
							["serial"] = "Player-76-09A108A3",
							["on_hold"] = false,
						}, -- [2]
						{
							["flag_original"] = -2147483648,
							["totalabsorbed"] = 0.002592,
							["damage_from"] = {
							},
							["targets"] = {
								["Thordren"] = 16,
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 16.002592,
							["delay"] = 0,
							["fight_component"] = true,
							["end_time"] = 1618529882,
							["aID"] = "",
							["damage_taken"] = 0.002592,
							["nome"] = "Environment (Falling)",
							["spells"] = {
								["_ActorTable"] = {
									[3] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Thordren"] = 16,
										},
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 3,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 3,
									},
								},
								["tipo"] = 2,
							},
							["total"] = 16.002592,
							["last_dps"] = 0,
							["friendlyfire"] = {
							},
							["custom"] = 0,
							["last_event"] = 1618529876,
							["on_hold"] = false,
							["start_time"] = 1618529876,
							["serial"] = "",
							["dps_started"] = false,
						}, -- [3]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3524,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3524,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3524,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[122] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 122,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									[280286] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 280286,
										["uptime"] = 12,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 153,
							["cc_done_spells"] = {
								["_ActorTable"] = {
									[122] = {
										["id"] = 122,
										["targets"] = {
											["Ancient Pearltusk"] = 2,
										},
										["counter"] = 2,
									},
								},
								["tipo"] = 9,
							},
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[186401] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 186401,
										["uptime"] = 29,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[212653] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 2,
										["refreshamt"] = 0,
										["id"] = 212653,
										["uptime"] = 2,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
									[326396] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 326396,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[116267] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 116267,
										["uptime"] = 29,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[2479] = {
										["activedamt"] = 1,
										["id"] = 2479,
										["targets"] = {
										},
										["actived_at"] = 1618529853,
										["uptime"] = 0,
										["counter"] = 0,
										["refreshamt"] = 0,
										["actived"] = true,
										["appliedamt"] = 1,
									},
									[235450] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 235450,
										["uptime"] = 29,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[269083] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 269083,
										["uptime"] = 29,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[289982] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 289982,
										["uptime"] = 29,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[198065] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 1,
										["id"] = 198065,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["debuff_uptime"] = 20,
							["tipo"] = 4,
							["cc_done"] = 2.004982,
							["debuff_uptime_targets"] = {
							},
							["spec"] = 62,
							["grupo"] = true,
							["spell_cast"] = {
								[235450] = 1,
								[122] = 1,
								[212653] = 2,
							},
							["cc_done_targets"] = {
								["Ancient Pearltusk"] = 2,
							},
							["classe"] = "MAGE",
							["last_event"] = 1618529882,
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["serial"] = "Player-76-09A108A3",
							["nome"] = "Thordren",
						}, -- [1]
						{
							["monster"] = true,
							["last_event"] = 0,
							["nome"] = "Ancient Pearltusk",
							["aID"] = "82452",
							["spell_cast"] = {
								[158207] = 4,
							},
							["fight_component"] = true,
							["tipo"] = 4,
							["classe"] = "UNKNOW",
							["pets"] = {
							},
							["serial"] = "Creature-0-4215-1116-50-82452-000078B77D",
							["flag_original"] = 68168,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3524,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["raid_roster_indexed"] = {
					"Thordren", -- [1]
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					549, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					174, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "19:38:03",
				["cleu_timeline"] = {
				},
				["enemy"] = "Ancient Pearltusk",
				["TotalElapsedCombatTime"] = 14308.05,
				["CombatEndedAt"] = 14308.05,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "19:37:33",
				["end_time"] = 14308.05,
				["combat_id"] = 3524,
				["player_last_events"] = {
				},
				["tempo_start"] = 1618529853,
				["spells_cast_timeline"] = {
				},
				["contra"] = "Ancient Pearltusk",
				["combat_counter"] = 4825,
				["CombatSkillCache"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thordren"] = 174.002663,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["start_time"] = 14278.453,
				["TimeData"] = {
				},
				["frags"] = {
				},
			}, -- [14]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3523,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003417,
							["damage_from"] = {
								["Seacliff Kaliri"] = true,
								["Ancient Pearltusk"] = true,
							},
							["targets"] = {
								["Seacliff Kaliri"] = 4403,
							},
							["pets"] = {
							},
							["end_time"] = 1605463197,
							["spells"] = {
								["_ActorTable"] = {
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 42,
										["targets"] = {
											["Seacliff Kaliri"] = 134,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 134,
										["n_min"] = 22,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 134,
										["c_max"] = 0,
										["spellschool"] = 48,
										["id"] = 259756,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[44425] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 990,
										["targets"] = {
											["Seacliff Kaliri"] = 990,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 990,
										["n_min"] = 990,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 990,
										["c_max"] = 0,
										["spellschool"] = 64,
										["id"] = 44425,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[30451] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 833,
										["targets"] = {
											["Seacliff Kaliri"] = 3279,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3279,
										["n_min"] = 214,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 3279,
										["c_max"] = 0,
										["spellschool"] = 64,
										["id"] = 30451,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 6,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 4403.003417,
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["dps_started"] = false,
							["total"] = 4403.003417,
							["aID"] = "76-09A108A3",
							["friendlyfire"] = {
							},
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["tipo"] = 1,
							["colocacao"] = 1,
							["classe"] = "MAGE",
							["custom"] = 0,
							["last_event"] = 1605463196,
							["last_dps"] = 309.7870553014903,
							["start_time"] = 1605463188,
							["delay"] = 0,
							["damage_taken"] = 1784.003417,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.008738,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 1411,
							},
							["serial"] = "Creature-0-3783-1116-30456-82354-000030A85F",
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1411.008738,
							["total"] = 1411.008738,
							["monster"] = true,
							["end_time"] = 1605463197,
							["friendlyfire"] = {
							},
							["last_event"] = 1605463195,
							["nome"] = "Seacliff Kaliri",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 199,
										["targets"] = {
											["Thordren"] = 1205,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1205,
										["n_min"] = 140,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 1205,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 7,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[163716] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 206,
										["targets"] = {
											["Thordren"] = 206,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 206,
										["n_min"] = 206,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 206,
										["c_max"] = 0,
										["id"] = 163716,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["fight_component"] = true,
							["on_hold"] = false,
							["damage_taken"] = 4403.008738,
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605463183,
							["delay"] = 0,
							["aID"] = "82354",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3523,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thordren"] = 39,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-09A108A3",
							["totalover"] = 39.008383,
							["total_without_pet"] = 594.008383,
							["total"] = 594.008383,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.008383,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 594,
							},
							["totalover_without_pet"] = 0.008383,
							["healing_taken"] = 594.008383,
							["fight_component"] = true,
							["end_time"] = 1605463197,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["spec"] = 62,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 39,
										},
										["n_max"] = 211,
										["targets"] = {
											["Thordren"] = 594,
										},
										["n_min"] = 172,
										["counter"] = 3,
										["overheal"] = 39,
										["total"] = 594,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["n_curado"] = 594,
										["n_amt"] = 3,
										["m_healed"] = 0,
										["totaldenied"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605463183,
							["custom"] = 0,
							["last_event"] = 1605463195,
							["classe"] = "MAGE",
							["totaldenied"] = 0.008383,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3523,
					["_ActorTable"] = {
						{
							["received"] = 182.00305,
							["resource"] = 4.00305,
							["targets"] = {
								["Thordren"] = 182,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-09A108A3",
							["passiveover"] = 0.00305,
							["fight_component"] = true,
							["total"] = 182.00305,
							["resource_type"] = 16,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 182,
										["id"] = 59914,
										["totalover"] = 0,
										["targets"] = {
											["Thordren"] = 182,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 62,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00305,
							["tipo"] = 3,
							["last_event"] = 1605463196,
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 0.00305,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3523,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["pets"] = {
							},
							["aID"] = "76-09A108A3",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[256374] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 256374,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[326396] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 326396,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[116267] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 116267,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[276743] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 276743,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[264774] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 264774,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269083] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 269083,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[289982] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 289982,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236298] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 236298,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["buff_uptime_targets"] = {
							},
							["spec"] = 62,
							["grupo"] = true,
							["spell_cast"] = {
								[30451] = 6,
								[44425] = 1,
							},
							["nome"] = "Thordren",
							["last_event"] = 1605463197,
							["buff_uptime"] = 87,
							["tipo"] = 4,
							["serial"] = "Player-76-09A108A3",
							["classe"] = "MAGE",
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Seacliff Kaliri",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[163716] = 1,
							},
							["last_event"] = 0,
							["aID"] = "82354",
							["tipo"] = 4,
							["serial"] = "Creature-0-3783-1116-30456-82354-000030A85F",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3523,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["CombatStartedAt"] = 14276.347,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4823,
				["playing_solo"] = true,
				["totals"] = {
					5813.969827, -- [1]
					594, -- [2]
					{
						0, -- [1]
						[0] = 181.985361,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thordren"] = {
						{
							true, -- [1]
							1, -- [2]
							373, -- [3]
							1618529850.597, -- [4]
							7011, -- [5]
							"Ancient Pearltusk", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:59:58",
				["cleu_timeline"] = {
				},
				["enemy"] = "Seacliff Kaliri",
				["TotalElapsedCombatTime"] = 8088.265,
				["CombatEndedAt"] = 8088.265,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 594.008383,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thordren"] = 4403.003417,
						}, -- [1]
					},
				},
				["end_time"] = 8088.265,
				["combat_id"] = 3523,
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1605463183,
				["frags"] = {
					["Seacliff Kaliri"] = 1,
				},
				["contra"] = "Seacliff Kaliri",
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					4403, -- [1]
					594, -- [2]
					{
						0, -- [1]
						[0] = 182,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 8074.052000000001,
				["TimeData"] = {
				},
				["data_inicio"] = "12:59:44",
			}, -- [15]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3522,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00832,
							["damage_from"] = {
								["Ancient Pearltusk"] = true,
							},
							["targets"] = {
								["Ancient Pearltusk"] = 5518,
							},
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["damage_taken"] = 1397.00832,
							["classe"] = "MAGE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 5518.00832,
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["dps_started"] = false,
							["end_time"] = 1605463070,
							["aID"] = "76-09A108A3",
							["friendlyfire"] = {
							},
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[44425] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 898,
										["targets"] = {
											["Ancient Pearltusk"] = 898,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 898,
										["n_min"] = 898,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 898,
										["c_max"] = 0,
										["spellschool"] = 64,
										["id"] = 44425,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 98,
										["g_amt"] = 0,
										["n_max"] = 67,
										["targets"] = {
											["Ancient Pearltusk"] = 231,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 133,
										["n_min"] = 33,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 231,
										["c_max"] = 98,
										["spellschool"] = 1,
										["id"] = 280286,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 98,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[30451] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 617,
										["targets"] = {
											["Ancient Pearltusk"] = 1711,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1711,
										["n_min"] = 206,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1711,
										["c_max"] = 0,
										["spellschool"] = 64,
										["id"] = 30451,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[7268] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 923,
										["g_amt"] = 0,
										["n_max"] = 243,
										["targets"] = {
											["Ancient Pearltusk"] = 2678,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1755,
										["n_min"] = 203,
										["g_dmg"] = 0,
										["counter"] = 10,
										["total"] = 2678,
										["c_max"] = 474,
										["spellschool"] = 64,
										["id"] = 7268,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 449,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 8,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["total"] = 5518.00832,
							["last_event"] = 1605463069,
							["colocacao"] = 1,
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 339.9044178883935,
							["start_time"] = 1605463054,
							["delay"] = 0,
							["spec"] = 62,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002563,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 1397,
							},
							["serial"] = "Creature-0-3783-1116-30456-82452-0000314CA7",
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["monster"] = true,
							["aID"] = "82452",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1397.002563,
							["damage_taken"] = 5518.002563,
							["fight_component"] = true,
							["total"] = 1397.002563,
							["friendlyfire"] = {
							},
							["last_event"] = 1605463068,
							["nome"] = "Ancient Pearltusk",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 372,
										["targets"] = {
											["Thordren"] = 372,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 372,
										["n_min"] = 372,
										["g_dmg"] = 0,
										["counter"] = 2,
										["MISS"] = 1,
										["total"] = 372,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
									[158252] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 275,
										["targets"] = {
											["Thordren"] = 275,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 275,
										["n_min"] = 275,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 275,
										["c_max"] = 0,
										["id"] = 158252,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
									[158207] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 750,
										["targets"] = {
											["Thordren"] = 750,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 750,
										["n_min"] = 750,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 750,
										["c_max"] = 0,
										["id"] = 158207,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["dps_started"] = false,
							["friendlyfire_total"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605463061,
							["delay"] = 0,
							["end_time"] = 1605463070,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3522,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-09A108A3",
							["totalover"] = 0.00387,
							["total_without_pet"] = 211.00387,
							["total"] = 211.00387,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 0.00387,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 211,
							},
							["totalover_without_pet"] = 0.00387,
							["healing_taken"] = 211.00387,
							["fight_component"] = true,
							["end_time"] = 1605463070,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["spec"] = 62,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 211,
										["targets"] = {
											["Thordren"] = 211,
										},
										["n_min"] = 211,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 211,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["n_curado"] = 211,
										["n_amt"] = 1,
										["m_healed"] = 0,
										["totaldenied"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605463061,
							["custom"] = 0,
							["last_event"] = 1605463061,
							["classe"] = "MAGE",
							["totaldenied"] = 0.00387,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3522,
					["_ActorTable"] = {
						{
							["received"] = 182.002421,
							["resource"] = 4.002421,
							["targets"] = {
								["Thordren"] = 182,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-09A108A3",
							["passiveover"] = 0.002421,
							["fight_component"] = true,
							["total"] = 182.002421,
							["resource_type"] = 16,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 182,
										["id"] = 59914,
										["totalover"] = 0,
										["targets"] = {
											["Thordren"] = 182,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 62,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002421,
							["tipo"] = 3,
							["last_event"] = 1605463069,
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 0.002421,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3522,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[236299] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 236299,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 11,
										["id"] = 280286,
										["refreshamt"] = 3,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 86,
							["classe"] = "MAGE",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269083] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 16,
										["id"] = 269083,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[276743] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 276743,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[289982] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 16,
										["id"] = 289982,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 16,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236298] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 236298,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[264774] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 0,
										["id"] = 264774,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[116267] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 16,
										["id"] = 116267,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[326396] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 16,
										["id"] = 326396,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 16,
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["spell_cast"] = {
								[30451] = 4,
								[5143] = 3,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605463070,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-09A108A3",
							["serial"] = "Player-76-09A108A3",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Ancient Pearltusk",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[158252] = 1,
								[158207] = 1,
							},
							["last_event"] = 0,
							["aID"] = "82452",
							["tipo"] = 4,
							["serial"] = "Creature-0-3783-1116-30456-82452-0000314CA7",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3522,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["CombatStartedAt"] = 8072.39,
				["tempo_start"] = 1605463054,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4822,
				["playing_solo"] = true,
				["totals"] = {
					6914.991484000001, -- [1]
					211, -- [2]
					{
						0, -- [1]
						[0] = 181.989509,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:57:50",
				["cleu_timeline"] = {
				},
				["enemy"] = "Ancient Pearltusk",
				["TotalElapsedCombatTime"] = 7960.732,
				["CombatEndedAt"] = 7960.732,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 211.00387,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thordren"] = 5518.00832,
						}, -- [1]
					},
				},
				["end_time"] = 7960.732,
				["combat_id"] = 3522,
				["frags"] = {
					["Ancient Pearltusk"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					5518, -- [1]
					211, -- [2]
					{
						0, -- [1]
						[0] = 182,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 7944.498000000001,
				["contra"] = "Ancient Pearltusk",
				["data_inicio"] = "12:57:34",
			}, -- [16]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3521,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00216,
							["damage_from"] = {
								["Ancient Pearltusk"] = true,
							},
							["targets"] = {
								["Ancient Pearltusk"] = 5625,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["spells"] = {
								["_ActorTable"] = {
									[44425] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 956,
										["targets"] = {
											["Ancient Pearltusk"] = 956,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 956,
										["n_min"] = 956,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 956,
										["c_max"] = 0,
										["spellschool"] = 64,
										["id"] = 44425,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[30451] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1595,
										["g_amt"] = 0,
										["n_max"] = 691,
										["targets"] = {
											["Ancient Pearltusk"] = 4538,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2943,
										["n_min"] = 187,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 4538,
										["c_max"] = 1595,
										["spellschool"] = 64,
										["id"] = 30451,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1595,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 6,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 66,
										["g_amt"] = 0,
										["n_max"] = 33,
										["targets"] = {
											["Ancient Pearltusk"] = 131,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 65,
										["n_min"] = 32,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 131,
										["c_max"] = 66,
										["spellschool"] = 1,
										["id"] = 280286,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 66,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5625.00216,
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["dps_started"] = false,
							["total"] = 5625.00216,
							["aID"] = "76-09A108A3",
							["friendlyfire"] = {
							},
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["end_time"] = 1605463047,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605463046,
							["last_dps"] = 423.4418970189523,
							["start_time"] = 1605463034,
							["delay"] = 0,
							["damage_taken"] = 1382.00216,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 662.006488,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 1382,
							},
							["serial"] = "Creature-0-3783-1116-30456-82452-0000314C5E",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "82452",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1382.006488,
							["damage_taken"] = 5625.006488,
							["monster"] = true,
							["end_time"] = 1605463047,
							["on_hold"] = false,
							["last_event"] = 1605463046,
							["nome"] = "Ancient Pearltusk",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 404,
										["targets"] = {
											["Thordren"] = 1124,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1124,
										["n_min"] = 338,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1124,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 338,
										["r_amt"] = 0,
									}, -- [1]
									[158252] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 258,
										["targets"] = {
											["Thordren"] = 258,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 258,
										["n_min"] = 258,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 258,
										["c_max"] = 0,
										["id"] = 158252,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605463039,
							["delay"] = 0,
							["total"] = 1382.006488,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3521,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thordren"] = 19,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-09A108A3",
							["totalover"] = 19.005042,
							["total_without_pet"] = 1000.005042,
							["total"] = 1000.005042,
							["targets_absorbs"] = {
								["Thordren"] = 809,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 809.005042,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 1000,
							},
							["totalover_without_pet"] = 0.005042,
							["healing_taken"] = 1000.005042,
							["fight_component"] = true,
							["end_time"] = 1605463047,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["spec"] = 62,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[235450] = {
										["c_amt"] = 0,
										["totalabsorb"] = 809,
										["targets_overheal"] = {
										},
										["n_max"] = 404,
										["targets"] = {
											["Thordren"] = 809,
										},
										["n_min"] = 147,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 809,
										["c_max"] = 0,
										["id"] = 235450,
										["targets_absorbs"] = {
											["Thordren"] = 809,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 809,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
									[270117] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thordren"] = 19,
										},
										["n_max"] = 191,
										["targets"] = {
											["Thordren"] = 191,
										},
										["n_min"] = 191,
										["counter"] = 1,
										["overheal"] = 19,
										["total"] = 191,
										["c_max"] = 0,
										["id"] = 270117,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["n_curado"] = 191,
										["n_amt"] = 1,
										["m_healed"] = 0,
										["totaldenied"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605463039,
							["custom"] = 0,
							["last_event"] = 1605463044,
							["classe"] = "MAGE",
							["totaldenied"] = 0.005042,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3521,
					["_ActorTable"] = {
						{
							["received"] = 182.001181,
							["resource"] = 5.001181,
							["targets"] = {
								["Thordren"] = 182,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-09A108A3",
							["passiveover"] = 0.001181,
							["fight_component"] = true,
							["total"] = 182.001181,
							["resource_type"] = 16,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 182,
										["id"] = 59914,
										["totalover"] = 0,
										["targets"] = {
											["Thordren"] = 182,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 62,
							["flag_original"] = 1297,
							["alternatepower"] = 0.001181,
							["tipo"] = 3,
							["last_event"] = 1605463046,
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 0.001181,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3521,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[236299] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 236299,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[280286] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 9,
										["id"] = 280286,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 83,
							["classe"] = "MAGE",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269083] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 269083,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[326396] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 326396,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[235450] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 235450,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236298] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 236298,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[264774] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 3,
										["id"] = 264774,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[116267] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 116267,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[289982] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 289982,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 14,
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["spell_cast"] = {
								[30451] = 6,
								[44425] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605463047,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-09A108A3",
							["serial"] = "Player-76-09A108A3",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Ancient Pearltusk",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[158252] = 1,
							},
							["last_event"] = 0,
							["aID"] = "82452",
							["tipo"] = 4,
							["serial"] = "Creature-0-3783-1116-30456-82452-0000314C5E",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3521,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["CombatStartedAt"] = 7943.048,
				["tempo_start"] = 1605463034,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4821,
				["playing_solo"] = true,
				["totals"] = {
					7007, -- [1]
					1000, -- [2]
					{
						0, -- [1]
						[0] = 182,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:57:28",
				["cleu_timeline"] = {
				},
				["enemy"] = "Ancient Pearltusk",
				["TotalElapsedCombatTime"] = 13.28400000000056,
				["CombatEndedAt"] = 7938.016000000001,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 1000.005042,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thordren"] = 5625.00216,
						}, -- [1]
					},
				},
				["end_time"] = 7938.016000000001,
				["combat_id"] = 3521,
				["frags"] = {
					["Ancient Pearltusk"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					5625, -- [1]
					1000, -- [2]
					{
						0, -- [1]
						[0] = 182,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 7924.732,
				["contra"] = "Ancient Pearltusk",
				["data_inicio"] = "12:57:14",
			}, -- [17]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3520,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004343,
							["damage_from"] = {
								["Ancient Pearltusk"] = true,
							},
							["targets"] = {
								["Ancient Pearltusk"] = 5343,
							},
							["pets"] = {
							},
							["classe"] = "MAGE",
							["spells"] = {
								["_ActorTable"] = {
									[259756] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 91,
										["targets"] = {
											["Ancient Pearltusk"] = 214,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 214,
										["n_min"] = 1,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 214,
										["c_max"] = 0,
										["spellschool"] = 48,
										["id"] = 259756,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[44425] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1824,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Ancient Pearltusk"] = 1824,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1824,
										["c_max"] = 1824,
										["spellschool"] = 64,
										["id"] = 44425,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1824,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[30451] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1792,
										["g_amt"] = 0,
										["n_max"] = 662,
										["targets"] = {
											["Ancient Pearltusk"] = 3257,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1465,
										["n_min"] = 210,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 3257,
										["c_max"] = 1072,
										["spellschool"] = 64,
										["id"] = 30451,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 720,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[280286] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Ancient Pearltusk"] = 48,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 48,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 48,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 280286,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 5343.004343000001,
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["dps_started"] = false,
							["total"] = 5343.004343000001,
							["aID"] = "76-09A108A3",
							["friendlyfire"] = {
							},
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["end_time"] = 1605463021,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605463020,
							["last_dps"] = 362.5817279451714,
							["start_time"] = 1605463006,
							["delay"] = 0,
							["damage_taken"] = 657.0043430000001,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 657.0030790000001,
							["damage_from"] = {
								["Thordren"] = true,
							},
							["targets"] = {
								["Thordren"] = 657,
							},
							["serial"] = "Creature-0-3783-1116-30456-82452-0000315C7F",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "82452",
							["raid_targets"] = {
							},
							["total_without_pet"] = 657.0030790000001,
							["damage_taken"] = 5343.003079,
							["monster"] = true,
							["end_time"] = 1605463021,
							["on_hold"] = false,
							["last_event"] = 1605463013,
							["nome"] = "Ancient Pearltusk",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 365,
										["targets"] = {
											["Thordren"] = 365,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 365,
										["n_min"] = 365,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 365,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[158252] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 292,
										["targets"] = {
											["Thordren"] = 292,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 292,
										["n_min"] = 292,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 292,
										["c_max"] = 0,
										["id"] = 158252,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
									[158207] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 0,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 158207,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605463011,
							["delay"] = 0,
							["total"] = 657.0030790000001,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3520,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-09A108A3",
							["totalover"] = 0.008482,
							["total_without_pet"] = 657.008482,
							["total"] = 657.008482,
							["targets_absorbs"] = {
								["Thordren"] = 657,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09A108A3",
							["totalabsorb"] = 657.008482,
							["last_hps"] = 0,
							["targets"] = {
								["Thordren"] = 657,
							},
							["totalover_without_pet"] = 0.008482,
							["healing_taken"] = 657.008482,
							["fight_component"] = true,
							["end_time"] = 1605463021,
							["healing_from"] = {
								["Thordren"] = true,
							},
							["spec"] = 62,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[235450] = {
										["c_amt"] = 0,
										["totalabsorb"] = 657,
										["targets_overheal"] = {
										},
										["n_max"] = 365,
										["targets"] = {
											["Thordren"] = 657,
										},
										["n_min"] = 292,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 657,
										["c_max"] = 0,
										["id"] = 235450,
										["targets_absorbs"] = {
											["Thordren"] = 657,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 657,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605463011,
							["custom"] = 0,
							["last_event"] = 1605463013,
							["classe"] = "MAGE",
							["totaldenied"] = 0.008482,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3520,
					["_ActorTable"] = {
						{
							["received"] = 182.003325,
							["resource"] = 6.003325,
							["targets"] = {
								["Thordren"] = 182,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-09A108A3",
							["passiveover"] = 0.003325,
							["fight_component"] = true,
							["total"] = 182.003325,
							["resource_type"] = 16,
							["nome"] = "Thordren",
							["spells"] = {
								["_ActorTable"] = {
									[59914] = {
										["total"] = 182,
										["id"] = 59914,
										["totalover"] = 0,
										["targets"] = {
											["Thordren"] = 182,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 62,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003325,
							["tipo"] = 3,
							["last_event"] = 1605463034,
							["classe"] = "MAGE",
							["serial"] = "Player-76-09A108A3",
							["totalover"] = 0.003325,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3520,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[280286] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 8,
										["id"] = 280286,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236299] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 236299,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 108,
							["classe"] = "MAGE",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[256374] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 12,
										["id"] = 256374,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 15,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[326396] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 15,
										["id"] = 326396,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[116267] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 15,
										["id"] = 116267,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[235450] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 15,
										["id"] = 235450,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[264774] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 264774,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269083] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 15,
										["id"] = 269083,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[289982] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 15,
										["id"] = 289982,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[236298] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 236298,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 13,
							["nome"] = "Thordren",
							["spec"] = 62,
							["grupo"] = true,
							["spell_cast"] = {
								[30451] = 5,
								[44425] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605463021,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-09A108A3",
							["serial"] = "Player-76-09A108A3",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Ancient Pearltusk",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[158252] = 1,
								[158207] = 1,
							},
							["last_event"] = 0,
							["aID"] = "82452",
							["tipo"] = 4,
							["serial"] = "Creature-0-3783-1116-30456-82452-0000315C7F",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3520,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thordren"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4820,
				["playing_solo"] = true,
				["totals"] = {
					6000, -- [1]
					657, -- [2]
					{
						0, -- [1]
						[0] = 182,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:57:01",
				["cleu_timeline"] = {
				},
				["enemy"] = "Ancient Pearltusk",
				["TotalElapsedCombatTime"] = 7911.947,
				["CombatEndedAt"] = 7911.947,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thordren"] = 657.008482,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thordren"] = 5343.004343000001,
						}, -- [1]
					},
				},
				["end_time"] = 7911.947,
				["combat_id"] = 3520,
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1605463006,
				["frags"] = {
					["Ancient Pearltusk"] = 1,
				},
				["contra"] = "Ancient Pearltusk",
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					5343, -- [1]
					657, -- [2]
					{
						0, -- [1]
						[0] = 182,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 7897.211,
				["TimeData"] = {
				},
				["data_inicio"] = "12:56:47",
			}, -- [18]
		},
	},
	["ocd_tracker"] = {
		["enabled"] = false,
		["current_cooldowns"] = {
		},
		["cooldowns"] = {
		},
		["show_conditions"] = {
			["only_inside_instance"] = true,
			["only_in_group"] = true,
		},
		["show_options"] = false,
		["pos"] = {
		},
	},
	["last_version"] = "v9.0.5.8406",
	["force_font_outline"] = "",
	["tabela_instancias"] = {
	},
	["coach"] = {
		["enabled"] = false,
		["welcome_panel_pos"] = {
		},
		["last_coach_name"] = false,
	},
	["local_instances_config"] = {
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[2] = 2,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 25.85211181640625,
					["x"] = -1082.867156982422,
					["w"] = 266.7997741699219,
					["h"] = 224.6000366210938,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[4] = 1,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -166.7479248046875,
					["x"] = -1082.867156982422,
					["w"] = 266.7997741699219,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [2]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["isLocked"] = false,
			["is_open"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -147.2000732421875,
					["x"] = -238.400390625,
					["w"] = 320.0000305175781,
					["h"] = 130.0000610351563,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [3]
	},
	["cached_talents"] = {
	},
	["last_instance_id"] = 1448,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 1590271266,
	["active_profile"] = "Default",
	["last_realversion"] = 144,
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["dps"] = 0,
			["author"] = "Details! Team",
			["level"] = 1,
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["last_section_selected"] = "main",
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				["last_selected"] = 1,
			},
			["options"] = {
				["auto_create"] = true,
				["show_method"] = 4,
				["window_scale"] = 1,
			},
		},
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["animate"] = false,
			["showamount"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
			["bar_height"] = 24,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_height"] = 40,
		},
		["DETAILS_PLUGIN_STREAM_OVERLAY"] = {
			["font_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["is_first_run"] = false,
			["grow_direction"] = "right",
			["arrow_color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				0.5, -- [4]
			},
			["main_frame_size"] = {
				300, -- [1]
				500.000030517578, -- [2]
			},
			["minimap"] = {
				["minimapPos"] = 160,
				["radius"] = 160,
				["hide"] = false,
			},
			["scale"] = 1,
			["arrow_anchor_x"] = 0,
			["row_height"] = 20,
			["row_texture"] = "Details Serenity",
			["point"] = "CENTER",
			["author"] = "Details! Team",
			["main_frame_strata"] = "LOW",
			["square_amount"] = 5,
			["enabled"] = false,
			["arrow_size"] = 10,
			["use_spark"] = true,
			["row_spacement"] = 21,
			["main_frame_color"] = {
				0, -- [1]
				0, -- [2]
				0, -- [3]
				0.2, -- [4]
			},
			["row_color"] = {
				0.1, -- [1]
				0.1, -- [2]
				0.1, -- [3]
				0.4, -- [4]
			},
			["arrow_texture"] = "Interface\\CHATFRAME\\ChatFrameExpandArrow",
			["y"] = 5.340576171875e-05,
			["per_second"] = {
				["enabled"] = false,
				["point"] = "CENTER",
				["scale"] = 1,
				["font_shadow"] = true,
				["y"] = 0,
				["x"] = 0,
				["size"] = 32,
				["update_speed"] = 0.05,
				["attribute_type"] = 1,
			},
			["x"] = 9.1552734375e-05,
			["font_face"] = "Friz Quadrata TT",
			["square_size"] = 32,
			["font_size"] = 10,
			["arrow_anchor_y"] = 0,
			["main_frame_locked"] = false,
			["use_square_mode"] = false,
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["realm_lastamt"] = 0,
			["realm_history"] = {
			},
			["author"] = "Details! Team",
		},
	},
	["last_encounter"] = "Iron Reaver",
	["on_death_menu"] = true,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["nick_tag_cache"] = {
		["nextreset"] = 1619825840,
		["last_version"] = 14,
	},
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["combat_counter"] = 4839,
	["combat_id"] = 3537,
	["savedStyles"] = {
	},
	["announce_prepots"] = {
		["enabled"] = false,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.113601,
					["damage_from"] = {
						["Thorny Leafling"] = true,
						["Moonbark Ancient"] = true,
						["Luminous Orchid"] = true,
						["Thorny Stabber"] = true,
						["Podling Scavenger"] = true,
						["Crimson Mandragora"] = true,
						["Environment (Falling)"] = true,
						["Harvester Ommru"] = true,
						["Podling Trapper"] = true,
						["Shadowmoon Stalker"] = true,
						["Weald Stinger"] = true,
						["Ancient Pearltusk"] = true,
						["Seacliff Kaliri"] = true,
						["Midnight Kaliri"] = true,
					},
					["targets"] = {
						["Thorny Leafling"] = 11980,
						["Moonbark Ancient"] = 7211,
						["Twilight Wasp"] = 7,
						["Luminous Orchid"] = 12790,
						["Aggressive Growth"] = 631,
						["Thorny Stabber"] = 10792,
						["Crimson Mandragora"] = 7270,
						["Harvester Ommru"] = 14190,
						["Podling Scavenger"] = 14921,
						["Podling Trapper"] = 1874,
						["Shadowmoon Stalker"] = 8855,
						["Weald Stinger"] = 8963,
						["Ancient Pearltusk"] = 27743,
						["Seacliff Kaliri"] = 4403,
						["Midnight Kaliri"] = 3757,
					},
					["friendlyfire"] = {
					},
					["pets"] = {
					},
					["serial"] = "Player-76-09A108A3",
					["aID"] = "76-09A108A3",
					["raid_targets"] = {
					},
					["total_without_pet"] = 135387.113601,
					["total"] = 135387.113601,
					["on_hold"] = false,
					["dps_started"] = false,
					["end_time"] = 1605462880,
					["classe"] = "MAGE",
					["last_dps"] = 0,
					["nome"] = "Thordren",
					["spec"] = 62,
					["grupo"] = true,
					["last_event"] = 0,
					["spells"] = {
						["_ActorTable"] = {
							[30451] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 3387,
								["g_amt"] = 0,
								["n_max"] = 845,
								["targets"] = {
									["Luminous Orchid"] = 552,
									["Seacliff Kaliri"] = 3279,
									["Moonbark Ancient"] = 3717,
									["Ancient Pearltusk"] = 16440,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 20601,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 44,
								["total"] = 23988,
								["c_max"] = 1595,
								["id"] = 30451,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 41,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[1449] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 613,
								["g_amt"] = 0,
								["n_max"] = 318,
								["targets"] = {
									["Luminous Orchid"] = 4435,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3822,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 14,
								["total"] = 4435,
								["c_max"] = 613,
								["id"] = 1449,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 13,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[31661] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 1369,
								["g_amt"] = 0,
								["n_max"] = 238,
								["targets"] = {
									["Thorny Leafling"] = 942,
									["Podling Scavenger"] = 1331,
									["Thorny Stabber"] = 701,
								},
								["n_dmg"] = 1605,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 10,
								["total"] = 2974,
								["c_max"] = 474,
								["id"] = 31661,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 7,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[122] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 90,
								["g_amt"] = 0,
								["n_max"] = 25,
								["targets"] = {
									["Podling Scavenger"] = 50,
									["Ancient Pearltusk"] = 90,
								},
								["n_dmg"] = 50,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 140,
								["c_max"] = 45,
								["id"] = 122,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 2,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[12654] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 52,
								["targets"] = {
									["Thorny Leafling"] = 497,
									["Podling Scavenger"] = 818,
									["Crimson Mandragora"] = 447,
									["Harvester Ommru"] = 1177,
									["Twilight Wasp"] = 7,
									["Shadowmoon Stalker"] = 141,
									["Weald Stinger"] = 269,
									["Thorny Stabber"] = 272,
									["Podling Trapper"] = 28,
									["Midnight Kaliri"] = 171,
								},
								["n_dmg"] = 3827,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 221,
								["total"] = 3827,
								["c_max"] = 0,
								["id"] = 12654,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 221,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[259756] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 91,
								["targets"] = {
									["Thorny Leafling"] = 111,
									["Moonbark Ancient"] = 163,
									["Luminous Orchid"] = 104,
									["Thorny Stabber"] = 129,
									["Harvester Ommru"] = 148,
									["Podling Scavenger"] = 108,
									["Podling Trapper"] = 2,
									["Shadowmoon Stalker"] = 213,
									["Weald Stinger"] = 93,
									["Ancient Pearltusk"] = 214,
									["Seacliff Kaliri"] = 134,
									["Midnight Kaliri"] = 39,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1458,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 152,
								["total"] = 1458,
								["c_max"] = 0,
								["id"] = 259756,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 152,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[226757] = {
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 52,
								["g_amt"] = 0,
								["n_max"] = 7,
								["targets"] = {
									["Harvester Ommru"] = 94,
									["Crimson Mandragora"] = 31,
								},
								["n_dmg"] = 73,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 16,
								["total"] = 125,
								["c_max"] = 14,
								["id"] = 226757,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 12,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[280286] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 375,
								["g_amt"] = 0,
								["n_max"] = 74,
								["targets"] = {
									["Thorny Leafling"] = 118,
									["Podling Scavenger"] = 94,
									["Luminous Orchid"] = 16,
									["Crimson Mandragora"] = 104,
									["Harvester Ommru"] = 391,
									["Ancient Pearltusk"] = 510,
									["Weald Stinger"] = 18,
									["Moonbark Ancient"] = 164,
									["Thorny Stabber"] = 54,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1094,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 46,
								["total"] = 1469,
								["c_max"] = 149,
								["id"] = 280286,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 41,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[133] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 1749,
								["g_amt"] = 0,
								["n_max"] = 305,
								["targets"] = {
									["Harvester Ommru"] = 2377,
									["Crimson Mandragora"] = 299,
									["Shadowmoon Stalker"] = 578,
								},
								["n_dmg"] = 1505,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 8,
								["total"] = 3254,
								["c_max"] = 589,
								["id"] = 133,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 5,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[153640] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 515,
								["targets"] = {
									["Luminous Orchid"] = 2535,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2535,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 2535,
								["c_max"] = 0,
								["id"] = 153640,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[108853] = {
								["c_amt"] = 37,
								["b_amt"] = 0,
								["c_dmg"] = 24521,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Podling Scavenger"] = 3033,
									["Aggressive Growth"] = 631,
									["Crimson Mandragora"] = 2542,
									["Harvester Ommru"] = 3824,
									["Shadowmoon Stalker"] = 3297,
									["Weald Stinger"] = 3214,
									["Thorny Stabber"] = 4315,
									["Podling Trapper"] = 1844,
									["Midnight Kaliri"] = 1821,
								},
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 37,
								["total"] = 24521,
								["c_max"] = 903,
								["id"] = 108853,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[235314] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 529,
								["g_amt"] = 0,
								["n_max"] = 70,
								["targets"] = {
									["Harvester Ommru"] = 100,
									["Thorny Leafling"] = 800,
									["Podling Scavenger"] = 763,
									["Shadowmoon Stalker"] = 240,
									["Weald Stinger"] = 50,
									["Thorny Stabber"] = 197,
								},
								["n_dmg"] = 1621,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 34,
								["total"] = 2150,
								["c_max"] = 139,
								["id"] = 235314,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 29,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[205345] = {
								["c_amt"] = 20,
								["b_amt"] = 0,
								["c_dmg"] = 1144,
								["g_amt"] = 0,
								["n_max"] = 39,
								["targets"] = {
									["Harvester Ommru"] = 130,
									["Thorny Leafling"] = 1695,
									["Podling Scavenger"] = 510,
									["Thorny Stabber"] = 393,
									["Crimson Mandragora"] = 53,
								},
								["n_dmg"] = 1637,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 80,
								["total"] = 2781,
								["c_max"] = 77,
								["id"] = 205345,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 60,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[257542] = {
								["c_amt"] = 10,
								["b_amt"] = 0,
								["c_dmg"] = 5508,
								["g_amt"] = 0,
								["n_max"] = 508,
								["targets"] = {
									["Harvester Ommru"] = 1810,
									["Thorny Leafling"] = 1984,
									["Podling Scavenger"] = 2327,
									["Weald Stinger"] = 1467,
									["Thorny Stabber"] = 2968,
								},
								["n_dmg"] = 5048,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 28,
								["total"] = 10556,
								["c_max"] = 1000,
								["id"] = 257542,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 18,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[2948] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 72,
								["targets"] = {
									["Harvester Ommru"] = 72,
									["Shadowmoon Stalker"] = 135,
									["Podling Scavenger"] = 283,
									["Midnight Kaliri"] = 135,
								},
								["n_dmg"] = 625,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 625,
								["c_max"] = 0,
								["id"] = 2948,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 9,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[7268] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 2271,
								["g_amt"] = 0,
								["n_max"] = 315,
								["targets"] = {
									["Luminous Orchid"] = 1719,
									["Moonbark Ancient"] = 1342,
									["Ancient Pearltusk"] = 4941,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 5731,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 29,
								["total"] = 8002,
								["c_max"] = 474,
								["id"] = 7268,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 24,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[44425] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 2885,
								["g_amt"] = 0,
								["n_max"] = 1317,
								["targets"] = {
									["Luminous Orchid"] = 3429,
									["Seacliff Kaliri"] = 990,
									["Moonbark Ancient"] = 1825,
									["Ancient Pearltusk"] = 5548,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 8907,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["total"] = 11792,
								["c_max"] = 1824,
								["id"] = 44425,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 10,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[11366] = {
								["c_amt"] = 10,
								["b_amt"] = 0,
								["c_dmg"] = 11194,
								["g_amt"] = 0,
								["n_max"] = 732,
								["targets"] = {
									["Harvester Ommru"] = 3830,
									["Shadowmoon Stalker"] = 4251,
									["Weald Stinger"] = 3852,
									["Crimson Mandragora"] = 3794,
									["Midnight Kaliri"] = 1591,
								},
								["n_dmg"] = 6124,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 21,
								["total"] = 17318,
								["c_max"] = 1450,
								["id"] = 11366,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 11,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[2120] = {
								["c_amt"] = 12,
								["b_amt"] = 0,
								["c_dmg"] = 5790,
								["g_amt"] = 0,
								["n_max"] = 350,
								["targets"] = {
									["Harvester Ommru"] = 237,
									["Thorny Leafling"] = 5833,
									["Podling Scavenger"] = 5604,
									["Thorny Stabber"] = 1763,
								},
								["n_dmg"] = 7647,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 40,
								["total"] = 13437,
								["c_max"] = 497,
								["id"] = 2120,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 28,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 27821.113601,
					["start_time"] = 1605462596,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.004116,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 691,
					},
					["serial"] = "Creature-0-3783-1116-30456-82378-00003124E1",
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 691.004116,
					["end_time"] = 1605462880,
					["dps_started"] = false,
					["total"] = 691.004116,
					["on_hold"] = false,
					["last_event"] = 0,
					["nome"] = "Moonbark Ancient",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 263,
								["targets"] = {
									["Thordren"] = 691,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 691,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 691,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["a_amt"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[165851] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 165851,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["aID"] = "82378",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 7211.004115999999,
					["start_time"] = 1605462870,
					["delay"] = 0,
					["monster"] = true,
				}, -- [2]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 428.012836,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 1861,
					},
					["serial"] = "Creature-0-3783-1116-30456-82425-0000314B50",
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 1861.012836,
					["end_time"] = 1605462914,
					["dps_started"] = false,
					["total"] = 1861.012836,
					["on_hold"] = false,
					["last_event"] = 0,
					["nome"] = "Luminous Orchid",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 86,
								["targets"] = {
									["Thordren"] = 570,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 570,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 570,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 8,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[169567] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 197,
								["targets"] = {
									["Thordren"] = 1291,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1291,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 1291,
								["c_max"] = 0,
								["id"] = 169567,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 7,
								["b_dmg"] = 0,
								["n_amt"] = 7,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["aID"] = "82425",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 12790.012836,
					["start_time"] = 1605462900,
					["delay"] = 0,
					["monster"] = true,
				}, -- [3]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 1980.03222,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 5557,
					},
					["serial"] = "Creature-0-3783-1116-30456-82452-0000314C60",
					["pets"] = {
					},
					["friendlyfire"] = {
					},
					["aID"] = "82452",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 5557.032219999999,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605462962,
					["on_hold"] = false,
					["last_event"] = 0,
					["nome"] = "Ancient Pearltusk",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 422,
								["targets"] = {
									["Thordren"] = 3427,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3427,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 10,
								["total"] = 3427,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 2,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 9,
								["a_dmg"] = 760,
								["r_amt"] = 0,
							}, -- [1]
							[158252] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 292,
								["targets"] = {
									["Thordren"] = 1380,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1380,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 1380,
								["c_max"] = 0,
								["id"] = 158252,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 5,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[158207] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 750,
								["targets"] = {
									["Thordren"] = 750,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 750,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 750,
								["c_max"] = 0,
								["id"] = 158207,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 6,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["classe"] = "UNKNOW",
					["damage_taken"] = 27743.03222,
					["total"] = 5557.032219999999,
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1605462923,
					["delay"] = 0,
					["dps_started"] = false,
				}, -- [4]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.010164,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 1411,
					},
					["serial"] = "Creature-0-3783-1116-30456-82354-000030A85F",
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 1411.010164,
					["end_time"] = 1605463198,
					["dps_started"] = false,
					["total"] = 1411.010164,
					["on_hold"] = false,
					["last_event"] = 0,
					["nome"] = "Seacliff Kaliri",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 199,
								["targets"] = {
									["Thordren"] = 1205,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1205,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 1205,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 7,
								["a_amt"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[163716] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 206,
								["targets"] = {
									["Thordren"] = 206,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 206,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 206,
								["c_max"] = 0,
								["id"] = 163716,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["aID"] = "82354",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 4403.010164,
					["start_time"] = 1605463181,
					["delay"] = 0,
					["monster"] = true,
				}, -- [5]
				{
					["flag_original"] = -2147483648,
					["totalabsorbed"] = 0.008847,
					["damage_from"] = {
					},
					["targets"] = {
						["Thordren"] = 16,
					},
					["pets"] = {
					},
					["last_dps"] = 0,
					["on_hold"] = false,
					["aID"] = "",
					["raid_targets"] = {
					},
					["total_without_pet"] = 16.008847,
					["delay"] = 0,
					["fight_component"] = true,
					["end_time"] = 1618529883,
					["friendlyfire_total"] = 0,
					["damage_taken"] = 0.008847,
					["nome"] = "Environment (Falling)",
					["spells"] = {
						["_ActorTable"] = {
							[3] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 16,
								["targets"] = {
									["Thordren"] = 16,
								},
								["n_dmg"] = 16,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 16,
								["c_max"] = 0,
								["id"] = 3,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 1,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["tipo"] = 1,
					["dps_started"] = false,
					["total"] = 16.008847,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1618529874,
					["serial"] = "",
					["classe"] = "UNKNOW",
				}, -- [6]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 1178.018309,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 1178,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["dps_started"] = false,
					["damage_taken"] = 8855.018309,
					["aID"] = "82308",
					["raid_targets"] = {
					},
					["total_without_pet"] = 1178.018309,
					["total"] = 1178.018309,
					["fight_component"] = true,
					["end_time"] = 1618529932,
					["last_dps"] = 0,
					["tipo"] = 1,
					["nome"] = "Shadowmoon Stalker",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 228,
								["targets"] = {
									["Thordren"] = 649,
								},
								["n_dmg"] = 649,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 649,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 3,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[131944] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 529,
								["targets"] = {
									["Thordren"] = 529,
								},
								["n_dmg"] = 529,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 529,
								["c_max"] = 0,
								["id"] = 131944,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["a_amt"] = 0,
								["n_amt"] = 1,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1618529924,
					["serial"] = "Creature-0-4215-1116-50-82308-000275C465",
					["friendlyfire"] = {
					},
				}, -- [7]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.006919,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 1491,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["dps_started"] = false,
					["damage_taken"] = 3757.006919,
					["aID"] = "87671",
					["raid_targets"] = {
					},
					["total_without_pet"] = 1491.006919,
					["total"] = 1491.006919,
					["monster"] = true,
					["end_time"] = 1618530280,
					["last_dps"] = 0,
					["tipo"] = 1,
					["nome"] = "Midnight Kaliri",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 372,
								["g_amt"] = 0,
								["n_max"] = 201,
								["targets"] = {
									["Thordren"] = 1298,
								},
								["n_dmg"] = 926,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 1298,
								["c_max"] = 372,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 5,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[163716] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 193,
								["targets"] = {
									["Thordren"] = 193,
								},
								["n_dmg"] = 193,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 193,
								["c_max"] = 0,
								["id"] = 163716,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["a_amt"] = 0,
								["n_amt"] = 1,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["fight_component"] = true,
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1618530265,
					["serial"] = "Creature-0-4215-1116-50-87671-000078C8A1",
					["friendlyfire"] = {
					},
				}, -- [8]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.011222,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 266,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["aID"] = "84862",
					["raid_targets"] = {
					},
					["total_without_pet"] = 266.011222,
					["friendlyfire"] = {
					},
					["end_time"] = 1618674478,
					["dps_started"] = false,
					["total"] = 266.011222,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Podling Trapper",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 146,
								["targets"] = {
									["Thordren"] = 266,
								},
								["n_dmg"] = 266,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 266,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 1,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 2,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
						},
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84862-00007ABDE6",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674470,
					["delay"] = 0,
					["damage_taken"] = 1874.011222,
				}, -- [9]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 494.027218,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 2517,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["aID"] = "84402",
					["raid_targets"] = {
					},
					["total_without_pet"] = 2517.027218,
					["friendlyfire"] = {
					},
					["end_time"] = 1618674478,
					["dps_started"] = false,
					["total"] = 2517.027218,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Podling Scavenger",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 212,
								["g_amt"] = 0,
								["n_max"] = 65,
								["targets"] = {
									["Thordren"] = 2318,
								},
								["n_dmg"] = 2106,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 41,
								["total"] = 2318,
								["c_max"] = 114,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 1,
								["extra"] = {
								},
								["a_dmg"] = 46,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 1,
								["n_amt"] = 38,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[169519] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 169519,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 4,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[169522] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 100,
								["targets"] = {
									["Thordren"] = 199,
								},
								["n_dmg"] = 199,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 199,
								["c_max"] = 0,
								["id"] = 169522,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 2,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84402-00007AF674",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674434,
					["delay"] = 0,
					["damage_taken"] = 14921.027218,
				}, -- [10]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.00974,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 4068,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["aID"] = "84391",
					["raid_targets"] = {
					},
					["total_without_pet"] = 4068.00974,
					["friendlyfire"] = {
					},
					["end_time"] = 1618674514,
					["dps_started"] = false,
					["total"] = 4068.00974,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Crimson Mandragora",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 288,
								["targets"] = {
									["Thordren"] = 2188,
								},
								["n_dmg"] = 2188,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 2188,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 9,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[161533] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 353,
								["targets"] = {
									["Thordren"] = 1043,
								},
								["n_dmg"] = 1043,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 1043,
								["c_max"] = 0,
								["id"] = 161533,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 3,
								["a_amt"] = 0,
								["n_amt"] = 3,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[167129] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 309,
								["targets"] = {
									["Thordren"] = 837,
								},
								["n_dmg"] = 837,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 837,
								["c_max"] = 0,
								["id"] = 167129,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 3,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[161299] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 161299,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84391-00007AFF26",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674484,
					["delay"] = 0,
					["damage_taken"] = 7270.00974,
				}, -- [11]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.007945,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
					},
					["dps_started"] = false,
					["pets"] = {
					},
					["last_event"] = 0,
					["aID"] = "",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.007945,
					["on_hold"] = false,
					["end_time"] = 1618674632,
					["monster"] = true,
					["total"] = 0.007945,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Aggressive Growth",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["serial"] = "Vehicle-0-4215-1116-69-84451-00007AFE81",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674629,
					["delay"] = 0,
					["damage_taken"] = 631.0079450000001,
				}, -- [12]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 871.017045,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 2748,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["aID"] = "84373",
					["raid_targets"] = {
					},
					["total_without_pet"] = 2748.017045,
					["friendlyfire"] = {
					},
					["end_time"] = 1618674700,
					["dps_started"] = false,
					["total"] = 2748.017045,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Harvester Ommru",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 272,
								["targets"] = {
									["Thordren"] = 1592,
								},
								["n_dmg"] = 1592,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 8,
								["total"] = 1592,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 1,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 7,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[169571] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 370,
								["targets"] = {
									["Thordren"] = 1089,
								},
								["n_dmg"] = 1089,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 1089,
								["c_max"] = 0,
								["id"] = 169571,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 3,
								["a_amt"] = 0,
								["n_amt"] = 3,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
							[173861] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 67,
								["targets"] = {
									["Thordren"] = 67,
								},
								["n_dmg"] = 67,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 67,
								["c_max"] = 0,
								["id"] = 173861,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["a_amt"] = 0,
								["n_amt"] = 1,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84373-00007AFF00",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674671,
					["delay"] = 0,
					["damage_taken"] = 14190.017045,
				}, -- [13]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 391.017681,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 2583,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["aID"] = "80744",
					["raid_targets"] = {
					},
					["total_without_pet"] = 2583.017681,
					["friendlyfire"] = {
					},
					["end_time"] = 1618674791,
					["dps_started"] = false,
					["total"] = 2583.017681,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Thorny Stabber",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 316,
								["g_amt"] = 0,
								["n_max"] = 160,
								["targets"] = {
									["Thordren"] = 2583,
								},
								["n_dmg"] = 2267,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 18,
								["total"] = 2583,
								["c_max"] = 316,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 17,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
						},
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-4215-1116-69-80744-00007AF7D8",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674758,
					["delay"] = 0,
					["damage_taken"] = 10792.017681,
				}, -- [14]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 1029.01281,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 3103,
					},
					["on_hold"] = false,
					["pets"] = {
					},
					["monster"] = true,
					["aID"] = "85809",
					["raid_targets"] = {
					},
					["total_without_pet"] = 3103.01281,
					["friendlyfire"] = {
					},
					["end_time"] = 1618674791,
					["dps_started"] = false,
					["total"] = 3103.01281,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Thorny Leafling",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 465,
								["g_amt"] = 0,
								["n_max"] = 138,
								["targets"] = {
									["Thordren"] = 3103,
								},
								["n_dmg"] = 2638,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 44,
								["total"] = 3103,
								["c_max"] = 140,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 1,
								["extra"] = {
								},
								["a_dmg"] = 72,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 1,
								["n_amt"] = 39,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[174732] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 174732,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 8,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							},
						},
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-4215-1116-69-85809-00007AF6A4",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674760,
					["delay"] = 0,
					["damage_taken"] = 11980.01281,
				}, -- [15]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 99.022044,
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
						["Thordren"] = 331,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 331.022044,
					["last_event"] = 0,
					["aID"] = "85807",
					["dps_started"] = false,
					["total"] = 331.022044,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Weald Stinger",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 120,
								["targets"] = {
									["Thordren"] = 331,
								},
								["n_dmg"] = 331,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 331,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 3,
								["b_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
						},
					},
					["on_hold"] = false,
					["end_time"] = 1618674791,
					["friendlyfire"] = {
					},
					["serial"] = "Creature-0-4215-1116-69-85807-00007AF7BA",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1618674779,
					["delay"] = 0,
					["damage_taken"] = 8963.022044,
				}, -- [16]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.005325,
					["aID"] = "88427",
					["damage_from"] = {
						["Thordren"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.005325,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1618674828,
					["on_hold"] = false,
					["friendlyfire_total"] = 0,
					["nome"] = "Twilight Wasp",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["friendlyfire"] = {
					},
					["total"] = 0.005325,
					["serial"] = "Creature-0-4215-1116-69-88427-00007AFF64",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 7.005325,
					["start_time"] = 1618674825,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [17]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["targets_overheal"] = {
						["Thordren"] = 536,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["classe"] = "MAGE",
					["totalover"] = 1272.103427,
					["total_without_pet"] = 14091.103427,
					["total"] = 14091.103427,
					["targets_absorbs"] = {
						["Thordren"] = 3005,
					},
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-09A108A3",
					["totalabsorb"] = 7115.103427,
					["last_hps"] = 0,
					["targets"] = {
						["Thordren"] = 7482,
					},
					["totalover_without_pet"] = 0.103427,
					["healing_taken"] = 14091.103427,
					["fight_component"] = true,
					["end_time"] = 1605462880,
					["healing_from"] = {
						["Thordren"] = true,
					},
					["aID"] = "76-09A108A3",
					["nome"] = "Thordren",
					["spells"] = {
						["_ActorTable"] = {
							[235450] = {
								["c_amt"] = 0,
								["totalabsorb"] = 2932,
								["targets_overheal"] = {
								},
								["n_max"] = 404,
								["targets"] = {
									["Thordren"] = 2932,
								},
								["n_min"] = 0,
								["counter"] = 11,
								["overheal"] = 0,
								["total"] = 2932,
								["c_max"] = 0,
								["id"] = 235450,
								["targets_absorbs"] = {
									["Thordren"] = 2932,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 2932,
								["n_amt"] = 11,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[259760] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thordren"] = 23,
								},
								["n_max"] = 12,
								["targets"] = {
									["Thordren"] = 36,
								},
								["n_min"] = 0,
								["counter"] = 5,
								["overheal"] = 23,
								["total"] = 36,
								["c_max"] = 0,
								["id"] = 259760,
								["targets_absorbs"] = {
								},
								["c_min"] = 0,
								["c_curado"] = 0,
								["n_curado"] = 36,
								["totaldenied"] = 0,
								["n_amt"] = 5,
								["absorbed"] = 0,
							},
							[270117] = {
								["c_amt"] = 6,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thordren"] = 1249,
								},
								["n_max"] = 236,
								["targets"] = {
									["Thordren"] = 6940,
								},
								["n_min"] = 0,
								["counter"] = 30,
								["overheal"] = 1249,
								["total"] = 6940,
								["c_max"] = 472,
								["id"] = 270117,
								["targets_absorbs"] = {
								},
								["c_curado"] = 2361,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 4579,
								["n_amt"] = 24,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[235313] = {
								["c_amt"] = 0,
								["totalabsorb"] = 4183,
								["targets_overheal"] = {
								},
								["n_max"] = 529,
								["targets"] = {
									["Thordren"] = 4183,
								},
								["n_min"] = 0,
								["counter"] = 37,
								["overheal"] = 0,
								["total"] = 4183,
								["c_max"] = 0,
								["id"] = 235313,
								["targets_absorbs"] = {
									["Thordren"] = 4183,
								},
								["c_min"] = 0,
								["c_curado"] = 0,
								["n_amt"] = 37,
								["n_curado"] = 4183,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["grupo"] = true,
					["heal_enemy_amt"] = 0,
					["start_time"] = 1605462670,
					["custom"] = 0,
					["last_event"] = 0,
					["spec"] = 62,
					["totaldenied"] = 0.103427,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 4336.094168,
					["resource"] = 110.176692,
					["targets"] = {
						["Thordren"] = 1894,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["aID"] = "76-09A108A3",
					["passiveover"] = 0.003819,
					["fight_component"] = true,
					["alternatepower"] = 0.09416800000000002,
					["nome"] = "Thordren",
					["spells"] = {
						["_ActorTable"] = {
							[59914] = {
								["total"] = 4336,
								["id"] = 59914,
								["totalover"] = 0,
								["targets"] = {
									["Thordren"] = 1894,
								},
								["counter"] = 43,
							},
						},
						["tipo"] = 7,
					},
					["grupo"] = true,
					["total"] = 4336.094168,
					["classe"] = "MAGE",
					["spec"] = 62,
					["last_event"] = 0,
					["flag_original"] = 1297,
					["tipo"] = 3,
					["serial"] = "Player-76-09A108A3",
					["totalover"] = 0.003819,
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["debuff_uptime_spells"] = {
						["_ActorTable"] = {
							[31661] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 10,
								["id"] = 31661,
								["uptime"] = 12,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[122] = {
								["counter"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 4,
								["id"] = 122,
								["uptime"] = 10,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[171389] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 1,
								["id"] = 171389,
								["uptime"] = 2,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[2120] = {
								["refreshamt"] = 15,
								["activedamt"] = 7,
								["appliedamt"] = 19,
								["id"] = 2120,
								["uptime"] = 47,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[226757] = {
								["refreshamt"] = 5,
								["activedamt"] = 0,
								["appliedamt"] = 2,
								["id"] = 226757,
								["uptime"] = 28,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[280286] = {
								["refreshamt"] = 11,
								["activedamt"] = 1,
								["appliedamt"] = 20,
								["id"] = 280286,
								["uptime"] = 137,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[12654] = {
								["counter"] = 0,
								["activedamt"] = 9,
								["appliedamt"] = 36,
								["id"] = 12654,
								["uptime"] = 134,
								["targets"] = {
								},
								["refreshamt"] = 91,
							},
							[236299] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 7,
								["id"] = 236299,
								["uptime"] = 30,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["interrupt"] = 1.003357,
					["pets"] = {
					},
					["interrupt_targets"] = {
						["Harvester Ommru"] = 1,
					},
					["cc_done_spells"] = {
						["_ActorTable"] = {
							[122] = {
								["id"] = 122,
								["targets"] = {
									["Podling Scavenger"] = 2,
									["Ancient Pearltusk"] = 2,
								},
								["counter"] = 4,
							},
							[31661] = {
								["id"] = 31661,
								["targets"] = {
									["Thorny Leafling"] = 3,
									["Podling Scavenger"] = 5,
									["Thorny Stabber"] = 2,
								},
								["counter"] = 10,
							},
						},
						["tipo"] = 9,
					},
					["aID"] = "76-09A108A3",
					["interrupt_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[2139] = {
								["id"] = 2139,
								["interrompeu_oque"] = {
									[173861] = 1,
								},
								["targets"] = {
									["Harvester Ommru"] = 1,
								},
								["counter"] = 1,
							},
						},
					},
					["buff_uptime_spells"] = {
						["_ActorTable"] = {
							[186401] = {
								["counter"] = 0,
								["activedamt"] = 14,
								["appliedamt"] = 14,
								["id"] = 186401,
								["uptime"] = 205,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[198065] = {
								["counter"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 198065,
								["uptime"] = 3,
								["targets"] = {
								},
								["refreshamt"] = 1,
							},
							[326396] = {
								["refreshamt"] = 0,
								["activedamt"] = 10,
								["appliedamt"] = 10,
								["id"] = 326396,
								["uptime"] = 121,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[12042] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 12042,
								["uptime"] = 7,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[236060] = {
								["counter"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 236060,
								["uptime"] = 12,
								["targets"] = {
								},
								["refreshamt"] = 4,
							},
							[116267] = {
								["refreshamt"] = 0,
								["activedamt"] = 8,
								["appliedamt"] = 8,
								["id"] = 116267,
								["uptime"] = 116,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[48107] = {
								["counter"] = 0,
								["activedamt"] = 30,
								["appliedamt"] = 30,
								["id"] = 48107,
								["uptime"] = 40,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[48108] = {
								["counter"] = 0,
								["activedamt"] = 20,
								["appliedamt"] = 20,
								["id"] = 48108,
								["uptime"] = 20,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[1459] = {
								["refreshamt"] = 0,
								["activedamt"] = 9,
								["appliedamt"] = 9,
								["id"] = 1459,
								["uptime"] = 128,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[235313] = {
								["counter"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 235313,
								["uptime"] = 40,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[186403] = {
								["refreshamt"] = 0,
								["activedamt"] = 9,
								["appliedamt"] = 9,
								["id"] = 186403,
								["uptime"] = 118,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[256374] = {
								["refreshamt"] = 0,
								["activedamt"] = 13,
								["appliedamt"] = 13,
								["id"] = 256374,
								["uptime"] = 104,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[289982] = {
								["refreshamt"] = 0,
								["activedamt"] = 14,
								["appliedamt"] = 14,
								["id"] = 289982,
								["uptime"] = 174,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[236298] = {
								["refreshamt"] = 3,
								["activedamt"] = 9,
								["appliedamt"] = 9,
								["id"] = 236298,
								["uptime"] = 31,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[116014] = {
								["counter"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 116014,
								["uptime"] = 20,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[212653] = {
								["counter"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 212653,
								["uptime"] = 2,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[2479] = {
								["counter"] = 0,
								["appliedamt"] = 1,
								["activedamt"] = 1,
								["actived_at"] = 1618529853,
								["id"] = 2479,
								["uptime"] = 0,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[157644] = {
								["refreshamt"] = 2,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 157644,
								["uptime"] = 24,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[229376] = {
								["refreshamt"] = 0,
								["appliedamt"] = 2,
								["activedamt"] = 2,
								["uptime"] = 0,
								["id"] = 229376,
								["actived_at"] = 3237349264,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[264774] = {
								["refreshamt"] = 0,
								["activedamt"] = 10,
								["appliedamt"] = 10,
								["id"] = 264774,
								["uptime"] = 17,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[269083] = {
								["refreshamt"] = 0,
								["activedamt"] = 23,
								["appliedamt"] = 23,
								["id"] = 269083,
								["uptime"] = 323,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[235450] = {
								["refreshamt"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 235450,
								["uptime"] = 70,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[276743] = {
								["refreshamt"] = 0,
								["activedamt"] = 8,
								["appliedamt"] = 8,
								["id"] = 276743,
								["uptime"] = 18,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["interrompeu_oque"] = {
						[173861] = 1,
					},
					["fight_component"] = true,
					["debuff_uptime"] = 400,
					["debuff_uptime_targets"] = {
					},
					["cc_done"] = 14.029062,
					["nome"] = "Thordren",
					["spec"] = 62,
					["grupo"] = true,
					["cc_done_targets"] = {
						["Thorny Stabber"] = 2,
						["Thorny Leafling"] = 3,
						["Podling Scavenger"] = 7,
						["Ancient Pearltusk"] = 2,
					},
					["classe"] = "MAGE",
					["buff_uptime_targets"] = {
					},
					["tipo"] = 4,
					["buff_uptime"] = 1593,
					["last_event"] = 0,
					["serial"] = "Player-76-09A108A3",
					["spell_cast"] = {
						[30451] = 38,
						[1449] = 5,
						[31661] = 3,
						[122] = 2,
						[2139] = 1,
						[1459] = 1,
						[235313] = 2,
						[133] = 8,
						[5143] = 9,
						[108853] = 32,
						[212653] = 2,
						[44425] = 8,
						[235450] = 2,
						[257541] = 9,
						[2948] = 8,
						[30449] = 2,
						[171389] = 1,
						[11366] = 17,
						[2120] = 11,
					},
				}, -- [1]
				{
					["monster"] = true,
					["aID"] = "82378",
					["nome"] = "Moonbark Ancient",
					["pets"] = {
					},
					["spell_cast"] = {
						[165851] = 1,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3783-1116-30456-82378-00003124E1",
					["flag_original"] = 68168,
				}, -- [2]
				{
					["monster"] = true,
					["aID"] = "80818",
					["nome"] = "Luminous Orchid",
					["pets"] = {
					},
					["spell_cast"] = {
						[169567] = 7,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3783-1116-30456-80818-0000B12CB6",
					["flag_original"] = 68168,
				}, -- [3]
				{
					["monster"] = true,
					["spell_cast"] = {
						[158252] = 5,
						[158207] = 6,
					},
					["nome"] = "Ancient Pearltusk",
					["classe"] = "UNKNOW",
					["pets"] = {
					},
					["flag_original"] = 68168,
					["tipo"] = 4,
					["aID"] = "82452",
					["last_event"] = 0,
					["serial"] = "Creature-0-3783-1116-30456-82452-0000314C60",
					["fight_component"] = true,
				}, -- [4]
				{
					["monster"] = true,
					["aID"] = "82354",
					["nome"] = "Seacliff Kaliri",
					["pets"] = {
					},
					["spell_cast"] = {
						[163716] = 1,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3783-1116-30456-82354-000030A85F",
					["flag_original"] = 68168,
				}, -- [5]
				{
					["monster"] = true,
					["flag_original"] = 68168,
					["nome"] = "Shadowmoon Stalker",
					["tipo"] = 4,
					["spell_cast"] = {
						[131944] = 1,
					},
					["fight_component"] = true,
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["aID"] = "82308",
					["serial"] = "Creature-0-4215-1116-50-82308-000275C465",
					["pets"] = {
					},
				}, -- [6]
				{
					["monster"] = true,
					["flag_original"] = 68168,
					["nome"] = "Midnight Kaliri",
					["tipo"] = 4,
					["spell_cast"] = {
						[163716] = 1,
					},
					["fight_component"] = true,
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["aID"] = "87671",
					["serial"] = "Creature-0-4215-1116-50-87671-000078C8A1",
					["pets"] = {
					},
				}, -- [7]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["spell_cast"] = {
						[169519] = 4,
					},
					["nome"] = "Podling Scavenger",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 2632,
					["aID"] = "84402",
					["last_event"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84402-00017AF674",
					["tipo"] = 4,
				}, -- [8]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["spell_cast"] = {
						[161533] = 3,
						[161299] = 1,
					},
					["nome"] = "Crimson Mandragora",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 68168,
					["aID"] = "84391",
					["last_event"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84391-00007AFF26",
					["tipo"] = 4,
				}, -- [9]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["spell_cast"] = {
						[169571] = 3,
						[173861] = 1,
					},
					["nome"] = "Harvester Ommru",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 68168,
					["aID"] = "84373",
					["last_event"] = 0,
					["serial"] = "Creature-0-4215-1116-69-84373-00007AFF00",
					["tipo"] = 4,
				}, -- [10]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["spell_cast"] = {
						[174732] = 8,
					},
					["nome"] = "Thorny Leafling",
					["pets"] = {
					},
					["fight_component"] = true,
					["flag_original"] = 2632,
					["aID"] = "85809",
					["last_event"] = 0,
					["serial"] = "Creature-0-4215-1116-69-85809-00007AF6A4",
					["tipo"] = 4,
				}, -- [11]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1605462855,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["spells_cast_timeline"] = {
		},
		["combat_counter"] = 4814,
		["totals"] = {
			163230.258399, -- [1]
			14091.101467, -- [2]
			{
				0, -- [1]
				[0] = 4336.090349,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 1.003357,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "12:54:15",
		["end_time"] = 7714.274,
		["cleu_events"] = {
			["n"] = 1,
		},
		["totals_grupo"] = {
			135387.106894, -- [1]
			14091.101467, -- [2]
			{
				0, -- [1]
				[0] = 4336.090349,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 1.003357,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["frags"] = {
		},
		["hasSaved"] = true,
		["segments_added"] = {
			{
				["elapsed"] = 4.88799999999992,
				["type"] = 0,
				["name"] = "Weald Stinger",
				["clock"] = "11:54:37",
			}, -- [1]
			{
				["elapsed"] = 6.6850000000004,
				["type"] = 0,
				["name"] = "Weald Stinger",
				["clock"] = "11:54:16",
			}, -- [2]
			{
				["elapsed"] = 19.49699999999939,
				["type"] = 0,
				["name"] = "Thorny Stabber",
				["clock"] = "11:53:29",
			}, -- [3]
			{
				["elapsed"] = 11.84300000000076,
				["type"] = 0,
				["name"] = "Thorny Stabber",
				["clock"] = "11:53:00",
			}, -- [4]
			{
				["elapsed"] = 7.350000000000364,
				["type"] = 0,
				["name"] = "Podling Scavenger",
				["clock"] = "11:51:55",
			}, -- [5]
			{
				["elapsed"] = 31.79899999999998,
				["type"] = 0,
				["name"] = "Harvester Ommru",
				["clock"] = "11:51:08",
			}, -- [6]
			{
				["elapsed"] = 16.50900000000002,
				["type"] = 0,
				["name"] = "Podling Scavenger",
				["clock"] = "11:50:16",
			}, -- [7]
			{
				["elapsed"] = 26.69700000000012,
				["type"] = 0,
				["name"] = "Crimson Mandragora",
				["clock"] = "11:48:07",
			}, -- [8]
			{
				["elapsed"] = 21.31500000000051,
				["type"] = 0,
				["name"] = "Podling Trapper",
				["clock"] = "11:47:37",
			}, -- [9]
			{
				["elapsed"] = 12.11700000000019,
				["type"] = 0,
				["name"] = "Midnight Kaliri",
				["clock"] = "19:44:27",
			}, -- [10]
			{
				["elapsed"] = 2.496999999999389,
				["type"] = 0,
				["name"] = "Shadowmoon Stalker",
				["clock"] = "19:42:07",
			}, -- [11]
			{
				["elapsed"] = 3.477999999999156,
				["type"] = 0,
				["name"] = "Shadowmoon Stalker",
				["clock"] = "19:41:56",
			}, -- [12]
			{
				["elapsed"] = 8.19800000000032,
				["type"] = 0,
				["name"] = "Shadowmoon Stalker",
				["clock"] = "19:38:44",
			}, -- [13]
			{
				["elapsed"] = 29.59700000000157,
				["type"] = 0,
				["name"] = "Ancient Pearltusk",
				["clock"] = "19:37:33",
			}, -- [14]
			{
				["elapsed"] = 14.21299999999974,
				["type"] = 0,
				["name"] = "Seacliff Kaliri",
				["clock"] = "12:59:44",
			}, -- [15]
			{
				["elapsed"] = 16.23399999999947,
				["type"] = 0,
				["name"] = "Ancient Pearltusk",
				["clock"] = "12:57:34",
			}, -- [16]
			{
				["elapsed"] = 13.28400000000056,
				["type"] = 0,
				["name"] = "Ancient Pearltusk",
				["clock"] = "12:57:14",
			}, -- [17]
			{
				["elapsed"] = 14.73599999999988,
				["type"] = 0,
				["name"] = "Ancient Pearltusk",
				["clock"] = "12:56:47",
			}, -- [18]
			{
				["elapsed"] = 9.72399999999925,
				["type"] = 0,
				["name"] = "Ancient Pearltusk",
				["clock"] = "12:56:15",
			}, -- [19]
			{
				["elapsed"] = 7.574000000000524,
				["type"] = 0,
				["name"] = "Ancient Pearltusk",
				["clock"] = "12:55:54",
			}, -- [20]
			{
				["elapsed"] = 12.5019999999995,
				["type"] = 0,
				["name"] = "Luminous Orchid",
				["clock"] = "12:55:23",
			}, -- [21]
			{
				["elapsed"] = 7.332999999999629,
				["type"] = 0,
				["name"] = "Luminous Orchid",
				["clock"] = "12:55:06",
			}, -- [22]
			{
				["elapsed"] = 24.50200000000041,
				["type"] = 0,
				["name"] = "Moonbark Ancient",
				["clock"] = "12:54:15",
			}, -- [23]
		},
		["data_fim"] = "11:54:42",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage_section"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage"] = {
			},
		},
		["start_time"] = 7391.701999999999,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["cleu_timeline"] = {
		},
	},
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["character_data"] = {
		["logons"] = 125,
	},
	["last_day"] = "17",
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09A108A3"] = 63,
	},
}
