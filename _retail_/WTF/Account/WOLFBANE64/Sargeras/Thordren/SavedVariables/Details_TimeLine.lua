
DetailsTimeLineDB = {
	["max_segments"] = 4,
	["combat_data"] = {
		{
			["date_start"] = "23:13:40",
			["date_end"] = "23:14:56",
			["name"] = "Odyn",
			["total_time"] = 76.0470000000205,
		}, -- [1]
		{
			["date_start"] = "23:11:49",
			["date_end"] = "23:12:44",
			["name"] = "God-King Skovald",
			["total_time"] = 55.0070000000414,
		}, -- [2]
		{
			["date_start"] = "23:04:47",
			["date_end"] = "23:06:07",
			["name"] = "Hyrja",
			["total_time"] = 80,
		}, -- [3]
		{
			["date_start"] = "23:00:25",
			["date_end"] = "23:01:09",
			["name"] = "Fenryr",
			["total_time"] = 44.0120000000461,
		}, -- [4]
	},
	["hide_on_combat"] = false,
	["IndividualSpells"] = {
	},
	["useicons"] = false,
	["window_scale"] = 1,
	["backdrop_color"] = {
		0, -- [1]
		0, -- [2]
		0, -- [3]
		0.4, -- [4]
	},
	["deaths_data"] = {
		{
		}, -- [1]
		{
		}, -- [2]
		{
		}, -- [3]
		{
		}, -- [4]
	},
	["debuff_timeline"] = {
		{
			["Overprotec-Drakkari"] = {
				[197967] = {
					44.2299999999814, -- [1]
					50.6030000000028, -- [2]
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Nostirak"] = {
				[197963] = {
					44.2299999999814, -- [1]
					51.8209999999963, -- [2]
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Strelixia-Drakkari"] = {
				[197965] = {
					44.2299999999814, -- [1]
					56.2260000000242, -- [2]
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Justblazee"] = {
				[57724] = {
					76.0470000000205, -- [1]
					["source"] = "Justblazee",
					["active"] = true,
				},
			},
			["Thordren"] = {
				[200988] = {
					37.8229999999749, -- [1]
					37.9230000000098, -- [2]
					["source"] = "[*] Spear of Light",
					["active"] = false,
				},
				[197966] = {
					44.2299999999814, -- [1]
					53.1560000000172, -- [2]
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Jintyr"] = {
				[57724] = {
					76.0470000000205, -- [1]
					["source"] = "Jintyr",
					["active"] = true,
				},
			},
			["Tyradrel-Drakkari"] = {
				[197964] = {
					44.2299999999814, -- [1]
					56.2260000000242, -- [2]
					["source"] = "Odyn",
					["active"] = false,
				},
			},
		}, -- [1]
		{
			["Strelixia-Drakkari"] = {
				[193702] = {
					34.9530000000377, -- [1]
					37.054999999993, -- [2]
					39.5410000000265, -- [3]
					40.75900000002, -- [4]
					["source"] = "[*] Infernal Flames",
					["active"] = false,
				},
			},
			["Thordren"] = {
				[193702] = {
					36.5210000000079, -- [1]
					38.505999999994, -- [2]
					["source"] = "[*] Infernal Flames",
					["active"] = false,
				},
			},
			["Tyradrel-Drakkari"] = {
				[193686] = {
					46.515000000014, -- [1]
					54.3400000000256, -- [2]
					["source"] = "God-King Skovald",
					["active"] = false,
				},
			},
		}, -- [2]
		{
			["Overprotec-Drakkari"] = {
				[203963] = {
					5.83900000003632, -- [1]
					18.4530000000377, -- [2]
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Nostirak"] = {
				[203963] = {
					5.30600000004051, -- [1]
					18.4530000000377, -- [2]
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Thordren"] = {
				[203963] = {
					10.6780000000144, -- [1]
					18.4530000000377, -- [2]
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Strelixia-Drakkari"] = {
				[203963] = {
					12.4960000000428, -- [1]
					18.4530000000377, -- [2]
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Tyradrel-Drakkari"] = {
				[203963] = {
					4.4550000000163, -- [1]
					6.29000000003725, -- [2]
					9.02600000001257, -- [3]
					9.67700000002515, -- [4]
					13.9980000000214, -- [5]
					13.9980000000214, -- [6]
					17.2179999999935, -- [7]
					17.9189999999944, -- [8]
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
		}, -- [3]
		{
			["Overprotec-Drakkari"] = {
				[196497] = {
					1.10100000002421, -- [1]
					42.945000000007, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
				[196838] = {
					12.9470000000438, -- [1]
					18.9360000000452, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
				[197556] = {
					30.6320000000414, -- [1]
					36.9380000000238, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Nostirak"] = {
				[196497] = {
					1.10100000002421, -- [1]
					42.945000000007, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
				[197556] = {
					30.6320000000414, -- [1]
					35.1359999999986, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Strelixia-Drakkari"] = {
				[197556] = {
					30.6320000000414, -- [1]
					36.6210000000428, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Thordren"] = {
				[197556] = {
					30.6320000000414, -- [1]
					33.6520000000019, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
				[196497] = {
					35.1870000000345, -- [1]
					42.945000000007, -- [2]
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
		}, -- [4]
	},
	["cooldowns_timeline"] = {
		{
			["Overprotec-Drakkari"] = {
				{
					17.7860000000219, -- [1]
					"Overprotec-Drakkari", -- [2]
					108271, -- [3]
				}, -- [1]
			},
		}, -- [1]
		{
			["Overprotec-Drakkari"] = {
				{
					14.9990000000107, -- [1]
					"Overprotec-Drakkari", -- [2]
					108271, -- [3]
				}, -- [1]
			},
			["Nostirak"] = {
				{
					20.4550000000163, -- [1]
					"[*] raid wide cooldown", -- [2]
					97462, -- [3]
				}, -- [1]
			},
		}, -- [2]
		{
			["Nostirak"] = {
				{
					55.6579999999958, -- [1]
					"[*] raid wide cooldown", -- [2]
					97462, -- [3]
				}, -- [1]
			},
		}, -- [3]
		{
		}, -- [4]
	},
	["BossSpellCast"] = {
	},
}
