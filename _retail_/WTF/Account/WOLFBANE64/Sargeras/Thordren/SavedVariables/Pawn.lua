
PawnOptions = {
	["LastVersion"] = 2.0418,
	["LastPlayerFullName"] = "Thordren-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 162801,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:162801::::::::45:64::29:2:1472:3336::::|h[Spell-Splintered Cowl]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 58,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::45:64::11:4:4932:4933:4935:1472::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 155042,
			["Level"] = 58,
			["Link"] = "|cff0070dd|Hitem:155042::::::::45:64::25:2:1471:4785::::|h[Navigator's Mantle]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 159904,
			["Level"] = 58,
			["Link"] = "|cff0070dd|Hitem:159904::::::::45:64::11:::::|h[Robes of the Champion]|h|r",
		}, -- [5]
		{
			["ID"] = 158285,
			["Level"] = 50,
			["Link"] = "|cff1eff00|Hitem:158285::::::::45:64::11:1:4790:1:9:45:::|h[Navigator's Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 162802,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:162802::::::::45:64::30:2:1472:3336::::|h[Spell-Splintered Pantaloons]|h|r",
		}, -- [7]
		{
			["ID"] = 162799,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:162799::::::::45:64::29:3:1808:1472:3528::::|h[Spell-Splintered Treads]|h|r",
		}, -- [8]
		{
			["ID"] = 162806,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:162806::::::::45:64::30:2:1472:3336::::|h[Spell-Splintered Wristwraps]|h|r",
		}, -- [9]
		{
			["ID"] = 155029,
			["Level"] = 50,
			["Link"] = "|cff1eff00|Hitem:155029::::::::45:64::11:1:4787:1:9:45:::|h[Navigator's Gloves]|h|r",
		}, -- [10]
		{
			["ID"] = 128169,
			["Level"] = 54,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff00ccff|Hitem:128169::::::::47:63:::1:3592::::|h[Signet of the Third Fleet]|h|r",
		}, -- [11]
		{
			["ID"] = 128169,
			["Level"] = 52,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff00ccff|Hitem:128169::::::::46:62:::1:3592::::|h[Signet of the Third Fleet]|h|r",
		}, -- [12]
		{
			["ID"] = 122362,
			["Level"] = 54,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122362::::::::47:63:::1:3592::::|h[Discerning Eye of the Beast]|h|r",
		}, -- [13]
		{
			["ID"] = 122362,
			["Level"] = 52,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122362::::::::46:62:::1:3592::::|h[Discerning Eye of the Beast]|h|r",
		}, -- [14]
		{
			["ID"] = 155086,
			["Level"] = 50,
			["Link"] = "|cff1eff00|Hitem:155086::::::::45:64::11:1:4787:1:9:45:::|h[Keelson's Bloody Drape]|h|r",
		}, -- [15]
		{
			["ID"] = 163015,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:163015::::::::45:64::30:2:1472:3528::::|h[Spell-Splintered Rod]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "MAGE",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
