
PawnOptions = {
	["LastVersion"] = 2.0234,
	["LastPlayerFullName"] = "Thorggar-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "WARRIOR",
	["LastAdded"] = 1,
}
