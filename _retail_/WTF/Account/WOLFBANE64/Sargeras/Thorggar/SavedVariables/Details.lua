
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 13,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008493,
							["damage_from"] = {
								["Northwatch Scout"] = true,
							},
							["targets"] = {
								["Northwatch Scout"] = 33,
							},
							["total"] = 33.008493,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 33.008493,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1547955002,
							["friendlyfire_total"] = 0,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Northwatch Scout"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 26.3435698324659,
							["custom"] = 0,
							["last_event"] = 1547955002,
							["damage_taken"] = 4.008493,
							["start_time"] = 1547955001,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 13,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 8.00232,
							["total_without_pet"] = 4.00232,
							["total"] = 4.00232,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.00232,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["totalover_without_pet"] = 0.00232,
							["healing_taken"] = 4.00232,
							["fight_component"] = true,
							["end_time"] = 1547955002,
							["targets_overheal"] = {
								["Thorggar"] = 8,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 8,
										},
										["n_max"] = 4,
										["targets"] = {
											["Thorggar"] = 4,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 8,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 4,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547955002,
							["custom"] = 0,
							["last_event"] = 1547955002,
							["spec"] = 71,
							["totaldenied"] = 0.00232,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 13,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.004157,
							["resource"] = 0.004157,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.004157,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547955001,
							["alternatepower"] = 0.004157,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 13,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[88163] = 1,
								[1464] = 1,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 13,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47510.374,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 16,
				["playing_solo"] = true,
				["totals"] = {
					32.981741, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 2.997244,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 4.00232,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 33.008493,
						}, -- [1]
					},
				},
				["end_time"] = 47511.627,
				["combat_id"] = 13,
				["TotalElapsedCombatTime"] = 1.25299999999697,
				["CombatEndedAt"] = 47511.627,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:30:02",
				["data_inicio"] = "22:30:01",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					33, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47510.374,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005972,
							["total"] = 51.005972,
							["damage_from"] = {
								["Northwatch Scout"] = true,
							},
							["targets"] = {
								["Northwatch Scout"] = 51,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 51.005972,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954995,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 34,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 24.1162988179901,
							["custom"] = 0,
							["last_event"] = 1547954995,
							["damage_taken"] = 3.005972,
							["start_time"] = 1547954993,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 12,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 10.003954,
							["total_without_pet"] = 3.003954,
							["total"] = 3.003954,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.003954,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["totalover_without_pet"] = 0.003954,
							["healing_taken"] = 3.003954,
							["fight_component"] = true,
							["end_time"] = 1547954995,
							["targets_overheal"] = {
								["Thorggar"] = 10,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 10,
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 10,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 3,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954995,
							["custom"] = 0,
							["last_event"] = 1547954995,
							["spec"] = 71,
							["totaldenied"] = 0.003954,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 12,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.003175,
							["resource"] = 0.003175,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.003175,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954993,
							["alternatepower"] = 0.003175,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 12,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[88163] = 2,
								[1464] = 2,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47502.336,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 15,
				["playing_solo"] = true,
				["totals"] = {
					50.998467, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 3.003954,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 51.005972,
						}, -- [1]
					},
				},
				["end_time"] = 47504.711,
				["combat_id"] = 12,
				["TotalElapsedCombatTime"] = 2.375,
				["CombatEndedAt"] = 47504.711,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:29:55",
				["data_inicio"] = "22:29:53",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					51, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47502.336,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [2]
			{
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005868,
							["total"] = 34.005868,
							["damage_from"] = {
							},
							["targets"] = {
								["Northwatch Scout"] = 34,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.005868,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954978,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 23.323640603587,
							["custom"] = 0,
							["last_event"] = 1547954977,
							["damage_taken"] = 0.005868,
							["start_time"] = 1547954977,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 11,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorb"] = 0.002055,
							["last_hps"] = 0,
							["healing_from"] = {
							},
							["targets"] = {
							},
							["targets_absorbs"] = {
							},
							["pets"] = {
							},
							["totalover_without_pet"] = 0.002055,
							["targets_overheal"] = {
								["Thorggar"] = 13,
							},
							["heal_enemy_amt"] = 0,
							["totalover"] = 13.002055,
							["total_without_pet"] = 0.002055,
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["end_time"] = 1547954978,
							["total"] = 0.002055,
							["healing_taken"] = 0.002055,
							["start_time"] = 1547954977,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 13,
										},
										["n_max"] = 0,
										["targets"] = {
											["Thorggar"] = 0,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 13,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 0,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 71,
							["heal_enemy"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["custom"] = 0,
							["last_event"] = 1547954977,
							["on_hold"] = false,
							["totaldenied"] = 0.002055,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 11,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 11,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["tipo"] = 4,
							["spell_cast"] = {
								[1464] = 1,
							},
							["serial"] = "Player-76-0A082E91",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47486.28,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 14,
				["playing_solo"] = true,
				["totals"] = {
					33.992203, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.00411900000000021,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 0.002055,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 34.005868,
						}, -- [1]
					},
				},
				["end_time"] = 47487.738,
				["combat_id"] = 11,
				["TotalElapsedCombatTime"] = 1.45799999999872,
				["CombatEndedAt"] = 47487.738,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:29:38",
				["data_inicio"] = "22:29:37",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					34, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47486.28,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [3]
			{
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008766,
							["total"] = 35.008766,
							["damage_from"] = {
								["Northwatch Scout"] = true,
							},
							["targets"] = {
								["Northwatch Scout"] = 35,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 35.008766,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954968,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Northwatch Scout"] = 18,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 20.1663398616998,
							["custom"] = 0,
							["last_event"] = 1547954967,
							["damage_taken"] = 4.008766,
							["start_time"] = 1547954967,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 10,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 9.004371,
							["total_without_pet"] = 4.004371,
							["total"] = 4.004371,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.004371,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 7,
							},
							["totalover_without_pet"] = 0.004371,
							["healing_taken"] = 4.004371,
							["fight_component"] = true,
							["end_time"] = 1547954968,
							["targets_overheal"] = {
								["Thorggar"] = 9,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 9,
										},
										["n_max"] = 4,
										["targets"] = {
											["Thorggar"] = 4,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 9,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 4,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954967,
							["custom"] = 0,
							["last_event"] = 1547954967,
							["spec"] = 71,
							["totaldenied"] = 0.004371,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 10,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.003217,
							["resource"] = 0.003217,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.003217,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954967,
							["alternatepower"] = 0.003217,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 10,
					["tipo"] = 9,
					["_ActorTable"] = {
					},
				}, -- [4]
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 13,
				["playing_solo"] = true,
				["totals"] = {
					34.988978, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 2.998314,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 4.004371,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 35.008766,
						}, -- [1]
					},
				},
				["end_time"] = 47478.003,
				["combat_id"] = 10,
				["TotalElapsedCombatTime"] = 47478.003,
				["CombatEndedAt"] = 47478.003,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:29:29",
				["data_inicio"] = "22:29:27",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					35, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47476.267,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [4]
			{
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003046,
							["damage_from"] = {
								["Northwatch Scout"] = true,
							},
							["targets"] = {
								["Northwatch Scout"] = 34,
							},
							["total"] = 34.003046,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.003046,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1547954963,
							["friendlyfire_total"] = 0,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Northwatch Scout"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 18,
										["targets"] = {
											["Northwatch Scout"] = 18,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 18,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 26.8162823344612,
							["custom"] = 0,
							["last_event"] = 1547954961,
							["damage_taken"] = 3.003046,
							["start_time"] = 1547954961,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 9,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 9.005338,
							["total_without_pet"] = 3.005338,
							["total"] = 3.005338,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.005338,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["totalover_without_pet"] = 0.005338,
							["healing_taken"] = 3.005338,
							["fight_component"] = true,
							["end_time"] = 1547954963,
							["targets_overheal"] = {
								["Thorggar"] = 9,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 9,
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 9,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 3,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954961,
							["custom"] = 0,
							["last_event"] = 1547954961,
							["spec"] = 71,
							["totaldenied"] = 0.005338,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 9,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.00751,
							["resource"] = 0.00751,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.00751,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954961,
							["alternatepower"] = 0.00751,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 9,
					["tipo"] = 9,
					["_ActorTable"] = {
					},
				}, -- [4]
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47476.267,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 12,
				["playing_solo"] = true,
				["totals"] = {
					33.99317, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 3.005338,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 34.003046,
						}, -- [1]
					},
				},
				["end_time"] = 47472.278,
				["combat_id"] = 9,
				["TotalElapsedCombatTime"] = 1.26799999999639,
				["CombatEndedAt"] = 47472.278,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:29:23",
				["data_inicio"] = "22:29:22",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					34, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47471.01,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [5]
			{
				{
					["combatId"] = 8,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005584,
							["damage_from"] = {
								["Northwatch Scout"] = true,
							},
							["targets"] = {
								["Northwatch Scout"] = 64,
							},
							["total"] = 64.005584,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 64.005584,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1547954958,
							["friendlyfire_total"] = 0,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 31,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Northwatch Scout"] = 47,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 47,
										["c_max"] = 31,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 31,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Northwatch Scout"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 18.4880369728506,
							["custom"] = 0,
							["last_event"] = 1547954957,
							["damage_taken"] = 7.005584,
							["start_time"] = 1547954954,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 8,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 6.006535,
							["total_without_pet"] = 7.006535,
							["total"] = 7.006535,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.006535,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 13,
							},
							["totalover_without_pet"] = 0.006535,
							["healing_taken"] = 7.006535,
							["fight_component"] = true,
							["end_time"] = 1547954958,
							["targets_overheal"] = {
								["Thorggar"] = 6,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 6,
										},
										["n_max"] = 6,
										["targets"] = {
											["Thorggar"] = 7,
										},
										["n_min"] = 1,
										["counter"] = 2,
										["overheal"] = 6,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 7,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954957,
							["custom"] = 0,
							["last_event"] = 1547954957,
							["spec"] = 71,
							["totaldenied"] = 0.006535,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 8,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.007122,
							["resource"] = 0.007122,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.007122,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954954,
							["alternatepower"] = 0.007122,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 8,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["tipo"] = 4,
							["spell_cast"] = {
								[1464] = 1,
								[88163] = 1,
							},
							["serial"] = "Player-76-0A082E91",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 8,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47463.594,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 11,
				["playing_solo"] = true,
				["totals"] = {
					63.993857, -- [1]
					7, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 7.006535,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 64.005584,
						}, -- [1]
					},
				},
				["end_time"] = 47467.34,
				["combat_id"] = 8,
				["TotalElapsedCombatTime"] = 3.74599999999919,
				["CombatEndedAt"] = 47467.34,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:29:18",
				["data_inicio"] = "22:29:14",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					64, -- [1]
					7, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47463.594,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [6]
			{
				{
					["combatId"] = 7,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007561,
							["total"] = 66.007561,
							["damage_from"] = {
								["Northwatch Scout"] = true,
							},
							["targets"] = {
								["Northwatch Scout"] = 66,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 66.007561,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954947,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 32,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Northwatch Scout"] = 32,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 32,
										["c_max"] = 32,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 32,
									}, -- [1]
									[1464] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 34,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Northwatch Scout"] = 34,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 34,
										["c_max"] = 34,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 34,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 67.9789505665853,
							["custom"] = 0,
							["last_event"] = 1547954946,
							["damage_taken"] = 4.007561,
							["start_time"] = 1547954946,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 7,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 9.004872,
							["total_without_pet"] = 4.004872,
							["total"] = 4.004872,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.004872,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 7,
							},
							["totalover_without_pet"] = 0.004872,
							["healing_taken"] = 4.004872,
							["fight_component"] = true,
							["end_time"] = 1547954947,
							["targets_overheal"] = {
								["Thorggar"] = 9,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 9,
										},
										["n_max"] = 4,
										["targets"] = {
											["Thorggar"] = 4,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 9,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 4,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954946,
							["custom"] = 0,
							["last_event"] = 1547954946,
							["spec"] = 71,
							["totaldenied"] = 0.004872,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 7,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.005511,
							["resource"] = 0.005511,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.005511,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954946,
							["alternatepower"] = 0.005511,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 7,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[1464] = 1,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 7,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47455.412,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Northwatch Scout",
				["combat_counter"] = 10,
				["playing_solo"] = true,
				["totals"] = {
					65.990048, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 4.004872,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 66.007561,
						}, -- [1]
					},
				},
				["end_time"] = 47456.383,
				["combat_id"] = 7,
				["TotalElapsedCombatTime"] = 0.97099999999773,
				["CombatEndedAt"] = 47456.383,
				["frags"] = {
					["Northwatch Scout"] = 1,
				},
				["data_fim"] = "22:29:07",
				["data_inicio"] = "22:29:06",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					66, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47455.412,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [7]
			{
				{
					["combatId"] = 6,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007823,
							["total"] = 34.007823,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 34,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.007823,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954881,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Mottled Boar"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 17,
										["targets"] = {
											["Mottled Boar"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 17,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 17,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 19.3446092149991,
							["custom"] = 0,
							["last_event"] = 1547954879,
							["damage_taken"] = 4.007823,
							["start_time"] = 1547954879,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 6,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 9.005495,
							["total_without_pet"] = 4.005495,
							["total"] = 4.005495,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.005495,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["totalover_without_pet"] = 0.005495,
							["healing_taken"] = 4.005495,
							["fight_component"] = true,
							["end_time"] = 1547954881,
							["targets_overheal"] = {
								["Thorggar"] = 9,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 9,
										},
										["n_max"] = 4,
										["targets"] = {
											["Thorggar"] = 4,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 9,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 4,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954879,
							["custom"] = 0,
							["last_event"] = 1547954879,
							["spec"] = 71,
							["totaldenied"] = 0.005495,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 6,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.003208,
							["resource"] = 0.003208,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.003208,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954879,
							["alternatepower"] = 0.003208,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 6,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[88163] = 1,
								[1464] = 1,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 6,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47388.435,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 9,
				["playing_solo"] = true,
				["totals"] = {
					33.975121, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 2.991578,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 4.005495,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 34.007823,
						}, -- [1]
					},
				},
				["end_time"] = 47390.193,
				["combat_id"] = 6,
				["TotalElapsedCombatTime"] = 1.75800000000163,
				["CombatEndedAt"] = 47390.193,
				["frags"] = {
					["Mottled Boar"] = 2,
				},
				["data_fim"] = "22:28:01",
				["data_inicio"] = "22:27:59",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					34, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47388.435,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [8]
			{
				{
					["combatId"] = 5,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007338,
							["total"] = 31.007338,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 31,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.007338,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954877,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mottled Boar"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 15,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mottled Boar"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 11.2467675009158,
							["custom"] = 0,
							["last_event"] = 1547954875,
							["damage_taken"] = 5.007338,
							["start_time"] = 1547954874,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 5,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorb"] = 0.00352,
							["last_hps"] = 0,
							["healing_from"] = {
							},
							["targets"] = {
							},
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 13,
										},
										["n_max"] = 0,
										["targets"] = {
											["Thorggar"] = 0,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 13,
										["total"] = 0,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 0,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["targets_overheal"] = {
								["Thorggar"] = 13,
							},
							["healing_taken"] = 0.00352,
							["totalover"] = 13.00352,
							["total_without_pet"] = 0.00352,
							["totalover_without_pet"] = 0.00352,
							["heal_enemy_amt"] = 0,
							["end_time"] = 1547954877,
							["total"] = 0.00352,
							["classe"] = "WARRIOR",
							["start_time"] = 1547954875,
							["nome"] = "Thorggar",
							["targets_absorbs"] = {
							},
							["grupo"] = true,
							["spec"] = 71,
							["heal_enemy"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["custom"] = 0,
							["tipo"] = 2,
							["on_hold"] = false,
							["totaldenied"] = 0.00352,
							["delay"] = 0,
							["last_event"] = 1547954875,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 5,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.003909,
							["resource"] = 0.003909,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["total"] = 3.003909,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954874,
							["alternatepower"] = 0.003909,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 5,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "WARRIOR",
							["tipo"] = 4,
							["spell_cast"] = {
								[88163] = 2,
								[1464] = 1,
							},
							["serial"] = "Player-76-0A082E91",
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 5,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47383.748,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 8,
				["playing_solo"] = true,
				["totals"] = {
					30.991285, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 2.998658,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 0.00352,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 31.007338,
						}, -- [1]
					},
				},
				["end_time"] = 47386.505,
				["combat_id"] = 5,
				["TotalElapsedCombatTime"] = 2.75699999999779,
				["CombatEndedAt"] = 47386.505,
				["frags"] = {
					["Mottled Boar"] = 2,
				},
				["data_fim"] = "22:27:57",
				["data_inicio"] = "22:27:54",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					31, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47383.748,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [9]
			{
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00816,
							["total"] = 32.00816,
							["damage_from"] = {
							},
							["targets"] = {
								["Mottled Boar"] = 32,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 32.00816,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954872,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mottled Boar"] = 32,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 32,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 32,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 12.3393060909919,
							["custom"] = 0,
							["last_event"] = 1547954871,
							["damage_taken"] = 0.00816,
							["start_time"] = 1547954869,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 4,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 10.007819,
							["total_without_pet"] = 2.007819,
							["total"] = 2.007819,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.007819,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["totalover_without_pet"] = 0.007819,
							["healing_taken"] = 2.007819,
							["fight_component"] = true,
							["end_time"] = 1547954872,
							["targets_overheal"] = {
								["Thorggar"] = 10,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 10,
										},
										["n_max"] = 2,
										["targets"] = {
											["Thorggar"] = 2,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 10,
										["total"] = 2,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 2,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954871,
							["custom"] = 0,
							["last_event"] = 1547954871,
							["spec"] = 71,
							["totaldenied"] = 0.007819,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 4,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.007421,
							["resource"] = 0.007421,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.007421,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954869,
							["alternatepower"] = 0.007421,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 4,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[1464] = 1,
								[88163] = 2,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47379.033,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 7,
				["playing_solo"] = true,
				["totals"] = {
					31.997751, -- [1]
					2, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 2.007819,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 32.00816,
						}, -- [1]
					},
				},
				["end_time"] = 47381.627,
				["combat_id"] = 4,
				["TotalElapsedCombatTime"] = 2.59399999999732,
				["CombatEndedAt"] = 47381.627,
				["frags"] = {
					["Mottled Boar"] = 1,
				},
				["data_fim"] = "22:27:52",
				["data_inicio"] = "22:27:50",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					32, -- [1]
					2, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47379.033,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [10]
			{
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008573,
							["total"] = 45.008573,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 45,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 45.008573,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954868,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mottled Boar"] = 29,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 29,
										["n_min"] = 14,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 29,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mottled Boar"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 12.2438990750834,
							["custom"] = 0,
							["last_event"] = 1547954868,
							["damage_taken"] = 10.008573,
							["start_time"] = 1547954864,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 3,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 4.008161,
							["total_without_pet"] = 8.008161,
							["total"] = 8.008161,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.008161,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 12,
							},
							["totalover_without_pet"] = 0.008161,
							["healing_taken"] = 8.008161,
							["fight_component"] = true,
							["end_time"] = 1547954868,
							["targets_overheal"] = {
								["Thorggar"] = 4,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 4,
										},
										["n_max"] = 6,
										["targets"] = {
											["Thorggar"] = 8,
										},
										["n_min"] = 2,
										["counter"] = 2,
										["overheal"] = 4,
										["total"] = 8,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 8,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954868,
							["custom"] = 0,
							["last_event"] = 1547954868,
							["spec"] = 71,
							["totaldenied"] = 0.008161,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 3,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 6.002126,
							["resource"] = 0.002126,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 6.002126,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954866,
							["alternatepower"] = 0.002126,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 6,
										["targets"] = {
											["Thorggar"] = 6,
										},
										["counter"] = 2,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 3,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[1464] = 1,
								[88163] = 4,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47373.976,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 6,
				["playing_solo"] = true,
				["totals"] = {
					44.979188, -- [1]
					8, -- [2]
					{
						0, -- [1]
						[0] = 5.993899,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorggar"] = {
						{
							true, -- [1]
							1, -- [2]
							2, -- [3]
							1547954870.458, -- [4]
							297, -- [5]
							"Mottled Boar", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 8.008161,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 45.008573,
						}, -- [1]
					},
				},
				["end_time"] = 47377.982,
				["combat_id"] = 3,
				["TotalElapsedCombatTime"] = 4.00600000000122,
				["CombatEndedAt"] = 47377.982,
				["frags"] = {
					["Mottled Boar"] = 3,
				},
				["data_fim"] = "22:27:48",
				["data_inicio"] = "22:27:44",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					45, -- [1]
					8, -- [2]
					{
						0, -- [1]
						[0] = 6,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47373.976,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [11]
			{
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001906,
							["total"] = 31.001906,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 31,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.001906,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954862,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mottled Boar"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 15,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mottled Boar"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 25.0419273020024,
							["custom"] = 0,
							["last_event"] = 1547954862,
							["damage_taken"] = 4.001906,
							["start_time"] = 1547954861,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 2,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 7.007612,
							["total_without_pet"] = 4.007612,
							["total"] = 4.007612,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.007612,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 5,
							},
							["totalover_without_pet"] = 0.007612,
							["healing_taken"] = 4.007612,
							["fight_component"] = true,
							["end_time"] = 1547954862,
							["targets_overheal"] = {
								["Thorggar"] = 7,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 7,
										},
										["n_max"] = 4,
										["targets"] = {
											["Thorggar"] = 4,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 7,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 4,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954862,
							["custom"] = 0,
							["last_event"] = 1547954862,
							["spec"] = 71,
							["totaldenied"] = 0.007612,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 2,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.001918,
							["resource"] = 0.001918,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.001918,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954861,
							["alternatepower"] = 0.001918,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 2,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[1464] = 1,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47370.388,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 5,
				["playing_solo"] = true,
				["totals"] = {
					30.982718, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 2.991137,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 4.007612,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 31.001906,
						}, -- [1]
					},
				},
				["end_time"] = 47371.626,
				["combat_id"] = 2,
				["TotalElapsedCombatTime"] = 1.23800000000483,
				["CombatEndedAt"] = 47371.626,
				["frags"] = {
					["Mottled Boar"] = 1,
				},
				["data_fim"] = "22:27:42",
				["data_inicio"] = "22:27:41",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					31, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47370.388,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [12]
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006034,
							["total"] = 31.006034,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 31,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.006034,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1547954859,
							["on_hold"] = false,
							["spec"] = 71,
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 15,
										["targets"] = {
											["Mottled Boar"] = 15,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 15,
										["n_min"] = 15,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 15,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mottled Boar"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A082E91",
							["last_dps"] = 13.3416669535254,
							["custom"] = 0,
							["last_event"] = 1547954858,
							["damage_taken"] = 3.006034,
							["start_time"] = 1547954857,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorggar"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 9.00233,
							["total_without_pet"] = 3.00233,
							["total"] = 3.00233,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-0A082E91",
							["totalabsorb"] = 0.00233,
							["last_hps"] = 0,
							["targets"] = {
								["Thorggar"] = 6,
							},
							["totalover_without_pet"] = 0.00233,
							["healing_taken"] = 3.00233,
							["fight_component"] = true,
							["end_time"] = 1547954859,
							["targets_overheal"] = {
								["Thorggar"] = 9,
							},
							["nome"] = "Thorggar",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorggar"] = 9,
										},
										["n_max"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 9,
										["total"] = 3,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 3,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1547954858,
							["custom"] = 0,
							["last_event"] = 1547954858,
							["spec"] = 71,
							["totaldenied"] = 0.00233,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.006851,
							["resource"] = 0.006851,
							["targets"] = {
								["Thorggar"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.006851,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1547954857,
							["alternatepower"] = 0.006851,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorggar"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorggar",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[1464] = 1,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-0A082E91",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorggar"] = true,
				},
				["CombatStartedAt"] = 47366.36,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					30.980109, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 2.993184,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorggar"] = 3.00233,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorggar"] = 31.006034,
						}, -- [1]
					},
				},
				["end_time"] = 47368.684,
				["combat_id"] = 1,
				["TotalElapsedCombatTime"] = 2.32400000000052,
				["CombatEndedAt"] = 47368.684,
				["frags"] = {
					["Mottled Boar"] = 2,
				},
				["data_fim"] = "22:27:39",
				["data_inicio"] = "22:27:37",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					31, -- [1]
					3, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 47366.36,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [13]
		},
	},
	["last_version"] = "v8.1.0.6702",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 95.1754150390625,
					["x"] = 543.460388183594,
					["w"] = 310.000030517578,
					["h"] = 158,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-0A082E91"] = {
		},
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorggar-Sargeras",
	["last_realversion"] = 135,
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "19",
	["nick_tag_cache"] = {
		["nextreset"] = 1549250726,
		["last_version"] = 11,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["combat_id"] = 13,
	["savedStyles"] = {
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["combat_counter"] = 16,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.089717,
					["damage_from"] = {
						["Northwatch Scout"] = true,
						["Mottled Boar"] = true,
					},
					["targets"] = {
						["Northwatch Scout"] = 317,
						["Mottled Boar"] = 204,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 521.089717,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["dps_started"] = false,
					["total"] = 521.089717,
					["classe"] = "WARRIOR",
					["end_time"] = 1547954859,
					["nome"] = "Thorggar",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 63,
								["g_amt"] = 0,
								["n_max"] = 17,
								["targets"] = {
									["Northwatch Scout"] = 162,
									["Mottled Boar"] = 91,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 190,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 14,
								["total"] = 253,
								["c_max"] = 32,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 12,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[1464] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 34,
								["g_amt"] = 0,
								["n_max"] = 18,
								["targets"] = {
									["Northwatch Scout"] = 155,
									["Mottled Boar"] = 113,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 234,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 15,
								["total"] = 268,
								["c_max"] = 34,
								["id"] = 1464,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 14,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["on_hold"] = false,
					["spec"] = 71,
					["serial"] = "Player-76-0A082E91",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1547954829,
					["delay"] = 0,
					["damage_taken"] = 49.089717,
				}, -- [1]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["targets_overheal"] = {
						["Thorggar"] = 116,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["heal_enemy_amt"] = 0,
					["totalover"] = 116.065683,
					["total_without_pet"] = 46.065683,
					["total"] = 46.065683,
					["targets_absorbs"] = {
					},
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-0A082E91",
					["totalabsorb"] = 0.065683,
					["last_hps"] = 0,
					["targets"] = {
						["Thorggar"] = 80,
					},
					["totalover_without_pet"] = 0.065683,
					["healing_taken"] = 46.065683,
					["fight_component"] = true,
					["end_time"] = 1547954859,
					["healing_from"] = {
						["Thorggar"] = true,
					},
					["nome"] = "Thorggar",
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[59913] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thorggar"] = 116,
								},
								["n_max"] = 6,
								["targets"] = {
									["Thorggar"] = 46,
								},
								["n_min"] = 0,
								["counter"] = 26,
								["overheal"] = 116,
								["total"] = 46,
								["c_max"] = 0,
								["id"] = 59913,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 46,
								["n_amt"] = 26,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["grupo"] = true,
					["start_time"] = 1547954844,
					["classe"] = "WARRIOR",
					["custom"] = 0,
					["tipo"] = 2,
					["spec"] = 71,
					["totaldenied"] = 0.065683,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 39.060701,
					["resource"] = 0.060701,
					["targets"] = {
						["Thorggar"] = 39,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "WARRIOR",
					["fight_component"] = true,
					["total"] = 39.060701,
					["nome"] = "Thorggar",
					["spec"] = 71,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[195707] = {
								["id"] = 195707,
								["total"] = 39,
								["targets"] = {
									["Thorggar"] = 39,
								},
								["counter"] = 13,
							},
						},
					},
					["tipo"] = 3,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.060701,
					["serial"] = "Player-76-0A082E91",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["pets"] = {
					},
					["nome"] = "Thorggar",
					["spec"] = 71,
					["grupo"] = true,
					["spell_cast"] = {
						[1464] = 12,
						[88163] = 13,
					},
					["fight_component"] = true,
					["tipo"] = 4,
					["classe"] = "WARRIOR",
					["serial"] = "Player-76-0A082E91",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			730.183267, -- [1]
			46.064382, -- [2]
			{
				0, -- [1]
				[0] = 51.078806,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["end_time"] = 47511.627,
		["data_inicio"] = "22:27:37",
		["frags"] = {
		},
		["data_fim"] = "22:30:02",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 1.25299999999697,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:30:01",
			}, -- [1]
			{
				["elapsed"] = 2.375,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:29:53",
			}, -- [2]
			{
				["elapsed"] = 1.45799999999872,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:29:37",
			}, -- [3]
			{
				["elapsed"] = 1.73600000000442,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:29:27",
			}, -- [4]
			{
				["elapsed"] = 1.26799999999639,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:29:22",
			}, -- [5]
			{
				["elapsed"] = 3.74599999999919,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:29:14",
			}, -- [6]
			{
				["elapsed"] = 0.97099999999773,
				["type"] = 0,
				["name"] = "Northwatch Scout",
				["clock"] = "22:29:06",
			}, -- [7]
			{
				["elapsed"] = 1.75800000000163,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "22:27:59",
			}, -- [8]
			{
				["elapsed"] = 2.75699999999779,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "22:27:54",
			}, -- [9]
			{
				["elapsed"] = 2.59399999999732,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "22:27:50",
			}, -- [10]
			{
				["elapsed"] = 4.00600000000122,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "22:27:44",
			}, -- [11]
			{
				["elapsed"] = 1.23800000000483,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "22:27:41",
			}, -- [12]
			{
				["elapsed"] = 2.32400000000052,
				["type"] = 0,
				["name"] = "Mottled Boar",
				["clock"] = "22:27:37",
			}, -- [13]
		},
		["start_time"] = 47484.143,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["totals_grupo"] = {
			521.085124, -- [1]
			46.064382, -- [2]
			{
				0, -- [1]
				[0] = 39.056125,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["force_font_outline"] = "",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A082E91"] = 71,
		["Player-57-0B425F3F"] = 63,
		["Player-77-0BEB1B85"] = 71,
		["Player-77-0BEB1B8E"] = 253,
	},
}
