
PawnOptions = {
	["LastVersion"] = 2.0404,
	["LastPlayerFullName"] = "Thorwin-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 139070,
			["Level"] = 47,
			["Link"] = "|cff0070dd|Hitem:139070::::::::42:105::17:1:1795:1:9:41:::|h[Tranquil Bough Hood]|h|r",
		}, -- [1]
		{
			["ID"] = 137311,
			["Level"] = 47,
			["Link"] = "|cff0070dd|Hitem:137311::::::::42:105::17:1:1795:1:9:41:::|h[Chain of the Green Flight]|h|r",
		}, -- [2]
		{
			["ID"] = 137364,
			["Level"] = 47,
			["Link"] = "|cff0070dd|Hitem:137364::::::::42:105::17:1:1795:1:9:41:::|h[Crashing Oceantide Mantle]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 139071,
			["Level"] = 47,
			["Link"] = "|cff0070dd|Hitem:139071::::::::42:105::17:1:1795:1:9:41:::|h[Tranquil Bough Vest]|h|r",
		}, -- [5]
		{
			["ID"] = 136776,
			["Level"] = 48,
			["Link"] = "|cff0070dd|Hitem:136776::::::::42:105::17:1:1795:1:9:42:::|h[Bjorn's Hunting Strap]|h|r",
		}, -- [6]
		{
			["ID"] = 115636,
			["Level"] = 44,
			["Link"] = "|cffa335ee|Hitem:115636::::::::42:105::14:::::|h[Primal Gladiator's Dragonhide Legguards]|h|r",
		}, -- [7]
		{
			["ID"] = 124572,
			["Level"] = 44,
			["Link"] = "|cff0070dd|Hitem:124572::::::::42:105:::3:157:647:653::::|h[Bladefang Boots of the Aurora]|h|r",
		}, -- [8]
		{
			["ID"] = 134461,
			["Level"] = 48,
			["Link"] = "|cff0070dd|Hitem:134461::::::::42:105::17:1:1795:1:9:42:::|h[Thermal Bindings]|h|r",
		}, -- [9]
		{
			["ID"] = 115174,
			["Level"] = 44,
			["Link"] = "|cff0070dd|Hitem:115174::::::::42:105::14:::::|h[Primal Combatant's Gloves]|h|r",
		}, -- [10]
		{
			["ID"] = 134539,
			["Level"] = 47,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:134539::::::::42:105::17:1:1795:1:9:41:::|h[Braided Silver Ring]|h|r",
		}, -- [11]
		{
			["ID"] = 121027,
			["Level"] = 45,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff1eff00|Hitem:121027::::::::42:105:::2:1812:1754:1:9:41:::|h[Azurewing Signet of the Peerless]|h|r",
		}, -- [12]
		{
			["ID"] = 133642,
			["Level"] = 48,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:133642::::::::42:105::17:1:1795:1:9:42:::|h[Horn of Valor]|h|r",
		}, -- [13]
		{
			["ID"] = 115154,
			["Level"] = 44,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff0070dd|Hitem:115154::::::::42:105::14:::::|h[Primal Combatant's Badge of Dominance]|h|r",
		}, -- [14]
		{
			["ID"] = 127973,
			["Level"] = 45,
			["Link"] = "|cffa335ee|Hitem:127973::::::::42:105:::::::|h[Nether-Touched Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 128306,
			["Level"] = 35,
			["Link"] = "|cffe6cc80|Hitem:128306::136973::::::42:105::9:1:1487::3:1795:1467:1809::|h[G'Hanir, the Mother Tree]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
	["Artifacts"] = {
		[128306] = {
			["Relics"] = {
				{
					["ItemLevel"] = 3,
					["Type"] = "Life",
				}, -- [1]
				{
					["Type"] = "Frost",
				}, -- [2]
			},
			["Name"] = "G'Hanir, the Mother Tree",
		},
	},
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DRUID",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
