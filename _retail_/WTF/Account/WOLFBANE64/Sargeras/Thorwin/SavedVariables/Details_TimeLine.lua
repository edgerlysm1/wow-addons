
DetailsTimeLineDB = {
	["max_segments"] = 4,
	["combat_data"] = {
		{
			["date_end"] = "09:38:55",
			["date_start"] = "09:37:38",
			["name"] = "Odyn",
			["total_time"] = 77.0030000000002,
		}, -- [1]
		{
			["date_end"] = "09:36:44",
			["date_start"] = "09:35:36",
			["name"] = "God-King Skovald",
			["total_time"] = 68.0079999999998,
		}, -- [2]
		{
			["date_end"] = "09:29:24",
			["date_start"] = "09:27:57",
			["name"] = "Hyrja",
			["total_time"] = 87.002,
		}, -- [3]
		{
			["date_end"] = "09:24:19",
			["date_start"] = "09:23:24",
			["name"] = "Fenryr",
			["total_time"] = 55.002,
		}, -- [4]
	},
	["hide_on_combat"] = false,
	["IndividualSpells"] = {
		{
			[197961] = {
				{
					45.4190000000003, -- [1]
					"Odyn", -- [2]
					197961, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[198077] = {
				{
					42.4120000000003, -- [1]
					"Odyn", -- [2]
					198077, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[198263] = {
				{
					15.4350000000004, -- [1]
					"Odyn", -- [2]
					198263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [1]
		{
			[194112] = {
				{
					31.3440000000001, -- [1]
					"God-King Skovald", -- [2]
					194112, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[193826] = {
				{
					14.7570000000001, -- [1]
					"God-King Skovald", -- [2]
					193826, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[193659] = {
				{
					7.91800000000012, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Clevahgurl-GrizzlyHills", -- [5]
				}, -- [1]
				{
					24.8809999999999, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [2]
				{
					51.6210000000001, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bajh-Durotan", -- [5]
				}, -- [3]
				{
					64.9690000000001, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bajh-Durotan", -- [5]
				}, -- [4]
			},
			[193660] = {
				{
					8.19799999999987, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					24.991, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					51.721, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					65.0989999999997, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[193668] = {
				{
					46.4360000000002, -- [1]
					"God-King Skovald", -- [2]
					193668, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [1]
			},
		}, -- [2]
		{
			[198944] = {
				{
					1.53699999999981, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [1]
				{
					3.96900000000005, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [2]
				{
					17.3240000000001, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [3]
				{
					22.1750000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [4]
				{
					33.0770000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [5]
			},
			[199050] = {
				{
					0.121999999999844, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					13.4639999999999, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					18.3110000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					29.2350000000001, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					34.0880000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[200901] = {
				{
					60.4459999999999, -- [1]
					"Solsten", -- [2]
					200901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[192158] = {
				{
					13.4639999999999, -- [1]
					"Olmyr the Enlightened", -- [2]
					192158, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					40.1840000000002, -- [1]
					"Olmyr the Enlightened", -- [2]
					192158, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[192288] = {
				{
					36.797, -- [1]
					"Olmyr the Enlightened", -- [2]
					192288, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bajh-Durotan", -- [5]
				}, -- [1]
			},
		}, -- [3]
		{
			[196543] = {
				{
					4.31999999999994, -- [1]
					"Fenryr", -- [2]
					196543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					43.25, -- [1]
					"Fenryr", -- [2]
					196543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[196512] = {
				{
					39.309, -- [1]
					"Fenryr", -- [2]
					196512, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[197558] = {
				{
					13.1369999999999, -- [1]
					"Fenryr", -- [2]
					197558, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					49.5940000000001, -- [1]
					"Fenryr", -- [2]
					197558, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[196838] = {
				{
					23.2529999999999, -- [1]
					"Fenryr", -- [2]
					196838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Clevahgurl-GrizzlyHills", -- [5]
				}, -- [1]
			},
		}, -- [4]
		{
			[197558] = {
				{
					18.1120000000001, -- [1]
					"Fenryr", -- [2]
					197558, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[200561] = {
				{
					46.0029999999999, -- [1]
					"Unknown", -- [2]
					200561, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[196567] = {
				{
					38.711, -- [1]
					"Fenryr", -- [2]
					196567, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[196512] = {
				{
					12.693, -- [1]
					"Fenryr", -- [2]
					196512, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					26.0629999999999, -- [1]
					"Fenryr", -- [2]
					196512, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					35.778, -- [1]
					"Fenryr", -- [2]
					196512, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[196543] = {
				{
					9.33699999999999, -- [1]
					"Fenryr", -- [2]
					196543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [5]
		{
			[191284] = {
				{
					11.124, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					54.869, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[193092] = {
				{
					15.89, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.543, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					53.559, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					74.205, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[193235] = {
				{
					6.70500000000004, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Clevahgurl-GrizzlyHills", -- [5]
				}, -- [1]
				{
					38.311, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [2]
				{
					50.471, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [3]
				{
					82.029, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pickett-GrizzlyHills", -- [5]
				}, -- [4]
			},
			[188404] = {
				{
					18.3390000000001, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					61.6710000000001, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					66.954, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					72.206, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [6]
		{
			[191284] = {
				{
					12.1470000000001, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					54.638, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[193092] = {
				{
					16.904, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					37.554, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					59.3820000000001, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[193235] = {
				{
					7.755, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pickett-GrizzlyHills", -- [5]
				}, -- [1]
				{
					39.311, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pickett-GrizzlyHills", -- [5]
				}, -- [2]
				{
					50.23, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [3]
			},
			[188404] = {
				{
					18.942, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					61.433, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					67.095, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					71.9540000000001, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [7]
		{
			[191284] = {
				{
					11.057, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					53.178, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					97.295, -- [1]
					"Hymdall", -- [2]
					191284, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[193092] = {
				{
					16.639, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.8679999999999, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					51.856, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					68.893, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					89.52, -- [1]
					"Hymdall", -- [2]
					193092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[193235] = {
				{
					6.64300000000003, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pickett-GrizzlyHills", -- [5]
				}, -- [1]
				{
					37.861, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pickett-GrizzlyHills", -- [5]
				}, -- [2]
				{
					47.975, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [3]
				{
					80.381, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Luzuna-Hellscream", -- [5]
				}, -- [4]
				{
					91.675, -- [1]
					"Hymdall", -- [2]
					193235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Luzuna-Hellscream", -- [5]
				}, -- [5]
			},
			[188404] = {
				{
					18.281, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					60.382, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					65.641, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					70.116, -- [1]
					"Storm Drake", -- [2]
					188404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [8]
		{
			[200154] = {
				{
					13.7069999999985, -- [1]
					"Molten Charskin", -- [2]
					200154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [1]
				{
					19.107, -- [1]
					"Molten Charskin", -- [2]
					200154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [2]
				{
					85.3669999999984, -- [1]
					"Molten Charskin", -- [2]
					200154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [3]
			},
			[226406] = {
				{
					1.50300000000061, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					5.52999999999884, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					9.9989999999998, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					12.4279999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					19.7360000000008, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					23.3629999999976, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					30.6339999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					33.0669999999991, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					40.357, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					43.9879999999976, -- [1]
					"Emberhusk Dominator", -- [2]
					226406, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
			},
			[200700] = {
				{
					16.6630000000005, -- [1]
					"Dargrul", -- [2]
					200700, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [1]
				{
					33.6450000000004, -- [1]
					"Dargrul", -- [2]
					200700, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [2]
				{
					53.0689999999995, -- [1]
					"Dargrul", -- [2]
					200700, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [3]
				{
					74.9339999999975, -- [1]
					"Dargrul", -- [2]
					200700, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [4]
			},
			[200732] = {
				{
					20.6640000000007, -- [1]
					"Dargrul", -- [2]
					200732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [1]
				{
					37.6529999999984, -- [1]
					"Dargrul", -- [2]
					200732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [2]
				{
					57.0760000000009, -- [1]
					"Dargrul", -- [2]
					200732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [3]
				{
					78.9389999999985, -- [1]
					"Dargrul", -- [2]
					200732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [4]
			},
			[200404] = {
				{
					67.4409999999989, -- [1]
					"Dargrul", -- [2]
					200404, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[201983] = {
				{
					0.1, -- [1]
					"Emberhusk Dominator", -- [2]
					201983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					8.11100000000079, -- [1]
					"Emberhusk Dominator", -- [2]
					201983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					10.5609999999979, -- [1]
					"Emberhusk Dominator", -- [2]
					201983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					28.7769999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					31.1899999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					51.8070000000007, -- [1]
					"Emberhusk Dominator", -- [2]
					201983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[193585] = {
				{
					5.66999999999825, -- [1]
					"Rockbound Trapper", -- [2]
					193585, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [1]
				{
					29.9660000000004, -- [1]
					"Rockbound Trapper", -- [2]
					193585, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [2]
				{
					51.8070000000007, -- [1]
					"Rockbound Trapper", -- [2]
					193585, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [3]
			},
			[200637] = {
				{
					10.851999999999, -- [1]
					"Dargrul", -- [2]
					200637, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					82.5049999999974, -- [1]
					"Dargrul", -- [2]
					200637, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[201959] = {
				{
					1.62900000000082, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					1.62900000000082, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					3.2589999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					4.07400000000052, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					4.07400000000052, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					5.68000000000029, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					6.48300000000018, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					8.11100000000079, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					10.530999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					12.5720000000001, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					13.7890000000007, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					15.0040000000008, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					16.2219999999979, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					17.4399999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					18.7289999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					19.8709999999992, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					21.0920000000006, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					22.3050000000003, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					23.5239999999976, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					24.7199999999975, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					25.9359999999979, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					28.357, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					33.1990000000005, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					34.4120000000003, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					35.2659999999996, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					36.857, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					38.0760000000009, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					39.2769999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					40.4969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					41.7089999999989, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					42.9199999999983, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					44.1339999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					45.3539999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					46.5709999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					48.9850000000006, -- [1]
					"Emberhusk Dominator", -- [2]
					201959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
			},
			[200551] = {
				{
					7.09599999999773, -- [1]
					"Dargrul", -- [2]
					200551, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.9779999999992, -- [1]
					"Dargrul", -- [2]
					200551, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					50.8070000000007, -- [1]
					"Dargrul", -- [2]
					200551, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					72.6820000000007, -- [1]
					"Dargrul", -- [2]
					200551, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[183633] = {
				{
					0.618999999998778, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [1]
				{
					0.618999999998778, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [2]
				{
					1.85199999999895, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [3]
				{
					1.85199999999895, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [4]
				{
					3.06899999999951, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [5]
				{
					3.06899999999951, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [6]
				{
					4.27399999999761, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [7]
				{
					5.47999999999956, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [8]
				{
					6.70599999999831, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [9]
				{
					7.91100000000006, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [10]
				{
					9.12800000000061, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [11]
				{
					9.12800000000061, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [12]
				{
					10.3509999999987, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [13]
				{
					10.3509999999987, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [14]
				{
					11.5730000000003, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [15]
				{
					11.5730000000003, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [16]
				{
					12.7849999999999, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [17]
				{
					12.7849999999999, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [18]
				{
					14.0190000000002, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [19]
				{
					14.0190000000002, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [20]
				{
					15.226999999999, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [21]
				{
					15.226999999999, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [22]
				{
					16.4559999999983, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [23]
				{
					16.4559999999983, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [24]
				{
					17.6739999999991, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [25]
				{
					17.6739999999991, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [26]
				{
					20.0920000000006, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [27]
				{
					20.0920000000006, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [28]
				{
					21.2959999999985, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [29]
				{
					21.2959999999985, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [30]
				{
					22.5020000000004, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [31]
				{
					22.5119999999988, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [32]
				{
					23.7150000000001, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [33]
				{
					23.7150000000001, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [34]
				{
					24.9409999999989, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [35]
				{
					24.9409999999989, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [36]
				{
					26.1499999999978, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [37]
				{
					27.357, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [38]
				{
					30.987000000001, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [39]
				{
					32.1990000000005, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [40]
				{
					33.4069999999992, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [41]
				{
					33.4169999999976, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [42]
				{
					35.8590000000004, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [43]
				{
					37.0669999999991, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [44]
				{
					38.2769999999982, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [45]
				{
					41.9110000000001, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [46]
				{
					43.1399999999994, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [47]
				{
					44.344000000001, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [48]
				{
					45.5619999999981, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [49]
				{
					46.7729999999974, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [50]
				{
					47.9830000000002, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [51]
				{
					49.1899999999987, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [52]
				{
					52.839, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [53]
				{
					55.2700000000004, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [54]
				{
					56.4959999999992, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [55]
				{
					57.6990000000005, -- [1]
					"Rockbound Trapper", -- [2]
					183633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [56]
			},
			[201953] = {
				{
					1.62900000000082, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [1]
				{
					1.62900000000082, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [2]
				{
					1.62900000000082, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [3]
				{
					1.63899999999921, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [4]
				{
					1.63899999999921, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [5]
				{
					1.64999999999782, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [6]
				{
					3.2589999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [7]
				{
					3.2589999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [8]
				{
					3.2589999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [9]
				{
					4.07400000000052, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [10]
				{
					4.07400000000052, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [11]
				{
					4.07400000000052, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [12]
				{
					4.08399999999892, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [13]
				{
					4.08399999999892, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [14]
				{
					4.08399999999892, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [15]
				{
					5.68000000000029, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [16]
				{
					5.68000000000029, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [17]
				{
					5.68000000000029, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [18]
				{
					6.48300000000018, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [19]
				{
					6.48300000000018, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [20]
				{
					6.48300000000018, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [21]
				{
					8.12099999999919, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [22]
				{
					8.12099999999919, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [23]
				{
					8.12099999999919, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [24]
				{
					10.5509999999995, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [25]
				{
					10.5509999999995, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [26]
				{
					10.5509999999995, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [27]
				{
					12.5819999999985, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [28]
				{
					12.5819999999985, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [29]
				{
					12.5819999999985, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [30]
				{
					13.7890000000007, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [31]
				{
					13.7890000000007, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [32]
				{
					13.7890000000007, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [33]
				{
					15.0040000000008, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [34]
				{
					15.0040000000008, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [35]
				{
					15.0040000000008, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [36]
				{
					16.2219999999979, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [37]
				{
					16.2219999999979, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [38]
				{
					16.2219999999979, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [39]
				{
					17.4399999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [40]
				{
					17.4399999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adilasa-Stormrage", -- [5]
				}, -- [41]
				{
					17.4399999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [42]
				{
					18.7289999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [43]
				{
					18.7289999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [44]
				{
					18.7289999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [45]
				{
					19.8709999999992, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [46]
				{
					19.8709999999992, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [47]
				{
					19.8709999999992, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [48]
				{
					21.101999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [49]
				{
					21.101999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [50]
				{
					21.101999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [51]
				{
					22.3050000000003, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [52]
				{
					22.3269999999975, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [53]
				{
					22.3269999999975, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [54]
				{
					23.5239999999976, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [55]
				{
					23.5239999999976, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [56]
				{
					23.5239999999976, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [57]
				{
					24.7199999999975, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [58]
				{
					24.7199999999975, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [59]
				{
					24.7199999999975, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [60]
				{
					25.9459999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [61]
				{
					25.9459999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [62]
				{
					25.9459999999999, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [63]
				{
					28.357, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [64]
				{
					28.357, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [65]
				{
					28.357, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [66]
				{
					33.1990000000005, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [67]
				{
					33.1990000000005, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [68]
				{
					33.1990000000005, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [69]
				{
					34.4239999999991, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [70]
				{
					34.4239999999991, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [71]
				{
					34.4239999999991, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [72]
				{
					35.2659999999996, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [73]
				{
					35.2659999999996, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [74]
				{
					35.2659999999996, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [75]
				{
					36.8669999999984, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [76]
				{
					36.8669999999984, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [77]
				{
					36.8669999999984, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [78]
				{
					38.0859999999993, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [79]
				{
					38.0859999999993, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [80]
				{
					38.0859999999993, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [81]
				{
					39.2969999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [82]
				{
					39.2969999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [83]
				{
					39.2969999999987, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [84]
				{
					40.4969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [85]
				{
					40.4969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [86]
				{
					40.4969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [87]
				{
					41.7089999999989, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [88]
				{
					41.7089999999989, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [89]
				{
					41.7089999999989, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [90]
				{
					42.9199999999983, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [91]
				{
					42.9199999999983, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [92]
				{
					42.9199999999983, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [93]
				{
					44.1339999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [94]
				{
					44.1339999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [95]
				{
					44.1339999999982, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [96]
				{
					45.3539999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [97]
				{
					45.3539999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [98]
				{
					45.3539999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [99]
				{
					46.5819999999985, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [100]
				{
					46.5819999999985, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [101]
				{
					46.5819999999985, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [102]
				{
					48.9969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dyorrva-Lothar", -- [5]
				}, -- [103]
				{
					48.9969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [104]
				{
					48.9969999999994, -- [1]
					"Emberhusk Dominator", -- [2]
					201953, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darknastyy-Garithos", -- [5]
				}, -- [105]
			},
		}, -- [9]
		{
			[205549] = {
				{
					7.65700000000288, -- [1]
					"Naraxas", -- [2]
					205549, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.4750000000022, -- [1]
					"Naraxas", -- [2]
					205549, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[210150] = {
				{
					13.2309999999998, -- [1]
					"Naraxas", -- [2]
					210150, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.5480000000025, -- [1]
					"Naraxas", -- [2]
					210150, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					41.9580000000024, -- [1]
					"Naraxas", -- [2]
					210150, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[199775] = {
				{
					35.104000000003, -- [1]
					"Naraxas", -- [2]
					199775, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[199246] = {
				{
					25.4750000000022, -- [1]
					"Naraxas", -- [2]
					199246, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [10]
		{
			[193267] = {
				{
					18.8830000000016, -- [1]
					"Unknown", -- [2]
					193267, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					20.9470000000001, -- [1]
					"Bellowing Idol", -- [2]
					193267, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[198428] = {
				{
					13.5490000000027, -- [1]
					"Ularogg Cragshaper", -- [2]
					198428, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.9470000000001, -- [1]
					"Ularogg Cragshaper", -- [2]
					198428, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[193375] = {
				{
					18.8170000000027, -- [1]
					"Ularogg Cragshaper", -- [2]
					193375, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[193376] = {
				{
					20.9170000000013, -- [1]
					"Ularogg Cragshaper", -- [2]
					193376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[198496] = {
				{
					4.33899999999994, -- [1]
					"Ularogg Cragshaper", -- [2]
					198496, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [1]
				{
					23.3770000000004, -- [1]
					"Ularogg Cragshaper", -- [2]
					198496, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [2]
				{
					60.2400000000016, -- [1]
					"Ularogg Cragshaper", -- [2]
					198496, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [3]
			},
		}, -- [11]
		{
			[192799] = {
				{
					9.50600000000122, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					11.3340000000026, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					18.6620000000003, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					23.6730000000025, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					24.1190000000024, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					34.1409999999996, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					39.3400000000002, -- [1]
					"Blightshard Skitter", -- [2]
					192799, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[188169] = {
				{
					28.4710000000014, -- [1]
					"Rokmora", -- [2]
					188169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Toroath-Garithos", -- [5]
				}, -- [1]
			},
			[188114] = {
				{
					24.1080000000002, -- [1]
					"Rokmora", -- [2]
					188114, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [12]
		{
			[154396] = {
				{
					12.7109999999993, -- [1]
					"High Sage Viryx", -- [2]
					154396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bernadino-Gallywix", -- [5]
				}, -- [1]
			},
		}, -- [13]
		{
			[169810] = {
				{
					22.7079999999987, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[153810] = {
				{
					14.3850000000002, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.8549999999996, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153794] = {
				{
					10.3220000000001, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bernadino-Gallywix", -- [5]
				}, -- [1]
				{
					21.2619999999988, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bernadino-Gallywix", -- [5]
				}, -- [2]
				{
					32.2150000000002, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bernadino-Gallywix", -- [5]
				}, -- [3]
			},
		}, -- [14]
		{
			[154135] = {
				{
					23.0589999999993, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[154113] = {
				{
					15.9759999999988, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[154110] = {
				{
					7.49799999999959, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.5460000000003, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [15]
		{
			[153757] = {
				{
					9.70800000000054, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.514000000001, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[165731] = {
				{
					4.85800000000017, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Leeroygankin-Lightbringer", -- [5]
				}, -- [1]
				{
					19.4459999999999, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bernadino-Gallywix", -- [5]
				}, -- [2]
			},
			[156793] = {
				{
					35.5040000000008, -- [1]
					"Ranjit", -- [2]
					156793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[153315] = {
				{
					14.348, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.9529999999995, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[165733] = {
				{
					4.83799999999974, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					19.4300000000003, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [16]
		{
			[150755] = {
				{
					24.2489999999998, -- [1]
					"Gug'rokk", -- [2]
					150755, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.1149999999998, -- [1]
					"Gug'rokk", -- [2]
					150755, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[150776] = {
				{
					12.406, -- [1]
					"Gug'rokk", -- [2]
					150776, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					33.058, -- [1]
					"Gug'rokk", -- [2]
					150776, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					53.6800000000003, -- [1]
					"Gug'rokk", -- [2]
					150776, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150677] = {
				{
					8.26500000000033, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [1]
				{
					21.6210000000001, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [2]
				{
					36.1859999999997, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [3]
				{
					49.549, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [4]
			},
		}, -- [17]
		{
			[153247] = {
				{
					11.4830000000002, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					15.1269999999995, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					18.7609999999995, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					51.5590000000002, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[152940] = {
				{
					27.0019999999995, -- [1]
					"Roltall", -- [2]
					152940, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[152939] = {
				{
					35.7179999999999, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.9359999999997, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.1880000000001, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					39.3639999999996, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					40.5810000000001, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					41.7969999999996, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [18]
		{
			[150038] = {
				{
					57.5789999999997, -- [1]
					"Magmolatus", -- [2]
					150038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					79.4539999999997, -- [1]
					"Magmolatus", -- [2]
					150038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					101.283, -- [1]
					"Magmolatus", -- [2]
					150038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150290] = {
				{
					36.9080000000004, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Murilosalla-Nemesis", -- [5]
				}, -- [1]
				{
					41.7790000000005, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xiitara-Goldrinn", -- [5]
				}, -- [2]
			},
			[150023] = {
				{
					50.0810000000001, -- [1]
					"Magmolatus", -- [2]
					150023, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					68.3100000000004, -- [1]
					"Magmolatus", -- [2]
					150023, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					87.741, -- [1]
					"Magmolatus", -- [2]
					150023, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[152073] = {
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					152073, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Murilosalla-Nemesis", -- [5]
				}, -- [1]
			},
			[152170] = {
				{
					57.5789999999997, -- [1]
					"Magmolatus", -- [2]
					152170, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					79.4539999999997, -- [1]
					"Magmolatus", -- [2]
					152170, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					101.313, -- [1]
					"Magmolatus", -- [2]
					152170, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150004] = {
				{
					2.61800000000039, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					8.66899999999987, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					14.7390000000005, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					20.8220000000001, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					26.8800000000001, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					65.8069999999998, -- [1]
					"Molten Elemental", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					87.6610000000001, -- [1]
					"Molten Elemental", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[151965] = {
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					151965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					151965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[150078] = {
				{
					15.951, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150078, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[149997] = {
				{
					29.3249999999998, -- [1]
					"Calamity", -- [2]
					149997, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.3609999999999, -- [1]
					"Calamity", -- [2]
					149997, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[150032] = {
				{
					46.8770000000004, -- [1]
					"Magmolatus", -- [2]
					150032, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					72.3860000000004, -- [1]
					"Magmolatus", -- [2]
					150032, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					99.0810000000001, -- [1]
					"Magmolatus", -- [2]
					150032, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150048] = {
				{
					95.723, -- [1]
					"Molten Elemental", -- [2]
					150048, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[77508] = {
				{
					3.41899999999987, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [1]
				{
					16.777, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [2]
				{
					30.1310000000003, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [3]
				{
					44.7380000000003, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [4]
				{
					58.1109999999999, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [5]
				{
					72.6970000000001, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [6]
				{
					86.0560000000005, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [7]
				{
					103.03, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [8]
				{
					108.004, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [9]
				{
					108.004, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [10]
				{
					108.004, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [11]
			},
			[149975] = {
				{
					27.402, -- [1]
					"Calamity", -- [2]
					149975, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150076] = {
				{
					9.88000000000011, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150076, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[151697] = {
				{
					108.004, -- [1]
					"Bloodmaul Overseer", -- [2]
					151697, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[152043] = {
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					152043, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xiitara-Goldrinn", -- [5]
				}, -- [1]
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					152043, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Iniuria-Nazgrel", -- [5]
				}, -- [2]
			},
			[151990] = {
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					151990, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					108.004, -- [1]
					"Bloodmaul Slaver", -- [2]
					151990, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[149941] = {
				{
					19.1840000000002, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.6729999999998, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					34.982, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					42.2820000000002, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					49.576, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[150324] = {
				{
					23.5360000000001, -- [1]
					"Ruination", -- [2]
					150324, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.125, -- [1]
					"Ruination", -- [2]
					150324, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [19]
		{
			[150751] = {
				{
					25.8040000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					150751, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					48.8890000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					150751, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					71.9589999999998, -- [1]
					"Slave Watcher Crushto", -- [2]
					150751, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[151092] = {
				{
					63.5600000000004, -- [1]
					"Captured Miner", -- [2]
					151092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Raptor", -- [5]
				}, -- [1]
				{
					63.5600000000004, -- [1]
					"Captured Miner", -- [2]
					151092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Raptor", -- [5]
				}, -- [2]
			},
			[153679] = {
				{
					5.21399999999994, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [1]
				{
					18.5770000000002, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [2]
				{
					36.8130000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [3]
				{
					59.9070000000002, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [4]
				{
					82.9189999999999, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [5]
			},
			[150807] = {
				{
					29.5650000000005, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [1]
				{
					29.5650000000005, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [2]
				{
					30.7449999999999, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [3]
				{
					36.8450000000003, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [4]
				{
					36.8450000000003, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [5]
				{
					42.915, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [6]
				{
					42.915, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [7]
				{
					43.9920000000002, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [8]
				{
					51.3969999999999, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [9]
				{
					55.0370000000003, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Murilosalla-Nemesis", -- [5]
				}, -- [10]
				{
					62.339, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [11]
				{
					62.339, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [12]
				{
					63.5600000000004, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [13]
				{
					68.0330000000004, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [14]
				{
					68.4030000000003, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [15]
				{
					72.0330000000004, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Murilosalla-Nemesis", -- [5]
				}, -- [16]
				{
					75.6509999999998, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [17]
				{
					75.6909999999998, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [18]
				{
					76.5299999999998, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [19]
				{
					79.2920000000004, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Murilosalla-Nemesis", -- [5]
				}, -- [20]
				{
					81.7269999999999, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [21]
				{
					81.7269999999999, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorwin", -- [5]
				}, -- [22]
				{
					82.942, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myrrodin-Nemesis", -- [5]
				}, -- [23]
			},
			[150759] = {
				{
					11.2830000000004, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.5190000000002, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					52.5879999999997, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					75.6509999999998, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[150753] = {
				{
					23.9380000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					150753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.0169999999998, -- [1]
					"Slave Watcher Crushto", -- [2]
					150753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					70.0929999999999, -- [1]
					"Slave Watcher Crushto", -- [2]
					150753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150801] = {
				{
					16.3600000000006, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.973, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					43.1140000000005, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					55.2380000000003, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					67.3879999999999, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					79.4750000000004, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [20]
	},
	["useicons"] = false,
	["deaths_data"] = {
		{
		}, -- [1]
		{
		}, -- [2]
		{
		}, -- [3]
		{
		}, -- [4]
	},
	["backdrop_color"] = {
		0, -- [1]
		0, -- [2]
		0, -- [3]
		0.4, -- [4]
	},
	["cooldowns_timeline"] = {
		{
			["Blaackmoon-Quel'Thalas"] = {
				{
					13.1130000000003, -- [1]
					"Blaackmoon-Quel'Thalas", -- [2]
					31224, -- [3]
				}, -- [1]
				{
					14.625, -- [1]
					"Blaackmoon-Quel'Thalas", -- [2]
					5277, -- [3]
				}, -- [2]
			},
		}, -- [1]
		{
		}, -- [2]
		{
			["Blaackmoon-Quel'Thalas"] = {
				{
					16.2800000000002, -- [1]
					"Blaackmoon-Quel'Thalas", -- [2]
					5277, -- [3]
				}, -- [1]
				{
					20.7020000000002, -- [1]
					"Blaackmoon-Quel'Thalas", -- [2]
					31224, -- [3]
				}, -- [2]
			},
		}, -- [3]
		{
		}, -- [4]
	},
	["debuff_timeline"] = {
		{
			["Mollymillion-Perenolde"] = {
				[197967] = {
					45.4190000000003, -- [1]
					57.4300000000003, -- [2]
					["stacks"] = {
					},
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Blaackmoon-Quel'Thalas"] = {
				[197964] = {
					45.4190000000003, -- [1]
					53.7490000000003, -- [2]
					["stacks"] = {
					},
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Bajh-Durotan"] = {
				[198088] = {
					43.7490000000003, -- [1]
					46.7510000000002, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Glowing Fragment",
					["active"] = false,
				},
				[197963] = {
					45.4340000000002, -- [1]
					48.1410000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Thorwin"] = {
				[197965] = {
					45.4190000000003, -- [1]
					54.7890000000002, -- [2]
					["stacks"] = {
					},
					["source"] = "Odyn",
					["active"] = false,
				},
			},
			["Clevahgurl-GrizzlyHills"] = {
				[197966] = {
					45.4340000000002, -- [1]
					54.799, -- [2]
					["stacks"] = {
					},
					["source"] = "Odyn",
					["active"] = false,
				},
			},
		}, -- [1]
		{
			["Mollymillion-Perenolde"] = {
				[193702] = {
					42.8339999999998, -- [1]
					43.5940000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Infernal Flames",
					["active"] = false,
				},
			},
			["Bajh-Durotan"] = {
				[193702] = {
					43.1639999999998, -- [1]
					44.5279999999998, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Infernal Flames",
					["active"] = false,
				},
			},
			["Clevahgurl-GrizzlyHills"] = {
				[193702] = {
					42.7339999999999, -- [1]
					44.288, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Infernal Flames",
					["active"] = false,
				},
			},
			["Thorwin"] = {
				[193702] = {
					36.741, -- [1]
					39.5009999999998, -- [2]
					41.3710000000001, -- [3]
					42.2440000000001, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Infernal Flames",
					["active"] = false,
				},
			},
		}, -- [2]
		{
			["Mollymillion-Perenolde"] = {
				[199050] = {
					0.121999999999844, -- [1]
					8.1260000000002, -- [2]
					13.4639999999999, -- [3]
					26.3249999999998, -- [4]
					34.0880000000002, -- [5]
					42.0900000000002, -- [6]
					["stacks"] = {
					},
					["source"] = "Valarjar Shieldmaiden",
					["active"] = false,
				},
				[198944] = {
					1.53699999999981, -- [1]
					12.9740000000002, -- [2]
					17.3240000000001, -- [3]
					31.2040000000002, -- [4]
					33.0770000000002, -- [5]
					42.0700000000002, -- [6]
					["stacks"] = {
					},
					["source"] = "Valarjar Shieldmaiden",
					["active"] = false,
				},
				[203963] = {
					56.4589999999998, -- [1]
					57.5810000000001, -- [2]
					57.721, -- [3]
					70.4969999999999, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Blaackmoon-Quel'Thalas"] = {
				[199050] = {
					13.4639999999999, -- [1]
					21.4740000000002, -- [2]
					["stacks"] = {
					},
					["source"] = "Valarjar Shieldmaiden",
					["active"] = false,
				},
				[203963] = {
					58.377, -- [1]
					70.4969999999999, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Bajh-Durotan"] = {
				[199050] = {
					0.121999999999844, -- [1]
					8.1260000000002, -- [2]
					29.2350000000001, -- [3]
					37.2370000000001, -- [4]
					["stacks"] = {
					},
					["source"] = "Valarjar Shieldmaiden",
					["active"] = false,
				},
				[203963] = {
					59.4229999999998, -- [1]
					70.4969999999999, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Thorwin"] = {
				[203963] = {
					58.4119999999998, -- [1]
					70.4969999999999, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
			["Clevahgurl-GrizzlyHills"] = {
				[203963] = {
					59.308, -- [1]
					70.4969999999999, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Eye of the Storm",
					["active"] = false,
				},
			},
		}, -- [3]
		{
			["Mollymillion-Perenolde"] = {
				[196497] = {
					14.1210000000001, -- [1]
					52.8430000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Blaackmoon-Quel'Thalas"] = {
				[197556] = {
					10.1300000000001, -- [1]
					13.6870000000001, -- [2]
					46.5910000000001, -- [3]
					50.806, -- [4]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
				[196497] = {
					14.1210000000001, -- [1]
					52.8430000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Bajh-Durotan"] = {
				[197556] = {
					46.5910000000001, -- [1]
					50.4860000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
				[196497] = {
					50.817, -- [1]
					52.8430000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Clevahgurl-GrizzlyHills"] = {
				[196497] = {
					13.6870000000001, -- [1]
					52.8430000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
				[196838] = {
					23.2529999999999, -- [1]
					29.261, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
				[197556] = {
					10.1300000000001, -- [1]
					13.1369999999999, -- [2]
					46.5910000000001, -- [3]
					49.9559999999999, -- [4]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
			["Thorwin"] = {
				[197556] = {
					10.1300000000001, -- [1]
					14.1089999999999, -- [2]
					46.5910000000001, -- [3]
					49.5940000000001, -- [4]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
				[196497] = {
					14.1210000000001, -- [1]
					52.8430000000001, -- [2]
					["stacks"] = {
					},
					["source"] = "Fenryr",
					["active"] = false,
				},
			},
		}, -- [4]
	},
	["window_scale"] = 1,
	["BossSpellCast"] = {
		{
			["Odyn"] = {
				{
					15.4350000000004, -- [1]
					"Odyn", -- [2]
					198263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					42.4120000000003, -- [1]
					"Odyn", -- [2]
					198077, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					45.4190000000003, -- [1]
					"Odyn", -- [2]
					197961, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [1]
		{
			["God-King Skovald"] = {
				{
					7.91800000000012, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Clevahgurl-GrizzlyHills", -- [5]
				}, -- [1]
				{
					8.19799999999987, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					14.7570000000001, -- [1]
					"God-King Skovald", -- [2]
					193826, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					24.8809999999999, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [4]
				{
					24.991, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					31.3440000000001, -- [1]
					"God-King Skovald", -- [2]
					194112, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					46.4360000000002, -- [1]
					"God-King Skovald", -- [2]
					193668, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [7]
				{
					51.6210000000001, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bajh-Durotan", -- [5]
				}, -- [8]
				{
					51.721, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					64.9690000000001, -- [1]
					"God-King Skovald", -- [2]
					193659, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bajh-Durotan", -- [5]
				}, -- [10]
				{
					65.0989999999997, -- [1]
					"God-King Skovald", -- [2]
					193660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
			},
		}, -- [2]
		{
			["Valarjar Shieldmaiden"] = {
				{
					0.121999999999844, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					1.53699999999981, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [2]
				{
					3.96900000000005, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [3]
				{
					13.4639999999999, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					17.3240000000001, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [5]
				{
					18.3110000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					22.1750000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [7]
				{
					29.2350000000001, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					33.0770000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					198944, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mollymillion-Perenolde", -- [5]
				}, -- [9]
				{
					34.0880000000002, -- [1]
					"Valarjar Shieldmaiden", -- [2]
					199050, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
			},
			["Olmyr the Enlightened"] = {
				{
					13.4639999999999, -- [1]
					"Olmyr the Enlightened", -- [2]
					192158, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.797, -- [1]
					"Olmyr the Enlightened", -- [2]
					192288, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bajh-Durotan", -- [5]
				}, -- [2]
				{
					40.1840000000002, -- [1]
					"Olmyr the Enlightened", -- [2]
					192158, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			["Solsten"] = {
				{
					60.4459999999999, -- [1]
					"Solsten", -- [2]
					200901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [3]
		{
			["Fenryr"] = {
				{
					4.31999999999994, -- [1]
					"Fenryr", -- [2]
					196543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					13.1369999999999, -- [1]
					"Fenryr", -- [2]
					197558, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					23.2529999999999, -- [1]
					"Fenryr", -- [2]
					196838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Clevahgurl-GrizzlyHills", -- [5]
				}, -- [3]
				{
					39.309, -- [1]
					"Fenryr", -- [2]
					196512, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					43.25, -- [1]
					"Fenryr", -- [2]
					196543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					49.5940000000001, -- [1]
					"Fenryr", -- [2]
					197558, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [4]
	},
}
