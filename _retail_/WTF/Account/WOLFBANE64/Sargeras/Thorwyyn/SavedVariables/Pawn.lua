
PawnOptions = {
	["LastVersion"] = 2.0326,
	["LastPlayerFullName"] = "Thorwyyn-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 138176,
			["Level"] = 142,
			["Link"] = "|cff0070dd|Hitem:138176::::::::103:254:512:9:1:3387:100:::|h[Fel-Chain Helm]|h|r",
		}, -- [1]
		{
			["ID"] = 139172,
			["Level"] = 142,
			["Link"] = "|cff0070dd|Hitem:139172::::::::103:254::14::::|h[Legionnaire's Fel Pendant]|h|r",
		}, -- [2]
		{
			["ID"] = 129253,
			["Level"] = 145,
			["Link"] = "|cff1eff00|Hitem:129253::::::::103:254:512:11:1:768:101:::|h[Greywatch Spaulders]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 129322,
			["Level"] = 147,
			["Link"] = "|cff0070dd|Hitem:129322::::::::103:254:512:11:2:767:1733:101:::|h[Humming Stormwing Chest]|h|r",
		}, -- [5]
		{
			["ID"] = 129333,
			["Level"] = 142,
			["Link"] = "|cff1eff00|Hitem:129333::::::::103:254:512:11:1:664:101:::|h[Mistcaller Mail Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 130005,
			["Level"] = 160,
			["Link"] = "|cff0070dd|Hitem:130005::::::::103:254::11::::|h[Valor-Bound Greaves]|h|r",
		}, -- [7]
		{
			["ID"] = 133623,
			["Level"] = 146,
			["Link"] = "|cff0070dd|Hitem:133623::::::::103:254:512:17:1:1795:102:::|h[Felstep Footguards]|h|r",
		}, -- [8]
		{
			["ID"] = 140612,
			["Level"] = 154,
			["Link"] = "|cff0070dd|Hitem:140612::::::::103:254:512:11:1:1793:102:::|h[Bracers of the Fallen]|h|r",
		}, -- [9]
		{
			["ID"] = 138174,
			["Level"] = 142,
			["Link"] = "|cff0070dd|Hitem:138174::::::::103:254:512:9:1:3387:100:::|h[Fel-Chain Grips]|h|r",
		}, -- [10]
		{
			["ID"] = 138450,
			["Level"] = 142,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:138450::::::::103:254:512:11:1:3387:100:::|h[Signet of Stormwind]|h|r",
		}, -- [11]
		{
			["ID"] = 138162,
			["Level"] = 142,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff0070dd|Hitem:138162::::::::103:254::14::::|h[Legion Bound Ring]|h|r",
		}, -- [12]
		{
			["ID"] = 138170,
			["Level"] = 142,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:138170::::::::103:254::14::::|h[Felstalker Spine]|h|r",
		}, -- [13]
		{
			["ID"] = 128959,
			["Level"] = 139,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff0070dd|Hitem:128959::::::::103:254::11::::|h[Seal of House Wrynn]|h|r",
		}, -- [14]
		{
			["ID"] = 133765,
			["Level"] = 146,
			["Link"] = "|cff0070dd|Hitem:133765::::::::103:254:512:17:1:1795:102:::|h[Cape of Valarjar Courage]|h|r",
		}, -- [15]
		{
			["ID"] = 128826,
			["Level"] = 165,
			["Link"] = "|cffe6cc80|Hitem:128826::132834:132846:::::103:254:256:9:1:1485:114:3:1793:1486:1809:3:1793:1488:1809:|h[Thas'dorah, Legacy of the Windrunners]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "HUNTER",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
