
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
		},
	},
	["last_version"] = "v8.2.5.7227",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1576002631,
		["last_version"] = 11,
	},
	["last_instance_id"] = 1950,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 1574707245,
	["active_profile"] = "Nntailfa-Sargeras",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["last_day"] = "25",
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["character_data"] = {
		["logons"] = 2,
	},
	["combat_id"] = 1,
	["savedStyles"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 73.949462890625,
					["x"] = 420.1483764648438,
					["w"] = 310,
					["h"] = 157.9999847412109,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["force_font_outline"] = "",
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.009553,
					["damage_from"] = {
					},
					["targets"] = {
						["Target Dummy"] = 27538,
					},
					["pets"] = {
					},
					["last_dps"] = 0,
					["tipo"] = 1,
					["classe"] = "PALADIN",
					["raid_targets"] = {
					},
					["total_without_pet"] = 27538.009553,
					["delay"] = 0,
					["dps_started"] = false,
					["end_time"] = 1574706809,
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 11,
								["b_amt"] = 0,
								["c_dmg"] = 10389,
								["g_amt"] = 0,
								["n_max"] = 478,
								["targets"] = {
									["Target Dummy"] = 24385,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 13996,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 41,
								["total"] = 24385,
								["c_max"] = 967,
								["id"] = 1,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 30,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							}, -- [1]
							[20271] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 739,
								["targets"] = {
									["Target Dummy"] = 1477,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1477,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 1477,
								["c_max"] = 0,
								["id"] = 20271,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							[184575] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 1676,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Target Dummy"] = 1676,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 1676,
								["c_max"] = 1676,
								["id"] = 184575,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["r_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 0.009553,
					["nome"] = "Nntailfa",
					["spec"] = 70,
					["grupo"] = true,
					["total"] = 27538.009553,
					["on_hold"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1574706668,
					["serial"] = "Player-76-0A4279D8",
					["friendlyfire_total"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.013824,
					["damage_from"] = {
						["Nntailfa"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.013824,
					["last_dps"] = 0,
					["fight_component"] = true,
					["end_time"] = 1574706809,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Target Dummy",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 27538.013824,
					["total"] = 0.013824,
					["friendlyfire"] = {
					},
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1574706806,
					["serial"] = "Creature-0-3138-1950-29719-107104-00005C1DE5",
					["dps_started"] = false,
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 0.01462,
					["resource"] = 4.01462,
					["targets"] = {
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "PALADIN",
					["passiveover"] = 0.005761,
					["resource_type"] = 9,
					["nome"] = "Nntailfa",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 7,
					},
					["grupo"] = true,
					["totalover"] = 0.005761,
					["spec"] = 70,
					["last_event"] = 0,
					["tipo"] = 3,
					["alternatepower"] = 0.01462,
					["flag_original"] = 1297,
					["serial"] = "Player-76-0A4279D8",
					["total"] = 0.01462,
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["nome"] = "Nntailfa",
					["spec"] = 70,
					["grupo"] = true,
					["last_event"] = 0,
					["spell_cast"] = {
						[20271] = 2,
						[184575] = 1,
					},
					["buff_uptime_targets"] = {
					},
					["pets"] = {
					},
					["classe"] = "PALADIN",
					["buff_uptime"] = 138,
					["buff_uptime_spells"] = {
						["_ActorTable"] = {
							[186403] = {
								["counter"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 186403,
								["uptime"] = 138,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["serial"] = "Player-76-0A4279D8",
					["tipo"] = 4,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1574706671,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["cleu_timeline"] = {
		},
		["combat_counter"] = 4,
		["totals"] = {
			63879.018441, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0.008859,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "13:31:11",
		["end_time"] = 11214.095,
		["totals_grupo"] = {
			27538.008526, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0.008859,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["segments_added"] = {
			{
				["elapsed"] = 137.2929999999997,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "13:31:11",
			}, -- [1]
		},
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = "13:33:29",
		["overall_enemy_name"] = "Target Dummy",
		["CombatSkillCache"] = {
		},
		["frags"] = {
		},
		["start_time"] = 11076.802,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["cleu_events"] = {
			["n"] = 1,
		},
	},
	["combat_counter"] = 7,
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["last_realversion"] = 140,
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A4279D8"] = 70,
	},
}
