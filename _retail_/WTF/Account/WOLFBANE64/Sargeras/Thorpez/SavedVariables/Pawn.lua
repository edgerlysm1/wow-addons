
PawnOptions = {
	["LastVersion"] = 2.0324,
	["LastPlayerFullName"] = "Thorpez-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 98983,
			["Level"] = 123,
			["Link"] = "|cffa335ee|Hitem:98983::76884:76692:::::94:263::::::|h[Helmet of Celestial Harmony]|h|r",
		}, -- [1]
		{
			["ID"] = 104909,
			["Level"] = 123,
			["Link"] = "|cffa335ee|Hitem:104909::::::::94:263::::::|h[Immerseus' Crystalline Eye]|h|r",
		}, -- [2]
		{
			["ID"] = 98977,
			["Level"] = 123,
			["Link"] = "|cffa335ee|Hitem:98977::76692:76692:::::94:263::::::|h[Spaulders of Celestial Harmony]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 117502,
			["Level"] = 120,
			["Link"] = "|cff1eff00|Hitem:117502::::::::94:263::11::::|h[Grimfrost Frostmail]|h|r",
		}, -- [5]
		{
			["ID"] = 118036,
			["Level"] = 125,
			["Link"] = "|cff0070dd|Hitem:118036::::::::94:263::11:1:171:::|h[Wildwood Wrangler Links]|h|r",
		}, -- [6]
		{
			["ID"] = 106172,
			["Level"] = 121,
			["Link"] = "|cff0070dd|Hitem:106172::::::::94:263::11::::|h[Frostwolf Ringmail Leggings]|h|r",
		}, -- [7]
		{
			["ID"] = 112593,
			["Level"] = 124,
			["Link"] = "|cff1eff00|Hitem:112593::::::::94:263::11::::|h[Wildwood Wrangler Sabatons]|h|r",
		}, -- [8]
		{
			["ID"] = 105119,
			["Level"] = 123,
			["Link"] = "|cffa335ee|Hitem:105119::::::::94:263::::::|h[Bracers of Infinite Pipes]|h|r",
		}, -- [9]
		{
			["ID"] = 98993,
			["Level"] = 123,
			["Link"] = "|cffa335ee|Hitem:98993::76692:76692:::::94:263::::::|h[Grips of Celestial Harmony]|h|r",
		}, -- [10]
		{
			["ID"] = 105060,
			["Level"] = 123,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffa335ee|Hitem:105060::76692::::::94:263::::::|h[Ring of Restless Energy]|h|r",
		}, -- [11]
		{
			["ID"] = 118038,
			["Level"] = 123,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff1eff00|Hitem:118038::::::::94:263::11::::|h[Steamfury Signet]|h|r",
		}, -- [12]
		{
			["ID"] = 112886,
			["Level"] = 128,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:112886::::::::94:263::11:1:15:::|h[Ferocity of Kor'gall]|h|r",
		}, -- [13]
		{
			["ID"] = 105029,
			["Level"] = 123,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:105029::::::::94:263::::::|h[Haromm's Talisman]|h|r",
		}, -- [14]
		{
			["ID"] = 105028,
			["Level"] = 123,
			["Link"] = "|cffa335ee|Hitem:105028::::::::94:263::::::|h[Poisonmist Nightcloak]|h|r",
		}, -- [15]
		{
			["ID"] = 118026,
			["Level"] = 126,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cff1eff00|Hitem:118026::::::::94:263::11::::|h[Ogron Slayer's Eyebruiser]|h|r",
		}, -- [16]
		{
			["ID"] = 111912,
			["Level"] = 122,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cff0070dd|Hitem:111912::::::::94:263::11::::|h[Thunderlord Riding Crop]|h|r",
		}, -- [17]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "SHAMAN",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
