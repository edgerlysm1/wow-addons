
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
		},
	},
	["combat_counter"] = 23,
	["character_data"] = {
		["logons"] = 2,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 24,
					["x"] = -493.999816894531,
					["w"] = 320.000122070313,
					["h"] = 130,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09C9A61C"] = {
			21973, -- [1]
			19272, -- [2]
			22150, -- [3]
			19260, -- [4]
			22355, -- [5]
			22147, -- [6]
			21966, -- [7]
		},
	},
	["last_instance_id"] = 1554,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["active_profile"] = "Lochogh-Sargeras",
	["last_realversion"] = 130,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["enabled"] = true,
			["dps"] = 0,
			["level"] = 1,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["author"] = "Details! Team",
			["realm_history"] = {
			},
			["realm_lastamt"] = 0,
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				["last_selected"] = 1,
			},
			["options"] = {
				["show_method"] = 4,
				["auto_create"] = true,
				["window_scale"] = 1,
			},
		},
	},
	["nick_tag_cache"] = {
		["nextreset"] = 1530754944,
		["last_version"] = 10,
	},
	["ignore_nicktag"] = false,
	["last_day"] = "19",
	["last_version"] = "v7.3.5.5572",
	["combat_id"] = 14,
	["savedStyles"] = {
	},
	["last_instance_time"] = 1529459039,
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.048517,
					["damage_from"] = {
						["Alliance Infantry"] = true,
						["Felblaze Infernal"] = true,
						["Felfire Imp"] = true,
					},
					["targets"] = {
						["Arcane Construct"] = 115539,
						["Ambushing Fel Bat"] = 29102,
						["Alliance Infantry"] = 658923,
						["Felblaze Infernal"] = 225681,
						["Felfire Imp"] = 499887,
						["Target Dummy"] = 2245910,
					},
					["spec"] = 262,
					["pets"] = {
						"Greater Fire Elemental <Lochogh> <Lochogh>", -- [1]
					},
					["last_event"] = 0,
					["classe"] = "SHAMAN",
					["raid_targets"] = {
					},
					["total_without_pet"] = 3697258.048517,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1529459031,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Lochogh",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 23,
								["b_amt"] = 1,
								["c_dmg"] = 64761,
								["g_amt"] = 0,
								["n_max"] = 1676,
								["targets"] = {
									["Target Dummy"] = 150698,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 85937,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 99,
								["total"] = 150698,
								["c_max"] = 3378,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 10,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 2283,
								["n_amt"] = 66,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[193786] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 27026,
								["g_amt"] = 0,
								["n_max"] = 13249,
								["targets"] = {
									["Target Dummy"] = 197735,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 170709,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 14,
								["total"] = 197735,
								["c_max"] = 27026,
								["id"] = 193786,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 13,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[8042] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 33073,
								["targets"] = {
									["Alliance Infantry"] = 99808,
									["Felblaze Infernal"] = 33073,
									["Target Dummy"] = 75595,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 208476,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 11,
								["total"] = 208476,
								["c_max"] = 0,
								["id"] = 8042,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 11,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[10444] = {
								["c_amt"] = 25,
								["b_amt"] = 0,
								["c_dmg"] = 39068,
								["g_amt"] = 0,
								["n_max"] = 771,
								["targets"] = {
									["Target Dummy"] = 131183,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 92115,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 145,
								["total"] = 131183,
								["c_max"] = 1573,
								["id"] = 10444,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 120,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[188443] = {
								["c_amt"] = 14,
								["b_amt"] = 0,
								["c_dmg"] = 205719,
								["g_amt"] = 0,
								["n_max"] = 7204,
								["targets"] = {
									["Arcane Construct"] = 115539,
									["Ambushing Fel Bat"] = 29102,
									["Alliance Infantry"] = 122743,
									["Felblaze Infernal"] = 57916,
									["Felfire Imp"] = 499887,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 619468,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 100,
								["total"] = 825187,
								["c_max"] = 14695,
								["id"] = 188443,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 86,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[196834] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 7275,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Target Dummy"] = 7275,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 7275,
								["c_max"] = 7275,
								["id"] = 196834,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[32176] = {
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 59520,
								["g_amt"] = 0,
								["n_max"] = 7811,
								["targets"] = {
									["Target Dummy"] = 156980,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 97460,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 17,
								["total"] = 156980,
								["c_max"] = 15045,
								["id"] = 32176,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 13,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[188389] = {
								["c_amt"] = 22,
								["b_amt"] = 0,
								["c_dmg"] = 105834,
								["g_amt"] = 0,
								["n_max"] = 3843,
								["targets"] = {
									["Alliance Infantry"] = 104373,
									["Felblaze Infernal"] = 25592,
									["Target Dummy"] = 238718,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 262849,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 128,
								["total"] = 368683,
								["c_max"] = 7839,
								["id"] = 188389,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 106,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[188196] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 51431,
								["g_amt"] = 0,
								["n_max"] = 8404,
								["targets"] = {
									["Alliance Infantry"] = 93113,
									["Felblaze Infernal"] = 42018,
									["Target Dummy"] = 126394,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 210094,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 28,
								["total"] = 261525,
								["c_max"] = 17144,
								["id"] = 188196,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 25,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[32175] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 53080,
								["g_amt"] = 0,
								["n_max"] = 13265,
								["targets"] = {
									["Target Dummy"] = 247261,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 194181,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 17,
								["total"] = 247261,
								["c_max"] = 26854,
								["id"] = 32175,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 15,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[25504] = {
								["c_amt"] = 6,
								["b_amt"] = 0,
								["c_dmg"] = 20966,
								["g_amt"] = 0,
								["n_max"] = 1776,
								["targets"] = {
									["Target Dummy"] = 65653,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 44687,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 32,
								["total"] = 65653,
								["c_max"] = 3536,
								["id"] = 25504,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 26,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[193796] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 23593,
								["g_amt"] = 0,
								["n_max"] = 5783,
								["targets"] = {
									["Target Dummy"] = 57847,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 34254,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 8,
								["total"] = 57847,
								["c_max"] = 11797,
								["id"] = 193796,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[187874] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 9319,
								["g_amt"] = 0,
								["n_max"] = 4778,
								["targets"] = {
									["Target Dummy"] = 46588,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 37269,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 46588,
								["c_max"] = 9319,
								["id"] = 187874,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 8,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[195256] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5561,
								["targets"] = {
									["Target Dummy"] = 104241,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 104241,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 77,
								["total"] = 104241,
								["c_max"] = 0,
								["id"] = 195256,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 77,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[60103] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 59321,
								["g_amt"] = 0,
								["n_max"] = 29231,
								["targets"] = {
									["Target Dummy"] = 299506,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 240185,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 299506,
								["c_max"] = 29724,
								["id"] = 60103,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 11,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[51505] = {
								["c_amt"] = 18,
								["b_amt"] = 0,
								["c_dmg"] = 484891,
								["g_amt"] = 0,
								["n_max"] = 13205,
								["targets"] = {
									["Alliance Infantry"] = 161102,
									["Felblaze Infernal"] = 67082,
									["Target Dummy"] = 296322,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 39615,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 21,
								["total"] = 524506,
								["c_max"] = 26939,
								["id"] = 51505,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[210801] = {
								["c_amt"] = 8,
								["b_amt"] = 0,
								["c_dmg"] = 10086,
								["g_amt"] = 0,
								["n_max"] = 619,
								["targets"] = {
									["Target Dummy"] = 43914,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 33828,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 63,
								["total"] = 43914,
								["c_max"] = 1261,
								["id"] = 210801,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 55,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 3775042.048517,
					["serial"] = "Player-76-09C9A61C",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 177140.048517,
					["start_time"] = 1529458628,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 8465,
					["totalabsorbed"] = 0.014373,
					["last_event"] = 0,
					["damage_from"] = {
					},
					["targets"] = {
						["Alliance Infantry"] = 77784,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "PET",
					["raid_targets"] = {
					},
					["total_without_pet"] = 77784.014373,
					["damage_taken"] = 0.014373,
					["dps_started"] = false,
					["total"] = 77784.014373,
					["friendlyfire_total"] = 0,
					["ownerName"] = "Lochogh",
					["nome"] = "Greater Fire Elemental <Lochogh> <Lochogh>",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							[57984] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 12964,
								["targets"] = {
									["Alliance Infantry"] = 77784,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 77784,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 77784,
								["c_max"] = 0,
								["id"] = 57984,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["end_time"] = 1529459332,
					["friendlyfire"] = {
					},
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1529459328,
					["delay"] = 0,
					["serial"] = "Creature-0-3138-1554-17128-95061-000029B215",
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorb"] = 0.011975,
					["last_hps"] = 0,
					["healing_from"] = {
						["Lochogh"] = true,
						["Anchorite Taliah"] = true,
					},
					["targets_overheal"] = {
						["Lochogh"] = 2690,
					},
					["targets"] = {
						["Lochogh"] = 84532,
					},
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[8004] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Lochogh"] = 2690,
								},
								["n_max"] = 20924,
								["targets"] = {
									["Lochogh"] = 81842,
								},
								["n_min"] = 0,
								["counter"] = 3,
								["overheal"] = 2690,
								["total"] = 81842,
								["c_max"] = 39994,
								["id"] = 8004,
								["targets_absorbs"] = {
								},
								["c_curado"] = 39994,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 41848,
								["n_amt"] = 2,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["totalover_without_pet"] = 0.011975,
					["classe"] = "SHAMAN",
					["totalover"] = 2690.011975,
					["total_without_pet"] = 81842.011975,
					["healing_taken"] = 101338.011975,
					["heal_enemy_amt"] = 0,
					["fight_component"] = true,
					["end_time"] = 1529459264,
					["total"] = 81842.011975,
					["start_time"] = 1529459254,
					["nome"] = "Lochogh",
					["spec"] = 262,
					["grupo"] = true,
					["targets_absorbs"] = {
					},
					["heal_enemy"] = {
					},
					["serial"] = "Player-76-09C9A61C",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["totaldenied"] = 0.011975,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 1600.047425,
					["resource"] = 0.047425,
					["targets"] = {
						["Lochogh"] = 1600,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "SHAMAN",
					["total"] = 1600.047425,
					["resource_type"] = 5,
					["nome"] = "Lochogh",
					["spec"] = 262,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[51505] = {
								["id"] = 51505,
								["total"] = 228,
								["targets"] = {
									["Lochogh"] = 228,
								},
								["counter"] = 19,
							},
							[187890] = {
								["id"] = 187890,
								["total"] = 585,
								["targets"] = {
									["Lochogh"] = 585,
								},
								["counter"] = 117,
							},
							[193786] = {
								["id"] = 193786,
								["total"] = 325,
								["targets"] = {
									["Lochogh"] = 325,
								},
								["counter"] = 13,
							},
							[214815] = {
								["id"] = 214815,
								["total"] = 216,
								["targets"] = {
									["Lochogh"] = 216,
								},
								["counter"] = 27,
							},
							[195897] = {
								["id"] = 195897,
								["total"] = 246,
								["targets"] = {
									["Lochogh"] = 246,
								},
								["counter"] = 13,
							},
						},
					},
					["tipo"] = 3,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.047425,
					["serial"] = "Player-76-09C9A61C",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["debuff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[188389] = {
								["refreshamt"] = 29,
								["activedamt"] = 3,
								["appliedamt"] = 9,
								["id"] = 188389,
								["uptime"] = 177,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[147732] = {
								["refreshamt"] = 21,
								["activedamt"] = 0,
								["appliedamt"] = 1,
								["id"] = 147732,
								["uptime"] = 19,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["pets"] = {
						"Greater Fire Elemental <Lochogh> <Lochogh>", -- [1]
					},
					["classe"] = "SHAMAN",
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[205648] = {
								["refreshamt"] = 1,
								["activedamt"] = 15,
								["appliedamt"] = 15,
								["id"] = 205648,
								["uptime"] = 16,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[215785] = {
								["refreshamt"] = 0,
								["activedamt"] = 7,
								["appliedamt"] = 7,
								["id"] = 215785,
								["uptime"] = 35,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[196834] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 196834,
								["uptime"] = 16,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[58875] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 58875,
								["uptime"] = 8,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[195222] = {
								["refreshamt"] = 1,
								["activedamt"] = 5,
								["appliedamt"] = 5,
								["id"] = 195222,
								["uptime"] = 43,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[194084] = {
								["refreshamt"] = 4,
								["activedamt"] = 4,
								["appliedamt"] = 4,
								["id"] = 194084,
								["uptime"] = 120,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[77762] = {
								["refreshamt"] = 1,
								["activedamt"] = 14,
								["appliedamt"] = 14,
								["id"] = 77762,
								["uptime"] = 25,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[201846] = {
								["refreshamt"] = 4,
								["activedamt"] = 14,
								["appliedamt"] = 14,
								["id"] = 201846,
								["uptime"] = 64,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["debuff_uptime"] = 196,
					["nome"] = "Lochogh",
					["spec"] = 262,
					["grupo"] = true,
					["spell_cast"] = {
						[8042] = 11,
						[193796] = 7,
						[188443] = 24,
						[188389] = 35,
						[188196] = 27,
						[8004] = 3,
						[58875] = 1,
						[17364] = 17,
						[187874] = 9,
						[193786] = 14,
						[60103] = 13,
						[196834] = 1,
						[51505] = 19,
					},
					["buff_uptime_targets"] = {
					},
					["last_event"] = 0,
					["debuff_uptime_targets"] = {
					},
					["buff_uptime"] = 327,
					["serial"] = "Player-76-09C9A61C",
					["tipo"] = 4,
				}, -- [1]
				{
					["flag_original"] = 8465,
					["ownerName"] = "Lochogh",
					["nome"] = "Greater Fire Elemental <Lochogh> <Lochogh>",
					["spell_cast"] = {
						[57984] = 6,
					},
					["pets"] = {
					},
					["classe"] = "PET",
					["last_event"] = 0,
					["serial"] = "Creature-0-3138-1554-17128-95061-000029B215",
					["tipo"] = 4,
				}, -- [2]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 8,
		["totals"] = {
			4642912.052218, -- [1]
			81841.975244, -- [2]
			{
				0, -- [1]
				[0] = 1600.038685,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["end_time"] = 531012.907,
		["data_inicio"] = "21:43:35",
		["frags"] = {
		},
		["data_fim"] = "21:54:28",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 114.001999999979,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:52:34",
			}, -- [1]
			{
				["elapsed"] = 34.2530000000261,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:51:48",
			}, -- [2]
			{
				["elapsed"] = 55.7909999999683,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:49:09",
			}, -- [3]
			{
				["elapsed"] = 58.4769999999553,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:47:54",
			}, -- [4]
			{
				["elapsed"] = 22.2559999999357,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:47:22",
			}, -- [5]
			{
				["elapsed"] = 10.1269999999786,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:46:11",
			}, -- [6]
			{
				["elapsed"] = 59.5949999999721,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:44:29",
			}, -- [7]
			{
				["elapsed"] = 30.0310000000754,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:43:53",
			}, -- [8]
			{
				["elapsed"] = 15.716999999946,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "21:43:35",
			}, -- [9]
		},
		["start_time"] = 530612.658,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["totals_grupo"] = {
			3775042.040845, -- [1]
			81842.010637, -- [2]
			{
				0, -- [1]
				[0] = 1600.038685,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
	},
	["force_font_outline"] = "",
	["SoloTablesSaved"] = {
		["LastSelected"] = "DETAILS_PLUGIN_DAMAGE_RANK",
		["Mode"] = 1,
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09C9A61C"] = 263,
	},
}
