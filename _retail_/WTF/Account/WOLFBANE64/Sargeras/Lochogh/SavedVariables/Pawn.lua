
PawnOptions = {
	["LastVersion"] = 2.0218,
	["LastPlayerFullName"] = "Lochogh-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "SHAMAN",
	["LastAdded"] = 1,
}
