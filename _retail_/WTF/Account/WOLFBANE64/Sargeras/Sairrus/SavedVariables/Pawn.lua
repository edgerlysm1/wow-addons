
PawnOptions = {
	["LastVersion"] = 2.0406,
	["LastPlayerFullName"] = "Sairrus-Sargeras",
	["AutoSelectScales"] = false,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 160909,
			["Level"] = 58,
			["Link"] = "|cff0070dd|Hitem:160909::::::::45:64::11:::::|h[Crown of the Champion]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 55,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::45:64::11:4:4932:4933:4935:1469::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 134309,
			["Level"] = 53,
			["Link"] = "|cffa335ee|Hitem:134309::::::::45:64::35:3:3536:1475:3337::::|h[Manawracker Shoulders]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 151947,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:151947::::::::45:64::5:3:3611:1472:3336::::|h[Vestments of Enflamed Blight]|h|r",
		}, -- [5]
		{
			["ID"] = 151952,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:151952::::::::45:64::5:3:3611:1472:3336::::|h[Cord of Blossoming Petals]|h|r",
		}, -- [6]
		{
			["ID"] = 142202,
			["Level"] = 52,
			["Link"] = "|cffa335ee|Hitem:142202::::::::45:64::35:3:3536:1474:3337::::|h[Trousers of Royal Vanity]|h|r",
		}, -- [7]
		{
			["ID"] = 137319,
			["Level"] = 52,
			["Link"] = "|cffa335ee|Hitem:137319::::::::45:64::35:4:3510:40:1474:3337::::|h[Paranoid Sprinters]|h|r",
		}, -- [8]
		{
			["ID"] = 138140,
			["Level"] = 59,
			["Link"] = "|cffff8000|Hitem:138140::::::::45:64:::2:1811:3630::::|h[Magtheridon's Banished Bracers]|h|r",
		}, -- [9]
		{
			["ID"] = 134217,
			["Level"] = 53,
			["Link"] = "|cffa335ee|Hitem:134217::::::::45:64::35:3:3536:1475:3337::::|h[Bonespeaker Gloves]|h|r",
		}, -- [10]
		{
			["ID"] = 132410,
			["Level"] = 59,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffff8000|Hitem:132410:5427:151580::::::45:64:::2:1811:3630::::|h[Shard of the Exodar]|h|r",
		}, -- [11]
		{
			["ID"] = 151311,
			["Level"] = 53,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:151311:5427:::::::45:64::35:4:3536:42:1475:3337::::|h[Band of the Triumvirate]|h|r",
		}, -- [12]
		{
			["ID"] = 151971,
			["Level"] = 50,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:151971::::::::45:64::5:3:3611:1472:3336::::|h[Sheath of Asara]|h|r",
		}, -- [13]
		{
			["ID"] = 151955,
			["Level"] = 50,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:151955::::::::45:64::3:4:3610:41:1472:3337::::|h[Acrid Catalyst Injector]|h|r",
		}, -- [14]
		{
			["ID"] = 152062,
			["Level"] = 50,
			["Link"] = "|cffa335ee|Hitem:152062::::::::45:64::5:3:3611:1472:3337::::|h[Greatcloak of the Dark Pantheon]|h|r",
		}, -- [15]
		{
			["ID"] = 128862,
			["Level"] = 71,
			["Link"] = "|cffe6cc80|Hitem:128862::152039:137379:155850::::45:64::9:2:731:1523:2:8:888:24:1:3:3610:1472:3528:3:3536:1475:3337:3:3610:1472:3528|h[Ebonchill]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "MAGE",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
