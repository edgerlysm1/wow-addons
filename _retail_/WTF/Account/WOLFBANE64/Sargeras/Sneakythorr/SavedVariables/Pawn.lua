
PawnOptions = {
	["LastVersion"] = 2.0406,
	["LastPlayerFullName"] = "Sneakythorr-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 183557,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183557::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Cowl]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 58,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::50:260::11:4:4932:4933:4935:1472:1:28:2122:::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 183637,
			["Level"] = 110,
			["Link"] = "|cffa335ee|Hitem:183637::::::::50:260:::2:1482:4786:1:28:1401:::|h[Shoulderpads of the Notorious Knave]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 183526,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183526::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Tunic]|h|r",
		}, -- [5]
		{
			["ID"] = 183559,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183559::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 183531,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183531::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Leggings]|h|r",
		}, -- [7]
		{
			["ID"] = 183558,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183558::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Boots]|h|r",
		}, -- [8]
		{
			["ID"] = 183534,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183534::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Wriststraps]|h|r",
		}, -- [9]
		{
			["ID"] = 183528,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183528::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Grips]|h|r",
		}, -- [10]
		{
			["ID"] = 183673,
			["Level"] = 110,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffa335ee|Hitem:183673::::::::50:260:::2:1482:4786:1:28:1401:::|h[Nerubian Aegis Ring]|h|r",
		}, -- [11]
		{
			["ID"] = 183676,
			["Level"] = 110,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:183676::::::::50:260:::2:1482:4786:1:28:1401:::|h[Hailstone Loop]|h|r",
		}, -- [12]
		{
			["ID"] = 183650,
			["Level"] = 110,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:183650::::::::50:260:::2:1482:4786:1:28:1401:::|h[Miniscule Abomination in a Jar]|h|r",
		}, -- [13]
		{
			["ID"] = 183650,
			["Level"] = 110,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:183650::::::::51:260:::2:1482:4786:1:28:1401:::|h[Miniscule Abomination in a Jar]|h|r",
		}, -- [14]
		{
			["ID"] = 183549,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183549::::::::50:260::14::1:28:1401:::|h[Argent Conscript's Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 165625,
			["Level"] = 115,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cffa335ee|Hitem:165625::::::::50:260::81:3:5844:1527:4786:1:28:1401:::|h[Sentinel's Warhammer]|h|r",
		}, -- [16]
		{
			["ID"] = 165625,
			["Level"] = 115,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cffa335ee|Hitem:165625::::::::51:260::81:3:5844:1527:4786:1:28:1401:::|h[Sentinel's Warhammer]|h|r",
		}, -- [17]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "ROGUE",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
