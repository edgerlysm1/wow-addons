
PawnOptions = {
	["LastVersion"] = 2.0218,
	["LastPlayerFullName"] = "Fincamagar-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "ROGUE",
	["LastAdded"] = 1,
}
