
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
		},
	},
	["combat_counter"] = 12,
	["character_data"] = {
		["logons"] = 2,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 134.000030517578,
					["x"] = -535.399963378906,
					["w"] = 320,
					["h"] = 129.999984741211,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09CB7B20"] = {
			22114, -- [1]
			19238, -- [2]
			22123, -- [3]
			21188, -- [4]
			19241, -- [5]
			22119, -- [6]
			19249, -- [7]
		},
	},
	["last_instance_id"] = 1554,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["active_profile"] = "Fincamagar-Sargeras",
	["last_realversion"] = 130,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["enabled"] = true,
			["dps"] = 0,
			["level"] = 1,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["author"] = "Details! Team",
			["realm_history"] = {
			},
			["realm_lastamt"] = 0,
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				["last_selected"] = 1,
			},
			["options"] = {
				["show_method"] = 4,
				["auto_create"] = true,
				["window_scale"] = 1,
			},
		},
	},
	["nick_tag_cache"] = {
		["nextreset"] = 1531257408,
		["last_version"] = 10,
	},
	["ignore_nicktag"] = false,
	["last_day"] = "25",
	["last_version"] = "v7.3.5.5572",
	["combat_id"] = 5,
	["savedStyles"] = {
	},
	["last_instance_time"] = 1529961984,
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.02911,
					["damage_from"] = {
						["Alliance Infantry"] = true,
						["Felblaze Infernal"] = true,
					},
					["targets"] = {
						["Alliance Infantry"] = 664554,
						["Felblaze Infernal"] = 296513,
						["Ambushing Fel Bat"] = 208686,
						["Target Dummy"] = 3825337,
					},
					["spec"] = 259,
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "ROGUE",
					["raid_targets"] = {
					},
					["total_without_pet"] = 4995090.02911,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1529961625,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Fincamagar",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 118,
								["b_amt"] = 14,
								["c_dmg"] = 387264,
								["g_amt"] = 0,
								["n_max"] = 2340,
								["targets"] = {
									["Alliance Infantry"] = 91114,
									["Felblaze Infernal"] = 62858,
									["Ambushing Fel Bat"] = 33392,
									["Target Dummy"] = 813467,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 613567,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 609,
								["total"] = 1000831,
								["c_max"] = 4771,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 93,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 21825,
								["n_amt"] = 398,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[5374] = {
								["c_amt"] = 3,
								["b_amt"] = 2,
								["c_dmg"] = 59172,
								["g_amt"] = 0,
								["n_max"] = 10487,
								["targets"] = {
									["Alliance Infantry"] = 99869,
									["Felblaze Infernal"] = 33654,
									["Ambushing Fel Bat"] = 50257,
									["Target Dummy"] = 109643,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 234251,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 27,
								["total"] = 293423,
								["c_max"] = 19838,
								["id"] = 5374,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 13854,
								["n_amt"] = 24,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[32645] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 51483,
								["g_amt"] = 0,
								["n_max"] = 25237,
								["targets"] = {
									["Alliance Infantry"] = 25237,
									["Target Dummy"] = 76720,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 50474,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 101957,
								["c_max"] = 51483,
								["id"] = 32645,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[2098] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 327585,
								["g_amt"] = 0,
								["n_max"] = 38234,
								["targets"] = {
									["Target Dummy"] = 778738,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 451153,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 19,
								["total"] = 778738,
								["c_max"] = 77997,
								["id"] = 2098,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 14,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[113780] = {
								["c_amt"] = 10,
								["b_amt"] = 0,
								["c_dmg"] = 34059,
								["g_amt"] = 0,
								["n_max"] = 1670,
								["targets"] = {
									["Alliance Infantry"] = 33593,
									["Felblaze Infernal"] = 23505,
									["Ambushing Fel Bat"] = 15159,
									["Target Dummy"] = 73657,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 111855,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 77,
								["total"] = 145914,
								["c_max"] = 3406,
								["id"] = 113780,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 67,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[2818] = {
								["c_amt"] = 6,
								["b_amt"] = 0,
								["c_dmg"] = 36792,
								["g_amt"] = 0,
								["n_max"] = 3006,
								["targets"] = {
									["Alliance Infantry"] = 66612,
									["Felblaze Infernal"] = 27054,
									["Ambushing Fel Bat"] = 12144,
									["Target Dummy"] = 108335,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 177353,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 65,
								["total"] = 214145,
								["c_max"] = 6132,
								["id"] = 2818,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 59,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[27576] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 20621,
								["g_amt"] = 0,
								["n_max"] = 5249,
								["targets"] = {
									["Alliance Infantry"] = 49717,
									["Felblaze Infernal"] = 20071,
									["Ambushing Fel Bat"] = 25168,
									["Target Dummy"] = 50319,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 124654,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 27,
								["total"] = 145275,
								["c_max"] = 10344,
								["id"] = 27576,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 25,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[197834] = {
								["c_amt"] = 21,
								["b_amt"] = 3,
								["c_dmg"] = 371031,
								["g_amt"] = 0,
								["n_max"] = 11492,
								["targets"] = {
									["Target Dummy"] = 959518,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 588487,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 88,
								["total"] = 959518,
								["c_max"] = 19485,
								["id"] = 197834,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 19398,
								["n_amt"] = 67,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[86392] = {
								["c_amt"] = 18,
								["b_amt"] = 2,
								["c_dmg"] = 92237,
								["g_amt"] = 0,
								["n_max"] = 2581,
								["targets"] = {
									["Target Dummy"] = 235101,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 142864,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 76,
								["total"] = 235101,
								["c_max"] = 5255,
								["id"] = 86392,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 3561,
								["n_amt"] = 58,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[185763] = {
								["c_amt"] = 7,
								["b_amt"] = 0,
								["c_dmg"] = 126882,
								["g_amt"] = 0,
								["n_max"] = 8886,
								["targets"] = {
									["Target Dummy"] = 286815,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 159933,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 25,
								["total"] = 286815,
								["c_max"] = 18126,
								["id"] = 185763,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 18,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[1943] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 82008,
								["g_amt"] = 0,
								["n_max"] = 8040,
								["targets"] = {
									["Alliance Infantry"] = 177198,
									["Felblaze Infernal"] = 56601,
									["Ambushing Fel Bat"] = 24120,
									["Target Dummy"] = 177841,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 353752,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 49,
								["total"] = 435760,
								["c_max"] = 16402,
								["id"] = 1943,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 44,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[703] = {
								["c_amt"] = 10,
								["b_amt"] = 0,
								["c_dmg"] = 98437,
								["g_amt"] = 0,
								["n_max"] = 4826,
								["targets"] = {
									["Alliance Infantry"] = 121214,
									["Felblaze Infernal"] = 72770,
									["Ambushing Fel Bat"] = 48446,
									["Target Dummy"] = 155183,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 299176,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 72,
								["total"] = 397613,
								["c_max"] = 9844,
								["id"] = 703,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 62,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 4995090.02911,
					["serial"] = "Player-76-09CB7B20",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 214207.02911,
					["start_time"] = 1529961154,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorb"] = 0.022312,
					["last_hps"] = 0,
					["targets_overheal"] = {
						["Fincamagar"] = 44846,
					},
					["healing_from"] = {
						["Fincamagar"] = true,
						["Anchorite Taliah"] = true,
					},
					["targets"] = {
						["Fincamagar"] = 41194,
					},
					["targets_absorbs"] = {
					},
					["pets"] = {
					},
					["totalover_without_pet"] = 0.022312,
					["iniciar_hps"] = false,
					["heal_enemy_amt"] = 0,
					["totalover"] = 44846.022312,
					["total_without_pet"] = 40675.022312,
					["healing_taken"] = 42815.022312,
					["classe"] = "ROGUE",
					["fight_component"] = true,
					["end_time"] = 1529961826,
					["total"] = 40675.022312,
					["start_time"] = 1529961793,
					["nome"] = "Fincamagar",
					["spec"] = 259,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[143924] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Fincamagar"] = 44846,
								},
								["n_max"] = 7489,
								["targets"] = {
									["Fincamagar"] = 40675,
								},
								["n_min"] = 0,
								["counter"] = 25,
								["overheal"] = 44846,
								["total"] = 40675,
								["c_max"] = 0,
								["id"] = 143924,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 40675,
								["n_amt"] = 25,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["heal_enemy"] = {
					},
					["serial"] = "Player-76-09CB7B20",
					["custom"] = 0,
					["tipo"] = 2,
					["on_hold"] = false,
					["totaldenied"] = 0.022312,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 868.033675,
					["resource"] = 0.033675,
					["targets"] = {
						["Fincamagar"] = 868,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "ROGUE",
					["fight_component"] = true,
					["total"] = 868.033675,
					["nome"] = "Fincamagar",
					["spec"] = 259,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[193315] = {
								["id"] = 193315,
								["total"] = 63,
								["targets"] = {
									["Fincamagar"] = 63,
								},
								["counter"] = 60,
							},
							[185763] = {
								["id"] = 185763,
								["total"] = 21,
								["targets"] = {
									["Fincamagar"] = 21,
								},
								["counter"] = 25,
							},
							[1329] = {
								["id"] = 1329,
								["total"] = 48,
								["targets"] = {
									["Fincamagar"] = 48,
								},
								["counter"] = 25,
							},
							[35546] = {
								["id"] = 35546,
								["total"] = 680,
								["targets"] = {
									["Fincamagar"] = 680,
								},
								["counter"] = 68,
							},
							[197834] = {
								["id"] = 197834,
								["total"] = 20,
								["targets"] = {
									["Fincamagar"] = 20,
								},
								["counter"] = 26,
							},
							[703] = {
								["id"] = 703,
								["total"] = 10,
								["targets"] = {
									["Fincamagar"] = 10,
								},
								["counter"] = 11,
							},
							[139546] = {
								["id"] = 139546,
								["total"] = 26,
								["targets"] = {
									["Fincamagar"] = 26,
								},
								["counter"] = 26,
							},
						},
					},
					["tipo"] = 3,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.033675,
					["serial"] = "Player-76-09CB7B20",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["debuff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[185763] = {
								["refreshamt"] = 10,
								["activedamt"] = 0,
								["appliedamt"] = 15,
								["id"] = 185763,
								["uptime"] = 126,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[2818] = {
								["refreshamt"] = 1,
								["activedamt"] = 1,
								["appliedamt"] = 8,
								["id"] = 2818,
								["uptime"] = 214,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[1330] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 2,
								["id"] = 1330,
								["uptime"] = 6,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[703] = {
								["refreshamt"] = 1,
								["activedamt"] = 1,
								["appliedamt"] = 12,
								["id"] = 703,
								["uptime"] = 159,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[1943] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 9,
								["id"] = 1943,
								["uptime"] = 113,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["buff_uptime"] = 1001,
					["classe"] = "ROGUE",
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[193357] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 193357,
								["uptime"] = 62,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[193359] = {
								["refreshamt"] = 0,
								["activedamt"] = 5,
								["appliedamt"] = 5,
								["id"] = 193359,
								["uptime"] = 84,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[193316] = {
								["refreshamt"] = 8,
								["appliedamt"] = 3,
								["activedamt"] = 3,
								["uptime"] = 190,
								["id"] = 193316,
								["actived_at"] = 1529962087,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[199603] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 199603,
								["uptime"] = 28,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[2823] = {
								["refreshamt"] = 0,
								["activedamt"] = 4,
								["appliedamt"] = 4,
								["id"] = 2823,
								["uptime"] = 244,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[32645] = {
								["refreshamt"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 32645,
								["uptime"] = 13,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[193356] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 193356,
								["uptime"] = 23,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[1784] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 1784,
								["uptime"] = 0,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[199600] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 199600,
								["uptime"] = 18,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[195627] = {
								["refreshamt"] = 6,
								["activedamt"] = 22,
								["appliedamt"] = 22,
								["id"] = 195627,
								["uptime"] = 93,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[193538] = {
								["refreshamt"] = 18,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 193538,
								["uptime"] = 199,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[185763] = {
								["refreshamt"] = 0,
								["activedamt"] = 25,
								["appliedamt"] = 25,
								["id"] = 185763,
								["uptime"] = 15,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[13877] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 13877,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[193358] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 193358,
								["uptime"] = 29,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["fight_component"] = true,
					["debuff_uptime"] = 618,
					["buff_uptime_targets"] = {
					},
					["spec"] = 259,
					["grupo"] = true,
					["spell_cast"] = {
						[13877] = 1,
						[193316] = 11,
						[1329] = 27,
						[32645] = 3,
						[193315] = 60,
						[27576] = 27,
						[2098] = 19,
						[139546] = 26,
						[185763] = 25,
						[1943] = 9,
						[703] = 11,
					},
					["debuff_uptime_targets"] = {
					},
					["tipo"] = 4,
					["nome"] = "Fincamagar",
					["pets"] = {
					},
					["serial"] = "Player-76-09CB7B20",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 6,
		["totals"] = {
			6557090.149882, -- [1]
			72345.034531, -- [2]
			{
				0, -- [1]
				[0] = 868.024757,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["end_time"] = 418574.697,
		["data_inicio"] = "17:18:30",
		["frags"] = {
		},
		["data_fim"] = "17:28:22",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 236.995999999985,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:24:25",
			}, -- [1]
			{
				["elapsed"] = 66.8699999999954,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:22:39",
			}, -- [2]
			{
				["elapsed"] = 41.4089999999851,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:21:37",
			}, -- [3]
			{
				["elapsed"] = 20.7049999999581,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:21:02",
			}, -- [4]
			{
				["elapsed"] = 115.252999999968,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:18:30",
			}, -- [5]
		},
		["start_time"] = 418093.464,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["totals_grupo"] = {
			4995090.021791, -- [1]
			40675.013815, -- [2]
			{
				0, -- [1]
				[0] = 868.024757,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
	},
	["force_font_outline"] = "",
	["SoloTablesSaved"] = {
		["LastSelected"] = "DETAILS_PLUGIN_DAMAGE_RANK",
		["Mode"] = 1,
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09CB7B20"] = 260,
	},
}
