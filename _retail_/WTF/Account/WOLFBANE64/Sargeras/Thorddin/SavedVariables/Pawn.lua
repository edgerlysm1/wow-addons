
PawnOptions = {
	["LastVersion"] = 2.0406,
	["LastPlayerFullName"] = "Thorddin-Sargeras",
	["AutoSelectScales"] = false,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 167778,
			["Level"] = 71,
			["Link"] = "|cffa335ee|Hitem:167778::::::::50:70:::6:6296:6300:1472:4786:5414:4775::::|h[Zanj'ir Scaleguard Faceguard]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 112,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::50:70::11:4:4932:4933:6316:1526::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 158028,
			["Level"] = 83,
			["Link"] = "|cff0070dd|Hitem:158028::::::::50:70::26:4:1492:4785:6259:4775::::|h[Bleakweald Pauldrons]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 157990,
			["Level"] = 83,
			["Link"] = "|cff0070dd|Hitem:157990::::::::50:70::25:4:1492:4785:6257:4775::::|h[Harbormaster Cuirass]|h|r",
		}, -- [5]
		{
			["ID"] = 183573,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183573::::::::50:70::14::1:28:1401:::|h[Argent Conscript's Buckle]|h|r",
		}, -- [6]
		{
			["ID"] = 169811,
			["Level"] = 72,
			["Link"] = "|cffa335ee|Hitem:169811::::::::50:70:::4:6300:6289:1478:4786::::|h[Zanj'ir Scaleguard Legguards]|h|r",
		}, -- [7]
		{
			["ID"] = 183572,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183572::::::::50:70::14::1:28:1401:::|h[Argent Conscript's Greaves]|h|r",
		}, -- [8]
		{
			["ID"] = 169815,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:169815::::::::50:70:::2:6288:6300::::|h[Zanj'ir Scaleguard Vambraces]|h|r",
		}, -- [9]
		{
			["ID"] = 170336,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:170336::::::::50:70:::3:6288:6300:42::::|h[Poen's Deepsea Handguards]|h|r",
		}, -- [10]
		{
			["ID"] = 158161,
			["Level"] = 78,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:158161::::::::50:70::26:6:4803:42:6516:6513:1492:4785::::|h[Spearfisher's Band]|h|r",
		}, -- [11]
		{
			["ID"] = 165679,
			["Level"] = 78,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff0070dd|Hitem:165679::::::::50:70::26:5:4803:6516:6515:1492:4785::::|h[Ornate Elun'dris Ring]|h|r",
		}, -- [12]
		{
			["ID"] = 158164,
			["Level"] = 60,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:158164::::::::50:70::26:4:4803:6513:1474:4785::::|h[Plunderbeard's Flask]|h|r",
		}, -- [13]
		{
			["ID"] = 158219,
			["Level"] = 58,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff1eff00|Hitem:158219::::::::50:70::11:1:4787:1:9:49:::|h[Petula's Locket]|h|r",
		}, -- [14]
		{
			["ID"] = 183551,
			["Level"] = 100,
			["Link"] = "|cff0070dd|Hitem:183551::::::::50:70::14::1:28:1401:::|h[Argent Conscript's Lined Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 165632,
			["Level"] = 115,
			["Link"] = "|cffa335ee|Hitem:165632::::::::50:70::81:3:5844:1527:4786:1:28:1401:::|h[Sentinel's Greatblade]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PALADIN",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
