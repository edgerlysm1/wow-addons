
DetailsTimeLineDB = {
	["max_segments"] = 4,
	["combat_data"] = {
		{
			["date_start"] = "20:50:04",
			["date_end"] = "20:50:57",
			["name"] = "Argus the Unmaker",
			["total_time"] = 186.7649999999994,
		}, -- [1]
		{
			["date_start"] = "20:47:51",
			["date_end"] = "20:49:36",
			["name"] = "Argus the Unmaker",
			["total_time"] = 105.0040000000008,
		}, -- [2]
		{
			["date_start"] = "20:42:59",
			["date_end"] = "20:45:10",
			["name"] = "Aggramar",
			["total_time"] = 131.7299999999959,
		}, -- [3]
		{
			["date_start"] = "20:39:57",
			["date_end"] = "20:41:08",
			["name"] = "The Coven of Shivarra",
			["total_time"] = 71.2870000000039,
		}, -- [4]
	},
	["hide_on_combat"] = false,
	["IndividualSpells"] = {
		{
			[258029] = {
				{
					37.29499999999825, -- [1]
					"Apocalypsis Module", -- [2]
					258029, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[79863] = {
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					79863, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vryku-Stormrage", -- [5]
				}, -- [1]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					79863, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kabloomia-Stormrage", -- [5]
				}, -- [2]
			},
			[257296] = {
				{
					48.90699999999924, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[277524] = {
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					277524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vryku-Stormrage", -- [5]
				}, -- [1]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					277524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purified Healer", -- [5]
				}, -- [2]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					277524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vryku-Stormrage", -- [5]
				}, -- [3]
			},
			[258838] = {
				{
					10.15499999999884, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
				{
					18.80400000000373, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [2]
				{
					28.04899999999907, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [3]
				{
					37.00099999999657, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [4]
				{
					49.91900000000169, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [5]
			},
		}, -- [1]
		{
			[255826] = {
				{
					53.59799999999814, -- [1]
					"Argus the Unmaker", -- [2]
					255826, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[256542] = {
				{
					101.525999999998, -- [1]
					"Argus the Unmaker", -- [2]
					256542, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[248499] = {
				{
					4.940999999998894, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
				{
					15.73999999999796, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [2]
				{
					21.54899999999907, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [3]
				{
					46.2410000000018, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [4]
			},
			[258399] = {
				{
					104.5380000000005, -- [1]
					"Argus the Unmaker", -- [2]
					258399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[257296] = {
				{
					14.73199999999633, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [2]
		{
			[244912] = {
				{
					58.17299999999523, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					63.46399999999994, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					67.50299999999697, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					69.94799999999668, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					69.94799999999668, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					126.5979999999981, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					127.8099999999977, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					129.0190000000002, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					129.8179999999993, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[244901] = {
				{
					32.53800000000047, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.53800000000047, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					93.32600000000093, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					93.32600000000093, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[244693] = {
				{
					12.70899999999529, -- [1]
					"Aggramar", -- [2]
					244693, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Muertio-Alleria", -- [5]
				}, -- [1]
			},
			[245631] = {
				{
					34.54200000000128, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.54200000000128, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					95.32499999999709, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					95.33999999999651, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[247079] = {
				{
					17.2239999999947, -- [1]
					"Aggramar", -- [2]
					247079, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [1]
			},
			[255058] = {
				{
					17.2239999999947, -- [1]
					"Aggramar", -- [2]
					255058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244903] = {
				{
					40.75699999999779, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					41.57299999999668, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					44.39899999999761, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					102.7289999999994, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					103.1270000000004, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					103.5429999999979, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[245458] = {
				{
					24.86699999999837, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					85.34700000000157, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[243431] = {
				{
					47.01399999999558, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					108.3269999999975, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [3]
		{
			[245303] = {
				{
					5.012000000002445, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.75300000000425, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					21.72000000000116, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					27.77500000000146, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					39.93800000000192, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					50.86499999999796, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					56.93600000000151, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					63.02300000000105, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					69.08899999999994, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[245627] = {
				{
					9.89900000000489, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.55600000000413, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[244899] = {
				{
					11.13000000000466, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [1]
				{
					22.06199999999808, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [2]
				{
					33.00500000000466, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [3]
				{
					43.93300000000454, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [4]
				{
					54.87700000000041, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [5]
				{
					65.80099999999948, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [6]
			},
			[252861] = {
				{
					31.84600000000501, -- [1]
					"Asara, Mother of Night", -- [2]
					252861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[250333] = {
				{
					4.5370000000039, -- [1]
					"Diima, Mother of Gloom", -- [2]
					250333, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[250334] = {
				{
					50.7190000000046, -- [1]
					"Thu'raya, Mother of the Cosmos", -- [2]
					250334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[253520] = {
				{
					20.34000000000378, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Owlcapöne-Tichondrius", -- [5]
				}, -- [1]
				{
					20.34000000000378, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [2]
				{
					20.34000000000378, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [3]
				{
					60.44800000000396, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ðb-Nemesis", -- [5]
				}, -- [4]
				{
					60.44800000000396, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pallycon-Saurfang", -- [5]
				}, -- [5]
				{
					60.44800000000396, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [6]
			},
			[246329] = {
				{
					11.83600000000297, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					40.68100000000413, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					70.16700000000128, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [4]
		{
			[243960] = {
				{
					8.070999999996275, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					20.21100000000297, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					29.94399999999587, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[248732] = {
				{
					20.0460000000021, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [1]
				{
					25.54499999999825, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [2]
				{
					29.62399999999616, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Renera-Goldrinn", -- [5]
				}, -- [3]
				{
					32.36800000000221, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [4]
			},
			[243999] = {
				{
					16.15200000000186, -- [1]
					"Varimathras", -- [2]
					243999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
			},
			[244042] = {
				{
					23.84999999999855, -- [1]
					"Varimathras", -- [2]
					244042, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244093] = {
				{
					33.58000000000175, -- [1]
					"Varimathras", -- [2]
					244093, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [5]
		{
			[248214] = {
				{
					13.48999999999796, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					79.10599999999977, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					99.75400000000081, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[243975] = {
				{
					113.1419999999998, -- [1]
					"Varimathras", -- [2]
					243975, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246516] = {
				{
					38.86800000000221, -- [1]
					"Kin'garoth", -- [2]
					246516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246833] = {
				{
					28.62599999999657, -- [1]
					"Kin'garoth", -- [2]
					246833, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ruiner", -- [5]
				}, -- [1]
				{
					103.9649999999965, -- [1]
					"Kin'garoth", -- [2]
					246833, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ruiner", -- [5]
				}, -- [2]
			},
			[249535] = {
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Owlcapöne-Tichondrius", -- [5]
				}, -- [1]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Muertio-Alleria", -- [5]
				}, -- [2]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [3]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [4]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bakïren-Stormrage", -- [5]
				}, -- [5]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vyktur-Gilneas", -- [5]
				}, -- [6]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thranduil-Mannoroth", -- [5]
				}, -- [7]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [8]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [9]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Baresard-Anvilmar", -- [5]
				}, -- [10]
				{
					87.58000000000175, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [11]
				{
					87.59500000000116, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pikomi-Drakkari", -- [5]
				}, -- [12]
				{
					87.59500000000116, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Renera-Goldrinn", -- [5]
				}, -- [13]
				{
					87.59500000000116, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ðb-Nemesis", -- [5]
				}, -- [14]
				{
					87.59500000000116, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yarayarcy-MoonGuard", -- [5]
				}, -- [15]
				{
					87.59500000000116, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [16]
				{
					87.59500000000116, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pallycon-Saurfang", -- [5]
				}, -- [17]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Owlcapöne-Tichondrius", -- [5]
				}, -- [18]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Muertio-Alleria", -- [5]
				}, -- [19]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [20]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [21]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bakïren-Stormrage", -- [5]
				}, -- [22]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vyktur-Gilneas", -- [5]
				}, -- [23]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thranduil-Mannoroth", -- [5]
				}, -- [24]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [25]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [26]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Baresard-Anvilmar", -- [5]
				}, -- [27]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [28]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pikomi-Drakkari", -- [5]
				}, -- [29]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Renera-Goldrinn", -- [5]
				}, -- [30]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ðb-Nemesis", -- [5]
				}, -- [31]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yarayarcy-MoonGuard", -- [5]
				}, -- [32]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [33]
				{
					103.4199999999983, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pallycon-Saurfang", -- [5]
				}, -- [34]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Owlcapöne-Tichondrius", -- [5]
				}, -- [35]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Muertio-Alleria", -- [5]
				}, -- [36]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [37]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [38]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bakïren-Stormrage", -- [5]
				}, -- [39]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vyktur-Gilneas", -- [5]
				}, -- [40]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thranduil-Mannoroth", -- [5]
				}, -- [41]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [42]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [43]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Baresard-Anvilmar", -- [5]
				}, -- [44]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [45]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pikomi-Drakkari", -- [5]
				}, -- [46]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Renera-Goldrinn", -- [5]
				}, -- [47]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ðb-Nemesis", -- [5]
				}, -- [48]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yarayarcy-MoonGuard", -- [5]
				}, -- [49]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [50]
				{
					109.9059999999954, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pallycon-Saurfang", -- [5]
				}, -- [51]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Owlcapöne-Tichondrius", -- [5]
				}, -- [52]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Muertio-Alleria", -- [5]
				}, -- [53]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [54]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [55]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bakïren-Stormrage", -- [5]
				}, -- [56]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vyktur-Gilneas", -- [5]
				}, -- [57]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thranduil-Mannoroth", -- [5]
				}, -- [58]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [59]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [60]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Baresard-Anvilmar", -- [5]
				}, -- [61]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [62]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pikomi-Drakkari", -- [5]
				}, -- [63]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Renera-Goldrinn", -- [5]
				}, -- [64]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ðb-Nemesis", -- [5]
				}, -- [65]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yarayarcy-MoonGuard", -- [5]
				}, -- [66]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [67]
				{
					110.538999999997, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pallycon-Saurfang", -- [5]
				}, -- [68]
			},
			[254919] = {
				{
					7.40400000000227, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					21.97699999999895, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					81.53399999999965, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					96.08599999999569, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					112.1990000000005, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[254926] = {
				{
					15.98500000000058, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					82.74699999999575, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246504] = {
				{
					38.98099999999977, -- [1]
					"Unknown", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.98099999999977, -- [1]
					"Unknown", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.99399999999878, -- [1]
					"Unknown", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[254796] = {
				{
					70.01599999999598, -- [1]
					"Kin'garoth", -- [2]
					254796, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244328] = {
				{
					82.7609999999986, -- [1]
					"Detonation Charge", -- [2]
					244328, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [6]
		{
			[252758] = {
				{
					220.989999999998, -- [1]
					"Garothi Demolisher", -- [2]
					252758, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					220.989999999998, -- [1]
					"Garothi Demolisher", -- [2]
					252758, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [7]
		{
			[250701] = {
				{
					81.75600000000122, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					157.0620000000054, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					166.7799999999988, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					217.073000000004, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					364.4550000000018, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[246753] = {
				{
					41.70400000000518, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					276.5690000000031, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					321.1870000000054, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					411.4390000000058, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[254769] = {
				{
					22.03300000000309, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					40.07900000000518, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					72.45100000000093, -- [1]
					"Fel-Powered Purifier", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					123.0620000000054, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					407.3670000000057, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					427.1890000000058, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[250703] = {
				{
					226.7950000000055, -- [1]
					"Fel-Powered Purifier", -- [2]
					250703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[249934] = {
				{
					109.8670000000057, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					226.560000000005, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					320.487000000001, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					434.6650000000009, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[249148] = {
				{
					61.94800000000396, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					182.9860000000044, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					277.7790000000023, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					384.6920000000027, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[246305] = {
				{
					131.1399999999994, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246301] = {
				{
					409.0040000000008, -- [1]
					"Fel-Infused Destructor", -- [2]
					246301, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					428.3990000000049, -- [1]
					"Fel-Infused Destructor", -- [2]
					246301, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [8]
		{
			[244016] = {
				{
					6.149000000004889, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244016, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
				{
					19.51200000000245, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244016, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [2]
				{
					43.77400000000489, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244016, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [3]
			},
			[244689] = {
				{
					10.46199999999953, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[243983] = {
				{
					19.49599999999919, -- [1]
					"Portal Keeper Hasabel", -- [2]
					243983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244709] = {
				{
					23.49599999999919, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.48800000000483, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					27.49799999999959, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[244000] = {
				{
					29.19000000000233, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244000, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[255805] = {
				{
					43.97000000000116, -- [1]
					"Vulcanar", -- [2]
					255805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [9]
		{
			[253038] = {
				{
					12.77100000000064, -- [1]
					"Felblade Shocktrooper", -- [2]
					253038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244892] = {
				{
					8.199000000000524, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.78399999999965, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					25.2980000000025, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					33.7960000000021, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					42.2980000000025, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[257262] = {
				{
					7.845000000001164, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					7.923000000002503, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					8.319000000003143, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					8.505000000004657, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					8.654999999998836, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					18.32800000000134, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					18.88900000000285, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					19.23600000000442, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					19.23600000000442, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					19.36800000000221, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					27.27200000000448, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					27.86499999999796, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					28.03500000000349, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					28.31300000000192, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					28.6710000000021, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					36.97400000000198, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					37.00400000000082, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					37.08499999999913, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					37.43500000000495, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					38.07300000000396, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
			},
			[244722] = {
				{
					15.16500000000087, -- [1]
					"Admiral Svirax", -- [2]
					244722, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.95599999999831, -- [1]
					"Admiral Svirax", -- [2]
					244722, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[253037] = {
				{
					12.77100000000064, -- [1]
					"Felblade Shocktrooper", -- [2]
					253037, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Renera-Goldrinn", -- [5]
				}, -- [1]
			},
			[254130] = {
				{
					35.82600000000093, -- [1]
					"Screaming Shrike", -- [2]
					254130, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					44.32400000000052, -- [1]
					"Screaming Shrike", -- [2]
					254130, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [10]
		{
			[251445] = {
				{
					10.98799999999756, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [1]
				{
					21.95100000000093, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [2]
			},
			[245098] = {
				{
					10.98799999999756, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
				{
					21.95100000000093, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [2]
			},
		}, -- [11]
		{
			[244969] = {
				{
					24.4230000000025, -- [1]
					"Garothi Worldbreaker", -- [2]
					244969, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					37.80200000000332, -- [1]
					"Garothi Worldbreaker", -- [2]
					244969, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246965] = {
				{
					15.60900000000402, -- [1]
					"Annihilator", -- [2]
					246965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[245237] = {
				{
					18.92200000000594, -- [1]
					"Garothi Worldbreaker", -- [2]
					245237, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.29000000000087, -- [1]
					"Garothi Worldbreaker", -- [2]
					245237, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246848] = {
				{
					18.92200000000594, -- [1]
					"Garothi Worldbreaker", -- [2]
					246848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.29000000000087, -- [1]
					"Garothi Worldbreaker", -- [2]
					246848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[244294] = {
				{
					7.999000000003434, -- [1]
					"Annihilator", -- [2]
					244294, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246897] = {
				{
					28.81300000000192, -- [1]
					"Decimator", -- [2]
					246897, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [12]
		{
		}, -- [13]
		{
			[87654] = {
				{
					32.37900000000082, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.37900000000082, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					96.08800000000338, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[89299] = {
				{
					32.79100000000472, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [1]
				{
					36.43100000000413, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [2]
				{
					46.97000000000116, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [3]
				{
					47.37700000000041, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [4]
				{
					47.37700000000041, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [5]
				{
					47.37700000000041, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [6]
				{
					47.38700000000245, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [7]
				{
					55.49599999999919, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [8]
				{
					55.89900000000489, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [9]
				{
					55.89900000000489, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [10]
				{
					55.89900000000489, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [11]
				{
					55.89900000000489, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [12]
				{
					151.6940000000031, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [13]
				{
					156.5480000000025, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [14]
				{
					156.5480000000025, -- [1]
					"Twilight Whelp", -- [2]
					89299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [15]
			},
			[95855] = {
				{
					0.4809999999997672, -- [1]
					"Sinestra", -- [2]
					95855, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					146.601999999999, -- [1]
					"Sinestra", -- [2]
					95855, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[86227] = {
				{
					13.6359999999986, -- [1]
					"Sinestra", -- [2]
					86227, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90028] = {
				{
					78.08200000000215, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					102.0480000000025, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					125.1949999999997, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					133.7060000000056, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					149.5480000000025, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[87299] = {
				{
					6.118000000002212, -- [1]
					"Sinestra", -- [2]
					87299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87947] = {
				{
					140.734000000004, -- [1]
					"Sinestra", -- [2]
					87947, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Calen", -- [5]
				}, -- [1]
			},
			[90125] = {
				{
					155.4290000000037, -- [1]
					"Sinestra", -- [2]
					90125, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [14]
		{
			[82414] = {
				{
					9.319999999999709, -- [1]
					"Cho'gall", -- [2]
					82414, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[82299] = {
				{
					8.895000000004075, -- [1]
					"Cho'gall", -- [2]
					82299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[82361] = {
				{
					9.319999999999709, -- [1]
					"Cho'gall", -- [2]
					82361, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[93245] = {
				{
					12.16600000000472, -- [1]
					"Fire Elemental", -- [2]
					93245, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[82630] = {
				{
					9.319999999999709, -- [1]
					"Cho'gall", -- [2]
					82630, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[81171] = {
				{
					5.654999999998836, -- [1]
					"Cho'gall", -- [2]
					81171, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[82411] = {
				{
					15.70100000000093, -- [1]
					"Darkened Creation", -- [2]
					82411, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [1]
				{
					18.13700000000245, -- [1]
					"Darkened Creation", -- [2]
					82411, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [2]
				{
					20.56599999999889, -- [1]
					"Darkened Creation", -- [2]
					82411, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [3]
				{
					22.18500000000495, -- [1]
					"Darkened Creation", -- [2]
					82411, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [4]
				{
					24.63200000000506, -- [1]
					"Darkened Creation", -- [2]
					82411, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [5]
				{
					26.36500000000524, -- [1]
					"Darkened Creation", -- [2]
					82411, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [6]
			},
		}, -- [15]
		{
			[93327] = {
				{
					9.239000000001397, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [1]
				{
					15.32300000000396, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [2]
				{
					21.39699999999721, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [3]
				{
					27.48500000000058, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [4]
			},
			[93325] = {
				{
					11.46399999999994, -- [1]
					"Bound Rumbler", -- [2]
					93325, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					24.84399999999732, -- [1]
					"Bound Rumbler", -- [2]
					93325, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[82285] = {
				{
					11.6730000000025, -- [1]
					"Arion", -- [2]
					82285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[84918] = {
				{
					28.71800000000076, -- [1]
					"Elementium Monstrosity", -- [2]
					84918, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [16]
		{
			[86369] = {
				{
					8.88300000000163, -- [1]
					"Theralion", -- [2]
					86369, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					11.30299999999988, -- [1]
					"Theralion", -- [2]
					86369, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[87710] = {
				{
					3.542000000001281, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [1]
				{
					5.959999999999127, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [2]
				{
					9.593999999997322, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [3]
				{
					12.01499999999942, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [4]
				{
					12.9429999999993, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [5]
				{
					12.9429999999993, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [6]
			},
			[87707] = {
				{
					6.886999999995169, -- [1]
					"Chosen Seer", -- [2]
					87707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [1]
			},
			[86360] = {
				{
					0.1, -- [1]
					"Theralion", -- [2]
					86360, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[85575] = {
				{
					12.9429999999993, -- [1]
					"Twilight Shadow Mender", -- [2]
					85575, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					12.9429999999993, -- [1]
					"Twilight Shadow Mender", -- [2]
					85575, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					12.9429999999993, -- [1]
					"Twilight Shadow Mender", -- [2]
					85575, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					12.9429999999993, -- [1]
					"Twilight Shadow Mender", -- [2]
					85575, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					12.9429999999993, -- [1]
					"Twilight Shadow Mender", -- [2]
					85575, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [17]
		{
			[86058] = {
				{
					2.451000000000931, -- [1]
					"Proto-Behemoth", -- [2]
					86058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					4.889000000002852, -- [1]
					"Proto-Behemoth", -- [2]
					86058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					7.315000000002328, -- [1]
					"Proto-Behemoth", -- [2]
					86058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[83703] = {
				{
					14.59200000000419, -- [1]
					"Halfus Wyrmbreaker", -- [2]
					83703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[83707] = {
				{
					10.25200000000041, -- [1]
					"Proto-Behemoth", -- [2]
					83707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[83710] = {
				{
					8.53399999999965, -- [1]
					"Halfus Wyrmbreaker", -- [2]
					83710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.97300000000541, -- [1]
					"Halfus Wyrmbreaker", -- [2]
					83710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					13.40699999999924, -- [1]
					"Halfus Wyrmbreaker", -- [2]
					83710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[87710] = {
				{
					18.27300000000105, -- [1]
					"Chosen Seer", -- [2]
					87710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorddin", -- [5]
				}, -- [1]
			},
		}, -- [18]
	},
	["useicons"] = false,
	["deaths_data"] = {
		{
			["Thorddin"] = {
				{
					["time"] = 28.08899999999994,
					["events"] = {
						{
							true, -- [1]
							258838, -- [2]
							61712, -- [3]
							1574301032.449, -- [4]
							1, -- [5]
							"Argus the Unmaker", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							11812, -- [10]
						}, -- [1]
					},
				}, -- [1]
			},
		}, -- [1]
		{
			["Owlcapöne-Tichondrius"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Pallycon-Saurfang"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Vyktur-Gilneas"] = {
				{
					["time"] = 19.03699999999662,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							10558, -- [3]
							1574300889.021, -- [4]
							11918, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							11732, -- [3]
							1574300889.517, -- [4]
							186, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							9676, -- [3]
							1574300890.063, -- [4]
							1, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							9490, -- [10]
						}, -- [3]
					},
				}, -- [1]
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [2]
			},
			["Bellak-Stormrage"] = {
				{
					["time"] = 37.17699999999604,
					["events"] = {
						{
							true, -- [1]
							255646, -- [2]
							15816, -- [3]
							1574300907.492, -- [4]
							35527, -- [5]
							"Golganneth", -- [6]
							nil, -- [7]
							8, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							255646, -- [2]
							15816, -- [3]
							1574300907.731, -- [4]
							19711, -- [5]
							"Golganneth", -- [6]
							nil, -- [7]
							8, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							255646, -- [2]
							15816, -- [3]
							1574300907.984, -- [4]
							3895, -- [5]
							"Golganneth", -- [6]
							nil, -- [7]
							8, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [2]
			},
			["Renera-Goldrinn"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Syllith-Alexstrasza"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							22809, -- [3]
							1574300972.309, -- [4]
							266706, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							22808, -- [3]
							1574300972.82, -- [4]
							255338, -- [5]
							"[*] Unleashed Rage", -- [6]
							11440, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							22808, -- [3]
							1574300973.326, -- [4]
							238428, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Pikomi-Drakkari"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Ðb-Nemesis"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							5953, -- [3]
							1574300974.59, -- [4]
							313594, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							5954, -- [3]
							1574300975.095, -- [4]
							307640, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							5954, -- [3]
							1574300975.592, -- [4]
							307640, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Necrotos-Stormrage"] = {
				{
					["time"] = 83.05399999999645,
					["events"] = {
						{
							true, -- [1]
							1, -- [2]
							9179, -- [3]
							1574300953.272, -- [4]
							33922, -- [5]
							"Argus the Unmaker", -- [6]
							2753, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							41024, -- [3]
							1574300953.563, -- [4]
							8379, -- [5]
							"[*] Unleashed Rage", -- [6]
							12307, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							42267, -- [3]
							1574300954.059, -- [4]
							2, -- [5]
							"[*] Unleashed Rage", -- [6]
							25508, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [2]
			},
			["Greatgamer-Drakkari"] = {
				{
					["time"] = 80.98799999999756,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							58086, -- [3]
							1574300951.023, -- [4]
							91192, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							58086, -- [3]
							1574300951.541, -- [4]
							33106, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							59271, -- [3]
							1574300952.042, -- [4]
							1, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							17783, -- [10]
						}, -- [3]
					},
				}, -- [1]
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [2]
			},
			["Thranduil-Mannoroth"] = {
				{
					["time"] = 102.0709999999963,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							24012, -- [3]
							1574300972.079, -- [4]
							24378, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							24012, -- [3]
							1574300972.58, -- [4]
							366, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							24011, -- [3]
							1574300973.112, -- [4]
							1, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							23645, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Temajin-Terenas"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Thorddin"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Muertio-Alleria"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
			["Baresard-Anvilmar"] = {
				{
					["time"] = 104.5380000000005,
					["events"] = {
						{
							true, -- [1]
							257930, -- [2]
							7524, -- [3]
							1574300974.56, -- [4]
							3860, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257930, -- [2]
							7524, -- [3]
							1574300975.065, -- [4]
							3332, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257930, -- [2]
							7524, -- [3]
							1574300975.58, -- [4]
							1, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							4192, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Yarayarcy-MoonGuard"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
						{
							true, -- [1]
							257930, -- [2]
							9558, -- [3]
							1574300974.57, -- [4]
							257305, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257930, -- [2]
							9558, -- [3]
							1574300975.075, -- [4]
							250049, -- [5]
							"[*] Crushing Fear", -- [6]
							1924, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257930, -- [2]
							9558, -- [3]
							1574300975.592, -- [4]
							240491, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Bakïren-Stormrage"] = {
				{
					["time"] = 40.49799999999959,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							38395, -- [3]
							1574300910.529, -- [4]
							35282, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257911, -- [2]
							38394, -- [3]
							1574300911.024, -- [4]
							3387, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							38394, -- [3]
							1574300911.552, -- [4]
							1, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							35007, -- [10]
						}, -- [3]
					},
				}, -- [1]
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [2]
			},
			["Broslav-Lightbringer"] = {
				{
					["time"] = 104.737000000001,
					["events"] = {
					},
				}, -- [1]
			},
		}, -- [2]
		{
			["Pikomi-Drakkari"] = {
				{
					["time"] = 69.98599999999715,
					["events"] = {
						{
							true, -- [1]
							244912, -- [2]
							2781, -- [3]
							1574300647.711, -- [4]
							24308, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							244912, -- [2]
							14794, -- [3]
							1574300648.464, -- [4]
							11527, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							244912, -- [2]
							14655, -- [3]
							1574300648.491, -- [4]
							1, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							3128, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Pallycon-Saurfang"] = {
				{
					["time"] = 19.91300000000047,
					["events"] = {
						{
							true, -- [1]
							254452, -- [2]
							1465, -- [3]
							1574300591.064, -- [4]
							205308, -- [5]
							"Aggramar", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							254452, -- [2]
							1465, -- [3]
							1574300591.564, -- [4]
							205308, -- [5]
							"Aggramar", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							247079, -- [2]
							16436, -- [3]
							1574300595.745, -- [4]
							221841, -- [5]
							"Aggramar", -- [6]
							14537, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Broslav-Lightbringer"] = {
				{
					["time"] = 19.79599999999482,
					["events"] = {
						{
							true, -- [1]
							254455, -- [2]
							26068, -- [3]
							1574300591.58, -- [4]
							576800, -- [5]
							"[*] Ravenous Blaze", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							1, -- [2]
							5134, -- [3]
							1574300592.851, -- [4]
							576800, -- [5]
							"Aggramar", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							247079, -- [2]
							15439, -- [3]
							1574300595.745, -- [4]
							576800, -- [5]
							"Aggramar", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Muertio-Alleria"] = {
				{
					["time"] = 24.92899999999645,
					["events"] = {
						{
							true, -- [1]
							244291, -- [2]
							111019, -- [3]
							1574300603.393, -- [4]
							1, -- [5]
							"Aggramar", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							35759, -- [10]
						}, -- [1]
					},
				}, -- [1]
			},
			["Thorddin"] = {
				{
					["time"] = 24.92899999999645,
					["events"] = {
						{
							true, -- [1]
							244291, -- [2]
							128772, -- [3]
							1574300603.411, -- [4]
							1, -- [5]
							"Aggramar", -- [6]
							5213, -- [7]
							1, -- [8]
							false, -- [9]
							78179, -- [10]
						}, -- [1]
					},
				}, -- [1]
				{
					["time"] = 70.17199999999866,
					["events"] = {
						{
							true, -- [1]
							244912, -- [2]
							17868, -- [3]
							1574300648.464, -- [4]
							19552, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							244912, -- [2]
							17711, -- [3]
							1574300648.491, -- [4]
							1841, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							254452, -- [2]
							2059, -- [3]
							1574300648.677, -- [4]
							1, -- [5]
							"Aggramar", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							218, -- [10]
						}, -- [3]
					},
				}, -- [2]
			},
			["Vyktur-Gilneas"] = {
				{
					["time"] = 73.18499999999767,
					["events"] = {
						{
							true, -- [1]
							244912, -- [2]
							3248, -- [3]
							1574300650.705, -- [4]
							39172, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							254455, -- [2]
							21443, -- [3]
							1574300651.66, -- [4]
							17729, -- [5]
							"[*] Ravenous Blaze", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							254455, -- [2]
							21330, -- [3]
							1574300651.69, -- [4]
							1, -- [5]
							"[*] Ravenous Blaze", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							3601, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
		}, -- [3]
		{
		}, -- [4]
	},
	["backdrop_color"] = {
		0, -- [1]
		0, -- [2]
		0, -- [3]
		0.4, -- [4]
	},
	["cooldowns_timeline"] = {
		{
			["Owlcapöne-Tichondrius"] = {
				{
					36.74700000000303, -- [1]
					"Owlcapöne-Tichondrius", -- [2]
					22812, -- [3]
				}, -- [1]
			},
			["Bakïren-Stormrage"] = {
				{
					46.16599999999744, -- [1]
					"Argus the Unmaker", -- [2]
					122470, -- [3]
				}, -- [1]
			},
			["Necrotos-Stormrage"] = {
				{
					10.46500000000378, -- [1]
					"Necrotos-Stormrage", -- [2]
					55233, -- [3]
				}, -- [1]
			},
			["Temajin-Terenas"] = {
				{
					4.673000000002503, -- [1]
					"Temajin-Terenas", -- [2]
					184662, -- [3]
				}, -- [1]
				{
					37.07100000000355, -- [1]
					"Temajin-Terenas", -- [2]
					642, -- [3]
				}, -- [2]
			},
			["Vyktur-Gilneas"] = {
				{
					31.43600000000151, -- [1]
					"Vyktur-Gilneas", -- [2]
					48792, -- [3]
				}, -- [1]
			},
			["Thorddin"] = {
				{
					20.96399999999994, -- [1]
					"Thorddin", -- [2]
					642, -- [3]
				}, -- [1]
			},
			["Renera-Goldrinn"] = {
				{
					34.76299999999901, -- [1]
					"[*] raid wide cooldown", -- [2]
					265202, -- [3]
				}, -- [1]
				{
					38.18800000000192, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [2]
				{
					39.50600000000122, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [3]
				{
					40.82800000000134, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [4]
				{
					42.1449999999968, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [5]
				{
					43.46800000000076, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [6]
			},
			["Yarayarcy-MoonGuard"] = {
				{
					21.92199999999866, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					105809, -- [3]
				}, -- [1]
			},
			["Syllith-Alexstrasza"] = {
				{
					30.07400000000052, -- [1]
					"Syllith-Alexstrasza", -- [2]
					31850, -- [3]
				}, -- [1]
			},
			["Bellak-Stormrage"] = {
				{
					42.0480000000025, -- [1]
					"[*] raid wide cooldown", -- [2]
					265202, -- [3]
				}, -- [1]
			},
		}, -- [1]
		{
			["Temajin-Terenas"] = {
				{
					0.8929999999963911, -- [1]
					"Temajin-Terenas", -- [2]
					184662, -- [3]
				}, -- [1]
			},
			["Pikomi-Drakkari"] = {
				{
					10.81699999999546, -- [1]
					"Pikomi-Drakkari", -- [2]
					187827, -- [3]
				}, -- [1]
			},
			["Thranduil-Mannoroth"] = {
				{
					84.86499999999796, -- [1]
					"[*] raid wide cooldown", -- [2]
					97462, -- [3]
				}, -- [1]
				{
					85.12599999999657, -- [1]
					"Thranduil-Mannoroth", -- [2]
					184364, -- [3]
				}, -- [2]
			},
			["Bellak-Stormrage"] = {
				{
					34.09100000000035, -- [1]
					"Bakïren-Stormrage", -- [2]
					47788, -- [3]
				}, -- [1]
			},
			["Yarayarcy-MoonGuard"] = {
				{
					2.385999999998603, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					105809, -- [3]
				}, -- [1]
				{
					18.69599999999628, -- [1]
					"[*] raid wide cooldown", -- [2]
					31821, -- [3]
				}, -- [2]
				{
					24.41799999999785, -- [1]
					"Bakïren-Stormrage", -- [2]
					6940, -- [3]
				}, -- [3]
				{
					25.77300000000105, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					498, -- [3]
				}, -- [4]
				{
					53.06999999999971, -- [1]
					"Greatgamer-Drakkari", -- [2]
					633, -- [3]
				}, -- [5]
			},
			["Renera-Goldrinn"] = {
				{
					53.22299999999814, -- [1]
					"Greatgamer-Drakkari", -- [2]
					47788, -- [3]
				}, -- [1]
			},
			["Necrotos-Stormrage"] = {
				{
					8.091999999996915, -- [1]
					"Necrotos-Stormrage", -- [2]
					55233, -- [3]
				}, -- [1]
			},
			["Bakïren-Stormrage"] = {
				{
					24.33699999999953, -- [1]
					"Argus the Unmaker", -- [2]
					122470, -- [3]
				}, -- [1]
			},
		}, -- [2]
		{
			["Syllith-Alexstrasza"] = {
				{
					70.66899999999441, -- [1]
					"Syllith-Alexstrasza", -- [2]
					31850, -- [3]
				}, -- [1]
			},
			["Pikomi-Drakkari"] = {
				{
					29.43099999999686, -- [1]
					"Pikomi-Drakkari", -- [2]
					187827, -- [3]
				}, -- [1]
			},
			["Necrotos-Stormrage"] = {
				{
					3.822999999996682, -- [1]
					"Necrotos-Stormrage", -- [2]
					55233, -- [3]
				}, -- [1]
			},
			["Temajin-Terenas"] = {
				{
					8.720000000001164, -- [1]
					"Temajin-Terenas", -- [2]
					184662, -- [3]
				}, -- [1]
				{
					111.0780000000013, -- [1]
					"Temajin-Terenas", -- [2]
					184662, -- [3]
				}, -- [2]
			},
			["Thranduil-Mannoroth"] = {
				{
					45.21199999999953, -- [1]
					"[*] raid wide cooldown", -- [2]
					97462, -- [3]
				}, -- [1]
			},
			["Bakïren-Stormrage"] = {
				{
					32.79000000000087, -- [1]
					"Aggramar", -- [2]
					122470, -- [3]
				}, -- [1]
			},
			["Yarayarcy-MoonGuard"] = {
				{
					6.461999999999534, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					105809, -- [3]
				}, -- [1]
				{
					65.60699999999633, -- [1]
					"[*] raid wide cooldown", -- [2]
					31821, -- [3]
				}, -- [2]
				{
					74.2390000000014, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					498, -- [3]
				}, -- [3]
				{
					120.3149999999951, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					105809, -- [3]
				}, -- [4]
			},
			["Bellak-Stormrage"] = {
				{
					75.46199999999953, -- [1]
					"[*] raid wide cooldown", -- [2]
					265202, -- [3]
				}, -- [1]
			},
			["Renera-Goldrinn"] = {
				{
					77.04200000000128, -- [1]
					"[*] raid wide cooldown", -- [2]
					265202, -- [3]
				}, -- [1]
				{
					79.83299999999872, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [2]
				{
					81.45899999999529, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [3]
				{
					83.06699999999546, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [4]
				{
					84.67799999999988, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [5]
				{
					86.29799999999523, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [6]
			},
		}, -- [3]
		{
			["Pikomi-Drakkari"] = {
				{
					14.12600000000384, -- [1]
					"Pikomi-Drakkari", -- [2]
					187827, -- [3]
				}, -- [1]
			},
			["Vyktur-Gilneas"] = {
				{
					46.125, -- [1]
					"Vyktur-Gilneas", -- [2]
					48792, -- [3]
				}, -- [1]
			},
			["Bellak-Stormrage"] = {
				{
					61.61899999999878, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [1]
				{
					63.37800000000425, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [2]
				{
					65.12600000000384, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [3]
				{
					66.89300000000367, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [4]
				{
					68.65899999999965, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [5]
			},
			["Temajin-Terenas"] = {
				{
					6.069999999999709, -- [1]
					"Temajin-Terenas", -- [2]
					184662, -- [3]
				}, -- [1]
			},
			["Yarayarcy-MoonGuard"] = {
				{
					10.37800000000425, -- [1]
					"Yarayarcy-MoonGuard", -- [2]
					105809, -- [3]
				}, -- [1]
				{
					35.22800000000279, -- [1]
					"[*] raid wide cooldown", -- [2]
					31821, -- [3]
				}, -- [2]
			},
			["Necrotos-Stormrage"] = {
				{
					14.69400000000314, -- [1]
					"Necrotos-Stormrage", -- [2]
					55233, -- [3]
				}, -- [1]
			},
			["Bakïren-Stormrage"] = {
				{
					14.46100000000297, -- [1]
					"Asara, Mother of Night", -- [2]
					122470, -- [3]
				}, -- [1]
			},
		}, -- [4]
	},
	["debuff_timeline"] = {
		{
			["Owlcapöne-Tichondrius"] = {
				[258834] = {
					25.93699999999808, -- [1]
					37.94499999999971, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[257869] = {
					28.90600000000268, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Rage",
					["active"] = false,
				},
			},
			["Pallycon-Saurfang"] = {
				[258838] = {
					18.82200000000012, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Vryku-Stormrage"] = {
				[79863] = {
					186.7649999999994, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Shadoweye Scout",
					["active"] = false,
				},
			},
			["Vyktur-Gilneas"] = {
				[258838] = {
					28.08899999999994, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257869] = {
					28.90600000000268, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Rage",
					["active"] = false,
				},
			},
			["Bellak-Stormrage"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					20.92799999999988, -- [1]
					32.93899999999849, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Renera-Goldrinn"] = {
				[258834] = {
					20.92799999999988, -- [1]
					32.95799999999872, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[258838] = {
					37.03300000000309, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Kabloomia-Stormrage"] = {
				[79863] = {
					186.7649999999994, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Shadoweye Scout",
					["active"] = false,
				},
			},
			["Syllith-Alexstrasza"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					20.92799999999988, -- [1]
					37.94499999999971, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Pikomi-Drakkari"] = {
				[258838] = {
					18.82200000000012, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					25.93699999999808, -- [1]
					37.94499999999971, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Ðb-Nemesis"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257931] = {
					28.90600000000268, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
			},
			["Necrotos-Stormrage"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					25.93699999999808, -- [1]
					37.94499999999971, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Greatgamer-Drakkari"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Thorddin"] = {
				[258838] = {
					18.82200000000012, -- [1]
					28.06199999999808, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Temajin-Terenas"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					20.92799999999988, -- [1]
					32.93899999999849, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Thranduil-Mannoroth"] = {
				[258838] = {
					18.82200000000012, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257931] = {
					28.90600000000268, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
				[257930] = {
					43.15499999999884, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Crushing Fear",
					["active"] = false,
				},
			},
			["Muertio-Alleria"] = {
				[258838] = {
					28.08899999999994, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					52.17199999999866, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Baresard-Anvilmar"] = {
				[258838] = {
					28.08899999999994, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Bakïren-Stormrage"] = {
				[258838] = {
					10.18000000000029, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Yarayarcy-MoonGuard"] = {
				[258838] = {
					37.03300000000309, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Broslav-Lightbringer"] = {
				[258838] = {
					37.03300000000309, -- [1]
					186.7649999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
		}, -- [1]
		{
			["Owlcapöne-Tichondrius"] = {
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					46.26000000000204, -- [3]
					61.25499999999738, -- [4]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Pallycon-Saurfang"] = {
				[257911] = {
					11.46899999999732, -- [1]
					42.23300000000018, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[253903] = {
					29.46699999999692, -- [1]
					59.47200000000157, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sky",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
			},
			["Vyktur-Gilneas"] = {
				[257911] = {
					11.46899999999732, -- [1]
					18.99399999999878, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[257931] = {
					10.69799999999668, -- [1]
					18.99399999999878, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
				[258647] = {
					15.52799999999843, -- [1]
					18.99399999999878, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Gift of the Sea",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
			},
			["Thranduil-Mannoroth"] = {
				[257869] = {
					10.69799999999668, -- [1]
					44.93000000000029, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Rage",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
				[253901] = {
					22.10800000000018, -- [1]
					52.11699999999837, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sea",
					["active"] = false,
				},
				[257911] = {
					65.52100000000064, -- [1]
					102.0460000000021, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Yarayarcy-MoonGuard"] = {
				[257931] = {
					57.25, -- [1]
					93.16599999999744, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
				[257930] = {
					57.98799999999756, -- [1]
					104.5699999999997, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Crushing Fear",
					["active"] = false,
				},
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					46.26000000000204, -- [3]
					61.25499999999738, -- [4]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Syllith-Alexstrasza"] = {
				[257911] = {
					58.76399999999558, -- [1]
					102.2719999999972, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					4.940999999998894, -- [1]
					30.74300000000221, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
			},
			["Pikomi-Drakkari"] = {
				[253903] = {
					23.66199999999662, -- [1]
					53.66300000000047, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sky",
					["active"] = false,
				},
				[248499] = {
					46.26000000000204, -- [1]
					61.25499999999738, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					11.46899999999732, -- [1]
					41.47799999999552, -- [2]
					58.0089999999982, -- [3]
					88.76599999999598, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Ðb-Nemesis"] = {
				[257911] = {
					11.46899999999732, -- [1]
					45.22499999999855, -- [2]
					58.0089999999982, -- [3]
					89.52500000000146, -- [4]
					89.53499999999622, -- [5]
					104.5899999999965, -- [6]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					46.26000000000204, -- [3]
					61.25499999999738, -- [4]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Necrotos-Stormrage"] = {
				[255199] = {
					49.92399999999907, -- [1]
					78.03299999999581, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Avatar of Aggramar",
					["active"] = false,
				},
				[248499] = {
					4.940999999998894, -- [1]
					19.95500000000175, -- [2]
					21.57600000000093, -- [3]
					36.58899999999994, -- [4]
					46.2410000000018, -- [5]
					61.25499999999738, -- [6]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					58.0089999999982, -- [1]
					83.04000000000087, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
			},
			["Greatgamer-Drakkari"] = {
				[257911] = {
					11.46899999999732, -- [1]
					80.98799999999756, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					46.26000000000204, -- [1]
					61.25499999999738, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[253901] = {
					26.96800000000076, -- [1]
					56.96800000000076, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sea",
					["active"] = false,
				},
			},
			["Temajin-Terenas"] = {
				[257911] = {
					11.46899999999732, -- [1]
					42.23300000000018, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[253903] = {
					25.18899999999849, -- [1]
					55.19599999999628, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sky",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
			},
			["Thorddin"] = {
				[257911] = {
					11.46899999999732, -- [1]
					51.96399999999994, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[253903] = {
					23.6339999999982, -- [1]
					53.66300000000047, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sky",
					["active"] = false,
				},
			},
			["Broslav-Lightbringer"] = {
				[258646] = {
					15.52799999999843, -- [1]
					20.53899999999703, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Gift of the Sky",
					["active"] = false,
				},
				[253903] = {
					23.55199999999604, -- [1]
					53.5679999999993, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sky",
					["active"] = false,
				},
			},
			["Baresard-Anvilmar"] = {
				[257911] = {
					11.46899999999732, -- [1]
					41.47799999999552, -- [2]
					58.0089999999982, -- [3]
					99.28499999999622, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					46.26000000000204, -- [3]
					61.25499999999738, -- [4]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257930] = {
					59.49899999999616, -- [1]
					104.5109999999986, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Crushing Fear",
					["active"] = false,
				},
				[257931] = {
					57.25, -- [1]
					93.16599999999744, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
			},
			["Muertio-Alleria"] = {
				[257911] = {
					18.22699999999895, -- [1]
					50.48099999999977, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[248499] = {
					21.57600000000093, -- [1]
					36.58899999999994, -- [2]
					46.26000000000204, -- [3]
					61.25499999999738, -- [4]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257869] = {
					57.23999999999796, -- [1]
					93.16599999999744, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Rage",
					["active"] = false,
				},
			},
			["Bakïren-Stormrage"] = {
				[257911] = {
					11.46899999999732, -- [1]
					40.49799999999959, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Renera-Goldrinn"] = {
				[248499] = {
					46.26000000000204, -- [1]
					61.25499999999738, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[256899] = {
					105.0040000000008, -- [1]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = true,
				},
			},
		}, -- [2]
		{
			["Owlcapöne-Tichondrius"] = {
				[254452] = {
					5.050999999999476, -- [1]
					13.05900000000111, -- [2]
					69.13299999999435, -- [3]
					77.14400000000023, -- [4]
					129.4739999999947, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[244291] = {
					85.34700000000157, -- [1]
					115.3600000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					44.77500000000146, -- [1]
					45.36999999999534, -- [2]
					47.93799999999465, -- [3]
					49.31999999999971, -- [4]
					71.05599999999686, -- [5]
					71.84799999999814, -- [6]
					93.2719999999972, -- [7]
					93.87900000000081, -- [8]
					126.2249999999985, -- [9]
					130.6650000000009, -- [10]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					96.78499999999622, -- [1]
					108.8070000000007, -- [2]
					124.7769999999946, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Pallycon-Saurfang"] = {
				[254452] = {
					5.050999999999476, -- [1]
					13.05900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[247079] = {
					17.23999999999796, -- [1]
					19.89099999999598, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Vyktur-Gilneas"] = {
				[245916] = {
					51.11599999999453, -- [1]
					51.66599999999744, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					73.16899999999441, -- [2]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Thranduil-Mannoroth"] = {
				[244291] = {
					24.88799999999901, -- [1]
					54.89199999999983, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					41.91500000000087, -- [1]
					41.94599999999628, -- [2]
					41.99699999999575, -- [3]
					42.05399999999645, -- [4]
					42.05399999999645, -- [5]
					43.47200000000157, -- [6]
					93.07999999999447, -- [7]
					93.82299999999668, -- [8]
					100.5570000000007, -- [9]
					101.3379999999961, -- [10]
					127.7939999999944, -- [11]
					127.8229999999967, -- [12]
					129.0629999999946, -- [13]
					131.7299999999959, -- [14]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					14.64699999999721, -- [1]
					26.66100000000006, -- [2]
					106.3709999999992, -- [3]
					118.3869999999952, -- [4]
					122.9130000000005, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Renera-Goldrinn"] = {
				[254452] = {
					5.050999999999476, -- [1]
					13.05900000000111, -- [2]
					69.13299999999435, -- [3]
					77.14400000000023, -- [4]
					129.4739999999947, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[244291] = {
					24.92899999999645, -- [1]
					54.92899999999645, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					119.6939999999959, -- [1]
					119.9599999999991, -- [2]
					126.2249999999985, -- [3]
					127.400999999998, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					109.6419999999998, -- [1]
					121.6359999999986, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Syllith-Alexstrasza"] = {
				[244291] = {
					24.9059999999954, -- [1]
					54.91700000000128, -- [2]
					85.34700000000157, -- [3]
					115.3600000000006, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					39.25400000000082, -- [1]
					40.78199999999924, -- [2]
					47.54499999999825, -- [3]
					49.31999999999971, -- [4]
					53.97299999999814, -- [5]
					54.40199999999459, -- [6]
					75.16100000000006, -- [7]
					76.0879999999961, -- [8]
					78.04299999999785, -- [9]
					80.51599999999598, -- [10]
					113.5210000000006, -- [11]
					114.2019999999975, -- [12]
					125.3960000000006, -- [13]
					126.4559999999983, -- [14]
					128.9110000000001, -- [15]
					130.3189999999959, -- [16]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[245990] = {
					48.47299999999814, -- [1]
					75.21499999999651, -- [2]
					78.56399999999849, -- [3]
					93.58599999999569, -- [4]
					110.0899999999965, -- [5]
					128.0970000000016, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Pikomi-Drakkari"] = {
				[244291] = {
					24.9059999999954, -- [1]
					54.91700000000128, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					50.67499999999563, -- [1]
					52.61299999999756, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					69.98599999999715, -- [2]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Ðb-Nemesis"] = {
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					50.18299999999726, -- [1]
					52.72099999999773, -- [2]
					127.5930000000008, -- [3]
					127.8229999999967, -- [4]
					128.6919999999955, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					106.1329999999944, -- [1]
					118.1319999999978, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[254452] = {
					69.15099999999802, -- [1]
					77.15799999999581, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Necrotos-Stormrage"] = {
				[244736] = {
					14.64699999999721, -- [1]
					26.65000000000146, -- [2]
					106.1909999999989, -- [3]
					118.1939999999959, -- [4]
					123.7739999999976, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245990] = {
					1.417999999997846, -- [1]
					22.45499999999447, -- [2]
					55.76499999999942, -- [3]
					88.56199999999808, -- [4]
					118.9320000000007, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244291] = {
					24.86699999999837, -- [1]
					54.87999999999738, -- [2]
					85.34700000000157, -- [3]
					115.3600000000006, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					48.39699999999721, -- [1]
					53.58599999999569, -- [2]
					53.60299999999552, -- [3]
					56.97999999999593, -- [4]
					57.35499999999593, -- [5]
					57.78399999999965, -- [6]
					57.9629999999961, -- [7]
					59.65299999999843, -- [8]
					61.10299999999552, -- [9]
					62.11099999999715, -- [10]
					67.30999999999767, -- [11]
					67.5309999999954, -- [12]
					74.60299999999552, -- [13]
					76.96800000000076, -- [14]
					77.77500000000146, -- [15]
					79.50099999999657, -- [16]
					81.52699999999459, -- [17]
					81.67699999999604, -- [18]
					109.2809999999954, -- [19]
					112.2180000000008, -- [20]
					114.2019999999975, -- [21]
					115.2479999999996, -- [22]
					117.4619999999995, -- [23]
					119.0649999999951, -- [24]
					119.6939999999959, -- [25]
					119.9599999999991, -- [26]
					121.6619999999966, -- [27]
					123.2059999999983, -- [28]
					124.4229999999952, -- [29]
					125.7419999999984, -- [30]
					130.7719999999972, -- [31]
					131.7299999999959, -- [32]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Greatgamer-Drakkari"] = {
				[244291] = {
					24.9059999999954, -- [1]
					54.91700000000128, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					44.6359999999986, -- [1]
					45.02599999999802, -- [2]
					47.33899999999994, -- [3]
					48.6339999999982, -- [4]
					49.10800000000018, -- [5]
					50.59700000000157, -- [6]
					50.90399999999499, -- [7]
					51.3859999999986, -- [8]
					92.99399999999878, -- [9]
					95.50999999999476, -- [10]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Temajin-Terenas"] = {
				[254452] = {
					5.050999999999476, -- [1]
					13.05900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.97899999999936, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[244291] = {
					24.9059999999954, -- [1]
					54.90499999999884, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					46.71099999999569, -- [1]
					48.78299999999581, -- [2]
					68.07600000000093, -- [3]
					70.6140000000014, -- [4]
					92.4629999999961, -- [5]
					94.17099999999482, -- [6]
					99.81699999999546, -- [7]
					100.0420000000013, -- [8]
					103.5429999999979, -- [9]
					104.4979999999996, -- [10]
					106.5360000000001, -- [11]
					108.4809999999998, -- [12]
					108.4809999999998, -- [13]
					111.0489999999991, -- [14]
					111.0619999999981, -- [15]
					111.7099999999991, -- [16]
					122.4429999999993, -- [17]
					124.1080000000002, -- [18]
					127.7939999999944, -- [19]
					127.8229999999967, -- [20]
					128.7339999999967, -- [21]
					130.3189999999959, -- [22]
					130.3329999999987, -- [23]
					131.7299999999959, -- [24]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[247079] = {
					17.23999999999796, -- [1]
					47.23999999999796, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244736] = {
					93.88999999999942, -- [1]
					105.887999999999, -- [2]
					123.2200000000012, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Bellak-Stormrage"] = {
				[244291] = {
					24.88799999999901, -- [1]
					54.89199999999983, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					115.2479999999996, -- [1]
					116.9929999999949, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[254452] = {
					5.050999999999476, -- [1]
					13.05900000000111, -- [2]
					69.13299999999435, -- [3]
					77.14400000000023, -- [4]
					129.4739999999947, -- [5]
					131.7299999999959, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.97899999999936, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Broslav-Lightbringer"] = {
				[245990] = {
					14.37999999999738, -- [1]
					19.76599999999598, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[247079] = {
					17.23999999999796, -- [1]
					19.76599999999598, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Baresard-Anvilmar"] = {
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					44.11099999999715, -- [1]
					45.02599999999802, -- [2]
					47.39099999999598, -- [3]
					48.8379999999961, -- [4]
					50.08199999999488, -- [5]
					51.53299999999581, -- [6]
					64.51599999999598, -- [7]
					64.6840000000011, -- [8]
					67.71499999999651, -- [9]
					69.82200000000012, -- [10]
					69.87199999999575, -- [11]
					70.80099999999948, -- [12]
					74.31199999999808, -- [13]
					75.75999999999476, -- [14]
					92.36299999999756, -- [15]
					94.17099999999482, -- [16]
					104.5420000000013, -- [17]
					105.8229999999967, -- [18]
					106.6219999999958, -- [19]
					108.150999999998, -- [20]
					108.6049999999959, -- [21]
					109.7129999999961, -- [22]
					109.7239999999947, -- [23]
					110.2030000000013, -- [24]
					115.4439999999959, -- [25]
					117.2719999999972, -- [26]
					125.7419999999984, -- [27]
					126.9179999999979, -- [28]
					129.9259999999995, -- [29]
					131.2019999999975, -- [30]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[254452] = {
					129.4739999999947, -- [1]
					131.7299999999959, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Yarayarcy-MoonGuard"] = {
				[244736] = {
					94.5879999999961, -- [1]
					110.5930000000008, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.99099999999453, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[244291] = {
					24.92899999999645, -- [1]
					54.92899999999645, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					46.77399999999761, -- [1]
					47.08899999999994, -- [2]
					47.3640000000014, -- [3]
					49.31999999999971, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[247079] = {
					17.23999999999796, -- [1]
					47.25099999999657, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Bakïren-Stormrage"] = {
				[244736] = {
					98.68000000000029, -- [1]
					110.6919999999955, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					58.20599999999831, -- [1]
					84.97899999999936, -- [2]
					126.6129999999976, -- [3]
					131.7299999999959, -- [4]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					48.40899999999965, -- [1]
					49.65399999999499, -- [2]
					50.47099999999773, -- [3]
					52.65699999999924, -- [4]
					52.66700000000128, -- [5]
					54.14699999999721, -- [6]
					93.28299999999581, -- [7]
					95.27500000000146, -- [8]
					106.9890000000014, -- [9]
					110.2149999999965, -- [10]
					110.2259999999951, -- [11]
					111.2779999999984, -- [12]
					111.3059999999969, -- [13]
					111.6979999999967, -- [14]
					127.6080000000002, -- [15]
					127.8229999999967, -- [16]
					129.3099999999977, -- [17]
					130.9099999999962, -- [18]
					130.9099999999962, -- [19]
					131.7299999999959, -- [20]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[247079] = {
					17.23999999999796, -- [1]
					47.25099999999657, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[254452] = {
					129.4739999999947, -- [1]
					131.7299999999959, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Thorddin"] = {
				[244912] = {
					58.20599999999831, -- [1]
					70.14899999999761, -- [2]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[254452] = {
					69.13299999999435, -- [1]
					70.14899999999761, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
		}, -- [3]
		{
			["Owlcapöne-Tichondrius"] = {
				[253753] = {
					14.53800000000047, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253520] = {
					20.35300000000279, -- [1]
					30.36899999999878, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Pallycon-Saurfang"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.747000000003027, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253520] = {
					60.46300000000338, -- [1]
					70.45799999999872, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Vyktur-Gilneas"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.91900000000169, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Bellak-Stormrage"] = {
				[253753] = {
					14.53800000000047, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253520] = {
					20.35300000000279, -- [1]
					30.35300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Yarayarcy-MoonGuard"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.97499999999855, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Syllith-Alexstrasza"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[244899] = {
					11.13000000000466, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Bakïren-Stormrage"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.90499999999884, -- [1]
					46.75400000000082, -- [2]
					47.0109999999986, -- [3]
					48.89600000000064, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Ðb-Nemesis"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.97499999999855, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
				[253520] = {
					60.46300000000338, -- [1]
					70.45799999999872, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
			},
			["Necrotos-Stormrage"] = {
				[253753] = {
					14.53800000000047, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.90499999999884, -- [1]
					37.93000000000029, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Greatgamer-Drakkari"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.87400000000343, -- [1]
					35.24599999999919, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
				[253520] = {
					20.35300000000279, -- [1]
					30.35300000000279, -- [2]
					60.46300000000338, -- [3]
					70.45799999999872, -- [4]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
			},
			["Renera-Goldrinn"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.89100000000326, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Temajin-Terenas"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.87400000000343, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Broslav-Lightbringer"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.87400000000343, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Baresard-Anvilmar"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.95799999999872, -- [1]
					34.95100000000093, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Muertio-Alleria"] = {
				[253753] = {
					14.53800000000047, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.90499999999884, -- [1]
					48.89600000000064, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Thranduil-Mannoroth"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Pikomi-Drakkari"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.550999999999476, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.97499999999855, -- [1]
					36.60100000000239, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
			["Thorddin"] = {
				[253753] = {
					14.55800000000454, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.747000000003027, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					33.91900000000169, -- [1]
					34.80299999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					71.2870000000039, -- [1]
					71.2870000000039, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = false,
				},
			},
		}, -- [4]
	},
	["window_scale"] = 1,
	["BossSpellCast"] = {
		{
			["Apocalypsis Module"] = {
				{
					37.29499999999825, -- [1]
					"Apocalypsis Module", -- [2]
					258029, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			["Argus the Unmaker"] = {
				{
					10.15499999999884, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
				{
					18.80400000000373, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [2]
				{
					28.04899999999907, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [3]
				{
					37.00099999999657, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [4]
				{
					48.90699999999924, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					49.91900000000169, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [6]
			},
			["Shadoweye Scout"] = {
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					277524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vryku-Stormrage", -- [5]
				}, -- [1]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					277524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purified Healer", -- [5]
				}, -- [2]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					79863, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vryku-Stormrage", -- [5]
				}, -- [3]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					277524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vryku-Stormrage", -- [5]
				}, -- [4]
				{
					186.7649999999994, -- [1]
					"Shadoweye Scout", -- [2]
					79863, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kabloomia-Stormrage", -- [5]
				}, -- [5]
			},
		}, -- [1]
		{
			["Argus the Unmaker"] = {
				{
					4.940999999998894, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [1]
				{
					14.73199999999633, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					15.73999999999796, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [3]
				{
					21.54899999999907, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [4]
				{
					46.2410000000018, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Necrotos-Stormrage", -- [5]
				}, -- [5]
				{
					53.59799999999814, -- [1]
					"Argus the Unmaker", -- [2]
					255826, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					101.525999999998, -- [1]
					"Argus the Unmaker", -- [2]
					256542, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					104.5380000000005, -- [1]
					"Argus the Unmaker", -- [2]
					258399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
		}, -- [2]
		{
			["Aggramar"] = {
				{
					12.70899999999529, -- [1]
					"Aggramar", -- [2]
					244693, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Muertio-Alleria", -- [5]
				}, -- [1]
				{
					17.2239999999947, -- [1]
					"Aggramar", -- [2]
					247079, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Broslav-Lightbringer", -- [5]
				}, -- [2]
				{
					17.2239999999947, -- [1]
					"Aggramar", -- [2]
					255058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					24.86699999999837, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					47.01399999999558, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					85.34700000000157, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					108.3269999999975, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			["Unknown"] = {
				{
					32.53800000000047, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.53800000000047, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			["Flame of Taeshalach"] = {
				{
					34.54200000000128, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.54200000000128, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					93.32600000000093, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					93.32600000000093, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					95.32499999999709, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					95.33999999999651, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			["Ember of Taeshalach"] = {
				{
					40.75699999999779, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					41.57299999999668, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					44.39899999999761, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					58.17299999999523, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					63.46399999999994, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					67.50299999999697, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					69.94799999999668, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					69.94799999999668, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					102.7289999999994, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					103.1270000000004, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					103.5429999999979, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					126.5979999999981, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					127.8099999999977, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					129.0190000000002, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					129.8179999999993, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
			},
		}, -- [3]
		{
			["Diima, Mother of Gloom"] = {
				{
					4.5370000000039, -- [1]
					"Diima, Mother of Gloom", -- [2]
					250333, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			["Noura, Mother of Flames"] = {
				{
					9.89900000000489, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					11.13000000000466, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [2]
				{
					20.34000000000378, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Owlcapöne-Tichondrius", -- [5]
				}, -- [3]
				{
					20.34000000000378, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bellak-Stormrage", -- [5]
				}, -- [4]
				{
					20.34000000000378, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [5]
				{
					22.06199999999808, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [6]
				{
					33.00500000000466, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [7]
				{
					43.93300000000454, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [8]
				{
					47.55600000000413, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					54.87700000000041, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [10]
				{
					60.44800000000396, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ðb-Nemesis", -- [5]
				}, -- [11]
				{
					60.44800000000396, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pallycon-Saurfang", -- [5]
				}, -- [12]
				{
					60.44800000000396, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Greatgamer-Drakkari", -- [5]
				}, -- [13]
				{
					65.80099999999948, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syllith-Alexstrasza", -- [5]
				}, -- [14]
			},
			["Asara, Mother of Night"] = {
				{
					5.012000000002445, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.75300000000425, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					11.83600000000297, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					21.72000000000116, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					27.77500000000146, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					31.84600000000501, -- [1]
					"Asara, Mother of Night", -- [2]
					252861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					39.93800000000192, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					40.68100000000413, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					50.86499999999796, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					56.93600000000151, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					63.02300000000105, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					69.08899999999994, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					70.16700000000128, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
			},
			["Thu'raya, Mother of the Cosmos"] = {
				{
					50.7190000000046, -- [1]
					"Thu'raya, Mother of the Cosmos", -- [2]
					250334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [4]
	},
}
