
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 2,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["tipo"] = 2,
					["combatId"] = 1596,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00455,
							["damage_from"] = {
								["[*] Chilling Winds"] = true,
							},
							["targets"] = {
							},
							["serial"] = "Player-76-057FA942",
							["pets"] = {
							},
							["spec"] = 70,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.00455,
							["classe"] = "PALADIN",
							["dps_started"] = false,
							["total"] = 0.00455,
							["friendlyfire"] = {
							},
							["tipo"] = 1,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1605285566,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 1137.00455,
							["start_time"] = 1605285566,
							["delay"] = 0,
							["aID"] = "76-057FA942",
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 2317.003943,
							["damage_from"] = {
							},
							["targets"] = {
								["Authumn"] = 1482,
								["Thorddin"] = 1137,
								["Giefan"] = 1530,
								["Saeew"] = 1173,
								["Daswarr"] = 1491,
							},
							["spellicon"] = 629077,
							["monster"] = true,
							["friendlyfire_total"] = 0,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 6813.003943,
							["pets"] = {
							},
							["last_dps"] = 0,
							["fight_component"] = true,
							["total"] = 6813.003943,
							["serial"] = "",
							["classe"] = "UNKNOW",
							["timeMachine"] = 1,
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 391,
										["targets"] = {
											["Authumn"] = 1482,
											["Thorddin"] = 1137,
											["Giefan"] = 1530,
											["Saeew"] = 1173,
											["Daswarr"] = 1491,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6813,
										["n_min"] = 370,
										["g_dmg"] = 0,
										["counter"] = 18,
										["total"] = 6813,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 3,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 18,
										["a_dmg"] = 1144,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = true,
							["dps_started"] = true,
							["last_event"] = 1605285602,
							["nome"] = "[*] Chilling Winds",
							["custom"] = 0,
							["tipo"] = 1,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285562,
							["delay"] = 1605285602,
							["damage_taken"] = 0.003943,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1596,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1596,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1596,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["aID"] = "76-057FA942",
							["buff_uptime"] = 16,
							["buff_uptime_targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 1605285566,
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231435] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 231435,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 0,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-76-057FA942",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1596,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1914,
				["playing_solo"] = true,
				["totals"] = {
					6812.971632, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285571.905, -- [4]
							9172, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285581.873, -- [4]
							9756, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 3,
					},
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:39:27",
				["cleu_timeline"] = {
				},
				["enemy"] = "Unknown",
				["TotalElapsedCombatTime"] = 12656.516,
				["CombatEndedAt"] = 12656.516,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 0.00455,
						}, -- [1]
					},
				},
				["end_time"] = 12656.516,
				["combat_id"] = 1596,
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1605285562,
				["frags"] = {
				},
				["contra"] = "[*] Chilling Winds",
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					0, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12652.364,
				["TimeData"] = {
				},
				["data_inicio"] = "11:39:22",
			}, -- [1]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1595,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004605,
							["damage_from"] = {
								["Darksworn Sentry"] = true,
								["[*] Chilling Winds"] = true,
							},
							["targets"] = {
							},
							["serial"] = "Player-76-057FA942",
							["pets"] = {
							},
							["spec"] = 70,
							["on_hold"] = false,
							["aID"] = "76-057FA942",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.004605,
							["end_time"] = 1605285559,
							["dps_started"] = false,
							["total"] = 0.004605,
							["friendlyfire"] = {
							},
							["last_event"] = 0,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["friendlyfire_total"] = 0,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 1019.004605,
							["start_time"] = 1605285559,
							["delay"] = 0,
							["classe"] = "PALADIN",
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 1144.001175,
							["damage_from"] = {
							},
							["targets"] = {
								["Giefan"] = 765,
								["Authumn"] = 742,
								["Thorddin"] = 379,
								["Memmonk"] = 379,
							},
							["pets"] = {
							},
							["dps_started"] = false,
							["total"] = 2265.001175,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 2265.001175,
							["spellicon"] = 629077,
							["last_dps"] = 0,
							["monster"] = true,
							["end_time"] = 1605285562,
							["serial"] = "",
							["friendlyfire_total"] = 0,
							["nome"] = "[*] Chilling Winds",
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 383,
										["targets"] = {
											["Giefan"] = 765,
											["Authumn"] = 742,
											["Thorddin"] = 379,
											["Memmonk"] = 379,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2265,
										["n_min"] = 371,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 2265,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 6,
										["a_dmg"] = 379,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["fight_component"] = true,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["last_event"] = 1605285562,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285547,
							["delay"] = 0,
							["damage_taken"] = 0.001175,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 301.004303,
							["damage_from"] = {
							},
							["targets"] = {
								["Thorddin"] = 640,
							},
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EB4DA",
							["pets"] = {
							},
							["total"] = 640.0043029999999,
							["monster"] = true,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 640.0043029999999,
							["damage_taken"] = 0.004303,
							["dps_started"] = false,
							["end_time"] = 1605285562,
							["on_hold"] = false,
							["last_event"] = 1605285560,
							["nome"] = "Darksworn Sentry",
							["spells"] = {
								["_ActorTable"] = {
									[16564] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 339,
										["targets"] = {
											["Thorddin"] = 640,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 640,
										["n_min"] = 301,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 640,
										["c_max"] = 0,
										["spellschool"] = 8,
										["id"] = 16564,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285558,
							["delay"] = 1605285545,
							["fight_component"] = true,
						}, -- [3]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1595,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.001852,
							["total_without_pet"] = 379.001852,
							["total"] = 379.001852,
							["targets_absorbs"] = {
								["Thorddin"] = 379,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 379.001852,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 379,
							},
							["totalover_without_pet"] = 0.001852,
							["healing_taken"] = 379.001852,
							["fight_component"] = true,
							["end_time"] = 1605285559,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 379,
										["targets_overheal"] = {
										},
										["n_max"] = 379,
										["targets"] = {
											["Thorddin"] = 379,
										},
										["n_min"] = 379,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 379,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 379,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 379,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285552,
							["custom"] = 0,
							["last_event"] = 1605285552,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.001852,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1595,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1595,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 70,
							["grupo"] = true,
							["nome"] = "Thorddin",
							["buff_uptime"] = 70,
							["pets"] = {
							},
							["tipo"] = 4,
							["aID"] = "76-057FA942",
							["last_event"] = 1605285559,
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231435] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 231435,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 14,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["serial"] = "Player-76-057FA942",
							["classe"] = "PALADIN",
						}, -- [1]
						{
							["flag_original"] = 1304,
							["aID"] = "",
							["nome"] = "[*] Chilling Winds",
							["pets"] = {
							},
							["fight_component"] = true,
							["spell_cast"] = {
								[326788] = 5,
							},
							["tipo"] = 4,
							["classe"] = "UNGROUPPLAYER",
							["serial"] = "",
							["last_event"] = 0,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Darksworn Sentry",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[16564] = 1,
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["aID"] = "",
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EB4DA",
							["monster"] = true,
						}, -- [3]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1595,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12650.415,
				["tempo_start"] = 1605285545,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1913,
				["playing_solo"] = true,
				["totals"] = {
					2904.970959, -- [1]
					378.985849, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorddin"] = {
						{
							true, -- [1]
							16564, -- [2]
							301, -- [3]
							1605285559.95, -- [4]
							9391, -- [5]
							"Darksworn Sentry", -- [6]
							nil, -- [7]
							8, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:39:20",
				["cleu_timeline"] = {
				},
				["enemy"] = "Darksworn Sentry",
				["TotalElapsedCombatTime"] = 12649.985,
				["CombatEndedAt"] = 12649.985,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 379.001852,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 0.004605,
						}, -- [1]
					},
				},
				["end_time"] = 12649.985,
				["combat_id"] = 1595,
				["frags"] = {
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					0, -- [1]
					379, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12635.404,
				["contra"] = "Darksworn Sentry",
				["data_inicio"] = "11:39:05",
			}, -- [2]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1594,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002133,
							["damage_from"] = {
								["[*] Chilling Winds"] = true,
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 7457,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 477,
										["g_amt"] = 0,
										["n_max"] = 239,
										["targets"] = {
											["Chillbone Gnawer"] = 953,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 476,
										["n_min"] = 237,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 953,
										["c_max"] = 477,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 477,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 230,
										["targets"] = {
											["Chillbone Gnawer"] = 230,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 230,
										["n_min"] = 230,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 230,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 551,
										["targets"] = {
											["Chillbone Gnawer"] = 551,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 551,
										["n_min"] = 551,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 551,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 2871,
										["g_amt"] = 0,
										["n_max"] = 1356,
										["targets"] = {
											["Chillbone Gnawer"] = 4227,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1356,
										["n_min"] = 1356,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 4227,
										["c_max"] = 2871,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 2871,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 537,
										["targets"] = {
											["Chillbone Gnawer"] = 537,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 537,
										["n_min"] = 537,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 537,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 733,
										["targets"] = {
											["Chillbone Gnawer"] = 733,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 733,
										["n_min"] = 733,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 733,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7457.002133,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 7457.002133,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605285525,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285525,
							["last_dps"] = 900.7129040948463,
							["start_time"] = 1605285517,
							["delay"] = 0,
							["damage_taken"] = 2215.002133,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 726.006383,
							["damage_from"] = {
								["Daswarr"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Eadric the Pure"] = 5159,
								["Daswarr"] = 2294,
								["Thorddin"] = 1078,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB118",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 8531.006383,
							["damage_taken"] = 18241.006383,
							["monster"] = true,
							["end_time"] = 1605285545,
							["on_hold"] = false,
							["last_event"] = 1605285544,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 3,
										["b_amt"] = 0,
										["c_dmg"] = 1930,
										["g_amt"] = 0,
										["n_max"] = 413,
										["targets"] = {
											["Eadric the Pure"] = 5159,
											["Daswarr"] = 2294,
											["Thorddin"] = 1078,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6601,
										["n_min"] = 236,
										["g_dmg"] = 0,
										["counter"] = 24,
										["a_amt"] = 0,
										["total"] = 8531,
										["c_max"] = 652,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 639,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 20,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285518,
							["delay"] = 0,
							["total"] = 8531.006383,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 757.00797,
							["damage_from"] = {
							},
							["targets"] = {
								["Thorddin"] = 1137,
								["Giefan"] = 382,
								["Daswarr"] = 373,
								["Gnarill"] = 397,
								["Memmonk"] = 1135,
							},
							["pets"] = {
							},
							["dps_started"] = false,
							["total"] = 3424.00797,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 3424.00797,
							["spellicon"] = 629077,
							["last_dps"] = 0,
							["monster"] = true,
							["end_time"] = 1605285545,
							["serial"] = "",
							["friendlyfire_total"] = 0,
							["nome"] = "[*] Chilling Winds",
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 397,
										["targets"] = {
											["Thorddin"] = 1137,
											["Giefan"] = 382,
											["Daswarr"] = 373,
											["Gnarill"] = 397,
											["Memmonk"] = 1135,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3424,
										["n_min"] = 373,
										["g_dmg"] = 0,
										["counter"] = 9,
										["total"] = 3424,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 3,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 9,
										["a_dmg"] = 1139,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["fight_component"] = true,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["last_event"] = 1605285542,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285522,
							["delay"] = 0,
							["damage_taken"] = 0.00797,
						}, -- [3]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1594,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.007862,
							["total_without_pet"] = 868.0078619999999,
							["total"] = 868.0078619999999,
							["targets_absorbs"] = {
								["Thorddin"] = 868,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 868.0078619999999,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 868,
							},
							["totalover_without_pet"] = 0.007862,
							["healing_taken"] = 868.0078619999999,
							["fight_component"] = true,
							["end_time"] = 1605285525,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 868,
										["targets_overheal"] = {
										},
										["n_max"] = 364,
										["targets"] = {
											["Thorddin"] = 868,
										},
										["n_min"] = 142,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 868,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 868,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 868,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285518,
							["custom"] = 0,
							["last_event"] = 1605285522,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.007862,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1594,
					["_ActorTable"] = {
						{
							["received"] = 0.00417,
							["resource"] = 4.00417,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.00417,
							["fight_component"] = true,
							["total"] = 0.00417,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00417,
							["tipo"] = 3,
							["last_event"] = 1605285525,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.00417,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1594,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 53,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 267611,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[280204] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 280204,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 286393,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 231843,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 1,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[24275] = 1,
								[184575] = 1,
								[85256] = 2,
								[35395] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285525,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 1304,
							["aID"] = "",
							["nome"] = "[*] Chilling Winds",
							["pets"] = {
							},
							["fight_component"] = true,
							["spell_cast"] = {
								[326788] = 3,
							},
							["tipo"] = 4,
							["classe"] = "UNGROUPPLAYER",
							["serial"] = "",
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1594,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12632.987,
				["tempo_start"] = 1605285517,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1912,
				["playing_solo"] = true,
				["totals"] = {
					19411.986899, -- [1]
					867.9865510000001, -- [2]
					{
						-0.002285999999998012, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285531.894, -- [4]
							9409, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285541.904, -- [4]
							9612, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 3,
					},
					["Memmonk"] = {
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285525.964, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285532.426, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285532.838, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285533.415, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [4]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285533.903, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [5]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285534.434, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [6]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285534.841, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [7]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285535.395, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [8]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285535.842, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [9]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285542.418, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [10]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285542.937, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [11]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285543.347, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [12]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285543.901, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [13]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285544.366, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [14]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285544.939, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 16,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:38:46",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 12616.033,
				["CombatEndedAt"] = 12616.033,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 868.0078619999999,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 7457.002133,
						}, -- [1]
					},
				},
				["end_time"] = 12616.033,
				["combat_id"] = 1594,
				["frags"] = {
					["Chillbone Gnawer"] = 2,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					7457, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12607.754,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:38:38",
			}, -- [3]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1593,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004411,
							["damage_from"] = {
								["Darksworn Bonecaster"] = true,
								["[*] Chilling Winds"] = true,
							},
							["targets"] = {
								["Darksworn Bonecaster"] = 5891,
								["Chillbone Gnawer"] = 6306,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 513,
										["g_amt"] = 0,
										["n_max"] = 270,
										["targets"] = {
											["Darksworn Bonecaster"] = 538,
											["Chillbone Gnawer"] = 764,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 789,
										["n_min"] = 251,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1302,
										["c_max"] = 513,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 513,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Darksworn Bonecaster"] = 226,
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 452,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 452,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[255937] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1355,
										["targets"] = {
											["Chillbone Gnawer"] = 1315,
											["Darksworn Bonecaster"] = 1355,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2670,
										["n_min"] = 1315,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2670,
										["c_max"] = 0,
										["spellschool"] = 6,
										["id"] = 255937,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 481,
										["g_amt"] = 0,
										["n_max"] = 254,
										["targets"] = {
											["Darksworn Bonecaster"] = 254,
											["Chillbone Gnawer"] = 481,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 254,
										["n_min"] = 254,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 735,
										["c_max"] = 481,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 481,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[295367] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 272,
										["targets"] = {
											["Darksworn Bonecaster"] = 272,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 272,
										["n_min"] = 272,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 272,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 295367,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 474,
										["targets"] = {
											["Darksworn Bonecaster"] = 474,
											["Chillbone Gnawer"] = 453,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 927,
										["n_min"] = 453,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 927,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1144,
										["targets"] = {
											["Darksworn Bonecaster"] = 1144,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1144,
										["n_min"] = 1144,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1144,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1434,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 1434,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1434,
										["c_max"] = 1434,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1434,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[53385] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 826,
										["targets"] = {
											["Chillbone Gnawer"] = 1633,
											["Darksworn Bonecaster"] = 1628,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3261,
										["n_min"] = 802,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 3261,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 53385,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 12197.004411,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 12197.004411,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605285511,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285510,
							["last_dps"] = 1001.31388317868,
							["start_time"] = 1605285498,
							["delay"] = 0,
							["damage_taken"] = 1470.004411,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 1520.002684,
							["damage_from"] = {
							},
							["targets"] = {
								["Thorddin"] = 759,
								["Giefan"] = 766,
								["Daswarr"] = 373,
								["Gnarill"] = 396,
								["Memmonk"] = 758,
							},
							["pets"] = {
							},
							["dps_started"] = false,
							["total"] = 3052.002684,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 3052.002684,
							["spellicon"] = 629077,
							["last_dps"] = 0,
							["monster"] = true,
							["end_time"] = 1605285517,
							["serial"] = "",
							["friendlyfire_total"] = 0,
							["nome"] = "[*] Chilling Winds",
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 396,
										["targets"] = {
											["Thorddin"] = 759,
											["Giefan"] = 766,
											["Daswarr"] = 373,
											["Gnarill"] = 396,
											["Memmonk"] = 758,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3052,
										["n_min"] = 373,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 3052,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 8,
										["a_dmg"] = 383,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["fight_component"] = true,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["last_event"] = 1605285517,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285502,
							["delay"] = 0,
							["damage_taken"] = 0.002684,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.008312,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Eadric the Pure"] = 2009,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB5AF",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 2009.008312,
							["damage_taken"] = 6306.008312,
							["monster"] = true,
							["end_time"] = 1605285517,
							["on_hold"] = false,
							["last_event"] = 1605285515,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 377,
										["targets"] = {
											["Eadric the Pure"] = 2009,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2009,
										["n_min"] = 297,
										["g_dmg"] = 0,
										["counter"] = 9,
										["MISS"] = 3,
										["total"] = 2009,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285499,
							["delay"] = 0,
							["total"] = 2009.008312,
						}, -- [3]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 0.001688,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 711,
							},
							["serial"] = "Creature-0-3023-571-32355-171757-00002EB5BA",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "171757",
							["raid_targets"] = {
							},
							["total_without_pet"] = 711.0016880000001,
							["damage_taken"] = 5891.001688,
							["monster"] = true,
							["end_time"] = 1605285511,
							["on_hold"] = false,
							["last_event"] = 1605285509,
							["nome"] = "Darksworn Bonecaster",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 331,
										["targets"] = {
											["Thorddin"] = 331,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 331,
										["n_min"] = 331,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 331,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 331,
										["r_amt"] = 0,
									}, -- [1]
									[183345] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 380,
										["targets"] = {
											["Thorddin"] = 380,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 380,
										["n_min"] = 380,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 380,
										["c_max"] = 0,
										["id"] = 183345,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285507,
							["delay"] = 0,
							["total"] = 711.0016880000001,
						}, -- [4]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1593,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.00119,
							["total_without_pet"] = 489.00119,
							["total"] = 489.00119,
							["targets_absorbs"] = {
								["Thorddin"] = 489,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 489.00119,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 489,
							},
							["totalover_without_pet"] = 0.00119,
							["healing_taken"] = 489.00119,
							["fight_component"] = true,
							["end_time"] = 1605285511,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 489,
										["targets_overheal"] = {
										},
										["n_max"] = 379,
										["targets"] = {
											["Thorddin"] = 489,
										},
										["n_min"] = 110,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 489,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 489,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 489,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285502,
							["custom"] = 0,
							["last_event"] = 1605285507,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.00119,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1593,
					["_ActorTable"] = {
						{
							["received"] = 0.004646,
							["resource"] = 9.004646000000001,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.004646,
							["fight_component"] = true,
							["total"] = 0.004646,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.004646,
							["tipo"] = 3,
							["last_event"] = 1605285516,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.004646,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1593,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[255937] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 255937,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 3,
										["id"] = 295367,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[255941] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 255941,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 62,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[280204] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 280204,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 13,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[85256] = 1,
								[255937] = 1,
								[53385] = 2,
								[184575] = 1,
								[24275] = 1,
								[35395] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285511,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 1304,
							["aID"] = "",
							["nome"] = "[*] Chilling Winds",
							["pets"] = {
							},
							["fight_component"] = true,
							["spell_cast"] = {
								[326788] = 3,
							},
							["tipo"] = 4,
							["classe"] = "UNGROUPPLAYER",
							["serial"] = "",
							["last_event"] = 0,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Darksworn Bonecaster",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[183345] = 1,
							},
							["last_event"] = 0,
							["aID"] = "171757",
							["tipo"] = 4,
							["serial"] = "Creature-0-3023-571-32355-171757-00002EB5BA",
							["monster"] = true,
						}, -- [3]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1593,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12607.272,
				["tempo_start"] = 1605285498,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1911,
				["playing_solo"] = true,
				["totals"] = {
					17968.97339900001, -- [1]
					488.990511, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Memmonk"] = {
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285512.411, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285512.882, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285513.401, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285513.902, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [4]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285514.364, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [5]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285514.88, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [6]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285515.413, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [7]
						{
							true, -- [1]
							124255, -- [2]
							3, -- [3]
							1605285515.909, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 9,
					},
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							380, -- [3]
							1605285511.818, -- [4]
							9795, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:38:31",
				["cleu_timeline"] = {
				},
				["enemy"] = "Darksworn Bonecaster",
				["TotalElapsedCombatTime"] = 12601.384,
				["CombatEndedAt"] = 12601.384,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 489.00119,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 12197.004411,
						}, -- [1]
					},
				},
				["end_time"] = 12601.384,
				["combat_id"] = 1593,
				["frags"] = {
					["Darksworn Bonecaster"] = 1,
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					12197, -- [1]
					489, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12589.203,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:38:19",
			}, -- [4]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1592,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004264,
							["damage_from"] = {
								["Darksworn Sentry"] = true,
								["[*] Chilling Winds"] = true,
							},
							["targets"] = {
								["Darksworn Sentry"] = 6670,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 498,
										["g_amt"] = 0,
										["n_max"] = 237,
										["targets"] = {
											["Darksworn Sentry"] = 969,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 471,
										["n_min"] = 234,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 969,
										["c_max"] = 498,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 498,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[35395] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 463,
										["g_amt"] = 0,
										["n_max"] = 240,
										["targets"] = {
											["Darksworn Sentry"] = 703,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 240,
										["n_min"] = 240,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 703,
										["c_max"] = 463,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 463,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 459,
										["targets"] = {
											["Darksworn Sentry"] = 459,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 459,
										["n_min"] = 459,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 459,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1580,
										["targets"] = {
											["Darksworn Sentry"] = 3078,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3078,
										["n_min"] = 1498,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 3078,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 606,
										["targets"] = {
											["Darksworn Sentry"] = 606,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 606,
										["n_min"] = 606,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 606,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 855,
										["targets"] = {
											["Darksworn Sentry"] = 855,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 855,
										["n_min"] = 855,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 855,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6670.004264,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 6670.004264,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["end_time"] = 1605285481,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285480,
							["last_dps"] = 700.6307000001018,
							["start_time"] = 1605285471,
							["delay"] = 0,
							["damage_taken"] = 2054.004264,
						}, -- [1]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 2665.002544,
							["damage_from"] = {
							},
							["targets"] = {
								["Giefan"] = 1149,
								["Daswarr"] = 1117,
								["Thorddin"] = 1137,
								["Memmonk"] = 1136,
							},
							["pets"] = {
							},
							["dps_started"] = false,
							["total"] = 4539.002544,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4539.002544,
							["spellicon"] = 629077,
							["last_dps"] = 0,
							["monster"] = true,
							["end_time"] = 1605285498,
							["serial"] = "",
							["friendlyfire_total"] = 0,
							["nome"] = "[*] Chilling Winds",
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 383,
										["targets"] = {
											["Giefan"] = 1149,
											["Daswarr"] = 1117,
											["Thorddin"] = 1137,
											["Memmonk"] = 1136,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4539,
										["n_min"] = 372,
										["g_dmg"] = 0,
										["counter"] = 12,
										["total"] = 4539,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 12,
										["a_dmg"] = 378,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["fight_component"] = true,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["last_event"] = 1605285497,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285472,
							["delay"] = 0,
							["damage_taken"] = 0.002544,
						}, -- [2]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.002258,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 917,
							},
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EB644",
							["pets"] = {
							},
							["total"] = 917.002258,
							["monster"] = true,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 917.002258,
							["damage_taken"] = 6670.002258,
							["dps_started"] = false,
							["end_time"] = 1605285481,
							["on_hold"] = false,
							["last_event"] = 1605285480,
							["nome"] = "Darksworn Sentry",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 353,
										["targets"] = {
											["Thorddin"] = 917,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 917,
										["n_min"] = 264,
										["g_dmg"] = 0,
										["counter"] = 4,
										["a_amt"] = 1,
										["total"] = 917,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 264,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285474,
							["delay"] = 0,
							["fight_component"] = true,
						}, -- [3]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1592,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.007529,
							["total_without_pet"] = 489.007529,
							["total"] = 489.007529,
							["targets_absorbs"] = {
								["Thorddin"] = 489,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 489.007529,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 489,
							},
							["totalover_without_pet"] = 0.007529,
							["healing_taken"] = 489.007529,
							["fight_component"] = true,
							["end_time"] = 1605285481,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 489,
										["targets_overheal"] = {
										},
										["n_max"] = 379,
										["targets"] = {
											["Thorddin"] = 489,
										},
										["n_min"] = 110,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 489,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 489,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 489,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285472,
							["custom"] = 0,
							["last_event"] = 1605285474,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.007529,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1592,
					["_ActorTable"] = {
						{
							["received"] = 0.005544,
							["resource"] = 7.005544,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.005544,
							["fight_component"] = true,
							["total"] = 0.005544,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.005544,
							["tipo"] = 3,
							["last_event"] = 1605285498,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.005544,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1592,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 2,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 46,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[268534] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 268534,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 8,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 2,
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[24275] = 1,
								[35395] = 2,
								[85256] = 2,
								[184575] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285481,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 1304,
							["aID"] = "",
							["nome"] = "[*] Chilling Winds",
							["pets"] = {
							},
							["fight_component"] = true,
							["spell_cast"] = {
								[326788] = 4,
							},
							["tipo"] = 4,
							["classe"] = "UNGROUPPLAYER",
							["serial"] = "",
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1592,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12589.203,
				["tempo_start"] = 1605285471,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1910,
				["playing_solo"] = true,
				["totals"] = {
					12125.975292, -- [1]
					488.985698, -- [2]
					{
						-0.004498000000012326, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Memmonk"] = {
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285482.414, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285482.937, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285483.424, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285483.904, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [4]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285484.333, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [5]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285484.86, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [6]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285485.329, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [7]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285485.912, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [8]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285492.412, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [9]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285492.906, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [10]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285493.368, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [11]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285493.881, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [12]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285494.406, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [13]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285494.911, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [14]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285495.415, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [15]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285495.916, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 17,
					},
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285481.9, -- [4]
							9722, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285491.889, -- [4]
							10307, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 3,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:38:01",
				["cleu_timeline"] = {
				},
				["enemy"] = "Darksworn Sentry",
				["TotalElapsedCombatTime"] = 12571.372,
				["CombatEndedAt"] = 12571.372,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 489.007529,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6670.004264,
						}, -- [1]
					},
				},
				["end_time"] = 12571.372,
				["combat_id"] = 1592,
				["frags"] = {
					["Darksworn Sentry"] = 1,
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6670, -- [1]
					489, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12561.852,
				["contra"] = "Darksworn Sentry",
				["data_inicio"] = "11:37:52",
			}, -- [5]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1591,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001447,
							["damage_from"] = {
								["[*] Chilling Winds"] = true,
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 8455,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 993,
										["g_amt"] = 0,
										["n_max"] = 252,
										["targets"] = {
											["Chillbone Gnawer"] = 1245,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 252,
										["n_min"] = 252,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1245,
										["c_max"] = 515,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 478,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 540,
										["targets"] = {
											["Chillbone Gnawer"] = 540,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 540,
										["n_min"] = 540,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 540,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 476,
										["g_amt"] = 0,
										["n_max"] = 233,
										["targets"] = {
											["Chillbone Gnawer"] = 709,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 233,
										["n_min"] = 233,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 709,
										["c_max"] = 476,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 476,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[295367] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 272,
										["targets"] = {
											["Chillbone Gnawer"] = 1088,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1088,
										["n_min"] = 272,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1088,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 295367,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 442,
										["targets"] = {
											["Chillbone Gnawer"] = 442,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 442,
										["n_min"] = 442,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 442,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1446,
										["targets"] = {
											["Chillbone Gnawer"] = 2778,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2778,
										["n_min"] = 1332,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2778,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1427,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 1427,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1427,
										["c_max"] = 1427,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1427,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 8455.001447,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 8455.001447,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605285452,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285451,
							["last_dps"] = 946.808672676341,
							["start_time"] = 1605285443,
							["delay"] = 0,
							["damage_taken"] = 1985.001447,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.004804,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Eadric the Pure"] = 4674,
								["Orgrimmar Champion"] = 400,
								["Thorddin"] = 1226,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB55B",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 6300.004804,
							["damage_taken"] = 8455.004804,
							["monster"] = true,
							["end_time"] = 1605285471,
							["on_hold"] = false,
							["last_event"] = 1605285471,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 729,
										["g_amt"] = 0,
										["n_max"] = 425,
										["targets"] = {
											["Eadric the Pure"] = 4674,
											["Orgrimmar Champion"] = 400,
											["Thorddin"] = 1226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5571,
										["n_min"] = 280,
										["g_dmg"] = 0,
										["counter"] = 19,
										["a_amt"] = 1,
										["total"] = 6300,
										["c_max"] = 729,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 307,
										["m_crit"] = 0,
										["PARRY"] = 2,
										["c_min"] = 729,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 16,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285444,
							["delay"] = 0,
							["total"] = 6300.004804,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 1139.008435,
							["damage_from"] = {
							},
							["targets"] = {
								["Thorddin"] = 759,
								["Giefan"] = 383,
								["Daswarr"] = 730,
								["Miniøn"] = 391,
								["Memmonk"] = 755,
							},
							["pets"] = {
							},
							["dps_started"] = false,
							["total"] = 3018.008435,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 3018.008435,
							["spellicon"] = 629077,
							["last_dps"] = 0,
							["monster"] = true,
							["end_time"] = 1605285471,
							["serial"] = "",
							["friendlyfire_total"] = 0,
							["nome"] = "[*] Chilling Winds",
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 391,
										["targets"] = {
											["Thorddin"] = 759,
											["Giefan"] = 383,
											["Daswarr"] = 730,
											["Miniøn"] = 391,
											["Memmonk"] = 755,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3018,
										["n_min"] = 357,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 3018,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 2,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 8,
										["a_dmg"] = 751,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["fight_component"] = true,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["last_event"] = 1605285467,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285452,
							["delay"] = 0,
							["damage_taken"] = 0.008435,
						}, -- [3]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1591,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.008376,
							["total_without_pet"] = 110.008376,
							["total"] = 110.008376,
							["targets_absorbs"] = {
								["Thorddin"] = 110,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 110.008376,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 110,
							},
							["totalover_without_pet"] = 0.008376,
							["healing_taken"] = 110.008376,
							["fight_component"] = true,
							["end_time"] = 1605285452,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 110,
										["targets_overheal"] = {
										},
										["n_max"] = 110,
										["targets"] = {
											["Thorddin"] = 110,
										},
										["n_min"] = 110,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 110,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 110,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 110,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285445,
							["custom"] = 0,
							["last_event"] = 1605285445,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.008376,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1591,
					["_ActorTable"] = {
						{
							["received"] = 0.007848,
							["resource"] = 6.007848,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.007848,
							["fight_component"] = true,
							["total"] = 0.007848,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007848,
							["tipo"] = 3,
							["last_event"] = 1605285470,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.007848,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1591,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 3,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 8,
										["id"] = 295367,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 36,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["activedamt"] = 1,
										["id"] = 231843,
										["targets"] = {
										},
										["actived_at"] = 1605285451,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 267611,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 11,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[24275] = 1,
								[184575] = 1,
								[85256] = 2,
								[35395] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285452,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 1304,
							["aID"] = "",
							["nome"] = "[*] Chilling Winds",
							["pets"] = {
							},
							["fight_component"] = true,
							["spell_cast"] = {
								[326788] = 4,
							},
							["tipo"] = 4,
							["classe"] = "UNGROUPPLAYER",
							["serial"] = "",
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1591,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12561.061,
				["tempo_start"] = 1605285443,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1909,
				["playing_solo"] = true,
				["totals"] = {
					17772.950525, -- [1]
					109.990795, -- [2]
					{
						-0.001511999999991076, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285461.89, -- [4]
							10441, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
					["Memmonk"] = {
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285452.949, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285453.421, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285453.921, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285454.372, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [4]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285454.929, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [5]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285455.326, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [6]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285455.92, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [7]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285462.394, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [8]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285462.898, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [9]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285463.413, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [10]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285463.877, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [11]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285464.457, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [12]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285464.882, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [13]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285465.366, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [14]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285465.912, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 16,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:37:33",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 12542.99,
				["CombatEndedAt"] = 12542.99,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 110.008376,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 8455.001447,
						}, -- [1]
					},
				},
				["end_time"] = 12542.99,
				["combat_id"] = 1591,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
					["Brittle Boneguard"] = 7,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					8455, -- [1]
					110, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12534.06,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:37:24",
			}, -- [6]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1590,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005389,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
								["[*] Chilling Winds"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 6689,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 473,
										["g_amt"] = 0,
										["n_max"] = 252,
										["targets"] = {
											["Chillbone Gnawer"] = 957,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 484,
										["n_min"] = 232,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 957,
										["c_max"] = 473,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 473,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[224266] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 2986,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 2986,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 2986,
										["c_max"] = 2986,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 2986,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 454,
										["targets"] = {
											["Chillbone Gnawer"] = 454,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 454,
										["n_min"] = 454,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 454,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 952,
										["g_amt"] = 0,
										["n_max"] = 231,
										["targets"] = {
											["Chillbone Gnawer"] = 1183,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 231,
										["n_min"] = 231,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1183,
										["c_max"] = 476,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 476,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1109,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 1109,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1109,
										["c_max"] = 1109,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1109,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6689.005389,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 6689.005389,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["end_time"] = 1605285419,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285418,
							["last_dps"] = 806.1956597566685,
							["start_time"] = 1605285410,
							["delay"] = 0,
							["damage_taken"] = 2699.005389,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.005236,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Eadric the Pure"] = 5368,
								["Thorddin"] = 1183,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB483",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 6551.005236,
							["damage_taken"] = 6689.005236,
							["monster"] = true,
							["end_time"] = 1605285443,
							["on_hold"] = false,
							["last_event"] = 1605285442,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 426,
										["targets"] = {
											["Eadric the Pure"] = 5368,
											["Thorddin"] = 1183,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6551,
										["n_min"] = 243,
										["g_dmg"] = 0,
										["counter"] = 20,
										["DODGE"] = 1,
										["total"] = 6551,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 339,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 1,
										["n_amt"] = 19,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285412,
							["delay"] = 0,
							["total"] = 6551.005236,
						}, -- [2]
						{
							["flag_original"] = 2632,
							["totalabsorbed"] = 2272.004717,
							["damage_from"] = {
							},
							["targets"] = {
								["Daswarr"] = 745,
								["Miniøn"] = 1173,
								["Thorddin"] = 1516,
								["Memmonk"] = 1514,
							},
							["pets"] = {
							},
							["dps_started"] = false,
							["total"] = 4948.004717,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4948.004717,
							["spellicon"] = 629077,
							["last_dps"] = 0,
							["monster"] = true,
							["end_time"] = 1605285443,
							["serial"] = "",
							["friendlyfire_total"] = 0,
							["nome"] = "[*] Chilling Winds",
							["spells"] = {
								["_ActorTable"] = {
									[326788] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 391,
										["targets"] = {
											["Daswarr"] = 745,
											["Miniøn"] = 1173,
											["Thorddin"] = 1516,
											["Memmonk"] = 1514,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4948,
										["n_min"] = 372,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 4948,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 326788,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 13,
										["a_dmg"] = 379,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["fight_component"] = true,
							["tipo"] = 1,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["last_event"] = 1605285442,
							["friendlyfire"] = {
							},
							["start_time"] = 1605285432,
							["delay"] = 1605285432,
							["damage_taken"] = 0.004717,
						}, -- [3]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1590,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.00863,
							["total_without_pet"] = 489.00863,
							["total"] = 489.00863,
							["targets_absorbs"] = {
								["Thorddin"] = 489,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 489.00863,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 489,
							},
							["totalover_without_pet"] = 0.00863,
							["healing_taken"] = 489.00863,
							["fight_component"] = true,
							["end_time"] = 1605285419,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 489,
										["targets_overheal"] = {
										},
										["n_max"] = 379,
										["targets"] = {
											["Thorddin"] = 489,
										},
										["n_min"] = 110,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 489,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 489,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 489,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285412,
							["custom"] = 0,
							["last_event"] = 1605285412,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.00863,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1590,
					["_ActorTable"] = {
						{
							["received"] = 0.007899,
							["resource"] = 6.007899,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.007899,
							["fight_component"] = true,
							["total"] = 0.007899,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007899,
							["tipo"] = 3,
							["last_event"] = 1605285442,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.007899,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1590,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 4,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 35,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["activedamt"] = 1,
										["id"] = 231843,
										["targets"] = {
										},
										["actived_at"] = 1605285418,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 4,
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[35395] = 3,
								[85256] = 1,
								[184575] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285419,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 1304,
							["aID"] = "",
							["nome"] = "[*] Chilling Winds",
							["pets"] = {
							},
							["fight_component"] = true,
							["spell_cast"] = {
								[326788] = 2,
							},
							["tipo"] = 4,
							["classe"] = "UNGROUPPLAYER",
							["serial"] = "",
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1590,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12533.149,
				["tempo_start"] = 1605285410,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1908,
				["playing_solo"] = true,
				["totals"] = {
					18187.976262, -- [1]
					488.994581, -- [2]
					{
						-0.00723599999999891, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Memmonk"] = {
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285422.409, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285422.894, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285423.332, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285423.888, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [4]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285424.399, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [5]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285424.849, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [6]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285425.332, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [7]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285425.908, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [8]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285432.351, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [9]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285432.909, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [10]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285433.345, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [11]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285433.919, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [12]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285434.415, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [13]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285434.909, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [14]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285435.42, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [15]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285435.88, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [16]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285442.358, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [17]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285442.891, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [18]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285443.416, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 20,
					},
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285421.895, -- [4]
							10184, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285431.89, -- [4]
							10768, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285441.867, -- [4]
							11352, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 4,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:36:59",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 12509.292,
				["CombatEndedAt"] = 12509.292,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 489.00863,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6689.005389,
						}, -- [1]
					},
				},
				["end_time"] = 12509.292,
				["combat_id"] = 1590,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6689, -- [1]
					489, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12500.995,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:36:51",
			}, -- [7]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1589,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006424,
							["damage_from"] = {
								["[*] Chilling Winds"] = true,
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 7518,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 251,
										["targets"] = {
											["Chillbone Gnawer"] = 725,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 725,
										["n_min"] = 234,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 725,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 236,
										["targets"] = {
											["Chillbone Gnawer"] = 468,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 468,
										["n_min"] = 232,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 468,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[295367] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 272,
										["targets"] = {
											["Chillbone Gnawer"] = 815,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 815,
										["n_min"] = 271,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 815,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 295367,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 565,
										["targets"] = {
											["Chillbone Gnawer"] = 565,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 565,
										["n_min"] = 565,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 565,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 2672,
										["g_amt"] = 0,
										["n_max"] = 1323,
										["targets"] = {
											["Chillbone Gnawer"] = 3995,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1323,
										["n_min"] = 1323,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 3995,
										["c_max"] = 2672,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 2672,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 724,
										["targets"] = {
											["Chillbone Gnawer"] = 724,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 724,
										["n_min"] = 724,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 724,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7518.006424,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 7518.006424,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["end_time"] = 1605285376,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285376,
							["last_dps"] = 1021.884793258126,
							["start_time"] = 1605285369,
							["delay"] = 0,
							["damage_taken"] = 2614.006424,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 269.007773,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Eadric the Pure"] = 335,
								["Thorddin"] = 1477,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB4C6",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1812.007773,
							["damage_taken"] = 7518.007772999999,
							["monster"] = true,
							["end_time"] = 1605285410,
							["on_hold"] = false,
							["last_event"] = 1605285410,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 644,
										["g_amt"] = 0,
										["n_max"] = 335,
										["targets"] = {
											["Eadric the Pure"] = 335,
											["Thorddin"] = 1477,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1168,
										["n_min"] = 269,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 1812,
										["c_max"] = 644,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 644,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 644,
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285369,
							["delay"] = 0,
							["total"] = 1812.007773,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1589,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.004921,
							["total_without_pet"] = 868.004921,
							["total"] = 868.004921,
							["targets_absorbs"] = {
								["Thorddin"] = 868,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 868.004921,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 868,
							},
							["totalover_without_pet"] = 0.004921,
							["healing_taken"] = 868.004921,
							["fight_component"] = true,
							["end_time"] = 1605285376,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 868,
										["targets_overheal"] = {
										},
										["n_max"] = 599,
										["targets"] = {
											["Thorddin"] = 868,
										},
										["n_min"] = 269,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 868,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 868,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 868,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285369,
							["custom"] = 0,
							["last_event"] = 1605285372,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.004921,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1589,
					["_ActorTable"] = {
						{
							["received"] = 0.006367,
							["resource"] = 6.006367,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.006367,
							["fight_component"] = true,
							["total"] = 0.006367,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006367,
							["tipo"] = 3,
							["last_event"] = 1605285410,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.006367,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1589,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 7,
										["id"] = 295367,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 32,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 3,
										["targets"] = {
										},
										["activedamt"] = 3,
										["uptime"] = 7,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 267611,
										["refreshamt"] = 2,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 7,
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[24275] = 1,
								[35395] = 2,
								[85256] = 2,
								[184575] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285376,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1589,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12500.466,
				["tempo_start"] = 1605285369,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1907,
				["playing_solo"] = true,
				["totals"] = {
					9329.981783999998, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Memmonk"] = {
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285382.375, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285382.896, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285383.393, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [3]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285383.895, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [4]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285384.382, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [5]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285384.807, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [6]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285385.385, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [7]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285385.889, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [8]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285392.363, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [9]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285392.877, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [10]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285393.386, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [11]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285393.892, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [12]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285394.351, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [13]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285394.907, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [14]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285395.374, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [15]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285395.934, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [16]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285402.429, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [17]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285402.927, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [18]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285403.423, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [19]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285403.879, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [20]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285404.424, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [21]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285404.921, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [22]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285405.421, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [23]
						{
							true, -- [1]
							124255, -- [2]
							2, -- [3]
							1605285405.93, -- [4]
							0, -- [5]
							"Memmonk", -- [6]
							nil, -- [7]
							1, -- [8]
							true, -- [9]
							-1, -- [10]
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 25,
					},
					["Thorddin"] = {
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285381.873, -- [4]
							10883, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285391.894, -- [4]
							11088, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							326788, -- [2]
							379, -- [3]
							1605285401.911, -- [4]
							11520, -- [5]
							"[*] Chilling Winds", -- [6]
							nil, -- [7]
							16, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 4,
					},
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:36:17",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 12467.164,
				["CombatEndedAt"] = 12467.164,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 868.004921,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 7518.006424,
						}, -- [1]
					},
				},
				["end_time"] = 12467.164,
				["combat_id"] = 1589,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					7518, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12459.807,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:36:10",
			}, -- [8]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1588,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005181,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 6959,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 469,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 469,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 469,
										["c_max"] = 469,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 469,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[255937] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 3823,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 3823,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 3823,
										["c_max"] = 3823,
										["spellschool"] = 6,
										["id"] = 255937,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 3823,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 439,
										["targets"] = {
											["Chillbone Gnawer"] = 439,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 439,
										["n_min"] = 439,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 439,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1449,
										["targets"] = {
											["Chillbone Gnawer"] = 1449,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1449,
										["n_min"] = 1449,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1449,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 553,
										["targets"] = {
											["Chillbone Gnawer"] = 553,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 553,
										["n_min"] = 553,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 553,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6959.005181,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 6959.005181,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["end_time"] = 1605285365,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285365,
							["last_dps"] = 1713.196745691821,
							["start_time"] = 1605285361,
							["delay"] = 0,
							["damage_taken"] = 306.005181,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 306.003678,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 306,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB45B",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 306.003678,
							["damage_taken"] = 6959.003678,
							["monster"] = true,
							["end_time"] = 1605285365,
							["on_hold"] = false,
							["last_event"] = 1605285364,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 306,
										["targets"] = {
											["Thorddin"] = 306,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 306,
										["n_min"] = 306,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 306,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285364,
							["delay"] = 0,
							["total"] = 306.003678,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1588,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.00898,
							["total_without_pet"] = 306.00898,
							["total"] = 306.00898,
							["targets_absorbs"] = {
								["Thorddin"] = 306,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 306.00898,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 306,
							},
							["totalover_without_pet"] = 0.00898,
							["healing_taken"] = 306.00898,
							["fight_component"] = true,
							["end_time"] = 1605285365,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 306,
										["targets_overheal"] = {
										},
										["n_max"] = 306,
										["targets"] = {
											["Thorddin"] = 306,
										},
										["n_min"] = 306,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 306,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 306,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 306,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285364,
							["custom"] = 0,
							["last_event"] = 1605285364,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.00898,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1588,
					["_ActorTable"] = {
						{
							["received"] = 0.00598,
							["resource"] = 4.00598,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.00598,
							["fight_component"] = true,
							["total"] = 0.00598,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00598,
							["tipo"] = 3,
							["last_event"] = 1605285365,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.00598,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1588,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 2,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[255941] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 0,
										["id"] = 255941,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 17,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 269279,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 2,
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[255937] = 1,
								[20271] = 1,
								[85256] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285365,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1588,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12459.038,
				["tempo_start"] = 1605285361,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1906,
				["playing_solo"] = true,
				["totals"] = {
					7265, -- [1]
					306, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:36:06",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 12456.213,
				["CombatEndedAt"] = 12456.213,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 306.00898,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6959.005181,
						}, -- [1]
					},
				},
				["end_time"] = 12456.213,
				["combat_id"] = 1588,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6959, -- [1]
					306, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12451.71,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:36:02",
			}, -- [9]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1587,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006018,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 6641,
							},
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["damage_taken"] = 691.006018,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 6641.006018,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["end_time"] = 1605285294,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 256,
										["targets"] = {
											["Chillbone Gnawer"] = 490,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 490,
										["n_min"] = 234,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 490,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 240,
										["targets"] = {
											["Chillbone Gnawer"] = 473,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 473,
										["n_min"] = 233,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 473,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 453,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 453,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 453,
										["c_max"] = 453,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 453,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 539,
										["targets"] = {
											["Chillbone Gnawer"] = 539,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 539,
										["n_min"] = 539,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 539,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1627,
										["targets"] = {
											["Chillbone Gnawer"] = 3082,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3082,
										["n_min"] = 1455,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 3082,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 791,
										["targets"] = {
											["Chillbone Gnawer"] = 791,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 791,
										["n_min"] = 791,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 791,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[295367] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 271,
										["targets"] = {
											["Chillbone Gnawer"] = 813,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 813,
										["n_min"] = 271,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 813,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 295367,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["total"] = 6641.006018,
							["last_event"] = 1605285294,
							["colocacao"] = 1,
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 965.6835855750483,
							["start_time"] = 1605285287,
							["delay"] = 0,
							["spec"] = 65,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.00829,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Undercity Champion"] = 10499,
								["Thorddin"] = 691,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB4E2",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 11190.00829,
							["damage_taken"] = 6641.00829,
							["monster"] = true,
							["total"] = 11190.00829,
							["on_hold"] = false,
							["tipo"] = 1,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1462,
										["g_amt"] = 0,
										["n_max"] = 426,
										["targets"] = {
											["Undercity Champion"] = 10499,
											["Thorddin"] = 691,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9728,
										["a_amt"] = 1,
										["g_dmg"] = 0,
										["n_min"] = 294,
										["MISS"] = 3,
										["counter"] = 36,
										["b_dmg"] = 0,
										["r_amt"] = 0,
										["c_max"] = 841,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["a_dmg"] = 347,
										["m_amt"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 621,
										["successful_casted"] = 0,
										["extra"] = {
										},
										["n_amt"] = 27,
										["total"] = 11190,
										["DODGE"] = 3,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["aID"] = "166004",
							["dps_started"] = false,
							["friendlyfire_total"] = 0,
							["custom"] = 0,
							["last_event"] = 1605285357,
							["last_dps"] = 0,
							["start_time"] = 1605285288,
							["delay"] = 0,
							["end_time"] = 1605285361,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1587,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.002059,
							["total_without_pet"] = 226.002059,
							["total"] = 226.002059,
							["targets_absorbs"] = {
								["Thorddin"] = 226,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 226.002059,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 226,
							},
							["totalover_without_pet"] = 0.002059,
							["healing_taken"] = 226.002059,
							["fight_component"] = true,
							["end_time"] = 1605285294,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 226,
										["targets_overheal"] = {
										},
										["n_max"] = 226,
										["targets"] = {
											["Thorddin"] = 226,
										},
										["n_min"] = 226,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 226,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 226,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 226,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285288,
							["custom"] = 0,
							["last_event"] = 1605285288,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.002059,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1587,
					["_ActorTable"] = {
						{
							["received"] = 0.00544,
							["resource"] = 5.00544,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.00544,
							["fight_component"] = true,
							["total"] = 0.00544,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00544,
							["tipo"] = 3,
							["last_event"] = 1605285361,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.00544,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1587,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 7,
										["id"] = 295367,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 48,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 267611,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[268534] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 268534,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[280204] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 280204,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 8,
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[24275] = 1,
								[35395] = 2,
								[85256] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285294,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1587,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12451.71,
				["tempo_start"] = 1605285287,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1905,
				["playing_solo"] = true,
				["totals"] = {
					17830.994754, -- [1]
					226, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:34:55",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 12384.857,
				["CombatEndedAt"] = 12384.857,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 226.002059,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6641.006018,
						}, -- [1]
					},
				},
				["end_time"] = 12384.857,
				["combat_id"] = 1587,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6641, -- [1]
					226, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12377.98,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:34:48",
			}, -- [10]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1586,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00351,
							["damage_from"] = {
								["Darksworn Sentry"] = true,
							},
							["targets"] = {
								["Darksworn Sentry"] = 6670,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 252,
										["targets"] = {
											["Darksworn Sentry"] = 964,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 964,
										["n_min"] = 231,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 964,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 603,
										["targets"] = {
											["Darksworn Sentry"] = 603,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 603,
										["n_min"] = 603,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 603,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[53385] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 983,
										["targets"] = {
											["Darksworn Sentry"] = 983,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 983,
										["n_min"] = 983,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 983,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 53385,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 453,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Darksworn Sentry"] = 905,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 452,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 905,
										["c_max"] = 453,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 453,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 442,
										["targets"] = {
											["Darksworn Sentry"] = 442,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 442,
										["n_min"] = 442,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 442,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1574,
										["targets"] = {
											["Darksworn Sentry"] = 1574,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1574,
										["n_min"] = 1574,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1574,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 730,
										["targets"] = {
											["Darksworn Sentry"] = 730,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 730,
										["n_min"] = 730,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 730,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 235,
										["targets"] = {
											["Darksworn Sentry"] = 469,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 469,
										["n_min"] = 234,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 469,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6670.003510000001,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 6670.003510000001,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605285283,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285283,
							["last_dps"] = 585.85889415897,
							["start_time"] = 1605285271,
							["delay"] = 0,
							["damage_taken"] = 998.00351,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 998.006423,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 998,
							},
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EB52B",
							["pets"] = {
							},
							["total"] = 998.006423,
							["monster"] = true,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 998.006423,
							["damage_taken"] = 6670.006423,
							["dps_started"] = false,
							["end_time"] = 1605285283,
							["on_hold"] = false,
							["last_event"] = 1605285281,
							["nome"] = "Darksworn Sentry",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 356,
										["targets"] = {
											["Thorddin"] = 708,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 708,
										["n_min"] = 352,
										["g_dmg"] = 0,
										["counter"] = 3,
										["a_amt"] = 0,
										["total"] = 708,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
									[16564] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 290,
										["targets"] = {
											["Thorddin"] = 290,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 290,
										["n_min"] = 290,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 290,
										["c_max"] = 0,
										["id"] = 16564,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285274,
							["delay"] = 0,
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1586,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.002707,
							["total_without_pet"] = 998.002707,
							["total"] = 998.002707,
							["targets_absorbs"] = {
								["Thorddin"] = 998,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 998.002707,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 998,
							},
							["totalover_without_pet"] = 0.002707,
							["healing_taken"] = 998.002707,
							["fight_component"] = true,
							["end_time"] = 1605285283,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 998,
										["targets_overheal"] = {
										},
										["n_max"] = 356,
										["targets"] = {
											["Thorddin"] = 998,
										},
										["n_min"] = 290,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 998,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 998,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 998,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285274,
							["custom"] = 0,
							["last_event"] = 1605285279,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.002707,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1586,
					["_ActorTable"] = {
						{
							["received"] = 0.004078,
							["resource"] = 5.004078,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.004078,
							["fight_component"] = true,
							["total"] = 0.004078,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.004078,
							["tipo"] = 3,
							["last_event"] = 1605285287,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.004078,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1586,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 2,
										["id"] = 295367,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 73,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 12,
										["id"] = 269279,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 1,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 12,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[268534] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 268534,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 12,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["activedamt"] = 1,
										["id"] = 231843,
										["targets"] = {
										},
										["actived_at"] = 1605285282,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 12,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 3,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[85256] = 1,
								[35395] = 2,
								[20271] = 1,
								[24275] = 1,
								[53385] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285283,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Darksworn Sentry",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[16564] = 1,
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["aID"] = "",
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EB52B",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1586,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12377.469,
				["tempo_start"] = 1605285271,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1904,
				["playing_solo"] = true,
				["totals"] = {
					7668, -- [1]
					998, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:34:44",
				["cleu_timeline"] = {
				},
				["enemy"] = "Darksworn Sentry",
				["TotalElapsedCombatTime"] = 12373.518,
				["CombatEndedAt"] = 12373.518,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 998.002707,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6670.003510000001,
						}, -- [1]
					},
				},
				["end_time"] = 12373.518,
				["combat_id"] = 1586,
				["frags"] = {
					["Darksworn Sentry"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6670, -- [1]
					998, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12362.133,
				["contra"] = "Darksworn Sentry",
				["data_inicio"] = "11:34:32",
			}, -- [11]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1585,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005131,
							["damage_from"] = {
								["Darksworn Sentry"] = true,
							},
							["targets"] = {
								["Darksworn Sentry"] = 7166,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 979,
										["g_amt"] = 0,
										["n_max"] = 254,
										["targets"] = {
											["Darksworn Sentry"] = 1233,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 254,
										["n_min"] = 254,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1233,
										["c_max"] = 515,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 464,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 236,
										["targets"] = {
											["Darksworn Sentry"] = 236,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 236,
										["n_min"] = 236,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 236,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 549,
										["targets"] = {
											["Darksworn Sentry"] = 549,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 549,
										["n_min"] = 549,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 549,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1319,
										["targets"] = {
											["Darksworn Sentry"] = 2481,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2481,
										["n_min"] = 1162,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2481,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1434,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Darksworn Sentry"] = 1434,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1434,
										["c_max"] = 1434,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1434,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[53385] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1233,
										["targets"] = {
											["Darksworn Sentry"] = 1233,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1233,
										["n_min"] = 1233,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1233,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 53385,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7166.005131,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 7166.005131,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605285218,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285217,
							["last_dps"] = 862.3351541516774,
							["start_time"] = 1605285210,
							["delay"] = 0,
							["damage_taken"] = 996.005131,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 659.0013309999999,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 996,
							},
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EAE42",
							["pets"] = {
							},
							["total"] = 996.0013309999999,
							["monster"] = true,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 996.0013309999999,
							["damage_taken"] = 7166.001331,
							["dps_started"] = false,
							["end_time"] = 1605285218,
							["on_hold"] = false,
							["last_event"] = 1605285216,
							["nome"] = "Darksworn Sentry",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 360,
										["targets"] = {
											["Thorddin"] = 697,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 697,
										["n_min"] = 337,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 697,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 337,
										["r_amt"] = 0,
									}, -- [1]
									[16564] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 299,
										["targets"] = {
											["Thorddin"] = 299,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 299,
										["n_min"] = 299,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 299,
										["c_max"] = 0,
										["id"] = 16564,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 1,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285212,
							["delay"] = 0,
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1585,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.001914,
							["total_without_pet"] = 939.0019139999999,
							["total"] = 939.0019139999999,
							["targets_absorbs"] = {
								["Thorddin"] = 939,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 939.0019139999999,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 939,
							},
							["totalover_without_pet"] = 0.001914,
							["healing_taken"] = 939.0019139999999,
							["fight_component"] = true,
							["end_time"] = 1605285218,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 939,
										["targets_overheal"] = {
										},
										["n_max"] = 360,
										["targets"] = {
											["Thorddin"] = 939,
										},
										["n_min"] = 280,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 939,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 939,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 939,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285212,
							["custom"] = 0,
							["last_event"] = 1605285216,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.001914,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1585,
					["_ActorTable"] = {
						{
							["received"] = 0.003774,
							["resource"] = 5.003774,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.003774,
							["fight_component"] = true,
							["total"] = 0.003774,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003774,
							["tipo"] = 3,
							["last_event"] = 1605285271,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.003774,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1585,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 55,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 7,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[280192] = {
										["activedamt"] = 1,
										["id"] = 280192,
										["targets"] = {
										},
										["actived_at"] = 1605285210,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 267611,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[280204] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 280204,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 5,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[221883] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 0,
										["id"] = 221883,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 1,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[85256] = 2,
								[53385] = 1,
								[20271] = 1,
								[24275] = 1,
								[35395] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285218,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Darksworn Sentry",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[16564] = 1,
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["aID"] = "",
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EAE42",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1585,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12362.133,
				["tempo_start"] = 1605285210,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1903,
				["playing_solo"] = true,
				["totals"] = {
					8162, -- [1]
					939, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:33:39",
				["cleu_timeline"] = {
				},
				["enemy"] = "Darksworn Sentry",
				["TotalElapsedCombatTime"] = 8.30999999999949,
				["CombatEndedAt"] = 12309.029,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 939.0019139999999,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 7166.005131,
						}, -- [1]
					},
				},
				["end_time"] = 12309.029,
				["combat_id"] = 1585,
				["frags"] = {
					["Darksworn Sentry"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					7166, -- [1]
					939, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12300.719,
				["contra"] = "Darksworn Sentry",
				["data_inicio"] = "11:33:31",
			}, -- [12]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1584,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003624,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 6604,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 252,
										["targets"] = {
											["Chillbone Gnawer"] = 495,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 495,
										["n_min"] = 243,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 495,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[255937] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1882,
										["targets"] = {
											["Chillbone Gnawer"] = 1882,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1882,
										["n_min"] = 1882,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1882,
										["c_max"] = 0,
										["spellschool"] = 6,
										["id"] = 255937,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 237,
										["targets"] = {
											["Chillbone Gnawer"] = 237,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 237,
										["n_min"] = 237,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 237,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 452,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 452,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 452,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 455,
										["targets"] = {
											["Chillbone Gnawer"] = 455,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 455,
										["n_min"] = 455,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 455,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1434,
										["targets"] = {
											["Chillbone Gnawer"] = 1434,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1434,
										["n_min"] = 1434,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1434,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 563,
										["targets"] = {
											["Chillbone Gnawer"] = 563,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 563,
										["n_min"] = 563,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 563,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[295367] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 544,
										["g_amt"] = 0,
										["n_max"] = 271,
										["targets"] = {
											["Chillbone Gnawer"] = 1086,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 542,
										["n_min"] = 271,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1086,
										["c_max"] = 544,
										["spellschool"] = 4,
										["id"] = 295367,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 544,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6604.003624,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 6604.003624,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605285206,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605285205,
							["last_dps"] = 941.4117781896718,
							["start_time"] = 1605285199,
							["delay"] = 0,
							["damage_taken"] = 289.003624,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 289.00199,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Darnassus Champion"] = 1132,
								["Thorddin"] = 289,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EB517",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1421.00199,
							["damage_taken"] = 6604.00199,
							["monster"] = true,
							["end_time"] = 1605285210,
							["on_hold"] = false,
							["last_event"] = 1605285208,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 425,
										["targets"] = {
											["Darnassus Champion"] = 1132,
											["Thorddin"] = 289,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1421,
										["g_dmg"] = 0,
										["n_min"] = 289,
										["MISS"] = 1,
										["counter"] = 6,
										["a_amt"] = 0,
										["r_amt"] = 0,
										["c_max"] = 0,
										["b_dmg"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["m_amt"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_dmg"] = 0,
										["n_amt"] = 4,
										["spellschool"] = 1,
										["total"] = 1421,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605285202,
							["delay"] = 0,
							["total"] = 1421.00199,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1584,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.003974,
							["total_without_pet"] = 289.003974,
							["total"] = 289.003974,
							["targets_absorbs"] = {
								["Thorddin"] = 289,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 289.003974,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 289,
							},
							["totalover_without_pet"] = 0.003974,
							["healing_taken"] = 289.003974,
							["fight_component"] = true,
							["end_time"] = 1605285206,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 289,
										["targets_overheal"] = {
										},
										["n_max"] = 289,
										["targets"] = {
											["Thorddin"] = 289,
										},
										["n_min"] = 289,
										["counter"] = 1,
										["overheal"] = 0,
										["total"] = 289,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 289,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 289,
										["n_amt"] = 1,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605285202,
							["custom"] = 0,
							["last_event"] = 1605285202,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.003974,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1584,
					["_ActorTable"] = {
						{
							["received"] = 0.008788,
							["resource"] = 5.008788,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.008788,
							["fight_component"] = true,
							["total"] = 0.008788,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.008788,
							["tipo"] = 3,
							["last_event"] = 1605285204,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.008788,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1584,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[255937] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 255937,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 3,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[255941] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 255941,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 6,
										["id"] = 295367,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 34,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 11,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[255937] = 1,
								[35395] = 1,
								[85256] = 1,
								[20271] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605285206,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1584,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 12289.411,
				["tempo_start"] = 1605285199,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1902,
				["playing_solo"] = true,
				["totals"] = {
					8024.998813, -- [1]
					289, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:33:26",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 7.014999999999418,
				["CombatEndedAt"] = 12296.426,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 289.003974,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6604.003624,
						}, -- [1]
					},
				},
				["end_time"] = 12296.426,
				["combat_id"] = 1584,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6604, -- [1]
					289, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12289.411,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:33:19",
			}, -- [13]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1583,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002042,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 7594,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 256,
										["targets"] = {
											["Chillbone Gnawer"] = 995,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 995,
										["n_min"] = 238,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 995,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[20271] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1099,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 1099,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1099,
										["c_max"] = 1099,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1099,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[53385] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 995,
										["targets"] = {
											["Chillbone Gnawer"] = 995,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 995,
										["n_min"] = 995,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 995,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 53385,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 571,
										["targets"] = {
											["Chillbone Gnawer"] = 571,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 571,
										["n_min"] = 571,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 571,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1351,
										["targets"] = {
											["Chillbone Gnawer"] = 2495,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2495,
										["n_min"] = 1144,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2495,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 732,
										["targets"] = {
											["Chillbone Gnawer"] = 732,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 732,
										["n_min"] = 732,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 732,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 481,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 481,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 481,
										["c_max"] = 481,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 481,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7594.002042,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 7594.002042,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605284903,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605284903,
							["last_dps"] = 758.5657818399441,
							["start_time"] = 1605284893,
							["delay"] = 0,
							["damage_taken"] = 1199.002042,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 546.007549,
							["damage_from"] = {
								["Thunder Bluff Champion"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 1199,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EAD99",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1199.007549,
							["damage_taken"] = 8684.007549,
							["monster"] = true,
							["end_time"] = 1605284903,
							["on_hold"] = false,
							["last_event"] = 1605284902,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 331,
										["targets"] = {
											["Thorddin"] = 1199,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1199,
										["n_min"] = 245,
										["g_dmg"] = 0,
										["counter"] = 5,
										["a_amt"] = 1,
										["total"] = 1199,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["b_dmg"] = 0,
										["a_dmg"] = 331,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["extra"] = {
										},
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605284894,
							["delay"] = 0,
							["total"] = 1199.007549,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1583,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.007404,
							["total_without_pet"] = 868.007404,
							["total"] = 868.007404,
							["targets_absorbs"] = {
								["Thorddin"] = 868,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 868.007404,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 868,
							},
							["totalover_without_pet"] = 0.007404,
							["healing_taken"] = 868.007404,
							["fight_component"] = true,
							["end_time"] = 1605284903,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 868,
										["targets_overheal"] = {
										},
										["n_max"] = 322,
										["targets"] = {
											["Thorddin"] = 868,
										},
										["n_min"] = 245,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 868,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 868,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 868,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605284894,
							["custom"] = 0,
							["last_event"] = 1605284898,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.007404,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1583,
					["_ActorTable"] = {
						{
							["received"] = 0.003951,
							["resource"] = 7.003951,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.003951,
							["fight_component"] = true,
							["total"] = 0.003951,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.003951,
							["tipo"] = 3,
							["last_event"] = 1605285199,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.003951,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1583,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 1,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 71,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 6,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 9,
										["id"] = 267611,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[280204] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 8,
										["id"] = 280204,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 10,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 231843,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 1,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[85256] = 1,
								[35395] = 1,
								[184575] = 1,
								[20271] = 1,
								[24275] = 1,
								[53385] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605284903,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1583,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1901,
				["playing_solo"] = true,
				["totals"] = {
					8792.995804, -- [1]
					867.995255, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:28:24",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 11994.199,
				["CombatEndedAt"] = 11994.199,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 868.007404,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 7594.002042,
						}, -- [1]
					},
				},
				["end_time"] = 11994.199,
				["combat_id"] = 1583,
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1605284893,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["contra"] = "Chillbone Gnawer",
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					7594, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 11984.188,
				["TimeData"] = {
				},
				["data_inicio"] = "11:28:14",
			}, -- [14]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1582,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008598,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 7865,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 252,
										["targets"] = {
											["Chillbone Gnawer"] = 974,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 974,
										["n_min"] = 238,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 974,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[255937] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1916,
										["targets"] = {
											["Chillbone Gnawer"] = 1916,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1916,
										["n_min"] = 1916,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1916,
										["c_max"] = 0,
										["spellschool"] = 6,
										["id"] = 255937,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 482,
										["g_amt"] = 0,
										["n_max"] = 241,
										["targets"] = {
											["Chillbone Gnawer"] = 961,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 479,
										["n_min"] = 238,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 961,
										["c_max"] = 482,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 482,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 226,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 226,
										["c_max"] = 0,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 445,
										["targets"] = {
											["Chillbone Gnawer"] = 445,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 445,
										["n_min"] = 445,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 445,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1453,
										["targets"] = {
											["Chillbone Gnawer"] = 2797,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2797,
										["n_min"] = 1344,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2797,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 546,
										["targets"] = {
											["Chillbone Gnawer"] = 546,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 546,
										["n_min"] = 546,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 546,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7865.008598,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 7865.008598,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605284889,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605284888,
							["last_dps"] = 735.391173258552,
							["start_time"] = 1605284878,
							["delay"] = 0,
							["damage_taken"] = 764.008598,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 764.008116,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 764,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EAE3F",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 764.008116,
							["damage_taken"] = 7865.008116,
							["monster"] = true,
							["end_time"] = 1605284889,
							["on_hold"] = false,
							["last_event"] = 1605284887,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 270,
										["targets"] = {
											["Thorddin"] = 764,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 764,
										["g_dmg"] = 0,
										["n_min"] = 245,
										["a_amt"] = 0,
										["counter"] = 5,
										["DODGE"] = 1,
										["r_amt"] = 0,
										["c_max"] = 0,
										["b_dmg"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["m_amt"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["a_dmg"] = 0,
										["n_amt"] = 3,
										["spellschool"] = 1,
										["total"] = 764,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605284879,
							["delay"] = 0,
							["total"] = 764.008116,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1582,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.003864,
							["total_without_pet"] = 764.003864,
							["total"] = 764.003864,
							["targets_absorbs"] = {
								["Thorddin"] = 764,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 764.003864,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 764,
							},
							["totalover_without_pet"] = 0.003864,
							["healing_taken"] = 764.003864,
							["fight_component"] = true,
							["end_time"] = 1605284889,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 764,
										["targets_overheal"] = {
										},
										["n_max"] = 270,
										["targets"] = {
											["Thorddin"] = 764,
										},
										["n_min"] = 245,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 764,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 764,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 764,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605284879,
							["custom"] = 0,
							["last_event"] = 1605284885,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.003864,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1582,
					["_ActorTable"] = {
						{
							["received"] = 0.007931,
							["resource"] = 8.007931,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.007931,
							["fight_component"] = true,
							["total"] = 0.007931,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007931,
							["tipo"] = 3,
							["last_event"] = 1605284888,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.007931,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1582,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 3,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[255941] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 0,
										["id"] = 255941,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 59,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 11,
										["id"] = 269279,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 11,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 7,
										["id"] = 267611,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[280204] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 2,
										["id"] = 280204,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 11,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 286393,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["activedamt"] = 1,
										["id"] = 231843,
										["targets"] = {
										},
										["actived_at"] = 1605284886,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 11,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 3,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[255937] = 1,
								[35395] = 3,
								[85256] = 2,
								[184575] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605284889,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1582,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 11983.249,
				["tempo_start"] = 1605284878,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1900,
				["playing_solo"] = true,
				["totals"] = {
					8629, -- [1]
					764, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:28:10",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 11979.579,
				["CombatEndedAt"] = 11979.579,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 764.003864,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 7865.008598,
						}, -- [1]
					},
				},
				["end_time"] = 11979.579,
				["combat_id"] = 1582,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					7865, -- [1]
					764, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 11968.559,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:27:59",
			}, -- [15]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1581,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008957,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 6304,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 255,
										["targets"] = {
											["Chillbone Gnawer"] = 494,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 494,
										["n_min"] = 239,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 494,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[295367] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 271,
										["targets"] = {
											["Chillbone Gnawer"] = 542,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 542,
										["n_min"] = 271,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 542,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 295367,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 556,
										["targets"] = {
											["Chillbone Gnawer"] = 556,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 556,
										["n_min"] = 556,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 556,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 2855,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 2855,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 2855,
										["c_max"] = 2855,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 2855,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1122,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 1122,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 1122,
										["c_max"] = 1122,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 1122,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[24275] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 735,
										["targets"] = {
											["Chillbone Gnawer"] = 735,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 735,
										["n_min"] = 735,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 735,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 24275,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 6304.008957,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 6304.008957,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["end_time"] = 1605284744,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605284743,
							["last_dps"] = 1140.584215125737,
							["start_time"] = 1605284738,
							["delay"] = 0,
							["damage_taken"] = 628.008957,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 628.004413,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 628,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EAE05",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 628.004413,
							["damage_taken"] = 6304.004413000001,
							["monster"] = true,
							["end_time"] = 1605284744,
							["on_hold"] = false,
							["last_event"] = 1605284741,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 337,
										["targets"] = {
											["Thorddin"] = 628,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 628,
										["n_min"] = 291,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 628,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605284739,
							["delay"] = 0,
							["total"] = 628.004413,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1581,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.00897,
							["total_without_pet"] = 628.00897,
							["total"] = 628.00897,
							["targets_absorbs"] = {
								["Thorddin"] = 628,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 628.00897,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 628,
							},
							["totalover_without_pet"] = 0.00897,
							["healing_taken"] = 628.00897,
							["fight_component"] = true,
							["end_time"] = 1605284744,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 70,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 628,
										["targets_overheal"] = {
										},
										["n_max"] = 337,
										["targets"] = {
											["Thorddin"] = 628,
										},
										["n_min"] = 291,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 628,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 628,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 628,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605284739,
							["custom"] = 0,
							["last_event"] = 1605284741,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.00897,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1581,
					["_ActorTable"] = {
						{
							["received"] = 0.007698,
							["resource"] = 4.007698,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.007698,
							["fight_component"] = true,
							["total"] = 0.007698,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 70,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007698,
							["tipo"] = 3,
							["last_event"] = 1605284877,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.007698,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1581,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 3,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 295367,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 27,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 0,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 6,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 8,
							["nome"] = "Thorddin",
							["spec"] = 70,
							["grupo"] = true,
							["spell_cast"] = {
								[24275] = 1,
								[184575] = 1,
								[85256] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605284744,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1581,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 11967.742,
				["tempo_start"] = 1605284738,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1899,
				["playing_solo"] = true,
				["totals"] = {
					6932, -- [1]
					628, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:25:44",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 11834.314,
				["CombatEndedAt"] = 11834.314,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 628.00897,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6304.008957,
						}, -- [1]
					},
				},
				["end_time"] = 11834.314,
				["combat_id"] = 1581,
				["frags"] = {
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					6304, -- [1]
					628, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 11828.787,
				["contra"] = "Chillbone Gnawer",
				["data_inicio"] = "11:25:39",
			}, -- [16]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1580,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003366,
							["damage_from"] = {
								["Darksworn Sentry"] = true,
							},
							["targets"] = {
								["Darksworn Sentry"] = 7564,
							},
							["pets"] = {
							},
							["classe"] = "PALADIN",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 508,
										["g_amt"] = 0,
										["n_max"] = 251,
										["targets"] = {
											["Darksworn Sentry"] = 1251,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 743,
										["n_min"] = 244,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1251,
										["c_max"] = 508,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 508,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[35395] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 468,
										["g_amt"] = 0,
										["n_max"] = 237,
										["targets"] = {
											["Darksworn Sentry"] = 705,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 237,
										["n_min"] = 237,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 705,
										["c_max"] = 468,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 468,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 906,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Darksworn Sentry"] = 906,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 906,
										["c_max"] = 453,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 453,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 455,
										["targets"] = {
											["Darksworn Sentry"] = 455,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 455,
										["n_min"] = 455,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 455,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[224266] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1616,
										["targets"] = {
											["Darksworn Sentry"] = 3078,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3078,
										["n_min"] = 1462,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 3078,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[20271] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 611,
										["targets"] = {
											["Darksworn Sentry"] = 1169,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1169,
										["n_min"] = 558,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1169,
										["c_max"] = 0,
										["spellschool"] = 2,
										["id"] = 20271,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 7564.003366,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["total"] = 7564.003366,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["end_time"] = 1605284729,
							["tipo"] = 1,
							["colocacao"] = 1,
							["custom"] = 0,
							["last_event"] = 1605284729,
							["last_dps"] = 554.8711389378094,
							["start_time"] = 1605284716,
							["delay"] = 0,
							["damage_taken"] = 1800.003366,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 600.00112,
							["damage_from"] = {
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 1800,
							},
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EADCB",
							["pets"] = {
							},
							["total"] = 1800.00112,
							["monster"] = true,
							["aID"] = "",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1800.00112,
							["damage_taken"] = 7564.00112,
							["dps_started"] = false,
							["end_time"] = 1605284729,
							["on_hold"] = false,
							["last_event"] = 1605284727,
							["nome"] = "Darksworn Sentry",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 329,
										["targets"] = {
											["Thorddin"] = 1200,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1200,
										["n_min"] = 284,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1200,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 329,
										["r_amt"] = 0,
									}, -- [1]
									[16564] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 312,
										["targets"] = {
											["Thorddin"] = 600,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 600,
										["n_min"] = 288,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 600,
										["c_max"] = 0,
										["id"] = 16564,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 2,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["friendlyfire"] = {
							},
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605284718,
							["delay"] = 0,
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1580,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.001722,
							["total_without_pet"] = 868.001722,
							["total"] = 868.001722,
							["targets_absorbs"] = {
								["Thorddin"] = 868,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 868.001722,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 868,
							},
							["totalover_without_pet"] = 0.001722,
							["healing_taken"] = 868.001722,
							["fight_component"] = true,
							["end_time"] = 1605284729,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 868,
										["targets_overheal"] = {
										},
										["n_max"] = 312,
										["targets"] = {
											["Thorddin"] = 868,
										},
										["n_min"] = 268,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 868,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 868,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 868,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605284718,
							["custom"] = 0,
							["last_event"] = 1605284721,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.001722,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1580,
					["_ActorTable"] = {
						{
							["received"] = 0.00799,
							["resource"] = 6.00799,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.00799,
							["fight_component"] = true,
							["total"] = 0.00799,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00799,
							["tipo"] = 3,
							["last_event"] = 1605284738,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.00799,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1580,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[197277] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 5,
										["id"] = 197277,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[295367] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 2,
										["id"] = 295367,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 53,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[268534] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 268534,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 6,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 13,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 7,
							["nome"] = "Thorddin",
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[35395] = 2,
								[184575] = 1,
								[85256] = 2,
								[20271] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1605284729,
							["tipo"] = 4,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["serial"] = "Player-76-057FA942",
							["buff_uptime_targets"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68168,
							["classe"] = "UNKNOW",
							["nome"] = "Darksworn Sentry",
							["fight_component"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[16564] = 2,
							},
							["tipo"] = 4,
							["last_event"] = 0,
							["aID"] = "",
							["serial"] = "Vehicle-0-3023-571-32355-171768-00002EADCB",
							["monster"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1580,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 11828.437,
				["tempo_start"] = 1605284716,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1898,
				["playing_solo"] = true,
				["totals"] = {
					9363.99221, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:25:30",
				["cleu_timeline"] = {
				},
				["enemy"] = "Darksworn Sentry",
				["TotalElapsedCombatTime"] = 11820.097,
				["CombatEndedAt"] = 11820.097,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 868.001722,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 7564.003366,
						}, -- [1]
					},
				},
				["end_time"] = 11820.097,
				["combat_id"] = 1580,
				["frags"] = {
					["Darksworn Sentry"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					7564, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 11806.465,
				["contra"] = "Darksworn Sentry",
				["data_inicio"] = "11:25:16",
			}, -- [17]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1579,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008441,
							["damage_from"] = {
								["Chillbone Gnawer"] = true,
							},
							["targets"] = {
								["Chillbone Gnawer"] = 6803,
							},
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["damage_taken"] = 1129.008441,
							["classe"] = "PALADIN",
							["raid_targets"] = {
							},
							["total_without_pet"] = 6803.008441,
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["dps_started"] = false,
							["end_time"] = 1605284662,
							["aID"] = "76-057FA942",
							["friendlyfire"] = {
							},
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 502,
										["g_amt"] = 0,
										["n_max"] = 250,
										["targets"] = {
											["Chillbone Gnawer"] = 752,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 250,
										["n_min"] = 250,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 752,
										["c_max"] = 502,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 502,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[224266] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 4704,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Chillbone Gnawer"] = 4704,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 4704,
										["c_max"] = 2384,
										["spellschool"] = 2,
										["id"] = 224266,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 2320,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 0,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[303389] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 453,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Chillbone Gnawer"] = 679,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 226,
										["n_min"] = 226,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 679,
										["c_max"] = 453,
										["spellschool"] = 16,
										["id"] = 303389,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 453,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[35395] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 231,
										["targets"] = {
											["Chillbone Gnawer"] = 231,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 231,
										["n_min"] = 231,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 231,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 35395,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
									[184575] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 437,
										["targets"] = {
											["Chillbone Gnawer"] = 437,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 437,
										["n_min"] = 437,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 437,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 184575,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["total"] = 6803.008441,
							["last_event"] = 1605284661,
							["colocacao"] = 1,
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 683.5133568773564,
							["start_time"] = 1605284652,
							["delay"] = 0,
							["spec"] = 65,
						}, -- [1]
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 629.0087080000001,
							["damage_from"] = {
								["Thunder Bluff Champion"] = true,
								["Thorddin"] = true,
							},
							["targets"] = {
								["Thorddin"] = 1129,
							},
							["serial"] = "Creature-0-3023-571-32355-166004-00002EADDD",
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["fight_component"] = true,
							["aID"] = "166004",
							["raid_targets"] = {
							},
							["total_without_pet"] = 1129.008708,
							["damage_taken"] = 6803.008707999999,
							["monster"] = true,
							["end_time"] = 1605284662,
							["on_hold"] = false,
							["last_event"] = 1605284659,
							["nome"] = "Chillbone Gnawer",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 326,
										["targets"] = {
											["Thorddin"] = 1129,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1129,
										["n_min"] = 243,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1129,
										["c_max"] = 0,
										["spellschool"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 1,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 243,
										["r_amt"] = 0,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["classe"] = "UNKNOW",
							["custom"] = 0,
							["tipo"] = 1,
							["last_dps"] = 0,
							["start_time"] = 1605284653,
							["delay"] = 0,
							["total"] = 1129.008708,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1579,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["aID"] = "76-057FA942",
							["totalover"] = 0.007296,
							["total_without_pet"] = 868.007296,
							["total"] = 868.007296,
							["targets_absorbs"] = {
								["Thorddin"] = 868,
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-057FA942",
							["totalabsorb"] = 868.007296,
							["last_hps"] = 0,
							["targets"] = {
								["Thorddin"] = 868,
							},
							["totalover_without_pet"] = 0.007296,
							["healing_taken"] = 868.007296,
							["fight_component"] = true,
							["end_time"] = 1605284662,
							["healing_from"] = {
								["Thorddin"] = true,
							},
							["spec"] = 65,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
									[269279] = {
										["c_amt"] = 0,
										["totalabsorb"] = 868,
										["targets_overheal"] = {
										},
										["n_max"] = 326,
										["targets"] = {
											["Thorddin"] = 868,
										},
										["n_min"] = 239,
										["counter"] = 3,
										["overheal"] = 0,
										["total"] = 868,
										["c_max"] = 0,
										["id"] = 269279,
										["targets_absorbs"] = {
											["Thorddin"] = 868,
										},
										["c_curado"] = 0,
										["c_min"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 868,
										["n_amt"] = 3,
										["totaldenied"] = 0,
										["is_shield"] = true,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1605284653,
							["custom"] = 0,
							["last_event"] = 1605284657,
							["classe"] = "PALADIN",
							["totaldenied"] = 0.007296,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1579,
					["_ActorTable"] = {
						{
							["received"] = 0.007195,
							["resource"] = 4.007194999999999,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["aID"] = "76-057FA942",
							["passiveover"] = 0.007195,
							["fight_component"] = true,
							["total"] = 0.007195,
							["resource_type"] = 9,
							["nome"] = "Thorddin",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["spec"] = 65,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007195,
							["tipo"] = 3,
							["last_event"] = 1605284715,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-057FA942",
							["totalover"] = 0.007195,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1579,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["pets"] = {
							},
							["aID"] = "76-057FA942",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[465] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 465,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[64373] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 64373,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[286393] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 286393,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[281178] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 4,
										["id"] = 281178,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[231843] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 3,
										["id"] = 231843,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[186403] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 10,
										["id"] = 186403,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[267611] = {
										["appliedamt"] = 2,
										["targets"] = {
										},
										["activedamt"] = 2,
										["uptime"] = 7,
										["id"] = 267611,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[269279] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 1,
										["uptime"] = 5,
										["id"] = 269279,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["buff_uptime_targets"] = {
							},
							["spec"] = 65,
							["grupo"] = true,
							["spell_cast"] = {
								[35395] = 1,
								[85256] = 1,
								[184575] = 1,
							},
							["nome"] = "Thorddin",
							["last_event"] = 1605284662,
							["buff_uptime"] = 53,
							["tipo"] = 4,
							["serial"] = "Player-76-057FA942",
							["classe"] = "PALADIN",
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1579,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorddin"] = true,
				},
				["CombatStartedAt"] = 11806.121,
				["tempo_start"] = 1605284652,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 1897,
				["playing_solo"] = true,
				["totals"] = {
					7931.991722, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					6803, -- [1]
					868, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "11:24:23",
				["cleu_timeline"] = {
				},
				["enemy"] = "Chillbone Gnawer",
				["TotalElapsedCombatTime"] = 9.95299999999952,
				["CombatEndedAt"] = 11752.997,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "11:24:13",
				["end_time"] = 11752.997,
				["combat_id"] = 1579,
				["frags"] = {
					["Darksworn Sentry"] = 1,
					["Chillbone Gnawer"] = 1,
				},
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["TimeData"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 11743.044,
				["contra"] = "Chillbone Gnawer",
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorddin"] = 868.007296,
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorddin"] = 6803.008441,
						}, -- [1]
					},
				},
			}, -- [18]
		},
	},
	["combat_counter"] = 1916,
	["force_font_outline"] = "",
	["tabela_instancias"] = {
	},
	["coach"] = {
		["enabled"] = false,
	},
	["local_instances_config"] = {
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[2] = 2,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 105.0527954101563,
					["x"] = -894.4672241210938,
					["w"] = 264.3997497558594,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[4] = 1,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -35.5472412109375,
					["x"] = -894.4671325683594,
					["w"] = 264.3998718261719,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [2]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["isLocked"] = false,
			["is_open"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -147.2000732421875,
					["x"] = -238.400390625,
					["w"] = 320.0000305175781,
					["h"] = 130.0000610351563,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [3]
	},
	["cached_talents"] = {
	},
	["last_instance_id"] = 1712,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["active_profile"] = "Default",
	["last_realversion"] = 144,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["plugin_database"] = {
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["enabled"] = true,
			["author"] = "Details! Team",
			["level"] = 1,
			["dps"] = 0,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["last_section_selected"] = "main",
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				{
					["iType"] = "raid-TANK",
					["version"] = "v2.0",
					["options"] = {
						["iType"] = "raid-TANK",
						["name"] = "Tanks Damage Taken",
					},
					["segment_type"] = 1,
					["name"] = "Tanks Damage Taken",
					["data"] = "PRESET_TANK_TAKEN",
					["texture"] = "line",
				}, -- [4]
				{
					["version"] = "v2.0",
					["iType"] = "raid-HEALER",
					["options"] = {
						["iType"] = "raid-HEALER",
						["name"] = "Raid Healing Done",
					},
					["segment_type"] = 1,
					["name"] = "Raid Healing Done",
					["data"] = "PRESET_RAID_HEAL",
					["texture"] = "line",
				}, -- [5]
				{
					["version"] = "v2.0",
					["iType"] = "raid-HEALER",
					["options"] = {
						["iType"] = "raid-HEALER",
						["name"] = "Healing (Same Class)",
					},
					["segment_type"] = 1,
					["name"] = "Healing (Same Class)",
					["data"] = "PRESET_HEAL_SAME_CLASS",
					["texture"] = "line",
				}, -- [6]
				{
					["version"] = "v2.0",
					["iType"] = "raid-HEALER",
					["options"] = {
						["iType"] = "raid-HEALER",
						["name"] = "All Healers",
					},
					["segment_type"] = 1,
					["name"] = "All Healers",
					["data"] = "PRESET_ALL_HEALERS",
					["texture"] = "line",
				}, -- [7]
				["last_selected"] = 7,
			},
			["options"] = {
				["show_method"] = 4,
				["auto_create"] = true,
				["window_scale"] = 1,
			},
		},
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["showamount"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["animate"] = false,
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["author"] = "Details! Team",
			["realm_history"] = {
			},
			["realm_lastamt"] = 0,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["ignore_nicktag"] = false,
	["cd_tracker"] = {
		["enabled"] = false,
		["cds_enabled"] = {
		},
		["show_conditions"] = {
			["only_inside_instance"] = true,
			["only_in_group"] = true,
		},
		["pos"] = {
		},
	},
	["last_encounter"] = "Argus the Unmaker",
	["nick_tag_cache"] = {
		["last_version"] = 12,
		["nextreset"] = 1606932867,
	},
	["on_death_menu"] = true,
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "Black Rook Hold",
		["started"] = false,
		["segment_id"] = 5,
		["ej_id"] = 740,
		["started_at"] = 1528419698.7,
		["run_id"] = 2,
		["level"] = 2,
		["dungeon_zone_id"] = 1501,
		["previous_boss_killed_at"] = 1528420773,
	},
	["last_version"] = "v9.0.1.8000",
	["combat_id"] = 1596,
	["savedStyles"] = {
	},
	["last_instance_time"] = 1574297878,
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.6354529999999999,
					["damage_from"] = {
						["Trollgore"] = true,
						["[*] Plague-Tipped Arrows"] = true,
						["Vrykul Necrolord"] = true,
						["Chillbone Gnawer"] = true,
						["Vile Sludgefiend"] = true,
						["Darksworn Bonecaster"] = true,
						["Decrepit Laborer"] = true,
						["[*] Chilling Winds"] = true,
						["Frostbrood Whelp"] = true,
						["Colossal Plaguespreader"] = true,
						["[*] Rain of Arrows"] = true,
						["Plaguefang"] = true,
						["Wyrm Reanimator"] = true,
						["Risen Fiend"] = true,
						["Skeletal Footsoldier"] = true,
						["Shambling Horror"] = true,
						["Bone Giant"] = true,
						["Cult Shadowmage"] = true,
						["Putrid Experiment"] = true,
						["Lumbering Atrocity"] = true,
						["Risen Skeleton"] = true,
						["Darksworn Sentry"] = true,
					},
					["targets"] = {
						["Trollgore"] = 20119,
						["Vrykul Necrolord"] = 8596,
						["Dark Zealot"] = 62822,
						["Blight-Howl"] = 2723,
						["Darksworn Bonecaster"] = 47561,
						["Cult Shadowmage"] = 14557,
						["Dark Ritualist"] = 15182,
						["Cultist Corrupter"] = 3672,
						["Frostbrood Whelp"] = 58548,
						["Risen Skeleton"] = 54425,
						["Wyrm Reanimator"] = 42606,
						["Colossal Plaguespreader"] = 22417,
						["Putrid Atrocity"] = 2686,
						["Brittle Boneguard"] = 4590,
						["Plaguefang"] = 1250,
						["Lumbering Atrocity"] = 47461,
						["Zealous Neophyte"] = 20124,
						["Nathanos Blightcaller"] = 87508,
						["Risen Fiend"] = 9403,
						["Decrepit Laborer"] = 94162,
						["Vile Sludgefiend"] = 44035,
						["Shambling Horror"] = 12756,
						["Gloomshroom"] = 13024,
						["Skeletal Footsoldier"] = 20586,
						["Putrid Experiment"] = 45502,
						["Chillbone Gnawer"] = 414212,
						["Bone Giant"] = 44116,
						["Darksworn Sentry"] = 70488,
					},
					["delay"] = 0,
					["pets"] = {
						"Altered Ghoul <Thorddin>", -- [1]
					},
					["on_hold"] = false,
					["classe"] = "PALADIN",
					["aID"] = "76-057FA942",
					["raid_targets"] = {
					},
					["total_without_pet"] = 1184317.635453,
					["total"] = 1285131.635453,
					["dps_started"] = false,
					["end_time"] = 1605058494,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["nome"] = "Thorddin",
					["spec"] = 70,
					["grupo"] = true,
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 98,
								["b_amt"] = 0,
								["c_dmg"] = 48650,
								["g_amt"] = 0,
								["n_max"] = 325,
								["targets"] = {
									["Trollgore"] = 3208,
									["Vrykul Necrolord"] = 934,
									["Chillbone Gnawer"] = 43120,
									["Vile Sludgefiend"] = 3648,
									["Darksworn Bonecaster"] = 4945,
									["Decrepit Laborer"] = 6449,
									["Cultist Corrupter"] = 270,
									["Frostbrood Whelp"] = 5871,
									["Colossal Plaguespreader"] = 2189,
									["Brittle Boneguard"] = 498,
									["Wyrm Reanimator"] = 4736,
									["Nathanos Blightcaller"] = 7598,
									["Lumbering Atrocity"] = 5304,
									["Cult Shadowmage"] = 1511,
									["Bone Giant"] = 6369,
									["Shambling Horror"] = 644,
									["Gloomshroom"] = 1204,
									["Skeletal Footsoldier"] = 1883,
									["Putrid Experiment"] = 5597,
									["Risen Fiend"] = 1302,
									["Risen Skeleton"] = 2766,
									["Darksworn Sentry"] = 7813,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 69209,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 379,
								["total"] = 117859,
								["c_max"] = 645,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 281,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[255937] = {
								["c_amt"] = 20,
								["b_amt"] = 0,
								["c_dmg"] = 60450,
								["g_amt"] = 0,
								["n_max"] = 2076,
								["targets"] = {
									["Trollgore"] = 3914,
									["Chillbone Gnawer"] = 46779,
									["Blight-Howl"] = 1227,
									["Darksworn Bonecaster"] = 1355,
									["Decrepit Laborer"] = 20570,
									["Frostbrood Whelp"] = 4497,
									["Colossal Plaguespreader"] = 4531,
									["Brittle Boneguard"] = 3266,
									["Plaguefang"] = 1250,
									["Wyrm Reanimator"] = 4952,
									["Nathanos Blightcaller"] = 6073,
									["Skeletal Footsoldier"] = 1899,
									["Shambling Horror"] = 2883,
									["Vile Sludgefiend"] = 2573,
									["Bone Giant"] = 2037,
									["Putrid Experiment"] = 2076,
									["Lumbering Atrocity"] = 4896,
									["Risen Skeleton"] = 4624,
									["Darksworn Sentry"] = 9950,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 68902,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 69,
								["total"] = 129352,
								["c_max"] = 4896,
								["id"] = 255937,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 49,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[303389] = {
								["c_amt"] = 34,
								["b_amt"] = 0,
								["c_dmg"] = 15424,
								["g_amt"] = 0,
								["n_max"] = 237,
								["targets"] = {
									["Trollgore"] = 1143,
									["Vrykul Necrolord"] = 678,
									["Chillbone Gnawer"] = 18075,
									["Blight-Howl"] = 225,
									["Darksworn Bonecaster"] = 2486,
									["Decrepit Laborer"] = 1584,
									["Cultist Corrupter"] = 226,
									["Frostbrood Whelp"] = 2269,
									["Colossal Plaguespreader"] = 452,
									["Vile Sludgefiend"] = 226,
									["Wyrm Reanimator"] = 2028,
									["Nathanos Blightcaller"] = 3069,
									["Risen Fiend"] = 675,
									["Bone Giant"] = 1125,
									["Lumbering Atrocity"] = 225,
									["Shambling Horror"] = 225,
									["Gloomshroom"] = 679,
									["Cult Shadowmage"] = 225,
									["Putrid Experiment"] = 1582,
									["Skeletal Footsoldier"] = 1132,
									["Risen Skeleton"] = 225,
									["Darksworn Sentry"] = 3847,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 26977,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 153,
								["total"] = 42401,
								["c_max"] = 473,
								["id"] = 303389,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 119,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[53385] = {
								["c_amt"] = 42,
								["b_amt"] = 0,
								["c_dmg"] = 78189,
								["g_amt"] = 0,
								["n_max"] = 1528,
								["targets"] = {
									["Vrykul Necrolord"] = 967,
									["Chillbone Gnawer"] = 25256,
									["Vile Sludgefiend"] = 20793,
									["Darksworn Bonecaster"] = 1628,
									["Decrepit Laborer"] = 44616,
									["Cultist Corrupter"] = 2175,
									["Frostbrood Whelp"] = 17396,
									["Brittle Boneguard"] = 826,
									["Wyrm Reanimator"] = 3522,
									["Nathanos Blightcaller"] = 5767,
									["Lumbering Atrocity"] = 4188,
									["Darksworn Sentry"] = 3029,
									["Bone Giant"] = 957,
									["Shambling Horror"] = 6760,
									["Gloomshroom"] = 2875,
									["Risen Fiend"] = 3693,
									["Putrid Experiment"] = 4613,
									["Cult Shadowmage"] = 10452,
									["Risen Skeleton"] = 36807,
									["Skeletal Footsoldier"] = 2885,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 121016,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 179,
								["total"] = 199205,
								["c_max"] = 2566,
								["id"] = 53385,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 137,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[184575] = {
								["c_amt"] = 27,
								["b_amt"] = 0,
								["c_dmg"] = 26828,
								["g_amt"] = 0,
								["n_max"] = 726,
								["targets"] = {
									["Trollgore"] = 1659,
									["Vrykul Necrolord"] = 562,
									["Chillbone Gnawer"] = 32667,
									["Blight-Howl"] = 422,
									["Darksworn Bonecaster"] = 4805,
									["Decrepit Laborer"] = 5721,
									["Cultist Corrupter"] = 470,
									["Frostbrood Whelp"] = 5554,
									["Colossal Plaguespreader"] = 2728,
									["Wyrm Reanimator"] = 3611,
									["Nathanos Blightcaller"] = 6059,
									["Skeletal Footsoldier"] = 1781,
									["Lumbering Atrocity"] = 4444,
									["Bone Giant"] = 3575,
									["Shambling Horror"] = 883,
									["Gloomshroom"] = 1090,
									["Cult Shadowmage"] = 917,
									["Putrid Experiment"] = 2591,
									["Vile Sludgefiend"] = 2843,
									["Risen Skeleton"] = 1661,
									["Darksworn Sentry"] = 4577,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 61792,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 152,
								["total"] = 88620,
								["c_max"] = 1313,
								["id"] = 184575,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 125,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[24275] = {
								["c_amt"] = 9,
								["b_amt"] = 0,
								["c_dmg"] = 13432,
								["g_amt"] = 0,
								["n_max"] = 879,
								["targets"] = {
									["Trollgore"] = 1835,
									["Chillbone Gnawer"] = 19268,
									["Wyrm Reanimator"] = 1534,
									["Nathanos Blightcaller"] = 682,
									["Lumbering Atrocity"] = 695,
									["Darksworn Bonecaster"] = 724,
									["Bone Giant"] = 703,
									["Shambling Horror"] = 631,
									["Gloomshroom"] = 1502,
									["Frostbrood Whelp"] = 1426,
									["Putrid Experiment"] = 713,
									["Darksworn Sentry"] = 3898,
									["Risen Skeleton"] = 2822,
									["Vile Sludgefiend"] = 1407,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 24408,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 42,
								["total"] = 37840,
								["c_max"] = 1835,
								["id"] = 24275,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 33,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[35395] = {
								["c_amt"] = 46,
								["b_amt"] = 0,
								["c_dmg"] = 22002,
								["g_amt"] = 0,
								["n_max"] = 308,
								["targets"] = {
									["Trollgore"] = 579,
									["Vrykul Necrolord"] = 463,
									["Chillbone Gnawer"] = 17688,
									["Vile Sludgefiend"] = 454,
									["Darksworn Bonecaster"] = 1488,
									["Cult Shadowmage"] = 483,
									["Frostbrood Whelp"] = 2994,
									["Colossal Plaguespreader"] = 993,
									["Wyrm Reanimator"] = 994,
									["Nathanos Blightcaller"] = 3213,
									["Lumbering Atrocity"] = 2477,
									["Darksworn Sentry"] = 3287,
									["Risen Skeleton"] = 1384,
									["Shambling Horror"] = 213,
									["Gloomshroom"] = 226,
									["Risen Fiend"] = 417,
									["Putrid Experiment"] = 2468,
									["Decrepit Laborer"] = 2170,
									["Bone Giant"] = 4130,
									["Skeletal Footsoldier"] = 693,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 24812,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 149,
								["total"] = 46814,
								["c_max"] = 601,
								["id"] = 35395,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 103,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[273481] = {
								["c_amt"] = 12,
								["b_amt"] = 0,
								["c_dmg"] = 4763,
								["g_amt"] = 0,
								["n_max"] = 245,
								["targets"] = {
									["Trollgore"] = 840,
									["Chillbone Gnawer"] = 1719,
									["Wyrm Reanimator"] = 173,
									["Nathanos Blightcaller"] = 572,
									["Lumbering Atrocity"] = 2225,
									["Cult Shadowmage"] = 173,
									["Darksworn Bonecaster"] = 923,
									["Vile Sludgefiend"] = 171,
									["Gloomshroom"] = 374,
									["Skeletal Footsoldier"] = 685,
									["Darksworn Sentry"] = 558,
									["Frostbrood Whelp"] = 346,
									["Decrepit Laborer"] = 187,
									["Colossal Plaguespreader"] = 1174,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 5357,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 41,
								["total"] = 10120,
								["c_max"] = 490,
								["id"] = 273481,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 29,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[20271] = {
								["c_amt"] = 27,
								["b_amt"] = 0,
								["c_dmg"] = 29814,
								["g_amt"] = 0,
								["n_max"] = 755,
								["targets"] = {
									["Trollgore"] = 2354,
									["Vrykul Necrolord"] = 1061,
									["Chillbone Gnawer"] = 31592,
									["Vile Sludgefiend"] = 2182,
									["Darksworn Bonecaster"] = 7260,
									["Decrepit Laborer"] = 5012,
									["Cultist Corrupter"] = 531,
									["Frostbrood Whelp"] = 3812,
									["Colossal Plaguespreader"] = 1199,
									["Wyrm Reanimator"] = 3828,
									["Nathanos Blightcaller"] = 6107,
									["Skeletal Footsoldier"] = 1070,
									["Risen Fiend"] = 974,
									["Bone Giant"] = 3236,
									["Shambling Horror"] = 517,
									["Gloomshroom"] = 1593,
									["Cult Shadowmage"] = 526,
									["Putrid Experiment"] = 2536,
									["Lumbering Atrocity"] = 2463,
									["Risen Skeleton"] = 532,
									["Darksworn Sentry"] = 7502,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 56073,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 127,
								["total"] = 85887,
								["c_max"] = 1288,
								["id"] = 20271,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 100,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[295374] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 602,
								["targets"] = {
									["Vile Sludgefiend"] = 602,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 602,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 602,
								["c_max"] = 0,
								["id"] = 295374,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[224266] = {
								["c_amt"] = 49,
								["b_amt"] = 0,
								["c_dmg"] = 142542,
								["g_amt"] = 0,
								["n_max"] = 2200,
								["targets"] = {
									["Trollgore"] = 4587,
									["Vrykul Necrolord"] = 3931,
									["Chillbone Gnawer"] = 156057,
									["Vile Sludgefiend"] = 7970,
									["Darksworn Bonecaster"] = 20045,
									["Decrepit Laborer"] = 5139,
									["Frostbrood Whelp"] = 14111,
									["Colossal Plaguespreader"] = 8607,
									["Wyrm Reanimator"] = 17228,
									["Nathanos Blightcaller"] = 35294,
									["Lumbering Atrocity"] = 13914,
									["Skeletal Footsoldier"] = 8558,
									["Gloomshroom"] = 1228,
									["Risen Skeleton"] = 3604,
									["Putrid Experiment"] = 17541,
									["Risen Fiend"] = 2342,
									["Bone Giant"] = 19013,
									["Darksworn Sentry"] = 24125,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 220752,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 201,
								["total"] = 363294,
								["c_max"] = 4250,
								["id"] = 224266,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 152,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[295367] = {
								["c_amt"] = 36,
								["b_amt"] = 0,
								["c_dmg"] = 19684,
								["g_amt"] = 0,
								["n_max"] = 283,
								["targets"] = {
									["Chillbone Gnawer"] = 21991,
									["Blight-Howl"] = 849,
									["Nathanos Blightcaller"] = 9910,
									["Lumbering Atrocity"] = 3243,
									["Cult Shadowmage"] = 270,
									["Darksworn Bonecaster"] = 1902,
									["Decrepit Laborer"] = 2714,
									["Gloomshroom"] = 1087,
									["Frostbrood Whelp"] = 272,
									["Darksworn Sentry"] = 1902,
									["Putrid Experiment"] = 4619,
									["Bone Giant"] = 2971,
									["Colossal Plaguespreader"] = 544,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 32590,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 155,
								["total"] = 52274,
								["c_max"] = 571,
								["id"] = 295367,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 119,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[184689] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3387,
								["targets"] = {
									["Gloomshroom"] = 1166,
									["Putrid Experiment"] = 1166,
									["Nathanos Blightcaller"] = 3164,
									["Lumbering Atrocity"] = 3387,
									["Vile Sludgefiend"] = 1166,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 10049,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 10049,
								["c_max"] = 0,
								["id"] = 184689,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[255941] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Bone Giant"] = 0,
									["Trollgore"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 0,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 255941,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 0,
								["extra"] = {
								},
								["IMMUNE"] = 2,
							},
						},
						["tipo"] = 2,
					},
					["friendlyfire_total"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 215281.6354530001,
					["start_time"] = 1605057069,
					["serial"] = "Player-76-057FA942",
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 2291.032931,
					["damage_from"] = {
						["Faultline <Thauriel>"] = true,
						["Thauriel"] = true,
						["Faultline"] = true,
						["Shoegaze"] = true,
						["Ankha"] = true,
						["Magria"] = true,
						["Ponzini"] = true,
						["Kreedhum"] = true,
						["Mièlikki"] = true,
						["Gerdylocks"] = true,
						["Thorddin"] = true,
						["吉特班"] = true,
						["Snappelz"] = true,
						["Sìnarìa-Illidan"] = true,
					},
					["targets"] = {
						["Faultline <Thauriel>"] = 58,
						["Thauriel"] = 662,
						["Faultline"] = 50,
						["Ankha"] = 9,
						["Magria"] = 19,
						["Ponzini"] = 190,
						["Kreedhum"] = 28,
						["Thorddin"] = 2695,
						["Gerdylocks"] = 194,
						["Dorillea-Illidan"] = 164,
						["Mièlikki"] = 396,
						["Snappelz"] = 1124,
						["Sìnarìa-Illidan"] = 205,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 5794.032931000001,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 5794.032931000001,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605058494,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Shambling Horror",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 273,
								["targets"] = {
									["Thauriel"] = 465,
									["Thorddin"] = 1541,
									["Faultline"] = 0,
									["Snappelz"] = 532,
									["Sìnarìa-Illidan"] = 205,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2743,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 2743,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 242,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 1,
								["n_amt"] = 12,
								["extra"] = {
								},
								["DODGE"] = 1,
							}, -- [1]
							[331651] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 198,
								["targets"] = {
									["Kreedhum"] = 28,
									["Thauriel"] = 197,
									["Faultline"] = 50,
									["Ankha"] = 9,
									["Magria"] = 19,
									["Ponzini"] = 190,
									["Faultline <Thauriel>"] = 58,
									["Thorddin"] = 1154,
									["Dorillea-Illidan"] = 164,
									["Mièlikki"] = 396,
									["Snappelz"] = 592,
									["Gerdylocks"] = 194,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3051,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 32,
								["total"] = 3051,
								["c_max"] = 0,
								["id"] = 331651,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 8,
								["b_dmg"] = 0,
								["n_amt"] = 32,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 26411.032931,
					["start_time"] = 1605058475,
					["serial"] = "Creature-0-3778-0-1485-170235-00032B3FA5",
					["aID"] = "170235",
				}, -- [2]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 976.0377169999999,
					["damage_from"] = {
						["Kreedhum"] = true,
						["ßlyss"] = true,
						["Ankha"] = true,
						["Alkizar"] = true,
						["Ponzini"] = true,
						["Erneke"] = true,
						["Mcgeek"] = true,
						["Thorddin"] = true,
						["Magria"] = true,
						["Wrathsputin"] = true,
						["裂蹄牛"] = true,
						["Zynic"] = true,
						["Hachiy"] = true,
						["Thauriel"] = true,
						["Gerdylocks"] = true,
						["Faultline <Thauriel>"] = true,
						["Faultline"] = true,
						["Boriis"] = true,
						["Chubin"] = true,
						["Shoegaze"] = true,
						["Threatgodx"] = true,
						["Pallore"] = true,
						["Mièlikki"] = true,
						["Friartukk"] = true,
						["Dorillea-Illidan"] = true,
						["Unknown"] = true,
						["吉特班"] = true,
						["Sìnarìa-Illidan"] = true,
					},
					["targets"] = {
						["Kreedhum"] = 1611,
						["Jhazvhug"] = 211,
						["Chubin"] = 514,
						["吉特班"] = 280,
						["Zynic"] = 1121,
						["Erneke"] = 232,
						["Gerdylocks"] = 435,
						["Thorddin"] = 967,
						["Faultline <Thauriel>"] = 236,
						["Unknown"] = 349,
						["Hachiy"] = 376,
						["Mcgeek"] = 333,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 6665.037716999999,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 6665.037716999999,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605058494,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Risen Fiend",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 435,
								["targets"] = {
									["Hachiy"] = 376,
									["Zynic"] = 1121,
									["Wrathsputin"] = 0,
									["Jhazvhug"] = 211,
									["Ankha"] = 0,
									["吉特班"] = 280,
									["Chubin"] = 514,
									["Erneke"] = 232,
									["Gerdylocks"] = 435,
									["Thorddin"] = 967,
									["Kreedhum"] = 1611,
									["Unknown"] = 349,
									["Faultline <Thauriel>"] = 236,
									["Mcgeek"] = 333,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 6665,
								["MISS"] = 1,
								["n_min"] = 0,
								["a_amt"] = 9,
								["counter"] = 25,
								["total"] = 6665,
								["r_amt"] = 0,
								["c_max"] = 0,
								["b_dmg"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 2943,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["extra"] = {
								},
								["n_amt"] = 21,
								["DODGE"] = 2,
								["g_dmg"] = 0,
							}, -- [1]
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 50704.03771700001,
					["start_time"] = 1605058472,
					["serial"] = "Creature-0-3778-0-1485-170236-00002B3FAD",
					["aID"] = "170236",
				}, -- [3]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 6265.01946,
					["damage_from"] = {
						["Onewithbind"] = true,
						["Risen Skulker <Mortemitera>"] = true,
						["Harambê"] = true,
						["Gnuxlock"] = true,
						["Friskkilla"] = true,
						["Erincia"] = true,
						["Boriis"] = true,
						["Thorddin"] = true,
						["Hart"] = true,
						["Duraness"] = true,
						["Xuen <Liléath>"] = true,
						["Baayy"] = true,
						["Ræquan"] = true,
						["Loki"] = true,
						["Ddoczd"] = true,
						["Swòrds"] = true,
						["Solangè"] = true,
						["Splintr"] = true,
						["Liléath"] = true,
						["Pallore"] = true,
						["Infernal <Harambê>"] = true,
						["Mortemitera"] = true,
						["Cósmós"] = true,
						["Magus of the Dead <Mortemitera>"] = true,
						["Saduz"] = true,
						["Sigrynth"] = true,
						["Weefermadnes"] = true,
						["Jôêkêr"] = true,
						["Esleeze"] = true,
						["Isi"] = true,
						["Leglessfatty"] = true,
						["Hunterdo"] = true,
						["Sebastian"] = true,
						["Archimedes"] = true,
						["Guardian of Azeroth <Hunterdo>"] = true,
						["Endell"] = true,
						["Cat"] = true,
						["Eyerumbler"] = true,
						["Jubjub"] = true,
						["Wolf"] = true,
						["Senyth"] = true,
						["Caltonse"] = true,
						["Nilrem"] = true,
						["Zartric"] = true,
						["Lethumper"] = true,
						["Hausstalker"] = true,
						["Kruniak"] = true,
						["Allyliia"] = true,
						["Skittlesx"] = true,
						["Risen Skulker"] = true,
						["Douginator"] = true,
						["Ræquan <Ræquan>"] = true,
						["Xuen <Ræquan>"] = true,
						["Quarrel"] = true,
						["Skoll"] = true,
						["Taelory"] = true,
						["Spirit Beast"] = true,
						["Braincruncher <Mortemitera>"] = true,
					},
					["targets"] = {
						["Archimedes"] = 893,
						["Harambê"] = 10243,
						["Allyliia"] = 227,
						["Mortemitera"] = 941,
						["Cósmós"] = 1613,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 13917.01946,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 13917.01946,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605059083,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Blight-Howl",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 941,
								["targets"] = {
									["Archimedes"] = 893,
									["Mortemitera"] = 941,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1834,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 1834,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[331340] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 1756,
								["targets"] = {
									["Allyliia"] = 227,
									["Harambê"] = 10243,
									["Cósmós"] = 1613,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 12083,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 14,
								["total"] = 12083,
								["c_max"] = 0,
								["id"] = 331340,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 4,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 6,
								["b_dmg"] = 0,
								["n_amt"] = 14,
								["a_dmg"] = 4393,
								["extra"] = {
								},
							},
							[331607] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 331607,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 167866.01946,
					["start_time"] = 1605059061,
					["serial"] = "Creature-0-3778-0-1756-169118-00002B419C",
					["aID"] = "169118",
				}, -- [4]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 1769.019018,
					["damage_from"] = {
						["Onewithbind"] = true,
						["Risen Skulker <Mortemitera>"] = true,
						["Harambê"] = true,
						["Heckblazer"] = true,
						["Friskkilla"] = true,
						["Quarrel"] = true,
						["Boriis"] = true,
						["Loki"] = true,
						["Citatìon"] = true,
						["Bruumon"] = true,
						["Thorddin"] = true,
						["Hart"] = true,
						["Lethumper"] = true,
						["Douginator"] = true,
						["Xuen <Liléath>"] = true,
						["Onewithbind <Onewithbind>"] = true,
						["Ræquan"] = true,
						["Splintr"] = true,
						["Gnuxlock"] = true,
						["Ddoczd"] = true,
						["Isi"] = true,
						["Solangè"] = true,
						["Mortemitera"] = true,
						["Liléath"] = true,
						["Pallore"] = true,
						["Senyth"] = true,
						["Caltonse"] = true,
						["Hausstalker"] = true,
						["Xuen <Onewithbind>"] = true,
						["Saduz"] = true,
						["Cósmós"] = true,
						["Weefermadnes"] = true,
						["Jôêkêr"] = true,
						["Esleeze"] = true,
						["Swòrds"] = true,
						["Leglessfatty"] = true,
						["Liléath <Liléath>"] = true,
						["Sebastian"] = true,
						["Wolf"] = true,
						["Archimedes"] = true,
						["Mavieñar"] = true,
						["Cat"] = true,
						["Allyliia"] = true,
						["Jubjub"] = true,
						["Eyerumbler"] = true,
						["Baayy"] = true,
						["Hunterdo"] = true,
						["Nilrem"] = true,
						["Zartric"] = true,
						["Guardian of Azeroth <Hunterdo>"] = true,
						["Qüelana"] = true,
						["Kruniak"] = true,
						["Sigrynth"] = true,
						["Endell"] = true,
						["Risen Skulker"] = true,
						["Infernal <Harambê>"] = true,
						["Ræquan <Ræquan>"] = true,
						["Xuen <Ræquan>"] = true,
						["Drcoffee"] = true,
						["Skoll"] = true,
						["Taelory"] = true,
						["Spirit Beast"] = true,
						["Braincruncher <Mortemitera>"] = true,
					},
					["targets"] = {
						["Pallore"] = 6700,
						["Mortemitera"] = 2269,
						["Kruniak"] = 1520,
						["Allyliia"] = 5411,
						["Saduz"] = 1495,
						["Thorddin"] = 2218,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 19613.019018,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 19613.019018,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605059083,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Plaguefang",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 1520,
								["targets"] = {
									["Kruniak"] = 1520,
									["Allyliia"] = 1747,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3267,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 3267,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[331607] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 331607,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[331340] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 931,
								["targets"] = {
									["Pallore"] = 6700,
									["Thorddin"] = 2218,
									["Mortemitera"] = 2269,
									["Leglessfatty"] = 0,
									["Allyliia"] = 3664,
									["Saduz"] = 1495,
									["Jôêkêr"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 16346,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 30,
								["r_amt"] = 0,
								["total"] = 16346,
								["c_max"] = 0,
								["extra"] = {
								},
								["id"] = 331340,
								["r_dmg"] = 0,
								["b_dmg"] = 0,
								["m_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["c_min"] = 0,
								["successful_casted"] = 7,
								["a_dmg"] = 477,
								["n_amt"] = 27,
								["DODGE"] = 2,
								["a_amt"] = 1,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 188573.019018,
					["start_time"] = 1605059050,
					["serial"] = "Creature-0-3778-0-1756-169119-00002B419C",
					["aID"] = "169119",
				}, -- [5]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 10158.00966,
					["damage_from"] = {
						["Onewithbind"] = true,
						["Harambê"] = true,
						["Gnuxlock"] = true,
						["Citatìon"] = true,
						["Thorddin"] = true,
						["Xuen <Liléath>"] = true,
						["Onewithbind <Onewithbind>"] = true,
						["Swòrds"] = true,
						["Ddoczd"] = true,
						["Darkglare <Gnuxlock>"] = true,
						["Liléath"] = true,
						["Hausstalker"] = true,
						["Infernal <Harambê>"] = true,
						["Mortemitera"] = true,
						["Jessicazeal"] = true,
						["Endell"] = true,
						["Cósmós"] = true,
						["Jôêkêr"] = true,
						["Magus of the Dead <Mortemitera>"] = true,
						["Liléath <Liléath>"] = true,
						["Sebastian"] = true,
						["Eyerumbler"] = true,
						["Zartric"] = true,
						["Guardian of Azeroth <Baayy>"] = true,
						["Xuen <Onewithbind>"] = true,
						["Skittlesx"] = true,
						["Qüelana"] = true,
						["Himesey"] = true,
						["Polter"] = true,
						["Bobaloo"] = true,
						["Skoll"] = true,
						["Taelory"] = true,
						["Spirit Beast"] = true,
						["Baayy"] = true,
						["Shadowfiend <Janier>"] = true,
						["Douginator"] = true,
						["Risen Skulker <Mortemitera>"] = true,
						["Hunterdo"] = true,
						["Heckblazer"] = true,
						["Friskkilla"] = true,
						["Saíx"] = true,
						["Bürgo"] = true,
						["Erincia"] = true,
						["Boriis"] = true,
						["Kniteynight"] = true,
						["Loki"] = true,
						["Hart"] = true,
						["Risen Ghoul"] = true,
						["Duraness"] = true,
						["Splintr"] = true,
						["Risen Skulker"] = true,
						["Starfawx"] = true,
						["Ræquan"] = true,
						["Wolf"] = true,
						["Ariazadda"] = true,
						["Ellylanah"] = true,
						["Solangè"] = true,
						["Guardian of Azeroth <Lethumper>"] = true,
						["Caltonse"] = true,
						["Pallore"] = true,
						["Isi"] = true,
						["Marshmallowx"] = true,
						["Sigrynth"] = true,
						["Erzsi"] = true,
						["Baayy <Baayy>"] = true,
						["Esleeze"] = true,
						["Weefermadnes"] = true,
						["Shadowfiend <Ellylanah>"] = true,
						["Staticpalms"] = true,
						["ßlyss"] = true,
						["Leglessfatty"] = true,
						["Janier"] = true,
						["Allyliia"] = true,
						["Archimedes"] = true,
						["Guardian of Azeroth <Hunterdo>"] = true,
						["Saduz"] = true,
						["Cat"] = true,
						["Risen Ghoul <Endell>"] = true,
						["Jubjub"] = true,
						["Mavieñar"] = true,
						["Guardian of Azeroth <Onewithbind>"] = true,
						["Risen Ravasaur <Endell>"] = true,
						["Nilrem"] = true,
						["Lethumper"] = true,
						["Infernal <Bürgo>"] = true,
						["Legadon"] = true,
						["Kruniak"] = true,
						["Quarrel"] = true,
						["Army of the Dead <Mortemitera>"] = true,
						["Neljä"] = true,
						["Beast <Solangè>"] = true,
						["Ræquan <Ræquan>"] = true,
						["Xuen <Ræquan>"] = true,
						["Drcoffee"] = true,
						["Bruumon"] = true,
						["Senyth"] = true,
						["Greater Fire Elemental <Himesey>"] = true,
						["Braincruncher <Mortemitera>"] = true,
					},
					["targets"] = {
						["Jôêkêr"] = 4920,
						["Harambê"] = 5149,
						["ßlyss"] = 5292,
						["Saíx"] = 5230,
						["Ellylanah"] = 11208,
						["Sigrynth"] = 35588,
						["Quarrel"] = 11854,
						["Baayy"] = 4611,
						["Allyliia"] = 2394,
						["Vaeliir"] = 5479,
						["Qüelana"] = 5769,
						["Kruniak"] = 11775,
						["Leglessfatty"] = 5912,
						["Solangè"] = 5691,
						["Legadon"] = 5746,
						["Janier"] = 5586,
						["Pallore"] = 5651,
						["Senyth"] = 5522,
						["Mortemitera"] = 5568,
						["Mavieñar"] = 5288,
						["Ïnvi"] = 5453,
						["Endell"] = 5661,
						["Cósmós"] = 24909,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 190256.00966,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 190256.00966,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605059083,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Nathanos Blightcaller",
					["spells"] = {
						["_ActorTable"] = {
							[330062] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5993,
								["targets"] = {
									["Jôêkêr"] = 4920,
									["Harambê"] = 5149,
									["ßlyss"] = 5292,
									["Saíx"] = 5230,
									["Ellylanah"] = 11208,
									["Cósmós"] = 4070,
									["Sigrynth"] = 4960,
									["Baayy"] = 4611,
									["Allyliia"] = 2394,
									["Vaeliir"] = 5479,
									["Janier"] = 5586,
									["Kruniak"] = 11775,
									["Leglessfatty"] = 5912,
									["Solangè"] = 5691,
									["Legadon"] = 5746,
									["Mavieñar"] = 5288,
									["Pallore"] = 5651,
									["Senyth"] = 5522,
									["Mortemitera"] = 5568,
									["Qüelana"] = 5769,
									["Ïnvi"] = 5453,
									["Endell"] = 5661,
									["Quarrel"] = 11854,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 138789,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 26,
								["total"] = 138789,
								["c_max"] = 0,
								["id"] = 330062,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 10,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 11,
								["b_dmg"] = 0,
								["n_amt"] = 26,
								["a_dmg"] = 54328,
								["extra"] = {
								},
							},
							[331256] = {
								["c_amt"] = 0,
								["b_amt"] = 4,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 5599,
								["targets"] = {
									["Sigrynth"] = 30628,
									["Cósmós"] = 20839,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 51467,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 16,
								["total"] = 51467,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 331256,
								["r_dmg"] = 0,
								["a_dmg"] = 16582,
								["b_dmg"] = 9137,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 16,
								["a_amt"] = 4,
								["n_amt"] = 14,
								["extra"] = {
								},
								["DODGE"] = 2,
							},
							[331253] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 331253,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 9,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[330079] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 330079,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 27,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[331605] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 331605,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 3883503.00966,
					["start_time"] = 1605058981,
					["serial"] = "Creature-0-3778-0-1756-169035-00002B419C",
					["aID"] = "169035",
				}, -- [6]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 46257.013286,
					["damage_from"] = {
					},
					["targets"] = {
						["Kyokan"] = 3864,
						["Harambê"] = 19998,
						["Gnuxlock"] = 12438,
						["Friskkilla"] = 8778,
						["Saíx"] = 3686,
						["Ingenuîty-Stormrage"] = 2220,
						["Erincia"] = 35049,
						["Douginator"] = 15326,
						["Thorddin"] = 8711,
						["Lethumper"] = 9539,
						["Boriis"] = 18905,
						["Draebane"] = 3298,
						["Hunterdo"] = 8810,
						["Heckblazer"] = 14898,
						["Bräkus"] = 2443,
						["Missvickies"] = 5328,
						["Drcoffee"] = 10273,
						["Ddoczd"] = 3138,
						["Polter"] = 26288,
						["Solangè"] = 3687,
						["Majestique"] = 2206,
						["Liléath"] = 12012,
						["Hausstalker"] = 16645,
						["Senyth"] = 9924,
						["Caltonse"] = 3397,
						["Jessicazeal"] = 6218,
						["Ïnvi"] = 1868,
						["Endell"] = 10228,
						["Cósmós"] = 17188,
						["Weefermadnes"] = 6363,
						["Jôêkêr"] = 3706,
						["Rhit"] = 8416,
						["Ariazadda"] = 6481,
						["ßlyss"] = 16153,
						["Duraness"] = 5367,
						["Bürgo"] = 8361,
						["Onewithbind"] = 3516,
						["Mortemitera"] = 9706,
						["Legadon"] = 6082,
						["Gattï"] = 7352,
						["Starfawx"] = 6627,
						["Ellylanah"] = 14370,
						["Allyliia"] = 12277,
						["Chuckleê"] = 18181,
						["Marshmallowx"] = 11520,
						["Nilrem"] = 20529,
						["Zartric"] = 4153,
						["Baayy"] = 11884,
						["Qüelana"] = 4436,
						["Kruniak"] = 1558,
						["Lecoup-Zul'jin"] = 8549,
						["Skittlesx"] = 1312,
						["Ræquan"] = 3681,
						["Flup"] = 3114,
						["Saduz"] = 3908,
						["Svenhardt"] = 2810,
						["Bobaloo"] = 19800,
						["Swòrds"] = 17083,
						["Citatìon"] = 3304,
						["Jaesa"] = 1855,
						["Quarrel"] = 3620,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["last_dps"] = 0,
					["aID"] = "",
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 552437.013286,
					["dps_started"] = false,
					["fight_component"] = true,
					["total"] = 552437.013286,
					["friendlyfire"] = {
					},
					["tipo"] = 1,
					["nome"] = "[*] Rain of Arrows",
					["spells"] = {
						["_ActorTable"] = {
							[330102] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2220,
								["targets"] = {
									["Kyokan"] = 3864,
									["Harambê"] = 19998,
									["Gnuxlock"] = 12438,
									["Friskkilla"] = 8778,
									["Saíx"] = 3686,
									["Ingenuîty-Stormrage"] = 2220,
									["Erincia"] = 35049,
									["Douginator"] = 15326,
									["Thorddin"] = 8711,
									["Lethumper"] = 9539,
									["Boriis"] = 18905,
									["Draebane"] = 3298,
									["Hunterdo"] = 8810,
									["Heckblazer"] = 14898,
									["Bräkus"] = 2443,
									["Missvickies"] = 5328,
									["Drcoffee"] = 10273,
									["Ddoczd"] = 3138,
									["Polter"] = 26288,
									["Solangè"] = 3687,
									["Majestique"] = 2206,
									["Liléath"] = 12012,
									["Hausstalker"] = 16645,
									["Senyth"] = 9924,
									["Caltonse"] = 3397,
									["Jessicazeal"] = 6218,
									["Ïnvi"] = 1868,
									["Endell"] = 10228,
									["Cósmós"] = 17188,
									["Weefermadnes"] = 6363,
									["Jôêkêr"] = 3706,
									["Rhit"] = 8416,
									["Ariazadda"] = 6481,
									["ßlyss"] = 16153,
									["Duraness"] = 5367,
									["Bürgo"] = 8361,
									["Onewithbind"] = 3516,
									["Mortemitera"] = 9706,
									["Legadon"] = 6082,
									["Gattï"] = 7352,
									["Starfawx"] = 6627,
									["Ellylanah"] = 14370,
									["Allyliia"] = 12277,
									["Chuckleê"] = 18181,
									["Marshmallowx"] = 11520,
									["Nilrem"] = 20529,
									["Zartric"] = 4153,
									["Baayy"] = 11884,
									["Qüelana"] = 4436,
									["Kruniak"] = 1558,
									["Lecoup-Zul'jin"] = 8549,
									["Skittlesx"] = 1312,
									["Ræquan"] = 3681,
									["Flup"] = 3114,
									["Saduz"] = 3908,
									["Svenhardt"] = 2810,
									["Bobaloo"] = 19800,
									["Swòrds"] = 17083,
									["Citatìon"] = 3304,
									["Jaesa"] = 1855,
									["Quarrel"] = 3620,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 552437,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 308,
								["total"] = 552437,
								["c_max"] = 0,
								["id"] = 330102,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 52,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 308,
								["a_dmg"] = 96366,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 0.013286,
					["friendlyfire_total"] = 0,
					["end_time"] = 1605059083,
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1605058986,
					["serial"] = "",
					["monster"] = true,
				}, -- [7]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 77003.01584400001,
					["damage_from"] = {
					},
					["targets"] = {
						["Onewithbind"] = 52519,
						["Hunterdo"] = 5159,
						["Gnuxlock"] = 6011,
						["Friskkilla"] = 4951,
						["Saíx"] = 14687,
						["Douginator"] = 9112,
						["Thorddin"] = 15184,
						["Duraness"] = 8809,
						["Bräkus"] = 5286,
						["Ræquan"] = 42522,
						["Ddoczd"] = 2865,
						["Liléath"] = 47861,
						["Pallore"] = 26112,
						["Senyth"] = 3032,
						["Caltonse"] = 26725,
						["Saduz"] = 9042,
						["Cósmós"] = 36693,
						["Jôêkêr"] = 22886,
						["Staticpalms"] = 8365,
						["Lethumper"] = 64973,
						["ßlyss"] = 2825,
						["Sigrynth"] = 19163,
						["Splintr"] = 18160,
						["Mortemitera"] = 41616,
						["Drcoffee"] = 3002,
						["Endell"] = 17672,
						["Esleeze"] = 13261,
						["Swòrds"] = 3118,
						["Leglessfatty"] = 15405,
						["Mavieñar"] = 16190,
						["Erzsi"] = 2693,
						["Qüelana"] = 9158,
						["Nilrem"] = 2693,
						["Zartric"] = 12344,
						["Kruniak"] = 33042,
						["Legadon"] = 9093,
						["Kynarae"] = 17990,
						["Lecoup-Zul'jin"] = 2924,
						["Ellylanah"] = 14560,
						["Allyliia"] = 60286,
						["Flup"] = 14916,
						["Citatìon"] = 6074,
						["Hausstalker"] = 28491,
						["Bobaloo"] = 3050,
						["Boriis"] = 15970,
						["Taelory"] = 26544,
						["Jaesa"] = 6045,
						["Quarrel"] = 2925,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["last_dps"] = 0,
					["aID"] = "",
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 832004.015844,
					["dps_started"] = false,
					["fight_component"] = true,
					["total"] = 832004.015844,
					["friendlyfire"] = {
					},
					["tipo"] = 1,
					["nome"] = "[*] Plague-Tipped Arrows",
					["spells"] = {
						["_ActorTable"] = {
							[330095] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3162,
								["targets"] = {
									["Onewithbind"] = 52519,
									["Hunterdo"] = 5159,
									["Gnuxlock"] = 6011,
									["Friskkilla"] = 4951,
									["Saíx"] = 14687,
									["Douginator"] = 9112,
									["Thorddin"] = 15184,
									["Duraness"] = 8809,
									["Bräkus"] = 5286,
									["Ræquan"] = 42522,
									["Ddoczd"] = 2865,
									["Liléath"] = 47861,
									["Pallore"] = 26112,
									["Senyth"] = 3032,
									["Caltonse"] = 26725,
									["Saduz"] = 9042,
									["Cósmós"] = 36693,
									["Jôêkêr"] = 22886,
									["Staticpalms"] = 8365,
									["Lethumper"] = 64973,
									["ßlyss"] = 2825,
									["Sigrynth"] = 19163,
									["Splintr"] = 18160,
									["Mortemitera"] = 41616,
									["Drcoffee"] = 3002,
									["Endell"] = 17672,
									["Esleeze"] = 13261,
									["Swòrds"] = 3118,
									["Leglessfatty"] = 15405,
									["Mavieñar"] = 16190,
									["Erzsi"] = 2693,
									["Qüelana"] = 9158,
									["Nilrem"] = 2693,
									["Zartric"] = 12344,
									["Kruniak"] = 33042,
									["Legadon"] = 9093,
									["Kynarae"] = 17990,
									["Lecoup-Zul'jin"] = 2924,
									["Ellylanah"] = 14560,
									["Allyliia"] = 60286,
									["Flup"] = 14916,
									["Citatìon"] = 6074,
									["Hausstalker"] = 28491,
									["Bobaloo"] = 3050,
									["Boriis"] = 15970,
									["Taelory"] = 26544,
									["Jaesa"] = 6045,
									["Quarrel"] = 2925,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 832004,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 375,
								["total"] = 832004,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 330095,
								["r_dmg"] = 0,
								["a_dmg"] = 187592,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 66,
								["n_amt"] = 297,
								["extra"] = {
								},
								["IMMUNE"] = 78,
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 0.015844,
					["friendlyfire_total"] = 0,
					["end_time"] = 1605059083,
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1605058973,
					["serial"] = "",
					["monster"] = true,
				}, -- [8]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 19812.36915200001,
					["damage_from"] = {
						["Tallisar"] = true,
						["Blightgobbler <Exorno>"] = true,
						["Evansdee"] = true,
						["Wolfanstein"] = true,
						["Homecat"] = true,
						["Cat"] = true,
						["Thorddin"] = true,
						["Ragoar"] = true,
						["Daswarr"] = true,
						["Silver Covenant Guardian"] = true,
						["Prakas"] = true,
						["Risen Skulker <Exorno>"] = true,
						["Tzekel"] = true,
						["Roeweena"] = true,
						["Cryptgrinder"] = true,
						["Rubicks"] = true,
						["Risen Skulker"] = true,
						["Exorno"] = true,
						["Thunder Bluff Champion"] = true,
						["Taurolyon"] = true,
						["Toblekai"] = true,
					},
					["targets"] = {
						["Cellian Daybreak"] = 742,
						["Darnassus Champion"] = 830,
						["Exorno"] = 1169,
						["Evansdee"] = 576,
						["Yob"] = 0,
						["Roeweena"] = 1478,
						["Homecat"] = 4194,
						["Cat"] = 573,
						["Thorddin"] = 45895,
						["Undercity Champion"] = 399,
						["Orgrimmar Champion"] = 0,
						["Daswarr"] = 1549,
						["Zor'be the Bloodletter"] = 1744,
						["Stormwind Champion"] = 2068,
						["Silver Covenant Guardian"] = 26758,
						["Prakas"] = 431,
						["Morgên"] = 0,
						["Eadric the Pure"] = 13953,
						["Tzekel"] = 2546,
						["Ritchard"] = 0,
						["Blightgobbler <Exorno>"] = 599,
						["Tallisar"] = 359,
						["Illyrie Nightfall"] = 2113,
						["Taurolyon"] = 824,
						["Thunder Bluff Champion"] = 739,
						["Glacierthief"] = 0,
						["Toblekai"] = 752,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["dps_started"] = false,
					["monster"] = true,
					["aID"] = "166004",
					["raid_targets"] = {
					},
					["total_without_pet"] = 110291.369152,
					["total"] = 110291.369152,
					["fight_component"] = true,
					["end_time"] = 1605059559,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Chillbone Gnawer",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 9,
								["b_amt"] = 1,
								["c_dmg"] = 6157,
								["g_amt"] = 0,
								["n_max"] = 527,
								["targets"] = {
									["Cellian Daybreak"] = 742,
									["Darnassus Champion"] = 830,
									["Exorno"] = 1169,
									["Evansdee"] = 576,
									["Yob"] = 0,
									["Roeweena"] = 1478,
									["Homecat"] = 4194,
									["Cat"] = 573,
									["Thorddin"] = 45895,
									["Undercity Champion"] = 399,
									["Daswarr"] = 1549,
									["Orgrimmar Champion"] = 0,
									["Argent Peacekeeper"] = 0,
									["Crusader Rhydalla"] = 0,
									["Zor'be the Bloodletter"] = 1744,
									["Stormwind Champion"] = 2068,
									["Silver Covenant Guardian"] = 26758,
									["Prakas"] = 431,
									["Morgên"] = 0,
									["Glacierthief"] = 0,
									["Tzekel"] = 2546,
									["Ritchard"] = 0,
									["Blightgobbler <Exorno>"] = 599,
									["Tallisar"] = 359,
									["Illyrie Nightfall"] = 2113,
									["Taurolyon"] = 824,
									["Thunder Bluff Champion"] = 739,
									["Eadric the Pure"] = 13953,
									["Toblekai"] = 752,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 104134,
								["DODGE"] = 16,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 390,
								["total"] = 110291,
								["r_amt"] = 0,
								["c_max"] = 800,
								["b_dmg"] = 163,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 7671,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 33,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["extra"] = {
								},
								["n_amt"] = 321,
								["MISS"] = 11,
								["a_amt"] = 25,
							}, -- [1]
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 494644.3691520002,
					["friendlyfire"] = {
					},
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["last_event"] = 0,
					["last_dps"] = 0,
					["start_time"] = 1605059115,
					["serial"] = "Creature-0-3138-571-32512-166004-00012B4334",
					["friendlyfire_total"] = 0,
				}, -- [9]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 30051.238597,
					["damage_from"] = {
					},
					["targets"] = {
						["Tallisar"] = 952,
						["Kyokan"] = 396,
						["Evansdee"] = 8871,
						["Treloar"] = 1460,
						["Keflä"] = 395,
						["Shinöbü"] = 0,
						["Thorddin"] = 21813,
						["Thalroy"] = 390,
						["Cyberthugg"] = 0,
						["Kerisong"] = 3890,
						["Nahjdorf"] = 390,
						["Prakas"] = 683,
						["Tangyhunter"] = 0,
						["Tryllé"] = 0,
						["Toblekai"] = 0,
						["Sneha"] = 0,
						["Sweetnsexi"] = 376,
						["Qinshouju"] = 0,
						["Elfix"] = 347,
						["Lovecannons"] = 0,
						["Singoon"] = 1529,
						["Xianmokafei"] = 0,
						["Nizh"] = 0,
						["Iefyr"] = 344,
						["Lagato"] = 398,
						["Yharnuum"] = 0,
						["Llechwraidd"] = 381,
						["Ellanar"] = 779,
						["Umadon"] = 0,
						["Tooshotz"] = 796,
						["Acesummer"] = 389,
						["Spaniolo"] = 776,
						["Forak"] = 379,
						["Bladërunner"] = 702,
						["Rambal"] = 0,
						["Rubicks"] = 395,
						["Hellisah"] = 394,
						["Daswarr"] = 1118,
						["Miniøn"] = 391,
						["Memmonk"] = 2271,
						["Funkdup"] = 217,
						["Exorno"] = 1533,
						["Cerealshield"] = 0,
						["Gilannah"] = 394,
						["Flashlamp"] = 2098,
						["Vynllindon"] = 1980,
						["Goldfaith"] = 392,
						["Homecat"] = 1850,
						["Authumn"] = 371,
						["Osseous"] = 39,
						["Kryuura"] = 362,
						["Giefan"] = 1531,
						["Maybecast"] = 40,
						["Habdrid"] = 0,
						["Mahira"] = 0,
						["Maehwa"] = 783,
						["Hheather"] = 0,
						["Combenius"] = 795,
						["Niobei"] = 1189,
						["Wolfanstein"] = 1129,
						["Prðxx"] = 322,
						["Verk"] = 0,
						["Masja"] = 0,
						["Caked"] = 0,
						["Ravenscarlet"] = 0,
						["Thylyssia"] = 0,
						["Gotgirth"] = 380,
						["Jeraiya"] = 387,
						["Taurolyon"] = 387,
						["Saeew"] = 0,
						["Gnarill"] = 0,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["monster"] = true,
					["fight_component"] = true,
					["aID"] = "",
					["raid_targets"] = {
					},
					["total_without_pet"] = 67184.23859700002,
					["end_time"] = 1605059559,
					["dps_started"] = false,
					["total"] = 67184.23859700002,
					["damage_taken"] = 0.238597,
					["tipo"] = 1,
					["nome"] = "[*] Chilling Winds",
					["spells"] = {
						["_ActorTable"] = {
							[326788] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 398,
								["targets"] = {
									["Tallisar"] = 952,
									["Kyokan"] = 396,
									["Evansdee"] = 8871,
									["Treloar"] = 1460,
									["Keflä"] = 395,
									["Shinöbü"] = 0,
									["Thorddin"] = 21813,
									["Thalroy"] = 390,
									["Cyberthugg"] = 0,
									["Kerisong"] = 3890,
									["Nahjdorf"] = 390,
									["Prakas"] = 683,
									["Tangyhunter"] = 0,
									["Tryllé"] = 0,
									["Toblekai"] = 0,
									["Sneha"] = 0,
									["Sweetnsexi"] = 376,
									["Qinshouju"] = 0,
									["Elfix"] = 347,
									["Lovecannons"] = 0,
									["Singoon"] = 1529,
									["Xianmokafei"] = 0,
									["Nizh"] = 0,
									["Iefyr"] = 344,
									["Lagato"] = 398,
									["Yharnuum"] = 0,
									["Llechwraidd"] = 381,
									["Ellanar"] = 779,
									["Umadon"] = 0,
									["Tooshotz"] = 796,
									["Acesummer"] = 389,
									["Spaniolo"] = 776,
									["Forak"] = 379,
									["Bladërunner"] = 702,
									["Rambal"] = 0,
									["Rubicks"] = 395,
									["Hellisah"] = 394,
									["Daswarr"] = 1118,
									["Miniøn"] = 391,
									["Memmonk"] = 2271,
									["Funkdup"] = 217,
									["Exorno"] = 1533,
									["Cerealshield"] = 0,
									["Gilannah"] = 394,
									["Flashlamp"] = 2098,
									["Vynllindon"] = 1980,
									["Goldfaith"] = 392,
									["Homecat"] = 1850,
									["Authumn"] = 371,
									["Osseous"] = 39,
									["Kryuura"] = 362,
									["Giefan"] = 1531,
									["Maybecast"] = 40,
									["Habdrid"] = 0,
									["Mahira"] = 0,
									["Maehwa"] = 783,
									["Hheather"] = 0,
									["Combenius"] = 795,
									["Niobei"] = 1189,
									["Wolfanstein"] = 1129,
									["Prðxx"] = 322,
									["Verk"] = 0,
									["Masja"] = 0,
									["Caked"] = 0,
									["Ravenscarlet"] = 0,
									["Thylyssia"] = 0,
									["Gotgirth"] = 380,
									["Jeraiya"] = 387,
									["Taurolyon"] = 387,
									["Saeew"] = 0,
									["Gnarill"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 67184,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 178,
								["total"] = 67184,
								["c_max"] = 0,
								["id"] = 326788,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 26,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 178,
								["a_dmg"] = 9960,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1605059192,
					["serial"] = "",
					["friendlyfire_total"] = 0,
				}, -- [10]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 1496.06285,
					["damage_from"] = {
						["Funkdup"] = true,
						["Aodplays"] = true,
						["Thorddin"] = true,
						["Vynllindon"] = true,
						["paws"] = true,
						["Dirknight"] = true,
					},
					["targets"] = {
						["Aodplays"] = 410,
						["Thorddin"] = 5090,
						["paws"] = 0,
						["Thalroy"] = 0,
						["Vynllindon"] = 4616,
						["Dirknight"] = 1564,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["aID"] = "168024",
					["raid_targets"] = {
					},
					["total_without_pet"] = 11680.06285,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1605059873,
					["last_dps"] = 0,
					["tipo"] = 1,
					["nome"] = "Frostbrood Whelp",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 284,
								["targets"] = {
									["Aodplays"] = 0,
									["Thorddin"] = 3369,
									["Thalroy"] = 0,
									["Vynllindon"] = 2886,
									["Dirknight"] = 1102,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 7357,
								["DODGE"] = 1,
								["n_min"] = 0,
								["MISS"] = 2,
								["counter"] = 53,
								["r_amt"] = 0,
								["total"] = 7357,
								["c_max"] = 0,
								["id"] = 1,
								["extra"] = {
								},
								["r_dmg"] = 0,
								["b_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 4,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["a_dmg"] = 1094,
								["n_amt"] = 46,
								["a_amt"] = 7,
								["g_dmg"] = 0,
							}, -- [1]
							[60814] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 462,
								["targets"] = {
									["Aodplays"] = 410,
									["Thorddin"] = 1721,
									["paws"] = 0,
									["Thalroy"] = 0,
									["Vynllindon"] = 1730,
									["Dirknight"] = 462,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 4323,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 10,
								["total"] = 4323,
								["c_max"] = 0,
								["id"] = 60814,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 9,
								["b_dmg"] = 0,
								["n_amt"] = 10,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["total"] = 11680.06285,
					["damage_taken"] = 102557.06285,
					["on_hold"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1605059806,
					["serial"] = "Creature-0-3138-571-32512-168024-00012B44D9",
					["friendlyfire_total"] = 0,
				}, -- [11]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 1251.037989,
					["damage_from"] = {
						["Dirknight"] = true,
						["Igothealforu"] = true,
						["Vynllindon"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Thorddin"] = 2733,
						["Igothealforu"] = 800,
						["Vynllindon"] = 177,
						["Dirknight"] = 0,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["aID"] = "168026",
					["raid_targets"] = {
					},
					["total_without_pet"] = 3710.037989,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1605059934,
					["last_dps"] = 0,
					["tipo"] = 1,
					["nome"] = "Wyrm Reanimator",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 486,
								["g_amt"] = 0,
								["n_max"] = 361,
								["targets"] = {
									["Dirknight"] = 0,
									["Igothealforu"] = 361,
									["Thorddin"] = 2302,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2177,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["r_amt"] = 0,
								["total"] = 2663,
								["c_max"] = 486,
								["a_dmg"] = 486,
								["extra"] = {
								},
								["r_dmg"] = 0,
								["b_dmg"] = 0,
								["m_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 2,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 1,
								["n_amt"] = 8,
								["id"] = 1,
								["MISS"] = 1,
							}, -- [1]
							[32063] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 45,
								["targets"] = {
									["Vynllindon"] = 177,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 177,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 177,
								["c_max"] = 0,
								["id"] = 32063,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[9613] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 439,
								["targets"] = {
									["Igothealforu"] = 439,
									["Thorddin"] = 431,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 870,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 870,
								["c_max"] = 0,
								["id"] = 9613,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["total"] = 3710.037989,
					["damage_taken"] = 58222.037989,
					["on_hold"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1605059897,
					["serial"] = "Creature-0-3138-571-32512-168026-00002B431F",
					["friendlyfire_total"] = 0,
				}, -- [12]
				{
					["flag_original"] = 4369,
					["totalabsorbed"] = 0.04168,
					["damage_from"] = {
						["Zealous Neophyte"] = true,
						["Dark Ritualist"] = true,
						["Putrid Atrocity"] = true,
						["Dark Zealot"] = true,
					},
					["targets"] = {
						["Zealous Neophyte"] = 20124,
						["Dark Ritualist"] = 15182,
						["Putrid Atrocity"] = 2686,
						["Dark Zealot"] = 62822,
					},
					["pets"] = {
					},
					["total"] = 100814.04168,
					["tipo"] = 1,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 100814.04168,
					["delay"] = 0,
					["dps_started"] = false,
					["end_time"] = 1605060121,
					["friendlyfire"] = {
					},
					["ownerName"] = "Thorddin",
					["nome"] = "Altered Ghoul <Thorddin>",
					["spells"] = {
						["_ActorTable"] = {
							[337945] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 5051,
								["g_amt"] = 0,
								["n_max"] = 1168,
								["targets"] = {
									["Zealous Neophyte"] = 10518,
									["Dark Ritualist"] = 10800,
									["Putrid Atrocity"] = 779,
									["Dark Zealot"] = 48764,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 65810,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 75,
								["total"] = 70861,
								["c_max"] = 1906,
								["id"] = 337945,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 72,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[337943] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 242,
								["targets"] = {
									["Zealous Neophyte"] = 242,
									["Dark Zealot"] = 462,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 704,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 704,
								["c_max"] = 0,
								["id"] = 337943,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[337959] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 572,
								["targets"] = {
									["Zealous Neophyte"] = 9364,
									["Dark Zealot"] = 13596,
									["Putrid Atrocity"] = 1907,
									["Dark Ritualist"] = 4382,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 29249,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 66,
								["total"] = 29249,
								["c_max"] = 0,
								["id"] = 337959,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 66,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["classe"] = "PET",
					["aID"] = "",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1605059945,
					["serial"] = "Vehicle-0-3138-571-32512-172939-00002B45F8",
					["damage_taken"] = 50009.04168,
				}, -- [13]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.026575,
					["damage_from"] = {
						["Altered Ghoul"] = true,
						["Altered Ghoul <Thorddin>"] = true,
					},
					["targets"] = {
						["Altered Ghoul"] = 39907,
						["Altered Ghoul <Thorddin>"] = 29852,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 69759.026575,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 69759.026575,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605060121,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Dark Zealot",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 13,
								["b_amt"] = 0,
								["c_dmg"] = 8683,
								["g_amt"] = 0,
								["n_max"] = 396,
								["targets"] = {
									["Altered Ghoul"] = 37168,
									["Altered Ghoul <Thorddin>"] = 26601,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 55086,
								["b_dmg"] = 0,
								["g_dmg"] = 0,
								["n_min"] = 0,
								["a_amt"] = 0,
								["counter"] = 209,
								["total"] = 63769,
								["r_amt"] = 0,
								["c_max"] = 757,
								["extra"] = {
								},
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 7,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["DODGE"] = 7,
								["n_amt"] = 170,
								["MISS"] = 8,
								["IMMUNE"] = 4,
							}, -- [1]
							[13584] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 372,
								["targets"] = {
									["Altered Ghoul"] = 2739,
									["Altered Ghoul <Thorddin>"] = 3251,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 5990,
								["DODGE"] = 1,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 23,
								["total"] = 5990,
								["r_amt"] = 0,
								["c_max"] = 0,
								["b_dmg"] = 0,
								["id"] = 13584,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 23,
								["extra"] = {
								},
								["n_amt"] = 19,
								["MISS"] = 2,
								["a_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 136071.026575,
					["start_time"] = 1605059934,
					["serial"] = "Creature-0-3138-571-32512-172915-00002B4593",
					["aID"] = "172915",
				}, -- [14]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.016123,
					["damage_from"] = {
						["Altered Ghoul"] = true,
						["Altered Ghoul <Thorddin>"] = true,
					},
					["targets"] = {
						["Altered Ghoul"] = 8094,
						["Altered Ghoul <Thorddin>"] = 11745,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 19839.016123,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 19839.016123,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605060121,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Dark Ritualist",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 269,
								["g_amt"] = 0,
								["n_max"] = 357,
								["targets"] = {
									["Altered Ghoul"] = 3693,
									["Altered Ghoul <Thorddin>"] = 5814,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 9238,
								["b_dmg"] = 0,
								["g_dmg"] = 0,
								["n_min"] = 0,
								["a_amt"] = 0,
								["counter"] = 54,
								["total"] = 9507,
								["r_amt"] = 0,
								["c_max"] = 269,
								["extra"] = {
								},
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["DODGE"] = 2,
								["n_amt"] = 38,
								["MISS"] = 10,
								["IMMUNE"] = 2,
							}, -- [1]
							[32000] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 412,
								["targets"] = {
									["Altered Ghoul"] = 3502,
									["Altered Ghoul <Thorddin>"] = 5356,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 8858,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 31,
								["total"] = 8858,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 32000,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 11,
								["a_amt"] = 0,
								["n_amt"] = 27,
								["extra"] = {
								},
								["IMMUNE"] = 4,
							},
							[32026] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 582,
								["targets"] = {
									["Altered Ghoul"] = 899,
									["Altered Ghoul <Thorddin>"] = 575,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1474,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 1474,
								["c_max"] = 0,
								["id"] = 32026,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 21975.016123,
					["start_time"] = 1605060039,
					["serial"] = "Creature-0-3138-571-32512-172917-00002B45AC",
					["aID"] = "172917",
				}, -- [15]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.013219,
					["damage_from"] = {
						["Altered Ghoul"] = true,
						["Altered Ghoul <Thorddin>"] = true,
					},
					["targets"] = {
						["Altered Ghoul"] = 11918,
						["Altered Ghoul <Thorddin>"] = 295,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 12213.013219,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 12213.013219,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605060201,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Putrid Atrocity",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 394,
								["targets"] = {
									["Altered Ghoul"] = 7527,
									["Altered Ghoul <Thorddin>"] = 295,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 7822,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 24,
								["total"] = 7822,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 23,
								["extra"] = {
								},
								["MISS"] = 1,
							}, -- [1]
							[49861] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 390,
								["targets"] = {
									["Altered Ghoul"] = 4391,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 4391,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 15,
								["total"] = 4391,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 49861,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 4,
								["a_amt"] = 0,
								["n_amt"] = 12,
								["extra"] = {
								},
								["IMMUNE"] = 3,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 32360.013219,
					["start_time"] = 1605060144,
					["serial"] = "Creature-0-3138-571-32512-172985-00002B4543",
					["aID"] = "172985",
				}, -- [16]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.011668,
					["damage_from"] = {
						["Altered Ghoul"] = true,
						["Altered Ghoul <Thorddin>"] = true,
					},
					["targets"] = {
						["Altered Ghoul"] = 3924,
						["Altered Ghoul <Thorddin>"] = 8117,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 12041.011668,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 12041.011668,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605060201,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Zealous Neophyte",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 1439,
								["g_amt"] = 0,
								["n_max"] = 302,
								["targets"] = {
									["Altered Ghoul"] = 2465,
									["Altered Ghoul <Thorddin>"] = 4589,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 5615,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 30,
								["r_amt"] = 0,
								["total"] = 7054,
								["c_max"] = 516,
								["extra"] = {
								},
								["id"] = 1,
								["r_dmg"] = 0,
								["b_dmg"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["a_dmg"] = 0,
								["n_amt"] = 23,
								["MISS"] = 2,
								["IMMUNE"] = 2,
							}, -- [1]
							[323720] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 777,
								["targets"] = {
									["Altered Ghoul"] = 1459,
									["Altered Ghoul <Thorddin>"] = 3528,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 4987,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 13,
								["total"] = 4987,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 323720,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 13,
								["a_amt"] = 0,
								["n_amt"] = 11,
								["extra"] = {
								},
								["IMMUNE"] = 2,
							},
							[323727] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 323727,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 23547.011668,
					["start_time"] = 1605060148,
					["serial"] = "Creature-0-3138-571-32512-172984-00002B45E9",
					["aID"] = "172984",
				}, -- [17]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 1205.060194,
					["damage_from"] = {
						["Feramir"] = true,
						["Thorddin"] = true,
						["Cat"] = true,
						["Ragoar"] = true,
						["Tzekel"] = true,
					},
					["targets"] = {
						["Billybads"] = 0,
						["Buterzz"] = 0,
						["Anuur"] = 305,
						["Sambas"] = 0,
						["Thorddin"] = 2151,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["fight_component"] = true,
					["dps_started"] = false,
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 2456.060194,
					["end_time"] = 1605060499,
					["monster"] = true,
					["total"] = 2456.060194,
					["damage_taken"] = 56737.06019400001,
					["tipo"] = 1,
					["nome"] = "Darksworn Bonecaster",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 331,
								["targets"] = {
									["Billybads"] = 0,
									["Buterzz"] = 0,
									["Anuur"] = 305,
									["Sambas"] = 0,
									["Thorddin"] = 844,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1149,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 1149,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 566,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["a_amt"] = 2,
								["r_amt"] = 0,
							}, -- [1]
							[183345] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 467,
								["targets"] = {
									["Billybads"] = 0,
									["Sambas"] = 0,
									["Thorddin"] = 1307,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1307,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 1307,
								["c_max"] = 0,
								["id"] = 183345,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 3,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1605060477,
					["serial"] = "Creature-0-3138-571-32512-173431-00002B4775",
					["aID"] = "173431",
				}, -- [18]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 5820.013063,
					["damage_from"] = {
						["Màlfurione"] = true,
						["Kerisong"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Màlfurione"] = 1268,
						["Thorddin"] = 15031,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 16299.013063,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 16299.013063,
					["monster"] = true,
					["dps_started"] = false,
					["end_time"] = 1605060499,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Risen Skeleton",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 1460,
								["g_amt"] = 0,
								["n_max"] = 540,
								["targets"] = {
									["Màlfurione"] = 1268,
									["Thorddin"] = 15031,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 14839,
								["DODGE"] = 2,
								["b_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 66,
								["total"] = 16299,
								["r_amt"] = 0,
								["c_max"] = 749,
								["extra"] = {
								},
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 6,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["MISS"] = 3,
								["n_amt"] = 47,
								["a_amt"] = 0,
								["IMMUNE"] = 6,
							}, -- [1]
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["fight_component"] = true,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 67988.013063,
					["start_time"] = 1605060443,
					["serial"] = "Creature-0-3138-571-32512-170438-0001AB4775",
					["aID"] = "170438",
				}, -- [19]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 5036.038517000001,
					["damage_from"] = {
						["Kerisong"] = true,
						["Cat"] = true,
						["Thorddin"] = true,
						["Màlfurione"] = true,
						["Spitting Cobra <Ragoar>"] = true,
						["Ragoar"] = true,
						["Roeweena"] = true,
					},
					["targets"] = {
						["Màlfurione"] = 1368,
						["Thorddin"] = 13192,
						["Ragoar"] = 2246,
						["Roeweena"] = 5211,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 22017.038517,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 22017.038517,
					["fight_component"] = true,
					["monster"] = true,
					["end_time"] = 1605060530,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Lumbering Atrocity",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 1297,
								["g_amt"] = 0,
								["n_max"] = 834,
								["targets"] = {
									["Màlfurione"] = 0,
									["Thorddin"] = 9637,
									["Ragoar"] = 2246,
									["Roeweena"] = 1970,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 12556,
								["g_dmg"] = 0,
								["n_min"] = 0,
								["a_amt"] = 2,
								["counter"] = 28,
								["total"] = 13853,
								["r_amt"] = 0,
								["c_max"] = 1297,
								["b_dmg"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 1168,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 3,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["extra"] = {
								},
								["n_amt"] = 19,
								["MISS"] = 2,
								["DODGE"] = 3,
							}, -- [1]
							[323623] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 1368,
								["targets"] = {
									["Màlfurione"] = 1368,
									["Thorddin"] = 2649,
									["Roeweena"] = 2396,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 6413,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 6413,
								["c_max"] = 0,
								["id"] = 323623,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 1,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["a_dmg"] = 1073,
								["extra"] = {
								},
							},
							[289570] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 906,
								["targets"] = {
									["Thorddin"] = 906,
									["Roeweena"] = 845,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1751,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 1751,
								["c_max"] = 0,
								["id"] = 289570,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["aID"] = "173432",
					["friendlyfire"] = {
					},
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 100718.038517,
					["start_time"] = 1605060462,
					["serial"] = "Creature-0-3138-571-32512-173432-00002B4793",
					["dps_started"] = false,
				}, -- [20]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 1079.014327,
					["damage_from"] = {
						["Màlfurione"] = true,
						["Kerisong"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Màlfurione"] = 402,
						["Thorddin"] = 3794,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 4196.014327,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 4196.014327,
					["monster"] = true,
					["dps_started"] = false,
					["end_time"] = 1605060557,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Cult Shadowmage",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 350,
								["targets"] = {
									["Thorddin"] = 608,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 608,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 608,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_dmg"] = 0,
								["extra"] = {
								},
							}, -- [1]
							[333999] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 475,
								["targets"] = {
									["Màlfurione"] = 402,
									["Thorddin"] = 3186,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3588,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["total"] = 3588,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 333999,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 12,
								["a_amt"] = 0,
								["n_amt"] = 9,
								["extra"] = {
								},
								["IMMUNE"] = 3,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["fight_component"] = true,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 19372.014327,
					["start_time"] = 1605060528,
					["serial"] = "Creature-0-3138-571-32512-170441-0000AB47B1",
					["aID"] = "170441",
				}, -- [21]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 2385.014276,
					["damage_from"] = {
						["Kerisong"] = true,
						["Korfred"] = true,
						["Risen Ghoul <Màlfurione>"] = true,
						["Thorddin"] = true,
						["Màlfurione"] = true,
						["Pagnip"] = true,
						["Koanan"] = true,
						["Grimstefani"] = true,
					},
					["targets"] = {
						["Màlfurione"] = 3924,
						["Koanan"] = 1279,
						["Thorddin"] = 20860,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["total"] = 26063.014276,
					["classe"] = "UNKNOW",
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 26063.014276,
					["monster"] = true,
					["dps_started"] = false,
					["end_time"] = 1605060666,
					["on_hold"] = false,
					["tipo"] = 1,
					["nome"] = "Bone Giant",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 1211,
								["g_amt"] = 0,
								["n_max"] = 761,
								["targets"] = {
									["Màlfurione"] = 1897,
									["Koanan"] = 707,
									["Thorddin"] = 10348,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 11741,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 21,
								["total"] = 12952,
								["c_max"] = 1211,
								["r_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 1211,
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 1,
								["n_amt"] = 19,
								["extra"] = {
								},
								["MISS"] = 1,
							}, -- [1]
							[341827] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 341827,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[31277] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 572,
								["targets"] = {
									["Màlfurione"] = 342,
									["Koanan"] = 572,
									["Thorddin"] = 1006,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1920,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 1920,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 31277,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 2,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["extra"] = {
								},
								["a_amt"] = 0,
							},
							[341833] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 1096,
								["targets"] = {
									["Màlfurione"] = 1138,
									["Thorddin"] = 7667,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 8805,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 46,
								["total"] = 8805,
								["c_max"] = 0,
								["r_amt"] = 0,
								["id"] = 341833,
								["r_dmg"] = 0,
								["a_dmg"] = 1208,
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 2,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 44,
								["extra"] = {
								},
								["a_amt"] = 2,
							},
							[32736] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 662,
								["targets"] = {
									["Màlfurione"] = 547,
									["Thorddin"] = 1839,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2386,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 2386,
								["c_max"] = 0,
								["id"] = 32736,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 4,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
							[341840] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 341840,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_amt"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["friendlyfire"] = {
					},
					["fight_component"] = true,
					["custom"] = 0,
					["last_event"] = 0,
					["damage_taken"] = 212000.014276,
					["start_time"] = 1605060595,
					["serial"] = "Creature-0-3138-571-32512-170447-00002B47E9",
					["aID"] = "170447",
				}, -- [22]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 7285.059440999999,
					["damage_from"] = {
						["Poonkin"] = true,
						["Vyndicktator"] = true,
						["Roahn"] = true,
						["Madox"] = true,
						["Seyedots"] = true,
						["Pyfitl"] = true,
						["Tricksea"] = true,
						["Relynn"] = true,
						["Phfoxy"] = true,
						["Thorddin"] = true,
						["Psyles"] = true,
						["Zepnip"] = true,
						["Kendall"] = true,
						["Dirknight"] = true,
					},
					["targets"] = {
						["Terol"] = 133,
						["Roahn"] = 652,
						["Madox"] = 300,
						["Odins"] = 0,
						["Pyfitl"] = 2424,
						["Tricksea"] = 1541,
						["Relynn"] = 240,
						["Thorddin"] = 6941,
						["Psyles"] = 0,
						["Gyeder"] = 0,
						["Kendall"] = 355,
						["Dirknight"] = 136,
					},
					["serial"] = "Creature-0-3020-571-2273-168227-00012C91F3",
					["pets"] = {
					},
					["monster"] = true,
					["friendlyfire_total"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 12722.059441,
					["fight_component"] = true,
					["dps_started"] = false,
					["total"] = 12722.059441,
					["damage_taken"] = 139549.059441,
					["last_event"] = 0,
					["nome"] = "Decrepit Laborer",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 283,
								["targets"] = {
									["Terol"] = 133,
									["Vyndicktator"] = 0,
									["Roahn"] = 0,
									["Madox"] = 300,
									["Odins"] = 0,
									["Pyfitl"] = 1765,
									["Tricksea"] = 1220,
									["Thorddin"] = 5925,
									["Psyles"] = 0,
									["Gyeder"] = 0,
									["Kendall"] = 0,
									["Dirknight"] = 136,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 9479,
								["g_dmg"] = 0,
								["n_min"] = 0,
								["a_amt"] = 5,
								["counter"] = 87,
								["MISS"] = 6,
								["r_amt"] = 0,
								["c_max"] = 0,
								["b_dmg"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["m_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 10,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_dmg"] = 774,
								["n_amt"] = 68,
								["total"] = 9479,
								["DODGE"] = 3,
							}, -- [1]
							[48374] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 85,
								["targets"] = {
									["Relynn"] = 240,
									["Tricksea"] = 321,
									["Thorddin"] = 1016,
									["Kendall"] = 355,
									["Roahn"] = 652,
									["Pyfitl"] = 659,
									["Dirknight"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3243,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 48,
								["total"] = 3243,
								["c_max"] = 0,
								["id"] = 48374,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 80,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 7,
								["b_dmg"] = 0,
								["n_amt"] = 48,
								["a_amt"] = 1,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["end_time"] = 1605145091,
					["on_hold"] = false,
					["last_dps"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["friendlyfire"] = {
					},
					["start_time"] = 1605144999,
					["delay"] = 0,
					["aID"] = "168227",
				}, -- [23]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 3169.020006,
					["damage_from"] = {
						["Roahn"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Roahn"] = 652,
						["Furocìous"] = 261,
						["Thorddin"] = 2602,
					},
					["serial"] = "Creature-0-3020-571-2273-168247-00002C9100",
					["pets"] = {
					},
					["damage_taken"] = 22065.020006,
					["classe"] = "UNKNOW",
					["aID"] = "168247",
					["raid_targets"] = {
					},
					["total_without_pet"] = 3515.020006,
					["monster"] = true,
					["fight_component"] = true,
					["end_time"] = 1605145125,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Skeletal Footsoldier",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 355,
								["targets"] = {
									["Roahn"] = 291,
									["Furocìous"] = 261,
									["Thorddin"] = 1852,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2404,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 2404,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 1,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 8,
								["a_dmg"] = 346,
								["r_amt"] = 0,
							}, -- [1]
							[191887] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 380,
								["targets"] = {
									["Thorddin"] = 750,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 750,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 750,
								["c_max"] = 0,
								["id"] = 191887,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[87081] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 361,
								["targets"] = {
									["Roahn"] = 361,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 361,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 361,
								["c_max"] = 0,
								["id"] = 87081,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["dps_started"] = false,
					["friendlyfire"] = {
					},
					["total"] = 3515.020006,
					["custom"] = 0,
					["tipo"] = 1,
					["on_hold"] = false,
					["start_time"] = 1605145106,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
				}, -- [24]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 1363.011101,
					["damage_from"] = {
						["Zfb"] = true,
						["Vyndicktator"] = true,
						["Furocìous"] = true,
						["Seyedots"] = true,
						["Pyfitl"] = true,
						["Quicketl"] = true,
						["Roahn"] = true,
						["Thorddin"] = true,
						["Kendall"] = true,
						["Zepnip"] = true,
						["Ratrumbler"] = true,
						["Phfoxy"] = true,
					},
					["targets"] = {
						["Zfb"] = 1994,
						["Vyndicktator"] = 1442,
						["Furocìous"] = 1272,
						["Madox"] = 0,
						["Seyedots"] = 2194,
						["Bishopofrome"] = 4244,
						["Tricksea"] = 0,
						["Quicketl"] = 3668,
						["Kariyln"] = 0,
						["Thorddin"] = 8660,
						["Zepnip"] = 196,
						["Ratrumbler"] = 161,
						["Kendall"] = 1624,
						["Pyfitl"] = 899,
					},
					["serial"] = "Creature-0-3020-571-2273-168184-00002C9294",
					["pets"] = {
					},
					["damage_taken"] = 109789.011101,
					["friendlyfire_total"] = 0,
					["aID"] = "168184",
					["raid_targets"] = {
					},
					["total_without_pet"] = 26354.011101,
					["dps_started"] = false,
					["monster"] = true,
					["end_time"] = 1605145270,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Colossal Plaguespreader",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 1,
								["c_dmg"] = 1426,
								["g_amt"] = 0,
								["n_max"] = 651,
								["targets"] = {
									["Thorddin"] = 4550,
									["Pyfitl"] = 255,
									["Tricksea"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3379,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 4805,
								["c_max"] = 1426,
								["a_amt"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["m_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 2,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 255,
								["n_amt"] = 6,
								["a_dmg"] = 1426,
								["r_amt"] = 0,
							}, -- [1]
							[28405] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Kariyln"] = 0,
									["Madox"] = 0,
									["Pyfitl"] = 0,
									["Zepnip"] = 0,
									["Tricksea"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 28405,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[63546] = {
								["c_amt"] = 0,
								["b_amt"] = 1,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 2195,
								["targets"] = {
									["Zfb"] = 1994,
									["Vyndicktator"] = 1442,
									["Furocìous"] = 1272,
									["Madox"] = 0,
									["Seyedots"] = 2194,
									["Bishopofrome"] = 4244,
									["Tricksea"] = 0,
									["Quicketl"] = 3668,
									["Kariyln"] = 0,
									["Thorddin"] = 4110,
									["Zepnip"] = 196,
									["Ratrumbler"] = 161,
									["Kendall"] = 1624,
									["Pyfitl"] = 644,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 21549,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 16,
								["total"] = 21549,
								["c_max"] = 0,
								["id"] = 63546,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 14936,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 644,
								["n_amt"] = 16,
								["a_amt"] = 10,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["fight_component"] = true,
					["friendlyfire"] = {
					},
					["total"] = 26354.011101,
					["custom"] = 0,
					["tipo"] = 1,
					["on_hold"] = false,
					["start_time"] = 1605145246,
					["delay"] = 0,
					["classe"] = "UNKNOW",
				}, -- [25]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 29225.015395,
					["damage_from"] = {
						["Slimchance"] = true,
						["Furocìous"] = true,
						["Madox"] = true,
						["Bishopofrome"] = true,
						["Kÿa-Proudmoore"] = true,
						["Vociferos"] = true,
						["Mnemae"] = true,
						["Harnongo"] = true,
						["Eiisiss"] = true,
						["Saltybro"] = true,
						["Vyndicktator"] = true,
						["Morrer"] = true,
						["Seyedots"] = true,
						["Shadowfiend <Purrplekitty>"] = true,
						["Valancia"] = true,
						["Unknown"] = true,
						["Zfb"] = true,
						["Cøunterparts"] = true,
						["Magus of the Dead <Quicketl>"] = true,
						["Dhurmadin"] = true,
						["Gyeder <Gyeder>"] = true,
						["Sarbah"] = true,
						["Jaymacc"] = true,
						["Koják"] = true,
						["Ditronus"] = true,
						["Poonkin"] = true,
						["Voìdrunner"] = true,
						["Gardëk"] = true,
						["Relynn"] = true,
						["Quicketl"] = true,
						["Arllyn"] = true,
						["Carea"] = true,
						["Tråphouse"] = true,
						["Void Tendril"] = true,
						["Adrielia"] = true,
						["Heckblazer"] = true,
						["Malithra"] = true,
						["Ratrumbler"] = true,
						["Lavits"] = true,
						["Zalyrra"] = true,
						["Water Elemental"] = true,
						["Galaysia"] = true,
						["Discobeaver"] = true,
						["Mevlinjr"] = true,
						["Tyraan"] = true,
						["Mackallouroy"] = true,
						["Nutswitch"] = true,
						["Darathas"] = true,
						["Meitian"] = true,
						["Vellitus"] = true,
						["Ghostree"] = true,
						["Eunbin"] = true,
						["Kash"] = true,
						["Spirit Wolf <Lavits>"] = true,
						["Tricksea"] = true,
						["Vygo"] = true,
						["Pyfitl"] = true,
						["Darkglare <Seyedots>"] = true,
						["Bird of Prey"] = true,
						["Roahn"] = true,
						["Ayeumi"] = true,
						["Sneakyball"] = true,
						["Snert"] = true,
						["Memourial"] = true,
						["Douglinhas"] = true,
						["Maloliente"] = true,
						["Vaethiran"] = true,
						["Purrplekitty"] = true,
						["Dominil"] = true,
						["Kendall"] = true,
						["Lianthia"] = true,
						["Gyeder"] = true,
						["Phfoxy"] = true,
						["Cat"] = true,
						["Zepnip"] = true,
						["Executie"] = true,
						["Thoragnar"] = true,
						["Kegshots"] = true,
						["Úther"] = true,
						["Mctreats"] = true,
						["Lailyann"] = true,
						["Talanisar"] = true,
						["dad"] = true,
						["Wormbasher"] = true,
						["Thorddin"] = true,
						["Torvas"] = true,
						["Crane"] = true,
						["Slamu"] = true,
						["Anakki"] = true,
						["Ocissor"] = true,
						["Devizerus"] = true,
						["Spirit Beast"] = true,
						["Mindbender <Mackallouroy>"] = true,
						["Sitta"] = true,
						["Army of the Dead <Quicketl>"] = true,
					},
					["targets"] = {
						["Slimchance"] = 4773,
						["Furocìous"] = 7197,
						["Madox"] = 8331,
						["Bishopofrome"] = 4889,
						["Kÿa-Proudmoore"] = 4663,
						["Thorddin"] = 5661,
						["Mnemae"] = 13982,
						["Torvas"] = 10828,
						["Eiisiss"] = 14938,
						["Saltybro"] = 7456,
						["Vyndicktator"] = 8861,
						["Morrer"] = 4629,
						["Seyedots"] = 4624,
						["Shadowfiend <Purrplekitty>"] = 227,
						["Valancia"] = 20713,
						["Zfb"] = 4870,
						["Cøunterparts"] = 4933,
						["Dhurmadin"] = 17337,
						["Gyeder <Gyeder>"] = 0,
						["Sarbah"] = 12134,
						["Jaymacc"] = 3302,
						["Koják"] = 11462,
						["Ditronus"] = 9823,
						["Poonkin"] = 4822,
						["Memourial"] = 5504,
						["Army of the Dead <Quicketl>"] = 301,
						["Lavits"] = 5349,
						["Quicketl"] = 6396,
						["Arllyn"] = 4841,
						["Carea"] = 4663,
						["Spirit Beast"] = 1527,
						["Ayeumi"] = 18630,
						["Zalyrra"] = 13936,
						["dad"] = 6340,
						["Malithra"] = 4565,
						["Ratrumbler"] = 137,
						["Tråphouse"] = 4870,
						["Ocissor"] = 7338,
						["Water Elemental"] = 0,
						["Galaysia"] = 4459,
						["Discobeaver"] = 4485,
						["Mevlinjr"] = 107,
						["Vaethiran"] = 8416,
						["Mackallouroy"] = 5000,
						["Voìdrunner"] = 4803,
						["Vociferos"] = 4603,
						["Meitian"] = 8740,
						["Mctreats"] = 4463,
						["Ghostree"] = 4728,
						["Spirit Wolf <Lavits>"] = 99,
						["Crane"] = 9525,
						["Eunbin"] = 4397,
						["Slamu"] = 151,
						["Vygo"] = 10069,
						["Bird of Prey"] = 185,
						["Darkglare <Seyedots>"] = 0,
						["Roahn"] = 17810,
						["Darathas"] = 4795,
						["Vellitus"] = 6139,
						["Sneakyball"] = 4727,
						["Snert"] = 4763,
						["Nutswitch"] = 4668,
						["Douglinhas"] = 4920,
						["Maloliente"] = 6074,
						["Void Tendril"] = 24801,
						["Purrplekitty"] = 4680,
						["Dominil"] = 4839,
						["Tyraan"] = 8715,
						["Úther"] = 11587,
						["Phfoxy"] = 4606,
						["Kegshots"] = 12830,
						["Cat"] = 254,
						["Harnongo"] = 4892,
						["Kendall"] = 7939,
						["Thoragnar"] = 8690,
						["Heckblazer"] = 3603,
						["Lianthia"] = 5057,
						["Gyeder"] = 3077,
						["Lailyann"] = 4324,
						["Talanisar"] = 4778,
						["Relynn"] = 4601,
						["Wormbasher"] = 127,
						["Tricksea"] = 4945,
						["Pyfitl"] = 9279,
						["Kash"] = 245,
						["Executie"] = 9150,
						["Anakki"] = 4358,
						["Adrielia"] = 4546,
						["Devizerus"] = 15131,
						["Gardëk"] = 4556,
						["Mindbender <Mackallouroy>"] = 0,
						["Sitta"] = 4844,
						["Zepnip"] = 0,
					},
					["serial"] = "Creature-0-3020-571-2273-174051-00002C92B7",
					["pets"] = {
					},
					["damage_taken"] = 1312918.015395,
					["friendlyfire_total"] = 0,
					["aID"] = "174051",
					["raid_targets"] = {
					},
					["total_without_pet"] = 579432.015395,
					["dps_started"] = false,
					["monster"] = true,
					["end_time"] = 1605145418,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Trollgore",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 1,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 4462,
								["targets"] = {
									["dad"] = 5800,
									["Pyfitl"] = 3258,
									["Spirit Beast"] = 1470,
									["Crane"] = 8864,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 19392,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 10,
								["total"] = 19392,
								["c_max"] = 0,
								["DODGE"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 2308,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 8,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[341362] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 341362,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[341357] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 1398,
								["targets"] = {
									["Slimchance"] = 0,
									["Furocìous"] = 2902,
									["Madox"] = 4116,
									["Zepnip"] = 0,
									["Kÿa-Proudmoore"] = 0,
									["Vociferos"] = 0,
									["Mnemae"] = 9422,
									["Torvas"] = 6108,
									["Eiisiss"] = 10818,
									["Vaethiran"] = 3898,
									["Vyndicktator"] = 4350,
									["Executie"] = 4372,
									["Seyedots"] = 0,
									["Shadowfiend <Purrplekitty>"] = 227,
									["Valancia"] = 15894,
									["Cøunterparts"] = 0,
									["Dhurmadin"] = 12802,
									["Gyeder <Gyeder>"] = 0,
									["Sarbah"] = 7971,
									["Mindbender <Mackallouroy>"] = 0,
									["Koják"] = 7308,
									["Ditronus"] = 5155,
									["Memourial"] = 948,
									["Army of the Dead <Quicketl>"] = 301,
									["Lavits"] = 490,
									["Quicketl"] = 1606,
									["Arllyn"] = 0,
									["Carea"] = 0,
									["Kendall"] = 3233,
									["Ayeumi"] = 14565,
									["dad"] = 540,
									["Malithra"] = 0,
									["Water Elemental"] = 0,
									["Galaysia"] = 0,
									["Mevlinjr"] = 107,
									["Heckblazer"] = 0,
									["Mackallouroy"] = 0,
									["Zalyrra"] = 10139,
									["Spirit Beast"] = 57,
									["Meitian"] = 3832,
									["Vellitus"] = 1458,
									["Roahn"] = 13021,
									["Harnongo"] = 0,
									["Spirit Wolf <Lavits>"] = 99,
									["Pyfitl"] = 4865,
									["Slamu"] = 151,
									["Vygo"] = 5050,
									["Ghostree"] = 0,
									["Darkglare <Seyedots>"] = 0,
									["Tyraan"] = 3827,
									["Jaymacc"] = 0,
									["Crane"] = 661,
									["Sneakyball"] = 0,
									["Snert"] = 0,
									["Saltybro"] = 2832,
									["Douglinhas"] = 0,
									["Maloliente"] = 1457,
									["Ocissor"] = 3190,
									["Purrplekitty"] = 0,
									["Dominil"] = 0,
									["Adrielia"] = 0,
									["Thorddin"] = 1178,
									["Phfoxy"] = 0,
									["Kegshots"] = 7803,
									["Cat"] = 254,
									["Gardëk"] = 0,
									["Darathas"] = 0,
									["Thoragnar"] = 3735,
									["Úther"] = 10178,
									["Lianthia"] = 0,
									["Void Tendril"] = 24801,
									["Eunbin"] = 0,
									["Talanisar"] = 0,
									["Tricksea"] = 0,
									["Wormbasher"] = 127,
									["Relynn"] = 0,
									["Gyeder"] = 0,
									["Kash"] = 245,
									["Morrer"] = 0,
									["Anakki"] = 0,
									["Nutswitch"] = 0,
									["Devizerus"] = 10320,
									["Mctreats"] = 0,
									["Bird of Prey"] = 185,
									["Ratrumbler"] = 137,
									["Bishopofrome"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 226735,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 355,
								["total"] = 226735,
								["c_max"] = 0,
								["id"] = 341357,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 24169,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 355,
								["a_amt"] = 33,
								["r_amt"] = 0,
							},
							[49380] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3667,
								["targets"] = {
									["Slimchance"] = 4773,
									["Furocìous"] = 4295,
									["Madox"] = 4215,
									["Bishopofrome"] = 4889,
									["Kÿa-Proudmoore"] = 4663,
									["Thorddin"] = 4483,
									["Mnemae"] = 4560,
									["Harnongo"] = 4892,
									["Eiisiss"] = 4120,
									["Vaethiran"] = 4518,
									["Vyndicktator"] = 4511,
									["Morrer"] = 4629,
									["Seyedots"] = 4624,
									["Valancia"] = 4819,
									["Zfb"] = 4870,
									["Cøunterparts"] = 4933,
									["Dhurmadin"] = 4535,
									["Sarbah"] = 4163,
									["Jaymacc"] = 3302,
									["Koják"] = 4154,
									["Ditronus"] = 4668,
									["Poonkin"] = 4822,
									["Voìdrunner"] = 4803,
									["Gardëk"] = 4556,
									["Lavits"] = 4859,
									["Quicketl"] = 4790,
									["Arllyn"] = 4841,
									["Carea"] = 4663,
									["Kendall"] = 4706,
									["Ayeumi"] = 4065,
									["Heckblazer"] = 3603,
									["Malithra"] = 4565,
									["Galaysia"] = 4459,
									["Discobeaver"] = 4485,
									["Mackallouroy"] = 5000,
									["Meitian"] = 4908,
									["Vellitus"] = 4681,
									["Roahn"] = 4789,
									["Pyfitl"] = 1156,
									["Tricksea"] = 4945,
									["Vygo"] = 5019,
									["Sneakyball"] = 4727,
									["Snert"] = 4763,
									["Douglinhas"] = 4920,
									["Maloliente"] = 4617,
									["Zalyrra"] = 3797,
									["Purrplekitty"] = 4680,
									["Dominil"] = 4839,
									["Torvas"] = 4720,
									["Tråphouse"] = 4870,
									["Kegshots"] = 5027,
									["Phfoxy"] = 4606,
									["Ocissor"] = 4148,
									["Adrielia"] = 4546,
									["Gyeder"] = 3077,
									["Thoragnar"] = 4955,
									["Lianthia"] = 5057,
									["Úther"] = 1409,
									["Eunbin"] = 4397,
									["Lailyann"] = 4324,
									["Talanisar"] = 4778,
									["Executie"] = 4778,
									["Ghostree"] = 4728,
									["Mctreats"] = 4463,
									["Tyraan"] = 4888,
									["Vociferos"] = 4603,
									["Nutswitch"] = 4668,
									["Anakki"] = 4358,
									["Darathas"] = 4795,
									["Devizerus"] = 4811,
									["Memourial"] = 4556,
									["Saltybro"] = 4624,
									["Sitta"] = 4844,
									["Relynn"] = 4601,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 333305,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 146,
								["total"] = 333305,
								["c_max"] = 0,
								["IMMUNE"] = 1,
								["id"] = 49380,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 53,
								["c_min"] = 0,
								["successful_casted"] = 2,
								["m_amt"] = 0,
								["n_amt"] = 145,
								["a_dmg"] = 113536,
								["r_amt"] = 0,
							},
							[341361] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 341361,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["fight_component"] = true,
					["friendlyfire"] = {
					},
					["total"] = 579432.015395,
					["custom"] = 0,
					["tipo"] = 1,
					["on_hold"] = false,
					["start_time"] = 1605145394,
					["delay"] = 0,
					["classe"] = "UNKNOW",
				}, -- [26]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 10308.032029,
					["damage_from"] = {
						["Poonkin"] = true,
						["Tyraan"] = true,
						["Mctreats"] = true,
						["Pyfitl"] = true,
						["Relynn"] = true,
						["Kÿa-Proudmoore"] = true,
						["Warpedsanity"] = true,
						["Juk'vhug <Tyraan>"] = true,
						["Hathnak <Warpedsanity>"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Poonkin"] = 1779,
						["Tyraan"] = 881,
						["Mctreats"] = 2718,
						["Mctreats <Mctreats>"] = 2538,
						["Juk'vhug <Tyraan>"] = 10359,
						["Tricksea"] = 1625,
						["Relynn"] = 5128,
						["Kÿa-Proudmoore"] = 1101,
						["Warpedsanity"] = 2670,
						["Thorddin"] = 7993,
						["Hathnak <Warpedsanity>"] = 4551,
						["Pyfitl"] = 12800,
					},
					["serial"] = "Creature-0-3020-571-2273-169781-00002C926A",
					["pets"] = {
					},
					["damage_taken"] = 145413.032029,
					["classe"] = "UNKNOW",
					["aID"] = "169781",
					["raid_targets"] = {
					},
					["total_without_pet"] = 54143.032029,
					["monster"] = true,
					["fight_component"] = true,
					["end_time"] = 1605145501,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Vile Sludgefiend",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 8,
								["c_dmg"] = 1298,
								["g_amt"] = 0,
								["n_max"] = 757,
								["targets"] = {
									["Poonkin"] = 1779,
									["Mctreats"] = 2013,
									["Mctreats <Mctreats>"] = 784,
									["Pyfitl"] = 9600,
									["Tricksea"] = 1340,
									["Relynn"] = 4040,
									["Kÿa-Proudmoore"] = 1101,
									["Juk'vhug <Tyraan>"] = 8446,
									["Warpedsanity"] = 1812,
									["Thorddin"] = 5055,
									["Juk'vhug"] = 0,
									["Hathnak <Warpedsanity>"] = 3103,
									["Hathnak"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 37775,
								["a_amt"] = 49,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 193,
								["DODGE"] = 7,
								["r_amt"] = 0,
								["c_max"] = 654,
								["b_dmg"] = 1264,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["m_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 16,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["a_dmg"] = 10862,
								["n_amt"] = 158,
								["total"] = 39073,
								["MISS"] = 10,
							}, -- [1]
							[329442] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 149,
								["targets"] = {
									["Tyraan"] = 881,
									["Mctreats"] = 705,
									["Mctreats <Mctreats>"] = 1754,
									["Juk'vhug <Tyraan>"] = 1913,
									["Tricksea"] = 285,
									["Relynn"] = 1088,
									["Warpedsanity"] = 858,
									["Thorddin"] = 2938,
									["Hathnak <Warpedsanity>"] = 1448,
									["Pyfitl"] = 3200,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 15070,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 110,
								["total"] = 15070,
								["c_max"] = 0,
								["id"] = 329442,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 3387,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 20,
								["b_dmg"] = 0,
								["n_amt"] = 110,
								["a_amt"] = 24,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["dps_started"] = false,
					["friendlyfire"] = {
					},
					["total"] = 54143.032029,
					["custom"] = 0,
					["tipo"] = 1,
					["on_hold"] = false,
					["start_time"] = 1605145393,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
				}, -- [27]
				{
					["flag_original"] = 2600,
					["totalabsorbed"] = 0.046342,
					["damage_from"] = {
						["Poonkin"] = true,
						["Tyraan"] = true,
						["Mctreats"] = true,
						["Pyfitl"] = true,
						["Relynn"] = true,
						["Kÿa-Proudmoore"] = true,
						["Thorddin"] = true,
						["Warpedsanity"] = true,
						["Hathnak <Warpedsanity>"] = true,
						["Juk'vhug <Tyraan>"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["damage_taken"] = 30754.046342,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.046342,
					["serial"] = "Creature-0-3020-571-2273-173888-00002C9294",
					["dps_started"] = false,
					["end_time"] = 1605145501,
					["on_hold"] = false,
					["last_dps"] = 0,
					["nome"] = "Gloomshroom",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 2,
					},
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["aID"] = "173888",
					["custom"] = 0,
					["tipo"] = 1,
					["friendlyfire"] = {
					},
					["start_time"] = 1605145498,
					["delay"] = 0,
					["total"] = 0.046342,
				}, -- [28]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 8779.040205000001,
					["damage_from"] = {
						["Tyraan"] = true,
						["Mctreats"] = true,
						["Mctreats <Mctreats>"] = true,
						["Pyfitl"] = true,
						["Tricksea"] = true,
						["Relynn"] = true,
						["Kÿa-Proudmoore"] = true,
						["Thorddin"] = true,
						["Juk'vhug <Tyraan>"] = true,
						["Hathnak <Warpedsanity>"] = true,
						["Warpedsanity"] = true,
					},
					["targets"] = {
						["Poonkin"] = 0,
						["Tyraan"] = 5870,
						["Mctreats"] = 0,
						["Mctreats <Mctreats>"] = 80,
						["Pyfitl"] = 11383,
						["Tricksea"] = 2544,
						["Relynn"] = 1445,
						["Thorddin"] = 12593,
						["Hathnak <Warpedsanity>"] = 476,
						["Juk'vhug <Tyraan>"] = 6398,
					},
					["serial"] = "Creature-0-3020-571-2273-169784-00002C926A",
					["pets"] = {
					},
					["damage_taken"] = 131940.040205,
					["friendlyfire_total"] = 0,
					["aID"] = "169784",
					["raid_targets"] = {
					},
					["total_without_pet"] = 40789.040205,
					["dps_started"] = false,
					["monster"] = true,
					["end_time"] = 1605145547,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Putrid Experiment",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 7,
								["c_dmg"] = 3096,
								["g_amt"] = 0,
								["n_max"] = 954,
								["targets"] = {
									["Poonkin"] = 0,
									["Tyraan"] = 4329,
									["Juk'vhug"] = 0,
									["Tricksea"] = 2112,
									["Relynn"] = 1445,
									["Thorddin"] = 6988,
									["Pyfitl"] = 8934,
									["Hathnak <Warpedsanity>"] = 447,
									["Juk'vhug <Tyraan>"] = 5053,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 26212,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 64,
								["a_amt"] = 21,
								["total"] = 29308,
								["c_max"] = 1860,
								["DODGE"] = 6,
								["id"] = 1,
								["r_dmg"] = 0,
								["b_dmg"] = 2439,
								["a_dmg"] = 11963,
								["m_crit"] = 0,
								["PARRY"] = 3,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 53,
								["extra"] = {
								},
								["r_amt"] = 0,
							}, -- [1]
							[50107] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 321,
								["targets"] = {
									["Tyraan"] = 632,
									["Mctreats"] = 0,
									["Mctreats <Mctreats>"] = 80,
									["Pyfitl"] = 2323,
									["Tricksea"] = 288,
									["Relynn"] = 0,
									["Thorddin"] = 4399,
									["Hathnak <Warpedsanity>"] = 29,
									["Juk'vhug <Tyraan>"] = 175,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 7926,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 47,
								["total"] = 7926,
								["c_max"] = 0,
								["id"] = 50107,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 2194,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 47,
								["a_amt"] = 8,
								["r_amt"] = 0,
							},
							[60678] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 161,
								["targets"] = {
									["Relynn"] = 0,
									["Tyraan"] = 909,
									["Thorddin"] = 1206,
									["Juk'vhug <Tyraan>"] = 1170,
									["Pyfitl"] = 126,
									["Tricksea"] = 144,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3555,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 26,
								["a_amt"] = 10,
								["total"] = 3555,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = 60678,
								["r_dmg"] = 0,
								["b_dmg"] = 0,
								["a_dmg"] = 1506,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["c_min"] = 0,
								["successful_casted"] = 10,
								["m_amt"] = 0,
								["n_amt"] = 24,
								["extra"] = {
								},
								["r_amt"] = 0,
							},
							[50106] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 50106,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 3,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["fight_component"] = true,
					["friendlyfire"] = {
					},
					["total"] = 40789.040205,
					["custom"] = 0,
					["tipo"] = 1,
					["on_hold"] = false,
					["start_time"] = 1605145456,
					["delay"] = 0,
					["classe"] = "UNKNOW",
				}, -- [29]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 4483.093866000001,
					["damage_from"] = {
						["Thorddin"] = true,
						["Ravenscarlet"] = true,
						["Calfin"] = true,
						["Tzekel"] = true,
					},
					["targets"] = {
						["Verk"] = 0,
						["Alannalouwho"] = 0,
						["Silver Covenant Guardian"] = 11396,
						["Tzekel"] = 752,
						["Sunreaver Guardian"] = 0,
						["Ravenscarlet"] = 950,
						["Cat"] = 0,
						["Thorddin"] = 8303,
						["Argent Peacekeeper"] = 744,
					},
					["delay"] = 0,
					["pets"] = {
					},
					["classe"] = "UNKNOW",
					["monster"] = true,
					["aID"] = "",
					["raid_targets"] = {
					},
					["total_without_pet"] = 22145.093866,
					["total"] = 22145.093866,
					["fight_component"] = true,
					["end_time"] = 1605189553,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Darksworn Sentry",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 2848,
								["g_amt"] = 0,
								["n_max"] = 662,
								["targets"] = {
									["Verk"] = 0,
									["Alannalouwho"] = 0,
									["Silver Covenant Guardian"] = 11396,
									["Tzekel"] = 752,
									["Sunreaver Guardian"] = 0,
									["Ravenscarlet"] = 662,
									["Cat"] = 0,
									["Thorddin"] = 6132,
									["Argent Peacekeeper"] = 744,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 16838,
								["DODGE"] = 2,
								["n_min"] = 0,
								["MISS"] = 3,
								["counter"] = 62,
								["r_amt"] = 0,
								["total"] = 19686,
								["c_max"] = 797,
								["a_dmg"] = 1266,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["c_min"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 6,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 47,
								["a_amt"] = 4,
								["g_dmg"] = 0,
							}, -- [1]
							[16564] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 339,
								["targets"] = {
									["Ravenscarlet"] = 288,
									["Alannalouwho"] = 0,
									["Thorddin"] = 2171,
									["Cat"] = 0,
									["Silver Covenant Guardian"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 2459,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 8,
								["total"] = 2459,
								["c_max"] = 0,
								["id"] = 16564,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 327,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 14,
								["b_dmg"] = 0,
								["n_amt"] = 8,
								["a_amt"] = 1,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 76016.093866,
					["on_hold"] = false,
					["friendlyfire_total"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["friendlyfire"] = {
					},
					["start_time"] = 1605189427,
					["serial"] = "Vehicle-0-3023-571-32109-171768-00002D3723",
					["dps_started"] = false,
				}, -- [30]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.007495,
					["damage_from"] = {
						["Thorddin"] = true,
						["Dirknight"] = true,
					},
					["targets"] = {
					},
					["serial"] = "Creature-0-3023-571-32355-168025-00002DB8DF",
					["pets"] = {
					},
					["fight_component"] = true,
					["aID"] = "168025",
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.007495,
					["dps_started"] = false,
					["monster"] = true,
					["total"] = 0.007495,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Cultist Corrupter",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Dirknight"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["MISS"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
						},
						["tipo"] = 2,
					},
					["end_time"] = 1605221261,
					["friendlyfire"] = {
					},
					["on_hold"] = false,
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 7320.007495,
					["start_time"] = 1605221258,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
				}, -- [31]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 1255.004006,
					["damage_from"] = {
						["Thorddin"] = true,
					},
					["targets"] = {
						["Thorddin"] = 2194,
					},
					["serial"] = "Creature-0-3023-571-32355-169667-00002DBB49",
					["pets"] = {
					},
					["fight_component"] = true,
					["aID"] = "169667",
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 2194.004006,
					["dps_started"] = false,
					["monster"] = true,
					["total"] = 2194.004006,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Vrykul Necrolord",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 592,
								["g_amt"] = 0,
								["n_max"] = 347,
								["targets"] = {
									["Thorddin"] = 939,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 347,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 939,
								["c_max"] = 592,
								["a_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["m_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[50027] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 24,
								["targets"] = {
									["Thorddin"] = 48,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 48,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 48,
								["c_max"] = 0,
								["id"] = 50027,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
							[32712] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 299,
								["targets"] = {
									["Thorddin"] = 299,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 299,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 299,
								["c_max"] = 0,
								["id"] = 32712,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 299,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 1,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["a_amt"] = 1,
								["r_amt"] = 0,
							},
							[9613] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 466,
								["targets"] = {
									["Thorddin"] = 908,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 908,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 908,
								["c_max"] = 0,
								["id"] = 9613,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 2,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["a_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["end_time"] = 1605221298,
					["friendlyfire"] = {
					},
					["on_hold"] = false,
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 8596.004006,
					["start_time"] = 1605221279,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
				}, -- [32]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 819.0127560000001,
					["damage_from"] = {
						["Daswarr"] = true,
						["Thorddin"] = true,
					},
					["targets"] = {
						["Daswarr"] = 819,
					},
					["serial"] = "Creature-0-3023-571-32054-171809-0000ADFCB0",
					["pets"] = {
					},
					["end_time"] = 1605237956,
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 819.0127560000001,
					["dps_started"] = false,
					["monster"] = true,
					["total"] = 819.0127560000001,
					["last_dps"] = 0,
					["last_event"] = 0,
					["nome"] = "Brittle Boneguard",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 176,
								["targets"] = {
									["Daswarr"] = 819,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 819,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 9,
								["total"] = 819,
								["c_max"] = 0,
								["DODGE"] = 1,
								["id"] = 1,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["b_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["m_amt"] = 0,
								["n_amt"] = 8,
								["a_dmg"] = 0,
								["r_amt"] = 0,
							}, -- [1]
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 12029.012756,
					["on_hold"] = false,
					["friendlyfire_total"] = 0,
					["custom"] = 0,
					["tipo"] = 1,
					["friendlyfire"] = {
					},
					["start_time"] = 1605237946,
					["delay"] = 0,
					["aID"] = "171809",
				}, -- [33]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["healing_from"] = {
						["Vaethiran"] = true,
						["Ræquan"] = true,
						["Thorddin"] = true,
						["ßlyss"] = true,
						["Relynn"] = true,
						["Chuckleê"] = true,
						["Gardëk"] = true,
					},
					["pets"] = {
						"Altered Ghoul <Thorddin>", -- [1]
					},
					["iniciar_hps"] = false,
					["aID"] = "76-057FA942",
					["totalover"] = 5128.514869000001,
					["total_without_pet"] = 123205.514869,
					["total"] = 153888.514869,
					["targets_absorbs"] = {
						["Thorddin"] = 0,
					},
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-057FA942",
					["totalabsorb"] = 83178.51486899999,
					["last_hps"] = 0,
					["targets"] = {
						["Thorddin"] = 0,
						["Altered Ghoul <Thorddin>"] = 0,
					},
					["totalover_without_pet"] = 0.5148689999999999,
					["healing_taken"] = 129755.514869,
					["fight_component"] = true,
					["end_time"] = 1605058494,
					["targets_overheal"] = {
						["Thorddin"] = 0,
					},
					["tipo"] = 2,
					["nome"] = "Thorddin",
					["spells"] = {
						["_ActorTable"] = {
							[269279] = {
								["c_amt"] = 0,
								["totalabsorb"] = 64373,
								["targets_overheal"] = {
								},
								["n_max"] = 867,
								["targets"] = {
									["Thorddin"] = 64373,
								},
								["n_min"] = 0,
								["counter"] = 237,
								["overheal"] = 0,
								["total"] = 64373,
								["c_max"] = 0,
								["id"] = 269279,
								["targets_absorbs"] = {
									["Thorddin"] = 64373,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 237,
								["n_curado"] = 64373,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[85673] = {
								["c_amt"] = 3,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 2409,
								["targets"] = {
									["Thorddin"] = 18045,
								},
								["n_min"] = 0,
								["counter"] = 6,
								["overheal"] = 0,
								["total"] = 18045,
								["c_max"] = 4160,
								["id"] = 85673,
								["targets_absorbs"] = {
								},
								["c_curado"] = 12221,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 3,
								["n_curado"] = 5824,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[303380] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thorddin"] = 41,
								},
								["n_max"] = 271,
								["targets"] = {
									["Thorddin"] = 1583,
								},
								["n_min"] = 0,
								["counter"] = 5,
								["overheal"] = 41,
								["total"] = 1583,
								["c_max"] = 542,
								["id"] = 303380,
								["targets_absorbs"] = {
								},
								["c_curado"] = 542,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 4,
								["n_curado"] = 1041,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[19750] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thorddin"] = 0,
								},
								["n_max"] = 2134,
								["targets"] = {
									["Thorddin"] = 20399,
								},
								["n_min"] = 0,
								["counter"] = 11,
								["overheal"] = 0,
								["total"] = 20399,
								["c_max"] = 0,
								["id"] = 19750,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 11,
								["n_curado"] = 20399,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[184662] = {
								["c_amt"] = 0,
								["totalabsorb"] = 9916,
								["targets_overheal"] = {
								},
								["n_max"] = 3019,
								["targets"] = {
									["Thorddin"] = 9916,
								},
								["n_min"] = 0,
								["counter"] = 20,
								["overheal"] = 0,
								["total"] = 9916,
								["c_max"] = 0,
								["id"] = 184662,
								["targets_absorbs"] = {
									["Thorddin"] = 9916,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 20,
								["n_curado"] = 9916,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[303390] = {
								["c_amt"] = 0,
								["totalabsorb"] = 8889,
								["targets_overheal"] = {
								},
								["n_max"] = 1167,
								["targets"] = {
									["Thorddin"] = 8889,
								},
								["n_min"] = 0,
								["counter"] = 24,
								["overheal"] = 0,
								["total"] = 8889,
								["c_max"] = 0,
								["id"] = 303390,
								["targets_absorbs"] = {
									["Thorddin"] = 8889,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 24,
								["n_curado"] = 8889,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["grupo"] = true,
					["classe"] = "PALADIN",
					["totaldenied"] = 0.5148689999999999,
					["custom"] = 0,
					["last_event"] = 0,
					["spec"] = 70,
					["start_time"] = 1605057763,
					["delay"] = 0,
					["heal_enemy_amt"] = 0,
				}, -- [1]
				{
					["flag_original"] = 1304,
					["healing_from"] = {
						["ßlyss"] = true,
						["Chuckleê"] = true,
						["Lethumper"] = true,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["classe"] = "MONK",
					["totalover"] = 40342.015735,
					["total_without_pet"] = 177757.015735,
					["total"] = 177757.015735,
					["spec"] = 270,
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-0B1646CC",
					["totalabsorb"] = 8579.015735,
					["last_hps"] = 0,
					["targets"] = {
						["Onewithbind"] = 1002,
						["Hunterdo"] = 12672,
						["Gnuxlock"] = 1772,
						["Friskkilla"] = 671,
						["Saíx"] = 714,
						["Erincia"] = 2517,
						["Boriis"] = 1029,
						["Thorddin"] = 2946,
						["Swòrds"] = 20824,
						["Ddoczd"] = 370,
						["Solangè"] = 1166,
						["Liléath"] = 3544,
						["Hausstalker"] = 3397,
						["Senyth"] = 5612,
						["Mortemitera"] = 7732,
						["Jessicazeal"] = 883,
						["Endell"] = 962,
						["Sigrynth"] = 2083,
						["Weefermadnes"] = 2830,
						["Jôêkêr"] = 918,
						["Leglessfatty"] = 3363,
						["Baayy"] = 1831,
						["Caltonse"] = 1082,
						["Ellylanah"] = 1873,
						["Chuckleê"] = 8969,
						["Douginator"] = 443,
						["Nilrem"] = 2158,
						["Pallore"] = 1167,
						["ßlyss"] = 588,
						["Qüelana"] = 1919,
						["Kruniak"] = 2912,
						["Ræquan"] = 1560,
						["Skittlesx"] = 984,
						["Allyliia"] = 1420,
						["Flup"] = 458,
						["Saduz"] = 879,
						["Cósmós"] = 61486,
						["Bobaloo"] = 4333,
						["Marshmallowx"] = 155,
						["Taelory"] = 928,
						["Bürgo"] = 1790,
						["Lethumper"] = 3815,
					},
					["totalover_without_pet"] = 0.015735,
					["healing_taken"] = 14919.015735,
					["fight_component"] = true,
					["end_time"] = 1605059083,
					["targets_absorbs"] = {
						["Cósmós"] = 8579,
					},
					["nome"] = "Chuckleê",
					["spells"] = {
						["_ActorTable"] = {
							[116849] = {
								["c_amt"] = 0,
								["totalabsorb"] = 8579,
								["targets_overheal"] = {
									["Cósmós"] = 1,
								},
								["n_max"] = 5547,
								["targets"] = {
									["Cósmós"] = 8579,
								},
								["n_min"] = 0,
								["counter"] = 4,
								["overheal"] = 1,
								["total"] = 8579,
								["c_max"] = 0,
								["id"] = 116849,
								["targets_absorbs"] = {
									["Cósmós"] = 8579,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 4,
								["n_curado"] = 8579,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[116670] = {
								["c_amt"] = 13,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Weefermadnes"] = 177,
									["Hunterdo"] = 907,
									["Jessicazeal"] = 660,
									["Chuckleê"] = 138,
									["Cósmós"] = 4133,
								},
								["n_max"] = 1247,
								["targets"] = {
									["Weefermadnes"] = 472,
									["Swòrds"] = 8696,
									["Hunterdo"] = 6365,
									["Kruniak"] = 659,
									["Lethumper"] = 1327,
									["Bobaloo"] = 1298,
									["Erincia"] = 1326,
									["Hausstalker"] = 646,
									["Senyth"] = 3924,
									["Mortemitera"] = 1985,
									["Jessicazeal"] = 0,
									["Leglessfatty"] = 672,
									["Chuckleê"] = 527,
									["Cósmós"] = 14106,
								},
								["n_min"] = 0,
								["counter"] = 46,
								["overheal"] = 6015,
								["total"] = 42003,
								["c_max"] = 1870,
								["id"] = 116670,
								["targets_absorbs"] = {
								},
								["c_curado"] = 18467,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 33,
								["n_curado"] = 23536,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[119611] = {
								["c_amt"] = 36,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Weefermadnes"] = 133,
									["Ræquan"] = 266,
									["Hunterdo"] = 318,
									["Leglessfatty"] = 115,
									["Solangè"] = 116,
									["Erincia"] = 265,
									["Hausstalker"] = 133,
									["Caltonse"] = 69,
									["Jessicazeal"] = 133,
									["Swòrds"] = 1041,
									["Chuckleê"] = 133,
									["Cósmós"] = 743,
								},
								["n_max"] = 279,
								["targets"] = {
									["Weefermadnes"] = 1196,
									["Hunterdo"] = 1169,
									["Leglessfatty"] = 151,
									["Friskkilla"] = 189,
									["Erincia"] = 0,
									["Ellylanah"] = 291,
									["Chuckleê"] = 955,
									["Ræquan"] = 133,
									["Lethumper"] = 2083,
									["Kruniak"] = 1063,
									["Ddoczd"] = 370,
									["Solangè"] = 947,
									["Bobaloo"] = 1414,
									["Liléath"] = 790,
									["Hausstalker"] = 1461,
									["Senyth"] = 1264,
									["Mortemitera"] = 1286,
									["Jessicazeal"] = 663,
									["Caltonse"] = 462,
									["Swòrds"] = 1988,
									["Cósmós"] = 2521,
								},
								["n_min"] = 0,
								["counter"] = 136,
								["overheal"] = 3465,
								["total"] = 20396,
								["c_max"] = 372,
								["id"] = 119611,
								["targets_absorbs"] = {
								},
								["c_curado"] = 7867,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 100,
								["n_curado"] = 12529,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[124682] = {
								["c_amt"] = 21,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Cósmós"] = 7926,
									["Swòrds"] = 5361,
									["Hunterdo"] = 2873,
								},
								["n_max"] = 531,
								["targets"] = {
									["Cósmós"] = 14342,
									["Swòrds"] = 2008,
									["Chuckleê"] = 2832,
									["Hunterdo"] = 1252,
								},
								["n_min"] = 0,
								["counter"] = 85,
								["overheal"] = 16160,
								["total"] = 20434,
								["c_max"] = 1062,
								["id"] = 124682,
								["targets_absorbs"] = {
								},
								["c_curado"] = 7212,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 64,
								["n_curado"] = 13222,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[115175] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 325,
								["targets"] = {
									["Cósmós"] = 973,
								},
								["n_min"] = 0,
								["counter"] = 3,
								["overheal"] = 0,
								["total"] = 973,
								["c_max"] = 0,
								["id"] = 115175,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 3,
								["n_curado"] = 973,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[301861] = {
								["c_amt"] = 7,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Onewithbind"] = 175,
									["Marshmallowx"] = 64,
								},
								["n_max"] = 220,
								["targets"] = {
									["Jôêkêr"] = 439,
									["Gnuxlock"] = 439,
									["Erincia"] = 438,
									["Bürgo"] = 219,
									["Allyliia"] = 439,
									["Chuckleê"] = 440,
									["Ellylanah"] = 219,
									["Endell"] = 219,
									["Swòrds"] = 219,
									["Ræquan"] = 219,
									["Marshmallowx"] = 155,
									["Kruniak"] = 219,
									["Caltonse"] = 220,
									["Solangè"] = 219,
									["Mortemitera"] = 658,
									["Liléath"] = 439,
									["Pallore"] = 658,
									["Hausstalker"] = 438,
									["Bobaloo"] = 438,
									["Jessicazeal"] = 220,
									["Taelory"] = 220,
									["Saduz"] = 438,
									["Onewithbind"] = 264,
								},
								["n_min"] = 0,
								["counter"] = 30,
								["overheal"] = 239,
								["total"] = 7876,
								["c_max"] = 439,
								["id"] = 301861,
								["targets_absorbs"] = {
								},
								["c_curado"] = 2897,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 23,
								["n_curado"] = 4979,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[303380] = {
								["c_amt"] = 23,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Liléath"] = 274,
									["Swòrds"] = 3293,
									["Leglessfatty"] = 549,
									["Nilrem"] = 492,
									["Cósmós"] = 39,
								},
								["n_max"] = 412,
								["targets"] = {
									["Liléath"] = 1370,
									["Nilrem"] = 1426,
									["Swòrds"] = 1920,
									["Qüelana"] = 1919,
									["Leglessfatty"] = 2467,
									["Mortemitera"] = 2195,
									["Chuckleê"] = 822,
									["Cósmós"] = 6406,
								},
								["n_min"] = 0,
								["counter"] = 59,
								["overheal"] = 4647,
								["total"] = 18525,
								["c_max"] = 822,
								["id"] = 303380,
								["targets_absorbs"] = {
								},
								["c_curado"] = 10746,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 36,
								["n_curado"] = 7779,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[191894] = {
								["c_amt"] = 11,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Swòrds"] = 819,
									["Cósmós"] = 6489,
								},
								["n_max"] = 869,
								["targets"] = {
									["Cósmós"] = 13779,
									["Swòrds"] = 5993,
									["Chuckleê"] = 2127,
									["Hunterdo"] = 3550,
								},
								["n_min"] = 0,
								["counter"] = 41,
								["overheal"] = 7308,
								["total"] = 25449,
								["c_max"] = 1699,
								["id"] = 191894,
								["targets_absorbs"] = {
								},
								["c_curado"] = 11284,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 30,
								["n_curado"] = 14165,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[272428] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Swòrds"] = 670,
								},
								["n_max"] = 168,
								["targets"] = {
									["Swòrds"] = 0,
									["Hunterdo"] = 336,
									["ßlyss"] = 168,
									["Chuckleê"] = 167,
									["Cósmós"] = 167,
								},
								["n_min"] = 0,
								["counter"] = 8,
								["overheal"] = 670,
								["total"] = 838,
								["c_max"] = 0,
								["id"] = 272428,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 7,
								["n_curado"] = 838,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[191840] = {
								["c_amt"] = 112,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Zartric"] = 71,
									["Ræquan"] = 67,
									["ßlyss"] = 50,
									["Polter"] = 46,
									["Skittlesx"] = 205,
									["Baayy"] = 549,
									["Flup"] = 283,
									["Hausstalker"] = 23,
									["Caltonse"] = 31,
									["Thorddin"] = 74,
									["Ellylanah"] = 204,
									["Allyliia"] = 24,
									["Boriis"] = 47,
									["Sigrynth"] = 163,
								},
								["n_max"] = 386,
								["targets"] = {
									["Onewithbind"] = 738,
									["Gnuxlock"] = 1333,
									["Friskkilla"] = 482,
									["Saíx"] = 714,
									["Erincia"] = 753,
									["Boriis"] = 1029,
									["Thorddin"] = 2946,
									["Ræquan"] = 1208,
									["Polter"] = 0,
									["Liléath"] = 945,
									["Pallore"] = 509,
									["Senyth"] = 424,
									["Mortemitera"] = 1608,
									["Endell"] = 743,
									["Sigrynth"] = 2083,
									["Weefermadnes"] = 1162,
									["Jôêkêr"] = 479,
									["ßlyss"] = 420,
									["Ellylanah"] = 1363,
									["Chuckleê"] = 1099,
									["Lethumper"] = 405,
									["Nilrem"] = 732,
									["Zartric"] = 0,
									["Hausstalker"] = 852,
									["Douginator"] = 443,
									["Kruniak"] = 971,
									["Saduz"] = 441,
									["Skittlesx"] = 984,
									["Allyliia"] = 981,
									["Flup"] = 458,
									["Cósmós"] = 613,
									["Bürgo"] = 1571,
									["Bobaloo"] = 1183,
									["Leglessfatty"] = 73,
									["Taelory"] = 708,
									["Caltonse"] = 400,
									["Baayy"] = 1831,
								},
								["n_min"] = 0,
								["counter"] = 390,
								["overheal"] = 1837,
								["total"] = 32684,
								["c_max"] = 571,
								["id"] = 191840,
								["targets_absorbs"] = {
								},
								["c_curado"] = 13366,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 278,
								["n_curado"] = 19318,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["targets_overheal"] = {
						["Weefermadnes"] = 310,
						["Onewithbind"] = 175,
						["Hunterdo"] = 4098,
						["Leglessfatty"] = 664,
						["Erincia"] = 265,
						["Boriis"] = 47,
						["Thorddin"] = 74,
						["Baayy"] = 549,
						["Ellylanah"] = 204,
						["Chuckleê"] = 271,
						["Cósmós"] = 19331,
						["Nilrem"] = 492,
						["Zartric"] = 71,
						["Swòrds"] = 11184,
						["Allyliia"] = 24,
						["Flup"] = 283,
						["Polter"] = 46,
						["Solangè"] = 116,
						["Marshmallowx"] = 64,
						["Liléath"] = 274,
						["Hausstalker"] = 156,
						["ßlyss"] = 50,
						["Caltonse"] = 100,
						["Jessicazeal"] = 793,
						["Skittlesx"] = 205,
						["Ræquan"] = 333,
						["Sigrynth"] = 163,
					},
					["tipo"] = 2,
					["heal_enemy_amt"] = 0,
					["custom"] = 0,
					["last_event"] = 0,
					["totaldenied"] = 0.015735,
					["start_time"] = 1605058953,
					["delay"] = 0,
					["aID"] = "76-0B1646CC",
				}, -- [2]
				{
					["flag_original"] = 1304,
					["healing_from"] = {
						["ßlyss"] = true,
						["Ræquan"] = true,
						["Chuckleê"] = true,
						["Drcoffee"] = true,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["classe"] = "MONK",
					["totalover"] = 20364.015372,
					["total_without_pet"] = 47335.015372,
					["total"] = 47335.015372,
					["spec"] = 269,
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-08BCF804",
					["totalabsorb"] = 0.015372,
					["last_hps"] = 0,
					["targets"] = {
						["Sigrynth"] = 228,
						["Erincia"] = 631,
						["Ræquan"] = 39861,
						["Taelory"] = 648,
						["Kruniak"] = 218,
						["Pallore"] = 629,
						["Saíx"] = 219,
						["Thorddin"] = 1279,
						["Archimedes"] = 634,
						["Douginator"] = 636,
						["Boriis"] = 214,
						["Mortemitera"] = 475,
						["Hart"] = 558,
						["Mavieñar"] = 227,
						["Endell"] = 650,
						["Cósmós"] = 228,
					},
					["totalover_without_pet"] = 0.015372,
					["healing_taken"] = 42543.015372,
					["fight_component"] = true,
					["end_time"] = 1605059083,
					["targets_absorbs"] = {
					},
					["nome"] = "Ræquan",
					["spells"] = {
						["_ActorTable"] = {
							[116670] = {
								["c_amt"] = 4,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Ræquan"] = 2134,
								},
								["n_max"] = 1320,
								["targets"] = {
									["Ræquan"] = 31195,
								},
								["n_min"] = 0,
								["counter"] = 22,
								["overheal"] = 2134,
								["total"] = 31195,
								["c_max"] = 2628,
								["id"] = 116670,
								["targets_absorbs"] = {
								},
								["c_curado"] = 8058,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 18,
								["n_curado"] = 23137,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[303380] = {
								["c_amt"] = 5,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Ræquan"] = 2542,
								},
								["n_max"] = 318,
								["targets"] = {
									["Ræquan"] = 7242,
								},
								["n_min"] = 0,
								["counter"] = 26,
								["overheal"] = 2542,
								["total"] = 7242,
								["c_max"] = 632,
								["id"] = 303380,
								["targets_absorbs"] = {
								},
								["c_curado"] = 3136,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 21,
								["n_curado"] = 4106,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[322101] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Ræquan"] = 919,
								},
								["n_max"] = 0,
								["targets"] = {
									["Ræquan"] = 1424,
								},
								["n_min"] = 0,
								["counter"] = 2,
								["overheal"] = 919,
								["total"] = 1424,
								["c_max"] = 1424,
								["id"] = 322101,
								["targets_absorbs"] = {
								},
								["c_curado"] = 1424,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 1,
								["n_curado"] = 0,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[130654] = {
								["c_amt"] = 10,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Jôêkêr"] = 232,
									["Leglessfatty"] = 240,
									["Argent Sentry"] = 572,
									["Archimedes"] = 214,
									["Citatìon"] = 448,
									["Cat"] = 971,
									["Loki"] = 1499,
									["Hart"] = 440,
									["Wolf"] = 633,
									["Sebastian"] = 1312,
									["Risen Ghoul <Endell>"] = 211,
									["Onewithbind <Onewithbind>"] = 437,
									["Zartric"] = 233,
									["Ræquan"] = 878,
									["Allyliia"] = 458,
									["Xuen <Onewithbind>"] = 215,
									["Polter"] = 650,
									["Army of the Dead <Mortemitera>"] = 844,
									["Onewithbind"] = 441,
									["Thorddin"] = 232,
									["Hausstalker"] = 871,
									["Infernal <Bürgo>"] = 436,
									["Isi"] = 880,
									["Skoll"] = 550,
									["Taelory"] = 229,
									["Spirit Beast"] = 432,
									["Braincruncher <Mortemitera>"] = 211,
								},
								["n_max"] = 650,
								["targets"] = {
									["Onewithbind"] = 0,
									["Saíx"] = 219,
									["Infernal <Bürgo>"] = 0,
									["Douginator"] = 636,
									["Loki"] = 0,
									["Hart"] = 558,
									["Onewithbind <Onewithbind>"] = 0,
									["Ræquan"] = 0,
									["Polter"] = 0,
									["Hausstalker"] = 0,
									["Isi"] = 0,
									["Endell"] = 650,
									["Sigrynth"] = 228,
									["Jôêkêr"] = 0,
									["Leglessfatty"] = 0,
									["Argent Sentry"] = 0,
									["Archimedes"] = 634,
									["Cat"] = 0,
									["Cósmós"] = 228,
									["Allyliia"] = 0,
									["Pallore"] = 629,
									["Mavieñar"] = 227,
									["Sebastian"] = 0,
									["Zartric"] = 0,
									["Mortemitera"] = 475,
									["Wolf"] = 0,
									["Xuen <Onewithbind>"] = 0,
									["Thorddin"] = 1279,
									["Army of the Dead <Mortemitera>"] = 0,
									["Erincia"] = 631,
									["Risen Ghoul <Endell>"] = 0,
									["Citatìon"] = 0,
									["Kruniak"] = 218,
									["Boriis"] = 214,
									["Skoll"] = 0,
									["Taelory"] = 648,
									["Spirit Beast"] = 0,
									["Braincruncher <Mortemitera>"] = 0,
								},
								["n_min"] = 0,
								["counter"] = 52,
								["overheal"] = 14769,
								["total"] = 7474,
								["c_max"] = 1279,
								["id"] = 130654,
								["targets_absorbs"] = {
								},
								["c_curado"] = 1754,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 42,
								["n_curado"] = 5720,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["targets_overheal"] = {
						["Jôêkêr"] = 232,
						["Leglessfatty"] = 240,
						["Argent Sentry"] = 572,
						["Archimedes"] = 214,
						["Citatìon"] = 448,
						["Cat"] = 971,
						["Loki"] = 1499,
						["Hart"] = 440,
						["Wolf"] = 633,
						["Sebastian"] = 1312,
						["Risen Ghoul <Endell>"] = 211,
						["Onewithbind <Onewithbind>"] = 437,
						["Zartric"] = 233,
						["Ræquan"] = 6473,
						["Allyliia"] = 458,
						["Xuen <Onewithbind>"] = 215,
						["Polter"] = 650,
						["Army of the Dead <Mortemitera>"] = 844,
						["Onewithbind"] = 441,
						["Thorddin"] = 232,
						["Hausstalker"] = 871,
						["Infernal <Bürgo>"] = 436,
						["Isi"] = 880,
						["Skoll"] = 550,
						["Taelory"] = 229,
						["Spirit Beast"] = 432,
						["Braincruncher <Mortemitera>"] = 211,
					},
					["tipo"] = 2,
					["heal_enemy_amt"] = 0,
					["custom"] = 0,
					["last_event"] = 0,
					["totaldenied"] = 0.015372,
					["start_time"] = 1605058979,
					["delay"] = 0,
					["aID"] = "76-08BCF804",
				}, -- [3]
				{
					["flag_original"] = 1304,
					["healing_from"] = {
						["ßlyss"] = true,
						["Chuckleê"] = true,
						["Lethumper"] = true,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["aID"] = "76-0A67610B",
					["totalover"] = 40506.011368,
					["total_without_pet"] = 107265.011368,
					["total"] = 107265.011368,
					["spec"] = 257,
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-0A67610B",
					["totalabsorb"] = 2179.011368,
					["last_hps"] = 0,
					["targets"] = {
						["Jôêkêr"] = 4047,
						["Esleeze"] = 1683,
						["ßlyss"] = 15846,
						["Friskkilla"] = 3212,
						["Saíx"] = 3266,
						["Erincia"] = 4479,
						["Boriis"] = 6085,
						["Thorddin"] = 639,
						["Sigrynth"] = 6164,
						["Mavieñar"] = 363,
						["Chuckleê"] = 3075,
						["Douginator"] = 4440,
						["Nilrem"] = 7022,
						["Zartric"] = 2100,
						["Ræquan"] = 1005,
						["Qüelana"] = 599,
						["Kruniak"] = 10234,
						["Citatìon"] = 622,
						["Swòrds"] = 1844,
						["Pallore"] = 1885,
						["Liléath"] = 4941,
						["Hausstalker"] = 4885,
						["Senyth"] = 7287,
						["Mortemitera"] = 121,
						["Onewithbind"] = 1304,
						["Taelory"] = 1909,
						["Endell"] = 5998,
						["Cósmós"] = 2210,
					},
					["totalover_without_pet"] = 0.011368,
					["healing_taken"] = 19309.011368,
					["fight_component"] = true,
					["end_time"] = 1605059083,
					["targets_overheal"] = {
						["Jôêkêr"] = 121,
						["ßlyss"] = 8104,
						["Saíx"] = 805,
						["Erincia"] = 1001,
						["Citatìon"] = 3563,
						["Thorddin"] = 1490,
						["Chuckleê"] = 3396,
						["Nilrem"] = 1248,
						["Zartric"] = 3382,
						["Swòrds"] = 2374,
						["Legadon"] = 79,
						["Kruniak"] = 2094,
						["Polter"] = 2623,
						["Skittlesx"] = 854,
						["Himesey"] = 2992,
						["Hausstalker"] = 550,
						["Onewithbind"] = 828,
						["Ræquan"] = 2279,
						["Douginator"] = 283,
						["Taelory"] = 242,
						["Boriis"] = 68,
						["Sigrynth"] = 2130,
					},
					["nome"] = "ßlyss",
					["spells"] = {
						["_ActorTable"] = {
							[272260] = {
								["c_amt"] = 8,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Hausstalker"] = 220,
								},
								["n_max"] = 503,
								["targets"] = {
									["Liléath"] = 989,
									["Boriis"] = 1084,
									["Ræquan"] = 1005,
									["Hausstalker"] = 880,
									["Erincia"] = 1178,
								},
								["n_min"] = 0,
								["counter"] = 25,
								["overheal"] = 220,
								["total"] = 5136,
								["c_max"] = 1005,
								["id"] = 272260,
								["targets_absorbs"] = {
								},
								["c_curado"] = 1569,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 17,
								["n_curado"] = 3567,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[77489] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Onewithbind"] = 198,
									["ßlyss"] = 1098,
									["Erincia"] = 168,
									["Boriis"] = 68,
									["Thorddin"] = 395,
									["Chuckleê"] = 778,
									["Nilrem"] = 630,
									["Zartric"] = 1205,
									["Ræquan"] = 423,
									["Legadon"] = 79,
									["Kruniak"] = 138,
									["Polter"] = 200,
									["Skittlesx"] = 136,
									["Hausstalker"] = 330,
									["Himesey"] = 555,
									["Swòrds"] = 553,
									["Douginator"] = 283,
									["Citatìon"] = 1083,
									["Sigrynth"] = 382,
								},
								["n_max"] = 414,
								["targets"] = {
									["Jôêkêr"] = 591,
									["ßlyss"] = 2381,
									["Friskkilla"] = 596,
									["Saíx"] = 710,
									["Erincia"] = 927,
									["Citatìon"] = 0,
									["Thorddin"] = 0,
									["Sigrynth"] = 976,
									["Boriis"] = 872,
									["Chuckleê"] = 421,
									["Douginator"] = 594,
									["Nilrem"] = 979,
									["Zartric"] = 0,
									["Ræquan"] = 0,
									["Legadon"] = 0,
									["Kruniak"] = 2372,
									["Polter"] = 0,
									["Skittlesx"] = 0,
									["Hausstalker"] = 587,
									["Liléath"] = 732,
									["Pallore"] = 193,
									["Swòrds"] = 230,
									["Himesey"] = 0,
									["Onewithbind"] = 198,
									["Taelory"] = 195,
									["Endell"] = 957,
									["Cósmós"] = 387,
								},
								["n_min"] = 0,
								["counter"] = 154,
								["overheal"] = 8702,
								["total"] = 14898,
								["c_max"] = 0,
								["id"] = 77489,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 154,
								["n_curado"] = 14898,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[34861] = {
								["c_amt"] = 3,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Onewithbind"] = 630,
									["Zartric"] = 383,
									["Saíx"] = 805,
									["Thorddin"] = 1095,
									["ßlyss"] = 2780,
									["Kruniak"] = 1345,
									["Chuckleê"] = 822,
									["Sigrynth"] = 1748,
								},
								["n_max"] = 1738,
								["targets"] = {
									["Sigrynth"] = 5067,
									["Jôêkêr"] = 3456,
									["Esleeze"] = 1683,
									["Endell"] = 5041,
									["ßlyss"] = 637,
									["Kruniak"] = 5422,
									["Saíx"] = 905,
									["Hausstalker"] = 3418,
									["Pallore"] = 1692,
									["Boriis"] = 1698,
									["Zartric"] = 1294,
									["Thorddin"] = 639,
									["Onewithbind"] = 1106,
									["Taelory"] = 1714,
									["Chuckleê"] = 837,
									["Cósmós"] = 1701,
								},
								["n_min"] = 0,
								["counter"] = 24,
								["overheal"] = 9608,
								["total"] = 36310,
								["c_max"] = 3363,
								["id"] = 34861,
								["targets_absorbs"] = {
								},
								["c_curado"] = 7623,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 21,
								["n_curado"] = 28687,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[297375] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 1215,
								["targets"] = {
									["Senyth"] = 7287,
								},
								["n_min"] = 0,
								["counter"] = 6,
								["overheal"] = 0,
								["total"] = 7287,
								["c_max"] = 0,
								["id"] = 297375,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 6,
								["n_curado"] = 7287,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[17] = {
								["c_amt"] = 0,
								["totalabsorb"] = 2179,
								["targets_overheal"] = {
									["ßlyss"] = 1,
								},
								["n_max"] = 2179,
								["targets"] = {
									["ßlyss"] = 2179,
								},
								["n_min"] = 0,
								["counter"] = 2,
								["overheal"] = 1,
								["total"] = 2179,
								["c_max"] = 0,
								["id"] = 17,
								["targets_absorbs"] = {
									["ßlyss"] = 2179,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 2,
								["n_curado"] = 2179,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[270661] = {
								["c_amt"] = 2,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 103,
								["targets"] = {
									["ßlyss"] = 1649,
								},
								["n_min"] = 0,
								["counter"] = 14,
								["overheal"] = 0,
								["total"] = 1649,
								["c_max"] = 207,
								["id"] = 270661,
								["targets_absorbs"] = {
								},
								["c_curado"] = 413,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 12,
								["n_curado"] = 1236,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[297935] = {
								["c_amt"] = 2,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Taelory"] = 242,
									["Skittlesx"] = 121,
									["Jôêkêr"] = 121,
								},
								["n_max"] = 122,
								["targets"] = {
									["Jôêkêr"] = 0,
									["ßlyss"] = 243,
									["Saíx"] = 242,
									["Cósmós"] = 122,
									["Mortemitera"] = 121,
									["Skittlesx"] = 0,
									["Mavieñar"] = 363,
									["Taelory"] = 0,
									["Sigrynth"] = 121,
								},
								["n_min"] = 0,
								["counter"] = 12,
								["overheal"] = 484,
								["total"] = 1212,
								["c_max"] = 242,
								["id"] = 297935,
								["targets_absorbs"] = {
								},
								["c_curado"] = 484,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 10,
								["n_curado"] = 728,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[143924] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 92,
								["targets"] = {
									["ßlyss"] = 1180,
								},
								["n_min"] = 0,
								["counter"] = 33,
								["overheal"] = 0,
								["total"] = 1180,
								["c_max"] = 0,
								["id"] = 143924,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 33,
								["n_curado"] = 1180,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[298318] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
								},
								["n_max"] = 0,
								["targets"] = {
									["ßlyss"] = 1917,
								},
								["n_min"] = 0,
								["counter"] = 1,
								["overheal"] = 0,
								["total"] = 1917,
								["c_max"] = 1917,
								["id"] = 298318,
								["targets_absorbs"] = {
								},
								["c_curado"] = 1917,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 0,
								["n_curado"] = 0,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
							[596] = {
								["c_amt"] = 8,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Nilrem"] = 618,
									["Zartric"] = 1794,
									["Swòrds"] = 1821,
									["Kruniak"] = 611,
									["Polter"] = 2423,
									["Skittlesx"] = 597,
									["Erincia"] = 833,
									["Citatìon"] = 2480,
									["Ræquan"] = 1856,
									["ßlyss"] = 4225,
									["Chuckleê"] = 1796,
									["Himesey"] = 2437,
								},
								["n_max"] = 817,
								["targets"] = {
									["ßlyss"] = 5660,
									["Friskkilla"] = 2616,
									["Saíx"] = 1409,
									["Erincia"] = 2374,
									["Douginator"] = 3846,
									["Chuckleê"] = 1817,
									["Nilrem"] = 6043,
									["Zartric"] = 806,
									["Ræquan"] = 0,
									["Qüelana"] = 599,
									["Kruniak"] = 2440,
									["Polter"] = 0,
									["Skittlesx"] = 0,
									["Liléath"] = 3220,
									["Boriis"] = 2431,
									["Swòrds"] = 1614,
									["Citatìon"] = 622,
									["Himesey"] = 0,
								},
								["n_min"] = 0,
								["counter"] = 80,
								["overheal"] = 21491,
								["total"] = 35497,
								["c_max"] = 1636,
								["id"] = 596,
								["targets_absorbs"] = {
								},
								["c_curado"] = 6897,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 72,
								["n_curado"] = 28600,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["last_event"] = 0,
					["heal_enemy_amt"] = 0,
					["totaldenied"] = 0.011368,
					["custom"] = 0,
					["tipo"] = 2,
					["classe"] = "PRIEST",
					["start_time"] = 1605059013,
					["delay"] = 0,
					["targets_absorbs"] = {
						["ßlyss"] = 2179,
					},
				}, -- [4]
				{
					["flag_original"] = 4369,
					["targets_overheal"] = {
						["Altered Ghoul <Thorddin>"] = 0,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["aID"] = "",
					["totalover"] = 5087.02376,
					["total_without_pet"] = 30683.02376,
					["total"] = 30683.02376,
					["targets_absorbs"] = {
					},
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Vehicle-0-3138-571-32512-172939-00002B45F8",
					["totalabsorb"] = 0.02376,
					["last_hps"] = 0,
					["targets"] = {
						["Altered Ghoul <Thorddin>"] = 0,
					},
					["totalover_without_pet"] = 0.02376,
					["healing_taken"] = 30683.02376,
					["fight_component"] = true,
					["end_time"] = 1605060167,
					["ownerName"] = "Thorddin",
					["nome"] = "Altered Ghoul <Thorddin>",
					["spells"] = {
						["_ActorTable"] = {
							[337943] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Altered Ghoul <Thorddin>"] = 5087,
								},
								["n_max"] = 5419,
								["targets"] = {
									["Altered Ghoul <Thorddin>"] = 30683,
								},
								["n_min"] = 0,
								["counter"] = 15,
								["overheal"] = 5087,
								["total"] = 30683,
								["c_max"] = 0,
								["id"] = 337943,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["totaldenied"] = 0,
								["m_amt"] = 0,
								["m_healed"] = 0,
								["n_amt"] = 15,
								["n_curado"] = 30683,
								["c_min"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["classe"] = "PET",
					["healing_from"] = {
						["Altered Ghoul <Thorddin>"] = true,
					},
					["tipo"] = 2,
					["custom"] = 0,
					["last_event"] = 0,
					["totaldenied"] = 0.02376,
					["start_time"] = 1605060136,
					["delay"] = 0,
					["heal_enemy_amt"] = 0,
				}, -- [5]
				{
					["flag_original"] = 1304,
					["totalabsorb"] = 0.036316,
					["last_hps"] = 0,
					["targets_overheal"] = {
						["Poonkin"] = 0,
						["Tyraan"] = 0,
						["Thorddin"] = 668,
						["Juk'vhug <Tyraan>"] = 246,
						["Relynn"] = 0,
					},
					["targets"] = {
						["Relynn"] = 0,
						["Tyraan"] = 0,
						["Thorddin"] = 0,
					},
					["serial"] = "Player-76-0A566962",
					["pets"] = {
					},
					["totalover_without_pet"] = 0.036316,
					["healing_from"] = {
						["Relynn"] = true,
						["Vaethiran"] = true,
					},
					["heal_enemy_amt"] = 0,
					["totalover"] = 5690.036315999999,
					["total_without_pet"] = 6595.036315999999,
					["iniciar_hps"] = false,
					["start_time"] = 1605145079,
					["fight_component"] = true,
					["total"] = 6595.036315999999,
					["classe"] = "PRIEST",
					["aID"] = "76-0A566962",
					["nome"] = "Relynn",
					["spells"] = {
						["_ActorTable"] = {
							[281265] = {
								["c_amt"] = 1,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Relynn"] = 817,
									["Tyraan"] = 753,
									["Poonkin"] = 732,
									["Thorddin"] = 592,
								},
								["n_max"] = 203,
								["targets"] = {
									["Relynn"] = 866,
									["Tyraan"] = 122,
									["Poonkin"] = 0,
									["Thorddin"] = 882,
								},
								["n_min"] = 0,
								["counter"] = 25,
								["overheal"] = 2894,
								["total"] = 1870,
								["c_max"] = 343,
								["id"] = 281265,
								["targets_absorbs"] = {
								},
								["c_curado"] = 343,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 1527,
								["n_amt"] = 24,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[143924] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Relynn"] = 2,
								},
								["n_max"] = 21,
								["targets"] = {
									["Relynn"] = 239,
								},
								["n_min"] = 0,
								["counter"] = 27,
								["overheal"] = 2,
								["total"] = 239,
								["c_max"] = 0,
								["id"] = 143924,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 239,
								["n_amt"] = 27,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[298318] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Relynn"] = 1012,
								},
								["n_max"] = 0,
								["targets"] = {
									["Relynn"] = 0,
								},
								["n_min"] = 0,
								["counter"] = 1,
								["overheal"] = 1012,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 298318,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 0,
								["n_amt"] = 1,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[270117] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Relynn"] = 501,
								},
								["n_max"] = 511,
								["targets"] = {
									["Relynn"] = 3492,
								},
								["n_min"] = 0,
								["counter"] = 8,
								["overheal"] = 501,
								["total"] = 3492,
								["c_max"] = 0,
								["id"] = 270117,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 3492,
								["n_amt"] = 8,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[77489] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Poonkin"] = 65,
									["Tyraan"] = 308,
									["Thorddin"] = 76,
									["Juk'vhug <Tyraan>"] = 246,
									["Relynn"] = 586,
								},
								["n_max"] = 126,
								["targets"] = {
									["Poonkin"] = 0,
									["Tyraan"] = 0,
									["Thorddin"] = 60,
									["Juk'vhug <Tyraan>"] = 0,
									["Relynn"] = 934,
								},
								["n_min"] = 0,
								["counter"] = 20,
								["overheal"] = 1281,
								["total"] = 994,
								["c_max"] = 0,
								["id"] = 77489,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 994,
								["n_amt"] = 20,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["end_time"] = 1605145139,
					["healing_taken"] = 5883.036316,
					["heal_enemy"] = {
					},
					["targets_absorbs"] = {
					},
					["custom"] = 0,
					["tipo"] = 2,
					["on_hold"] = false,
					["totaldenied"] = 0.036316,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [6]
				{
					["flag_original"] = 1304,
					["targets_overheal"] = {
						["Roahn"] = 0,
						["Quicketl"] = 0,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["heal_enemy_amt"] = 0,
					["totalover"] = 42.009999,
					["total_without_pet"] = 3759.009999,
					["total"] = 3759.009999,
					["spec"] = 66,
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-0A6A85DC",
					["totalabsorb"] = 562.009999,
					["last_hps"] = 0,
					["targets"] = {
						["Ayeumi"] = 0,
						["Madox"] = 0,
						["Kendall"] = 0,
						["Executie"] = 0,
						["Dhurmadin"] = 0,
						["Voìdrunner"] = 0,
						["Bishopofrome"] = 0,
						["Roahn"] = 0,
						["Gardëk"] = 0,
						["Sarbah"] = 0,
						["Galaysia"] = 0,
						["Thorddin"] = 0,
						["Pyfitl"] = 0,
						["Torvas"] = 0,
						["Koják"] = 0,
						["Úther"] = 0,
						["Saltybro"] = 0,
						["Meitian"] = 0,
						["Mctreats"] = 0,
						["Ghostree"] = 0,
						["Morrer"] = 0,
						["Vygo"] = 0,
						["Eunbin"] = 0,
						["Ditronus"] = 0,
						["Zalyrra"] = 0,
						["Quicketl"] = 0,
						["Valancia"] = 0,
						["Vellitus"] = 0,
						["Memourial"] = 0,
						["Kegshots"] = 0,
						["Tråphouse"] = 0,
						["Vyndicktator"] = 0,
					},
					["totalover_without_pet"] = 0.009999,
					["healing_taken"] = 562.009999,
					["fight_component"] = true,
					["end_time"] = 1605145418,
					["healing_from"] = {
						["Gardëk"] = true,
					},
					["nome"] = "Gardëk",
					["spells"] = {
						["_ActorTable"] = {
							[183811] = {
								["c_amt"] = 15,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Roahn"] = 36,
									["Quicketl"] = 6,
								},
								["n_max"] = 37,
								["targets"] = {
									["Ayeumi"] = 35,
									["Madox"] = 145,
									["Kendall"] = 144,
									["Dhurmadin"] = 143,
									["Úther"] = 146,
									["Bishopofrome"] = 36,
									["Meitian"] = 36,
									["Ghostree"] = 36,
									["Kegshots"] = 217,
									["Galaysia"] = 73,
									["Thorddin"] = 72,
									["Morrer"] = 36,
									["Torvas"] = 73,
									["Koják"] = 216,
									["Ditronus"] = 71,
									["Saltybro"] = 144,
									["Voìdrunner"] = 37,
									["Mctreats"] = 73,
									["Roahn"] = 214,
									["Executie"] = 322,
									["Eunbin"] = 106,
									["Pyfitl"] = 107,
									["Vygo"] = 144,
									["Zalyrra"] = 183,
									["Quicketl"] = 29,
									["Valancia"] = 36,
									["Vellitus"] = 36,
									["Memourial"] = 72,
									["Sarbah"] = 36,
									["Tråphouse"] = 108,
									["Vyndicktator"] = 71,
								},
								["n_min"] = 0,
								["counter"] = 75,
								["overheal"] = 42,
								["total"] = 3197,
								["c_max"] = 74,
								["id"] = 183811,
								["targets_absorbs"] = {
								},
								["c_curado"] = 1083,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 2114,
								["n_amt"] = 60,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[229976] = {
								["c_amt"] = 0,
								["totalabsorb"] = 116,
								["targets_overheal"] = {
								},
								["n_max"] = 116,
								["targets"] = {
									["Gardëk"] = 116,
								},
								["n_min"] = 0,
								["counter"] = 1,
								["overheal"] = 0,
								["total"] = 116,
								["c_max"] = 0,
								["id"] = 229976,
								["targets_absorbs"] = {
									["Gardëk"] = 116,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 116,
								["n_amt"] = 1,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[272979] = {
								["c_amt"] = 0,
								["totalabsorb"] = 446,
								["targets_overheal"] = {
								},
								["n_max"] = 446,
								["targets"] = {
									["Gardëk"] = 446,
								},
								["n_min"] = 0,
								["counter"] = 1,
								["overheal"] = 0,
								["total"] = 446,
								["c_max"] = 0,
								["id"] = 272979,
								["targets_absorbs"] = {
									["Gardëk"] = 446,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 446,
								["n_amt"] = 1,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["targets_absorbs"] = {
						["Gardëk"] = 0,
					},
					["classe"] = "PALADIN",
					["start_time"] = 1605145408,
					["custom"] = 0,
					["last_event"] = 0,
					["aID"] = "76-0A6A85DC",
					["totaldenied"] = 0.009999,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [7]
				{
					["flag_original"] = 1304,
					["targets_overheal"] = {
						["Vaethiran"] = 0,
						["Darathas"] = 0,
						["Arllyn"] = 0,
						["Kÿa-Proudmoore"] = 0,
						["Vyndicktator"] = 0,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["heal_enemy_amt"] = 0,
					["totalover"] = 3237.007501,
					["total_without_pet"] = 12219.007501,
					["total"] = 12219.007501,
					["spec"] = 270,
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-05DA65D9",
					["totalabsorb"] = 4140.007501,
					["last_hps"] = 0,
					["targets"] = {
						["Ayeumi"] = 0,
						["Douglinhas"] = 0,
						["Dhurmadin"] = 0,
						["Dominil"] = 0,
						["Kÿa-Proudmoore"] = 0,
						["Thorddin"] = 0,
						["Mnemae"] = 0,
						["Ocissor"] = 0,
						["Koják"] = 0,
						["Úther"] = 0,
						["Vaethiran"] = 0,
						["Vyndicktator"] = 0,
						["Eunbin"] = 0,
						["Relynn"] = 0,
						["Darathas"] = 0,
						["Arllyn"] = 0,
						["Sneakyball"] = 0,
						["Torvas"] = 0,
						["Phfoxy"] = 0,
						["Tråphouse"] = 0,
						["Valancia"] = 0,
					},
					["totalover_without_pet"] = 0.007501000000000001,
					["healing_taken"] = 4140.007501,
					["fight_component"] = true,
					["end_time"] = 1605145418,
					["healing_from"] = {
						["Vaethiran"] = true,
					},
					["nome"] = "Vaethiran",
					["spells"] = {
						["_ActorTable"] = {
							[116849] = {
								["c_amt"] = 0,
								["totalabsorb"] = 4140,
								["targets_overheal"] = {
									["Vaethiran"] = 2783,
								},
								["n_max"] = 1429,
								["targets"] = {
									["Vaethiran"] = 4140,
								},
								["n_min"] = 0,
								["counter"] = 6,
								["overheal"] = 2783,
								["total"] = 4140,
								["c_max"] = 0,
								["id"] = 116849,
								["targets_absorbs"] = {
									["Vaethiran"] = 4140,
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 4140,
								["n_amt"] = 6,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
							[191840] = {
								["c_amt"] = 12,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Kÿa-Proudmoore"] = 204,
									["Vyndicktator"] = 88,
									["Arllyn"] = 101,
									["Darathas"] = 61,
								},
								["n_max"] = 272,
								["targets"] = {
									["Ayeumi"] = 592,
									["Douglinhas"] = 653,
									["Dhurmadin"] = 360,
									["Dominil"] = 376,
									["Kÿa-Proudmoore"] = 177,
									["Thorddin"] = 672,
									["Mnemae"] = 348,
									["Ocissor"] = 381,
									["Koják"] = 314,
									["Úther"] = 304,
									["Vyndicktator"] = 551,
									["Eunbin"] = 419,
									["Relynn"] = 352,
									["Torvas"] = 356,
									["Arllyn"] = 236,
									["Tråphouse"] = 374,
									["Phfoxy"] = 329,
									["Darathas"] = 337,
									["Sneakyball"] = 620,
									["Valancia"] = 328,
								},
								["n_min"] = 0,
								["counter"] = 98,
								["overheal"] = 454,
								["total"] = 8079,
								["c_max"] = 543,
								["id"] = 191840,
								["targets_absorbs"] = {
								},
								["c_curado"] = 2394,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["n_curado"] = 5685,
								["n_amt"] = 86,
								["m_healed"] = 0,
								["totaldenied"] = 0,
								["absorbed"] = 0,
							},
						},
						["tipo"] = 3,
					},
					["targets_absorbs"] = {
						["Vaethiran"] = 0,
					},
					["classe"] = "MONK",
					["start_time"] = 1605145400,
					["custom"] = 0,
					["last_event"] = 0,
					["aID"] = "76-05DA65D9",
					["totaldenied"] = 0.007501000000000001,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [8]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 0.5849220000000003,
					["resource"] = 1842.532496,
					["targets"] = {
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["aID"] = "76-057FA942",
					["passiveover"] = 0.00167,
					["fight_component"] = true,
					["resource_type"] = 9,
					["totalover"] = 0.00167,
					["nome"] = "Thorddin",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 7,
					},
					["grupo"] = true,
					["classe"] = "PALADIN",
					["alternatepower"] = 0.5849220000000003,
					["tipo"] = 3,
					["last_event"] = 0,
					["flag_original"] = 1297,
					["spec"] = 70,
					["serial"] = "Player-76-057FA942",
					["total"] = 0.5849220000000003,
				}, -- [1]
				{
					["received"] = 0.007053,
					["resource"] = 20.007053,
					["targets"] = {
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["aID"] = "76-08BCF804",
					["passiveover"] = 0.001346,
					["fight_component"] = true,
					["resource_type"] = 12,
					["nome"] = "Ræquan",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 7,
					},
					["totalover"] = 0.001346,
					["classe"] = "MONK",
					["alternatepower"] = 0.007053,
					["tipo"] = 3,
					["last_event"] = 0,
					["flag_original"] = 1304,
					["spec"] = 269,
					["serial"] = "Player-76-08BCF804",
					["total"] = 0.007053,
				}, -- [2]
				{
					["received"] = 50.010952,
					["resource"] = 8.010952,
					["targets"] = {
						["ßlyss"] = 50,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["aID"] = "76-0A67610B",
					["passiveover"] = 0.001323,
					["fight_component"] = true,
					["resource_type"] = 13,
					["nome"] = "ßlyss",
					["spells"] = {
						["_ActorTable"] = {
							[298324] = {
								["total"] = 50,
								["id"] = 298324,
								["totalover"] = 0,
								["targets"] = {
									["ßlyss"] = 50,
								},
								["counter"] = 1,
							},
						},
						["tipo"] = 7,
					},
					["totalover"] = 0.001323,
					["classe"] = "PRIEST",
					["alternatepower"] = 0.010952,
					["tipo"] = 3,
					["last_event"] = 0,
					["flag_original"] = 1304,
					["spec"] = 257,
					["serial"] = "Player-76-0A67610B",
					["total"] = 50.010952,
				}, -- [3]
				{
					["received"] = 0.026615,
					["resource"] = 60.061827,
					["targets"] = {
						["Relynn"] = 0,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["aID"] = "76-0A566962",
					["passiveover"] = 0.002168,
					["fight_component"] = true,
					["resource_type"] = 13,
					["nome"] = "Relynn",
					["spells"] = {
						["_ActorTable"] = {
							[298324] = {
								["total"] = 0,
								["id"] = 298324,
								["totalover"] = 0,
								["targets"] = {
									["Relynn"] = 0,
								},
								["counter"] = 1,
							},
						},
						["tipo"] = 7,
					},
					["total"] = 0.026615,
					["classe"] = "PRIEST",
					["flag_original"] = 1304,
					["last_event"] = 0,
					["alternatepower"] = 0.026615,
					["tipo"] = 3,
					["serial"] = "Player-76-0A566962",
					["totalover"] = 0.002168,
				}, -- [4]
				{
					["received"] = 0.011155,
					["resource"] = 35.030759,
					["targets"] = {
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["aID"] = "76-0A6A85DC",
					["passiveover"] = 0.006254,
					["fight_component"] = true,
					["resource_type"] = 9,
					["nome"] = "Gardëk",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 7,
					},
					["total"] = 0.011155,
					["classe"] = "PALADIN",
					["spec"] = 66,
					["flag_original"] = 1304,
					["last_event"] = 0,
					["tipo"] = 3,
					["alternatepower"] = 0.011155,
					["serial"] = "Player-76-0A6A85DC",
					["totalover"] = 0.006254,
				}, -- [5]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["debuff_uptime_spells"] = {
						["_ActorTable"] = {
							[255937] = {
								["counter"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 58,
								["id"] = 255937,
								["uptime"] = 99,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[255941] = {
								["counter"] = 0,
								["appliedamt"] = 42,
								["activedamt"] = -17,
								["actived_at"] = 9630976162,
								["id"] = 255941,
								["uptime"] = 54,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[25771] = {
								["counter"] = 0,
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["uptime"] = 10,
								["id"] = 25771,
								["appliedamt"] = 1,
								["targets"] = {
								},
								["actived_at"] = 1605060576,
							},
							[273481] = {
								["counter"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 24,
								["id"] = 273481,
								["uptime"] = 96,
								["targets"] = {
								},
								["refreshamt"] = 1,
							},
							[853] = {
								["refreshamt"] = 0,
								["counter"] = 0,
								["activedamt"] = -1,
								["uptime"] = 7,
								["id"] = 853,
								["appliedamt"] = 2,
								["targets"] = {
								},
								["actived_at"] = 1605235226,
							},
							[303438] = {
								["counter"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 6,
								["id"] = 303438,
								["uptime"] = 160,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[295367] = {
								["counter"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 68,
								["id"] = 295367,
								["uptime"] = 362,
								["targets"] = {
								},
								["refreshamt"] = 19,
							},
							[197277] = {
								["counter"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 125,
								["id"] = 197277,
								["uptime"] = 309,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[20066] = {
								["counter"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 20066,
								["uptime"] = 4,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["cooldowns_defensive"] = 4.033898,
					["pets"] = {
						"Altered Ghoul <Thorddin>", -- [1]
					},
					["cc_done_spells"] = {
						["_ActorTable"] = {
							[853] = {
								["id"] = 853,
								["targets"] = {
									["Vrykul Necrolord"] = 1,
									["Darksworn Sentry"] = 1,
								},
								["counter"] = 2,
							},
							[20066] = {
								["id"] = 20066,
								["targets"] = {
									["Chillbone Gnawer"] = 1,
								},
								["counter"] = 1,
							},
						},
						["tipo"] = 9,
					},
					["aID"] = "76-057FA942",
					["cooldowns_defensive_targets"] = {
						["Thorddin"] = 4,
					},
					["buff_uptime_spells"] = {
						["_ActorTable"] = {
							[267611] = {
								["counter"] = 0,
								["activedamt"] = 130,
								["appliedamt"] = 130,
								["id"] = 267611,
								["uptime"] = 655,
								["targets"] = {
								},
								["refreshamt"] = 82,
							},
							[268534] = {
								["counter"] = 0,
								["activedamt"] = 50,
								["appliedamt"] = 50,
								["id"] = 268534,
								["uptime"] = 312,
								["targets"] = {
								},
								["refreshamt"] = 9,
							},
							[114250] = {
								["counter"] = 0,
								["activedamt"] = 92,
								["appliedamt"] = 92,
								["id"] = 114250,
								["uptime"] = 786,
								["targets"] = {
								},
								["refreshamt"] = 102,
							},
							[223819] = {
								["counter"] = 0,
								["activedamt"] = 31,
								["appliedamt"] = 31,
								["id"] = 223819,
								["uptime"] = 49,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[280192] = {
								["counter"] = 0,
								["refreshamt"] = 0,
								["activedamt"] = 5,
								["uptime"] = 2,
								["id"] = 280192,
								["appliedamt"] = 5,
								["targets"] = {
								},
								["actived_at"] = 6420680030,
							},
							[231895] = {
								["counter"] = 0,
								["activedamt"] = 7,
								["appliedamt"] = 7,
								["id"] = 231895,
								["uptime"] = 122,
								["targets"] = {
								},
								["refreshamt"] = 18,
							},
							[465] = {
								["refreshamt"] = 0,
								["activedamt"] = 74,
								["appliedamt"] = 74,
								["id"] = 465,
								["uptime"] = 624,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[231843] = {
								["actived_at"] = 24078636239,
								["counter"] = 0,
								["activedamt"] = 69,
								["appliedamt"] = 69,
								["id"] = 231843,
								["uptime"] = 123,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[286393] = {
								["counter"] = 0,
								["activedamt"] = 75,
								["appliedamt"] = 75,
								["id"] = 286393,
								["uptime"] = 299,
								["targets"] = {
								},
								["refreshamt"] = 9,
							},
							[186403] = {
								["counter"] = 0,
								["activedamt"] = 132,
								["appliedamt"] = 132,
								["id"] = 186403,
								["uptime"] = 1497,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[337931] = {
								["counter"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 0,
								["id"] = 337931,
								["uptime"] = 0,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[281178] = {
								["counter"] = 0,
								["activedamt"] = 88,
								["appliedamt"] = 88,
								["id"] = 281178,
								["uptime"] = 252,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[64373] = {
								["counter"] = 0,
								["activedamt"] = 80,
								["appliedamt"] = 80,
								["id"] = 64373,
								["uptime"] = 620,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[221883] = {
								["counter"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 221883,
								["uptime"] = 8,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[642] = {
								["counter"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 642,
								["uptime"] = 8,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[273942] = {
								["refreshamt"] = 0,
								["activedamt"] = 10,
								["appliedamt"] = 10,
								["id"] = 273942,
								["uptime"] = 60,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[184662] = {
								["counter"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 184662,
								["uptime"] = 33,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[231435] = {
								["refreshamt"] = 0,
								["appliedamt"] = 5,
								["activedamt"] = 5,
								["uptime"] = 18,
								["id"] = 231435,
								["actived_at"] = 4815662800,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[303390] = {
								["counter"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 303390,
								["uptime"] = 35,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[295378] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 295378,
								["uptime"] = 8,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[269279] = {
								["counter"] = 0,
								["activedamt"] = 138,
								["appliedamt"] = 138,
								["id"] = 269279,
								["uptime"] = 817,
								["targets"] = {
								},
								["refreshamt"] = 11,
							},
							[280204] = {
								["counter"] = 0,
								["activedamt"] = 34,
								["appliedamt"] = 34,
								["id"] = 280204,
								["uptime"] = 208,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[337912] = {
								["counter"] = 0,
								["refreshamt"] = 0,
								["activedamt"] = 8,
								["uptime"] = 135,
								["id"] = 337912,
								["appliedamt"] = 8,
								["targets"] = {
								},
								["actived_at"] = 3210120476,
							},
							[1044] = {
								["counter"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 1044,
								["uptime"] = 12,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
							[303380] = {
								["counter"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 303380,
								["uptime"] = 10,
								["targets"] = {
								},
								["refreshamt"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["serial"] = "Player-76-057FA942",
					["fight_component"] = true,
					["debuff_uptime"] = 1101,
					["buff_uptime"] = 6693,
					["cc_done"] = 3.017237,
					["debuff_uptime_targets"] = {
					},
					["spec"] = 70,
					["grupo"] = true,
					["spell_cast"] = {
						[20066] = 1,
						[190784] = 1,
						[295373] = 1,
						[35395] = 147,
						[853] = 2,
						[231895] = 5,
						[24275] = 42,
						[85256] = 196,
						[19750] = 11,
						[642] = 1,
						[184575] = 113,
						[85673] = 6,
						[184662] = 3,
						[273942] = 8,
						[20271] = 75,
						[53385] = 72,
						[255937] = 35,
					},
					["classe"] = "PALADIN",
					["cc_done_targets"] = {
						["Darksworn Sentry"] = 1,
						["Vrykul Necrolord"] = 1,
						["Chillbone Gnawer"] = 1,
					},
					["last_event"] = 0,
					["tipo"] = 4,
					["buff_uptime_targets"] = {
					},
					["cooldowns_defensive_spells"] = {
						["_ActorTable"] = {
							[184662] = {
								["id"] = 184662,
								["targets"] = {
									["Thorddin"] = 3,
								},
								["counter"] = 3,
							},
							[642] = {
								["id"] = 642,
								["targets"] = {
									["Thorddin"] = 1,
								},
								["counter"] = 1,
							},
						},
						["tipo"] = 9,
					},
					["nome"] = "Thorddin",
				}, -- [1]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Shambling Horror",
					["flag_original"] = 2632,
					["pets"] = {
					},
					["fight_component"] = true,
					["tipo"] = 4,
					["aID"] = "170235",
					["last_event"] = 0,
					["serial"] = "Creature-0-3778-0-1485-170235-00022B3FA5",
					["spell_cast"] = {
						[331651] = 8,
					},
				}, -- [2]
				{
					["flag_original"] = 2632,
					["last_event"] = 0,
					["nome"] = "Nathanos Blightcaller",
					["pets"] = {
					},
					["spell_cast"] = {
						[330062] = 11,
						[331256] = 16,
						[331253] = 9,
						[330079] = 27,
						[331605] = 1,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "169035",
					["serial"] = "Creature-0-3778-0-1756-169035-00002B419C",
					["fight_component"] = true,
				}, -- [3]
				{
					["flag_original"] = 1304,
					["tipo"] = 4,
					["nome"] = "Chuckleê",
					["spec"] = 270,
					["spell_cast"] = {
						[116849] = 1,
						[116670] = 17,
						[124682] = 10,
						[115175] = 1,
						[116841] = 1,
						[191837] = 4,
						[285482] = 1,
						[116680] = 1,
						[115151] = 11,
						[197908] = 1,
					},
					["aID"] = "76-0B1646CC",
					["last_event"] = 0,
					["classe"] = "MONK",
					["pets"] = {
					},
					["serial"] = "Player-76-0B1646CC",
					["fight_component"] = true,
				}, -- [4]
				{
					["flag_original"] = 1304,
					["tipo"] = 4,
					["nome"] = "Ræquan",
					["spec"] = 269,
					["spell_cast"] = {
						[107428] = 3,
						[116670] = 22,
						[113656] = 4,
						[123904] = 1,
						[152175] = 2,
						[109132] = 4,
						[137639] = 1,
						[123986] = 2,
						[100780] = 8,
						[101546] = 3,
						[322101] = 2,
					},
					["aID"] = "76-08BCF804",
					["last_event"] = 0,
					["classe"] = "MONK",
					["pets"] = {
						"Earth Spirit <Ræquan>", -- [1]
						"Fire Spirit <Ræquan>", -- [2]
					},
					["serial"] = "Player-76-08BCF804",
					["fight_component"] = true,
				}, -- [5]
				{
					["flag_original"] = 2632,
					["last_event"] = 0,
					["nome"] = "Plaguefang",
					["pets"] = {
					},
					["spell_cast"] = {
						[331340] = 7,
						[331607] = 1,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "169119",
					["serial"] = "Creature-0-3778-0-1756-169119-00002B419C",
					["fight_component"] = true,
				}, -- [6]
				{
					["flag_original"] = 2632,
					["last_event"] = 0,
					["nome"] = "Blight-Howl",
					["pets"] = {
					},
					["spell_cast"] = {
						[331340] = 6,
						[331607] = 1,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "169118",
					["serial"] = "Creature-0-3778-0-1756-169118-00002B419C",
					["fight_component"] = true,
				}, -- [7]
				{
					["flag_original"] = 8472,
					["ownerName"] = "Ræquan",
					["nome"] = "Earth Spirit <Ræquan>",
					["last_event"] = 0,
					["spell_cast"] = {
						[138130] = 4,
					},
					["pets"] = {
					},
					["aID"] = "69792",
					["classe"] = "PET",
					["serial"] = "Creature-0-3778-0-1756-69792-00002B41CD",
					["tipo"] = 4,
				}, -- [8]
				{
					["flag_original"] = 8472,
					["ownerName"] = "Ræquan",
					["nome"] = "Fire Spirit <Ræquan>",
					["last_event"] = 0,
					["spell_cast"] = {
						[138130] = 4,
					},
					["pets"] = {
					},
					["aID"] = "69791",
					["classe"] = "PET",
					["serial"] = "Creature-0-3778-0-1756-69791-00002B41CD",
					["tipo"] = 4,
				}, -- [9]
				{
					["fight_component"] = true,
					["tipo"] = 4,
					["nome"] = "ßlyss",
					["spec"] = 257,
					["spell_cast"] = {
						[34861] = 4,
						[14914] = 1,
						[596] = 16,
						[88625] = 1,
						[589] = 2,
						[297375] = 1,
						[17] = 2,
					},
					["last_event"] = 0,
					["pets"] = {
					},
					["aID"] = "76-0A67610B",
					["classe"] = "PRIEST",
					["serial"] = "Player-76-0A67610B",
					["flag_original"] = 1304,
				}, -- [10]
				{
					["flag_original"] = 1304,
					["tipo"] = 4,
					["nome"] = "[*] Chilling Winds",
					["spell_cast"] = {
						[326788] = 173,
					},
					["fight_component"] = true,
					["aID"] = "",
					["last_event"] = 0,
					["pets"] = {
					},
					["serial"] = "",
					["classe"] = "UNGROUPPLAYER",
				}, -- [11]
				{
					["fight_component"] = true,
					["last_event"] = 0,
					["nome"] = "Frostbrood Whelp",
					["monster"] = true,
					["spell_cast"] = {
						[60814] = 9,
					},
					["pets"] = {
					},
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "168024",
					["serial"] = "Creature-0-3138-571-32512-168024-00022B44D9",
					["flag_original"] = 68168,
				}, -- [12]
				{
					["fight_component"] = true,
					["last_event"] = 0,
					["nome"] = "Wyrm Reanimator",
					["monster"] = true,
					["spell_cast"] = {
						[32063] = 1,
						[9613] = 2,
					},
					["pets"] = {
					},
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "168026",
					["serial"] = "Creature-0-3138-571-32512-168026-0000AB4315",
					["flag_original"] = 2632,
				}, -- [13]
				{
					["flag_original"] = 2632,
					["last_event"] = 0,
					["nome"] = "Dark Zealot",
					["pets"] = {
					},
					["spell_cast"] = {
						[13584] = 23,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "172915",
					["serial"] = "Creature-0-3138-571-32512-172915-00002B4593",
					["fight_component"] = true,
				}, -- [14]
				{
					["flag_original"] = 68168,
					["last_event"] = 0,
					["nome"] = "Dark Ritualist",
					["pets"] = {
					},
					["spell_cast"] = {
						[32000] = 11,
						[32026] = 4,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "172917",
					["serial"] = "Creature-0-3138-571-32512-172917-00002B45AC",
					["fight_component"] = true,
				}, -- [15]
				{
					["flag_original"] = 2632,
					["last_event"] = 0,
					["nome"] = "Putrid Atrocity",
					["pets"] = {
					},
					["spell_cast"] = {
						[49861] = 4,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "172985",
					["serial"] = "Creature-0-3138-571-32512-172985-00002B4543",
					["fight_component"] = true,
				}, -- [16]
				{
					["flag_original"] = 68168,
					["last_event"] = 0,
					["nome"] = "Zealous Neophyte",
					["pets"] = {
					},
					["spell_cast"] = {
						[323720] = 13,
						[323727] = 4,
					},
					["monster"] = true,
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["aID"] = "172984",
					["serial"] = "Creature-0-3138-571-32512-172984-00002B45E4",
					["fight_component"] = true,
				}, -- [17]
				{
					["fight_component"] = true,
					["tipo"] = 4,
					["ownerName"] = "Thorddin",
					["nome"] = "Altered Ghoul <Thorddin>",
					["pets"] = {
					},
					["last_event"] = 0,
					["aID"] = "",
					["classe"] = "PET",
					["spell_cast"] = {
						[337945] = 13,
						[337943] = 1,
						[337959] = 2,
					},
					["serial"] = "Vehicle-0-3138-571-32512-172939-00002B4673",
					["flag_original"] = 4369,
				}, -- [18]
				{
					["flag_original"] = 68168,
					["last_event"] = 0,
					["nome"] = "Lumbering Atrocity",
					["pets"] = {
					},
					["spell_cast"] = {
						[323623] = 4,
						[289570] = 2,
					},
					["monster"] = true,
					["aID"] = "173432",
					["classe"] = "UNKNOW",
					["tipo"] = 4,
					["serial"] = "Creature-0-3138-571-32512-173432-00002B4793",
					["fight_component"] = true,
				}, -- [19]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Cult Shadowmage",
					["flag_original"] = 2632,
					["pets"] = {
					},
					["fight_component"] = true,
					["tipo"] = 4,
					["aID"] = "170441",
					["last_event"] = 0,
					["serial"] = "Creature-0-3138-571-32512-170441-00002B47B1",
					["spell_cast"] = {
						[333999] = 12,
					},
				}, -- [20]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Bone Giant",
					["flag_original"] = 68168,
					["pets"] = {
					},
					["fight_component"] = true,
					["tipo"] = 4,
					["aID"] = "170447",
					["last_event"] = 0,
					["serial"] = "Creature-0-3138-571-32512-170447-00002B47E9",
					["spell_cast"] = {
						[31277] = 4,
						[32736] = 4,
						[341840] = 1,
						[341827] = 4,
					},
				}, -- [21]
				{
					["flag_original"] = 68168,
					["tipo"] = 4,
					["nome"] = "Skeletal Footsoldier",
					["fight_component"] = true,
					["spell_cast"] = {
						[191887] = 2,
						[87081] = 1,
					},
					["monster"] = true,
					["last_event"] = 0,
					["aID"] = "168247",
					["classe"] = "UNKNOW",
					["serial"] = "Creature-0-3020-571-2273-168247-00002C9100",
					["pets"] = {
					},
				}, -- [22]
				{
					["monster"] = true,
					["classe"] = "UNKNOW",
					["nome"] = "Decrepit Laborer",
					["flag_original"] = 2632,
					["pets"] = {
					},
					["spell_cast"] = {
						[48374] = 7,
					},
					["tipo"] = 4,
					["last_event"] = 0,
					["aID"] = "168227",
					["serial"] = "Creature-0-3020-571-2273-168227-00012C907B",
					["fight_component"] = true,
				}, -- [23]
				{
					["fight_component"] = true,
					["cc_done"] = 1.013542,
					["nome"] = "Relynn",
					["flag_original"] = 1304,
					["cc_done_targets"] = {
						["Putrid Experiment"] = 1,
					},
					["pets"] = {
					},
					["classe"] = "UNGROUPPLAYER",
					["cc_done_spells"] = {
						["_ActorTable"] = {
							[200200] = {
								["id"] = 200200,
								["targets"] = {
									["Putrid Experiment"] = 1,
								},
								["counter"] = 1,
							},
						},
						["tipo"] = 9,
					},
					["aID"] = "76-0A566962",
					["spell_cast"] = {
						[132157] = 8,
						[589] = 7,
						[88625] = 1,
						[585] = 28,
					},
					["tipo"] = 4,
					["serial"] = "Player-76-0A566962",
					["last_event"] = 0,
				}, -- [24]
				{
					["monster"] = true,
					["aID"] = "168184",
					["nome"] = "Colossal Plaguespreader",
					["pets"] = {
					},
					["spell_cast"] = {
						[28405] = 1,
						[63546] = 2,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3020-571-2273-168184-00002C9294",
					["flag_original"] = 68168,
				}, -- [25]
				{
					["flag_original"] = 1304,
					["last_event"] = 0,
					["nome"] = "Gardëk",
					["spec"] = 66,
					["pets"] = {
					},
					["fight_component"] = true,
					["tipo"] = 4,
					["aID"] = "76-0A6A85DC",
					["classe"] = "PALADIN",
					["serial"] = "Player-76-0A6A85DC",
					["spell_cast"] = {
						[53600] = 1,
						[275779] = 3,
						[31935] = 1,
						[26573] = 1,
						[255647] = 1,
						[24275] = 1,
						[53595] = 2,
					},
				}, -- [26]
				{
					["monster"] = true,
					["aID"] = "174051",
					["nome"] = "Trollgore",
					["pets"] = {
					},
					["spell_cast"] = {
						[341362] = 1,
						[341357] = 2,
						[49380] = 2,
						[341361] = 1,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3020-571-2273-174051-00002C92B7",
					["flag_original"] = 68168,
				}, -- [27]
				{
					["flag_original"] = 1304,
					["last_event"] = 0,
					["nome"] = "Vaethiran",
					["spec"] = 270,
					["pets"] = {
					},
					["fight_component"] = true,
					["tipo"] = 4,
					["aID"] = "76-05DA65D9",
					["classe"] = "MONK",
					["serial"] = "Player-76-05DA65D9",
					["spell_cast"] = {
						[101546] = 4,
						[107428] = 1,
						[116849] = 1,
						[191837] = 1,
					},
				}, -- [28]
				{
					["flag_original"] = 68168,
					["tipo"] = 4,
					["nome"] = "Vile Sludgefiend",
					["fight_component"] = true,
					["spell_cast"] = {
						[329442] = 20,
					},
					["monster"] = true,
					["last_event"] = 0,
					["aID"] = "169781",
					["classe"] = "UNKNOW",
					["serial"] = "Creature-0-3020-571-2273-169781-00002C926A",
					["pets"] = {
					},
				}, -- [29]
				{
					["monster"] = true,
					["aID"] = "169784",
					["nome"] = "Putrid Experiment",
					["pets"] = {
					},
					["spell_cast"] = {
						[60678] = 10,
						[50106] = 3,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3020-571-2273-169784-00002C926A",
					["flag_original"] = 68168,
				}, -- [30]
				{
					["fight_component"] = true,
					["monster"] = true,
					["nome"] = "Darksworn Bonecaster",
					["flag_original"] = 68168,
					["spell_cast"] = {
						[183345] = 3,
					},
					["tipo"] = 4,
					["last_event"] = 0,
					["aID"] = "171757",
					["classe"] = "UNKNOW",
					["serial"] = "Creature-0-3023-571-32109-171757-00002D3F1B",
					["pets"] = {
					},
				}, -- [31]
				{
					["monster"] = true,
					["aID"] = "169667",
					["nome"] = "Vrykul Necrolord",
					["pets"] = {
					},
					["spell_cast"] = {
						[50027] = 1,
						[32712] = 1,
						[9613] = 2,
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["last_event"] = 0,
					["tipo"] = 4,
					["serial"] = "Creature-0-3023-571-32355-169667-00002DBB49",
					["flag_original"] = 2632,
				}, -- [32]
				{
					["flag_original"] = 68168,
					["tipo"] = 4,
					["nome"] = "Darksworn Sentry",
					["fight_component"] = true,
					["spell_cast"] = {
						[16564] = 14,
					},
					["monster"] = true,
					["aID"] = "",
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["serial"] = "Vehicle-0-3023-571-32054-171768-00002DF3C1",
					["pets"] = {
					},
				}, -- [33]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1605058489,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 4.152000000000044,
				["type"] = 0,
				["name"] = "Unknown",
				["clock"] = "11:39:22",
			}, -- [1]
			{
				["elapsed"] = 14.58100000000013,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "11:39:05",
			}, -- [2]
			{
				["elapsed"] = 8.278999999998632,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:38:38",
			}, -- [3]
			{
				["elapsed"] = 12.1810000000005,
				["type"] = 0,
				["name"] = "Darksworn Bonecaster",
				["clock"] = "11:38:19",
			}, -- [4]
			{
				["elapsed"] = 9.519999999998618,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "11:37:52",
			}, -- [5]
			{
				["elapsed"] = 8.930000000000291,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:37:24",
			}, -- [6]
			{
				["elapsed"] = 8.296999999998661,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:36:51",
			}, -- [7]
			{
				["elapsed"] = 7.356999999999971,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:36:10",
			}, -- [8]
			{
				["elapsed"] = 4.502999999998792,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:36:02",
			}, -- [9]
			{
				["elapsed"] = 6.877000000000408,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:34:48",
			}, -- [10]
			{
				["elapsed"] = 11.38500000000022,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "11:34:32",
			}, -- [11]
			{
				["elapsed"] = 8.30999999999949,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "11:33:31",
			}, -- [12]
			{
				["elapsed"] = 7.014999999999418,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:33:19",
			}, -- [13]
			{
				["elapsed"] = 10.01100000000042,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:28:14",
			}, -- [14]
			{
				["elapsed"] = 11.01999999999862,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:27:59",
			}, -- [15]
			{
				["elapsed"] = 5.527000000000044,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:25:39",
			}, -- [16]
			{
				["elapsed"] = 13.63199999999961,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "11:25:16",
			}, -- [17]
			{
				["elapsed"] = 9.95299999999952,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:24:13",
			}, -- [18]
			{
				["elapsed"] = 10.28700000000026,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:23:54",
			}, -- [19]
			{
				["elapsed"] = 12.33699999999953,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:23:36",
			}, -- [20]
			{
				["elapsed"] = 8.69000000000051,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:23:19",
			}, -- [21]
			{
				["elapsed"] = 5.90599999999904,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "11:19:20",
			}, -- [22]
			{
				["elapsed"] = 8.950999999999112,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "11:18:47",
			}, -- [23]
			{
				["elapsed"] = 7.897999999999229,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "10:47:22",
			}, -- [24]
			{
				["elapsed"] = 7.741000000001804,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "22:52:59",
			}, -- [25]
			{
				["elapsed"] = 18.01400000000285,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "22:51:46",
			}, -- [26]
			{
				["elapsed"] = 9.163000000000466,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "22:51:33",
			}, -- [27]
			{
				["elapsed"] = 14.92000000000553,
				["type"] = 0,
				["name"] = "Darksworn Sentry",
				["clock"] = "22:51:12",
			}, -- [28]
			{
				["elapsed"] = 7.285000000003493,
				["type"] = 0,
				["name"] = "Chillbone Gnawer",
				["clock"] = "22:27:09",
			}, -- [29]
			{
				["elapsed"] = 5.292999999997846,
				["type"] = 0,
				["name"] = "Darksworn Bonecaster",
				["clock"] = "22:26:57",
			}, -- [30]
		},
		["combat_counter"] = 1773,
		["totals"] = {
			4828963.088654002, -- [1]
			621351.7897679992, -- [2]
			{
				446.9250060000001, -- [1]
				[0] = 50.28587500000022,
				["alternatepower"] = 0,
				[6] = 608.7294409523165,
				[3] = -0.06953199999989579,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 1.007569,
				["cooldowns_defensive"] = 4.027533,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "20:34:49",
		["end_time"] = 12656.516,
		["cleu_timeline"] = {
		},
		["totals_grupo"] = {
			1285131.62973, -- [1]
			153888.512559, -- [2]
			{
				0, -- [1]
				[0] = 0.5832520000000002,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 4.027533,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = "11:39:27",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["frags"] = {
		},
		["start_time"] = 11159.12500000002,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["cleu_events"] = {
			["n"] = 1,
		},
	},
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["character_data"] = {
		["logons"] = 124,
	},
	["last_day"] = "29",
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-057FA942"] = 70,
	},
}
