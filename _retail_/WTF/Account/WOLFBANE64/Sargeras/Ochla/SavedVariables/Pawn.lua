
PawnOptions = {
	["LastVersion"] = 2.0246,
	["LastPlayerFullName"] = "Ochla-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 153808,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153808::::::::110:253::::::|h[Trailseeker Helm]|h|r",
		}, -- [1]
		{
			["ID"] = 153800,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153800::::::::110:253::::::|h[Trailseeker Choker]|h|r",
		}, -- [2]
		{
			["ID"] = 153810,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153810::::::::110:253::::::|h[Trailseeker Spaulders]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 153805,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153805::::::::110:253::::::|h[Trailseeker Vest]|h|r",
		}, -- [5]
		{
			["ID"] = 153811,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153811::::::::110:253::::::|h[Trailseeker Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 153809,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153809::::::::110:253::::::|h[Trailseeker Legguards]|h|r",
		}, -- [7]
		{
			["ID"] = 153806,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153806::::::::110:253::::::|h[Trailseeker Greaves]|h|r",
		}, -- [8]
		{
			["ID"] = 153812,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153812::::::::110:253::::::|h[Trailseeker Bracers]|h|r",
		}, -- [9]
		{
			["ID"] = 153807,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153807::::::::110:253::::::|h[Trailseeker Gauntlets]|h|r",
		}, -- [10]
		{
			["ID"] = 153803,
			["Level"] = 176,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff1eff00|Hitem:153803::::::::110:253::::::|h[Trailseeker Ring of Onslaught]|h|r",
		}, -- [11]
		{
			["ID"] = 153802,
			["Level"] = 176,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff1eff00|Hitem:153802::::::::110:253::::::|h[Trailseeker Band of Onslaught]|h|r",
		}, -- [12]
		{
			["ID"] = 153801,
			["Level"] = 176,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff1eff00|Hitem:153801::::::::110:253::::::|h[Trailseeker Idol of Rage]|h|r",
		}, -- [13]
		{
			["ID"] = 153804,
			["Level"] = 176,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff1eff00|Hitem:153804::::::::110:253::::::|h[Trailseeker Stone of Rage]|h|r",
		}, -- [14]
		{
			["ID"] = 153799,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153799::::::::110:253::::::|h[Trailseeker Cloak of Rage]|h|r",
		}, -- [15]
		{
			["ID"] = 153813,
			["Level"] = 185,
			["Link"] = "|cff1eff00|Hitem:153813::::::::110:253::::::|h[Trailseeker Shotgun]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "HUNTER",
	["LastAdded"] = 1,
}
