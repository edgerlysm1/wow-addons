
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008597,
							["damage_from"] = {
							},
							["targets"] = {
								["Target Dummy"] = 32291,
							},
							["total"] = 32291.008597,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "HUNTER",
							["raid_targets"] = {
							},
							["total_without_pet"] = 32291.008597,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1569623388,
							["friendlyfire_total"] = 0,
							["spec"] = 254,
							["nome"] = "Ochla",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[75] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1529,
										["g_amt"] = 0,
										["n_max"] = 383,
										["targets"] = {
											["Target Dummy"] = 3824,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2295,
										["n_min"] = 382,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 3824,
										["c_max"] = 765,
										["id"] = 75,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["r_amt"] = 0,
										["c_min"] = 764,
									},
									[185358] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1572,
										["g_amt"] = 0,
										["n_max"] = 1376,
										["targets"] = {
											["Target Dummy"] = 7270,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 5698,
										["n_min"] = 786,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 7270,
										["c_max"] = 1572,
										["id"] = 185358,
										["r_dmg"] = 0,
										["spellschool"] = 64,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 1572,
									},
									[256893] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 5958,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Target Dummy"] = 5958,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 5958,
										["c_max"] = 5958,
										["id"] = 256893,
										["r_dmg"] = 0,
										["spellschool"] = 2,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 5958,
									},
									[56641] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 2200,
										["g_amt"] = 0,
										["n_max"] = 551,
										["targets"] = {
											["Target Dummy"] = 3851,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1651,
										["n_min"] = 550,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 3851,
										["c_max"] = 1100,
										["id"] = 56641,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 1100,
									},
									[19434] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 6832,
										["g_amt"] = 0,
										["n_max"] = 2278,
										["targets"] = {
											["Target Dummy"] = 11388,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4556,
										["n_min"] = 2278,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 11388,
										["c_max"] = 6832,
										["id"] = 19434,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 6832,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A371F4D",
							["last_dps"] = 1747.916455396587,
							["custom"] = 0,
							["last_event"] = 1569623383,
							["damage_taken"] = 0.008597,
							["start_time"] = 1569623364,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00787,
							["damage_from"] = {
								["Ochla"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.00787,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 0.00787,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3138-1949-26514-107104-00000E8A0A",
							["nome"] = "Target Dummy",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1569623388,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 32291.00787,
							["start_time"] = 1569623388,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 10,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 10,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 10,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Ochla",
							["spec"] = 254,
							["grupo"] = true,
							["spell_cast"] = {
								[185358] = 6,
								[56641] = 5,
								[19434] = 3,
							},
							["pets"] = {
							},
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 14,
							["tipo"] = 4,
							["last_event"] = 1569623386,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[260242] = {
										["activedamt"] = 2,
										["id"] = 260242,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[164273] = {
										["activedamt"] = 1,
										["id"] = 164273,
										["targets"] = {
										},
										["actived_at"] = 1569623364,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[231390] = {
										["activedamt"] = 2,
										["id"] = 231390,
										["targets"] = {
										},
										["actived_at"] = 1569623386,
										["uptime"] = 1,
										["appliedamt"] = 2,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
									[269576] = {
										["activedamt"] = 2,
										["id"] = 269576,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 2,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
									[193534] = {
										["activedamt"] = 3,
										["id"] = 193534,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 3,
										["refreshamt"] = 1,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0A371F4D",
							["classe"] = "HUNTER",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Ochla"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1569623364,
				["enemy"] = "Target Dummy",
				["combat_counter"] = 15,
				["playing_solo"] = true,
				["totals"] = {
					32291, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 24682.041,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "18:29:25",
				["end_time"] = 24682.041,
				["totals_grupo"] = {
					32291, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 10,
				["TotalElapsedCombatTime"] = 24682.041,
				["frags_need_refresh"] = false,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Ochla"] = 32291.008597,
						}, -- [1]
					},
				},
				["frags"] = {
				},
				["data_fim"] = "18:29:49",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 24658.625,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
		},
	},
	["last_version"] = "v8.2.0.7177",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1570918123,
		["last_version"] = 11,
	},
	["last_instance_id"] = 1949,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 1569622139,
	["active_profile"] = "Ochla-Sargeras",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "27",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 10,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 15,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.047979,
					["damage_from"] = {
						["Alliance Infantry"] = true,
						["Wyvern Rider"] = true,
						["Horde Grunt"] = true,
					},
					["targets"] = {
						["Alliance Infantry"] = 22360,
						["Horde Grunt"] = 28390,
						["Wyvern Rider"] = 40990,
						["Target Dummy"] = 617583,
					},
					["pets"] = {
						"Stag <Ochla>", -- [1]
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 581822.047979,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["dps_started"] = false,
					["total"] = 709323.047979,
					["classe"] = "HUNTER",
					["end_time"] = 1569622337,
					["nome"] = "Ochla",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							[217200] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 281,
								["targets"] = {
									["Alliance Infantry"] = 729,
									["Horde Grunt"] = 729,
									["Wyvern Rider"] = 3920,
									["Target Dummy"] = 7133,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 12511,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 52,
								["total"] = 12511,
								["c_max"] = 0,
								["id"] = 217200,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 52,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[19434] = {
								["c_amt"] = 7,
								["b_amt"] = 0,
								["c_dmg"] = 50856,
								["g_amt"] = 0,
								["n_max"] = 3885,
								["targets"] = {
									["Target Dummy"] = 151196,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 100340,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 34,
								["total"] = 151196,
								["c_max"] = 7769,
								["id"] = 19434,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 27,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[75] = {
								["c_amt"] = 91,
								["b_amt"] = 0,
								["c_dmg"] = 76466,
								["g_amt"] = 0,
								["n_max"] = 485,
								["targets"] = {
									["Alliance Infantry"] = 4452,
									["Horde Grunt"] = 5421,
									["Wyvern Rider"] = 9291,
									["Target Dummy"] = 178450,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 121148,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 380,
								["total"] = 197614,
								["c_max"] = 969,
								["id"] = 75,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 289,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[257620] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 782,
								["g_amt"] = 0,
								["n_max"] = 391,
								["targets"] = {
									["Target Dummy"] = 2346,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1564,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 2346,
								["c_max"] = 782,
								["id"] = 257620,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[256893] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 5958,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Target Dummy"] = 5958,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 5958,
								["c_max"] = 5958,
								["id"] = 256893,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[56641] = {
								["c_amt"] = 21,
								["b_amt"] = 0,
								["c_dmg"] = 25970,
								["g_amt"] = 0,
								["n_max"] = 626,
								["targets"] = {
									["Target Dummy"] = 53893,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 27923,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 66,
								["total"] = 53893,
								["c_max"] = 1252,
								["id"] = 56641,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 45,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[193455] = {
								["c_amt"] = 3,
								["b_amt"] = 0,
								["c_dmg"] = 1758,
								["g_amt"] = 0,
								["n_max"] = 314,
								["targets"] = {
									["Alliance Infantry"] = 1318,
									["Horde Grunt"] = 1067,
									["Wyvern Rider"] = 3515,
									["Target Dummy"] = 3015,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 7157,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 30,
								["total"] = 8915,
								["c_max"] = 628,
								["id"] = 193455,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 27,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[185358] = {
								["c_amt"] = 24,
								["b_amt"] = 0,
								["c_dmg"] = 58760,
								["g_amt"] = 0,
								["n_max"] = 1564,
								["targets"] = {
									["Target Dummy"] = 125445,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 66685,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 80,
								["total"] = 125445,
								["c_max"] = 3128,
								["id"] = 185358,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 56,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[257045] = {
								["c_amt"] = 16,
								["b_amt"] = 0,
								["c_dmg"] = 8674,
								["g_amt"] = 0,
								["n_max"] = 272,
								["targets"] = {
									["Target Dummy"] = 23310,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 14636,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 70,
								["total"] = 23310,
								["c_max"] = 543,
								["id"] = 257045,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 54,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[2643] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 160,
								["g_amt"] = 0,
								["n_max"] = 79,
								["targets"] = {
									["Alliance Infantry"] = 316,
									["Wyvern Rider"] = 79,
									["Horde Grunt"] = 239,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 474,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 634,
								["c_max"] = 160,
								["id"] = 2643,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["on_hold"] = false,
					["spec"] = 253,
					["serial"] = "Player-76-0A371F4D",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1569621338,
					["delay"] = 0,
					["damage_taken"] = 10153.047979,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.03211,
					["fight_component"] = true,
					["damage_from"] = {
						["Ochla"] = true,
						["Stag <Ochla>"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.03211,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1569622337,
					["last_event"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Target Dummy",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["total"] = 0.03211,
					["serial"] = "Creature-0-3138-1949-26514-107104-00000E88B2",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 617583.0321099999,
					["start_time"] = 1569622334,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
				{
					["flag_original"] = 4369,
					["totalabsorbed"] = 0.024362,
					["on_hold"] = false,
					["damage_from"] = {
						["Horde Grunt"] = true,
						["Wyvern Rider"] = true,
						["Alliance Infantry"] = true,
					},
					["targets"] = {
						["Alliance Infantry"] = 15545,
						["Horde Grunt"] = 20934,
						["Wyvern Rider"] = 24185,
						["Target Dummy"] = 66837,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["classe"] = "PET",
					["raid_targets"] = {
					},
					["total_without_pet"] = 127501.024362,
					["end_time"] = 1569622337,
					["dps_started"] = false,
					["total"] = 127501.024362,
					["last_event"] = 0,
					["ownerName"] = "Ochla",
					["nome"] = "Stag <Ochla>",
					["friendlyfire"] = {
					},
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 34,
								["b_amt"] = 0,
								["c_dmg"] = 14469,
								["g_amt"] = 0,
								["n_max"] = 257,
								["targets"] = {
									["Alliance Infantry"] = 3331,
									["Horde Grunt"] = 3436,
									["Wyvern Rider"] = 9687,
									["Target Dummy"] = 24337,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 26322,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 157,
								["total"] = 40791,
								["c_max"] = 514,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 123,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[49966] = {
								["c_amt"] = 15,
								["b_amt"] = 0,
								["c_dmg"] = 16508,
								["g_amt"] = 0,
								["n_max"] = 677,
								["targets"] = {
									["Alliance Infantry"] = 6086,
									["Horde Grunt"] = 6223,
									["Wyvern Rider"] = 6762,
									["Target Dummy"] = 26796,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 29359,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 67,
								["total"] = 45867,
								["c_max"] = 1354,
								["id"] = 49966,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 52,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[83381] = {
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 9558,
								["g_amt"] = 0,
								["n_max"] = 1138,
								["targets"] = {
									["Alliance Infantry"] = 4551,
									["Horde Grunt"] = 3868,
									["Wyvern Rider"] = 7736,
									["Target Dummy"] = 15704,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 22301,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 28,
								["total"] = 31859,
								["c_max"] = 2276,
								["id"] = 83381,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 23,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[118459] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 896,
								["targets"] = {
									["Alliance Infantry"] = 1577,
									["Horde Grunt"] = 7407,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 8984,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 21,
								["total"] = 8984,
								["c_max"] = 0,
								["id"] = 118459,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 21,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["serial"] = "Pet-0-3138-1949-26514-131074-0202DB4E1D",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1569622098,
					["delay"] = 0,
					["damage_taken"] = 16249.024362,
				}, -- [3]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.021645,
					["on_hold"] = false,
					["damage_from"] = {
						["Ochla"] = true,
						["Horde Grunt"] = true,
						["Stag <Ochla>"] = true,
					},
					["targets"] = {
						["Ochla"] = 280,
						["Horde Grunt"] = 51293,
						["Stag <Ochla>"] = 2437,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 54010.021645,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569622418,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Alliance Infantry",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 754,
								["g_amt"] = 0,
								["n_max"] = 2033,
								["targets"] = {
									["Ochla"] = 280,
									["Horde Grunt"] = 51293,
									["Stag"] = 0,
									["Stag <Ochla>"] = 2437,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 53256,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 87,
								["total"] = 54010,
								["c_max"] = 443,
								["a_amt"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 82,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
						},
					},
					["monster"] = true,
					["total"] = 54010.021645,
					["serial"] = "Creature-0-3138-1949-26514-102592-00000E8872",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 29324.021645,
					["start_time"] = 1569622318,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [4]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.010123,
					["on_hold"] = false,
					["damage_from"] = {
						["Stag <Ochla>"] = true,
						["Deckhand"] = true,
						["Ochla"] = true,
						["Magus Filinthus"] = true,
						["General Bret Hughes"] = true,
						["Alliance Lookout"] = true,
						["Alliance Infantry"] = true,
						["Alliance Recruit"] = true,
						["Anchorite Taliah"] = true,
					},
					["targets"] = {
						["Alliance Recruit"] = 6466,
						["Magus Filinthus"] = 1937,
						["General Bret Hughes"] = 2685,
						["Stag <Ochla>"] = 9557,
						["Alliance Infantry"] = 6964,
						["Deckhand"] = 12936,
						["Alliance Lookout"] = 1129,
						["Ochla"] = 2083,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 43757.010123,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569622537,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Horde Grunt",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 16,
								["b_amt"] = 0,
								["c_dmg"] = 3517,
								["g_amt"] = 0,
								["n_max"] = 284,
								["targets"] = {
									["Stag"] = 0,
									["Deckhand"] = 12936,
									["Ochla"] = 2083,
									["Magus Filinthus"] = 1937,
									["General Bret Hughes"] = 2685,
									["Alliance Lookout"] = 1129,
									["Alliance Infantry"] = 6964,
									["Stag <Ochla>"] = 9557,
									["Alliance Recruit"] = 6466,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 40240,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 416,
								["a_amt"] = 0,
								["total"] = 43757,
								["c_max"] = 521,
								["DODGE"] = 52,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 39,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 44,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 265,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[275764] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 275764,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 22,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["monster"] = true,
					["total"] = 43757.010123,
					["serial"] = "Creature-0-3138-1949-26514-140810-00018E89BA",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 208313.010123,
					["start_time"] = 1569622461,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [5]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.009021,
					["on_hold"] = false,
					["damage_from"] = {
						["Ochla"] = true,
						["Alliance Lookout"] = true,
						["Deckhand"] = true,
						["Stag <Ochla>"] = true,
						["Alliance Recruit"] = true,
					},
					["targets"] = {
						["Ochla"] = 7790,
						["Alliance Lookout"] = 8141,
						["Deckhand"] = 20430,
						["Alliance Recruit"] = 1992,
						["Stag <Ochla>"] = 4255,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 42608.009021,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569622537,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Wyvern Rider",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 4,
								["b_amt"] = 0,
								["c_dmg"] = 2718,
								["g_amt"] = 0,
								["n_max"] = 3313,
								["targets"] = {
									["Ochla"] = 7790,
									["Alliance Lookout"] = 8141,
									["Deckhand"] = 20430,
									["Alliance Recruit"] = 1992,
									["Stag <Ochla>"] = 4255,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 39890,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 53,
								["a_amt"] = 0,
								["total"] = 42608,
								["c_max"] = 713,
								["DODGE"] = 2,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 44,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
						},
					},
					["monster"] = true,
					["total"] = 42608.009021,
					["serial"] = "Creature-0-3138-1949-26514-140852-00008E89BB",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 45200.009021,
					["start_time"] = 1569622461,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [6]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 2584,
					["totalabsorb"] = 0.008232,
					["last_hps"] = 0,
					["healing_from"] = {
					},
					["targets"] = {
						["Ochla"] = 20895,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["start_time"] = 1569622415,
					["heal_enemy_amt"] = 0,
					["totalover"] = 20615.008232,
					["total_without_pet"] = 280.008232,
					["targets_overheal"] = {
						["Ochla"] = 20615,
					},
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[222169] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Ochla"] = 20615,
								},
								["n_max"] = 280,
								["targets"] = {
									["Ochla"] = 280,
								},
								["n_min"] = 0,
								["counter"] = 1,
								["overheal"] = 20615,
								["total"] = 280,
								["c_max"] = 0,
								["id"] = 222169,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 280,
								["n_amt"] = 1,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["fight_component"] = true,
					["total"] = 280.008232,
					["totalover_without_pet"] = 0.008232,
					["classe"] = "UNKNOW",
					["nome"] = "Anchorite Taliah",
					["targets_absorbs"] = {
					},
					["end_time"] = 1569622418,
					["healing_taken"] = 0.008232,
					["heal_enemy"] = {
					},
					["serial"] = "Creature-0-3138-1949-26514-107953-00000E8872",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["totaldenied"] = 0.008232,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [1]
				{
					["flag_original"] = 1297,
					["totalabsorb"] = 0.016047,
					["last_hps"] = 0,
					["healing_from"] = {
						["Anchorite Taliah"] = true,
					},
					["targets"] = {
					},
					["targets_absorbs"] = {
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["targets_overheal"] = {
						["Ochla"] = 5970,
					},
					["classe"] = "HUNTER",
					["totalover"] = 5970.016047,
					["total_without_pet"] = 0.016047,
					["healing_taken"] = 280.016047,
					["totalover_without_pet"] = 0.016047,
					["heal_enemy_amt"] = 0,
					["total"] = 0.016047,
					["start_time"] = 1569622415,
					["end_time"] = 1569622418,
					["nome"] = "Ochla",
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[109304] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Ochla"] = 5970,
								},
								["n_max"] = 0,
								["targets"] = {
									["Ochla"] = 0,
								},
								["n_min"] = 0,
								["counter"] = 1,
								["overheal"] = 5970,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 109304,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 0,
								["n_amt"] = 1,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["grupo"] = true,
					["spec"] = 253,
					["heal_enemy"] = {
					},
					["serial"] = "Player-76-0A371F4D",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["totaldenied"] = 0.016047,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [2]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1300,
					["debuff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[5116] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 1,
								["id"] = 5116,
								["uptime"] = 6,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[217200] = {
								["refreshamt"] = 5,
								["activedamt"] = 1,
								["appliedamt"] = 11,
								["id"] = 217200,
								["uptime"] = 88,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[257044] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 7,
								["id"] = 257044,
								["uptime"] = 19,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["cooldowns_defensive"] = 1.006491,
					["buff_uptime"] = 1214,
					["classe"] = "HUNTER",
					["cooldowns_defensive_targets"] = {
						["Ochla"] = 1,
					},
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[260242] = {
								["refreshamt"] = 2,
								["activedamt"] = 28,
								["appliedamt"] = 28,
								["id"] = 260242,
								["uptime"] = 114,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[246851] = {
								["refreshamt"] = 0,
								["activedamt"] = 5,
								["appliedamt"] = 5,
								["id"] = 246851,
								["uptime"] = 39,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[231390] = {
								["actived_at"] = 1569623386,
								["refreshamt"] = 0,
								["activedamt"] = 7,
								["appliedamt"] = 7,
								["id"] = 231390,
								["uptime"] = 12,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[246152] = {
								["refreshamt"] = 0,
								["activedamt"] = 13,
								["appliedamt"] = 13,
								["id"] = 246152,
								["uptime"] = 96,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[193534] = {
								["refreshamt"] = 13,
								["activedamt"] = 69,
								["appliedamt"] = 69,
								["id"] = 193534,
								["uptime"] = 78,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[164273] = {
								["actived_at"] = 1569623364,
								["refreshamt"] = 0,
								["activedamt"] = 5,
								["appliedamt"] = 5,
								["id"] = 164273,
								["uptime"] = 732,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[186257] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 186257,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[19574] = {
								["refreshamt"] = 0,
								["activedamt"] = 4,
								["appliedamt"] = 4,
								["id"] = 19574,
								["uptime"] = 49,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[268877] = {
								["refreshamt"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 268877,
								["uptime"] = 11,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[269576] = {
								["refreshamt"] = 7,
								["activedamt"] = 28,
								["appliedamt"] = 28,
								["id"] = 269576,
								["uptime"] = 72,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[186258] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 186258,
								["uptime"] = 8,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["debuff_uptime"] = 113,
					["cooldowns_defensive_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[109304] = {
								["id"] = 109304,
								["targets"] = {
									["Ochla"] = 1,
								},
								["counter"] = 1,
							},
						},
					},
					["buff_uptime_targets"] = {
					},
					["spec"] = 253,
					["grupo"] = true,
					["spell_cast"] = {
						[34026] = 27,
						[257620] = 4,
						[217200] = 18,
						[19434] = 33,
						[19574] = 4,
						[186387] = 1,
						[147362] = 1,
						[257044] = 7,
						[56641] = 66,
						[5116] = 1,
						[781] = 1,
						[185358] = 80,
						[109304] = 1,
						[193455] = 30,
						[2643] = 2,
						[186257] = 1,
						[272790] = 18,
					},
					["debuff_uptime_targets"] = {
					},
					["tipo"] = 4,
					["nome"] = "Ochla",
					["pets"] = {
						"Stag <Ochla>", -- [1]
					},
					["serial"] = "Player-76-0A371F4D",
					["last_event"] = 0,
				}, -- [1]
				{
					["flag_original"] = 4369,
					["ownerName"] = "Ochla",
					["nome"] = "Stag <Ochla>",
					["spell_cast"] = {
						[61684] = 2,
						[49966] = 68,
					},
					["tipo"] = 4,
					["last_event"] = 0,
					["pets"] = {
					},
					["serial"] = "Pet-0-3138-1949-26514-131074-0202DB4E1D",
					["classe"] = "PET",
				}, -- [2]
				{
					["fight_component"] = true,
					["nome"] = "Anchorite Taliah",
					["tipo"] = 4,
					["pets"] = {
					},
					["flag_original"] = 2584,
					["classe"] = "UNKNOW",
					["spell_cast"] = {
						[222169] = 1,
						[9734] = 4,
					},
					["serial"] = "Creature-0-3138-1949-26514-107953-00000E8872",
					["last_event"] = 0,
				}, -- [3]
				{
					["monster"] = true,
					["tipo"] = 4,
					["nome"] = "Horde Grunt",
					["pets"] = {
					},
					["spell_cast"] = {
						[275764] = 22,
					},
					["flag_original"] = 2632,
					["last_event"] = 0,
					["fight_component"] = true,
					["serial"] = "Creature-0-3138-1949-26514-140810-00078E89BA",
					["classe"] = "UNKNOW",
				}, -- [4]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1569622195,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 4,
		["totals"] = {
			977199.0817910002, -- [1]
			280.012898, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 1.003027,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "18:09:56",
		["end_time"] = 24682.041,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 23.41600000000108,
				["type"] = 0,
				["name"] = "Target Dummy",
				["clock"] = "18:29:25",
			}, -- [1]
			{
				["elapsed"] = 154.5220000000008,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:26:06",
			}, -- [2]
			{
				["elapsed"] = 499.7540000000008,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:17:38",
			}, -- [3]
			{
				["elapsed"] = 51.86299999999756,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:16:25",
			}, -- [4]
			{
				["elapsed"] = 25.24399999999878,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:15:59",
			}, -- [5]
			{
				["elapsed"] = 73.43000000000029,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:14:23",
			}, -- [6]
			{
				["elapsed"] = 14.71600000000035,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:13:47",
			}, -- [7]
			{
				["elapsed"] = 11.79299999999785,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:13:26",
			}, -- [8]
			{
				["elapsed"] = 140.9400000000023,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:09:56",
			}, -- [9]
		},
		["totals_grupo"] = {
			709323.045832, -- [1]
			0.009191999999999999, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 1.003027,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "18:29:49",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 23686.363,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 209.1941528320313,
					["x"] = -580.863525390625,
					["w"] = 310,
					["h"] = 158.0000152587891,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A371F4D"] = 254,
	},
}
