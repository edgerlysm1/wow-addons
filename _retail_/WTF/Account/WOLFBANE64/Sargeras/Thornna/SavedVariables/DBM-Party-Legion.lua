
DBMPartyLegion_SavedStats = {
	["1981"] = {
		["heroicLastTime"] = 17.4179999999997,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 17.4179999999997,
		["heroic25Kills"] = 0,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1825"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1718"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1480"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1835"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1826"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1657"] = {
		["heroicLastTime"] = 27.0950000000003,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 27.0950000000003,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1497"] = {
		["heroicLastTime"] = 43.0779999999977,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 43.0779999999977,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicKills"] = 1,
		["challengePulls"] = 0,
	},
	["1702"] = {
		["heroicLastTime"] = 26.2439999999988,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 26.2439999999988,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1663"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1492"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1491"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1653"] = {
		["heroicLastTime"] = 22.640000000014,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 22.640000000014,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1512"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1654"] = {
		["heroicLastTime"] = 26.0939999999991,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 26.0939999999991,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1720"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1479"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1662"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 72.1750000000002,
		["normalKills"] = 1,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 72.1750000000002,
		["challengePulls"] = 0,
	},
	["1688"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1687"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 84.7209999999996,
		["normalKills"] = 1,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 84.7209999999996,
		["challengePulls"] = 0,
	},
	["1498"] = {
		["heroicLastTime"] = 31.7330000000002,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 31.7330000000002,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicKills"] = 1,
		["challengePulls"] = 0,
	},
	["1501"] = {
		["heroicLastTime"] = 71.4739999999983,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 71.4739999999983,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicKills"] = 1,
		["challengePulls"] = 0,
	},
	["MawTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["HoVTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1469"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1500"] = {
		["heroicLastTime"] = 22.9409999999989,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 22.9409999999989,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicKills"] = 1,
		["challengePulls"] = 0,
	},
	["1470"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1468"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1820"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1485"] = {
		["heroicLastTime"] = 24.3930000000037,
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["heroicBestTime"] = 24.3930000000037,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 46.3150000000023,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 46.3150000000023,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1664"] = {
		["heroicLastTime"] = 22.3400000000256,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 22.3400000000256,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["AVHTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1518"] = {
		["heroicLastTime"] = 30.1149999999907,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 30.1149999999907,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1672"] = {
		["heroicLastTime"] = 59.3939999999711,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 59.3939999999711,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["NLTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["SoTTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["BRHTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1719"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1502"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1693"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1697"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1695"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1694"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1655"] = {
		["heroicLastTime"] = 22.9899999999998,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 22.9899999999998,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1467"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1827"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1904"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["EoATrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1980"] = {
		["heroicLastTime"] = 25.0100000000002,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 25.0100000000002,
		["heroic25Kills"] = 0,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1979"] = {
		["heroicLastTime"] = 30.915,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 30.915,
		["heroic25Kills"] = 0,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1486"] = {
		["heroicLastTime"] = 72.3919999999998,
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["heroicBestTime"] = 72.3919999999998,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 99.2700000000041,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 99.2700000000041,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["Nightbane"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1906"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["CoSTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["CoENTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1836"] = {
		["heroicLastTime"] = 20.2370000000083,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 20.2370000000083,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1489"] = {
		["heroicLastTime"] = 24.3609999999972,
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["heroicBestTime"] = 24.3609999999972,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 82.5359999999928,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 82.5359999999928,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1490"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1487"] = {
		["heroicLastTime"] = 69.3890000000029,
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["heroicBestTime"] = 69.3890000000029,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 184.542000000001,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 184.542000000001,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1488"] = {
		["heroicLastTime"] = 22.4230000000025,
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["heroicBestTime"] = 22.4230000000025,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 81.9020000000019,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 81.9020000000019,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1673"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 76.3960000000007,
		["normalKills"] = 1,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 76.3960000000007,
		["challengePulls"] = 0,
	},
	["1696"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["DHTTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1665"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 101.977,
		["normalKills"] = 1,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 101.977,
		["challengePulls"] = 0,
	},
	["1838"] = {
		["heroicLastTime"] = 58.9440000000032,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 58.9440000000032,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1818"] = {
		["heroicLastTime"] = 31.2649999999994,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 31.2649999999994,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1817"] = {
		["heroicLastTime"] = 15.9660000000004,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 15.9660000000004,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1982"] = {
		["heroicLastTime"] = 28.6300000000001,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 28.6300000000001,
		["heroic25Kills"] = 0,
		["heroicKills"] = 1,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1837"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1656"] = {
		["heroicLastTime"] = 29.0859999999993,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 29.0859999999993,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 1,
		["heroicPulls"] = 1,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["VoWTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1905"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1711"] = {
		["heroicLastTime"] = 32.6179999999949,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 32.6179999999949,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1499"] = {
		["heroicLastTime"] = 33.4180000000015,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 33.4180000000015,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicKills"] = 1,
		["challengePulls"] = 0,
	},
	["RTKTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1878"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["ArcwayTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1686"] = {
		["heroicLastTime"] = 23.0740000000005,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 23.0740000000005,
		["heroic25Kills"] = 0,
		["heroicPulls"] = 1,
		["heroicKills"] = 1,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["timewalkerPulls"] = 0,
		["challengePulls"] = 0,
	},
}
