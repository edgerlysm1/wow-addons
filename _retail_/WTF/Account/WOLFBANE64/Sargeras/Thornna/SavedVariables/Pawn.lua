
PawnOptions = {
	["LastVersion"] = 2.0406,
	["ItemLevels"] = {
		{
			["ID"] = 174167,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:174167::::::::50:65::5:5:4823:1502:4786:6508:4775::::|h[Greathelm of Phantasmic Reality]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 143,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::50:65::11:4:6316:4932:4933:1557::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 174165,
			["Level"] = 135,
			["Link"] = "|cffa335ee|Hitem:174165::::::::50:65::6:5:4824:1517:4786:6512:4775::::|h[Writhing Spaulders of Madness]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 159432,
			["Level"] = 135,
			["Link"] = "|cffa335ee|Hitem:159432::::::::50:70::35:5:5448:1543:4786:6510:4775::::|h[Breastplate of Arterial Protection]|h|r",
		}, -- [5]
		{
			["ID"] = 159426,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:159426::::::::50:70::35:8:6536:6578:6579:6474:6455:6513:1543:4786::::|h[Belt of the Unrelenting Gale]|h|r",
		}, -- [6]
		{
			["ID"] = 159435,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:159435::::::::50:70::35:8:6536:4802:6578:6579:6543:6513:1543:4786::::|h[Legplates of Charged Duality]|h|r",
		}, -- [7]
		{
			["ID"] = 159428,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:159428::168639::::::50:70::35:10:6536:4802:42:6578:6579:6482:6470:6513:1543:4786::::|h[Ballast Sinkers]|h|r",
		}, -- [8]
		{
			["ID"] = 159425,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:159425::::::::50:70::35:7:6536:6578:6579:6539:6513:1543:4786::::|h[Shard-Tipped Vambraces]|h|r",
		}, -- [9]
		{
			["ID"] = 155864,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:155864:5934:::::::50:70::35:9:6536:42:6578:6579:6473:6470:6513:1543:4786::::|h[Power-Assisted Vicegrips]|h|r",
		}, -- [10]
		{
			["ID"] = 174530,
			["Level"] = 130,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffa335ee|Hitem:174530:6108:::::::50:65::6:6:4824:42:6516:6515:1517:4786::::|h[Ring of Collective Consciousness]|h|r",
		}, -- [11]
		{
			["ID"] = 174530,
			["Level"] = 130,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:174530:6108:::::::50:70::6:6:4824:42:6516:6515:1517:4786::::|h[Ring of Collective Consciousness]|h|r",
		}, -- [12]
		{
			["ID"] = 174528,
			["Level"] = 115,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:174528::::::::50:65::5:4:4823:6515:1502:4786::::|h[Void-Twisted Titanshard]|h|r",
		}, -- [13]
		{
			["ID"] = 172673,
			["Level"] = 115,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:172673::::::::50:65::56:5:6371:6388:6513:1527:4786::::|h[Corrupted Gladiator's Safeguard]|h|r",
		}, -- [14]
		{
			["ID"] = 169223,
			["Level"] = 155,
			["Link"] = "|cffff8000|Hitem:169223::::::::50:65::11:2:6602:1472::::|h[Ashjra'kamas, Shroud of Resolve]|h|r",
		}, -- [15]
		{
			["ID"] = 159644,
			["Level"] = 125,
			["Link"] = "|cffa335ee|Hitem:159644:5965:168636::::::50:70::35:7:6532:6578:6579:6562:1538:4786:6514::::|h[Geti'ikku, Cut of Death]|h|r",
		}, -- [16]
		{
			["ID"] = 159663,
			["Level"] = 130,
			["Link"] = "|cffa335ee|Hitem:159663::::::::50:65::35:7:6536:6578:6579:6547:6513:1543:4786::::|h[G0-4W4Y Crowd Repeller]|h|r",
		}, -- [17]
	},
	["AutoSelectScales"] = false,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Thornna-Sargeras",
	["LastKeybindingsSet"] = 1,
	["Artifacts"] = {
		[128823] = {
			["Relics"] = {
				{
					["ItemLevel"] = 20,
					["Type"] = "Holy",
				}, -- [1]
				{
					["ItemLevel"] = 12,
					["Type"] = "Life",
				}, -- [2]
				{
					["ItemLevel"] = 16,
					["Type"] = "Holy",
				}, -- [3]
			},
			["Name"] = "The Silver Hand",
		},
		[120978] = {
			["Relics"] = {
				{
					["ItemLevel"] = 24,
					["Type"] = "Holy",
				}, -- [1]
				{
					["ItemLevel"] = 17,
					["Type"] = "Fire",
				}, -- [2]
				{
					["ItemLevel"] = 17,
					["Type"] = "Holy",
				}, -- [3]
			},
			["Name"] = "Ashbringer",
		},
	},
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PALADIN",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
