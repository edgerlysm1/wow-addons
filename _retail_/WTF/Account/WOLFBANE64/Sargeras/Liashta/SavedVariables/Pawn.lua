
PawnOptions = {
	["LastVersion"] = 2.0214,
	["LastPlayerFullName"] = "Liashta-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DEATHKNIGHT",
	["LastAdded"] = 1,
}
