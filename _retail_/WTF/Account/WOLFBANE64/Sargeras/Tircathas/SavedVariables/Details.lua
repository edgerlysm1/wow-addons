
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 68168,
							["totalabsorbed"] = 0.008844,
							["monster"] = true,
							["damage_from"] = {
							},
							["targets"] = {
								["Tircathas"] = 1039,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 1039.008844,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 1039.008844,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3024-1949-23611-102592-0000DC1C51",
							["nome"] = "Alliance Infantry",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 363,
										["g_amt"] = 0,
										["n_max"] = 259,
										["targets"] = {
											["Tircathas"] = 1039,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 676,
										["n_min"] = 192,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 1039,
										["c_max"] = 363,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 363,
									}, -- [1]
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1574706566,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.008844,
							["start_time"] = 1574706558,
							["delay"] = 0,
							["last_event"] = 1574706564,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002273,
							["damage_from"] = {
								["Alliance Infantry"] = true,
							},
							["targets"] = {
							},
							["on_hold"] = false,
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.002273,
							["spec"] = 70,
							["dps_started"] = false,
							["total"] = 0.002273,
							["classe"] = "PALADIN",
							["serial"] = "Player-76-0A4279C2",
							["nome"] = "Tircathas",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["grupo"] = true,
							["end_time"] = 1574706566,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 1039.002273,
							["start_time"] = 1574706566,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 4,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 4,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 4,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 70,
							["grupo"] = true,
							["buff_uptime"] = 0,
							["nome"] = "Tircathas",
							["pets"] = {
							},
							["last_event"] = 1574706558,
							["classe"] = "PALADIN",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186403] = {
										["activedamt"] = 1,
										["id"] = 186403,
										["targets"] = {
										},
										["actived_at"] = 1574706558,
										["uptime"] = 0,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = true,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0A4279C2",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Tircathas"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1574706558,
				["enemy"] = "Alliance Infantry",
				["combat_counter"] = 8,
				["playing_solo"] = true,
				["totals"] = {
					1038.990244000001, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 10971.666,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "13:29:19",
				["end_time"] = 10971.666,
				["totals_grupo"] = {
					0, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 4,
				["TotalElapsedCombatTime"] = 10971.666,
				["frags_need_refresh"] = false,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Tircathas"] = 0.002273,
						}, -- [1]
					},
				},
				["frags"] = {
				},
				["data_fim"] = "13:29:26",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 10963.982,
				["contra"] = "Alliance Infantry",
				["TimeData"] = {
				},
			}, -- [1]
		},
	},
	["last_version"] = "v8.2.5.7227",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1576002251,
		["last_version"] = 11,
	},
	["last_instance_id"] = 1949,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 1574706268,
	["active_profile"] = "Tircathas-Sargeras",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "25",
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 4,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 8,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.009742,
					["damage_from"] = {
						["Alliance Infantry"] = true,
					},
					["targets"] = {
						["Target Dummy"] = 55455,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 55455.009742,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["dps_started"] = false,
					["total"] = 55455.009742,
					["classe"] = "PALADIN",
					["end_time"] = 1574706431,
					["nome"] = "Tircathas",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 13,
								["b_amt"] = 0,
								["c_dmg"] = 12104,
								["g_amt"] = 0,
								["n_max"] = 478,
								["targets"] = {
									["Target Dummy"] = 26546,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 14442,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 44,
								["total"] = 26546,
								["c_max"] = 944,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 31,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[224266] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 6283,
								["g_amt"] = 0,
								["n_max"] = 1571,
								["targets"] = {
									["Target Dummy"] = 12566,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 6283,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 12566,
								["c_max"] = 3142,
								["id"] = 224266,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[184575] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 1643,
								["g_amt"] = 0,
								["n_max"] = 822,
								["targets"] = {
									["Target Dummy"] = 4929,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3286,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 4929,
								["c_max"] = 1643,
								["id"] = 184575,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 4,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[20271] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 2954,
								["g_amt"] = 0,
								["n_max"] = 739,
								["targets"] = {
									["Target Dummy"] = 6646,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3692,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 6646,
								["c_max"] = 1477,
								["id"] = 20271,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[35395] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 867,
								["g_amt"] = 0,
								["n_max"] = 434,
								["targets"] = {
									["Target Dummy"] = 4768,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 3901,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 10,
								["total"] = 4768,
								["c_max"] = 867,
								["id"] = 35395,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 9,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["on_hold"] = false,
					["spec"] = 70,
					["serial"] = "Player-76-0A4279C2",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1574706274,
					["delay"] = 0,
					["damage_taken"] = 1039.009742,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.012394,
					["damage_from"] = {
						["Tircathas"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.012394,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1574706431,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["nome"] = "Target Dummy",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["friendlyfire_total"] = 0,
					["total"] = 0.012394,
					["serial"] = "Creature-0-3024-1949-23611-107104-00005C1C68",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 55455.01239400001,
					["start_time"] = 1574706428,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.010217,
					["on_hold"] = false,
					["damage_from"] = {
					},
					["targets"] = {
						["Tircathas"] = 1039,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 1039.010217,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1574706566,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Alliance Infantry",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 363,
								["g_amt"] = 0,
								["n_max"] = 259,
								["targets"] = {
									["Tircathas"] = 1039,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 676,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 4,
								["total"] = 1039,
								["c_max"] = 363,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
						},
					},
					["monster"] = true,
					["total"] = 1039.010217,
					["serial"] = "Creature-0-3024-1949-23611-102592-0000DC1C51",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 0.010217,
					["start_time"] = 1574706555,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [3]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 0.018268,
					["resource"] = 20.018268,
					["targets"] = {
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "PALADIN",
					["passiveover"] = 0.008434,
					["resource_type"] = 9,
					["total"] = 0.018268,
					["tipo"] = 3,
					["nome"] = "Tircathas",
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
						},
					},
					["grupo"] = true,
					["totalover"] = 0.008434,
					["flag_original"] = 1297,
					["alternatepower"] = 0.018268,
					["last_event"] = 0,
					["spec"] = 70,
					["serial"] = "Player-76-0A4279C2",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["nome"] = "Tircathas",
					["buff_uptime_targets"] = {
					},
					["spec"] = 70,
					["grupo"] = true,
					["pets"] = {
					},
					["classe"] = "PALADIN",
					["buff_uptime"] = 154,
					["tipo"] = 4,
					["spell_cast"] = {
						[20271] = 6,
						[184575] = 5,
						[85256] = 6,
						[35395] = 10,
					},
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[186403] = {
								["actived_at"] = 1574706558,
								["refreshamt"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 186403,
								["uptime"] = 154,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["serial"] = "Player-76-0A4279C2",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1574706310,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 4,
		["totals"] = {
			252613.0505819999, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0.009833999999999999,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "13:25:10",
		["end_time"] = 10971.666,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 7.684000000001106,
				["type"] = 0,
				["name"] = "Alliance Infantry",
				["clock"] = "13:29:19",
			}, -- [1]
			{
				["elapsed"] = 34.34000000000015,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "13:27:15",
			}, -- [2]
			{
				["elapsed"] = 120.6569999999992,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "13:25:10",
			}, -- [3]
		},
		["totals_grupo"] = {
			55455.008606, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0.009833999999999999,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "13:29:26",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 10808.985,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -12.80685424804688,
					["x"] = 494.9241333007813,
					["w"] = 310,
					["h"] = 157.9999847412109,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A4279C2"] = 70,
	},
}
