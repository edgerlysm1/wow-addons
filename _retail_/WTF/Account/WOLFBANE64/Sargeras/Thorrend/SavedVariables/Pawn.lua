
PawnOptions = {
	["LastVersion"] = 2.0402,
	["ItemLevels"] = {
		{
			["ID"] = 167778,
			["Level"] = 71,
			["Link"] = "|cffa335ee|Hitem:167778::::::::50:71:::6:6296:6300:1472:4786:5414:4775::::|h[Zanj'ir Scaleguard Faceguard]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 112,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::50:71::11:4:4932:4933:6316:1526::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 157996,
			["Level"] = 83,
			["Link"] = "|cff0070dd|Hitem:157996::::::::50:71::25:4:1492:4785:6259:4775::::|h[Harbormaster Pauldrons]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 170528,
			["Level"] = 71,
			["Link"] = "|cffa335ee|Hitem:170528::::::::50:71::11:4:1480:4786:5413:4775::::|h[7th Legionnaire's Chestguard]|h|r",
		}, -- [5]
		{
			["ID"] = 157901,
			["Level"] = 76,
			["Link"] = "|cff0070dd|Hitem:157901::::::::50:71::25:6:4803:40:6516:6515:1490:4785::::|h[Bone-Fetished Greatbelt]|h|r",
		}, -- [6]
		{
			["ID"] = 157931,
			["Level"] = 76,
			["Link"] = "|cff0070dd|Hitem:157931::::::::50:71::25:5:4803:6516:6515:1490:4785::::|h[Wargreaves of Rezan's Bladeguard]|h|r",
		}, -- [7]
		{
			["ID"] = 170374,
			["Level"] = 72,
			["Link"] = "|cffa335ee|Hitem:170374::::::::50:71:::4:6300:6289:1478:4786::::|h[Zanj'ir Scaleguard Waders]|h|r",
		}, -- [8]
		{
			["ID"] = 157998,
			["Level"] = 76,
			["Link"] = "|cff0070dd|Hitem:157998::::::::50:71::25:6:4803:42:6516:6515:1490:4785::::|h[Harbormaster Wristplates]|h|r",
		}, -- [9]
		{
			["ID"] = 167777,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:167777::::::::50:71:::2:6288:6300::::|h[Zanj'ir Scaleguard Crushers]|h|r",
		}, -- [10]
		{
			["ID"] = 158161,
			["Level"] = 78,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:158161::::::::50:71::26:6:4803:4802:6516:6515:1492:4785::::|h[Spearfisher's Band]|h|r",
		}, -- [11]
		{
			["ID"] = 165683,
			["Level"] = 78,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff0070dd|Hitem:165683::::::::50:71::26:5:4803:6516:6513:1492:4785::::|h[Seal of Dath'Remar]|h|r",
		}, -- [12]
		{
			["ID"] = 158164,
			["Level"] = 74,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:158164::::::::50:71::26:5:4803:4802:6513:1488:4785::::|h[Plunderbeard's Flask]|h|r",
		}, -- [13]
		{
			["ID"] = 158220,
			["Level"] = 51,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff1eff00|Hitem:158220::::::::50:71::11:1:4790:1:9:46:::|h[Cooper's Horseshoe]|h|r",
		}, -- [14]
		{
			["ID"] = 168603,
			["Level"] = 78,
			["Link"] = "|cffa335ee|Hitem:168603::::::::50:71::3:3:4798:1478:4786::::|h[Cloak of Restless Spirits]|h|r",
		}, -- [15]
		{
			["ID"] = 170293,
			["Level"] = 72,
			["Link"] = "|cffa335ee|Hitem:170293::::::::50:71::13:1:1683::::|h[Uncanny Combatant's Deckpounder of the Peerless]|h|r",
		}, -- [16]
	},
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Thorrend-Sargeras",
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "WARRIOR",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
