
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001315,
							["total"] = 10.001315,
							["damage_from"] = {
							},
							["targets"] = {
								["Rat"] = 10,
							},
							["pets"] = {
							},
							["spec"] = 102,
							["colocacao"] = 1,
							["aID"] = "76-0A48B351",
							["raid_targets"] = {
							},
							["total_without_pet"] = 10.001315,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1604523446,
							["friendlyfire_total"] = 0,
							["classe"] = "DRUID",
							["nome"] = "Thorpawz",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[190984] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 10,
										["targets"] = {
											["Rat"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 10,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 10,
										["c_max"] = 0,
										["spellschool"] = 8,
										["id"] = 190984,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A48B351",
							["last_dps"] = 66.23387417305352,
							["custom"] = 0,
							["last_event"] = 1604523445,
							["damage_taken"] = 0.001315,
							["start_time"] = 1604523445,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00627,
							["classe"] = "UNKNOW",
							["damage_from"] = {
								["Thorpawz"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.00627,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["total"] = 0.00627,
							["aID"] = "4075",
							["serial"] = "Creature-0-3023-654-66-4075-0000231016",
							["nome"] = "Rat",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["end_time"] = 1604523446,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 10.00627,
							["start_time"] = 1604523446,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 4,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 4,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 4,
					["tipo"] = 9,
					["_ActorTable"] = {
					},
				}, -- [4]
				{
					["combatId"] = 4,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpawz"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1604523445,
				["enemy"] = "Rat",
				["combat_counter"] = 13,
				["playing_solo"] = true,
				["totals"] = {
					10, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 30993.368,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "15:57:26",
				["end_time"] = 30993.368,
				["totals_grupo"] = {
					10, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 4,
				["TotalElapsedCombatTime"] = 30993.368,
				["frags_need_refresh"] = true,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpawz"] = 10.001315,
						}, -- [1]
					},
				},
				["frags"] = {
					["Rat"] = 1,
				},
				["data_fim"] = "15:57:27",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 30992.273,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004306,
							["damage_from"] = {
							},
							["targets"] = {
								["Rat"] = 9,
							},
							["pets"] = {
							},
							["total"] = 9.004306,
							["on_hold"] = false,
							["classe"] = "DRUID",
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.004306,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1604523429,
							["aID"] = "76-0A48B351",
							["friendlyfire_total"] = 0,
							["nome"] = "Thorpawz",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[190984] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 9,
										["targets"] = {
											["Rat"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 9,
										["c_max"] = 0,
										["spellschool"] = 8,
										["id"] = 190984,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 102,
							["serial"] = "Player-76-0A48B351",
							["last_dps"] = 47.14296335105831,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.004306,
							["start_time"] = 1604523428,
							["delay"] = 0,
							["last_event"] = 1604523428,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005582,
							["damage_from"] = {
								["Thorpawz"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["aID"] = "4075",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.005582,
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1604523429,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Rat",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["friendlyfire"] = {
							},
							["total"] = 0.005582,
							["serial"] = "Creature-0-3023-654-66-4075-0012A2C668",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 9.005582,
							["start_time"] = 1604523429,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 3,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 3,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 3,
					["tipo"] = 9,
					["_ActorTable"] = {
					},
				}, -- [4]
				{
					["combatId"] = 3,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpawz"] = true,
				},
				["CombatStartedAt"] = 30990.898,
				["tempo_start"] = 1604523428,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 12,
				["playing_solo"] = true,
				["totals"] = {
					9, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					9, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["data_fim"] = "15:57:10",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rat",
				["TotalElapsedCombatTime"] = 30976.324,
				["CombatEndedAt"] = 30976.324,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorpawz"] = 9.004306,
						}, -- [1]
					},
				},
				["end_time"] = 30976.324,
				["combat_id"] = 3,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "15:57:09",
				["start_time"] = 30974.939,
				["TimeData"] = {
				},
				["frags"] = {
					["Rat"] = 1,
				},
			}, -- [2]
			{
				{
					["tipo"] = 2,
					["combatId"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002442,
							["total"] = 9.002442,
							["damage_from"] = {
							},
							["targets"] = {
								["Rat"] = 9,
							},
							["pets"] = {
							},
							["friendlyfire_total"] = 0,
							["last_dps"] = 8.445067542222645,
							["classe"] = "DRUID",
							["raid_targets"] = {
							},
							["total_without_pet"] = 9.002442,
							["tipo"] = 1,
							["delay"] = 0,
							["dps_started"] = false,
							["end_time"] = 1604521616,
							["aID"] = "76-0A48B351",
							["damage_taken"] = 0.002442,
							["nome"] = "Thorpawz",
							["spells"] = {
								["_ActorTable"] = {
									[190984] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 9,
										["targets"] = {
											["Rat"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 9,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 9,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 190984,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 1,
										["extra"] = {
										},
										["spellschool"] = 8,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["custom"] = 0,
							["spec"] = 102,
							["colocacao"] = 1,
							["last_event"] = 1604521615,
							["on_hold"] = false,
							["start_time"] = 1604521615,
							["serial"] = "Player-76-0A48B351",
							["friendlyfire"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008264,
							["damage_from"] = {
								["Thorpawz"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["classe"] = "UNKNOW",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.008264,
							["delay"] = 0,
							["fight_component"] = true,
							["total"] = 0.008264,
							["dps_started"] = false,
							["damage_taken"] = 9.008264,
							["nome"] = "Rat",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["aID"] = "4075",
							["last_dps"] = 0,
							["end_time"] = 1604521616,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1604521616,
							["serial"] = "Creature-0-3023-654-66-4075-000022FAE4",
							["friendlyfire_total"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 2,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 2,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 2,
					["_ActorTable"] = {
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorpawz"] = true,
				},
				["CombatStartedAt"] = 30974.045,
				["tempo_start"] = 1604521615,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					9, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					9, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "15:26:57",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Rat",
				["TotalElapsedCombatTime"] = 29162.839,
				["CombatEndedAt"] = 29162.839,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorpawz"] = 9.002442,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 29162.839,
				["combat_id"] = 2,
				["overall_added"] = true,
				["frags"] = {
					["Rat"] = 1,
				},
				["combat_counter"] = 10,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "15:26:55",
				["start_time"] = 29161.773,
				["TimeData"] = {
				},
				["spells_cast_timeline"] = {
				},
			}, -- [3]
		},
	},
	["last_version"] = "v9.0.1.7938",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1605817580,
		["last_version"] = 12,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Default",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["showamount"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["last_section_selected"] = "main",
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "04",
	["cached_talents"] = {
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 143,
	["combat_id"] = 4,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 13,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.012816,
					["damage_from"] = {
					},
					["targets"] = {
						["Rat"] = 28,
					},
					["end_time"] = 1604521617,
					["pets"] = {
					},
					["delay"] = 0,
					["classe"] = "DRUID",
					["raid_targets"] = {
					},
					["total_without_pet"] = 28.012816,
					["on_hold"] = false,
					["damage_taken"] = 0.012816,
					["dps_started"] = false,
					["total"] = 28.012816,
					["friendlyfire_total"] = 0,
					["aID"] = "76-0A48B351",
					["nome"] = "Thorpawz",
					["spec"] = 102,
					["grupo"] = true,
					["last_dps"] = 0,
					["tipo"] = 1,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1604521611,
					["serial"] = "Player-76-0A48B351",
					["spells"] = {
						["_ActorTable"] = {
							[190984] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 10,
								["targets"] = {
									["Rat"] = 28,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 28,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 28,
								["c_max"] = 0,
								["id"] = 190984,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["a_dmg"] = 0,
								["extra"] = {
								},
							},
						},
						["tipo"] = 2,
					},
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.024295,
					["damage_from"] = {
						["Thorpawz"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.024295,
					["tipo"] = 1,
					["last_dps"] = 0,
					["dps_started"] = false,
					["total"] = 0.024295,
					["classe"] = "UNKNOW",
					["aID"] = "4075",
					["nome"] = "Rat",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 2,
					},
					["delay"] = 0,
					["fight_component"] = true,
					["end_time"] = 1604521617,
					["damage_taken"] = 28.024295,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1604521614,
					["serial"] = "Creature-0-3023-654-66-4075-000022FAE4",
					["on_hold"] = false,
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1604521615,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["cleu_timeline"] = {
		},
		["combat_counter"] = 9,
		["totals"] = {
			28.028179, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
			["voidzone_damage"] = 0,
			["frags_total"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "15:26:55",
		["end_time"] = 30993.368,
		["totals_grupo"] = {
			28.008063, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[6] = 0,
				[3] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["dead"] = 0,
				["cc_break"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["dispell"] = 0,
				["cooldowns_defensive"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
		},
		["segments_added"] = {
			{
				["elapsed"] = 1.095000000001164,
				["type"] = 0,
				["name"] = "Rat",
				["clock"] = "15:57:26",
			}, -- [1]
			{
				["elapsed"] = 1.384999999998399,
				["type"] = 0,
				["name"] = "Rat",
				["clock"] = "15:57:09",
			}, -- [2]
			{
				["elapsed"] = 1.065999999998894,
				["type"] = 0,
				["name"] = "Rat",
				["clock"] = "15:26:55",
			}, -- [3]
		},
		["hasSaved"] = true,
		["spells_cast_timeline"] = {
		},
		["data_fim"] = "15:57:27",
		["overall_enemy_name"] = "Rat",
		["CombatSkillCache"] = {
		},
		["frags"] = {
		},
		["start_time"] = 30989.822,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["cleu_events"] = {
			["n"] = 1,
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[2] = 2,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 192.2528076171875,
					["x"] = -1231.001129150391,
					["w"] = 264.3998718261719,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = true,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
				[4] = 1,
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 51.65283203125,
					["x"] = -1231.001129150391,
					["w"] = 264.3998718261719,
					["h"] = 120.6000289916992,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [2]
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["isLocked"] = false,
			["is_open"] = false,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -147.2000732421875,
					["x"] = -238.400390625,
					["w"] = 320.0000305175781,
					["h"] = 130.0000610351563,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [3]
	},
	["character_data"] = {
		["logons"] = 3,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A48B351"] = 102,
	},
}
