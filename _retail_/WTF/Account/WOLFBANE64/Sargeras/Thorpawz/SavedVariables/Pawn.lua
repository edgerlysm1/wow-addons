
PawnOptions = {
	["LastVersion"] = 2.0404,
	["LastPlayerFullName"] = "Thorpawz-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		[7] = {
			["ID"] = 49566,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:49566::::::::1:102:::::::|h[Gilnean Novice's Pants]|h|r",
		},
		[8] = {
			["ID"] = 49564,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:49564::::::::1:102:::::::|h[Gilnean Novice's Boots]|h|r",
		},
		[10] = {
			["ID"] = 49565,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:49565::::::::1:102:::::::|h[Gilnean Novice's Gloves]|h|r",
		},
		[5] = {
			["ID"] = 49563,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:49563::::::::1:102:::::::|h[Gilnean Novice's Tunic]|h|r",
		},
		[16] = {
			["ID"] = 35,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:35::::::::1:102:::::::|h[Bent Staff]|h|r",
		},
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DRUID",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
