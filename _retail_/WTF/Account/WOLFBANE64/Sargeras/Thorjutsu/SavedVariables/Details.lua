
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 14,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00464,
							["damage_from"] = {
								["Jaomin Ro"] = true,
							},
							["targets"] = {
								["Jaomin Ro"] = 143,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 143.00464,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1578764474,
							["friendlyfire_total"] = 0,
							["spec"] = 269,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 39,
										["g_amt"] = 0,
										["n_max"] = 11,
										["targets"] = {
											["Jaomin Ro"] = 113,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 74,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 14,
										["total"] = 113,
										["c_max"] = 20,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 11,
										["r_amt"] = 0,
										["c_min"] = 19,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Jaomin Ro"] = 30,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 30,
										["n_min"] = 6,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 30,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 143.00464,
							["serial"] = "Player-76-0A48B31A",
							["last_dps"] = 7.460981895966882,
							["custom"] = 0,
							["last_event"] = 1578764473,
							["damage_taken"] = 34.00464,
							["start_time"] = 1578764455,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005595,
							["damage_from"] = {
								["Thorjutsu"] = true,
							},
							["targets"] = {
								["Thorjutsu"] = 34,
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.005595,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 34.005595,
							["classe"] = "UNKNOW",
							["serial"] = "Creature-0-3020-860-47-54611-000014A0C9",
							["nome"] = "Jaomin Ro",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorjutsu"] = 18,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[108937] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Thorjutsu"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 108937,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["friendlyfire"] = {
							},
							["end_time"] = 1578764474,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 143.005595,
							["start_time"] = 1578764455,
							["delay"] = 0,
							["last_event"] = 1578764471,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 14,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 14,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 0.004696,
							["resource"] = 5.004696,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.004696,
							["total"] = 0.004696,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
								},
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.004696,
							["last_event"] = 1578764471,
							["spec"] = 269,
							["tipo"] = 3,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.004696,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 14,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["classe"] = "MONK",
							["tipo"] = 4,
							["spell_cast"] = {
								[100780] = 5,
							},
							["serial"] = "Player-76-0A48B31A",
							["last_event"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["nome"] = "Jaomin Ro",
							["pets"] = {
							},
							["tipo"] = 4,
							["fight_component"] = true,
							["last_event"] = 0,
							["spell_cast"] = {
								[108938] = 1,
								[108937] = 1,
							},
							["serial"] = "Creature-0-3020-860-47-54611-000014A0C9",
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [4]
				{
					["combatId"] = 14,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 5984.432,
				["tempo_start"] = 1578764455,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 19,
				["playing_solo"] = true,
				["totals"] = {
					176.989091, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					143, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["data_fim"] = "12:41:15",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Jaomin Ro",
				["TotalElapsedCombatTime"] = 19.16700000000037,
				["CombatEndedAt"] = 6003.599,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorjutsu"] = 143.00464,
						}, -- [1]
					},
				},
				["end_time"] = 6003.599,
				["combat_id"] = 14,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "12:40:56",
				["start_time"] = 5984.432,
				["TimeData"] = {
				},
				["frags"] = {
				},
			}, -- [1]
			{
				{
					["tipo"] = 2,
					["combatId"] = 13,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006003,
							["damage_from"] = {
								["Environment (Falling)"] = true,
								["Tushui Trainee"] = true,
							},
							["targets"] = {
								["Tushui Trainee"] = 29,
							},
							["pets"] = {
							},
							["delay"] = 0,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 29.006003,
							["custom"] = 0,
							["tipo"] = 1,
							["dps_started"] = false,
							["total"] = 29.006003,
							["classe"] = "MONK",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Tushui Trainee"] = 19,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 19,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 19,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["spellschool"] = 1,
										["MISS"] = 2,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Tushui Trainee"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["end_time"] = 1578763882,
							["damage_taken"] = 22.006003,
							["last_dps"] = 3.28233597374692,
							["colocacao"] = 1,
							["last_event"] = 1578763879,
							["friendlyfire"] = {
							},
							["start_time"] = 1578763873,
							["serial"] = "Player-76-0A48B31A",
							["on_hold"] = false,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.00449,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Tiengwu"] = true,
							},
							["targets"] = {
								["Thorjutsu"] = 11,
								["Tiengwu"] = 7,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 18.00449,
							["last_event"] = 1578763879,
							["fight_component"] = true,
							["total"] = 18.00449,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Tushui Trainee",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Thorjutsu"] = 11,
											["Tiengwu"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 18,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 59.00449,
							["end_time"] = 1578763882,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1578763873,
							["serial"] = "Creature-0-3020-860-47-54587-00001A0647",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 13,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 13,
					["_ActorTable"] = {
						{
							["received"] = 0.008535,
							["resource"] = 0.008535,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.008535,
							["total"] = 0.008535,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 0.008535,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1578763879,
							["spec"] = 269,
							["alternatepower"] = 0.008535,
							["serial"] = "Player-76-0A48B31A",
							["resource_type"] = 12,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 13,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[100780] = 3,
							},
							["classe"] = "MONK",
							["last_event"] = 0,
							["serial"] = "Player-76-0A48B31A",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["nome"] = "Tushui Trainee",
							["spell_cast"] = {
								[109080] = 1,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["tipo"] = 4,
							["last_event"] = 0,
							["serial"] = "Creature-0-3020-860-47-54587-00001A0647",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 13,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 5401.608,
				["tempo_start"] = 1578763873,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					46.92739500000003, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.014405,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
					["Thorjutsu"] = {
						{
							true, -- [1]
							3, -- [2]
							11, -- [3]
							1578764394.943, -- [4]
							313, -- [5]
							"Environment (Falling)", -- [6]
							nil, -- [7]
							3, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:31:22",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Tushui Trainee",
				["TotalElapsedCombatTime"] = 8.836999999999534,
				["CombatEndedAt"] = 5410.445,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:31:13",
				["end_time"] = 5410.445,
				["combat_id"] = 13,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
				},
				["combat_counter"] = 17,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					29, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 5401.608,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorjutsu"] = 29.006003,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [2]
			{
				{
					["tipo"] = 2,
					["combatId"] = 12,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002754,
							["damage_from"] = {
								["Tushui Trainee"] = true,
							},
							["targets"] = {
								["Tushui Trainee"] = 40,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 40.002754,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["total"] = 40.002754,
							["damage_taken"] = 15.002754,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 5,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Tushui Trainee"] = 31,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 26,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 31,
										["c_max"] = 5,
										["id"] = 1,
										["r_dmg"] = 0,
										["c_min"] = 5,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Tushui Trainee"] = 9,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 9,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 9,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["last_dps"] = 4.174345612021449,
							["end_time"] = 1578763866,
							["colocacao"] = 1,
							["last_event"] = 1578763864,
							["on_hold"] = false,
							["start_time"] = 1578763856,
							["serial"] = "Player-76-0A48B31A",
							["friendlyfire"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008445,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Tiengwu"] = true,
							},
							["targets"] = {
								["Thorjutsu"] = 15,
								["Tiengwu"] = 19,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 34.008445,
							["last_event"] = 1578763871,
							["fight_component"] = true,
							["total"] = 34.008445,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Tushui Trainee",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorjutsu"] = 15,
											["Tiengwu"] = 19,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 34,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 13,
										["total"] = 34,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 12,
										["spellschool"] = 1,
										["DODGE"] = 1,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 101.008445,
							["end_time"] = 1578763873,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1578763856,
							["serial"] = "Creature-0-3020-860-47-54587-00001A064D",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 12,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 12,
					["_ActorTable"] = {
						{
							["received"] = 0.005272,
							["resource"] = 0.005272,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.005272,
							["total"] = 0.005272,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 0.005272,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1578763863,
							["spec"] = 269,
							["alternatepower"] = 0.005272,
							["serial"] = "Player-76-0A48B31A",
							["resource_type"] = 12,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 12,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[100780] = 2,
							},
							["classe"] = "MONK",
							["last_event"] = 0,
							["serial"] = "Player-76-0A48B31A",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["nome"] = "Tushui Trainee",
							["spell_cast"] = {
								[109080] = 1,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["tipo"] = 4,
							["last_event"] = 0,
							["serial"] = "Creature-0-3020-860-47-54587-00001A064D",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 12,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 5385.141000000001,
				["tempo_start"] = 1578763856,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					73.992842, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.002681,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:31:06",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Tushui Trainee",
				["TotalElapsedCombatTime"] = 9.582999999999629,
				["CombatEndedAt"] = 5394.724,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:30:57",
				["end_time"] = 5394.724,
				["combat_id"] = 12,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
				},
				["combat_counter"] = 16,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					40, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 5385.141000000001,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorjutsu"] = 40.002754,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [3]
			{
				{
					["tipo"] = 2,
					["combatId"] = 11,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007662,
							["damage_from"] = {
								["Tushui Trainee"] = true,
							},
							["targets"] = {
								["Tushui Trainee"] = 101,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 101.007662,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["total"] = 101.007662,
							["damage_taken"] = 37.007662,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Tushui Trainee"] = 79,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 79,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 22,
										["total"] = 79,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 20,
										["spellschool"] = 1,
										["MISS"] = 2,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Tushui Trainee"] = 22,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 22,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["last_dps"] = 3.237217550157112,
							["end_time"] = 1578763849,
							["colocacao"] = 1,
							["last_event"] = 1578763848,
							["on_hold"] = false,
							["start_time"] = 1578763818,
							["serial"] = "Player-76-0A48B31A",
							["friendlyfire"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.006396,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Wohang-Magtheridon"] = true,
								["Tiengwu"] = true,
							},
							["targets"] = {
								["Thorjutsu"] = 37,
								["Wohang-Magtheridon"] = 31,
								["Tiengwu"] = 17,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 85.006396,
							["last_event"] = 1578763851,
							["fight_component"] = true,
							["total"] = 85.006396,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Tushui Trainee",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorjutsu"] = 37,
											["Wohang-Magtheridon"] = 31,
											["Tiengwu"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 85,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 33,
										["total"] = 85,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 32,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									[109080] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Wohang-Magtheridon"] = 0,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 0,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 109080,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["PARRY"] = 1,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["spellschool"] = 1,
										["a_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 317.006396,
							["end_time"] = 1578763856,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1578763818,
							["serial"] = "Creature-0-3020-860-47-65471-00001A0560",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 11,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 11,
					["_ActorTable"] = {
						{
							["received"] = 0.001085,
							["resource"] = 0.001085,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 6.001085,
							["total"] = 0.001085,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 0.001085,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1578763856,
							["spec"] = 269,
							["alternatepower"] = 0.001085,
							["serial"] = "Player-76-0A48B31A",
							["resource_type"] = 12,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 11,
					["_ActorTable"] = {
						{
							["flag_original"] = 2600,
							["nome"] = "Tushui Trainee",
							["spell_cast"] = {
								[109080] = 5,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["tipo"] = 4,
							["last_event"] = 0,
							["serial"] = "Creature-0-3020-860-47-65471-00001A0482",
							["fight_component"] = true,
						}, -- [1]
						{
							["flag_original"] = 1297,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[107079] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 107079,
										["uptime"] = 3,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["pets"] = {
							},
							["cc_done_spells"] = {
								["_ActorTable"] = {
									[107079] = {
										["id"] = 107079,
										["targets"] = {
											["Tushui Trainee"] = 1,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["classe"] = "MONK",
							["debuff_uptime"] = 3,
							["cc_done"] = 1.005014,
							["debuff_uptime_targets"] = {
							},
							["spec"] = 269,
							["grupo"] = true,
							["cc_done_targets"] = {
								["Tushui Trainee"] = 1,
							},
							["last_event"] = 1578763830,
							["tipo"] = 4,
							["nome"] = "Thorjutsu",
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 6,
								[107079] = 1,
							},
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 11,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["tempo_start"] = 1578763818,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					185.983806, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.010607,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["totals_grupo"] = {
					101, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:30:50",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Tushui Trainee",
				["TotalElapsedCombatTime"] = 5378.155,
				["CombatEndedAt"] = 5378.155,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:30:19",
				["end_time"] = 5378.155,
				["combat_id"] = 11,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorjutsu"] = 101.007662,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["frags"] = {
				},
				["overall_added"] = true,
				["combat_counter"] = 15,
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 5346.953,
				["TimeData"] = {
				},
				["spells_cast_timeline"] = {
				},
			}, -- [4]
			{
				{
					["tipo"] = 2,
					["combatId"] = 10,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002172,
							["damage_from"] = {
								["Tushui Trainee"] = true,
							},
							["targets"] = {
								["Tushui Trainee"] = 53,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 53.002172,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["total"] = 53.002172,
							["damage_taken"] = 24.002172,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 5,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Tushui Trainee"] = 37,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 32,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 15,
										["total"] = 37,
										["c_max"] = 5,
										["c_min"] = 5,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 9,
										["spellschool"] = 1,
										["MISS"] = 5,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Tushui Trainee"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["last_dps"] = 2.905820833333223,
							["end_time"] = 1578763816,
							["colocacao"] = 1,
							["last_event"] = 1578763813,
							["on_hold"] = false,
							["start_time"] = 1578763795,
							["serial"] = "Player-76-0A48B31A",
							["friendlyfire"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002272,
							["damage_from"] = {
								["Thorjutsu"] = true,
							},
							["targets"] = {
								["Thorjutsu"] = 24,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 24.002272,
							["last_event"] = 1578763813,
							["fight_component"] = true,
							["total"] = 24.002272,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Tushui Trainee",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorjutsu"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 24,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 9,
										["total"] = 24,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 8,
										["spellschool"] = 1,
										["DODGE"] = 1,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 53.002272,
							["end_time"] = 1578763816,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1578763797,
							["serial"] = "Creature-0-3020-860-47-65471-00001A054D",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 10,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 10,
					["_ActorTable"] = {
						{
							["received"] = 0.008991,
							["resource"] = 1.008991,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.008991,
							["total"] = 0.008991,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 0.008991,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1578763818,
							["spec"] = 269,
							["alternatepower"] = 0.008991,
							["serial"] = "Player-76-0A48B31A",
							["resource_type"] = 12,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 10,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[100780] = 4,
							},
							["classe"] = "MONK",
							["last_event"] = 0,
							["serial"] = "Player-76-0A48B31A",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["nome"] = "Tushui Trainee",
							["spell_cast"] = {
								[109080] = 2,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["tipo"] = 4,
							["last_event"] = 0,
							["serial"] = "Creature-0-3020-860-47-65471-00001A054D",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 10,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 5346.953,
				["tempo_start"] = 1578763795,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					76.975463, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.003229,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:30:16",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Tushui Trainee",
				["TotalElapsedCombatTime"] = 20.54000000000087,
				["CombatEndedAt"] = 5344.534000000001,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:29:56",
				["end_time"] = 5344.534000000001,
				["combat_id"] = 10,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
					["Training Target"] = 1,
				},
				["combat_counter"] = 14,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					53, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 5323.994,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorjutsu"] = 53.002172,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [5]
			{
				{
					["tipo"] = 2,
					["combatId"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00833,
							["damage_from"] = {
								["Tushui Trainee"] = true,
							},
							["targets"] = {
								["Tushui Trainee"] = 55,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 55.00833,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["total"] = 55.00833,
							["damage_taken"] = 18.00833,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Tushui Trainee"] = 38,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 38,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 38,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 9,
										["spellschool"] = 1,
										["MISS"] = 2,
									}, -- [1]
									[100780] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 7,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Tushui Trainee"] = 17,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 17,
										["c_max"] = 7,
										["id"] = 100780,
										["r_dmg"] = 0,
										["c_min"] = 7,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["last_dps"] = 3.737740708024824,
							["end_time"] = 1578763750,
							["colocacao"] = 1,
							["last_event"] = 1578763748,
							["on_hold"] = false,
							["start_time"] = 1578763735,
							["serial"] = "Player-76-0A48B31A",
							["friendlyfire"] = {
							},
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005123,
							["damage_from"] = {
								["Thorjutsu"] = true,
							},
							["targets"] = {
								["Thorjutsu"] = 18,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 18.005123,
							["last_event"] = 1578763795,
							["fight_component"] = true,
							["total"] = 18.005123,
							["delay"] = 0,
							["classe"] = "UNKNOW",
							["nome"] = "Tushui Trainee",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Thorjutsu"] = 18,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 18,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 7,
										["spellschool"] = 1,
										["DODGE"] = 1,
									}, -- [1]
								},
								["tipo"] = 2,
							},
							["damage_taken"] = 55.005123,
							["end_time"] = 1578763795,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["on_hold"] = false,
							["start_time"] = 1578763735,
							["serial"] = "Creature-0-3020-860-47-54587-00001A027A",
							["dps_started"] = false,
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 9,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 9,
					["_ActorTable"] = {
						{
							["received"] = 0.003649,
							["resource"] = 7.003648999999999,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.003649,
							["total"] = 0.003649,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["totalover"] = 0.003649,
							["flag_original"] = 1297,
							["tipo"] = 3,
							["last_event"] = 1578763795,
							["spec"] = 269,
							["alternatepower"] = 0.003649,
							["serial"] = "Player-76-0A48B31A",
							["resource_type"] = 12,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["spell_cast"] = {
								[100780] = 4,
							},
							["classe"] = "MONK",
							["last_event"] = 0,
							["serial"] = "Player-76-0A48B31A",
							["tipo"] = 4,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["nome"] = "Tushui Trainee",
							["spell_cast"] = {
								[109080] = 1,
							},
							["pets"] = {
							},
							["classe"] = "UNKNOW",
							["tipo"] = 4,
							["last_event"] = 0,
							["serial"] = "Creature-0-3020-860-47-54587-00001A027A",
							["fight_component"] = true,
						}, -- [2]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 9,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 5264.159000000001,
				["tempo_start"] = 1578763735,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["playing_solo"] = true,
				["totals"] = {
					72.990558, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.007819,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
					["Thorjutsu"] = {
						{
							true, -- [1]
							1, -- [2]
							2, -- [3]
							1578763795.956, -- [4]
							171, -- [5]
							"Tushui Trainee", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:29:11",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Tushui Trainee",
				["TotalElapsedCombatTime"] = 14.71699999999964,
				["CombatEndedAt"] = 5278.876,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:28:56",
				["end_time"] = 5278.876,
				["combat_id"] = 9,
				["overall_added"] = true,
				["spells_cast_timeline"] = {
				},
				["frags"] = {
				},
				["combat_counter"] = 13,
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					55, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 5264.159000000001,
				["TimeData"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorjutsu"] = 55.00833,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
			}, -- [6]
			{
				{
					["tipo"] = 2,
					["combatId"] = 8,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008916,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 31,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.008916,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 31.008916,
							["friendlyfire"] = {
							},
							["last_event"] = 1577899492,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 10,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Training Target"] = 24,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 24,
										["c_max"] = 10,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 10,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Training Target"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["last_dps"] = 5.254857820708497,
							["end_time"] = 1577899492,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.008916,
							["start_time"] = 1577899487,
							["delay"] = 0,
							["classe"] = "MONK",
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.008813,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Annalisa-Destromath"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.008813,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.008813,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD32E",
							["damage_taken"] = 66.008813,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899492,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899492,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 8,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 8,
					["_ActorTable"] = {
						{
							["received"] = 0.006878,
							["resource"] = 0.006878,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.006878,
							["total"] = 0.006878,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006878,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899491,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.006878,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 8,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 2,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 8,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10702.163,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 11,
				["playing_solo"] = true,
				["totals"] = {
					30.99200000000001, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.003048,
						["alternatepower"] = 0,
						[3] = -0.003402,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					31, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:24:53",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 5.90099999999984,
				["CombatEndedAt"] = 10708.064,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:24:47",
				["end_time"] = 10708.064,
				["combat_id"] = 8,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 31.008916,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1577899487,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 10702.163,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 1,
				},
			}, -- [7]
			{
				{
					["tipo"] = 2,
					["combatId"] = 7,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006852,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 27,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 27.006852,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 27.006852,
							["on_hold"] = false,
							["last_event"] = 1577899484,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Training Target"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_amt"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Training Target"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899485,
							["last_dps"] = 4.176747912155667,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.006852,
							["start_time"] = 1577899479,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007663,
							["damage_from"] = {
								["Thorjutsu"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.007663,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.007663,
							["serial"] = "Creature-0-3782-860-17-57873-00000CD323",
							["damage_taken"] = 27.007663,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899485,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899485,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 7,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 0.005824,
							["resource"] = 0.005824,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.005824,
							["total"] = 0.005824,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.005824,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899482,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.005824,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 7,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 2,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 7,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10694.313,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 10,
				["playing_solo"] = true,
				["totals"] = {
					26.993837, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = -0.001828,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					27, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:24:45",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 6.466000000000349,
				["CombatEndedAt"] = 10700.779,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:24:39",
				["end_time"] = 10700.779,
				["combat_id"] = 7,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 27.006852,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1577899479,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 10694.313,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 1,
				},
			}, -- [8]
			{
				{
					["tipo"] = 2,
					["combatId"] = 6,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007109,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 32,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 32.007109,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 32.007109,
							["on_hold"] = false,
							["last_event"] = 1577899474,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Training Target"] = 26,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 26,
										["n_min"] = 2,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 26,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 7,
										["a_amt"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Training Target"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899474,
							["last_dps"] = 3.879649575757576,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.007109,
							["start_time"] = 1577899466,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005922,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Annalisa-Destromath"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.005922,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.005922,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD589",
							["damage_taken"] = 44.005922,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899474,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899474,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 6,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 6,
					["_ActorTable"] = {
						{
							["received"] = 0.005489,
							["resource"] = 0.005489,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.005489,
							["total"] = 0.005489,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.005489,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899472,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.005489,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 6,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 2,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 6,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["tempo_start"] = 1577899466,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 9,
				["playing_solo"] = true,
				["totals"] = {
					31.967942, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.010264,
						["alternatepower"] = 0,
						[3] = -0.003457,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:24:34",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 10689.796,
				["CombatEndedAt"] = 10689.796,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 32.007109,
						}, -- [1]
					},
				},
				["end_time"] = 10689.796,
				["combat_id"] = 6,
				["overall_added"] = true,
				["frags"] = {
					["Training Target"] = 2,
				},
				["spells_cast_timeline"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					32, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 10681.546,
				["TimeData"] = {
				},
				["data_inicio"] = "12:24:26",
			}, -- [9]
			{
				{
					["tipo"] = 2,
					["combatId"] = 5,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008647,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 35,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 35.008647,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 35.008647,
							["on_hold"] = false,
							["last_event"] = 1577899465,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 15,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Training Target"] = 29,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 14,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 29,
										["c_max"] = 10,
										["MISS"] = 2,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["b_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["c_min"] = 5,
										["successful_casted"] = 0,
										["m_amt"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Training Target"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899464,
							["last_dps"] = 4.586485916415677,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.008647,
							["start_time"] = 1577899456,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007992,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Annalisa-Destromath"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.007992,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.007992,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD543",
							["damage_taken"] = 56.007992,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899464,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899464,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 5,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 5,
					["_ActorTable"] = {
						{
							["received"] = 0.006627,
							["resource"] = 0.006627,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.006627,
							["total"] = 0.006627,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.006627,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899461,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.006627,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 5,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 1,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 5,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10680.163,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 8,
				["playing_solo"] = true,
				["totals"] = {
					34.992955, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.010016,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					35, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:24:24",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 7.516999999999825,
				["CombatEndedAt"] = 10679.713,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:24:17",
				["end_time"] = 10679.713,
				["combat_id"] = 5,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 35.008647,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1577899456,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 10672.08,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 1,
				},
			}, -- [10]
			{
				{
					["tipo"] = 2,
					["combatId"] = 4,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001127,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 32,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 32.001127,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 32.001127,
							["on_hold"] = false,
							["last_event"] = 1577899452,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Training Target"] = 25,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 25,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 25,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 6,
										["a_amt"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Training Target"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 7,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 7,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899452,
							["last_dps"] = 4.454499860802224,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.001127,
							["start_time"] = 1577899445,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007832,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Fanggung-Mannoroth"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.007832,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.007832,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD53A",
							["damage_taken"] = 67.00783200000001,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899452,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899452,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 4,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 4,
					["_ActorTable"] = {
						{
							["received"] = 0.002776,
							["resource"] = 5.002776,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.002776,
							["total"] = 0.002776,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.002776,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899456,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.002776,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 4,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 2,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 4,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10660.825,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 7,
				["playing_solo"] = true,
				["totals"] = {
					31.984016, -- [1]
					-0.007966, -- [2]
					{
						0, -- [1]
						[0] = -0.006068,
						["alternatepower"] = 0,
						[3] = -0.004179,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					32, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:24:13",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 7.183999999999287,
				["CombatEndedAt"] = 10668.009,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:24:05",
				["end_time"] = 10668.009,
				["combat_id"] = 4,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 32.001127,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1577899445,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 10660.825,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 2,
				},
			}, -- [11]
			{
				{
					["tipo"] = 2,
					["combatId"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002013,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 31,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.002013,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 31.002013,
							["on_hold"] = false,
							["last_event"] = 1577899381,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Training Target"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 21,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 21,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_amt"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Training Target"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899382,
							["last_dps"] = 2.457356769181994,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.002013,
							["start_time"] = 1577899369,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.002512,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Annalisa-Destromath"] = true,
								["Fanggung-Mannoroth"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.002512,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.002512,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD321",
							["damage_taken"] = 183.002512,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899382,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899382,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 3,
					["_ActorTable"] = {
						{
							["received"] = 0.00814,
							["resource"] = 0.00814,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.00814,
							["total"] = 0.00814,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.00814,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899378,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.00814,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[107079] = {
										["appliedamt"] = 1,
										["targets"] = {
										},
										["activedamt"] = 0,
										["uptime"] = 4,
										["id"] = 107079,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
								["tipo"] = 9,
							},
							["pets"] = {
							},
							["cc_done_spells"] = {
								["_ActorTable"] = {
									[107079] = {
										["id"] = 107079,
										["targets"] = {
											["Training Target"] = 1,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["classe"] = "MONK",
							["debuff_uptime"] = 4,
							["cc_done"] = 1.003448,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["cc_done_targets"] = {
								["Training Target"] = 1,
							},
							["last_event"] = 1577899376,
							["spell_cast"] = {
								[107079] = 1,
								[100780] = 2,
							},
							["debuff_uptime_targets"] = {
							},
							["serial"] = "Player-76-0A48B31A",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 3,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10584.602,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 6,
				["playing_solo"] = true,
				["totals"] = {
					30.961669, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.002754,
						["alternatepower"] = 0,
						[3] = -0.002268,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					31, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:23:02",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 12.61599999999999,
				["CombatEndedAt"] = 10597.218,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:22:49",
				["end_time"] = 10597.218,
				["combat_id"] = 3,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 31.002013,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1577899369,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 10584.602,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 2,
				},
			}, -- [12]
			{
				{
					["tipo"] = 2,
					["combatId"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007461,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 32,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 32.007461,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 32.007461,
							["on_hold"] = false,
							["last_event"] = 1577899353,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 8,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Training Target"] = 25,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 17,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 25,
										["c_max"] = 8,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 8,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["a_amt"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 7,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Training Target"] = 7,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 7,
										["c_max"] = 7,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 7,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899354,
							["last_dps"] = 3.895273335767049,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.007461,
							["start_time"] = 1577899345,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.005533,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Annalisa-Destromath"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.005533,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.005533,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD44C",
							["damage_taken"] = 56.005533,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899354,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899354,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 2,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 2,
					["_ActorTable"] = {
						{
							["received"] = 0.007593,
							["resource"] = 0.007593,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.007593,
							["total"] = 0.007593,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.007593,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899369,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.007593,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 1,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10561.035,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 5,
				["playing_solo"] = true,
				["totals"] = {
					31.982191, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.014236,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					32, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:22:34",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 8.217000000000553,
				["CombatEndedAt"] = 10569.252,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:22:26",
				["end_time"] = 10569.252,
				["combat_id"] = 2,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 32.007461,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["tempo_start"] = 1577899345,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["player_last_events"] = {
				},
				["start_time"] = 10561.035,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 2,
				},
			}, -- [13]
			{
				{
					["tipo"] = 2,
					["combatId"] = 1,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004148,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Target"] = 31,
							},
							["serial"] = "Player-76-0A48B31A",
							["pets"] = {
							},
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 31.004148,
							["spec"] = 269,
							["dps_started"] = false,
							["total"] = 31.004148,
							["on_hold"] = false,
							["last_event"] = 1577899343,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 8,
										["g_amt"] = 0,
										["n_max"] = 5,
										["targets"] = {
											["Training Target"] = 21,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 21,
										["c_max"] = 8,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 8,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_amt"] = 0,
										["r_amt"] = 0,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Training Target"] = 10,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 10,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 10,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["m_amt"] = 0,
										["c_min"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_amt"] = 0,
										["r_amt"] = 0,
									},
								},
								["tipo"] = 2,
							},
							["grupo"] = true,
							["end_time"] = 1577899344,
							["last_dps"] = 4.637868062827579,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.004148,
							["start_time"] = 1577899336,
							["delay"] = 0,
							["friendlyfire_total"] = 0,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.001134,
							["damage_from"] = {
								["Thorjutsu"] = true,
								["Annalisa-Destromath"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["tipo"] = 1,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.001134,
							["dps_started"] = false,
							["fight_component"] = true,
							["total"] = 0.001134,
							["serial"] = "Creature-0-3782-860-17-53714-00000CD452",
							["damage_taken"] = 57.001134,
							["nome"] = "Training Target",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 2,
							},
							["on_hold"] = false,
							["end_time"] = 1577899344,
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["friendlyfire"] = {
							},
							["start_time"] = 1577899344,
							["delay"] = 0,
							["classe"] = "UNKNOW",
						}, -- [2]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 1,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 1,
					["_ActorTable"] = {
						{
							["received"] = 0.008267,
							["resource"] = 5.008267,
							["targets"] = {
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["passiveover"] = 0.008267,
							["total"] = 0.008267,
							["nome"] = "Thorjutsu",
							["spells"] = {
								["_ActorTable"] = {
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["resource_type"] = 12,
							["flag_original"] = 1297,
							["alternatepower"] = 0.008267,
							["tipo"] = 3,
							["spec"] = 269,
							["last_event"] = 1577899343,
							["serial"] = "Player-76-0A48B31A",
							["totalover"] = 0.008267,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 1,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["nome"] = "Thorjutsu",
							["spec"] = 269,
							["grupo"] = true,
							["pets"] = {
							},
							["last_event"] = 0,
							["classe"] = "MONK",
							["tipo"] = 4,
							["serial"] = "Player-76-0A48B31A",
							["spell_cast"] = {
								[100780] = 3,
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 1,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorjutsu"] = true,
				},
				["CombatStartedAt"] = 10552.1,
				["tempo_start"] = 1577899336,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					30.991277, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = -0.008862,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["instance_type"] = "none",
				["hasSaved"] = true,
				["data_fim"] = "12:22:24",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Training Target",
				["TotalElapsedCombatTime"] = 7.468000000000757,
				["CombatEndedAt"] = 10559.568,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "12:22:17",
				["end_time"] = 10559.568,
				["combat_id"] = 1,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage_section"] = {
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage"] = {
						{
							["Thorjutsu"] = 31.004148,
						}, -- [1]
					},
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					31, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["cooldowns_defensive"] = 0,
						["dispell"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 10552.1,
				["TimeData"] = {
				},
				["frags"] = {
					["Training Target"] = 1,
				},
			}, -- [14]
		},
	},
	["last_version"] = "v8.2.5.7227",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1579195233,
		["last_version"] = 11,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorjutsu-Sargeras",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["showamount"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "11",
	["cached_talents"] = {
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 140,
	["combat_id"] = 14,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 19,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.081926,
					["damage_from"] = {
						["Jaomin Ro"] = true,
						["Tushui Trainee"] = true,
					},
					["targets"] = {
						["Jaomin Ro"] = 143,
						["Training Target"] = 251,
						["Tushui Trainee"] = 278,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 672.081926,
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 9,
								["b_amt"] = 0,
								["c_dmg"] = 90,
								["g_amt"] = 0,
								["n_max"] = 11,
								["targets"] = {
									["Jaomin Ro"] = 113,
									["Training Target"] = 192,
									["Tushui Trainee"] = 204,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 419,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 118,
								["total"] = 509,
								["c_max"] = 20,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 14,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 95,
								["a_amt"] = 0,
								["r_amt"] = 0,
							}, -- [1]
							[100780] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 14,
								["g_amt"] = 0,
								["n_max"] = 6,
								["targets"] = {
									["Jaomin Ro"] = 30,
									["Training Target"] = 59,
									["Tushui Trainee"] = 74,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 149,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 44,
								["total"] = 163,
								["c_max"] = 7,
								["id"] = 100780,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 42,
								["m_amt"] = 0,
								["r_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1577899344,
					["classe"] = "MONK",
					["serial"] = "Player-76-0A48B31A",
					["nome"] = "Thorjutsu",
					["spec"] = 269,
					["grupo"] = true,
					["total"] = 672.081926,
					["friendlyfire"] = {
					},
					["on_hold"] = false,
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 137.081926,
					["start_time"] = 1577899172,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.05532,
					["damage_from"] = {
						["Thorjutsu"] = true,
						["Annalisa-Destromath"] = true,
						["Fanggung-Mannoroth"] = true,
						["Tiengwu"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.05532,
					["friendlyfire"] = {
					},
					["fight_component"] = true,
					["end_time"] = 1577899344,
					["serial"] = "Creature-0-3782-860-17-53714-00000CD452",
					["friendlyfire_total"] = 0,
					["nome"] = "Training Target",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 2,
					},
					["on_hold"] = false,
					["total"] = 0.05532,
					["dps_started"] = false,
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 415.05532,
					["start_time"] = 1577899341,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.029696,
					["damage_from"] = {
						["Thorjutsu"] = true,
						["Tiengwu"] = true,
						["Wohang-Magtheridon"] = true,
					},
					["targets"] = {
						["Thorjutsu"] = 103,
						["Tiengwu"] = 34,
						["Wohang-Magtheridon"] = 31,
					},
					["pets"] = {
					},
					["tipo"] = 1,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 168.029696,
					["last_dps"] = 0,
					["fight_component"] = true,
					["end_time"] = 1578763751,
					["delay"] = 0,
					["friendlyfire_total"] = 0,
					["nome"] = "Tushui Trainee",
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 4,
								["targets"] = {
									["Thorjutsu"] = 103,
									["Tiengwu"] = 34,
									["Wohang-Magtheridon"] = 31,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 168,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 64,
								["total"] = 168,
								["c_max"] = 0,
								["c_min"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["b_dmg"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["a_amt"] = 0,
								["n_amt"] = 61,
								["DODGE"] = 2,
								["MISS"] = 1,
							}, -- [1]
							[109080] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Wohang-Magtheridon"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 109080,
								["r_dmg"] = 0,
								["c_min"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 1,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_dmg"] = 0,
								["a_amt"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["damage_taken"] = 531.029696,
					["total"] = 168.029696,
					["dps_started"] = false,
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["start_time"] = 1578763664,
					["serial"] = "Creature-0-3020-860-47-54587-00001A027A",
					["friendlyfire"] = {
					},
				}, -- [3]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.010624,
					["damage_from"] = {
						["Thorjutsu"] = true,
					},
					["targets"] = {
						["Thorjutsu"] = 34,
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 34.010624,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1578764475,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["nome"] = "Jaomin Ro",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Thorjutsu"] = 18,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 18,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 18,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 7,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[108937] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 16,
								["targets"] = {
									["Thorjutsu"] = 16,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 16,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 16,
								["c_max"] = 0,
								["id"] = 108937,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["friendlyfire_total"] = 0,
					["total"] = 34.010624,
					["serial"] = "Creature-0-3020-860-47-54611-000014A0C9",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 143.010624,
					["start_time"] = 1578764453,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [4]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 0.09275400000000002,
					["resource"] = 48.223474,
					["targets"] = {
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "MONK",
					["passiveover"] = 0.008932,
					["resource_type"] = 12,
					["nome"] = "Thorjutsu",
					["spells"] = {
						["_ActorTable"] = {
						},
						["tipo"] = 7,
					},
					["grupo"] = true,
					["total"] = 0.09275400000000002,
					["spec"] = 269,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.09275400000000002,
					["tipo"] = 3,
					["serial"] = "Player-76-0A48B31A",
					["totalover"] = 0.008932,
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["debuff_uptime_spells"] = {
						["_ActorTable"] = {
							[107079] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 2,
								["id"] = 107079,
								["uptime"] = 7,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["pets"] = {
					},
					["cc_done_spells"] = {
						["_ActorTable"] = {
							[107079] = {
								["id"] = 107079,
								["targets"] = {
									["Training Target"] = 1,
									["Tushui Trainee"] = 1,
								},
								["counter"] = 2,
							},
						},
						["tipo"] = 9,
					},
					["classe"] = "MONK",
					["debuff_uptime"] = 7,
					["cc_done"] = 2.014842,
					["nome"] = "Thorjutsu",
					["spec"] = 269,
					["grupo"] = true,
					["cc_done_targets"] = {
						["Training Target"] = 1,
						["Tushui Trainee"] = 1,
					},
					["tipo"] = 4,
					["spell_cast"] = {
						[100780] = 39,
						[107079] = 2,
					},
					["debuff_uptime_targets"] = {
					},
					["serial"] = "Player-76-0A48B31A",
					["last_event"] = 0,
				}, -- [1]
				{
					["fight_component"] = true,
					["nome"] = "Tushui Trainee",
					["spell_cast"] = {
						[109080] = 10,
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["tipo"] = 4,
					["classe"] = "UNKNOW",
					["serial"] = "Creature-0-3020-860-47-54587-00001A027A",
					["flag_original"] = 68136,
				}, -- [2]
				{
					["fight_component"] = true,
					["nome"] = "Jaomin Ro",
					["tipo"] = 4,
					["pets"] = {
					},
					["flag_original"] = 68136,
					["classe"] = "UNKNOW",
					["spell_cast"] = {
						[108938] = 1,
						[108937] = 1,
					},
					["serial"] = "Creature-0-3020-860-47-54611-000014A0C9",
					["last_event"] = 0,
				}, -- [3]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1577899336,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["spells_cast_timeline"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			1478.290056, -- [1]
			0.007966, -- [2]
			{
				0, -- [1]
				[0] = 0.133317,
				["alternatepower"] = 0,
				[3] = 0.011677,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "12:22:17",
		["end_time"] = 6003.599,
		["cleu_events"] = {
			["n"] = 1,
		},
		["totals_grupo"] = {
			672.0778339999999, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0.08382200000000002,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["overall_refreshed"] = true,
		["frags"] = {
		},
		["hasSaved"] = true,
		["segments_added"] = {
			{
				["elapsed"] = 19.16700000000037,
				["type"] = 0,
				["name"] = "Jaomin Ro",
				["clock"] = "12:40:56",
			}, -- [1]
			{
				["elapsed"] = 8.836999999999534,
				["type"] = 0,
				["name"] = "Tushui Trainee",
				["clock"] = "12:31:13",
			}, -- [2]
			{
				["elapsed"] = 9.582999999999629,
				["type"] = 0,
				["name"] = "Tushui Trainee",
				["clock"] = "12:30:57",
			}, -- [3]
			{
				["elapsed"] = 31.20199999999932,
				["type"] = 0,
				["name"] = "Tushui Trainee",
				["clock"] = "12:30:19",
			}, -- [4]
			{
				["elapsed"] = 20.54000000000087,
				["type"] = 0,
				["name"] = "Tushui Trainee",
				["clock"] = "12:29:56",
			}, -- [5]
			{
				["elapsed"] = 14.71699999999964,
				["type"] = 0,
				["name"] = "Tushui Trainee",
				["clock"] = "12:28:56",
			}, -- [6]
			{
				["elapsed"] = 5.90099999999984,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:24:47",
			}, -- [7]
			{
				["elapsed"] = 6.466000000000349,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:24:39",
			}, -- [8]
			{
				["elapsed"] = 8.25,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:24:26",
			}, -- [9]
			{
				["elapsed"] = 7.632999999999811,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:24:17",
			}, -- [10]
			{
				["elapsed"] = 7.183999999999287,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:24:05",
			}, -- [11]
			{
				["elapsed"] = 12.61599999999999,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:22:49",
			}, -- [12]
			{
				["elapsed"] = 8.217000000000553,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:22:26",
			}, -- [13]
			{
				["elapsed"] = 7.468000000000757,
				["type"] = 0,
				["name"] = "Training Target",
				["clock"] = "12:22:17",
			}, -- [14]
		},
		["data_fim"] = "12:41:15",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage_section"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage"] = {
			},
		},
		["start_time"] = 5835.818,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["cleu_timeline"] = {
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 15.59210205078125,
					["x"] = -545.4444580078125,
					["w"] = 310,
					["h"] = 158.0000152587891,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["character_data"] = {
		["logons"] = 3,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-154-0A4A685A"] = 253,
		["Player-76-0A48B31A"] = 269,
	},
}
