
PawnOptions = {
	["LastVersion"] = 2.0311,
	["LastPlayerFullName"] = "Thorjutsu-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 122249,
			["Level"] = 7,
			["Link"] = "|cff00ccff|Hitem:122249::::::::3:269::::::|h[Preened Tribal War Feathers]|h|r",
		}, -- [1]
		{
			["ID"] = 122666,
			["Level"] = 7,
			["Link"] = "|cff00ccff|Hitem:122666::::::::3:269:::1:582:::|h[Eternal Woven Ivy Necklace]|h|r",
		}, -- [2]
		{
			["ID"] = 122359,
			["Level"] = 7,
			["Link"] = "|cff00ccff|Hitem:122359::::::::3:269::::::|h[Preened Ironfeather Shoulders]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 122382,
			["Level"] = 7,
			["Link"] = "|cff00ccff|Hitem:122382::::::::3:269::::::|h[Preened Ironfeather Breastplate]|h|r",
		}, -- [5]
		{
			["ID"] = 73218,
			["Level"] = 2,
			["Link"] = "|cffffffff|Hitem:73218::::::::2:269::11::::|h[Initiate's Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 122255,
			["Level"] = 7,
			["Link"] = "|cff00ccff|Hitem:122255::::::::3:269::::::|h[Preened Wildfeather Leggings]|h|r",
		}, -- [7]
		nil, -- [8]
		{
			["ID"] = 77526,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:77526::::::::1:269::::::|h[Trainee's Wristwraps]|h|r",
		}, -- [9]
		nil, -- [10]
		nil, -- [11]
		nil, -- [12]
		{
			["ID"] = 122361,
			["Level"] = 7,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122361::::::::3:269:::1:5805:::|h[Swift Hand of Justice]|h|r",
		}, -- [13]
		{
			["ID"] = 122361,
			["Level"] = 6,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122361::::::::2:269:::1:5805:::|h[Swift Hand of Justice]|h|r",
		}, -- [14]
		{
			["ID"] = 122261,
			["Level"] = 7,
			["Link"] = "|cff00ccff|Hitem:122261::::::::3:269:::1:582:::|h[Inherited Cape of the Black Baron]|h|r",
		}, -- [15]
		{
			["ID"] = 122396,
			["Level"] = 7,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cff00ccff|Hitem:122396::::::::3:269::::::|h[Brawler's Razor Claws]|h|r",
		}, -- [16]
		{
			["ID"] = 122396,
			["Level"] = 6,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cff00ccff|Hitem:122396::::::::2:269::::::|h[Brawler's Razor Claws]|h|r",
		}, -- [17]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "MONK",
	["LastAdded"] = 1,
}
