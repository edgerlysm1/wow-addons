
PawnOptions = {
	["LastVersion"] = 2.033,
	["LastPlayerFullName"] = "Thorjitsu-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 168345,
			["Level"] = 450,
			["Link"] = "|cffa335ee|Hitem:168345::::::::120:270::6:5:4824:1517:4786:6270:4775:::|h[Helm of the Inexorable Tide]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 465,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::120:269::11:4:4932:4933:6316:1602:::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 168348,
			["Level"] = 445,
			["Link"] = "|cffa335ee|Hitem:168348::::::::120:270::6:4:4824:1517:4786:6271:::|h[Shoulderpads of Frothing Rage]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 168343,
			["Level"] = 450,
			["Link"] = "|cffa335ee|Hitem:168343::::::::120:270::6:5:4824:1517:4786:6269:4775:::|h[Blackwater Shimmerscale Vest]|h|r",
		}, -- [5]
		{
			["ID"] = 168379,
			["Level"] = 440,
			["Link"] = "|cffa335ee|Hitem:168379::::::::120:270::5:4:4799:1502:5855:4783:::|h[Ship Swallower's Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 159322,
			["Level"] = 440,
			["Link"] = "|cffa335ee|Hitem:159322::::::::120:269::35:3:5010:1612:4783:::|h[Seawalker's Pantaloons]|h|r",
		}, -- [7]
		{
			["ID"] = 168380,
			["Level"] = 445,
			["Link"] = "|cffa335ee|Hitem:168380::::::::120:270::6:3:4800:1517:4786:::|h[Ancient Tempest Striders]|h|r",
		}, -- [8]
		{
			["ID"] = 159308,
			["Level"] = 440,
			["Link"] = "|cffa335ee|Hitem:159308::::::::120:269::35:3:5010:1612:4783:::|h[Bracers of the Sacred Fleet]|h|r",
		}, -- [9]
		{
			["ID"] = 168376,
			["Level"] = 430,
			["Link"] = "|cffa335ee|Hitem:168376::::::::120:270::5:3:4799:1502:4786:::|h[Anglerfish Feelers]|h|r",
		}, -- [10]
		{
			["ID"] = 159463,
			["Level"] = 440,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffa335ee|Hitem:159463::::::::120:269::35:4:5010:4802:1612:4783:::|h[Loop of Pulsing Veins]|h|r",
		}, -- [11]
		{
			["ID"] = 159463,
			["Level"] = 440,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:159463::::::::120:270::35:4:5010:4802:1612:4783:::|h[Loop of Pulsing Veins]|h|r",
		}, -- [12]
		{
			["ID"] = 158320,
			["Level"] = 440,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:158320::::::::120:270::35:3:5010:1612:4783:::|h[Revitalizing Voodoo Totem]|h|r",
		}, -- [13]
		{
			["ID"] = 158320,
			["Level"] = 440,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:158320::::::::120:269::35:3:5010:1612:4783:::|h[Revitalizing Voodoo Totem]|h|r",
		}, -- [14]
		{
			["ID"] = 159287,
			["Level"] = 440,
			["Link"] = "|cffa335ee|Hitem:159287::::::::120:270::35:4:5010:4802:1612:4783:::|h[Cloak of Questionable Intent]|h|r",
		}, -- [15]
		{
			["ID"] = 159652,
			["Level"] = 440,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cffa335ee|Hitem:159652::::::::120:270::35:3:5010:1612:4783:::|h[Leaxa's Thought-Piercer]|h|r",
		}, -- [16]
		{
			["ID"] = 168477,
			["Level"] = 445,
			["Link"] = "|cffa335ee|Hitem:168477::::::::120:270::6:4:4800:1808:1517:4786:::|h[Tidebinder's Driftglobe]|h|r",
		}, -- [17]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "MONK",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
