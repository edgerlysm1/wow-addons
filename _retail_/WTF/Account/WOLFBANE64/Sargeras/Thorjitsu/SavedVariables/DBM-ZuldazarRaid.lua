
DBMZuldazarRaid_SavedStats = {
	["ZuldazarRaidTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["2323"] = {
		["mythicBestTime"] = 297.3349999999991,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 1,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["mythicLastTime"] = 297.3349999999991,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 1,
		["challengePulls"] = 0,
	},
	["2340"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["2334"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["2342"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["challengePulls"] = 0,
		["normalKills"] = 0,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["lfr25LastTime"] = 364.5579999999973,
		["lfr25BestTime"] = 364.5579999999973,
	},
	["2343"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["2330"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["challengePulls"] = 0,
		["normalKills"] = 0,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["lfr25LastTime"] = 214.4510000000009,
		["lfr25BestTime"] = 214.4510000000009,
	},
	["2335"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["challengePulls"] = 0,
		["normalKills"] = 0,
		["heroicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["normal25Kills"] = 0,
		["mythicPulls"] = 0,
		["timewalkerKills"] = 0,
		["lfr25LastTime"] = 256.5820000000022,
		["lfr25BestTime"] = 256.5820000000022,
	},
	["2337"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["2344"] = {
		["mythicBestTime"] = 100.0970000000016,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 1,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["mythicLastTime"] = 100.0970000000016,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 1,
		["challengePulls"] = 0,
	},
}
