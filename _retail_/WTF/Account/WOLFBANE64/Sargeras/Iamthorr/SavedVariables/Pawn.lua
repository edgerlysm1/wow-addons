
PawnOptions = {
	["LastVersion"] = 2.033,
	["ItemLevels"] = {
		nil, -- [1]
		{
			["ID"] = 122664,
			["Level"] = 5,
			["Link"] = "|cff00ccff|Hitem:122664::::::::1:262:::1:3592:::|h[Eternal Horizon Choker]|h|r",
		}, -- [2]
		{
			["ID"] = 122356,
			["Level"] = 5,
			["Link"] = "|cff00ccff|Hitem:122356::::::::1:262:::1:582:::|h[Champion Herod's Shoulder]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 122379,
			["Level"] = 5,
			["Link"] = "|cff00ccff|Hitem:122379::::::::1:262:::1:582:::|h[Champion's Deathdealer Breastplate]|h|r",
		}, -- [5]
		{
			["ID"] = 77509,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:77509::::::::1:262::::::|h[Trainee's Cord]|h|r",
		}, -- [6]
		{
			["ID"] = 72020,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:72020::::::::1:262::::::|h[Trainee's Leggings]|h|r",
		}, -- [7]
		nil, -- [8]
		{
			["ID"] = 77526,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:77526::::::::1:262::::::|h[Trainee's Wristwraps]|h|r",
		}, -- [9]
		nil, -- [10]
		{
			["ID"] = 128169,
			["Level"] = 5,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff00ccff|Hitem:128169::::::::1:262:::1:3592:::|h[Signet of the Third Fleet]|h|r",
		}, -- [11]
		nil, -- [12]
		{
			["ID"] = 122362,
			["Level"] = 5,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122362::::::::1:262:::1:3592:::|h[Discerning Eye of the Beast]|h|r",
		}, -- [13]
		{
			["ID"] = 122362,
			["Level"] = 3,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122362::::::::2:262:::1:3592::::|h[Discerning Eye of the Beast]|h|r",
		}, -- [14]
		{
			["ID"] = 122262,
			["Level"] = 5,
			["Link"] = "|cff00ccff|Hitem:122262::::::::1:262:::1:3592:::|h[Ancient Bloodmoon Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 122353,
			["Level"] = 5,
			["Link"] = "|cff00ccff|Hitem:122353::::::::1:262:::1:3592:::|h[Dignified Headmaster's Charge]|h|r",
		}, -- [16]
		{
			["ID"] = 76391,
			["Level"] = 1,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cffffffff|Hitem:76391::::::::1:262::::1:28:80:::|h[Trainee's Axe]|h|r",
		}, -- [17]
	},
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Iamthorr-Sargeras",
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "SHAMAN",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
