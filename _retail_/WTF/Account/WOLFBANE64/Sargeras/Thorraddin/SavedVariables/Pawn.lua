
PawnOptions = {
	["LastVersion"] = 2.0406,
	["LastPlayerFullName"] = "Thorraddin-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["ItemLevels"] = {
		{
			["ID"] = 167322,
			["Level"] = 83,
			["Link"] = "|cffa335ee|Hitem:167322::::::::50:70::56:6:6321:6348:1490:4786:6264:4775::::|h[Notorious Gladiator's Plate Headcover]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 126,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::50:70::11:4:6316:4932:4933:1540::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 173835,
			["Level"] = 90,
			["Link"] = "|cffa335ee|Hitem:173835::::::::50:70::9:4:1479:4786:6503:4775::::|h[Malignant Leviathan's Pauldrons]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 170537,
			["Level"] = 71,
			["Link"] = "|cffa335ee|Hitem:170537::::::::50:70::11:4:1480:4786:5413:4775::::|h[Honorbound Centurion's Breastplate]|h|r",
		}, -- [5]
		{
			["ID"] = 173514,
			["Level"] = 80,
			["Link"] = "|cffa335ee|Hitem:173514::::::::50:70::30:8:6412:4803:6515:6578:6579:6552:1474:4786::::|h[Malignant Leviathan's Girdle]|h|r",
		}, -- [6]
		{
			["ID"] = 164807,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:164807::::::::50:70::25:4:5252:5141:1478:4786::::|h[Sinister Gladiator's Plate Legguards]|h|r",
		}, -- [7]
		{
			["ID"] = 173455,
			["Level"] = 85,
			["Link"] = "|cffa335ee|Hitem:173455::::::::50:70::9:8:6412:6513:6578:6579:6494:6613:1479:4786::::|h[Malignant Leviathan's Stompers]|h|r",
		}, -- [8]
		{
			["ID"] = 174956,
			["Level"] = 95,
			["Link"] = "|cffa335ee|Hitem:174956::::::::50:70::28:5:6540:6578:6579:4803:6515::::|h[Malignant Leviathan's Armguards]|h|r",
		}, -- [9]
		{
			["ID"] = 173466,
			["Level"] = 82,
			["Link"] = "|cff0070dd|Hitem:173466::::::::50:70::29:6:6412:4803:6513:6516:1476:4785::::|h[Malignant Leviathan's Gauntlets]|h|r",
		}, -- [10]
		{
			["ID"] = 167524,
			["Level"] = 72,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:167524::::::::50:70::47:3:1482:5847:4781::::|h[Notorious Aspirant's Band]|h|r",
		}, -- [11]
		{
			["ID"] = 158160,
			["Level"] = 60,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:158160:5942:::::::50:70::27:3:4803:1474:4786::::|h[Smuggler's Cove Ring]|h|r",
		}, -- [12]
		{
			["ID"] = 167525,
			["Level"] = 70,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:167525::::::::50:70::47:2:1482:4785::::|h[Notorious Aspirant's Medallion]|h|r",
		}, -- [13]
		{
			["ID"] = 167526,
			["Level"] = 70,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff0070dd|Hitem:167526::::::::50:70::47:3:41:1482:4785::::|h[Notorious Aspirant's Emblem]|h|r",
		}, -- [14]
		{
			["ID"] = 169223,
			["Level"] = 133,
			["Link"] = "|cffff8000|Hitem:169223::::::::50:70::11:2:6276:1472::::|h[Ashjra'kamas, Shroud of Resolve]|h|r",
		}, -- [15]
		{
			["ID"] = 160950,
			["Level"] = 60,
			["Link"] = "|cff0070dd|Hitem:160950::::::::50:70::27:3:4803:1474:4785::::|h[Nicked Nazmani Greataxe]|h|r",
		}, -- [16]
	},
	["Artifacts"] = {
		[120978] = {
			["Relics"] = {
				{
					["ItemLevel"] = 2,
					["Type"] = "Holy",
				}, -- [1]
				{
					["Type"] = "Fire",
				}, -- [2]
			},
			["Name"] = "Ashbringer",
		},
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PALADIN",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
