
DetailsTimeLineDB = {
	["max_segments"] = 4,
	["combat_data"] = {
		{
			["date_end"] = "21:05:50",
			["date_start"] = "21:05:00",
			["name"] = "Argus the Unmaker",
			["total_time"] = 171.9850000000006,
		}, -- [1]
		{
			["date_end"] = "21:04:34",
			["date_start"] = "21:03:00",
			["name"] = "Argus the Unmaker",
			["total_time"] = 94.00299999999697,
		}, -- [2]
		{
			["date_end"] = "21:01:32",
			["date_start"] = "20:59:46",
			["name"] = "Aggramar",
			["total_time"] = 107.3859999999986,
		}, -- [3]
		{
			["date_end"] = "20:56:33",
			["date_start"] = "20:55:33",
			["name"] = "The Coven of Shivarra",
			["total_time"] = 60.60300000000279,
		}, -- [4]
	},
	["hide_on_combat"] = false,
	["IndividualSpells"] = {
		{
			[258029] = {
				{
					34.65499999999884, -- [1]
					"Apocalypsis Module", -- [2]
					258029, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[257296] = {
				{
					46.25200000000041, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[258838] = {
				{
					7.17500000000291, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Aedeath-Thrall", -- [5]
				}, -- [1]
				{
					15.71500000000378, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [2]
				{
					24.22800000000279, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [3]
				{
					33.93800000000192, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [4]
				{
					47.29499999999825, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [5]
			},
			[265754] = {
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [1]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [2]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [3]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [4]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [5]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [6]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [7]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [8]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [9]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [10]
			},
			[24331] = {
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [1]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [2]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [3]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [4]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [5]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [6]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Primal Earth Elemental", -- [5]
				}, -- [7]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Primal Earth Elemental", -- [5]
				}, -- [8]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [9]
			},
		}, -- [1]
		{
			[257296] = {
				{
					14.70300000000134, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[248499] = {
				{
					4.913000000000466, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [1]
				{
					10.98799999999756, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [2]
				{
					17.05000000000291, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [3]
			},
			[258399] = {
				{
					93.53899999999703, -- [1]
					"Argus the Unmaker", -- [2]
					258399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[256542] = {
				{
					90.5199999999968, -- [1]
					"Argus the Unmaker", -- [2]
					256542, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [2]
		{
			[245458] = {
				{
					16.21899999999732, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244912] = {
				{
					56.80599999999686, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					80.55399999999645, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					105.2730000000011, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[244903] = {
				{
					30.42399999999907, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.59300000000076, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					77.78199999999924, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					77.78199999999924, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					77.78199999999924, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					77.79699999999866, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					84.22299999999814, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[243431] = {
				{
					41.37700000000041, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					91.653999999995, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[244901] = {
				{
					23.00499999999738, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					23.00499999999738, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					77.63199999999779, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					77.63199999999779, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[245631] = {
				{
					25.01299999999901, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.01299999999901, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					79.6449999999968, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					79.6449999999968, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[244693] = {
				{
					11.73599999999715, -- [1]
					"Aggramar", -- [2]
					244693, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laegherie-Ragnaros", -- [5]
				}, -- [1]
			},
		}, -- [3]
		{
			[245303] = {
				{
					4.546000000002096, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.37000000000262, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					20.52400000000489, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					26.61699999999837, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					38.74300000000221, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					49.6820000000007, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					55.75500000000466, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[250333] = {
				{
					4.145000000004075, -- [1]
					"Diima, Mother of Gloom", -- [2]
					250333, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244899] = {
				{
					10.73300000000018, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
				{
					22.12000000000262, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [2]
				{
					33.02799999999843, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [3]
				{
					43.96700000000419, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [4]
				{
					54.88900000000285, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [5]
			},
			[250334] = {
				{
					49.54499999999825, -- [1]
					"Thu'raya, Mother of the Cosmos", -- [2]
					250334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246329] = {
				{
					11.44200000000274, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					39.81399999999849, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[252861] = {
				{
					30.69099999999889, -- [1]
					"Asara, Mother of Night", -- [2]
					252861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[253520] = {
				{
					20.40600000000268, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Rðcha-Azralon", -- [5]
				}, -- [1]
				{
					20.40600000000268, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darkneesh-Azralon", -- [5]
				}, -- [2]
				{
					20.40600000000268, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hadess-Azralon", -- [5]
				}, -- [3]
			},
			[245627] = {
				{
					9.507000000005064, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.58400000000256, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [4]
		{
			[244042] = {
				{
					24.82400000000052, -- [1]
					"Varimathras", -- [2]
					244042, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[243960] = {
				{
					9.02100000000064, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					21.17599999999948, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[248732] = {
				{
					10.1359999999986, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Piston-Area52", -- [5]
				}, -- [1]
				{
					14.6359999999986, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nohcolo-BleedingHollow", -- [5]
				}, -- [2]
				{
					15.62800000000425, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kotipëlto-Azralon", -- [5]
				}, -- [3]
				{
					20.34599999999773, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darkneesh-Azralon", -- [5]
				}, -- [4]
				{
					22.38799999999901, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Munnk-BleedingHollow", -- [5]
				}, -- [5]
				{
					24.00400000000082, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ssalah-MoonGuard", -- [5]
				}, -- [6]
				{
					26.63300000000163, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laegherie-Ragnaros", -- [5]
				}, -- [7]
			},
			[243999] = {
				{
					17.08600000000297, -- [1]
					"Varimathras", -- [2]
					243999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
			},
		}, -- [5]
		{
			[248214] = {
				{
					13.17700000000332, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					78.82100000000355, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[249686] = {
				{
					82.45300000000134, -- [1]
					"Kin'garoth", -- [2]
					249686, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nohcolo-BleedingHollow", -- [5]
				}, -- [1]
				{
					82.45300000000134, -- [1]
					"Kin'garoth", -- [2]
					249686, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Helpplzboy-Darrowmere", -- [5]
				}, -- [2]
				{
					82.45300000000134, -- [1]
					"Kin'garoth", -- [2]
					249686, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laegherie-Ragnaros", -- [5]
				}, -- [3]
				{
					82.45300000000134, -- [1]
					"Kin'garoth", -- [2]
					249686, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mongral-Nemesis", -- [5]
				}, -- [4]
				{
					82.45300000000134, -- [1]
					"Kin'garoth", -- [2]
					249686, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ssalah-MoonGuard", -- [5]
				}, -- [5]
			},
			[246516] = {
				{
					38.5679999999993, -- [1]
					"Kin'garoth", -- [2]
					246516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246833] = {
				{
					28.33400000000256, -- [1]
					"Kin'garoth", -- [2]
					246833, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ruiner", -- [5]
				}, -- [1]
			},
			[244328] = {
				{
					82.46899999999732, -- [1]
					"Detonation Charge", -- [2]
					244328, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[254926] = {
				{
					15.70399999999791, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					82.45300000000134, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246504] = {
				{
					38.68299999999726, -- [1]
					"Unknown", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.68299999999726, -- [1]
					"Unknown", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.68299999999726, -- [1]
					"Unknown", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[254795] = {
				{
					62.01699999999983, -- [1]
					"Kin'garoth", -- [2]
					254795, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[254919] = {
				{
					7.107000000003609, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					21.67799999999988, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					81.23799999999756, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [6]
		{
			[248068] = {
				{
					145.2289999999994, -- [1]
					"Imonar the Soulhunter", -- [2]
					248068, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[254244] = {
				{
					7.574000000000524, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					72.37900000000081, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[250135] = {
				{
					50.70900000000256, -- [1]
					"Imonar the Soulhunter", -- [2]
					250135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					127.2079999999987, -- [1]
					"Imonar the Soulhunter", -- [2]
					250135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[250255] = {
				{
					142.7960000000021, -- [1]
					"Imonar the Soulhunter", -- [2]
					250255, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
			},
			[248233] = {
				{
					19.11100000000442, -- [1]
					"Imonar the Soulhunter", -- [2]
					248233, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					83.52400000000489, -- [1]
					"Imonar the Soulhunter", -- [2]
					248233, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[248254] = {
				{
					42.48300000000018, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					117.7740000000049, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[247367] = {
				{
					3.930000000000291, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
				{
					8.790000000000873, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [2]
				{
					68.72400000000198, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Aedeath-Thrall", -- [5]
				}, -- [3]
				{
					73.58000000000175, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [4]
			},
			[247687] = {
				{
					39.54500000000553, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
				{
					116.0620000000054, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [2]
			},
		}, -- [7]
		{
			[250701] = {
				{
					75.3969999999972, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246753] = {
				{
					38.14699999999721, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					259.2359999999972, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					304.1809999999969, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					393.6619999999966, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[254769] = {
				{
					19.11299999999756, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					37.32599999999366, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					62.04899999999907, -- [1]
					"Fel-Powered Purifier", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					117.2089999999953, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					390.416999999994, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					410.2539999999935, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[250703] = {
				{
					199.3599999999933, -- [1]
					"Fel-Powered Purifier", -- [2]
					250703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					207.8609999999972, -- [1]
					"Fel-Powered Purifier", -- [2]
					250703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[249934] = {
				{
					105.5399999999936, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					223.0299999999988, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					308.0199999999968, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					417.7209999999977, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[246305] = {
				{
					125.288999999997, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					398.4989999999962, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					408.2029999999941, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					417.5279999999984, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[249148] = {
				{
					61.24199999999837, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					172.252999999997, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					262.903999999995, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					363.6919999999955, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[249196] = {
				{
					171.851999999999, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [8]
		{
			[244709] = {
				{
					16.09100000000035, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					17.12299999999959, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					18.11699999999837, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					19.12799999999697, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					20.12299999999959, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					24.0989999999947, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[244689] = {
				{
					7.081999999994878, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[243983] = {
				{
					17.26599999999598, -- [1]
					"Portal Keeper Hasabel", -- [2]
					243983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244000] = {
				{
					26.99099999999453, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244000, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244016] = {
				{
					7.992999999994936, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244016, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
			},
		}, -- [9]
		{
			[244722] = {
				{
					15.67700000000332, -- [1]
					"Admiral Svirax", -- [2]
					244722, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244892] = {
				{
					7.614999999997963, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.09799999999814, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					24.62400000000343, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[257262] = {
				{
					6.74199999999837, -- [1]
					"Unknown", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					6.963000000003376, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					7.283999999999651, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					7.360999999997148, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					7.885000000002037, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					16.36699999999837, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					16.77999999999884, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					17.13900000000285, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					17.42899999999645, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					17.5, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					25.52199999999721, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					25.88300000000163, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					25.91799999999785, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					26.01400000000285, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					26.25699999999779, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
			},
			[245888] = {
				{
					31.68000000000029, -- [1]
					"Antoran Champion", -- [2]
					245888, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.68000000000029, -- [1]
					"Antoran Champion", -- [2]
					245888, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					31.68000000000029, -- [1]
					"Antoran Champion", -- [2]
					245888, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[245868] = {
				{
					31.68000000000029, -- [1]
					"Antoran Doomguard", -- [2]
					245868, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.68000000000029, -- [1]
					"Antoran Doomguard", -- [2]
					245868, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [10]
		{
			[245098] = {
				{
					10.54400000000169, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
				{
					21.45300000000134, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [2]
			},
			[251445] = {
				{
					10.54400000000169, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [1]
				{
					21.45300000000134, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [2]
				{
					32.40099999999802, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [3]
			},
			[251626] = {
				{
					38.89999999999418, -- [1]
					"Clubfist Beastlord", -- [2]
					251626, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244072] = {
				{
					33.60300000000279, -- [1]
					"F'harg", -- [2]
					244072, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [11]
		{
			[246965] = {
				{
					25.75, -- [1]
					"Annihilator", -- [2]
					246965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[245237] = {
				{
					18.72899999999936, -- [1]
					"Garothi Worldbreaker", -- [2]
					245237, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.66100000000006, -- [1]
					"Garothi Worldbreaker", -- [2]
					245237, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246848] = {
				{
					18.72899999999936, -- [1]
					"Garothi Worldbreaker", -- [2]
					246848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.66100000000006, -- [1]
					"Garothi Worldbreaker", -- [2]
					246848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[244399] = {
				{
					7.805000000000291, -- [1]
					"Decimator", -- [2]
					244399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246897] = {
				{
					14.79699999999866, -- [1]
					"Decimator", -- [2]
					246897, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [12]
		{
			[87654] = {
				{
					32.88700000000245, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.88700000000245, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					96.95200000000477, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					177.3099999999977, -- [1]
					"Pulsing Twilight Egg", -- [2]
					87654, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[95855] = {
				{
					0.9919999999983702, -- [1]
					"Sinestra", -- [2]
					95855, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					231.1110000000044, -- [1]
					"Sinestra", -- [2]
					95855, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[86227] = {
				{
					14.15499999999884, -- [1]
					"Sinestra", -- [2]
					86227, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90083] = {
				{
					217.0710000000036, -- [1]
					"Twilight Drake", -- [2]
					90083, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87299] = {
				{
					6.650000000001455, -- [1]
					"Sinestra", -- [2]
					87299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87947] = {
				{
					225.2139999999999, -- [1]
					"Sinestra", -- [2]
					87947, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Calen", -- [5]
				}, -- [1]
			},
			[90028] = {
				{
					78.15000000000146, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					102.5319999999992, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					125.6470000000045, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					149.984000000004, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					197.512999999999, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					222.6540000000023, -- [1]
					"Twilight Spitecaller", -- [2]
					90028, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [13]
		{
			[82299] = {
				{
					3.097000000001572, -- [1]
					"Cho'gall", -- [2]
					82299, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[82630] = {
				{
					10.81100000000151, -- [1]
					"Cho'gall", -- [2]
					82630, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[81628] = {
				{
					5.02900000000227, -- [1]
					"Cho'gall", -- [2]
					81628, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[93301] = {
				{
					12.44499999999971, -- [1]
					"Unknown", -- [2]
					93301, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[91303] = {
				{
					7.177000000003318, -- [1]
					"Cho'gall", -- [2]
					91303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[81556] = {
				{
					5.961000000002969, -- [1]
					"Cho'gall", -- [2]
					81556, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[82524] = {
				{
					9.887000000002445, -- [1]
					"Cho'gall", -- [2]
					82524, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
		}, -- [14]
		{
			[93327] = {
				{
					11.49399999999878, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					17.57299999999668, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					23.65299999999843, -- [1]
					"Bound Rumbler", -- [2]
					93327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
			},
			[84918] = {
				{
					27.31500000000233, -- [1]
					"Elementium Monstrosity", -- [2]
					84918, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[93306] = {
				{
					21.21699999999692, -- [1]
					"Bound Zephyr", -- [2]
					93306, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					21.21699999999692, -- [1]
					"Bound Zephyr", -- [2]
					93306, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
			[93277] = {
				{
					11.49399999999878, -- [1]
					"Bound Zephyr", -- [2]
					93277, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					12.70300000000134, -- [1]
					"Bound Zephyr", -- [2]
					93277, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					17.55900000000111, -- [1]
					"Bound Zephyr", -- [2]
					93277, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					18.77300000000105, -- [1]
					"Bound Zephyr", -- [2]
					93277, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					23.65299999999843, -- [1]
					"Bound Zephyr", -- [2]
					93277, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					24.87799999999697, -- [1]
					"Bound Zephyr", -- [2]
					93277, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
			},
			[93278] = {
				{
					12.70300000000134, -- [1]
					"Bound Zephyr", -- [2]
					93278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					13.93000000000029, -- [1]
					"Bound Zephyr", -- [2]
					93278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					18.77300000000105, -- [1]
					"Bound Zephyr", -- [2]
					93278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					20.00400000000082, -- [1]
					"Bound Zephyr", -- [2]
					93278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					24.87799999999697, -- [1]
					"Bound Zephyr", -- [2]
					93278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					26.08099999999831, -- [1]
					"Bound Zephyr", -- [2]
					93278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
			},
			[85482] = {
				{
					32.85899999999674, -- [1]
					"Faceless Guardian", -- [2]
					85482, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[93340] = {
				{
					13.93000000000029, -- [1]
					"Bound Deluge", -- [2]
					93340, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.31500000000233, -- [1]
					"Bound Deluge", -- [2]
					93340, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[82285] = {
				{
					10.2699999999968, -- [1]
					"Arion", -- [2]
					82285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.2699999999968, -- [1]
					"Terrastra", -- [2]
					82285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[93325] = {
				{
					14.93099999999686, -- [1]
					"Bound Rumbler", -- [2]
					93325, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [15]
		{
			[86360] = {
				{
					0.1, -- [1]
					"Theralion", -- [2]
					86360, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [16]
		{
			[86058] = {
				{
					2.432000000000699, -- [1]
					"Proto-Behemoth", -- [2]
					86058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					4.860000000000582, -- [1]
					"Proto-Behemoth", -- [2]
					86058, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[83710] = {
				{
					4.860000000000582, -- [1]
					"Halfus Wyrmbreaker", -- [2]
					83710, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [17]
		{
			[258838] = {
				{
					10.02100000000064, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [1]
				{
					19.72400000000198, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [2]
				{
					29.4230000000025, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [3]
				{
					39.14000000000669, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [4]
				{
					48.79900000000635, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [5]
				{
					57.35700000000361, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [6]
				{
					67.06200000000536, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [7]
			},
			[258030] = {
				{
					55.96500000000378, -- [1]
					"Apocalypsis Module", -- [2]
					258030, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[258029] = {
				{
					35.80200000000332, -- [1]
					"Apocalypsis Module", -- [2]
					258029, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[257296] = {
				{
					47.78399999999965, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [18]
		{
			[255826] = {
				{
					61.20599999999831, -- [1]
					"Argus the Unmaker", -- [2]
					255826, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[256542] = {
				{
					111.4760000000024, -- [1]
					"Argus the Unmaker", -- [2]
					256542, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[257296] = {
				{
					16.22400000000198, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.59799999999814, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[258399] = {
				{
					114.487000000001, -- [1]
					"Argus the Unmaker", -- [2]
					258399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[248499] = {
				{
					6.42500000000291, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [1]
				{
					12.50699999999779, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [2]
				{
					18.60700000000361, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [3]
				{
					24.66999999999825, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [4]
				{
					30.61299999999756, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [5]
				{
					53.85199999999895, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [6]
				{
					62.33200000000215, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [7]
			},
		}, -- [19]
		{
			[245458] = {
				{
					17.33200000000215, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244912] = {
				{
					56.63799999999901, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					57.86899999999878, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					62.72500000000582, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					68.39900000000489, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					96.33400000000256, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					125.5050000000047, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					125.8080000000045, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					125.9200000000055, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					126.2300000000032, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					126.7230000000054, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					130.3060000000041, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					130.362000000001, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
			},
			[244903] = {
				{
					31.08800000000338, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.56100000000151, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.80900000000111, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					89.44800000000396, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					97.56200000000536, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[243431] = {
				{
					42.01699999999983, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					110.7890000000043, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[244901] = {
				{
					25.32700000000477, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.32700000000477, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					84.90600000000268, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					84.90600000000268, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[245631] = {
				{
					27.31999999999971, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.31999999999971, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					86.9120000000039, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					86.9120000000039, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[244693] = {
				{
					12.81900000000314, -- [1]
					"Aggramar", -- [2]
					244693, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [1]
			},
		}, -- [20]
		{
			[244899] = {
				{
					10.5280000000057, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					22.71399999999994, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [2]
				{
					33.64500000000408, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [3]
				{
					47.01400000000285, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [4]
				{
					57.97600000000239, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [5]
				{
					68.90500000000611, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [6]
				{
					82.25300000000425, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [7]
				{
					92.8090000000011, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [8]
				{
					104.1690000000017, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [9]
				{
					115.122000000003, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [10]
			},
			[250333] = {
				{
					5.17000000000553, -- [1]
					"Diima, Mother of Gloom", -- [2]
					250333, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[245627] = {
				{
					9.921000000002095, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					45.78300000000309, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					81.0370000000039, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[252861] = {
				{
					31.30000000000291, -- [1]
					"Asara, Mother of Night", -- [2]
					252861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					89.61900000000605, -- [1]
					"Asara, Mother of Night", -- [2]
					252861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[245303] = {
				{
					4.997999999999593, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.15400000000227, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					21.11200000000099, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					27.22600000000239, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					39.36100000000442, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					50.30600000000413, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					56.375, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					62.47400000000198, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					68.54100000000471, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					79.48099999999977, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					85.55400000000373, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					98.54700000000594, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					109.8860000000059, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					115.9660000000004, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
			},
			[246329] = {
				{
					11.24700000000303, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					40.4210000000021, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					69.63200000000506, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					100.0310000000027, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[245671] = {
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					98.5940000000046, -- [1]
					"Torment of Khaz'goroth", -- [2]
					245671, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
			[249793] = {
				{
					100.0310000000027, -- [1]
					"Diima, Mother of Gloom", -- [2]
					249793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[253520] = {
				{
					20.98099999999977, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					20.98099999999977, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [2]
				{
					20.98099999999977, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [3]
				{
					61.1160000000018, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [4]
				{
					61.1160000000018, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [5]
				{
					61.1160000000018, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [6]
				{
					101.2530000000043, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [7]
				{
					101.2530000000043, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [8]
				{
					101.2530000000043, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [9]
			},
			[250334] = {
				{
					51.37100000000646, -- [1]
					"Thu'raya, Mother of the Cosmos", -- [2]
					250334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [21]
		{
			[243960] = {
				{
					9.168999999994412, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					21.54199999999401, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					31.28499999999622, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					40.99299999999494, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					53.32799999999406, -- [1]
					"Varimathras", -- [2]
					243960, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[244093] = {
				{
					36.15099999999802, -- [1]
					"Varimathras", -- [2]
					244093, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[243999] = {
				{
					17.47899999999936, -- [1]
					"Varimathras", -- [2]
					243999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [1]
				{
					50.27999999999884, -- [1]
					"Varimathras", -- [2]
					243999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [2]
			},
			[244042] = {
				{
					25.18499999999767, -- [1]
					"Varimathras", -- [2]
					244042, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					55.56899999999587, -- [1]
					"Varimathras", -- [2]
					244042, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[248732] = {
				{
					11.10199999999895, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [1]
				{
					14.65199999999459, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [2]
				{
					18.29299999999785, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [3]
				{
					19.08499999999913, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [4]
				{
					19.5199999999968, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [5]
				{
					19.92499999999563, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [6]
				{
					21.95100000000093, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [7]
				{
					22.01499999999942, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [8]
				{
					23.16799999999785, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [9]
				{
					23.56399999999849, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [10]
				{
					24.59500000000116, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [11]
				{
					25.03699999999662, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [12]
				{
					25.59300000000076, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [13]
				{
					25.59300000000076, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [14]
				{
					26.82499999999709, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [15]
				{
					27.21199999999953, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					27.5879999999961, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [17]
				{
					29.23700000000099, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [18]
				{
					29.23700000000099, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [19]
				{
					29.23700000000099, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [20]
				{
					29.64199999999983, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [21]
				{
					30.10099999999511, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [22]
				{
					30.45899999999529, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [23]
				{
					30.86000000000058, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [24]
				{
					32.08599999999569, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [25]
				{
					32.52699999999459, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [26]
				{
					32.90299999999843, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [27]
				{
					32.90299999999843, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [28]
				{
					32.90299999999843, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [29]
				{
					33.71499999999651, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [30]
				{
					34.14600000000064, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [31]
				{
					34.52699999999459, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [32]
				{
					35.33899999999994, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [33]
				{
					35.74899999999616, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [34]
				{
					36.01900000000023, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [35]
				{
					36.15099999999802, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [36]
				{
					36.52899999999499, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [37]
				{
					36.54899999999907, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [38]
				{
					37.37599999999657, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [39]
				{
					38.58599999999569, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [40]
				{
					38.97599999999511, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [41]
				{
					39.03800000000047, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [42]
				{
					39.79299999999785, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [43]
				{
					39.79299999999785, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [44]
				{
					40.19199999999546, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [45]
				{
					40.53399999999965, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [46]
				{
					41.0109999999986, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [47]
				{
					42.22599999999511, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [48]
				{
					42.60800000000018, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [49]
				{
					43.41599999999744, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [50]
				{
					43.43400000000111, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [51]
				{
					43.60399999999936, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [52]
				{
					43.83499999999913, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [53]
				{
					44.23899999999412, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [54]
				{
					44.51199999999517, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [55]
				{
					44.64099999999598, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [56]
				{
					45.83199999999488, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [57]
				{
					46.24899999999616, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [58]
				{
					46.65099999999802, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [59]
				{
					47.06099999999424, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [60]
				{
					47.06099999999424, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [61]
				{
					47.47799999999552, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [62]
				{
					48.28899999999703, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [63]
				{
					48.28899999999703, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [64]
				{
					48.28899999999703, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [65]
				{
					48.28899999999703, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [66]
				{
					49.89800000000105, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [67]
				{
					50.03600000000006, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [68]
				{
					50.29699999999866, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [69]
				{
					50.69399999999587, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [70]
				{
					51.11099999999715, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [71]
				{
					51.91599999999744, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [72]
				{
					51.91599999999744, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [73]
				{
					51.91599999999744, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [74]
				{
					53.53399999999965, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [75]
				{
					53.94099999999889, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [76]
				{
					53.94099999999889, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [77]
				{
					54.35699999999633, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [78]
				{
					54.74899999999616, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [79]
				{
					55.56899999999587, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [80]
				{
					55.56899999999587, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [81]
				{
					55.56899999999587, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [82]
				{
					57.18000000000029, -- [1]
					"Shadow of Varimathras", -- [2]
					248732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [83]
			},
		}, -- [22]
		{
			[248214] = {
				{
					11.9910000000018, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					78.79699999999866, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					108.6849999999977, -- [1]
					"Kin'garoth", -- [2]
					248214, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[246516] = {
				{
					38.56699999999546, -- [1]
					"Kin'garoth", -- [2]
					246516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246833] = {
				{
					28.34199999999692, -- [1]
					"Kin'garoth", -- [2]
					246833, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ruiner", -- [5]
				}, -- [1]
				{
					101.6440000000002, -- [1]
					"Kin'garoth", -- [2]
					246833, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ruiner", -- [5]
				}, -- [2]
			},
			[249535] = {
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [1]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [2]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [3]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [4]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [6]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [7]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [8]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [9]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [10]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [11]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [12]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [13]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [14]
				{
					84.41300000000047, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [15]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [16]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [17]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [18]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [19]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [20]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [21]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [22]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [23]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [24]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [25]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [26]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [27]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [28]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [29]
				{
					87.87099999999919, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [30]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [31]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [32]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [33]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [34]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [35]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [36]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [37]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [38]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [39]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [40]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [41]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [42]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [43]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [44]
				{
					88.62799999999697, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [45]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [46]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [47]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [48]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [49]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [50]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [51]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [52]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [53]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [54]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [55]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [56]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [57]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [58]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [59]
				{
					120.0499999999956, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [60]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deathklokk-Bloodhoof", -- [5]
				}, -- [61]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [62]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [63]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Acindros-BleedingHollow", -- [5]
				}, -- [64]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [65]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Xei-Spirestone", -- [5]
				}, -- [66]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [67]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Valurtrois-WyrmrestAccord", -- [5]
				}, -- [68]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [69]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Drehy-WyrmrestAccord", -- [5]
				}, -- [70]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [71]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Barbonsaurio-Ragnaros", -- [5]
				}, -- [72]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dolbuk-Azralon", -- [5]
				}, -- [73]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [74]
				{
					121.2379999999976, -- [1]
					"Kin'garoth", -- [2]
					249535, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [75]
			},
			[244328] = {
				{
					82.84900000000198, -- [1]
					"Detonation Charge", -- [2]
					244328, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					112.788999999997, -- [1]
					"Detonation Charge", -- [2]
					244328, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					112.788999999997, -- [1]
					"Detonation Charge", -- [2]
					244328, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[254926] = {
				{
					15.63500000000204, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					82.8379999999961, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					112.788999999997, -- [1]
					"Kin'garoth", -- [2]
					254926, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[254796] = {
				{
					65.09900000000198, -- [1]
					"Kin'garoth", -- [2]
					254796, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246504] = {
				{
					38.68600000000151, -- [1]
					"Garothi Annihilator", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.68600000000151, -- [1]
					"Garothi Decimator", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.68600000000151, -- [1]
					"Garothi Demolisher", -- [2]
					246504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[254919] = {
				{
					6.909999999996217, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					21.69599999999628, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					81.22499999999854, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					95.78399999999965, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					110.7669999999998, -- [1]
					"Kin'garoth", -- [2]
					254919, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [23]
		{
			[247687] = {
				{
					53.11100000000442, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [1]
				{
					138.1140000000014, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [2]
			},
			[254244] = {
				{
					7.983000000000175, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					95.59599999999773, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[248233] = {
				{
					20.45300000000134, -- [1]
					"Imonar the Soulhunter", -- [2]
					248233, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					106.7410000000018, -- [1]
					"Imonar the Soulhunter", -- [2]
					248233, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[247367] = {
				{
					4.452000000004773, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Zhuxin-WyrmrestAccord", -- [5]
				}, -- [1]
				{
					9.325000000004366, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [2]
				{
					91.96399999999994, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [3]
				{
					96.80600000000413, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [4]
			},
			[248254] = {
				{
					56.02799999999843, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					141.0330000000031, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[252758] = {
				{
					171.4609999999957, -- [1]
					"Garothi Demolisher", -- [2]
					252758, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					171.4609999999957, -- [1]
					"Garothi Demolisher", -- [2]
					252758, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[250255] = {
				{
					165.9979999999996, -- [1]
					"Imonar the Soulhunter", -- [2]
					250255, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [1]
			},
			[248068] = {
				{
					167.2229999999981, -- [1]
					"Imonar the Soulhunter", -- [2]
					248068, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[250135] = {
				{
					65.4210000000021, -- [1]
					"Imonar the Soulhunter", -- [2]
					250135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					150.4700000000012, -- [1]
					"Imonar the Soulhunter", -- [2]
					250135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [24]
		{
			[247687] = {
				{
					48.3269999999975, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [1]
				{
					135.7699999999968, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [2]
				{
					144.2799999999988, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [3]
				{
					151.5679999999993, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [4]
				{
					158.8299999999945, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [5]
				{
					166.1129999999976, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [6]
				{
					173.4069999999992, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [7]
				{
					180.666999999994, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [8]
				{
					187.9719999999943, -- [1]
					"Imonar the Soulhunter", -- [2]
					247687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Seraia-Maelstrom", -- [5]
				}, -- [9]
			},
			[254244] = {
				{
					7.044999999998254, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					88.41300000000047, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					140.630999999994, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					152.7619999999952, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					164.9199999999983, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					177.0439999999944, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					189.1739999999991, -- [1]
					"Imonar the Soulhunter", -- [2]
					254244, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[248233] = {
				{
					23.06899999999587, -- [1]
					"Imonar the Soulhunter", -- [2]
					248233, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					105.6089999999967, -- [1]
					"Imonar the Soulhunter", -- [2]
					248233, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[248070] = {
				{
					142.3559999999998, -- [1]
					"Imonar the Soulhunter", -- [2]
					248070, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					169.0439999999944, -- [1]
					"Imonar the Soulhunter", -- [2]
					248070, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[247367] = {
				{
					4.594999999993888, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [1]
				{
					9.492999999994936, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [2]
				{
					14.35099999999511, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [3]
				{
					85.95099999999366, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [4]
				{
					90.82099999999627, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [5]
				{
					95.68399999999383, -- [1]
					"Imonar the Soulhunter", -- [2]
					247367, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [6]
			},
			[248254] = {
				{
					50.61499999999796, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					138.7039999999979, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					156.916999999994, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					175.1100000000006, -- [1]
					"Imonar the Soulhunter", -- [2]
					248254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[250135] = {
				{
					61.90499999999884, -- [1]
					"Imonar the Soulhunter", -- [2]
					250135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[247923] = {
				{
					54.41300000000047, -- [1]
					"Imonar the Soulhunter", -- [2]
					247923, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					96.91899999999441, -- [1]
					"Imonar the Soulhunter", -- [2]
					247923, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[248068] = {
				{
					13.12799999999697, -- [1]
					"Imonar the Soulhunter", -- [2]
					248068, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					94.4739999999947, -- [1]
					"Imonar the Soulhunter", -- [2]
					248068, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [25]
		{
			[254769] = {
				{
					23.06599999999889, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					40.51499999999942, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					120.6080000000002, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					395.1880000000019, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					415.060000000005, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[249148] = {
				{
					65.96399999999994, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					177.6970000000001, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					268.775999999998, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					369.2260000000024, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[249934] = {
				{
					111.0740000000005, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					237.400999999998, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					317.1240000000034, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					433.4389999999985, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[250701] = {
				{
					142.8340000000026, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					208.0540000000037, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					345.7079999999987, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[250703] = {
				{
					71.20700000000215, -- [1]
					"Fel-Powered Purifier", -- [2]
					250703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					151.3450000000012, -- [1]
					"Fel-Powered Purifier", -- [2]
					250703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[249196] = {
				{
					177.2669999999998, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					268.359000000004, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246301] = {
				{
					396.8040000000037, -- [1]
					"Fel-Infused Destructor", -- [2]
					246301, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246753] = {
				{
					41.33000000000175, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					263.510000000002, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					308.8420000000042, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					398.4270000000033, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[246305] = {
				{
					427.1770000000033, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					429.6140000000014, -- [1]
					"Fel-Infused Destructor", -- [2]
					246305, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[249126] = {
				{
					375.3220000000001, -- [1]
					"Paraxis Inquisitor", -- [2]
					249126, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [26]
		{
			[254769] = {
				{
					21.09000000000378, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					39.35900000000402, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					118.7090000000026, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					202.5690000000031, -- [1]
					"Fel-Powered Purifier", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[249148] = {
				{
					63.64600000000064, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					185.538999999997, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					282.2560000000012, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[249934] = {
				{
					118.4760000000024, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					245.247000000003, -- [1]
					"The Paraxis", -- [2]
					249934, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[250701] = {
				{
					144.6030000000028, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[250703] = {
				{
					69.71199999999953, -- [1]
					"Fel-Powered Purifier", -- [2]
					250703, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[248893] = {
				{
					308.3689999999988, -- [1]
					"The Paraxis", -- [2]
					248893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[249194] = {
				{
					285.8960000000006, -- [1]
					"Paraxis Inquisitor", -- [2]
					249194, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[249196] = {
				{
					181.4630000000034, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					272.1280000000043, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					281.8329999999987, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[246753] = {
				{
					40.58600000000297, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					264.8700000000026, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[249121] = {
				{
					308.3559999999998, -- [1]
					"The Paraxis", -- [2]
					249121, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[249126] = {
				{
					185.538999999997, -- [1]
					"Paraxis Inquisitor", -- [2]
					249126, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					276.1960000000036, -- [1]
					"Paraxis Inquisitor", -- [2]
					249126, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [27]
		{
			[246753] = {
				{
					41.72200000000157, -- [1]
					"Fel-Charged Obfuscator", -- [2]
					246753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[254769] = {
				{
					23.1140000000014, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					40.51699999999983, -- [1]
					"Fel-Infused Destructor", -- [2]
					254769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[249121] = {
				{
					105.3090000000011, -- [1]
					"The Paraxis", -- [2]
					249121, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[249196] = {
				{
					67.62299999999959, -- [1]
					"Paraxis Inquisitor", -- [2]
					249196, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[250701] = {
				{
					73.68299999999726, -- [1]
					"Fel-Powered Purifier", -- [2]
					250701, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[248893] = {
				{
					109.4599999999991, -- [1]
					"The Paraxis", -- [2]
					248893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[249148] = {
				{
					68.05399999999645, -- [1]
					"Paraxis Inquisitor", -- [2]
					249148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [28]
		{
			[244709] = {
				{
					18.04499999999825, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					19.04799999999523, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					21.02499999999418, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					22.06199999999808, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					25.02299999999377, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					26.03299999999581, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					27.0399999999936, -- [1]
					"Blazing Imp", -- [2]
					244709, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[244689] = {
				{
					9, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[243983] = {
				{
					19.23699999999371, -- [1]
					"Portal Keeper Hasabel", -- [2]
					243983, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244000] = {
				{
					28.94799999999668, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244000, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[244016] = {
				{
					6.276999999994587, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244016, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [1]
				{
					19.64799999999377, -- [1]
					"Portal Keeper Hasabel", -- [2]
					244016, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hamtauros-Area52", -- [5]
				}, -- [2]
			},
		}, -- [29]
		{
			[245888] = {
				{
					6.718000000000757, -- [1]
					"Antoran Champion", -- [2]
					245888, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [30]
		{
			[253038] = {
				{
					14.18299999999726, -- [1]
					"Felblade Shocktrooper", -- [2]
					253038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					14.29099999999744, -- [1]
					"Felblade Shocktrooper", -- [2]
					253038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					34.90699999999924, -- [1]
					"Felblade Shocktrooper", -- [2]
					253038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					38.64400000000023, -- [1]
					"Felblade Shocktrooper", -- [2]
					253038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[244892] = {
				{
					8.231999999996333, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.73300000000018, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					25.22899999999936, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					33.71699999999692, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					42.21699999999692, -- [1]
					"Admiral Svirax", -- [2]
					244892, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[257262] = {
				{
					6.136999999995169, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					6.286999999996624, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					6.642999999996391, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					6.705999999998312, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					7.108000000000175, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					16.83699999999953, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					17.08699999999953, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					17.30399999999645, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					17.63899999999558, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					17.63899999999558, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					27.2219999999943, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					27.29699999999866, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					27.3640000000014, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					27.63499999999476, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					28.17099999999482, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					36.93999999999505, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					37.24799999999959, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					37.39800000000105, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					37.67699999999604, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					37.67699999999604, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					47.59399999999732, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					48.05999999999767, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					48.05999999999767, -- [1]
					"Entropic Mine", -- [2]
					257262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
			},
			[246505] = {
				{
					16.5679999999993, -- [1]
					"Fanatical Pyromancer", -- [2]
					246505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caldrissa-Aegwynn", -- [5]
				}, -- [1]
				{
					31.77199999999721, -- [1]
					"Fanatical Pyromancer", -- [2]
					246505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [2]
				{
					36.5989999999947, -- [1]
					"Fanatical Pyromancer", -- [2]
					246505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [3]
				{
					41.47000000000116, -- [1]
					"Fanatical Pyromancer", -- [2]
					246505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [4]
			},
			[245868] = {
				{
					48.30399999999645, -- [1]
					"Antoran Doomguard", -- [2]
					245868, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[253037] = {
				{
					14.18299999999726, -- [1]
					"Felblade Shocktrooper", -- [2]
					253037, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Forseen-Area52", -- [5]
				}, -- [1]
				{
					14.29099999999744, -- [1]
					"Felblade Shocktrooper", -- [2]
					253037, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caldrissa-Aegwynn", -- [5]
				}, -- [2]
				{
					34.90699999999924, -- [1]
					"Felblade Shocktrooper", -- [2]
					253037, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Silendrias-WyrmrestAccord", -- [5]
				}, -- [3]
				{
					38.64400000000023, -- [1]
					"Felblade Shocktrooper", -- [2]
					253037, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caldrissa-Aegwynn", -- [5]
				}, -- [4]
			},
			[244722] = {
				{
					15.08499999999913, -- [1]
					"Admiral Svirax", -- [2]
					244722, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.87999999999738, -- [1]
					"Admiral Svirax", -- [2]
					244722, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					45.45699999999488, -- [1]
					"Admiral Svirax", -- [2]
					244722, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [31]
		{
			[245098] = {
				{
					8.474999999998545, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [1]
				{
					19.45100000000093, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [2]
				{
					30.35599999999977, -- [1]
					"Shatug", -- [2]
					245098, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyfart-Bloodhoof", -- [5]
				}, -- [3]
			},
			[251445] = {
				{
					8.474999999998545, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [1]
				{
					20.66400000000431, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [2]
				{
					30.35599999999977, -- [1]
					"F'harg", -- [2]
					251445, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Azuba-TheForgottenCoast", -- [5]
				}, -- [3]
			},
			[244072] = {
				{
					16.9910000000018, -- [1]
					"F'harg", -- [2]
					244072, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [32]
		{
			[244969] = {
				{
					28.53100000000268, -- [1]
					"Garothi Worldbreaker", -- [2]
					244969, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					41.89199999999983, -- [1]
					"Garothi Worldbreaker", -- [2]
					244969, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246965] = {
				{
					19.33299999999872, -- [1]
					"Annihilator", -- [2]
					246965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[245237] = {
				{
					23.01800000000367, -- [1]
					"Garothi Worldbreaker", -- [2]
					245237, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.37000000000262, -- [1]
					"Garothi Worldbreaker", -- [2]
					245237, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[246848] = {
				{
					23.01800000000367, -- [1]
					"Garothi Worldbreaker", -- [2]
					246848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.37000000000262, -- [1]
					"Garothi Worldbreaker", -- [2]
					246848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[244399] = {
				{
					8.442999999999302, -- [1]
					"Decimator", -- [2]
					244399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[246897] = {
				{
					33.20100000000093, -- [1]
					"Decimator", -- [2]
					246897, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [33]
		{
			[269843] = {
				{
					11.5360000000001, -- [1]
					"Unbound Abomination", -- [2]
					269843, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.3410000000004, -- [1]
					"Unbound Abomination", -- [2]
					269843, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					43.0999999999986, -- [1]
					"Unbound Abomination", -- [2]
					269843, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					58.882999999998, -- [1]
					"Unbound Abomination", -- [2]
					269843, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					74.7209999999977, -- [1]
					"Unbound Abomination", -- [2]
					269843, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					91.6659999999975, -- [1]
					"Unbound Abomination", -- [2]
					269843, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [34]
		{
			[272457] = {
				{
					12.3780000000006, -- [1]
					"Sporecaller Zancha", -- [2]
					272457, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					26.9279999999999, -- [1]
					"Sporecaller Zancha", -- [2]
					272457, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[273285] = {
				{
					21.0450000000019, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					21.0450000000019, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					21.0450000000019, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					21.0450000000019, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					21.0450000000019, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					21.0550000000003, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					46.5210000000006, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					46.5210000000006, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					46.5210000000006, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					46.5210000000006, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					46.5319999999992, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					46.5319999999992, -- [1]
					"Volatile Pod", -- [2]
					273285, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
			},
			[259718] = {
				{
					16.1920000000027, -- [1]
					"Sporecaller Zancha", -- [2]
					259718, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gnarledup-KulTiras", -- [5]
				}, -- [1]
				{
					16.1920000000027, -- [1]
					"Sporecaller Zancha", -- [2]
					259718, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"ßrim-Thrall", -- [5]
				}, -- [2]
				{
					36.8130000000019, -- [1]
					"Sporecaller Zancha", -- [2]
					259718, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Primaluna-Azralon", -- [5]
				}, -- [3]
				{
					36.8130000000019, -- [1]
					"Sporecaller Zancha", -- [2]
					259718, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"ßrim-Thrall", -- [5]
				}, -- [4]
			},
			[274213] = {
				{
					22.2100000000028, -- [1]
					"Sporecaller Zancha", -- [2]
					274213, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					22.2100000000028, -- [1]
					"Sporecaller Zancha", -- [2]
					274213, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					42.8209999999999, -- [1]
					"Sporecaller Zancha", -- [2]
					274213, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					42.8209999999999, -- [1]
					"Sporecaller Zancha", -- [2]
					274213, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[259732] = {
				{
					50.3009999999995, -- [1]
					"Sporecaller Zancha", -- [2]
					259732, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [35]
		{
			[260455] = {
				{
					69.8810000000012, -- [1]
					"Blood Tick", -- [2]
					260455, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Putridprayer-Antonidas", -- [5]
				}, -- [1]
			},
			[260292] = {
				{
					22.1239999999998, -- [1]
					"Cragmaw the Infested", -- [2]
					260292, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					42.7609999999986, -- [1]
					"Cragmaw the Infested", -- [2]
					260292, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					71.8909999999996, -- [1]
					"Cragmaw the Infested", -- [2]
					260292, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[260333] = {
				{
					51.6709999999985, -- [1]
					"Cragmaw the Infested", -- [2]
					260333, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[260793] = {
				{
					9.5109999999986, -- [1]
					"Cragmaw the Infested", -- [2]
					260793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [36]
		{
			[270987] = {
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					110.011, -- [1]
					"Disciple of Nalorakk", -- [2]
					270987, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
			[288043] = {
				{
					110.011, -- [1]
					"Kaldorei Glaive Thrower", -- [2]
					288043, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [37]
		{
			[153794] = {
				{
					11.5810000000001, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					22.5030000000006, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					33.4620000000014, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					71.0950000000012, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					82.0360000000001, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					92.9730000000018, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
				{
					103.892000000002, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					114.801000000001, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [8]
				{
					145.167000000001, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
				{
					156.114000000001, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
			},
			[159382] = {
				{
					45.371000000001, -- [1]
					"Rukhran", -- [2]
					159382, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					123.091, -- [1]
					"Rukhran", -- [2]
					159382, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153810] = {
				{
					15.2330000000002, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.0310000000009, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					88.1230000000014, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					107.52, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					148.816000000001, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					164.593000000001, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[153896] = {
				{
					62.389000000001, -- [1]
					"Rukhran", -- [2]
					153896, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					140.098000000002, -- [1]
					"Rukhran", -- [2]
					153896, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[169810] = {
				{
					30.0260000000017, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					37.2970000000005, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					44.5840000000008, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					60.3600000000006, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					70.0880000000016, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					90.755000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					107.728000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					112.579000000002, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					136.879000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					141.744000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
			},
		}, -- [38]
		{
			[153828] = {
				{
					33.771999999999, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					56.8359999999993, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					60.485999999999, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					64.1229999999996, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					65.3429999999989, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					66.5479999999989, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					95.2909999999993, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[153794] = {
				{
					14.110999999999, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					25.0569999999989, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					35.9939999999988, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					73.6260000000002, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					84.5619999999999, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					96.7070000000003, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
			},
			[153810] = {
				{
					11.2899999999991, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					32.3299999999999, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					77.2549999999992, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					94.262999999999, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[169810] = {
				{
					28.9200000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					42.2780000000003, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					47.9369999999999, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					50.777, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[153896] = {
				{
					64.9339999999993, -- [1]
					"Rukhran", -- [2]
					153896, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[159382] = {
				{
					47.9110000000001, -- [1]
					"Rukhran", -- [2]
					159382, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [39]
		{
			[154135] = {
				{
					22.7509999999993, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					45.8680000000004, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					71.393, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[154110] = {
				{
					7.21699999999964, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					15.6969999999992, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					53.3689999999988, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[154113] = {
				{
					30.2539999999999, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.7779999999993, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					64.3019999999997, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					78.8919999999998, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					87.4239999999991, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [40]
		{
			[154135] = {
				{
					21.9070000000002, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.1770000000006, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					69.2130000000007, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					92.3240000000005, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					115.360000000001, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					139.685, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					162.702, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					185.747, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					208.783, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					234.269, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					257.341, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					280.400000000001, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					303.456, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
			},
			[154113] = {
				{
					13.6120000000001, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.4080000000004, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					62.1570000000002, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					99.7860000000001, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					132.59, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					148.382000000001, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					156.349, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					170.194, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					178.678, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					193.243, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					201.723, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					216.272, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					225.987, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					250.275000000001, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					273.329000000001, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					310.961, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
			},
			[154110] = {
				{
					6.32700000000023, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					39.1190000000006, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					53.6779999999999, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					76.7420000000002, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					85.2560000000003, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					108.286, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					122.875, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					241.778, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					264.839, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					287.902, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					296.388, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
			},
		}, -- [41]
		{
			[153757] = {
				{
					10.567, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					26.3609999999999, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					42.1660000000002, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					57.9200000000001, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					73.6689999999999, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					89.4700000000003, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					105.252, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					121.035, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					136.839, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[153315] = {
				{
					10.3490000000002, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					24.9389999999994, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					72.2559999999994, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					119.611, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[165731] = {
				{
					15.4409999999998, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Finsky-Tichondrius", -- [5]
				}, -- [1]
				{
					30.0169999999998, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fairlead-Dalaran", -- [5]
				}, -- [2]
				{
					77.3239999999996, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					124.682, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Finsky-Tichondrius", -- [5]
				}, -- [4]
			},
			[156793] = {
				{
					40.0280000000002, -- [1]
					"Ranjit", -- [2]
					156793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					87.3299999999999, -- [1]
					"Ranjit", -- [2]
					156793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					134.699, -- [1]
					"Ranjit", -- [2]
					156793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[165733] = {
				{
					15.4290000000001, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.0039999999999, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					77.3109999999997, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					124.682, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[153544] = {
				{
					4.28700000000026, -- [1]
					"Ranjit", -- [2]
					153544, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [42]
		{
			[150677] = {
				{
					2.50900000000001, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					35.0339999999997, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					49.6390000000001, -- [1]
					"Gug'rokk", -- [2]
					150677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
			},
			[163802] = {
				{
					7.30099999999948, -- [1]
					"Gug'rokk", -- [2]
					163802, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					19.46, -- [1]
					"Gug'rokk", -- [2]
					163802, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					31.5919999999997, -- [1]
					"Gug'rokk", -- [2]
					163802, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					52.2740000000004, -- [1]
					"Gug'rokk", -- [2]
					163802, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[150755] = {
				{
					21.8859999999995, -- [1]
					"Gug'rokk", -- [2]
					150755, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.1840000000002, -- [1]
					"Gug'rokk", -- [2]
					150755, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[150776] = {
				{
					11.2389999999996, -- [1]
					"Gug'rokk", -- [2]
					150776, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.241, -- [1]
					"Gug'rokk", -- [2]
					150776, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					44.0519999999997, -- [1]
					"Gug'rokk", -- [2]
					150776, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [43]
		{
			[153247] = {
				{
					11.473, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					15.5149999999994, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					19.1419999999998, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					51.5709999999999, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					55.2159999999994, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					58.8379999999997, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					92.0499999999993, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					95.7109999999993, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					99.3419999999997, -- [1]
					"Roltall", -- [2]
					153247, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[152940] = {
				{
					27.3939999999993, -- [1]
					"Roltall", -- [2]
					152940, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					67.4769999999999, -- [1]
					"Roltall", -- [2]
					152940, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					107.574, -- [1]
					"Roltall", -- [2]
					152940, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[152939] = {
				{
					36.9039999999995, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.1359999999995, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					39.3399999999992, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					40.5499999999993, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					41.7709999999997, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					42.9969999999994, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					77.4020000000001, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					78.6279999999997, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					79.8239999999996, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					81.0529999999999, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					81.8509999999997, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					83.4809999999998, -- [1]
					"Roltall", -- [2]
					152939, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
			},
			[152941] = {
				{
					60.4849999999997, -- [1]
					"Roltall", -- [2]
					152941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					63.3310000000001, -- [1]
					"Roltall", -- [2]
					152941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yukimura-Tichondrius", -- [5]
				}, -- [2]
				{
					76.701, -- [1]
					"Roltall", -- [2]
					152941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					103.428, -- [1]
					"Roltall", -- [2]
					152941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yukimura-Tichondrius", -- [5]
				}, -- [4]
				{
					104.656999999999, -- [1]
					"Roltall", -- [2]
					152941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					105.858999999999, -- [1]
					"Roltall", -- [2]
					152941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
			},
		}, -- [44]
		{
			[150324] = {
				{
					23.4539999999997, -- [1]
					"Ruination", -- [2]
					150324, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[77508] = {
				{
					14.6619999999994, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [1]
				{
					30.4290000000001, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [2]
				{
					47.451, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [3]
				{
					63.25, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [4]
				{
					76.6260000000002, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [5]
				{
					89.973, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [6]
				{
					106.005, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [7]
				{
					106.005, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [8]
				{
					106.005, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [9]
				{
					106.005, -- [1]
					"Vengeful Magma Elemental", -- [2]
					77508, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bloodmaul Earthbreaker", -- [5]
				}, -- [10]
			},
			[150290] = {
				{
					21.0209999999997, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nossia-Madoran", -- [5]
				}, -- [1]
				{
					23.4639999999999, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Stukko-BurningLegion", -- [5]
				}, -- [2]
				{
					27.1089999999995, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nossia-Madoran", -- [5]
				}, -- [3]
				{
					38.0159999999996, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Stukko-BurningLegion", -- [5]
				}, -- [4]
				{
					40.4479999999994, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Rámsul-Nagrand", -- [5]
				}, -- [5]
				{
					42.8760000000002, -- [1]
					"Calamity", -- [2]
					150290, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Stukko-BurningLegion", -- [5]
				}, -- [6]
			},
			[150032] = {
				{
					43.0900000000001, -- [1]
					"Magmolatus", -- [2]
					150032, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					68.6149999999998, -- [1]
					"Magmolatus", -- [2]
					150032, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					94.1300000000001, -- [1]
					"Magmolatus", -- [2]
					150032, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150004] = {
				{
					2.51899999999932, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					8.59299999999985, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					14.6619999999994, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					20.7389999999996, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					26.8099999999995, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					62.0339999999997, -- [1]
					"Molten Elemental", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					79.0429999999997, -- [1]
					"Molten Elemental", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					83.9209999999994, -- [1]
					"Molten Elemental", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					97.2730000000001, -- [1]
					"Molten Elemental", -- [2]
					150004, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[150078] = {
				{
					15.8809999999994, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150078, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150038] = {
				{
					53.8189999999995, -- [1]
					"Magmolatus", -- [2]
					150038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					75.6849999999995, -- [1]
					"Magmolatus", -- [2]
					150038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					97.5639999999994, -- [1]
					"Magmolatus", -- [2]
					150038, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[152170] = {
				{
					53.8289999999997, -- [1]
					"Magmolatus", -- [2]
					152170, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					75.6949999999997, -- [1]
					"Magmolatus", -- [2]
					152170, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					97.5639999999994, -- [1]
					"Magmolatus", -- [2]
					152170, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[149997] = {
				{
					30.4290000000001, -- [1]
					"Calamity", -- [2]
					149997, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					43.8040000000001, -- [1]
					"Calamity", -- [2]
					149997, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[149941] = {
				{
					20.3000000000002, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.5990000000002, -- [1]
					"Ruination", -- [2]
					149941, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[150076] = {
				{
					9.80799999999999, -- [1]
					"Forgemaster Gog'duh", -- [2]
					150076, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150048] = {
				{
					73.7619999999997, -- [1]
					"Molten Elemental", -- [2]
					150048, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					91.9830000000002, -- [1]
					"Molten Elemental", -- [2]
					150048, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[149975] = {
				{
					24.8850000000002, -- [1]
					"Calamity", -- [2]
					149975, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150023] = {
				{
					46.3029999999999, -- [1]
					"Magmolatus", -- [2]
					150023, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					64.5419999999995, -- [1]
					"Magmolatus", -- [2]
					150023, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					83.982, -- [1]
					"Magmolatus", -- [2]
					150023, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [45]
		{
			[150759] = {
				{
					11.6270000000004, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.8730000000005, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					52.9620000000005, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					76.0560000000005, -- [1]
					"Slave Watcher Crushto", -- [2]
					150759, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[151092] = {
				{
					49.3520000000008, -- [1]
					"Captured Miner", -- [2]
					151092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Slave Watcher Crushto", -- [5]
				}, -- [1]
			},
			[150753] = {
				{
					24.2970000000005, -- [1]
					"Slave Watcher Crushto", -- [2]
					150753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.3890000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					150753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					70.4740000000002, -- [1]
					"Slave Watcher Crushto", -- [2]
					150753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150807] = {
				{
					33.5370000000003, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nossia-Madoran", -- [5]
				}, -- [1]
				{
					34.7370000000001, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nossia-Madoran", -- [5]
				}, -- [2]
				{
					35.3050000000003, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					43.2630000000008, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					44.4810000000007, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yukimura-Tichondrius", -- [5]
				}, -- [5]
				{
					44.4810000000007, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yukimura-Tichondrius", -- [5]
				}, -- [6]
				{
					51.1720000000005, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					52.1770000000006, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nossia-Madoran", -- [5]
				}, -- [8]
				{
					57.0380000000005, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nossia-Madoran", -- [5]
				}, -- [9]
				{
					58.2530000000006, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yukimura-Tichondrius", -- [5]
				}, -- [10]
				{
					59.4690000000001, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yukimura-Tichondrius", -- [5]
				}, -- [11]
				{
					64.0440000000008, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [12]
				{
					65.5490000000009, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [13]
				{
					66.75, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [14]
				{
					69.1930000000002, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [15]
				{
					69.1930000000002, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					75.2460000000001, -- [1]
					"Captured Miner", -- [2]
					150807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [17]
			},
			[150801] = {
				{
					16.6980000000003, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.3110000000006, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					43.4470000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					55.6110000000008, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					67.7600000000002, -- [1]
					"Slave Watcher Crushto", -- [2]
					150801, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[153679] = {
				{
					5.55500000000029, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [1]
				{
					18.9300000000003, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [2]
				{
					37.1480000000001, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [3]
				{
					60.2660000000005, -- [1]
					"Slave Watcher Crushto", -- [2]
					153679, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Earth Crush Stalker", -- [5]
				}, -- [4]
			},
			[150751] = {
				{
					26.2630000000008, -- [1]
					"Slave Watcher Crushto", -- [2]
					150751, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					49.1280000000006, -- [1]
					"Slave Watcher Crushto", -- [2]
					150751, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					72.4010000000008, -- [1]
					"Slave Watcher Crushto", -- [2]
					150751, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [46]
		{
			[169129] = {
				{
					105.674, -- [1]
					"Backdraft", -- [2]
					169129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					166.409000000001, -- [1]
					"Backdraft", -- [2]
					169129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[168402] = {
				{
					32.9130000000005, -- [1]
					"Koramar", -- [2]
					168402, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[168929] = {
				{
					37.6590000000006, -- [1]
					"Skulloc", -- [2]
					168929, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					94.7340000000004, -- [1]
					"Skulloc", -- [2]
					168929, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					155.49, -- [1]
					"Skulloc", -- [2]
					168929, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					168.826, -- [1]
					"Skulloc", -- [2]
					168929, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					219.791, -- [1]
					"Skulloc", -- [2]
					168929, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[168398] = {
				{
					4.08100000000013, -- [1]
					"Blackhand's Might Turret", -- [2]
					168398, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.223, -- [1]
					"Blackhand's Might Turret", -- [2]
					168398, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					28.375, -- [1]
					"Blackhand's Might Turret", -- [2]
					168398, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					51.4279999999999, -- [1]
					"Blackhand's Might Turret", -- [2]
					168398, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					63.5780000000004, -- [1]
					"Blackhand's Might Turret", -- [2]
					168398, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					75.7560000000003, -- [1]
					"Blackhand's Might Turret", -- [2]
					168398, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[168965] = {
				{
					13.2240000000002, -- [1]
					"Koramar", -- [2]
					168965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					64.2210000000005, -- [1]
					"Koramar", -- [2]
					168965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					84.884, -- [1]
					"Koramar", -- [2]
					168965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[168227] = {
				{
					31.0380000000005, -- [1]
					"Skulloc", -- [2]
					168227, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					88.143, -- [1]
					"Skulloc", -- [2]
					168227, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					148.862, -- [1]
					"Skulloc", -- [2]
					168227, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					213.156, -- [1]
					"Skulloc", -- [2]
					168227, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [47]
		{
			[162415] = {
				{
					50.3620000000001, -- [1]
					"Oshir", -- [2]
					162415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [1]
				{
					98.6450000000004, -- [1]
					"Oshir", -- [2]
					162415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [2]
			},
			[178155] = {
				{
					28.4459999999999, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.4459999999999, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					30.4669999999996, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					30.4669999999996, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					32.4669999999996, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					32.4669999999996, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					34.6390000000001, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					34.6390000000001, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					36.8689999999997, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					36.8689999999997, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					38.9549999999999, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					38.9549999999999, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					41.2640000000001, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					41.2640000000001, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					43.2889999999998, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					43.2889999999998, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					45.6239999999998, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					45.6239999999998, -- [1]
					"Rylak Skyterror", -- [2]
					178155, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
			},
			[178154] = {
				{
					28.4359999999997, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.4669999999996, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					32.4669999999996, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					34.6390000000001, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					36.8689999999997, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					38.9449999999997, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					41.2640000000001, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					43.2889999999998, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					45.6239999999998, -- [1]
					"Rylak Skyterror", -- [2]
					178154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[178124] = {
				{
					17.0079999999998, -- [1]
					"Oshir", -- [2]
					178124, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Rylak Cage", -- [5]
				}, -- [1]
				{
					77.7210000000005, -- [1]
					"Oshir", -- [2]
					178124, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Wolf Cage", -- [5]
				}, -- [2]
			},
		}, -- [48]
		{
			[163334] = {
				{
					4.12699999999995, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [1]
				{
					7.77500000000009, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [2]
				{
					16.2559999999999, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [3]
				{
					19.8820000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [4]
				{
					23.5169999999998, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [5]
				{
					27.1849999999999, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [6]
				{
					30.8359999999998, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [7]
				{
					34.48, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [8]
				{
					38.145, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [9]
				{
					46.6520000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [10]
				{
					50.29, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [11]
				{
					55.1349999999998, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [12]
				{
					58.797, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [13]
				{
					62.4180000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [14]
				{
					66.0529999999999, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [15]
				{
					74.5299999999998, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [16]
				{
					78.1709999999998, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [17]
				{
					81.8130000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [18]
				{
					85.4540000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [19]
				{
					89.0929999999999, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [20]
				{
					92.743, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [21]
				{
					96.3580000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [22]
				{
					104.86, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [23]
				{
					108.496, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [24]
				{
					113.368, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [25]
				{
					117.003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [26]
				{
					120.649, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [27]
				{
					124.317, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [28]
				{
					132.795, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [29]
				{
					136.424, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [30]
				{
					140.068, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [31]
				{
					143.687, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [32]
				{
					147.325, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [33]
				{
					150.978, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [34]
				{
					154.628, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [35]
				{
					163.139, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [36]
				{
					166.779, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Åssåssïn-Skywall", -- [5]
				}, -- [37]
			},
			[163379] = {
				{
					3.99699999999984, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					49.0500000000002, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					77.3110000000002, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					107.681, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					135.657, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					165.585, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[163665] = {
				{
					5.77199999999994, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.9099999999999, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					62.8290000000002, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					91.9479999999999, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					121.076, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					150.175, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					179.338, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[165122] = {
				{
					3.34099999999989, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					7.07200000000012, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					10.7159999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					14.3449999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					17.9589999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [5]
				{
					21.6030000000001, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [6]
				{
					33.768, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					37.4169999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [8]
				{
					41.0839999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
				{
					44.7159999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
				{
					48.3580000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [11]
				{
					52, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [12]
				{
					55.6469999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [13]
				{
					61.7089999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [14]
				{
					65.3359999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [15]
				{
					68.9870000000001, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					72.6059999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [17]
				{
					76.243, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [18]
				{
					79.8679999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [19]
				{
					83.518, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [20]
				{
					87.1660000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [21]
				{
					93.23, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [22]
				{
					96.848, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [23]
				{
					100.498, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [24]
				{
					104.136, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [25]
				{
					107.778, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [26]
				{
					111.427, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [27]
				{
					115.071, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [28]
				{
					121.158, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [29]
				{
					124.824, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [30]
				{
					128.443, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [31]
				{
					132.067, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [32]
				{
					135.713, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [33]
				{
					139.363, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [34]
				{
					142.975, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [35]
				{
					149.046, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [36]
				{
					152.694, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [37]
				{
					156.345, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [38]
				{
					159.976, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [39]
				{
					163.63, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [40]
			},
			[163390] = {
				{
					10.9169999999999, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					41.2889999999998, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					69.203, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					99.502, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					127.446, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					157.771, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[163689] = {
				{
					30.3429999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					58.2930000000001, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					89.8049999999998, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					117.721, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					145.612, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[163376] = {
				{
					52.6259999999998, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					110.838, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					169.112, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
			},
		}, -- [49]
		{
			[163334] = {
				{
					5.25900000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [1]
				{
					8.49499999999989, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [2]
				{
					11.3110000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [3]
				{
					19.4340000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [4]
				{
					23.087, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [5]
				{
					26.732, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [6]
				{
					30.3670000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [7]
				{
					34.0150000000003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [8]
				{
					37.6680000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [9]
				{
					41.2930000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [10]
				{
					49.7760000000003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [11]
				{
					53.4320000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [12]
				{
					58.2800000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [13]
				{
					61.9000000000001, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [14]
				{
					65.558, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [15]
				{
					69.1910000000003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [16]
				{
					77.6950000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [17]
				{
					81.3320000000003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [18]
				{
					84.9680000000003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [19]
				{
					88.6060000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [20]
				{
					92.2490000000003, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [21]
				{
					95.877, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [22]
				{
					99.5300000000002, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [23]
				{
					108.043, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [24]
				{
					111.685, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [25]
				{
					116.527, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [26]
				{
					120.168, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [27]
				{
					123.816, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [28]
				{
					127.467, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [29]
				{
					135.979, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [30]
				{
					139.624, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [31]
				{
					143.258, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [32]
				{
					146.883, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [33]
				{
					150.537, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [34]
				{
					154.191, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [35]
				{
					157.857, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [36]
				{
					166.335, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [37]
				{
					169.953, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [38]
				{
					172.73, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [39]
				{
					176.008, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [40]
				{
					179.662, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [41]
				{
					183.306, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [42]
				{
					186.942, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [43]
				{
					195.434, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [44]
				{
					199.071, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [45]
				{
					203.926, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [46]
				{
					207.567, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [47]
				{
					211.192, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [48]
				{
					214.845, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [49]
				{
					223.347, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [50]
				{
					226.985, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [51]
				{
					230.642, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [52]
				{
					234.268, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [53]
				{
					237.904, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [54]
				{
					241.559, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [55]
				{
					245.194, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [56]
				{
					253.679, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [57]
				{
					257.32, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [58]
				{
					262.194, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [59]
				{
					265.815, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [60]
				{
					269.443, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [61]
				{
					273.112, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [62]
				{
					281.607, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [63]
				{
					285.237, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [64]
				{
					288.893, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [65]
				{
					292.502, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [66]
				{
					296.134, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [67]
				{
					299.778, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [68]
				{
					303.432, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [69]
				{
					311.936, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [70]
				{
					315.582, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [71]
				{
					320.429, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [72]
				{
					324.082, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [73]
				{
					327.728, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [74]
				{
					331.376, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [75]
				{
					339.864, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [76]
				{
					343.502, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [77]
				{
					347.164, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [78]
				{
					350.79, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [79]
				{
					354.455, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [80]
				{
					358.1, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [81]
				{
					361.738, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [82]
				{
					370.23, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [83]
				{
					373.049, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [84]
				{
					376.294, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [85]
				{
					379.946, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [86]
				{
					383.591, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [87]
				{
					387.242, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [88]
				{
					390.902, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [89]
				{
					399.424, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [90]
				{
					403.47, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [91]
				{
					406.709, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [92]
				{
					409.536, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [93]
				{
					413.159, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [94]
				{
					416.788, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [95]
				{
					420.438, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [96]
				{
					428.934, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [97]
				{
					432.584, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [98]
				{
					436.229, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [99]
				{
					439.878, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [100]
				{
					443.498, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [101]
				{
					447.15, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [102]
				{
					455.634, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [103]
				{
					459.269, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [104]
				{
					462.908, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [105]
				{
					466.539, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [106]
				{
					470.206, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [107]
				{
					473.868, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [108]
				{
					477.508, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [109]
				{
					485.984, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [110]
				{
					489.629, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [111]
				{
					493.269, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [112]
				{
					496.92, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [113]
				{
					500.559, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [114]
				{
					504.216, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [115]
				{
					507.864, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [116]
				{
					516.393, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [117]
				{
					520.051, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [118]
				{
					523.301, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [119]
				{
					526.139, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [120]
				{
					529.773, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [121]
				{
					532.997, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [122]
				{
					537.03, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [123]
				{
					545.547, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [124]
				{
					549.171, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [125]
				{
					552.814, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [126]
				{
					556.451, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [127]
				{
					560.09, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [128]
				{
					563.734, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [129]
				{
					572.231, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [130]
				{
					575.874, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [131]
				{
					579.518, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [132]
				{
					583.153, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [133]
				{
					586.8, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [134]
				{
					590.434, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [135]
				{
					594.077, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [136]
				{
					602.561, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [137]
				{
					606.206, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [138]
				{
					609.835, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [139]
				{
					613.488, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [140]
				{
					617.123, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ryaalexia-BoreanTundra", -- [5]
				}, -- [141]
				{
					620.756, -- [1]
					"Neesa Nox", -- [2]
					163334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [142]
			},
			[163390] = {
				{
					14.0599999999999, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					44.4340000000002, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					72.3400000000002, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					102.67, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					130.609, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					160.972, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					190.095, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					217.993, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					248.335, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					276.259, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					306.578, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					334.499, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					364.88, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					394.051, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					423.576, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					450.286, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					480.626, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					511.017, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					540.174, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					566.885, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					597.205, -- [1]
					"Neesa Nox", -- [2]
					163390, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
			},
			[163665] = {
				{
					7.69399999999996, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.8800000000001, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					65.9659999999999, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					95.085, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					124.24, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					153.388, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					182.518, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					211.622, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					240.76, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					269.873, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					298.983, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					328.138, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					357.304, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					386.446, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					415.601, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					444.738, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					473.868, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					503.005, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					532.199, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					561.335, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					590.444, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					619.562, -- [1]
					"Makogg Emberblade", -- [2]
					163665, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
			},
			[165122] = {
				{
					4.55300000000034, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					14.2460000000001, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					17.893, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					21.1590000000001, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					24.806, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					28.4480000000003, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
				{
					32.0860000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					38.1660000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [8]
				{
					49.0500000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
				{
					52.7240000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
				{
					56.3499999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [11]
				{
					66.0480000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [12]
				{
					69.6890000000003, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [13]
				{
					73.3380000000002, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [14]
				{
					84.268, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [15]
				{
					87.884, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					93.953, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [17]
				{
					103.671, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [18]
				{
					107.323, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [19]
				{
					110.962, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [20]
				{
					114.595, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [21]
				{
					118.245, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [22]
				{
					124.322, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [23]
				{
					127.947, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [24]
				{
					131.606, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [25]
				{
					135.259, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [26]
				{
					138.903, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [27]
				{
					142.542, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [28]
				{
					146.182, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [29]
				{
					152.259, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [30]
				{
					155.907, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [31]
				{
					165.616, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [32]
				{
					169.249, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [33]
				{
					172.885, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [34]
				{
					176.518, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [35]
				{
					182.59, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [36]
				{
					186.241, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [37]
				{
					189.877, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [38]
				{
					193.522, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [39]
				{
					197.15, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [40]
				{
					200.789, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [41]
				{
					204.418, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [42]
				{
					210.487, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [43]
				{
					214.117, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [44]
				{
					217.763, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [45]
				{
					221.415, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [46]
				{
					225.064, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [47]
				{
					228.704, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [48]
				{
					232.356, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [49]
				{
					235.979, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [50]
				{
					242.061, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [51]
				{
					245.684, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [52]
				{
					249.318, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [53]
				{
					252.975, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [54]
				{
					256.609, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [55]
				{
					260.264, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [56]
				{
					263.913, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [57]
				{
					269.954, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [58]
				{
					273.608, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [59]
				{
					277.251, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [60]
				{
					280.887, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [61]
				{
					284.538, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [62]
				{
					288.172, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [63]
				{
					291.798, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [64]
				{
					297.856, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [65]
				{
					301.509, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [66]
				{
					305.146, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [67]
				{
					308.783, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [68]
				{
					312.428, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [69]
				{
					316.098, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [70]
				{
					319.704, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [71]
				{
					323.348, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [72]
				{
					329.443, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [73]
				{
					333.081, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [74]
				{
					336.722, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [75]
				{
					340.365, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [76]
				{
					348.868, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [77]
				{
					352.52, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [78]
				{
					358.586, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [79]
				{
					362.233, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [80]
				{
					371.931, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [81]
				{
					375.571, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [82]
				{
					379.23, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [83]
				{
					385.308, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [84]
				{
					388.962, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [85]
				{
					392.619, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [86]
				{
					396.258, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [87]
				{
					399.93, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [88]
				{
					403.961, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [89]
				{
					407.206, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [90]
				{
					411.252, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [91]
				{
					417.296, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [92]
				{
					420.944, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [93]
				{
					424.568, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [94]
				{
					428.207, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [95]
				{
					431.876, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [96]
				{
					435.504, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [97]
				{
					439.149, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [98]
				{
					445.213, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [99]
				{
					448.865, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [100]
				{
					452.503, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [101]
				{
					456.139, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [102]
				{
					459.771, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [103]
				{
					463.411, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [104]
				{
					467.046, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [105]
				{
					473.132, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [106]
				{
					476.789, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [107]
				{
					480.426, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [108]
				{
					484.068, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [109]
				{
					487.695, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [110]
				{
					491.345, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [111]
				{
					494.987, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [112]
				{
					498.623, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [113]
				{
					504.718, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [114]
				{
					508.373, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [115]
				{
					512.013, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [116]
				{
					515.677, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [117]
				{
					519.326, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [118]
				{
					522.988, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [119]
				{
					526.635, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [120]
				{
					532.681, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [121]
				{
					535.932, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [122]
				{
					539.975, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [123]
				{
					543.194, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [124]
				{
					547.255, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [125]
				{
					550.872, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [126]
				{
					555.735, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [127]
				{
					561.822, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [128]
				{
					565.456, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [129]
				{
					570.287, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [130]
				{
					573.94, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [131]
				{
					577.587, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [132]
				{
					581.237, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [133]
				{
					584.875, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [134]
				{
					590.922, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [135]
				{
					594.558, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [136]
				{
					598.21, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [137]
				{
					601.853, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [138]
				{
					605.477, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [139]
				{
					609.125, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [140]
				{
					612.773, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [141]
				{
					618.853, -- [1]
					"Ahri'ok Dugru", -- [2]
					165122, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [142]
			},
			[163379] = {
				{
					21.5709999999999, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					53.127, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					80.7090000000003, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					109.894, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					138.184, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					169.438, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					198.701, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					226.319, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					256.401, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					372.87, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					402.565, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					431.799, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					458.003, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					519.982, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					574.32, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					604.837, -- [1]
					"Bombsquad", -- [2]
					163379, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
			},
			[163689] = {
				{
					34.7339999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					61.4030000000003, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					90.5219999999999, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					120.881, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					148.808, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					179.16, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					207.067, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					238.627, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					266.53, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					294.43, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					326.013, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					355.169, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					381.874, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					413.881, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					441.79, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					469.709, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					501.279, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					529.267, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					558.375, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					587.502, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					615.413, -- [1]
					"Ahri'ok Dugru", -- [2]
					163689, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
			},
			[163376] = {
				{
					55.7650000000003, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					114.013, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					201.397, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					259.669, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					317.909, -- [1]
					"Neesa Nox", -- [2]
					163376, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
			},
		}, -- [50]
		{
			[166923] = {
				{
					46.9550000000004, -- [1]
					"Fleshrender Nok'gar", -- [2]
					166923, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					89.4560000000001, -- [1]
					"Fleshrender Nok'gar", -- [2]
					166923, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[164426] = {
				{
					37.2350000000001, -- [1]
					"Fleshrender Nok'gar", -- [2]
					164426, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					79.7340000000004, -- [1]
					"Fleshrender Nok'gar", -- [2]
					164426, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[164837] = {
				{
					29.3240000000001, -- [1]
					"Dreadfang", -- [2]
					164837, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [1]
			},
		}, -- [51]
		{
			[156954] = {
				{
					75.6420000000001, -- [1]
					"Teron'gor", -- [2]
					156954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[157168] = {
				{
					9.73299999999995, -- [1]
					"Unknown", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [1]
				{
					21.7449999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [2]
				{
					33.7749999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [3]
				{
					45.7929999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [4]
				{
					57.8139999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [5]
				{
					69.8309999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [6]
				{
					76.7729999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [7]
				{
					89.826, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [8]
				{
					101.845, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [9]
			},
			[156857] = {
				{
					16.579, -- [1]
					"Teron'gor", -- [2]
					156857, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[156829] = {
				{
					1.20000000000005, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					8.077, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					11.7190000000001, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					14.154, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					30.3429999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					32.7770000000001, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
				{
					36.0049999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					38.4389999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [8]
				{
					40.857, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
				{
					43.27, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
				{
					54.2049999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [11]
				{
					56.6309999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [12]
				{
					59.0609999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [13]
				{
					64.2929999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [14]
				{
					78.8699999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [15]
				{
					94.2429999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					96.6759999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [17]
				{
					99.0899999999999, -- [1]
					"Teron'gor", -- [2]
					156829, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [18]
			},
			[164841] = {
				{
					47.1230000000001, -- [1]
					"Teron'gor", -- [2]
					164841, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					62.086, -- [1]
					"Teron'gor", -- [2]
					164841, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					75.432, -- [1]
					"Teron'gor", -- [2]
					164841, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					92.0449999999999, -- [1]
					"Teron'gor", -- [2]
					164841, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[156925] = {
				{
					43.7, -- [1]
					"Teron'gor", -- [2]
					156925, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [1]
				{
					64.7059999999999, -- [1]
					"Teron'gor", -- [2]
					156925, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [2]
				{
					86.1689999999999, -- [1]
					"Teron'gor", -- [2]
					156925, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [3]
			},
			[156854] = {
				{
					18.2079999999999, -- [1]
					"Teron'gor", -- [2]
					156854, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [1]
				{
					47.3329999999999, -- [1]
					"Teron'gor", -- [2]
					156854, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [2]
				{
					65.9179999999999, -- [1]
					"Teron'gor", -- [2]
					156854, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [3]
				{
					84.1509999999999, -- [1]
					"Teron'gor", -- [2]
					156854, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [4]
			},
			[156842] = {
				{
					8.50700000000006, -- [1]
					"Teron'gor", -- [2]
					156842, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[156921] = {
				{
					32.799, -- [1]
					"Teron'gor", -- [2]
					156921, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [1]
				{
					45.319, -- [1]
					"Teron'gor", -- [2]
					156921, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [2]
				{
					59.8779999999999, -- [1]
					"Teron'gor", -- [2]
					156921, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [3]
				{
					73.2079999999999, -- [1]
					"Teron'gor", -- [2]
					156921, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					89.806, -- [1]
					"Teron'gor", -- [2]
					156921, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [5]
			},
		}, -- [52]
		{
			[153727] = {
				{
					53.396, -- [1]
					"Unknown", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					53.396, -- [1]
					"Unknown", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					53.396, -- [1]
					"Unknown", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					53.396, -- [1]
					"Unknown", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					144.47, -- [1]
					"Fel Spark", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					144.47, -- [1]
					"Fel Spark", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					144.47, -- [1]
					"Fel Spark", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					144.47, -- [1]
					"Fel Spark", -- [2]
					153727, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
			[153234] = {
				{
					16.9780000000001, -- [1]
					"Azzakel", -- [2]
					153234, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					94.6290000000001, -- [1]
					"Azzakel", -- [2]
					153234, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
			[177763] = {
				{
					149.986, -- [1]
					"Unknown", -- [2]
					177763, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					149.986, -- [1]
					"Unknown", -- [2]
					177763, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153499] = {
				{
					53.0720000000001, -- [1]
					"Azzakel", -- [2]
					153499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					144.15, -- [1]
					"Azzakel", -- [2]
					153499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[154221] = {
				{
					52.1800000000001, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					52.992, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [2]
				{
					60.4790000000001, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [3]
				{
					61.0940000000001, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [4]
				{
					61.481, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [5]
				{
					69.145, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [6]
				{
					69.963, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [7]
				{
					78.0699999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [8]
				{
					86.558, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [9]
				{
					95.076, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [10]
				{
					103.537, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [11]
				{
					112.045, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [12]
				{
					120.54, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [13]
				{
					129.046, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [14]
				{
					137.55, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [15]
				{
					141.193, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [16]
				{
					144.03, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [17]
				{
					146.074, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [18]
				{
					149.328, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [19]
				{
					149.758, -- [1]
					"Cackling Pyromaniac", -- [2]
					154221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [20]
			},
			[153762] = {
				{
					34.4739999999999, -- [1]
					"Unknown", -- [2]
					153762, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					125.502, -- [1]
					"Claws of Argus", -- [2]
					153762, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153764] = {
				{
					34.4550000000002, -- [1]
					"Azzakel", -- [2]
					153764, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					125.482, -- [1]
					"Azzakel", -- [2]
					153764, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[157173] = {
				{
					50.1320000000001, -- [1]
					"Felguard", -- [2]
					157173, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					143.63, -- [1]
					"Felguard", -- [2]
					157173, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153396] = {
				{
					20.3150000000001, -- [1]
					"Azzakel", -- [2]
					153396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [1]
				{
					59.135, -- [1]
					"Azzakel", -- [2]
					153396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [2]
				{
					79.7840000000001, -- [1]
					"Azzakel", -- [2]
					153396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [3]
				{
					100.398, -- [1]
					"Azzakel", -- [2]
					153396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [4]
			},
			[224729] = {
				{
					139.245, -- [1]
					"Felguard", -- [2]
					224729, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [1]
			},
		}, -- [53]
		{
			[154477] = {
				{
					5.57099999999991, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [1]
				{
					14.064, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [2]
				{
					22.5649999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [3]
				{
					35.9279999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pisajerk-Area52", -- [5]
				}, -- [4]
				{
					44.432, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [5]
				{
					53.1969999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [6]
				{
					61.4569999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Castorz-Area52", -- [5]
				}, -- [7]
				{
					69.9459999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [8]
				{
					86.9569999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [9]
			},
			[157794] = {
				{
					3.25199999999995, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					9.37099999999998, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					9.37099999999998, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					12.202, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					17.063, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					22.747, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					22.747, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					25.579, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					31.194, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					36.096, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					36.096, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					38.9379999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					45.002, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					49.4649999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					49.4649999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					52.299, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					58.3689999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					62.8340000000001, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					62.8340000000001, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					65.664, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					71.7539999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					76.185, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					76.185, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					79.0249999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					85.1030000000001, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					89.5450000000001, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					89.5450000000001, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					92.3910000000001, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					94.7629999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					94.7629999999999, -- [1]
					"Sargerei Magus", -- [2]
					157794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
			},
			[157168] = {
				{
					94.7629999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Chaosßolt-Area52", -- [5]
				}, -- [1]
				{
					94.7629999999999, -- [1]
					"Felborne Abyssal", -- [2]
					157168, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laazuli-Saurfang", -- [5]
				}, -- [2]
			},
			[155327] = {
				{
					30.79, -- [1]
					"Soulbinder Nyami", -- [2]
					155327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					81.818, -- [1]
					"Soulbinder Nyami", -- [2]
					155327, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153994] = {
				{
					52.693, -- [1]
					"Soulbinder Nyami", -- [2]
					153994, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[176931] = {
				{
					56.568, -- [1]
					"Malefic Defender", -- [2]
					176931, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Arakzinul", -- [5]
				}, -- [1]
				{
					59.009, -- [1]
					"Malefic Defender", -- [2]
					176931, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					73.589, -- [1]
					"Malefic Defender", -- [2]
					176931, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
			},
			[165744] = {
				{
					1.94200000000001, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					6.76199999999994, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					11.631, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					16.4789999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					22.5649999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					27.502, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					31.064, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					34.709, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					40.78, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					45.653, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					51.7169999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					56.568, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					60.2259999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					68.7359999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					76.008, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					80.8779999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					85.749, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					90.617, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					94.7629999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
			},
			[153477] = {
				{
					23.784, -- [1]
					"Soulbinder Nyami", -- [2]
					153477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					74.81, -- [1]
					"Soulbinder Nyami", -- [2]
					153477, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[154262] = {
				{
					55.194, -- [1]
					"Spiteful Arbiter", -- [2]
					154262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					72.375, -- [1]
					"Spiteful Arbiter", -- [2]
					154262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					88.1669999999999, -- [1]
					"Spiteful Arbiter", -- [2]
					154262, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[165747] = {
				{
					63.884, -- [1]
					"Sargerei Arbiter", -- [2]
					165747, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Sargerei Arbiter", -- [5]
				}, -- [1]
				{
					66.3039999999999, -- [1]
					"Sargerei Arbiter", -- [2]
					165747, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Sargerei Magus", -- [5]
				}, -- [2]
				{
					71.154, -- [1]
					"Sargerei Arbiter", -- [2]
					165747, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Sargerei Magus", -- [5]
				}, -- [3]
				{
					73.589, -- [1]
					"Sargerei Arbiter", -- [2]
					165747, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Sargerei Defender", -- [5]
				}, -- [4]
			},
			[154218] = {
				{
					65.874, -- [1]
					"Spiteful Arbiter", -- [2]
					154218, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					79.9549999999999, -- [1]
					"Spiteful Arbiter", -- [2]
					154218, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
			[154415] = {
				{
					9.98899999999992, -- [1]
					"Soulbinder Nyami", -- [2]
					154415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					55.194, -- [1]
					"Soulbinder Nyami", -- [2]
					154415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					61.0129999999999, -- [1]
					"Soulbinder Nyami", -- [2]
					154415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					67.1030000000001, -- [1]
					"Soulbinder Nyami", -- [2]
					154415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					73.163, -- [1]
					"Soulbinder Nyami", -- [2]
					154415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					88.9680000000001, -- [1]
					"Soulbinder Nyami", -- [2]
					154415, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
			},
			[167092] = {
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					94.7629999999999, -- [1]
					"Cackling Pyromaniac", -- [2]
					167092, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
			},
		}, -- [54]
		{
			[153002] = {
				{
					29.646, -- [1]
					"Vigilant Kaathar", -- [2]
					153002, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					78.201, -- [1]
					"Vigilant Kaathar", -- [2]
					153002, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[152954] = {
				{
					7.08100000000002, -- [1]
					"Vigilant Kaathar", -- [2]
					152954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					15.58, -- [1]
					"Vigilant Kaathar", -- [2]
					152954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					24.071, -- [1]
					"Vigilant Kaathar", -- [2]
					152954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					55.6370000000001, -- [1]
					"Vigilant Kaathar", -- [2]
					152954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					65.335, -- [1]
					"Vigilant Kaathar", -- [2]
					152954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					73.838, -- [1]
					"Vigilant Kaathar", -- [2]
					152954, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[157505] = {
				{
					104.157, -- [1]
					"Sargerei Magus", -- [2]
					157505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[157465] = {
				{
					26.732, -- [1]
					"Vigilant Kaathar", -- [2]
					157465, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					63.9, -- [1]
					"Vigilant Kaathar", -- [2]
					157465, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					101.523, -- [1]
					"Vigilant Kaathar", -- [2]
					157465, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[155646] = {
				{
					2.41999999999996, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					7.66100000000006, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					12.126, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					16.0700000000001, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					19.41, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					27.917, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					46.078, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					51.0120000000001, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					56.127, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					60.691, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					65.901, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					69.174, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					74.345, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					94.664, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					98.3010000000001, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					101.952, -- [1]
					"Vigilant Kaathar", -- [2]
					155646, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
			},
			[153006] = {
				{
					39.449, -- [1]
					"Vigilant Kaathar", -- [2]
					153006, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					87.951, -- [1]
					"Vigilant Kaathar", -- [2]
					153006, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [55]
		{
			[154396] = {
				{
					8.40900000000693, -- [1]
					"High Sage Viryx", -- [2]
					154396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					31.4930000000022, -- [1]
					"High Sage Viryx", -- [2]
					154396, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
		}, -- [56]
		{
			[153828] = {
				{
					106.661, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					116.408000000003, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					117.614000000001, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					123.692999999999, -- [1]
					"Solar Flare", -- [2]
					153828, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[169810] = {
				{
					36.2700000000041, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.1700000000055, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					53.234000000004, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					64.1880000000019, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					69.0380000000005, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					82.375, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					95.7380000000048, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					106.661, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					112.725000000006, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					117.614000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					122.471000000005, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					162.559000000001, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					166.197, -- [1]
					"Solar Flare", -- [2]
					169810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
			},
			[153810] = {
				{
					15.3970000000045, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					65.1920000000027, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					84.6020000000062, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					116.178, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					135.629000000001, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					170.839, -- [1]
					"Rukhran", -- [2]
					153810, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[159382] = {
				{
					28.5430000000051, -- [1]
					"Rukhran", -- [2]
					159382, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					100.166000000005, -- [1]
					"Rukhran", -- [2]
					159382, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[153896] = {
				{
					52.8000000000029, -- [1]
					"Rukhran", -- [2]
					153896, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					61.3220000000001, -- [1]
					"Rukhran", -- [2]
					153896, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					122.038, -- [1]
					"Rukhran", -- [2]
					153896, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[153794] = {
				{
					11.7610000000059, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					22.6790000000037, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					48.1710000000021, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hati", -- [5]
				}, -- [3]
				{
					59.1000000000058, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Getnasty", -- [5]
				}, -- [4]
				{
					70.0440000000017, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					80.961000000003, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
				{
					91.8780000000043, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					118.627, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hati", -- [5]
				}, -- [8]
				{
					130.762000000002, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
				{
					141.705000000002, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
				{
					156.272000000005, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [11]
				{
					167.204000000005, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [12]
				{
					178.138000000006, -- [1]
					"Rukhran", -- [2]
					153794, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [13]
			},
		}, -- [57]
		{
			[154135] = {
				{
					23.2649999999994, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.525999999998, -- [1]
					"Araknath", -- [2]
					154135, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[154113] = {
				{
					7.67500000000291, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					14.961000000003, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					30.7509999999966, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					55.0469999999987, -- [1]
					"Araknath", -- [2]
					154113, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[154110] = {
				{
					40.4519999999975, -- [1]
					"Araknath", -- [2]
					154110, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [58]
		{
			[153757] = {
				{
					10.5790000000052, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					26.3820000000051, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					42.2000000000044, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					58.0169999999998, -- [1]
					"Ranjit", -- [2]
					153757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[153544] = {
				{
					4.31000000000495, -- [1]
					"Ranjit", -- [2]
					153544, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[165731] = {
				{
					15.476999999999, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ballzacky-BleedingHollow", -- [5]
				}, -- [1]
				{
					68.9400000000023, -- [1]
					"Ranjit", -- [2]
					165731, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Getnasty", -- [5]
				}, -- [2]
			},
			[156793] = {
				{
					31.5299999999988, -- [1]
					"Ranjit", -- [2]
					156793, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[153315] = {
				{
					10.3680000000022, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					24.9639999999999, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					63.8660000000018, -- [1]
					"Ranjit", -- [2]
					153315, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[165733] = {
				{
					15.4619999999995, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					68.9400000000023, -- [1]
					"Ranjit", -- [2]
					165733, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [59]
		{
			[268007] = {
				{
					6.99399999999878, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					13.4689999999973, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					20.7180000000008, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					29.2379999999976, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					66.4629999999961, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [5]
				{
					73.5249999999942, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Jarlaxyl-Kilrogg", -- [5]
				}, -- [6]
				{
					122.703999999998, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [7]
				{
					132.447, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [8]
				{
					140.945999999996, -- [1]
					"Heart Guardian", -- [2]
					268007, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
			},
			[273677] = {
				{
					58.4889999999941, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					58.4889999999941, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					58.4889999999941, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					58.4889999999941, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					113.137999999999, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					113.137999999999, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					113.137999999999, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					113.137999999999, -- [1]
					"Hoodoo Hexer", -- [2]
					273677, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
		}, -- [60]
		{
			[266512] = {
				{
					51.0939999999973, -- [1]
					"Galvazzt", -- [2]
					266512, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[267278] = {
				{
					67.5840000000026, -- [1]
					"Polarized Spire", -- [2]
					267278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [1]
				{
					67.5840000000026, -- [1]
					"Polarized Spire", -- [2]
					267278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					67.5840000000026, -- [1]
					"Polarized Spire", -- [2]
					267278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Itsadot-Terokkar", -- [5]
				}, -- [3]
				{
					67.5840000000026, -- [1]
					"Polarized Spire", -- [2]
					267278, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [4]
			},
			[265986] = {
				{
					7.19400000000314, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					17.3329999999987, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					27.4720000000016, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					35.1699999999983, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					43.25, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					51.3280000000013, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					57.4349999999977, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					63.5, -- [1]
					"Energy Core", -- [2]
					265986, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
		}, -- [61]
		{
			[263912] = {
				{
					8.55000000000291, -- [1]
					"Merektha", -- [2]
					263912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					105.828000000001, -- [1]
					"Merektha", -- [2]
					263912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[267027] = {
				{
					21.5630000000019, -- [1]
					"Venomous Ophidian", -- [2]
					267027, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					21.5630000000019, -- [1]
					"Venomous Ophidian", -- [2]
					267027, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					22.7720000000045, -- [1]
					"Venomous Ophidian", -- [2]
					267027, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					24.7839999999997, -- [1]
					"Venomous Ophidian", -- [2]
					267027, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
			},
			[263914] = {
				{
					96.8190000000032, -- [1]
					"Merektha", -- [2]
					263914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[264235] = {
				{
					57.7520000000004, -- [1]
					"Egg", -- [2]
					264235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					57.7520000000004, -- [1]
					"Egg", -- [2]
					264235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					57.7520000000004, -- [1]
					"Egg", -- [2]
					264235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					57.7520000000004, -- [1]
					"Egg", -- [2]
					264235, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[267047] = {
				{
					57.7839999999997, -- [1]
					"Sand-crusted Striker", -- [2]
					267047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					57.7839999999997, -- [1]
					"Sand-crusted Striker", -- [2]
					267047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					57.7839999999997, -- [1]
					"Sand-crusted Striker", -- [2]
					267047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					57.7839999999997, -- [1]
					"Sand-crusted Striker", -- [2]
					267047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[264368] = {
				{
					0.554000000003725, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					0.554000000003725, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					0.554000000003725, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					0.554000000003725, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					47.5789999999979, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					47.5789999999979, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					47.5789999999979, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					47.5789999999979, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					88.8380000000034, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					88.8380000000034, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					88.8380000000034, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					88.8380000000034, -- [1]
					"Egg", -- [2]
					264368, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
			},
			[269116] = {
				{
					15.8700000000026, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.5149999999994, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					63.198000000004, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					77.3450000000012, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					110.915000000001, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					112.880000000005, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[264234] = {
				{
					16.512999999999, -- [1]
					"Egg", -- [2]
					264234, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.512999999999, -- [1]
					"Egg", -- [2]
					264234, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					16.512999999999, -- [1]
					"Egg", -- [2]
					264234, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					16.512999999999, -- [1]
					"Egg", -- [2]
					264234, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [62]
		{
			[263371] = {
				{
					23.3439999999973, -- [1]
					"Aspix", -- [2]
					263371, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [1]
			},
			[263309] = {
				{
					12.9159999999974, -- [1]
					"Adderis", -- [2]
					263309, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					28.7119999999995, -- [1]
					"Adderis", -- [2]
					263309, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [2]
				{
					42.0959999999977, -- [1]
					"Adderis", -- [2]
					263309, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Justcùz-Vashj", -- [5]
				}, -- [3]
				{
					54.2379999999976, -- [1]
					"Adderis", -- [2]
					263309, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quentyn-Turalyon", -- [5]
				}, -- [4]
			},
			[269116] = {
				{
					70.464, -- [1]
					"Imbued Stormcaller", -- [2]
					269116, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[263318] = {
				{
					5.8949999999968, -- [1]
					"Aspix", -- [2]
					263318, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					9.24199999999837, -- [1]
					"Aspix", -- [2]
					263318, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					12.8940000000002, -- [1]
					"Aspix", -- [2]
					263318, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					26.2679999999964, -- [1]
					"Aspix", -- [2]
					263318, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					29.9179999999978, -- [1]
					"Aspix", -- [2]
					263318, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Itsadot-Terokkar", -- [5]
				}, -- [5]
				{
					33.5749999999971, -- [1]
					"Aspix", -- [2]
					263318, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Itsadot-Terokkar", -- [5]
				}, -- [6]
			},
		}, -- [63]
		{
			[40504] = {
				{
					9.43399999999747, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [1]
				{
					17.4589999999989, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [2]
				{
					27.4739999999983, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [3]
				{
					37.4829999999965, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [4]
				{
					51.5379999999968, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [5]
				{
					61.5689999999995, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [6]
			},
		}, -- [64]
		{
			[21707] = {
				{
					22.8319999999985, -- [1]
					"Noxxion", -- [2]
					21707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					65.2849999999999, -- [1]
					"Noxxion", -- [2]
					21707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[21687] = {
				{
					8.24599999999919, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.7510000000002, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					28.9039999999986, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					38.5930000000008, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					48.2989999999991, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					61.6369999999988, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[10966] = {
				{
					10.6820000000007, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [1]
				{
					31.3329999999987, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [2]
				{
					44.6729999999989, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kikinas-Tichondrius", -- [5]
				}, -- [3]
			},
		}, -- [65]
		{
			[113136] = {
				{
					1.86700000000201, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [1]
				{
					4.20000000000073, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [2]
				{
					6.61600000000181, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [3]
				{
					9.04900000000271, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [4]
				{
					11.4820000000036, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [5]
				{
					16.3410000000004, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [6]
				{
					18.7630000000027, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [7]
				{
					21.1800000000003, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [8]
				{
					23.617000000002, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [9]
				{
					26.0450000000019, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [10]
				{
					30.8820000000014, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [11]
				{
					33.2960000000021, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [12]
				{
					35.7140000000036, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [13]
				{
					38.1400000000031, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [14]
				{
					40.5630000000019, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [15]
				{
					42.9930000000022, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [16]
				{
					45.4260000000031, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [17]
				{
					47.8400000000002, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [18]
				{
					52.7300000000032, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [19]
				{
					55.130000000001, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [20]
				{
					57.5710000000036, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [21]
				{
					59.987000000001, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [22]
				{
					64.8400000000002, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [23]
				{
					67.260000000002, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [24]
				{
					69.6890000000021, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [25]
				{
					72.122000000003, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [26]
				{
					74.5490000000027, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [27]
				{
					76.987000000001, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [28]
				{
					79.4180000000015, -- [1]
					"Darkmaster Gandling", -- [2]
					113136, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [29]
			},
			[113141] = {
				{
					13.8960000000006, -- [1]
					"Darkmaster Gandling", -- [2]
					113141, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [1]
				{
					28.4640000000036, -- [1]
					"Darkmaster Gandling", -- [2]
					113141, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [2]
				{
					50.2740000000013, -- [1]
					"Darkmaster Gandling", -- [2]
					113141, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [3]
				{
					62.4110000000001, -- [1]
					"Darkmaster Gandling", -- [2]
					113141, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [4]
			},
			[130260] = {
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [1]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [2]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [3]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [4]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [5]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [6]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [7]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [8]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [9]
				{
					52.130000000001, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [10]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [11]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [12]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [13]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [14]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [15]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [16]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [17]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [18]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [19]
				{
					53.7300000000032, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [20]
				{
					55.3640000000014, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [21]
				{
					56.9640000000036, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [22]
				{
					56.9640000000036, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [23]
				{
					56.9640000000036, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [24]
				{
					56.9640000000036, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [25]
				{
					56.9640000000036, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [26]
				{
					58.5800000000018, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [27]
				{
					65.4520000000011, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [28]
				{
					65.4520000000011, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [29]
				{
					69.099000000002, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [30]
				{
					70.3160000000025, -- [1]
					"Expired Test Subject", -- [2]
					130260, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [31]
			},
		}, -- [66]
		{
			[111775] = {
				{
					12.7350000000006, -- [1]
					"Lilian Voss", -- [2]
					111775, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[111649] = {
				{
					37.6910000000007, -- [1]
					"Unknown", -- [2]
					111649, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [67]
		{
			[113999] = {
				{
					6.46900000000096, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					13.7510000000002, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Loanz-Silvermoon", -- [5]
				}, -- [2]
				{
					21.4369999999999, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holyhat-Silvermoon", -- [5]
				}, -- [3]
				{
					32.362000000001, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					39.634, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Loanz-Silvermoon", -- [5]
				}, -- [5]
				{
					50.5460000000003, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fallingstars-MoonGuard", -- [5]
				}, -- [6]
				{
					60.2690000000002, -- [1]
					"Rattlegore", -- [2]
					113999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
			},
		}, -- [68]
		{
			[114062] = {
				{
					10.1409999999996, -- [1]
					"Jandice Barov", -- [2]
					114062, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.8359999999993, -- [1]
					"Jandice Barov", -- [2]
					114062, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					62.2839999999997, -- [1]
					"Jandice Barov", -- [2]
					114062, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[113866] = {
				{
					22.5569999999989, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					24.0919999999987, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					25.6459999999988, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					26.0659999999989, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					27.9049999999988, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					28.9229999999989, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					50.6779999999999, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					51.0479999999989, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					52.1679999999997, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					52.3899999999994, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					53.464, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					53.7599999999984, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					54.8709999999992, -- [1]
					"Jandice Barov", -- [2]
					113866, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
			},
		}, -- [69]
		{
			[111606] = {
				{
					27.9349999999995, -- [1]
					"Instructor Chillheart", -- [2]
					111606, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[111610] = {
				{
					16.9950000000008, -- [1]
					"Instructor Chillheart", -- [2]
					111610, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[111854] = {
				{
					2.50200000000041, -- [1]
					"Instructor Chillheart", -- [2]
					111854, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [70]
		{
			[22995] = {
				{
					93.8050000000003, -- [1]
					"Prince Tortheldrin", -- [2]
					22995, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Wickett-Winterhoof", -- [5]
				}, -- [1]
			},
			[22920] = {
				{
					11.4490000000005, -- [1]
					"Prince Tortheldrin", -- [2]
					22920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [1]
				{
					38.0799999999999, -- [1]
					"Prince Tortheldrin", -- [2]
					22920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Wickett-Winterhoof", -- [5]
				}, -- [2]
				{
					59.871000000001, -- [1]
					"Prince Tortheldrin", -- [2]
					22920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caliento-TheScryers", -- [5]
				}, -- [3]
				{
					92.5949999999994, -- [1]
					"Prince Tortheldrin", -- [2]
					22920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kosh-Lethon", -- [5]
				}, -- [4]
				{
					107.147999999999, -- [1]
					"Prince Tortheldrin", -- [2]
					22920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Wickett-Winterhoof", -- [5]
				}, -- [5]
			},
			[15589] = {
				{
					14.4570000000003, -- [1]
					"Prince Tortheldrin", -- [2]
					15589, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.4089999999997, -- [1]
					"Prince Tortheldrin", -- [2]
					15589, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					45.339, -- [1]
					"Prince Tortheldrin", -- [2]
					15589, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					84.1200000000008, -- [1]
					"Prince Tortheldrin", -- [2]
					15589, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[13787] = {
				{
					110.003000000001, -- [1]
					"Gordok Warlock", -- [2]
					13787, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[3391] = {
				{
					13.8670000000002, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					22.3490000000002, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					29.6059999999998, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					34.4500000000007, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					39.2900000000009, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					44.1239999999998, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					50.1820000000007, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					86.5460000000003, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					105.933000000001, -- [1]
					"Prince Tortheldrin", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[20537] = {
				{
					17.509, -- [1]
					"Prince Tortheldrin", -- [2]
					20537, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					51.4120000000003, -- [1]
					"Prince Tortheldrin", -- [2]
					20537, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caliento-TheScryers", -- [5]
				}, -- [2]
				{
					95.027, -- [1]
					"Prince Tortheldrin", -- [2]
					20537, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
			},
		}, -- [71]
		{
			[22950] = {
				{
					14.25, -- [1]
					"Immol'thar", -- [2]
					22950, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [1]
				{
					38.4940000000006, -- [1]
					"Immol'thar", -- [2]
					22950, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [2]
				{
					62.732, -- [1]
					"Immol'thar", -- [2]
					22950, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caliento-TheScryers", -- [5]
				}, -- [3]
			},
			[22909] = {
				{
					13.003999999999, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [1]
				{
					29.5990000000002, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Caliento-TheScryers", -- [5]
				}, -- [2]
				{
					44.5249999999996, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kosh-Lethon", -- [5]
				}, -- [3]
				{
					53.1180000000004, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					59.8809999999994, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [5]
				{
					65.9549999999999, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
				{
					70.8089999999993, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kosh-Lethon", -- [5]
				}, -- [7]
				{
					72.4300000000003, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [8]
				{
					78.4879999999994, -- [1]
					"Eye of Immol'thar", -- [2]
					22909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [9]
			},
			[18968] = {
				{
					51.018, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [1]
				{
					53.8400000000001, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [2]
				{
					63.9509999999991, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [3]
				{
					70.4269999999997, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [4]
				{
					80.5329999999995, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [5]
				{
					91.0079999999998, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [6]
				{
					91.0079999999998, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [7]
				{
					91.0079999999998, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [8]
				{
					91.0079999999998, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [9]
			},
			[12550] = {
				{
					89.4310000000005, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					91.0079999999998, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[22899] = {
				{
					9.39099999999962, -- [1]
					"Immol'thar", -- [2]
					22899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.1659999999993, -- [1]
					"Immol'thar", -- [2]
					22899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					39.6959999999999, -- [1]
					"Immol'thar", -- [2]
					22899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					50.6080000000002, -- [1]
					"Immol'thar", -- [2]
					22899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					68.7960000000003, -- [1]
					"Immol'thar", -- [2]
					22899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					79.7309999999998, -- [1]
					"Immol'thar", -- [2]
					22899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[16128] = {
				{
					8.17100000000028, -- [1]
					"Immol'thar", -- [2]
					16128, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [1]
				{
					25.1659999999993, -- [1]
					"Immol'thar", -- [2]
					16128, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [2]
				{
					54.241, -- [1]
					"Immol'thar", -- [2]
					16128, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [3]
				{
					70.4269999999997, -- [1]
					"Immol'thar", -- [2]
					16128, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [4]
			},
			[18100] = {
				{
					62.3320000000003, -- [1]
					"Eldreth Apparition", -- [2]
					18100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					67.1859999999997, -- [1]
					"Eldreth Apparition", -- [2]
					18100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					69.616, -- [1]
					"Eldreth Apparition", -- [2]
					18100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[5568] = {
				{
					7.36499999999978, -- [1]
					"Immol'thar", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					17.9009999999998, -- [1]
					"Immol'thar", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					26.3819999999996, -- [1]
					"Immol'thar", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					34.8500000000004, -- [1]
					"Immol'thar", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					59.0910000000004, -- [1]
					"Immol'thar", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					74.8709999999992, -- [1]
					"Immol'thar", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[8269] = {
				{
					44.9670000000006, -- [1]
					"Immol'thar", -- [2]
					8269, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [72]
		{
			[17156] = {
				{
					8.50900000000002, -- [1]
					"Ferra", -- [2]
					17156, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [1]
				{
					21.3209999999999, -- [1]
					"Ferra", -- [2]
					17156, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [2]
				{
					33.3529999999992, -- [1]
					"Ferra", -- [2]
					17156, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [3]
				{
					47.3909999999996, -- [1]
					"Ferra", -- [2]
					17156, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kosh-Lethon", -- [5]
				}, -- [4]
				{
					72.8999999999996, -- [1]
					"Ferra", -- [2]
					17156, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kosh-Lethon", -- [5]
				}, -- [5]
			},
			[18968] = {
				{
					145.535, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [1]
				{
					150.776, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [2]
				{
					152.388000000001, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [3]
				{
					162.897999999999, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [4]
				{
					163.700000000001, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [5]
				{
					165.002, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [6]
				{
					165.002, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [7]
				{
					165.002, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [8]
				{
					165.002, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [9]
				{
					165.002, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Seether", -- [5]
				}, -- [10]
				{
					165.002, -- [1]
					"Eldreth Seether", -- [2]
					18968, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Sorcerer", -- [5]
				}, -- [11]
			},
			[12550] = {
				{
					75.1229999999996, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					77.5529999999999, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					111.548000000001, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					117.603999999999, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					135.831, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					140.686, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					165.002, -- [1]
					"Arcane Feedback", -- [2]
					12550, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[22910] = {
				{
					7.6239999999998, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.8979999999992, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					51.2659999999996, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					67.0689999999995, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					82.8140000000003, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					105.892, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					126.518, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					142.290999999999, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					161.687, -- [1]
					"Illyanna Ravenoak", -- [2]
					22910, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[22914] = {
				{
					13.6839999999993, -- [1]
					"Illyanna Ravenoak", -- [2]
					22914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.5329999999995, -- [1]
					"Illyanna Ravenoak", -- [2]
					22914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					58.5779999999995, -- [1]
					"Illyanna Ravenoak", -- [2]
					22914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					90.1049999999996, -- [1]
					"Illyanna Ravenoak", -- [2]
					22914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					108.317999999999, -- [1]
					"Illyanna Ravenoak", -- [2]
					22914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					145.936, -- [1]
					"Illyanna Ravenoak", -- [2]
					22914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[22911] = {
				{
					3.98699999999917, -- [1]
					"Ferra", -- [2]
					22911, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Balu-Stormrage", -- [5]
				}, -- [1]
			},
			[7154] = {
				{
					45.509, -- [1]
					"Eldreth Spectre", -- [2]
					7154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ferra", -- [5]
				}, -- [1]
				{
					52.7770000000001, -- [1]
					"Eldreth Spectre", -- [2]
					7154, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ferra", -- [5]
				}, -- [2]
			},
			[30933] = {
				{
					17.3169999999991, -- [1]
					"Illyanna Ravenoak", -- [2]
					30933, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					41.5840000000007, -- [1]
					"Illyanna Ravenoak", -- [2]
					30933, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					75.5329999999995, -- [1]
					"Illyanna Ravenoak", -- [2]
					30933, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					94.9359999999997, -- [1]
					"Illyanna Ravenoak", -- [2]
					30933, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[17201] = {
				{
					33.1129999999994, -- [1]
					"Eldreth Spectre", -- [2]
					17201, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eldreth Spectre", -- [5]
				}, -- [1]
				{
					58.2109999999993, -- [1]
					"Eldreth Spectre", -- [2]
					17201, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ferra", -- [5]
				}, -- [2]
			},
			[20735] = {
				{
					1.68299999999908, -- [1]
					"Illyanna Ravenoak", -- [2]
					20735, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					5.19599999999991, -- [1]
					"Illyanna Ravenoak", -- [2]
					20735, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					62.2090000000008, -- [1]
					"Illyanna Ravenoak", -- [2]
					20735, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Wolf", -- [5]
				}, -- [3]
			},
		}, -- [73]
		{
			[9256] = {
				{
					27.4939999999988, -- [1]
					"High Inquisitor Whitemane", -- [2]
					9256, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[9232] = {
				{
					34.369999999999, -- [1]
					"High Inquisitor Whitemane", -- [2]
					9232, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[111107] = {
				{
					72.4279999999999, -- [1]
					"Scarlet Judicator", -- [2]
					111107, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Konkey-AeriePeak", -- [5]
				}, -- [1]
			},
			[115876] = {
				{
					38.0399999999991, -- [1]
					"Commander Durand", -- [2]
					115876, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[127399] = {
				{
					39.6589999999997, -- [1]
					"High Inquisitor Whitemane", -- [2]
					127399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"High Inquisitor Whitemane", -- [5]
				}, -- [1]
			},
			[111670] = {
				{
					22.5279999999984, -- [1]
					"Scarlet Zealot", -- [2]
					111670, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					79.0019999999986, -- [1]
					"Scarlet Zealot", -- [2]
					111670, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[114848] = {
				{
					26.3459999999996, -- [1]
					"High Inquisitor Whitemane", -- [2]
					114848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Konkey-AeriePeak", -- [5]
				}, -- [1]
				{
					42.1619999999984, -- [1]
					"High Inquisitor Whitemane", -- [2]
					114848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Julebug-Proudmoore", -- [5]
				}, -- [2]
				{
					51.8639999999996, -- [1]
					"High Inquisitor Whitemane", -- [2]
					114848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Konkey-AeriePeak", -- [5]
				}, -- [3]
			},
		}, -- [74]
		{
			[114807] = {
				{
					20.2369999999992, -- [1]
					"Brother Korloff", -- [2]
					114807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[113764] = {
				{
					10.8799999999992, -- [1]
					"Brother Korloff", -- [2]
					113764, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.4120000000003, -- [1]
					"Brother Korloff", -- [2]
					113764, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[114410] = {
				{
					6.87700000000041, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					9.29999999999927, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					17.8279999999995, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					26.3029999999999, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					26.3029999999999, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					26.3029999999999, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					31.1630000000005, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					34.7970000000005, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					43.3060000000005, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
		}, -- [75]
		{
			[115297] = {
				{
					25.0840000000007, -- [1]
					"Thalnos the Soulrender", -- [2]
					115297, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Fravashi-Area52", -- [5]
				}, -- [1]
			},
			[115139] = {
				{
					5.63700000000063, -- [1]
					"Thalnos the Soulrender", -- [2]
					115139, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[115289] = {
				{
					1.59400000000096, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					13.7150000000001, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					20.9880000000012, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					28.3000000000011, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					35.5789999999997, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [76]
		{
			[3391] = {
				{
					12.143, -- [1]
					"Princess Theradras", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.2000000000003, -- [1]
					"Princess Theradras", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					46.1120000000001, -- [1]
					"Princess Theradras", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					59.422, -- [1]
					"Princess Theradras", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					71.5660000000003, -- [1]
					"Princess Theradras", -- [2]
					3391, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[21909] = {
				{
					12.143, -- [1]
					"Princess Theradras", -- [2]
					21909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					43.6780000000003, -- [1]
					"Princess Theradras", -- [2]
					21909, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[21832] = {
				{
					59.0160000000001, -- [1]
					"Princess Theradras", -- [2]
					21832, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					69.9259999999999, -- [1]
					"Princess Theradras", -- [2]
					21832, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Popoki-Ragnaros", -- [5]
				}, -- [2]
			},
			[21869] = {
				{
					32.781, -- [1]
					"Princess Theradras", -- [2]
					21869, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					72.7760000000003, -- [1]
					"Princess Theradras", -- [2]
					21869, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [77]
		{
			[5568] = {
				{
					10.5510000000004, -- [1]
					"Landslide", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.9660000000004, -- [1]
					"Landslide", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					44.5570000000002, -- [1]
					"Landslide", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					56.6880000000001, -- [1]
					"Landslide", -- [2]
					5568, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[21337] = {
				{
					9.33100000000013, -- [1]
					"Deeprot Tangler", -- [2]
					21337, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deeprot Tangler", -- [5]
				}, -- [1]
			},
			[110762] = {
				{
					16.6150000000002, -- [1]
					"Landslide", -- [2]
					110762, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mooner-Windrunner", -- [5]
				}, -- [1]
				{
					33.6220000000003, -- [1]
					"Landslide", -- [2]
					110762, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mooner-Windrunner", -- [5]
				}, -- [2]
				{
					70.029, -- [1]
					"Landslide", -- [2]
					110762, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mooner-Windrunner", -- [5]
				}, -- [3]
			},
			[21808] = {
				{
					19.0320000000002, -- [1]
					"Landslide", -- [2]
					21808, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					59.1090000000004, -- [1]
					"Landslide", -- [2]
					21808, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [78]
		{
			[40504] = {
				{
					12.3040000000037, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [1]
				{
					20.3410000000004, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [2]
				{
					34.3790000000008, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [3]
				{
					42.4150000000009, -- [1]
					"Razorlash", -- [2]
					40504, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [4]
			},
		}, -- [79]
		{
			[21707] = {
				{
					14.8009999999995, -- [1]
					"Noxxion", -- [2]
					21707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					54.864999999998, -- [1]
					"Noxxion", -- [2]
					21707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[21687] = {
				{
					8.71600000000035, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					23.2750000000015, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					34.2220000000016, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					45.1449999999968, -- [1]
					"Noxxion", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[10966] = {
				{
					9.92899999999645, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [1]
				{
					30.5630000000019, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [2]
				{
					46.3559999999998, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [3]
				{
					58.4989999999962, -- [1]
					"Noxxion", -- [2]
					10966, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Habbit-Durotan", -- [5]
				}, -- [4]
			},
		}, -- [80]
		{
			[10348] = {
				{
					63.0040000000008, -- [1]
					"Leprous Technician", -- [2]
					10348, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[21337] = {
				{
					63.0040000000008, -- [1]
					"Unknown", -- [2]
					21337, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Unknown", -- [5]
				}, -- [1]
			},
			[52778] = {
				{
					16.0169999999998, -- [1]
					"Mekgineer Thermaplugg", -- [2]
					52778, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					42.7119999999995, -- [1]
					"Mekgineer Thermaplugg", -- [2]
					52778, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
			},
			[74720] = {
				{
					9.94799999999668, -- [1]
					"Mekgineer Thermaplugg", -- [2]
					74720, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					46.3609999999972, -- [1]
					"Mekgineer Thermaplugg", -- [2]
					74720, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
			},
			[93655] = {
				{
					12.6650000000009, -- [1]
					"Mekgineer Thermaplugg", -- [2]
					93655, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dreadstalker", -- [5]
				}, -- [1]
				{
					47.8859999999986, -- [1]
					"Mekgineer Thermaplugg", -- [2]
					93655, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [2]
			},
		}, -- [81]
		{
			[11082] = {
				{
					11.3759999999966, -- [1]
					"Electrocutioner 6000", -- [2]
					11082, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.9859999999972, -- [1]
					"Electrocutioner 6000", -- [2]
					11082, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[10346] = {
				{
					46.9989999999962, -- [1]
					"Mechano-Tank", -- [2]
					10346, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [1]
			},
			[10348] = {
				{
					7.09100000000035, -- [1]
					"Leprous Technician", -- [2]
					10348, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					13.9599999999991, -- [1]
					"Leprous Technician", -- [2]
					10348, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [82]
		{
			[81039] = {
				{
					10.4449999999997, -- [1]
					"Irradiated Slime", -- [2]
					81039, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.1239999999962, -- [1]
					"Irradiated Slime", -- [2]
					81039, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[21687] = {
				{
					7.06599999999889, -- [1]
					"Viscous Fallout", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					19.6239999999962, -- [1]
					"Viscous Fallout", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					30.5659999999989, -- [1]
					"Viscous Fallout", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					41.4950000000026, -- [1]
					"Viscous Fallout", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					52.4519999999975, -- [1]
					"Viscous Fallout", -- [2]
					21687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[10341] = {
				{
					48.8260000000009, -- [1]
					"Irradiated Slime", -- [2]
					10341, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [83]
		{
			[9460] = {
				{
					52.0069999999978, -- [1]
					"Corrosive Lurker", -- [2]
					9460, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[151583] = {
				{
					10.4890000000014, -- [1]
					"Charlga Razorflank", -- [2]
					151583, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					26.2719999999972, -- [1]
					"Charlga Razorflank", -- [2]
					151583, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gatrude-BleedingHollow", -- [5]
				}, -- [2]
				{
					42.0360000000001, -- [1]
					"Charlga Razorflank", -- [2]
					151583, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [3]
			},
			[151570] = {
				{
					18.1880000000019, -- [1]
					"Venomous Discharge Crystal", -- [2]
					151570, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					33.1820000000007, -- [1]
					"Venomous Discharge Crystal", -- [2]
					151570, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					48.1889999999985, -- [1]
					"Venomous Discharge Crystal", -- [2]
					151570, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[151534] = {
				{
					5.3839999999982, -- [1]
					"Charlga Razorflank", -- [2]
					151534, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Venomous Discharge Crystal", -- [5]
				}, -- [1]
				{
					17.5550000000003, -- [1]
					"Charlga Razorflank", -- [2]
					151534, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tidal Tempest Crystal", -- [5]
				}, -- [2]
			},
			[13864] = {
				{
					52.0069999999978, -- [1]
					"Unknown", -- [2]
					13864, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Unknown", -- [5]
				}, -- [1]
				{
					52.0069999999978, -- [1]
					"Holdout Medic", -- [2]
					13864, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Holdout Medic", -- [5]
				}, -- [2]
			},
		}, -- [84]
		{
			[151432] = {
				{
					9.22900000000664, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151432, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					31.0720000000001, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151432, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[151407] = {
				{
					21.1030000000028, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151407, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					42.5640000000058, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151407, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[151475] = {
				{
					3.31000000000495, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151475, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					13.586000000003, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151475, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					28.1610000000001, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151475, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					39.0710000000036, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151475, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					50.0240000000049, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151475, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
			},
			[151454] = {
				{
					20.461000000003, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151454, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					42.3029999999999, -- [1]
					"Groyat, the Blind Hunter", -- [2]
					151454, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [85]
		{
			[151273] = {
				{
					13.7669999999998, -- [1]
					"Aggem Thorncurse", -- [2]
					151273, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[151218] = {
				{
					19.2259999999951, -- [1]
					"Death Speaker Jargba", -- [2]
					151218, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[151253] = {
				{
					0.318999999995867, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					2.4059999999954, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [2]
				{
					4.82499999999709, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					7.25299999999697, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myronastus-BleedingHollow", -- [5]
				}, -- [4]
				{
					15.7560000000012, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gatrude-BleedingHollow", -- [5]
				}, -- [5]
				{
					21.8450000000012, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gatrude-BleedingHollow", -- [5]
				}, -- [6]
				{
					24.2679999999964, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [7]
				{
					26.6840000000011, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gatrude-BleedingHollow", -- [5]
				}, -- [8]
				{
					29.1059999999998, -- [1]
					"Death Speaker Jargba", -- [2]
					151253, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [9]
			},
			[151274] = {
				{
					0.180999999996857, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					1.40099999999802, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					2.61899999999878, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					3.83099999999831, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					5.02799999999843, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
				{
					6.25800000000163, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [6]
				{
					7.48399999999674, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [7]
				{
					8.6929999999993, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [8]
				{
					9.90200000000186, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [9]
				{
					11.1299999999974, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [10]
				{
					12.3339999999953, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [11]
				{
					13.551999999996, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [12]
				{
					14.7619999999952, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [13]
				{
					15.9789999999994, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [14]
				{
					17.2039999999979, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [15]
				{
					18.4259999999995, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					19.625, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [17]
				{
					20.8430000000008, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [18]
				{
					22.0509999999995, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [19]
				{
					23.2750000000015, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [20]
				{
					24.476999999999, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [21]
				{
					25.6800000000003, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [22]
				{
					26.9059999999954, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [23]
				{
					28.112000000001, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [24]
				{
					29.3339999999953, -- [1]
					"Aggem Thorncurse", -- [2]
					151274, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [25]
			},
		}, -- [86]
		{
			[150859] = {
				{
					4.7019999999975, -- [1]
					"Razorfen Beast Stalker", -- [2]
					150859, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [1]
				{
					10.7929999999978, -- [1]
					"Razorfen Beast Stalker", -- [2]
					150859, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
			},
			[150768] = {
				{
					17.1489999999976, -- [1]
					"Roogug", -- [2]
					150768, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[153528] = {
				{
					15.4309999999969, -- [1]
					"Roogug", -- [2]
					153528, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					31.9490000000005, -- [1]
					"Roogug", -- [2]
					153528, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
			},
			[150848] = {
				{
					11.3529999999955, -- [1]
					"Blood-Branded Razorfen", -- [2]
					150848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150849] = {
				{
					9.13299999999435, -- [1]
					"Razorfen Huntmaster", -- [2]
					150849, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[153525] = {
				{
					11.0619999999981, -- [1]
					"Roogug", -- [2]
					153525, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					27.3240000000005, -- [1]
					"Roogug", -- [2]
					153525, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					43.1270000000004, -- [1]
					"Roogug", -- [2]
					153525, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
			},
			[150774] = {
				{
					1.75499999999738, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					4.18400000000111, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					6.20499999999447, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					8.62299999999959, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					13.5079999999944, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
				{
					18.8179999999993, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [6]
				{
					20.0040000000008, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [7]
				{
					21.6399999999994, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [8]
				{
					22.8640000000014, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [9]
				{
					23.6679999999978, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [10]
				{
					24.8869999999952, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [11]
				{
					26.1029999999955, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [12]
				{
					28.5459999999948, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [13]
				{
					29.7619999999952, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [14]
				{
					30.9660000000004, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [15]
				{
					32.7229999999981, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [16]
				{
					34.6299999999974, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [17]
				{
					35.8479999999981, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [18]
				{
					37.0579999999973, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [19]
				{
					38.2699999999968, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [20]
				{
					39.489999999998, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [21]
				{
					40.6800000000003, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [22]
				{
					41.8989999999976, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [23]
				{
					44.3169999999955, -- [1]
					"Roogug", -- [2]
					150774, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [24]
			},
			[150100] = {
				{
					1.52199999999721, -- [1]
					"Razorfen Huntmaster", -- [2]
					150100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					4.18400000000111, -- [1]
					"Razorfen Huntmaster", -- [2]
					150100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					6.20499999999447, -- [1]
					"Razorfen Huntmaster", -- [2]
					150100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kowlshok", -- [5]
				}, -- [3]
				{
					11.0619999999981, -- [1]
					"Razorfen Huntmaster", -- [2]
					150100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					13.4949999999953, -- [1]
					"Razorfen Huntmaster", -- [2]
					150100, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
			},
		}, -- [87]
		{
			[150881] = {
				{
					20.0590000000011, -- [1]
					"Hunter Bonetusk", -- [2]
					150881, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
			},
			[150917] = {
				{
					27.3990000000049, -- [1]
					"Hunter Bonetusk", -- [2]
					150917, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150904] = {
				{
					6.22800000000279, -- [1]
					"Hunter Bonetusk", -- [2]
					150904, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					43.8349999999991, -- [1]
					"Hunter Bonetusk", -- [2]
					150904, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
			},
			[150914] = {
				{
					23.3960000000006, -- [1]
					"Hunter Bonetusk", -- [2]
					150914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150879] = {
				{
					1.36300000000483, -- [1]
					"Hunter Bonetusk", -- [2]
					150879, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					13.5, -- [1]
					"Hunter Bonetusk", -- [2]
					150879, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					18.3530000000028, -- [1]
					"Hunter Bonetusk", -- [2]
					150879, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					35.3400000000038, -- [1]
					"Hunter Bonetusk", -- [2]
					150879, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					40.2170000000042, -- [1]
					"Hunter Bonetusk", -- [2]
					150879, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
				{
					51.1169999999984, -- [1]
					"Hunter Bonetusk", -- [2]
					150879, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [6]
			},
		}, -- [88]
		{
			[9256] = {
				{
					26.0190000000002, -- [1]
					"High Inquisitor Whitemane", -- [2]
					9256, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[9232] = {
				{
					31.6650000000009, -- [1]
					"High Inquisitor Whitemane", -- [2]
					9232, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[151091] = {
				{
					61.002999999997, -- [1]
					"Razorfen Geomagus", -- [2]
					151091, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[115876] = {
				{
					35.3569999999963, -- [1]
					"Commander Durand", -- [2]
					115876, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[114848] = {
				{
					22.4470000000001, -- [1]
					"High Inquisitor Whitemane", -- [2]
					114848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					25.5590000000011, -- [1]
					"High Inquisitor Whitemane", -- [2]
					114848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					47.9789999999994, -- [1]
					"High Inquisitor Whitemane", -- [2]
					114848, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
			},
			[127399] = {
				{
					36.9859999999972, -- [1]
					"High Inquisitor Whitemane", -- [2]
					127399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"High Inquisitor Whitemane", -- [5]
				}, -- [1]
			},
			[151090] = {
				{
					61.002999999997, -- [1]
					"Razorfen Geomagus", -- [2]
					151090, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					61.002999999997, -- [1]
					"Razorfen Geomagus", -- [2]
					151090, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					61.002999999997, -- [1]
					"Razorfen Geomagus", -- [2]
					151090, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [89]
		{
			[114807] = {
				{
					20.0999999999985, -- [1]
					"Brother Korloff", -- [2]
					114807, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[113764] = {
				{
					10.9120000000039, -- [1]
					"Brother Korloff", -- [2]
					113764, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[114410] = {
				{
					4.33099999999831, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					7.98099999999977, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					10.4150000000009, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					17.6900000000023, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					17.6900000000023, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					18.8930000000037, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					26.1610000000001, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					26.1750000000029, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					27.3689999999988, -- [1]
					"Brother Korloff", -- [2]
					114410, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
		}, -- [90]
		{
			[115309] = {
				{
					28.1520000000019, -- [1]
					"Unknown", -- [2]
					115309, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[115297] = {
				{
					25.0869999999995, -- [1]
					"Thalnos the Soulrender", -- [2]
					115297, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[115139] = {
				{
					5.64600000000064, -- [1]
					"Thalnos the Soulrender", -- [2]
					115139, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[115289] = {
				{
					1.57299999999668, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					13.7050000000017, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					21.0149999999994, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					28.288999999997, -- [1]
					"Thalnos the Soulrender", -- [2]
					115289, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [91]
		{
			[15248] = {
				{
					7.01500000000669, -- [1]
					"Riverpaw Looter", -- [2]
					15248, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					25.0450000000055, -- [1]
					"Riverpaw Looter", -- [2]
					15248, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
			},
			[86604] = {
				{
					5.71399999999994, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					16.6430000000037, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					27.5780000000013, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					37.974000000002, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					48.2020000000048, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
				{
					62.7520000000004, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [6]
				{
					73.6530000000057, -- [1]
					"Hogger", -- [2]
					86604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [7]
			},
			[86736] = {
				{
					56.6920000000027, -- [1]
					"Hogger", -- [2]
					86736, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[86820] = {
				{
					13.3870000000024, -- [1]
					"Riverpaw Looter", -- [2]
					86820, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [1]
			},
			[12167] = {
				{
					1.43000000000029, -- [1]
					"Riverpaw Shaman", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [1]
				{
					5.08200000000215, -- [1]
					"Riverpaw Shaman", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [2]
				{
					9.12400000000343, -- [1]
					"Riverpaw Shaman", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [3]
				{
					20.3710000000065, -- [1]
					"Riverpaw Shaman", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [4]
			},
			[14516] = {
				{
					4.51200000000245, -- [1]
					"Riverpaw Slayer", -- [2]
					14516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					11.0310000000027, -- [1]
					"Riverpaw Slayer", -- [2]
					14516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					19.0390000000043, -- [1]
					"Riverpaw Slayer", -- [2]
					14516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					25.0450000000055, -- [1]
					"Riverpaw Slayer", -- [2]
					14516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					31.0680000000066, -- [1]
					"Riverpaw Slayer", -- [2]
					14516, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
			},
			[86620] = {
				{
					13.0760000000009, -- [1]
					"Hogger", -- [2]
					86620, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.1390000000029, -- [1]
					"Hogger", -- [2]
					86620, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [92]
		{
			[12466] = {
				{
					4.52900000000227, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					10.8440000000046, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					14.487000000001, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					19.3430000000008, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [4]
				{
					25.010000000002, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
				{
					28.6460000000006, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [6]
				{
					32.2870000000039, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [7]
				{
					36.3380000000034, -- [1]
					"Lord Overheat", -- [2]
					12466, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [8]
			},
			[86636] = {
				{
					16.135000000002, -- [1]
					"Lord Overheat", -- [2]
					86636, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					37.971000000005, -- [1]
					"Lord Overheat", -- [2]
					86636, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[86633] = {
				{
					7.42900000000373, -- [1]
					"Lord Overheat", -- [2]
					86633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					21.9760000000024, -- [1]
					"Lord Overheat", -- [2]
					86633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					40.1750000000029, -- [1]
					"Lord Overheat", -- [2]
					86633, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Oblivìon-Zul'jin", -- [5]
				}, -- [3]
			},
		}, -- [93]
		{
			[55964] = {
				{
					12.8309999999983, -- [1]
					"Randolph Moloch", -- [2]
					55964, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.8270000000048, -- [1]
					"Randolph Moloch", -- [2]
					55964, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[86726] = {
				{
					3.12099999999919, -- [1]
					"Randolph Moloch", -- [2]
					86726, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					18.9049999999988, -- [1]
					"Randolph Moloch", -- [2]
					86726, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					37.1080000000002, -- [1]
					"Randolph Moloch", -- [2]
					86726, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gatrude-BleedingHollow", -- [5]
				}, -- [3]
			},
			[86729] = {
				{
					2.69000000000233, -- [1]
					"Randolph Moloch", -- [2]
					86729, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					12.711000000003, -- [1]
					"Randolph Moloch", -- [2]
					86729, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					24.512999999999, -- [1]
					"Randolph Moloch", -- [2]
					86729, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					36.6880000000019, -- [1]
					"Randolph Moloch", -- [2]
					86729, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gatrude-BleedingHollow", -- [5]
				}, -- [4]
				{
					46.8309999999983, -- [1]
					"Randolph Moloch", -- [2]
					86729, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [5]
			},
		}, -- [94]
		{
			[113690] = {
				{
					4.83600000000297, -- [1]
					"Flameweaver Koegler", -- [2]
					113690, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [1]
				{
					10.3340000000026, -- [1]
					"Flameweaver Koegler", -- [2]
					113690, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [2]
				{
					26.6600000000035, -- [1]
					"Flameweaver Koegler", -- [2]
					113690, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Akkasha-Zul'jin", -- [5]
				}, -- [3]
				{
					43.0540000000037, -- [1]
					"Flameweaver Koegler", -- [2]
					113690, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Myram-Hellscream", -- [5]
				}, -- [4]
			},
			[113691] = {
				{
					15.1680000000051, -- [1]
					"Flameweaver Koegler", -- [2]
					113691, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.310000000005, -- [1]
					"Flameweaver Koegler", -- [2]
					113691, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[113641] = {
				{
					32.9500000000044, -- [1]
					"Flameweaver Koegler", -- [2]
					113641, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[113682] = {
				{
					5.46200000000681, -- [1]
					"Flameweaver Koegler", -- [2]
					113682, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					43.0540000000037, -- [1]
					"Flameweaver Koegler", -- [2]
					113682, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[113364] = {
				{
					23.0230000000011, -- [1]
					"Flameweaver Koegler", -- [2]
					113364, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Book Case", -- [5]
				}, -- [1]
			},
			[113626] = {
				{
					29.7170000000042, -- [1]
					"Flameweaver Koegler", -- [2]
					113626, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [95]
		{
			[111216] = {
				{
					45.8389999999999, -- [1]
					"Armsmaster Harlan", -- [2]
					111216, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[111217] = {
				{
					11.8570000000036, -- [1]
					"Armsmaster Harlan", -- [2]
					111217, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					23.9950000000026, -- [1]
					"Armsmaster Harlan", -- [2]
					111217, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					36.1360000000059, -- [1]
					"Armsmaster Harlan", -- [2]
					111217, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					66.471000000005, -- [1]
					"Armsmaster Harlan", -- [2]
					111217, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[111221] = {
				{
					25.2090000000026, -- [1]
					"Armsmaster Harlan", -- [2]
					111221, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[111218] = {
				{
					39.833000000006, -- [1]
					"Armsmaster Harlan", -- [2]
					111218, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[113959] = {
				{
					20.3610000000044, -- [1]
					"Unknown", -- [2]
					113959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					20.3610000000044, -- [1]
					"Unknown", -- [2]
					113959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					50.385000000002, -- [1]
					"Scarlet Defender", -- [2]
					113959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					50.385000000002, -- [1]
					"Scarlet Defender", -- [2]
					113959, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [96]
		{
			[114021] = {
				{
					6.33999999999651, -- [1]
					"Houndmaster Braun", -- [2]
					114021, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[114259] = {
				{
					8.89099999999598, -- [1]
					"Houndmaster Braun", -- [2]
					114259, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.1049999999959, -- [1]
					"Houndmaster Braun", -- [2]
					114259, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					12.5239999999976, -- [1]
					"Houndmaster Braun", -- [2]
					114259, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					14.9419999999955, -- [1]
					"Houndmaster Braun", -- [2]
					114259, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[116140] = {
				{
					18.5819999999949, -- [1]
					"Houndmaster Braun", -- [2]
					116140, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [97]
		{
			[151303] = {
				{
					14.4530000000013, -- [1]
					"Aku'mai the Devourer", -- [2]
					151303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[151275] = {
				{
					4.97800000000279, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [1]
				{
					10.2260000000024, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [2]
				{
					11.4400000000023, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [3]
				{
					15.8859999999986, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [4]
				{
					17.099000000002, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [5]
				{
					21.9689999999973, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [6]
				{
					23.1089999999967, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [7]
				{
					23.176999999996, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [8]
				{
					31.1990000000005, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [9]
				{
					31.4519999999975, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
				{
					39.6229999999996, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [11]
			},
			[152963] = {
				{
					14.9439999999959, -- [1]
					"Aku'mai", -- [2]
					152963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150660] = {
				{
					21.3119999999981, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					22.5599999999977, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					32.2459999999992, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					33.4649999999965, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [98]
		{
			[150020] = {
				{
					17.9500000000044, -- [1]
					"Twilight Lord Bathiel", -- [2]
					150020, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[151813] = {
				{
					17.9500000000044, -- [1]
					"Twilight Lord Bathiel", -- [2]
					151813, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [99]
		{
			[149943] = {
				{
					12.3359999999957, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.0889999999999, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [100]
		{
			[149908] = {
				{
					11.4390000000058, -- [1]
					"Thruk", -- [2]
					149908, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[149913] = {
				{
					23.4080000000031, -- [1]
					"Thruk", -- [2]
					149913, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [101]
		{
			[151159] = {
				{
					28.1059999999998, -- [1]
					"Subjugator Kor'ul", -- [2]
					151159, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150634] = {
				{
					11.7669999999998, -- [1]
					"Subjugator Kor'ul", -- [2]
					150634, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [1]
				{
					33.9570000000022, -- [1]
					"Subjugator Kor'ul", -- [2]
					150634, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [2]
			},
			[152417] = {
				{
					36.0639999999985, -- [1]
					"Deep Terror", -- [2]
					152417, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					36.0639999999985, -- [1]
					"Deep Terror", -- [2]
					152417, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[151963] = {
				{
					46.0009999999966, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.0009999999966, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					46.0009999999966, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					46.0009999999966, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					46.0009999999966, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [102]
		{
			[149893] = {
				{
					10.2610000000059, -- [1]
					"Domina", -- [2]
					149893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.849000000002, -- [1]
					"Domina", -- [2]
					149893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[149886] = {
				{
					18.4440000000031, -- [1]
					"Domina", -- [2]
					149886, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150377] = {
				{
					25.4380000000019, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [1]
				{
					32.6990000000005, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [2]
			},
		}, -- [103]
		{
			[149869] = {
				{
					6.07200000000012, -- [1]
					"Ghamoo-Ra", -- [2]
					149869, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150438] = {
				{
					9.18400000000111, -- [1]
					"Razorshell Snapjaw", -- [2]
					150438, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.7760000000053, -- [1]
					"Razorshell Snapjaw", -- [2]
					150438, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					26.1900000000023, -- [1]
					"Razorshell Snapjaw", -- [2]
					150438, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					42.7890000000043, -- [1]
					"Razorshell Snapjaw", -- [2]
					150438, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[151684] = {
				{
					1.78500000000349, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [1]
				{
					1.78500000000349, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					1.78500000000349, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [3]
				{
					4.78800000000047, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
				{
					4.78800000000047, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [5]
				{
					4.78800000000047, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [6]
				{
					7.79100000000472, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [7]
				{
					7.79100000000472, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [8]
				{
					7.79100000000472, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [9]
				{
					10.7890000000043, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [10]
				{
					10.7890000000043, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [11]
				{
					10.7890000000043, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [12]
				{
					13.8020000000033, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [13]
				{
					13.8020000000033, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [14]
				{
					13.8020000000033, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [15]
				{
					16.7780000000057, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					16.7780000000057, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [17]
				{
					16.7780000000057, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [18]
				{
					19.7920000000013, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [19]
				{
					19.7920000000013, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Grotwaz-Azgalor", -- [5]
				}, -- [20]
				{
					19.7920000000013, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [21]
				{
					22.7839999999997, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [22]
				{
					22.7839999999997, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [23]
				{
					22.7839999999997, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [24]
				{
					25.7960000000021, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [25]
				{
					25.7960000000021, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [26]
				{
					25.7960000000021, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Killforu-Azuremyst", -- [5]
				}, -- [27]
				{
					28.7890000000043, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kalendrios-Kilrogg", -- [5]
				}, -- [28]
				{
					28.7890000000043, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [29]
				{
					28.7890000000043, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Vely-Moonrunner", -- [5]
				}, -- [30]
			},
		}, -- [104]
		{
			[93675] = {
				{
					1.78899999999703, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [1]
				{
					9.08600000000297, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [2]
				{
					20.0350000000035, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [3]
				{
					24.9210000000021, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [4]
				{
					29.7690000000002, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [5]
				{
					34.6180000000022, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [6]
				{
					39.4720000000016, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [7]
				{
					51.589, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [8]
				{
					56.4700000000012, -- [1]
					"Lord Godfrey", -- [2]
					93675, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [9]
			},
			[93520] = {
				{
					12.3139999999985, -- [1]
					"Lord Godfrey", -- [2]
					93520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					42.6869999999981, -- [1]
					"Lord Godfrey", -- [2]
					93520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[93707] = {
				{
					5.44099999999889, -- [1]
					"Lord Godfrey", -- [2]
					93707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.8280000000013, -- [1]
					"Lord Godfrey", -- [2]
					93707, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[93629] = {
				{
					50.6690000000017, -- [1]
					"Lord Godfrey", -- [2]
					93629, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Yegor-Bonechewer", -- [5]
				}, -- [1]
			},
		}, -- [105]
		{
			[93697] = {
				{
					8.93499999999767, -- [1]
					"Lord Walden", -- [2]
					93697, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.6209999999992, -- [1]
					"Lord Walden", -- [2]
					93697, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					25.1069999999963, -- [1]
					"Lord Walden", -- [2]
					93697, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					37.2709999999934, -- [1]
					"Lord Walden", -- [2]
					93697, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[93527] = {
				{
					9.86399999999412, -- [1]
					"Lord Walden", -- [2]
					93527, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					41.8339999999953, -- [1]
					"Lord Walden", -- [2]
					93527, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[93505] = {
				{
					6.52699999999459, -- [1]
					"Lord Walden", -- [2]
					93505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lottidotty-Azuremyst", -- [5]
				}, -- [1]
				{
					20.2729999999938, -- [1]
					"Lord Walden", -- [2]
					93505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Totemo-SilverHand", -- [5]
				}, -- [2]
				{
					31.1689999999944, -- [1]
					"Lord Walden", -- [2]
					93505, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [3]
			},
		}, -- [106]
		{
			[93685] = {
				{
					5.29200000000128, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [1]
				{
					11.3610000000044, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [2]
				{
					16.2440000000061, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [3]
				{
					21.0940000000046, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [4]
				{
					25.9510000000009, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [5]
				{
					33.2550000000047, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [6]
				{
					39.3389999999999, -- [1]
					"Commander Springvale", -- [2]
					93685, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [7]
			},
			[93687] = {
				{
					10.1600000000035, -- [1]
					"Commander Springvale", -- [2]
					93687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.3860000000059, -- [1]
					"Commander Springvale", -- [2]
					93687, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [107]
		{
			[93861] = {
				{
					47.9170000000013, -- [1]
					"Wolf Master Nandos", -- [2]
					93861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [1]
			},
			[93857] = {
				{
					14.6370000000024, -- [1]
					"Baron Silverlaine", -- [2]
					93857, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.1330000000016, -- [1]
					"Baron Silverlaine", -- [2]
					93857, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[23224] = {
				{
					6.83800000000338, -- [1]
					"Baron Silverlaine", -- [2]
					23224, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [1]
				{
					23.8760000000038, -- [1]
					"Baron Silverlaine", -- [2]
					23224, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [2]
				{
					40.9239999999991, -- [1]
					"Baron Silverlaine", -- [2]
					23224, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ippu-Zul'jin", -- [5]
				}, -- [3]
			},
			[93914] = {
				{
					35.3530000000028, -- [1]
					"Razorclaw the Butcher", -- [2]
					93914, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lottidotty-Azuremyst", -- [5]
				}, -- [1]
			},
		}, -- [108]
		{
			[93423] = {
				{
					14.8060000000041, -- [1]
					"Baron Ashbury", -- [2]
					93423, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					52.4300000000003, -- [1]
					"Baron Ashbury", -- [2]
					93423, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[93581] = {
				{
					5.11100000000442, -- [1]
					"Baron Ashbury", -- [2]
					93581, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					26.9570000000022, -- [1]
					"Baron Ashbury", -- [2]
					93581, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lottidotty-Azuremyst", -- [5]
				}, -- [2]
				{
					43.9270000000033, -- [1]
					"Baron Ashbury", -- [2]
					93581, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Totemo-SilverHand", -- [5]
				}, -- [3]
			},
			[93468] = {
				{
					25.1050000000032, -- [1]
					"Baron Ashbury", -- [2]
					93468, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [109]
		{
			[8150] = {
				{
					9.48300000000018, -- [1]
					"Mutanus the Devourer", -- [2]
					8150, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[7399] = {
				{
					17.9549999999999, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gazbirkal-Mannoroth", -- [5]
				}, -- [1]
				{
					32.5520000000001, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Sneakysap-Lightning'sBlade", -- [5]
				}, -- [2]
				{
					45.9430000000002, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gazbirkal-Mannoroth", -- [5]
				}, -- [3]
			},
		}, -- [110]
		{
			[8142] = {
				{
					19.8429999999999, -- [1]
					"Verdan the Everliving", -- [2]
					8142, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					45.3620000000001, -- [1]
					"Verdan the Everliving", -- [2]
					8142, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [111]
		{
			[23381] = {
				{
					36.6970000000001, -- [1]
					"Lord Serpentis", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Druid of the Fang", -- [5]
				}, -- [1]
			},
			[7951] = {
				{
					0.242000000000189, -- [1]
					"Deviate Venomwing", -- [2]
					7951, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [1]
				{
					2.96000000000004, -- [1]
					"Deviate Venomwing", -- [2]
					7951, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [2]
				{
					11.203, -- [1]
					"Deviate Venomwing", -- [2]
					7951, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [3]
			},
			[8041] = {
				{
					30.0439999999999, -- [1]
					"Druid of the Fang", -- [2]
					8041, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[9532] = {
				{
					11.645, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [1]
				{
					14.855, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Djalanila-Bladefist", -- [5]
				}, -- [2]
				{
					18.3049999999998, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Djalanila-Bladefist", -- [5]
				}, -- [3]
				{
					22.152, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Djalanila-Bladefist", -- [5]
				}, -- [4]
				{
					25.7820000000002, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Djalanila-Bladefist", -- [5]
				}, -- [5]
				{
					29.4230000000002, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Djalanila-Bladefist", -- [5]
				}, -- [6]
			},
			[12167] = {
				{
					2.74900000000025, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gazbirkal-Mannoroth", -- [5]
				}, -- [1]
				{
					25.7820000000002, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [2]
				{
					33.0459999999998, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [3]
				{
					40.3110000000002, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [4]
			},
		}, -- [112]
		{
			[6254] = {
				{
					1.71799999999985, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gazbirkal-Mannoroth", -- [5]
				}, -- [1]
				{
					10.1120000000001, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [2]
				{
					14.9719999999998, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [3]
				{
					17.3809999999999, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [4]
				{
					21.029, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [5]
				{
					24.6599999999999, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [6]
				{
					27.1030000000001, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [7]
			},
		}, -- [113]
		{
			[744] = {
				{
					29.0740000000001, -- [1]
					"Lord Cobrahn", -- [2]
					744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [1]
			},
			[20805] = {
				{
					2.02099999999973, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gazbirkal-Mannoroth", -- [5]
				}, -- [1]
			},
			[8040] = {
				{
					19.8240000000001, -- [1]
					"Lord Cobrahn", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[7965] = {
				{
					20.5740000000001, -- [1]
					"Lord Cobrahn", -- [2]
					7965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [114]
		{
			[20805] = {
				{
					4.21000000000004, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [1]
				{
					17.402, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [2]
				{
					24.8289999999997, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [3]
				{
					40.5369999999998, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [4]
				{
					47.9110000000001, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [5]
				{
					52.7579999999998, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [6]
				{
					60.8899999999999, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [7]
				{
					66.1659999999997, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [8]
			},
			[8147] = {
				{
					42.4769999999999, -- [1]
					"Lord Pythas", -- [2]
					8147, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					72.8759999999998, -- [1]
					"Lord Pythas", -- [2]
					8147, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[11431] = {
				{
					7.84299999999985, -- [1]
					"Lord Pythas", -- [2]
					11431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Druid of the Fang", -- [5]
				}, -- [1]
				{
					36.9919999999997, -- [1]
					"Lord Pythas", -- [2]
					11431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lord Pythas", -- [5]
				}, -- [2]
				{
					56.4180000000001, -- [1]
					"Lord Pythas", -- [2]
					11431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lord Pythas", -- [5]
				}, -- [3]
			},
			[8041] = {
				{
					13.335, -- [1]
					"Druid of the Fang", -- [2]
					8041, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[8040] = {
				{
					32.8389999999999, -- [1]
					"Lord Pythas", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Sneakysap-Lightning'sBlade", -- [5]
				}, -- [1]
			},
			[23381] = {
				{
					10.2829999999999, -- [1]
					"Druid of the Fang", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Druid of the Fang", -- [5]
				}, -- [1]
			},
		}, -- [115]
		{
			[80362] = {
				{
					5.49400000000014, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [1]
				{
					20.7690000000002, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [2]
				{
					35.8440000000001, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [3]
			},
		}, -- [116]
		{
			[23381] = {
				{
					29.6089999999999, -- [1]
					"Lady Anacondra", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lady Anacondra", -- [5]
				}, -- [1]
				{
					50.2469999999999, -- [1]
					"Lady Anacondra", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lady Anacondra", -- [5]
				}, -- [2]
			},
			[20295] = {
				{
					5.37699999999995, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [1]
				{
					12.0360000000001, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					15.0500000000002, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [3]
				{
					18.7170000000001, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [4]
				{
					25.9699999999998, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [5]
				{
					36.0529999999999, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [6]
				{
					39.7159999999999, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [7]
				{
					46.5940000000001, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [8]
				{
					62.4479999999999, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Noraas-BleedingHollow", -- [5]
				}, -- [9]
			},
			[8148] = {
				{
					2.37899999999991, -- [1]
					"Lady Anacondra", -- [2]
					8148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					63.011, -- [1]
					"Lady Anacondra", -- [2]
					8148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[8040] = {
				{
					2.05899999999997, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					21.8429999999998, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
				{
					32.75, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					42.8589999999999, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
			},
		}, -- [117]
		{
			[151303] = {
				{
					14.7819999999992, -- [1]
					"Aku'mai the Devourer", -- [2]
					151303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					48.9409999999989, -- [1]
					"Aku'mai the Devourer", -- [2]
					151303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[151275] = {
				{
					5.83899999999994, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [1]
				{
					15.5630000000019, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [2]
				{
					21.5950000000012, -- [1]
					"Unknown", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [3]
				{
					23.5960000000014, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [4]
				{
					27.7000000000007, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [5]
				{
					28.9069999999992, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [6]
				{
					34.4789999999994, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [7]
				{
					37.3300000000017, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [8]
				{
					39.8610000000008, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [9]
				{
					43.5, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [10]
				{
					49.5960000000014, -- [1]
					"Aku'mai the Venomous", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [11]
				{
					49.5960000000014, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [12]
				{
					55.8559999999998, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [13]
				{
					61.7429999999986, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [14]
				{
					67.8050000000003, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [15]
				{
					73.8739999999998, -- [1]
					"Aku'mai the Devourer", -- [2]
					151275, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [16]
			},
			[152963] = {
				{
					19.5540000000001, -- [1]
					"Aku'mai", -- [2]
					152963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					50.1890000000021, -- [1]
					"Aku'mai", -- [2]
					152963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					50.8410000000004, -- [1]
					"Aku'mai", -- [2]
					152963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150660] = {
				{
					23.0230000000011, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.8509999999988, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					33.1340000000018, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					36.7720000000008, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					44.0950000000012, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					56.2540000000008, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					57.1039999999994, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					67.0970000000016, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					67.1860000000015, -- [1]
					"Deep Terror", -- [2]
					150660, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
		}, -- [118]
		{
			[151813] = {
				{
					17.7020000000011, -- [1]
					"Twilight Lord Bathiel", -- [2]
					151813, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					51.6450000000004, -- [1]
					"Twilight Lord Bathiel", -- [2]
					151813, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					85.6260000000002, -- [1]
					"Twilight Lord Bathiel", -- [2]
					151813, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150020] = {
				{
					17.7020000000011, -- [1]
					"Twilight Lord Bathiel", -- [2]
					150020, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					51.6450000000004, -- [1]
					"Twilight Lord Bathiel", -- [2]
					150020, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					85.6260000000002, -- [1]
					"Twilight Lord Bathiel", -- [2]
					150020, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[150035] = {
				{
					34.0639999999985, -- [1]
					"Restorative Waters", -- [2]
					150035, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.0639999999985, -- [1]
					"Restorative Waters", -- [2]
					150035, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					34.0639999999985, -- [1]
					"Restorative Waters", -- [2]
					150035, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					56.3290000000016, -- [1]
					"Restorative Waters", -- [2]
					150035, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					69.2540000000008, -- [1]
					"Restorative Waters", -- [2]
					150035, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					69.2540000000008, -- [1]
					"Restorative Waters", -- [2]
					150035, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [119]
		{
			[149955] = {
				{
					48.2279999999992, -- [1]
					"Executioner Gore", -- [2]
					149955, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					95.5339999999997, -- [1]
					"Executioner Gore", -- [2]
					149955, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[149943] = {
				{
					13.8110000000015, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					29.5989999999983, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					45.387999999999, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					61.1879999999983, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					76.9330000000009, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					92.7030000000013, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					108.505000000001, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
			[150405] = {
				{
					112.005000000001, -- [1]
					"Twilight Storm Mender", -- [2]
					150405, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [1]
			},
		}, -- [120]
		{
			[149943] = {
				{
					13.2259999999988, -- [1]
					"Executioner Gore", -- [2]
					149943, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[149916] = {
				{
					30.9809999999998, -- [1]
					"Guardian of the Deep", -- [2]
					149916, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					43.1160000000018, -- [1]
					"Guardian of the Deep", -- [2]
					149916, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					55.2609999999986, -- [1]
					"Guardian of the Deep", -- [2]
					149916, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					67.387999999999, -- [1]
					"Guardian of the Deep", -- [2]
					149916, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					79.5489999999991, -- [1]
					"Guardian of the Deep", -- [2]
					149916, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[152753] = {
				{
					51.6160000000018, -- [1]
					"Guardian of the Deep", -- [2]
					152753, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[149920] = {
				{
					56.1810000000005, -- [1]
					"Guardian of the Deep", -- [2]
					149920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					86.5159999999996, -- [1]
					"Guardian of the Deep", -- [2]
					149920, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[152757] = {
				{
					52.0270000000019, -- [1]
					"Guardian of the Deep", -- [2]
					152757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					52.2920000000013, -- [1]
					"Guardian of the Deep", -- [2]
					152757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [121]
		{
			[150377] = {
				{
					4.37199999999939, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [1]
				{
					4.37199999999939, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [2]
				{
					11.6440000000002, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [3]
				{
					11.6440000000002, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [4]
				{
					18.9249999999993, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [5]
			},
			[150543] = {
				{
					8.80199999999968, -- [1]
					"Twilight Shadowmage", -- [2]
					150543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [1]
				{
					11.2279999999992, -- [1]
					"Twilight Shadowmage", -- [2]
					150543, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [2]
			},
		}, -- [122]
		{
			[149908] = {
				{
					11.3709999999992, -- [1]
					"Thruk", -- [2]
					149908, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					43.3109999999979, -- [1]
					"Thruk", -- [2]
					149908, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[149913] = {
				{
					24.5159999999996, -- [1]
					"Thruk", -- [2]
					149913, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					55.2789999999986, -- [1]
					"Thruk", -- [2]
					149913, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [123]
		{
			[151159] = {
				{
					28.9929999999986, -- [1]
					"Subjugator Kor'ul", -- [2]
					151159, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[150634] = {
				{
					12.6519999999982, -- [1]
					"Subjugator Kor'ul", -- [2]
					150634, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [1]
				{
					35.6859999999979, -- [1]
					"Subjugator Kor'ul", -- [2]
					150634, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [2]
				{
					58.7589999999982, -- [1]
					"Subjugator Kor'ul", -- [2]
					150634, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [3]
			},
			[152417] = {
				{
					37.6260000000002, -- [1]
					"Deep Terror", -- [2]
					152417, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					37.8889999999992, -- [1]
					"Deep Terror", -- [2]
					152417, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					47.8509999999987, -- [1]
					"Deep Terror", -- [2]
					152417, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					48.3689999999988, -- [1]
					"Deep Terror", -- [2]
					152417, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[151963] = {
				{
					72.9989999999998, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					72.9989999999998, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					72.9989999999998, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					72.9989999999998, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					72.9989999999998, -- [1]
					"Deep Terror", -- [2]
					151963, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [124]
		{
			[149893] = {
				{
					10.0360000000001, -- [1]
					"Domina", -- [2]
					149893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					30.6389999999992, -- [1]
					"Domina", -- [2]
					149893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					51.3009999999995, -- [1]
					"Domina", -- [2]
					149893, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[149886] = {
				{
					18.2469999999994, -- [1]
					"Domina", -- [2]
					149886, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					58.2779999999984, -- [1]
					"Domina", -- [2]
					149886, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[150377] = {
				{
					31.2750000000015, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [1]
				{
					38.5679999999993, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [2]
				{
					45.8539999999994, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [3]
				{
					53.137999999999, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [4]
				{
					60.4409999999989, -- [1]
					"Twilight Disciple", -- [2]
					150377, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [5]
			},
		}, -- [125]
		{
			[149865] = {
				{
					8.44599999999991, -- [1]
					"Ghamoo-Ra", -- [2]
					149865, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.744999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					149865, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[151681] = {
				{
					1.8179999999993, -- [1]
					"Ghamoo-Ra", -- [2]
					151681, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[149869] = {
				{
					8.8070000000007, -- [1]
					"Ghamoo-Ra", -- [2]
					149869, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					39.4000000000015, -- [1]
					"Ghamoo-Ra", -- [2]
					149869, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[151684] = {
				{
					4.82800000000134, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [1]
				{
					4.83799999999974, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [2]
				{
					4.83799999999974, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [3]
				{
					7.82600000000093, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [4]
				{
					7.82600000000093, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [5]
				{
					7.82600000000093, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [6]
				{
					10.8240000000005, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [7]
				{
					10.8240000000005, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [8]
				{
					10.8240000000005, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [9]
				{
					13.8209999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [10]
				{
					13.8209999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [11]
				{
					13.8209999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [12]
				{
					16.8220000000001, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [13]
				{
					16.8220000000001, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [14]
				{
					16.8220000000001, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [15]
				{
					19.8189999999995, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [16]
				{
					19.8189999999995, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [17]
				{
					19.8189999999995, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [18]
				{
					22.8150000000023, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [19]
				{
					22.8150000000023, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [20]
				{
					22.8150000000023, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [21]
				{
					25.8260000000009, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [22]
				{
					25.8260000000009, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [23]
				{
					25.8260000000009, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [24]
				{
					28.8189999999995, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [25]
				{
					28.8189999999995, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [26]
				{
					28.8189999999995, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [27]
				{
					31.8209999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [28]
				{
					31.8209999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [29]
				{
					31.8209999999999, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [30]
				{
					34.8230000000003, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [31]
				{
					34.8230000000003, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [32]
				{
					34.8230000000003, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [33]
				{
					37.8339999999989, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [34]
				{
					37.8339999999989, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [35]
				{
					37.8339999999989, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [36]
				{
					40.831000000002, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [37]
				{
					40.831000000002, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [38]
				{
					40.831000000002, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Moistshield-Mal'Ganis", -- [5]
				}, -- [39]
				{
					43.8290000000016, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [40]
				{
					43.8290000000016, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [41]
				{
					43.8290000000016, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tolten-Spinebreaker", -- [5]
				}, -- [42]
				{
					46.8240000000005, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [43]
				{
					46.8240000000005, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [44]
				{
					46.8240000000005, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [45]
				{
					49.8199999999997, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [46]
				{
					49.8199999999997, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Pâmito-Goldrinn", -- [5]
				}, -- [47]
				{
					49.8199999999997, -- [1]
					"Ghamoo-Ra", -- [2]
					151684, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Matera-Scilla", -- [5]
				}, -- [48]
			},
			[150438] = {
				{
					35.2260000000024, -- [1]
					"Razorshell Snapjaw", -- [2]
					150438, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					52.2040000000015, -- [1]
					"Razorshell Snapjaw", -- [2]
					150438, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [126]
		{
			[89263] = {
				{
					6.27399999999989, -- [1]
					"\"Captain\" Cookie", -- [2]
					89263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Maorc-ShatteredHand", -- [5]
				}, -- [1]
				{
					37.788, -- [1]
					"\"Captain\" Cookie", -- [2]
					89263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
			[90680] = {
				{
					35.3539999999998, -- [1]
					"\"Captain\" Cookie", -- [2]
					90680, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Illuxion-WyrmrestAccord", -- [5]
				}, -- [1]
			},
			[90555] = {
				{
					20.8319999999999, -- [1]
					"\"Captain\" Cookie", -- [2]
					90555, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Maorc-ShatteredHand", -- [5]
				}, -- [1]
			},
			[90556] = {
				{
					15.9769999999999, -- [1]
					"\"Captain\" Cookie", -- [2]
					90556, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[89252] = {
				{
					4.27399999999989, -- [1]
					"\"Captain\" Cookie", -- [2]
					89252, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90603] = {
				{
					13.5539999999996, -- [1]
					"\"Captain\" Cookie", -- [2]
					90603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[89734] = {
				{
					10.7309999999998, -- [1]
					"Rotten Corn", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					15.5749999999998, -- [1]
					"Rotten Steak", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					20.4299999999998, -- [1]
					"Rotten Loaf", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					25.2759999999998, -- [1]
					"Rotten Melon", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					30.1179999999999, -- [1]
					"Rotten Mystery Meat", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					34.9639999999999, -- [1]
					"Rotten Bun", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[90605] = {
				{
					18.3969999999999, -- [1]
					"\"Captain\" Cookie", -- [2]
					90605, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Superdrol-Velen", -- [5]
				}, -- [1]
			},
			[90606] = {
				{
					32.9449999999997, -- [1]
					"\"Captain\" Cookie", -- [2]
					90606, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[90557] = {
				{
					30.5079999999998, -- [1]
					"\"Captain\" Cookie", -- [2]
					90557, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Maorc-ShatteredHand", -- [5]
				}, -- [1]
			},
			[90604] = {
				{
					28.0889999999999, -- [1]
					"\"Captain\" Cookie", -- [2]
					90604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Superdrol-Velen", -- [5]
				}, -- [1]
			},
			[89739] = {
				{
					8.69399999999996, -- [1]
					"\"Captain\" Cookie", -- [2]
					89739, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Superdrol-Velen", -- [5]
				}, -- [1]
			},
			[90560] = {
				{
					25.6679999999997, -- [1]
					"\"Captain\" Cookie", -- [2]
					90560, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Illuxion-WyrmrestAccord", -- [5]
				}, -- [1]
			},
			[90602] = {
				{
					23.2469999999998, -- [1]
					"\"Captain\" Cookie", -- [2]
					90602, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [1]
			},
			[90559] = {
				{
					11.1109999999999, -- [1]
					"\"Captain\" Cookie", -- [2]
					90559, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Maorc-ShatteredHand", -- [5]
				}, -- [1]
			},
		}, -- [127]
		{
			[88839] = {
				{
					3.63300000000027, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [1]
				{
					7.27199999999994, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [2]
				{
					10.9090000000001, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [3]
				{
					32.7919999999999, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [4]
				{
					36.4010000000003, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [5]
				{
					52.21, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [6]
				{
					55.8380000000002, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [7]
				{
					59.4580000000001, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [8]
				{
					63.0940000000001, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [9]
				{
					74.029, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [10]
				{
					77.6690000000003, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [11]
				{
					81.308, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [12]
			},
			[88836] = {
				{
					49.8110000000002, -- [1]
					"Admiral Ripsnarl", -- [2]
					88836, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Wild Imp", -- [5]
				}, -- [1]
			},
		}, -- [128]
		{
			[88490] = {
				{
					2.22900000000004, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [1]
				{
					21.0380000000002, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [2]
				{
					23.0629999999999, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [3]
				{
					25.4939999999999, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [4]
				{
					27.9190000000001, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [5]
				{
					33.9510000000003, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [6]
				{
					36.4110000000003, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [7]
				{
					38.845, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [8]
				{
					41.278, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [9]
				{
					43.6780000000001, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [10]
			},
			[88522] = {
				{
					30.7380000000001, -- [1]
					"Foe Reaper 5000", -- [2]
					88522, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88481] = {
				{
					7.07300000000009, -- [1]
					"Foe Reaper 5000", -- [2]
					88481, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90928] = {
				{
					47.0110000000002, -- [1]
					"Defias Enforcer", -- [2]
					90928, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Kade-Scilla", -- [5]
				}, -- [1]
			},
			[89757] = {
				{
					3.44299999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					3.85300000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					4.66600000000017, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					5.05700000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					5.05700000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					6.28600000000029, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					7.10299999999984, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					10.7300000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					11.1400000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					11.952, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					12.365, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					12.3750000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					13.5690000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					14.3799999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					18.0260000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					18.4379999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					19.2250000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					19.635, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					19.635, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					20.8480000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					21.6490000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					25.2929999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					25.6850000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					26.4910000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					26.9020000000003, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					26.912, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					28.1090000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					28.9089999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					32.55, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					32.9510000000003, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					33.751, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					34.1850000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					34.1850000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					35.3779999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					36.1780000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					39.845, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					40.2450000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					41.0449999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					41.4779999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					41.4779999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					42.6780000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					43.4779999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					47.0110000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
			},
			[89652] = {
				{
					8.11500000000001, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					11.2259999999999, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.0449999999999, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					39.6450000000002, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
		}, -- [129]
		{
			[89757] = {
				{
					0.501999999999953, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					0.921999999999798, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					1.71399999999994, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					2.13400000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					2.13400000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					3.33799999999997, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					4.1579999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					7.7829999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					8.18599999999992, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					9.00900000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					9.40899999999988, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					9.40899999999988, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					10.626, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					11.4269999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					15.067, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					15.4769999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					16.2729999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					16.6779999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					16.6879999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					17.8989999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					18.7149999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					22.3539999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					22.7539999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					23.5639999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					23.9689999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					23.9689999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					25.1789999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					25.9989999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					29.6009999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					30.001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					30.8209999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					31.231, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					31.231, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					32.4459999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					33.2459999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					36.8829999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					37.2729999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					38.086, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					38.4789999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					38.4889999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					39.704, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					40.5159999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					44.135, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					44.5409999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					45.3439999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					45.7339999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					45.7539999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					46.9579999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					47.78, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					51.4109999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					51.8109999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					52.6119999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					53.0139999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					53.0139999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					54.2269999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					55.0269999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [60]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [61]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [62]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [63]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [64]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [65]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [66]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [67]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [68]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [69]
				{
					57.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [70]
			},
			[88288] = {
				{
					18.9349999999999, -- [1]
					"Lumbering Oaf", -- [2]
					88288, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Illuxion-WyrmrestAccord", -- [5]
				}, -- [1]
			},
			[88974] = {
				{
					42.3219999999999, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					57.0029999999999, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					57.0029999999999, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[88300] = {
				{
					26.3489999999999, -- [1]
					"Lumbering Oaf", -- [2]
					88300, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90047] = {
				{
					0.309999999999945, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [1]
				{
					11.2369999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [2]
				{
					26.9989999999998, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [3]
				{
					39.0889999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [4]
				{
					51.2069999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [5]
				{
					57.0029999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [6]
			},
			[89652] = {
				{
					0.309999999999945, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					18.1109999999999, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					30.6209999999999, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					49.597, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					57.0029999999999, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
		}, -- [130]
		{
			[89757] = {
				{
					3.50199999999995, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					3.90200000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					5.1110000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					10.7739999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					11.1890000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					12.3989999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					18.0619999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					18.472, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					19.6859999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					24.135, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					24.5350000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					25.345, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					25.7549999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					25.7549999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					26.9649999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					27.7750000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					31.403, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					31.8029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					32.604, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					33.0139999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					33.0139999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					34.2239999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					35.037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					38.6690000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					39.069, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					39.8710000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					40.281, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					40.281, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					41.4970000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					42.3040000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					45.9500000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					46.347, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					47.0029999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
			},
			[88169] = {
				{
					34.0250000000001, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.0250000000001, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					35.644, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					36.4480000000001, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					36.4580000000001, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					37.25, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					37.6669999999999, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					37.6669999999999, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					38.8789999999999, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					39.691, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
			},
			[90047] = {
				{
					4.51600000000008, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [1]
				{
					20.28, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [2]
				{
					33.625, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [3]
				{
					44.5260000000001, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [4]
				{
					47.0029999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [5]
			},
			[87859] = {
				{
					8.12400000000002, -- [1]
					"Glubtok", -- [2]
					87859, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[91034] = {
				{
					47.0029999999999, -- [1]
					"Goblin Overseer", -- [2]
					91034, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87861] = {
				{
					21.492, -- [1]
					"Glubtok", -- [2]
					87861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88129] = {
				{
					33.625, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.827, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					35.2380000000001, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					36.4480000000001, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					38.4690000000001, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[90096] = {
				{
					47.0029999999999, -- [1]
					"Explode", -- [2]
					90096, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[89769] = {
				{
					47.0029999999999, -- [1]
					"Mining Powder", -- [2]
					89769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[89652] = {
				{
					1.69299999999998, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					9.36899999999992, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					33.2139999999999, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					38.4690000000001, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			[88736] = {
				{
					47.0029999999999, -- [1]
					"Admiral Ripsnarl", -- [2]
					88736, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88009] = {
				{
					30.3889999999999, -- [1]
					"Glubtok", -- [2]
					88009, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [131]
		{
			[7399] = {
				{
					6.80499999999984, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					32.2860000000001, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [2]
				{
					49.2950000000001, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [3]
			},
			[8150] = {
				{
					4.36499999999978, -- [1]
					"Mutanus the Devourer", -- [2]
					8150, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[7967] = {
				{
					52.5299999999998, -- [1]
					"Mutanus the Devourer", -- [2]
					7967, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Klanguinho-Gallywix", -- [5]
				}, -- [1]
			},
		}, -- [132]
		{
			[8142] = {
				{
					17.3620000000001, -- [1]
					"Verdan the Everliving", -- [2]
					8142, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [133]
		{
			[8040] = {
				{
					24.5639999999999, -- [1]
					"Lord Serpentis", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Klanguinho-Gallywix", -- [5]
				}, -- [1]
				{
					43.1509999999998, -- [1]
					"Lord Serpentis", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Klanguinho-Gallywix", -- [5]
				}, -- [2]
				{
					75.9319999999998, -- [1]
					"Lord Serpentis", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
			},
			[7951] = {
				{
					1.4699999999998, -- [1]
					"Deviate Venomwing", -- [2]
					7951, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					4.70299999999997, -- [1]
					"Deviate Venomwing", -- [2]
					7951, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
				{
					18.0209999999997, -- [1]
					"Deviate Venomwing", -- [2]
					7951, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [3]
			},
			[12167] = {
				{
					2.06999999999971, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					8.10699999999997, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
				{
					12.953, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [3]
				{
					29.9079999999999, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [4]
				{
					36.3799999999997, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [5]
				{
					40.011, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [6]
				{
					52.1579999999999, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [7]
				{
					55.8069999999998, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [8]
				{
					64.2759999999998, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [9]
				{
					70.3739999999998, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [10]
			},
			[9532] = {
				{
					46.0589999999997, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lumidrael-Area52", -- [5]
				}, -- [1]
			},
			[8041] = {
				{
					52.8019999999997, -- [1]
					"Druid of the Fang", -- [2]
					8041, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[7399] = {
				{
					3.10999999999967, -- [1]
					"Deviate Dreadfang", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
			},
			[23381] = {
				{
					48.5169999999998, -- [1]
					"Lord Serpentis", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lord Serpentis", -- [5]
				}, -- [1]
				{
					50.935, -- [1]
					"Druid of the Fang", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lord Serpentis", -- [5]
				}, -- [2]
				{
					59.4389999999999, -- [1]
					"Lord Serpentis", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lord Serpentis", -- [5]
				}, -- [3]
			},
		}, -- [134]
		{
			[6254] = {
				{
					3.07200000000012, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					6.70700000000011, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
				{
					16.4210000000001, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [3]
				{
					20.0650000000001, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [4]
				{
					22.4590000000001, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [5]
			},
		}, -- [135]
		{
			[80362] = {
				{
					4.86000000000013, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					21.8630000000001, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
				{
					37.6480000000001, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [3]
			},
		}, -- [136]
		{
			[20805] = {
				{
					2.16000000000008, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					6.00500000000011, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [2]
				{
					9.85400000000004, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [3]
				{
					15.326, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [4]
			},
			[7965] = {
				{
					18.3880000000002, -- [1]
					"Lord Cobrahn", -- [2]
					7965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [137]
		{
			[8147] = {
				{
					8.58400000000006, -- [1]
					"Lord Pythas", -- [2]
					8147, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					27.952, -- [1]
					"Lord Pythas", -- [2]
					8147, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[20805] = {
				{
					3.01800000000003, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					23.6859999999999, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [2]
				{
					27.3009999999999, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [3]
				{
					33.384, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [4]
				{
					37.02, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [5]
			},
			[8040] = {
				{
					18.3399999999999, -- [1]
					"Lord Pythas", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[9532] = {
				{
					2.0920000000001, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					5.51199999999994, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
				{
					10.3779999999999, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [3]
			},
		}, -- [138]
		{
			[8040] = {
				{
					8.89100000000008, -- [1]
					"Druid of the Fang", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lumidrael-Area52", -- [5]
				}, -- [1]
				{
					8.89100000000008, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lumidrael-Area52", -- [5]
				}, -- [2]
				{
					24.683, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Klanguinho-Gallywix", -- [5]
				}, -- [3]
				{
					42.8720000000001, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Klanguinho-Gallywix", -- [5]
				}, -- [4]
				{
					53.7750000000001, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Klanguinho-Gallywix", -- [5]
				}, -- [5]
			},
			[20295] = {
				{
					2.02300000000014, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					5.74400000000014, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
				{
					13.029, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [3]
				{
					16.6460000000002, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [4]
				{
					20.105, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [5]
				{
					30.038, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [6]
				{
					33.681, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [7]
				{
					36.9160000000002, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"kickiehoofie", -- [5]
				}, -- [8]
				{
					46.999, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [9]
				{
					50.6600000000001, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [10]
			},
			[8148] = {
				{
					2.75300000000016, -- [1]
					"Lady Anacondra", -- [2]
					8148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[9532] = {
				{
					2.91300000000001, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [1]
				{
					20.306, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tresianda-Area52", -- [5]
				}, -- [2]
			},
		}, -- [139]
		{
			[120024] = {
				{
					11.4960000000001, -- [1]
					"Lava Guard Gordoth", -- [2]
					120024, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					46.6400000000003, -- [1]
					"Lava Guard Gordoth", -- [2]
					120024, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[119999] = {
				{
					6.45700000000034, -- [1]
					"Lava Guard Gordoth", -- [2]
					119999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gobbyboom-Llane", -- [5]
				}, -- [1]
				{
					18.5410000000002, -- [1]
					"Lava Guard Gordoth", -- [2]
					119999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Khabib-TheScryers", -- [5]
				}, -- [2]
				{
					30.567, -- [1]
					"Lava Guard Gordoth", -- [2]
					119999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Gobbyboom-Llane", -- [5]
				}, -- [3]
				{
					41.5860000000002, -- [1]
					"Lava Guard Gordoth", -- [2]
					119999, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Eevee-Llane", -- [5]
				}, -- [4]
			},
			[50420] = {
				{
					44.1999999999998, -- [1]
					"Lava Guard Gordoth", -- [2]
					50420, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [140]
		{
			[119434] = {
				{
					2.53400000000056, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					5.95499999999993, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					9.60199999999986, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					13.2300000000005, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					16.8630000000003, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					24.1660000000002, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					27.7970000000005, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					31.4610000000002, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					35.1090000000004, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					38.7520000000004, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					46.031, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					49.665, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					53.2970000000005, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					56.9180000000006, -- [1]
					"Slagmaw", -- [2]
					119434, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
			},
		}, -- [141]
		{
			[119300] = {
				{
					6.29800000000068, -- [1]
					"Dark Shaman Koranthal", -- [2]
					119300, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Khabib-TheScryers", -- [5]
				}, -- [1]
				{
					13.5910000000004, -- [1]
					"Dark Shaman Koranthal", -- [2]
					119300, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Khabib-TheScryers", -- [5]
				}, -- [2]
				{
					20.8810000000003, -- [1]
					"Dark Shaman Koranthal", -- [2]
					119300, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Khabib-TheScryers", -- [5]
				}, -- [3]
			},
			[119971] = {
				{
					21.2910000000002, -- [1]
					"Dark Shaman Koranthal", -- [2]
					119971, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [142]
		{
			[119420] = {
				{
					13.085, -- [1]
					"Adarogg", -- [2]
					119420, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Khabib-TheScryers", -- [5]
				}, -- [1]
				{
					32.482, -- [1]
					"Adarogg", -- [2]
					119420, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Khabib-TheScryers", -- [5]
				}, -- [2]
			},
			[119405] = {
				{
					18.8419999999996, -- [1]
					"Adarogg", -- [2]
					119405, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					38.2419999999993, -- [1]
					"Adarogg", -- [2]
					119405, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [143]
		{
			[7399] = {
				{
					18.7010000000009, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Shirele-Stormreaver", -- [5]
				}, -- [1]
				{
					39.3329999999987, -- [1]
					"Mutanus the Devourer", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Shirele-Stormreaver", -- [5]
				}, -- [2]
			},
			[7967] = {
				{
					16.6680000000051, -- [1]
					"Mutanus the Devourer", -- [2]
					7967, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Uncleherb-Skullcrusher", -- [5]
				}, -- [1]
				{
					34.0630000000019, -- [1]
					"Mutanus the Devourer", -- [2]
					7967, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dracohasta", -- [5]
				}, -- [2]
			},
			[8150] = {
				{
					1.86500000000524, -- [1]
					"Mutanus the Devourer", -- [2]
					8150, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [144]
		{
			[8142] = {
				{
					22.1080000000002, -- [1]
					"Verdan the Everliving", -- [2]
					8142, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [145]
		{
			[8040] = {
				{
					5.68400000000111, -- [1]
					"Lord Serpentis", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					12.9559999999983, -- [1]
					"Druid of the Fang", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dracohasta", -- [5]
				}, -- [2]
			},
			[9532] = {
				{
					2.23500000000058, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dracohasta", -- [5]
				}, -- [1]
				{
					8.61000000000058, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [2]
			},
			[12167] = {
				{
					1.33299999999872, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Shirele-Stormreaver", -- [5]
				}, -- [1]
				{
					9.82100000000355, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Shirele-Stormreaver", -- [5]
				}, -- [2]
				{
					13.448000000004, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Shirele-Stormreaver", -- [5]
				}, -- [3]
				{
					35.3190000000031, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [4]
				{
					41.7980000000025, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [5]
				{
					54.7160000000004, -- [1]
					"Lord Serpentis", -- [2]
					12167, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [6]
			},
			[7399] = {
				{
					12.8830000000016, -- [1]
					"Deviate Dreadfang", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					35.9599999999991, -- [1]
					"Deviate Dreadfang", -- [2]
					7399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
			[23381] = {
				{
					21.9670000000042, -- [1]
					"Lord Serpentis", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deviate Dreadfang", -- [5]
				}, -- [1]
				{
					45.0279999999984, -- [1]
					"Lord Serpentis", -- [2]
					23381, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Deviate Dreadfang", -- [5]
				}, -- [2]
			},
		}, -- [146]
		{
			[6254] = {
				{
					5.34999999999855, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [1]
				{
					7.79400000000169, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [2]
				{
					11.3970000000045, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [3]
				{
					15.0440000000017, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [4]
				{
					21.1189999999988, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [5]
				{
					24.7690000000002, -- [1]
					"Skum", -- [2]
					6254, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [6]
			},
		}, -- [147]
		{
			[80362] = {
				{
					6.18600000000151, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dracohasta", -- [5]
				}, -- [1]
				{
					21.9570000000022, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [2]
				{
					37.7630000000063, -- [1]
					"Kresh", -- [2]
					80362, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [3]
			},
		}, -- [148]
		{
			[20805] = {
				{
					1.54099999999744, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [1]
				{
					10.9519999999975, -- [1]
					"Lord Cobrahn", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [2]
			},
			[7965] = {
				{
					16.4439999999959, -- [1]
					"Lord Cobrahn", -- [2]
					7965, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[744] = {
				{
					25.4539999999979, -- [1]
					"Lord Cobrahn", -- [2]
					744, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [1]
			},
		}, -- [149]
		{
			[9532] = {
				{
					2.38999999999942, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [1]
				{
					10.1679999999978, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [2]
				{
					24.6879999999946, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [3]
			},
			[20805] = {
				{
					2.39999999999418, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [1]
				{
					21.0709999999963, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [2]
				{
					30.7520000000004, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [3]
				{
					38.2619999999952, -- [1]
					"Lord Pythas", -- [2]
					20805, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [4]
			},
			[11431] = {
				{
					11.3639999999941, -- [1]
					"Lord Pythas", -- [2]
					11431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Druid of the Fang", -- [5]
				}, -- [1]
				{
					34.3819999999978, -- [1]
					"Lord Pythas", -- [2]
					11431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Lord Pythas", -- [5]
				}, -- [2]
			},
			[8040] = {
				{
					14.525999999998, -- [1]
					"Lord Pythas", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
		}, -- [150]
		{
			[9532] = {
				{
					1.80599999999686, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [1]
				{
					7.85999999999331, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [2]
				{
					11.4589999999953, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [3]
				{
					53.6549999999988, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dracohasta", -- [5]
				}, -- [4]
				{
					57.2359999999972, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Trigore", -- [5]
				}, -- [5]
				{
					64.4839999999968, -- [1]
					"Druid of the Fang", -- [2]
					9532, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Dracohasta", -- [5]
				}, -- [6]
			},
			[20295] = {
				{
					3.27499999999418, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Syzygy-Andorhal", -- [5]
				}, -- [1]
				{
					12.2949999999983, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [2]
				{
					15.9239999999991, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [3]
				{
					19.5949999999939, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [4]
				{
					26.8599999999933, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [5]
				{
					30.5109999999986, -- [1]
					"Lady Anacondra", -- [2]
					20295, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [6]
			},
			[8040] = {
				{
					6.04799999999523, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Bulkybeef-Area52", -- [5]
				}, -- [1]
				{
					14.6309999999939, -- [1]
					"Druid of the Fang", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Uncleherb-Skullcrusher", -- [5]
				}, -- [2]
				{
					23.1229999999996, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [3]
				{
					38.4309999999969, -- [1]
					"Lady Anacondra", -- [2]
					8040, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [4]
			},
			[8148] = {
				{
					9.29099999999744, -- [1]
					"Lady Anacondra", -- [2]
					8148, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [151]
		{
			[89263] = {
				{
					7.55099999999948, -- [1]
					"\"Captain\" Cookie", -- [2]
					89263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
				{
					39.0899999999965, -- [1]
					"\"Captain\" Cookie", -- [2]
					89263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [2]
			},
			[90680] = {
				{
					36.6710000000021, -- [1]
					"\"Captain\" Cookie", -- [2]
					90680, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[90555] = {
				{
					22.1039999999994, -- [1]
					"\"Captain\" Cookie", -- [2]
					90555, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [1]
			},
			[90556] = {
				{
					17.2750000000015, -- [1]
					"\"Captain\" Cookie", -- [2]
					90556, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purrcilla-Azshara", -- [5]
				}, -- [1]
			},
			[89252] = {
				{
					5.55299999999988, -- [1]
					"\"Captain\" Cookie", -- [2]
					89252, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90603] = {
				{
					14.8369999999995, -- [1]
					"\"Captain\" Cookie", -- [2]
					90603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purrcilla-Azshara", -- [5]
				}, -- [1]
			},
			[89734] = {
				{
					12.0310000000027, -- [1]
					"Rotten Corn", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					16.872000000003, -- [1]
					"Rotten Steak", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					21.7229999999981, -- [1]
					"Rotten Loaf", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					26.5849999999991, -- [1]
					"Rotten Melon", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					31.4579999999987, -- [1]
					"Rotten Mystery Meat", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					36.2790000000023, -- [1]
					"Rotten Bun", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[90605] = {
				{
					19.7139999999999, -- [1]
					"\"Captain\" Cookie", -- [2]
					90605, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purrcilla-Azshara", -- [5]
				}, -- [1]
			},
			[90606] = {
				{
					34.2540000000008, -- [1]
					"\"Captain\" Cookie", -- [2]
					90606, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tranayus-Tichondrius", -- [5]
				}, -- [1]
			},
			[90557] = {
				{
					31.8289999999979, -- [1]
					"\"Captain\" Cookie", -- [2]
					90557, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [1]
			},
			[90604] = {
				{
					29.4029999999984, -- [1]
					"\"Captain\" Cookie", -- [2]
					90604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[89739] = {
				{
					9.97200000000157, -- [1]
					"\"Captain\" Cookie", -- [2]
					89739, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Tranayus-Tichondrius", -- [5]
				}, -- [1]
			},
			[90560] = {
				{
					26.9749999999985, -- [1]
					"\"Captain\" Cookie", -- [2]
					90560, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purrcilla-Azshara", -- [5]
				}, -- [1]
			},
			[90602] = {
				{
					24.5380000000005, -- [1]
					"\"Captain\" Cookie", -- [2]
					90602, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [1]
			},
			[90559] = {
				{
					12.4110000000001, -- [1]
					"\"Captain\" Cookie", -- [2]
					90559, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purrcilla-Azshara", -- [5]
				}, -- [1]
			},
		}, -- [152]
		{
			[88839] = {
				{
					2.25499999999738, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [1]
				{
					5.8859999999986, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [2]
				{
					9.51199999999517, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [3]
				{
					30.5879999999961, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [4]
				{
					34.1939999999959, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [5]
				{
					39.0639999999985, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Varallinda-Dalvengyr", -- [5]
				}, -- [6]
				{
					54.8770000000004, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [7]
				{
					58.5199999999968, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [8]
				{
					62.1399999999994, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [9]
				{
					65.7589999999982, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [10]
				{
					76.663999999997, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [11]
				{
					80.3169999999955, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [12]
				{
					83.9199999999983, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [13]
				{
					87.5699999999997, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [14]
			},
			[88836] = {
				{
					30.5879999999961, -- [1]
					"Admiral Ripsnarl", -- [2]
					88836, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Purrcilla-Azshara", -- [5]
				}, -- [1]
			},
		}, -- [153]
		{
			[88490] = {
				{
					4.3660000000018, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [1]
				{
					20.2920000000013, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [2]
				{
					22.7020000000048, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [3]
				{
					25.1310000000012, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [4]
				{
					39.7160000000004, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [5]
				{
					42.1549999999988, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [6]
				{
					44.5760000000009, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [7]
				{
					46.9850000000006, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Quinba-Nemesis", -- [5]
				}, -- [8]
			},
			[88522] = {
				{
					36.0710000000036, -- [1]
					"Foe Reaper 5000", -- [2]
					88522, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88481] = {
				{
					8.7190000000046, -- [1]
					"Foe Reaper 5000", -- [2]
					88481, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90928] = {
				{
					52.0030000000043, -- [1]
					"Defias Enforcer", -- [2]
					90928, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Varallinda-Dalvengyr", -- [5]
				}, -- [1]
				{
					52.0030000000043, -- [1]
					"Defias Enforcer", -- [2]
					90928, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Varallinda-Dalvengyr", -- [5]
				}, -- [2]
			},
			[89757] = {
				{
					2.6620000000039, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					3.0570000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					3.0570000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					3.47699999999895, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					3.86499999999796, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					4.27599999999802, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					5.48500000000058, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					9.95799999999872, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					10.375, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					10.375, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					10.7710000000006, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					11.1869999999981, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					11.5930000000008, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					12.7989999999991, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					17.2430000000022, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					17.6529999999984, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					17.6529999999984, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					18.0540000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					18.4590000000026, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					18.8700000000026, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					20.0829999999987, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					24.5220000000045, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					24.9340000000011, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					24.9340000000011, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					25.3250000000044, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					25.7410000000018, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					26.1300000000047, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					27.3539999999994, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					31.823000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					32.224000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					32.224000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					32.6270000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					33.0319999999992, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					33.4360000000015, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					34.6390000000029, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					39.0930000000008, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					39.5060000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					39.5060000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					39.9110000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					40.3139999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					40.726999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					41.9429999999993, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					46.3800000000047, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					46.7930000000051, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					46.7930000000051, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					47.1770000000033, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					47.5900000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					47.9809999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					49.2030000000013, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [60]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [61]
				{
					52.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [62]
			},
		}, -- [154]
		{
			[89757] = {
				{
					3.19799999999668, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					3.58999999999651, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					3.58999999999651, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					3.99399999999878, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					4.40000000000146, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					4.8070000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					6.01199999999517, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					10.4459999999963, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					10.8399999999965, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					10.8399999999965, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					11.2479999999996, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					11.6399999999994, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					12.0440000000017, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					13.2710000000006, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					17.6909999999989, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					18.0970000000016, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					18.0970000000016, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					18.5, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					18.9059999999954, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					19.3070000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					20.5210000000006, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					24.9700000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					25.3799999999974, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					25.3799999999974, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					25.7770000000019, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					26.1869999999981, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					26.5800000000018, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					27.7880000000005, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					32.265999999996, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					32.6579999999958, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					32.6579999999958, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					33.0689999999959, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					33.4680000000008, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					33.8729999999996, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					35.0879999999961, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					39.5339999999997, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					39.9389999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					39.9389999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					40.3419999999969, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					40.7430000000022, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					41.1419999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					42.3529999999955, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					46.788999999997, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					47.1889999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					47.1889999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					47.6129999999976, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					48.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					48.400999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					49.6319999999978, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					54.0910000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					54.5040000000008, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					54.5040000000008, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					54.8899999999994, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					55.3139999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					55.7039999999979, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					56.9229999999952, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					61.3629999999976, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					61.7690000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					61.7690000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
				{
					62.1869999999981, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [60]
				{
					62.5910000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [61]
				{
					62.9910000000018, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [62]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [63]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [64]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [65]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [66]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [67]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [68]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [69]
				{
					63.0009999999966, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [70]
			},
			[88288] = {
				{
					17.8969999999972, -- [1]
					"Lumbering Oaf", -- [2]
					88288, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Varallinda-Dalvengyr", -- [5]
				}, -- [1]
			},
			[88300] = {
				{
					26.5369999999966, -- [1]
					"Lumbering Oaf", -- [2]
					88300, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90047] = {
				{
					9.03299999999581, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [1]
				{
					22.351999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [2]
				{
					34.4850000000006, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [3]
				{
					46.5979999999981, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [4]
				{
					58.7359999999972, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [5]
				{
					63.0009999999966, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [6]
			},
			[88974] = {
				{
					43.364999999998, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					57.9369999999981, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					60.7669999999998, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
		}, -- [155]
		{
			[89757] = {
				{
					2.23800000000483, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					2.64199999999983, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					2.64199999999983, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					3.04400000000169, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					3.44400000000314, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					3.83700000000681, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					5.06400000000576, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					9.50200000000041, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					9.91400000000431, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					9.91400000000431, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					10.3210000000036, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					10.7070000000022, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					11.1140000000014, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					12.3250000000044, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					16.7690000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					17.1730000000025, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					17.5889999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					17.9800000000032, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					18.3970000000045, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					19.5930000000008, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					24.0650000000023, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					24.4650000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					24.4650000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					24.8650000000052, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					25.2990000000063, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					25.6650000000009, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					26.8990000000049, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					31.3250000000044, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					31.724000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					31.724000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					32.1380000000063, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					32.5300000000061, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					32.9470000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					34.1520000000019, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					38.5970000000016, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					39.0180000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					39.0180000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					39.4110000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					39.8160000000062, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					40.2350000000006, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					41.4300000000003, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					45.8670000000056, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					46.2720000000045, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					46.2820000000065, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					46.6900000000023, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					47.0950000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					47.4910000000018, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					48.6990000000005, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					53.1690000000017, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					53.5770000000048, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					53.5770000000048, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					53.9830000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					54.0030000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
			},
			[91034] = {
				{
					54.0030000000043, -- [1]
					"Goblin Overseer", -- [2]
					91034, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87861] = {
				{
					19.3780000000043, -- [1]
					"Glubtok", -- [2]
					87861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88129] = {
				{
					35.1690000000017, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.5640000000058, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					36.3770000000004, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					37.1940000000031, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					38.8110000000015, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					40.0320000000065, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					40.8380000000034, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					43.2540000000008, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					43.2540000000008, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
			},
			[88009] = {
				{
					30.7250000000058, -- [1]
					"Glubtok", -- [2]
					88009, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87859] = {
				{
					6.06100000000151, -- [1]
					"Glubtok", -- [2]
					87859, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90047] = {
				{
					3.25100000000384, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [1]
				{
					17.7790000000023, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [2]
				{
					32.3410000000004, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [3]
				{
					45.6860000000015, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [4]
				{
					54.0030000000043, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Digger", -- [5]
				}, -- [5]
			},
			[88169] = {
				{
					34.7700000000041, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					35.9680000000008, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					36.7970000000059, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					38.4120000000039, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					38.8110000000015, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					40.0220000000045, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					40.0320000000065, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					41.237000000001, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					42.0460000000021, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					42.0460000000021, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					42.8500000000058, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					45.6860000000015, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
			},
		}, -- [156]
		{
			[90602] = {
				{
					25.0789999999979, -- [1]
					"\"Captain\" Cookie", -- [2]
					90602, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ñøvä-Tichondrius", -- [5]
				}, -- [1]
			},
			[89263] = {
				{
					8.10199999999895, -- [1]
					"\"Captain\" Cookie", -- [2]
					89263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ñøvä-Tichondrius", -- [5]
				}, -- [1]
				{
					39.6540000000023, -- [1]
					"\"Captain\" Cookie", -- [2]
					89263, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ñøvä-Tichondrius", -- [5]
				}, -- [2]
			},
			[90559] = {
				{
					12.961000000003, -- [1]
					"\"Captain\" Cookie", -- [2]
					90559, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mongal-BlackDragonflight", -- [5]
				}, -- [1]
				{
					44.5089999999982, -- [1]
					"\"Captain\" Cookie", -- [2]
					90559, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mongal-BlackDragonflight", -- [5]
				}, -- [2]
			},
			[90555] = {
				{
					22.6450000000041, -- [1]
					"\"Captain\" Cookie", -- [2]
					90555, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [1]
			},
			[90556] = {
				{
					17.7810000000027, -- [1]
					"\"Captain\" Cookie", -- [2]
					90556, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[89252] = {
				{
					6.09399999999732, -- [1]
					"\"Captain\" Cookie", -- [2]
					89252, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90603] = {
				{
					15.3740000000034, -- [1]
					"\"Captain\" Cookie", -- [2]
					90603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nøctürnal-Tichondrius", -- [5]
				}, -- [1]
				{
					46.9360000000015, -- [1]
					"\"Captain\" Cookie", -- [2]
					90603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ñøvä-Tichondrius", -- [5]
				}, -- [2]
			},
			[90604] = {
				{
					29.9259999999995, -- [1]
					"\"Captain\" Cookie", -- [2]
					90604, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[90680] = {
				{
					37.2170000000042, -- [1]
					"\"Captain\" Cookie", -- [2]
					90680, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nøctürnal-Tichondrius", -- [5]
				}, -- [1]
			},
			[90606] = {
				{
					34.7900000000009, -- [1]
					"\"Captain\" Cookie", -- [2]
					90606, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nøctürnal-Tichondrius", -- [5]
				}, -- [1]
			},
			[90560] = {
				{
					27.4950000000026, -- [1]
					"\"Captain\" Cookie", -- [2]
					90560, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mongal-BlackDragonflight", -- [5]
				}, -- [1]
			},
			[86603] = {
				{
					48.6070000000036, -- [1]
					"Shattered Hand Assassin", -- [2]
					86603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					48.6070000000036, -- [1]
					"Shattered Hand Assassin", -- [2]
					86603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					48.6070000000036, -- [1]
					"Shattered Hand Assassin", -- [2]
					86603, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
			},
			[89739] = {
				{
					10.5380000000005, -- [1]
					"\"Captain\" Cookie", -- [2]
					89739, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Nøctürnal-Tichondrius", -- [5]
				}, -- [1]
				{
					42.0809999999983, -- [1]
					"\"Captain\" Cookie", -- [2]
					89739, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [2]
			},
			[90557] = {
				{
					32.3559999999998, -- [1]
					"\"Captain\" Cookie", -- [2]
					90557, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
			[90605] = {
				{
					20.2150000000038, -- [1]
					"\"Captain\" Cookie", -- [2]
					90605, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Mongal-BlackDragonflight", -- [5]
				}, -- [1]
			},
			[89734] = {
				{
					12.5679999999993, -- [1]
					"Rotten Corn", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					17.4110000000001, -- [1]
					"Rotten Steak", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					23.4680000000008, -- [1]
					"Rotten Loaf", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					27.1030000000028, -- [1]
					"Rotten Melon", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					31.9820000000036, -- [1]
					"Rotten Mystery Meat", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					36.8450000000012, -- [1]
					"Rotten Bun", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					44.125, -- [1]
					"Rotten Corn", -- [2]
					89734, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
			},
		}, -- [157]
		{
			[88839] = {
				{
					3.52900000000227, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [1]
				{
					7.15800000000309, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [2]
				{
					11.2289999999994, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [3]
				{
					14.4809999999998, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [4]
				{
					18.148000000001, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [5]
				{
					36.810000000005, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [6]
				{
					40.4130000000005, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [7]
				{
					44.0619999999981, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [8]
				{
					47.6900000000023, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [9]
				{
					51.3220000000001, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [10]
				{
					67.1189999999988, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [11]
				{
					70.747000000003, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [12]
				{
					74.3899999999994, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [13]
				{
					87.7430000000022, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [14]
				{
					91.387999999999, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [15]
				{
					95.0400000000009, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [16]
				{
					98.7000000000044, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [17]
				{
					102.349999999999, -- [1]
					"Admiral Ripsnarl", -- [2]
					88839, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [18]
			},
			[88836] = {
				{
					36.810000000005, -- [1]
					"Admiral Ripsnarl", -- [2]
					88836, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Thorraddin", -- [5]
				}, -- [1]
			},
		}, -- [158]
		{
			[89652] = {
				{
					5.53100000000268, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					28.1780000000072, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					38.2770000000019, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					57.2540000000008, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					64.9990000000034, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[88522] = {
				{
					43.5330000000031, -- [1]
					"Foe Reaper 5000", -- [2]
					88522, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88481] = {
				{
					8.94400000000314, -- [1]
					"Foe Reaper 5000", -- [2]
					88481, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					52.6150000000052, -- [1]
					"Foe Reaper 5000", -- [2]
					88481, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[88490] = {
				{
					5.12700000000041, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [1]
				{
					22.9220000000059, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [2]
				{
					25.3180000000066, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [3]
				{
					27.7630000000063, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [4]
				{
					39.8890000000029, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [5]
				{
					42.3180000000066, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [6]
				{
					47.1780000000072, -- [1]
					"Foe Reaper 5000", -- [2]
					88490, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [7]
			},
			[88495] = {
				{
					35.1900000000023, -- [1]
					"Foe Reaper 5000", -- [2]
					88495, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[88521] = {
				{
					39.8890000000029, -- [1]
					"Foe Reaper 5000", -- [2]
					88521, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[89757] = {
				{
					0.849000000001979, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					2.06100000000151, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					3.70000000000437, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					4.11100000000442, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					4.51300000000629, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					4.9120000000039, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					6.13400000000547, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					8.15400000000227, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					9.3640000000014, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					10.9880000000048, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					11.3910000000033, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					11.7860000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					12.2030000000013, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					13.4170000000013, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					15.4270000000033, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					16.6320000000051, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					18.260000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					18.6760000000068, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					19.070000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					19.4790000000066, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					20.695000000007, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					22.7120000000068, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					23.9210000000021, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					25.5310000000027, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					25.9310000000041, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					26.323000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					26.7380000000048, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					27.9660000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					29.984000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					31.1900000000023, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					32.8350000000064, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					33.2250000000058, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					33.6450000000041, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					34.0450000000055, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					35.3010000000068, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					37.2730000000011, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					38.4770000000062, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					40.0900000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					40.4850000000006, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					40.9010000000053, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					41.3080000000045, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					42.5090000000055, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					44.5480000000025, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					45.7560000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					47.3760000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					47.7790000000023, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					48.1790000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					48.5810000000056, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					49.8120000000054, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					51.8320000000022, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					53.0260000000053, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					54.6530000000057, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					55.0500000000029, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					55.4540000000052, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					55.8580000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					57.073000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					59.0780000000013, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					60.2790000000023, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					61.9140000000043, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
				{
					62.3150000000023, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [60]
				{
					62.7200000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [61]
				{
					63.1270000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [62]
				{
					64.3520000000062, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [63]
				{
					64.9990000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [64]
				{
					64.9990000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [65]
				{
					64.9990000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [66]
				{
					64.9990000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [67]
				{
					64.9990000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [68]
				{
					64.9990000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [69]
			},
		}, -- [159]
		{
			[89757] = {
				{
					1.34599999999773, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					4.19199999999546, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					4.58199999999488, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					4.99299999999494, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					4.99299999999494, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					5.40199999999459, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					6.61399999999412, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					8.63199999999779, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					11.4889999999941, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					11.8899999999994, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					12.2899999999936, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					12.2899999999936, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					12.6909999999989, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					13.9309999999969, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					15.9449999999997, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					18.7860000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					19.1869999999981, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					19.5869999999995, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					19.5869999999995, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					20, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					21.2089999999953, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					23.2249999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					26.0449999999983, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					26.4549999999945, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					26.8589999999967, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					26.8589999999967, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					27.278999999995, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					28.4869999999937, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					30.5149999999994, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					33.3569999999963, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					33.7469999999958, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					34.1709999999948, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					34.5639999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					34.5639999999985, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					35.7709999999934, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					37.7929999999979, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					40.6129999999976, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					41.0169999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					41.4079999999958, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					41.8099999999977, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					41.8199999999997, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					43.0380000000005, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					45.0589999999938, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					47.8709999999992, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					48.2809999999954, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					48.6719999999987, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					49.0859999999957, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					50.301999999996, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					50.301999999996, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					52.3149999999951, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					55.1459999999934, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					55.5360000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					55.9470000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					56.3589999999968, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					57.1589999999997, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					57.5599999999977, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					59.5759999999937, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					62.4099999999962, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					62.8069999999934, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
				{
					63.2089999999953, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [60]
				{
					63.6329999999944, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [61]
				{
					63.6329999999944, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [62]
				{
					64.851999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [63]
				{
					66.8499999999986, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [64]
				{
					69.6819999999934, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [65]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [66]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [67]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [68]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [69]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [70]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [71]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [72]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [73]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [74]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [75]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [76]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [77]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [78]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [79]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [80]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [81]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [82]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [83]
				{
					70, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [84]
			},
			[89652] = {
				{
					20.2019999999975, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					23.0219999999972, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					50.5020000000004, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					52.1049999999959, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					70, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					70, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			[88974] = {
				{
					35.5709999999963, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					39.2019999999975, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					42.4349999999977, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					56.9559999999983, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					61.005999999994, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					70, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					70, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					70, -- [1]
					"Sticky Bomb", -- [2]
					88974, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
			},
			[89769] = {
				{
					5.1339999999982, -- [1]
					"Mining Powder", -- [2]
					89769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					44.6139999999941, -- [1]
					"Mining Powder", -- [2]
					89769, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[90047] = {
				{
					3.99199999999837, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [1]
				{
					17.3659999999945, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [2]
				{
					30.726999999999, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [3]
				{
					45.2489999999962, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [4]
				{
					58.5789999999979, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [5]
				{
					70, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [6]
				{
					70, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [7]
			},
			[88300] = {
				{
					26.4149999999936, -- [1]
					"Lumbering Oaf", -- [2]
					88300, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[90096] = {
				{
					5.1339999999982, -- [1]
					"Explode", -- [2]
					90096, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					44.6139999999941, -- [1]
					"Explode", -- [2]
					90096, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[88288] = {
				{
					18.9959999999992, -- [1]
					"Lumbering Oaf", -- [2]
					88288, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Adelphee-MoonGuard", -- [5]
				}, -- [1]
			},
		}, -- [160]
		{
			[89652] = {
				{
					13.9960000000065, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					19.2540000000008, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					39.4790000000066, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					52.0040000000008, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					67.3770000000004, -- [1]
					"Ogre Henchman", -- [2]
					89652, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
			},
			[89757] = {
				{
					0.452000000004773, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					0.854000000006636, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					1.66500000000087, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					4.07900000000518, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					7.71800000000076, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					7.71800000000076, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					8.12299999999959, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					8.93600000000151, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					8.93600000000151, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					10.1419999999998, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					11.3500000000058, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					15.0070000000051, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					15.0070000000051, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
				{
					15.4100000000035, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [14]
				{
					16.2130000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [15]
				{
					16.2130000000034, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [16]
				{
					17.4300000000003, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [17]
				{
					18.6410000000033, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [18]
				{
					22.2830000000031, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [19]
				{
					22.2830000000031, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [20]
				{
					22.6779999999999, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [21]
				{
					23.4860000000044, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [22]
				{
					23.4860000000044, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [23]
				{
					24.7010000000009, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [24]
				{
					25.9120000000039, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [25]
				{
					29.5720000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [26]
				{
					29.5720000000001, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [27]
				{
					29.9750000000058, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [28]
				{
					30.7800000000061, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [29]
				{
					30.7800000000061, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [30]
				{
					32.0060000000012, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [31]
				{
					33.2160000000004, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [32]
				{
					36.8420000000042, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [33]
				{
					36.8420000000042, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [34]
				{
					37.2400000000052, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [35]
				{
					38.0560000000041, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [36]
				{
					38.0560000000041, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [37]
				{
					39.2680000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [38]
				{
					40.4920000000057, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [39]
				{
					44.1210000000065, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [40]
				{
					44.1210000000065, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [41]
				{
					44.5250000000015, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [42]
				{
					45.3510000000024, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [43]
				{
					45.3510000000024, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [44]
				{
					46.5540000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [45]
				{
					47.7680000000037, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [46]
				{
					51.3970000000045, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [47]
				{
					51.3970000000045, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [48]
				{
					51.8140000000058, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [49]
				{
					52.6080000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [50]
				{
					52.6080000000002, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [51]
				{
					53.8290000000052, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [52]
				{
					55.0400000000009, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [53]
				{
					58.6630000000005, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [54]
				{
					58.6630000000005, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [55]
				{
					59.0740000000005, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [56]
				{
					59.8840000000055, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [57]
				{
					59.8840000000055, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [58]
				{
					61.0850000000064, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [59]
				{
					62.3060000000041, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [60]
				{
					65.9520000000048, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [61]
				{
					65.9619999999995, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [62]
				{
					66.3530000000028, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [63]
				{
					66.7590000000055, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [64]
				{
					67.1650000000009, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [65]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [66]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [67]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [68]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [69]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [70]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [71]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [72]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [73]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [74]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [75]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [76]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [77]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [78]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [79]
				{
					68.0010000000038, -- [1]
					"Defias Cannon", -- [2]
					89757, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [80]
			},
			[88169] = {
				{
					46.7640000000029, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					49.586000000003, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					49.9890000000014, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					51.2070000000022, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					51.6110000000044, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					52.4150000000009, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					52.4150000000009, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					53.2360000000044, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					53.6290000000008, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					53.6290000000008, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					54.8380000000034, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
				{
					54.8380000000034, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [12]
				{
					60.0850000000064, -- [1]
					"Glubtok", -- [2]
					88169, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [13]
			},
			[90047] = {
				{
					8.72300000000541, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [1]
				{
					22.083000000006, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [2]
				{
					36.6410000000033, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [3]
				{
					52.4150000000009, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [4]
				{
					64.5400000000009, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [5]
				{
					68.0010000000038, -- [1]
					"Defias Envoker", -- [2]
					90047, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Defias Miner", -- [5]
				}, -- [6]
			},
			[88009] = {
				{
					43.1170000000057, -- [1]
					"Glubtok", -- [2]
					88009, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[87859] = {
				{
					7.50200000000041, -- [1]
					"Glubtok", -- [2]
					87859, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.211000000003, -- [1]
					"Glubtok", -- [2]
					87859, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			[88129] = {
				{
					46.3540000000066, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					47.1640000000043, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					48.3710000000065, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					48.3710000000065, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					49.586000000003, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					49.586000000003, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					50.8050000000003, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					50.8050000000003, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					51.6110000000044, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					59.6830000000045, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
				{
					60.4880000000048, -- [1]
					"Glubtok", -- [2]
					88129, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [11]
			},
			[87861] = {
				{
					20.8640000000014, -- [1]
					"Glubtok", -- [2]
					87861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [161]
	},
	["useicons"] = true,
	["backdrop_color"] = {
		0, -- [1]
		0, -- [2]
		0, -- [3]
		0.4, -- [4]
	},
	["window_scale"] = 1,
	["deaths_data"] = {
		{
			["Ssalah-MoonGuard"] = {
				{
					["time"] = 31.29499999999825,
					["events"] = {
						{
							true, -- [1]
							258834, -- [2]
							47118, -- [3]
							1574215527.623, -- [4]
							41337, -- [5]
							"Edge of Annihilation", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							258834, -- [2]
							47118, -- [3]
							1574215529.643, -- [4]
							34057, -- [5]
							"Edge of Annihilation", -- [6]
							39838, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							258834, -- [2]
							47118, -- [3]
							1574215531.653, -- [4]
							1, -- [5]
							"Edge of Annihilation", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							11417, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Kotipëlto-Azralon"] = {
				{
					["time"] = 49.51699999999983,
					["events"] = {
						{
							true, -- [1]
							257911, -- [2]
							2272, -- [3]
							1574215548.895, -- [4]
							14765, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257930, -- [2]
							17670, -- [3]
							1574215549.366, -- [4]
							19688, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257911, -- [2]
							2524, -- [3]
							1574215549.394, -- [4]
							17164, -- [5]
							"[*] Unleashed Rage", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
			["Helpplzboy-Darrowmere"] = {
				{
					["time"] = 22.28199999999924,
					["events"] = {
						{
							true, -- [1]
							258837, -- [2]
							14681, -- [3]
							1574215517.601, -- [4]
							57500, -- [5]
							"[*] Rent Soul", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							258834, -- [2]
							45471, -- [3]
							1574215520.616, -- [4]
							12029, -- [5]
							"Edge of Annihilation", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							258834, -- [2]
							44369, -- [3]
							1574215522.622, -- [4]
							1, -- [5]
							"Edge of Annihilation", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							29722, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
		}, -- [1]
		{
			["Zaruuk-Azralon"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Thorraddin"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Enterverse-Tichondrius"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Ssalah-MoonGuard"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Darkneesh-Azralon"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Hadess-Azralon"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Torchx-Tichondrius"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Runicfury-Tichondrius"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Mikoriko-EmeraldDream"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Laegherie-Ragnaros"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Piston-Area52"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Kotipëlto-Azralon"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Aedeath-Thrall"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Rðcha-Azralon"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Munnk-BleedingHollow"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
			["Helpplzboy-Darrowmere"] = {
				{
					["time"] = 40.62399999999616,
					["events"] = {
						{
							true, -- [1]
							257930, -- [2]
							8698, -- [3]
							1574215419.533, -- [4]
							12329, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							257930, -- [2]
							9941, -- [3]
							1574215420.041, -- [4]
							2388, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							257930, -- [2]
							9941, -- [3]
							1574215420.545, -- [4]
							1, -- [5]
							"[*] Crushing Fear", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							7553, -- [10]
						}, -- [3]
					},
				}, -- [1]
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [2]
			},
			["Nohcolo-BleedingHollow"] = {
				{
					["time"] = 93.73799999999756,
					["events"] = {
					},
				}, -- [1]
			},
		}, -- [2]
		{
			["Helpplzboy-Darrowmere"] = {
				{
					["time"] = 90.62099999999919,
					["events"] = {
						{
							true, -- [1]
							245632, -- [2]
							2824, -- [3]
							1574215274.279, -- [4]
							5374, -- [5]
							"Flame of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							244912, -- [2]
							3326, -- [3]
							1574215275.205, -- [4]
							2048, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							244912, -- [2]
							3122, -- [3]
							1574215276.224, -- [4]
							1, -- [5]
							"Ember of Taeshalach", -- [6]
							nil, -- [7]
							4, -- [8]
							false, -- [9]
							1074, -- [10]
						}, -- [3]
					},
				}, -- [1]
			},
		}, -- [3]
		{
		}, -- [4]
	},
	["debuff_timeline"] = {
		{
			["Zaruuk-Azralon"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Thorraddin"] = {
				[258834] = {
					23.26499999999942, -- [1]
					35.29200000000128, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Enterverse-Tichondrius"] = {
				[256899] = {
					0.9760000000023865, -- [1]
					8.866000000001804, -- [2]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = false,
				},
				[258838] = {
					15.73500000000058, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Ssalah-MoonGuard"] = {
				[258838] = {
					15.73500000000058, -- [1]
					31.29499999999825, -- [2]
					47.29499999999825, -- [3]
					171.9850000000006, -- [4]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					23.26499999999942, -- [1]
					31.29499999999825, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Darkneesh-Azralon"] = {
				[258834] = {
					23.26499999999942, -- [1]
					35.29200000000128, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[258838] = {
					24.2410000000018, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Hadess-Azralon"] = {
				[256899] = {
					0.9760000000023865, -- [1]
					8.885999999998603, -- [2]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = false,
				},
				[258838] = {
					33.93800000000192, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Nohcolo-BleedingHollow"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					45.02000000000408, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Runicfury-Tichondrius"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					43.51800000000367, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Laegherie-Ragnaros"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257931] = {
					26.25600000000122, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
				[257930] = {
					34.49500000000262, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Crushing Fear",
					["active"] = false,
				},
				[257911] = {
					27.02900000000227, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Piston-Area52"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					23.26499999999942, -- [1]
					35.26600000000326, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[257911] = {
					27.02900000000227, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Pandaabutt-Area52"] = {
				[294852] = {
					171.9850000000006, -- [1]
					["stacks"] = {
					},
					["source"] = "Voice in the Deeps",
					["active"] = true,
				},
			},
			["Kotipëlto-Azralon"] = {
				[258834] = {
					18.26800000000367, -- [1]
					30.2609999999986, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[257911] = {
					27.02900000000227, -- [1]
					49.51699999999983, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[256899] = {
					0.9760000000023865, -- [1]
					8.876000000003842, -- [2]
					["stacks"] = {
					},
					["source"] = "Hungering Soul",
					["active"] = false,
				},
				[258838] = {
					15.73500000000058, -- [1]
					49.51699999999983, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257930] = {
					28.50300000000425, -- [1]
					49.51699999999983, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Crushing Fear",
					["active"] = false,
				},
				[257931] = {
					26.25600000000122, -- [1]
					49.51699999999983, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
			},
			["Aedeath-Thrall"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					23.26499999999942, -- [1]
					35.26600000000326, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
			},
			["Rðcha-Azralon"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					18.26800000000367, -- [1]
					30.2609999999986, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[257869] = {
					26.25600000000122, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Rage",
					["active"] = false,
				},
			},
			["Ràijin-Area52"] = {
				[1604] = {
					171.9850000000006, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Harbor Saurid",
					["active"] = false,
				},
				[24331] = {
					171.9850000000006, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Harbor Saurid",
					["active"] = false,
				},
			},
			["Munnk-BleedingHollow"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					38.26400000000285, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Creamfury-Area52"] = {
				[279956] = {
					171.9850000000006, -- [1]
					["stacks"] = {
					},
					["source"] = "Yisthat-BurningLegion",
					["active"] = true,
				},
			},
			["Torchx-Tichondrius"] = {
				[258838] = {
					7.190999999998894, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					23.26499999999942, -- [1]
					35.29200000000128, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[257911] = {
					45.02000000000408, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Goshocks-Area52"] = {
				[24331] = {
					171.9850000000006, -- [1]
					171.9850000000006, -- [2]
					171.9850000000006, -- [3]
					171.9850000000006, -- [4]
					171.9850000000006, -- [5]
					171.9850000000006, -- [6]
					171.9850000000006, -- [7]
					171.9850000000006, -- [8]
					171.9850000000006, -- [9]
					171.9850000000006, -- [10]
					171.9850000000006, -- [11]
					171.9850000000006, -- [12]
					["stacks"] = {
					},
					["source"] = "Harbor Saurid",
					["active"] = false,
				},
			},
			["Helpplzboy-Darrowmere"] = {
				[258838] = {
					47.29499999999825, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[258834] = {
					18.26800000000367, -- [1]
					22.26400000000285, -- [2]
					["stacks"] = {
					},
					["source"] = "Edge of Annihilation",
					["active"] = false,
				},
				[257911] = {
					48.78399999999965, -- [1]
					171.9850000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
		}, -- [1]
		{
			["Zaruuk-Azralon"] = {
				[258646] = {
					14.27300000000105, -- [1]
					19.29200000000128, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Gift of the Sky",
					["active"] = false,
				},
			},
			["Enterverse-Tichondrius"] = {
				[257911] = {
					12.875, -- [1]
					43.63900000000285, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
				[253901] = {
					22.29899999999907, -- [1]
					52.30299999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sea",
					["active"] = false,
				},
			},
			["Aedeath-Thrall"] = {
				[253901] = {
					24.10100000000239, -- [1]
					54.11099999999715, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Strength of the Sea",
					["active"] = false,
				},
			},
			["Ssalah-MoonGuard"] = {
				[248499] = {
					4.92500000000291, -- [1]
					19.93600000000151, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[257911] = {
					33.87399999999616, -- [1]
					63.875, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Hadess-Azralon"] = {
				[257869] = {
					9.856999999996333, -- [1]
					44.11800000000221, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Rage",
					["active"] = false,
				},
				[248499] = {
					17.05000000000291, -- [1]
					32.05299999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
			},
			["Helpplzboy-Darrowmere"] = {
				[257931] = {
					9.839999999996508, -- [1]
					40.62399999999616, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Sargeras' Fear",
					["active"] = false,
				},
				[257930] = {
					12.10399999999936, -- [1]
					40.62399999999616, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Crushing Fear",
					["active"] = false,
				},
			},
			["Runicfury-Tichondrius"] = {
				[248499] = {
					4.92500000000291, -- [1]
					32.05299999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "Argus the Unmaker",
					["active"] = false,
				},
				[255199] = {
					49.09399999999732, -- [1]
					67.48999999999796, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Avatar of Aggramar",
					["active"] = false,
				},
			},
			["Laegherie-Ragnaros"] = {
				[257911] = {
					39.87299999999959, -- [1]
					71.375, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Kotipëlto-Azralon"] = {
				[257911] = {
					33.87399999999616, -- [1]
					64.61800000000221, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
			["Torchx-Tichondrius"] = {
				[258647] = {
					14.27300000000105, -- [1]
					19.27599999999802, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Gift of the Sea",
					["active"] = false,
				},
				[257911] = {
					33.87399999999616, -- [1]
					63.875, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Unleashed Rage",
					["active"] = false,
				},
			},
		}, -- [2]
		{
			["Zaruuk-Azralon"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					88.34599999999773, -- [1]
					89.29599999999482, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[254452] = {
					3.658999999999651, -- [1]
					11.66500000000087, -- [2]
					63.16500000000087, -- [3]
					71.18999999999505, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Thorraddin"] = {
				[245916] = {
					27.95999999999913, -- [1]
					30.43599999999424, -- [2]
					85.98999999999796, -- [3]
					87.16799999999785, -- [4]
					98.82600000000093, -- [5]
					99.76900000000023, -- [6]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Enterverse-Tichondrius"] = {
				[245916] = {
					28.14600000000064, -- [1]
					29.60899999999674, -- [2]
					30.57600000000093, -- [3]
					32.05199999999604, -- [4]
					56.29499999999825, -- [5]
					56.46099999999569, -- [6]
					56.82299999999668, -- [7]
					58.07899999999791, -- [8]
					58.11200000000099, -- [9]
					59.16599999999744, -- [10]
					64.08599999999569, -- [11]
					65.58499999999913, -- [12]
					77.79699999999866, -- [13]
					79.22699999999895, -- [14]
					79.6449999999968, -- [15]
					80.88999999999942, -- [16]
					86.98700000000099, -- [17]
					89.14999999999418, -- [18]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Ssalah-MoonGuard"] = {
				[254452] = {
					3.658999999999651, -- [1]
					11.66500000000087, -- [2]
					63.16500000000087, -- [3]
					71.18999999999505, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					56.74499999999534, -- [1]
					59.67799999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Darkneesh-Azralon"] = {
				[254452] = {
					3.658999999999651, -- [1]
					11.66500000000087, -- [2]
					63.16500000000087, -- [3]
					71.17599999999948, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Hadess-Azralon"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					61.41599999999744, -- [1]
					62.21699999999692, -- [2]
					62.22999999999593, -- [3]
					63.09799999999814, -- [4]
					63.10899999999674, -- [5]
					64.9539999999979, -- [6]
					64.96399999999994, -- [7]
					65.59799999999814, -- [8]
					65.59799999999814, -- [9]
					66.25200000000041, -- [10]
					66.77999999999884, -- [11]
					67.55599999999686, -- [12]
					67.56699999999546, -- [13]
					68.778999999995, -- [14]
					68.79599999999482, -- [15]
					69.74599999999919, -- [16]
					69.75799999999435, -- [17]
					70.03600000000006, -- [18]
					70.05099999999948, -- [19]
					71.09999999999854, -- [20]
					86.0679999999993, -- [21]
					87.42899999999645, -- [22]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					83.48599999999715, -- [1]
					95.50599999999395, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[254452] = {
					3.658999999999651, -- [1]
					11.66500000000087, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Nohcolo-BleedingHollow"] = {
				[244291] = {
					16.21899999999732, -- [1]
					46.24499999999534, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					38.59799999999814, -- [1]
					40.05500000000029, -- [2]
					60.91799999999785, -- [3]
					63.67599999999948, -- [4]
					63.69099999999889, -- [5]
					64.03199999999924, -- [6]
					64.0570000000007, -- [7]
					65.07499999999709, -- [8]
					88.90099999999802, -- [9]
					90.29099999999744, -- [10]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Runicfury-Tichondrius"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					40.14699999999721, -- [1]
					47.7019999999975, -- [2]
					63.09799999999814, -- [3]
					68.38799999999901, -- [4]
					68.41300000000047, -- [5]
					69.98599999999715, -- [6]
					74.26399999999558, -- [7]
					75.4340000000011, -- [8]
					80.80199999999604, -- [9]
					81.916999999994, -- [10]
					83.16899999999441, -- [11]
					85.5199999999968, -- [12]
					98.416999999994, -- [13]
					99.8469999999943, -- [14]
					99.96399999999994, -- [15]
					100.8689999999988, -- [16]
					106.2799999999988, -- [17]
					107.3859999999986, -- [18]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[245990] = {
					70.0089999999982, -- [1]
					82.01900000000023, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244736] = {
					87.80299999999988, -- [1]
					99.81099999999424, -- [2]
					105.1800000000003, -- [3]
					107.3859999999986, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Mikoriko-EmeraldDream"] = {
				[245916] = {
					29.95100000000093, -- [1]
					30.43599999999424, -- [2]
					38.92399999999907, -- [3]
					41.07499999999709, -- [4]
					56.7809999999954, -- [5]
					58.69799999999668, -- [6]
					60.45799999999872, -- [7]
					61.17899999999645, -- [8]
					61.17899999999645, -- [9]
					62.09199999999692, -- [10]
					62.10299999999552, -- [11]
					62.70100000000093, -- [12]
					62.75, -- [13]
					64.21399999999994, -- [14]
					66.3859999999986, -- [15]
					66.87900000000081, -- [16]
					67.40799999999581, -- [17]
					67.86000000000058, -- [18]
					77.68699999999808, -- [19]
					78.67599999999948, -- [20]
					78.99599999999919, -- [21]
					79.0969999999943, -- [22]
					89.81099999999424, -- [23]
					90.85399999999936, -- [24]
					103.6329999999944, -- [25]
					103.8949999999968, -- [26]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Laegherie-Ragnaros"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					29.73700000000099, -- [1]
					30.78600000000006, -- [2]
					61.17899999999645, -- [3]
					62.04000000000087, -- [4]
					62.04000000000087, -- [5]
					63.16500000000087, -- [6]
					77.73899999999412, -- [7]
					79.0969999999943, -- [8]
					81.84599999999773, -- [9]
					83.6359999999986, -- [10]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					13.81499999999505, -- [1]
					25.82099999999628, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Piston-Area52"] = {
				[245916] = {
					56.54099999999744, -- [1]
					57.92799999999988, -- [2]
					57.92799999999988, -- [3]
					58.58399999999529, -- [4]
					58.58399999999529, -- [5]
					59.44700000000012, -- [6]
					61.5089999999982, -- [7]
					63.36599999999453, -- [8]
					64.03199999999924, -- [9]
					64.78499999999622, -- [10]
					64.84999999999854, -- [11]
					65.03899999999703, -- [12]
					85.97699999999895, -- [13]
					88.24899999999616, -- [14]
					89.98899999999412, -- [15]
					90.20899999999529, -- [16]
					91.86000000000058, -- [17]
					93.16100000000006, -- [18]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.3099999999977, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Kotipëlto-Azralon"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					99.41599999999744, -- [1]
					100.5760000000009, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					105.5009999999966, -- [1]
					107.3859999999986, -- [2]
					["stacks"] = {
					},
					["source"] = "Wake of Flame",
					["active"] = false,
				},
				[254452] = {
					3.658999999999651, -- [1]
					11.66500000000087, -- [2]
					63.16500000000087, -- [3]
					71.17599999999948, -- [4]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Aedeath-Thrall"] = {
				[245916] = {
					27.39199999999983, -- [1]
					31.07600000000093, -- [2]
					54.94900000000052, -- [3]
					56.30900000000111, -- [4]
					61.57899999999791, -- [5]
					62.19399999999587, -- [6]
					77.19799999999668, -- [7]
					77.80999999999767, -- [8]
					79.73399999999674, -- [9]
					80.60699999999633, -- [10]
					85.03899999999703, -- [11]
					85.4539999999979, -- [12]
					91.07099999999627, -- [13]
					91.79899999999907, -- [14]
					92.78399999999965, -- [15]
					92.9429999999993, -- [16]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.3099999999977, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Rðcha-Azralon"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					61.05500000000029, -- [1]
					62.39299999999639, -- [2]
					62.39299999999639, -- [3]
					63.16500000000087, -- [4]
					80.028999999995, -- [5]
					81.20899999999529, -- [6]
					98.11299999999756, -- [7]
					100.7350000000006, -- [8]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					90.74899999999616, -- [1]
					102.75, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244291] = {
					16.23599999999715, -- [1]
					46.24499999999534, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Munnk-BleedingHollow"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					30.62900000000082, -- [1]
					31.99799999999959, -- [2]
					33.63099999999395, -- [3]
					34.22799999999552, -- [4]
					38.36799999999494, -- [5]
					39.49599999999919, -- [6]
					39.71899999999732, -- [7]
					41.16500000000087, -- [8]
					41.95899999999529, -- [9]
					43.2589999999982, -- [10]
					61.36699999999837, -- [11]
					64.278999999995, -- [12]
					64.291999999994, -- [13]
					65.02699999999459, -- [14]
					78.35199999999895, -- [15]
					78.99599999999919, -- [16]
					82.01900000000023, -- [17]
					82.77599999999802, -- [18]
					86.43499999999767, -- [19]
					89.81099999999424, -- [20]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[254452] = {
					63.16500000000087, -- [1]
					71.17599999999948, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Despa-Dragonmaw"] = {
				[244736] = {
					105.3099999999977, -- [1]
					107.3859999999986, -- [2]
					["stacks"] = {
					},
					["source"] = "Wake of Flame",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[244291] = {
					16.21899999999732, -- [1]
					46.24499999999534, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					38.3070000000007, -- [1]
					40.14699999999721, -- [2]
					40.24299999999494, -- [3]
					40.98099999999977, -- [4]
					58.15699999999924, -- [5]
					58.28199999999924, -- [6]
					77.72599999999511, -- [7]
					77.80999999999767, -- [8]
					79.6449999999968, -- [9]
					80.62900000000081, -- [10]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[245990] = {
					2.118999999998778, -- [1]
					25.27599999999802, -- [2]
					41.43400000000111, -- [3]
					80.52100000000064, -- [4]
					91.73999999999796, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
			["Helpplzboy-Darrowmere"] = {
				[244291] = {
					16.23599999999715, -- [1]
					46.24499999999534, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[245916] = {
					30.42399999999907, -- [1]
					30.43599999999424, -- [2]
					36.66699999999401, -- [3]
					38.50799999999435, -- [4]
					60.91799999999785, -- [5]
					63.00400000000082, -- [6]
					63.36599999999453, -- [7]
					64.25400000000081, -- [8]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[245990] = {
					0.1, -- [1]
					12.02799999999843, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					90.59799999999814, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
			},
			["Torchx-Tichondrius"] = {
				[244912] = {
					56.82299999999668, -- [1]
					71.82600000000093, -- [2]
					80.5679999999993, -- [3]
					95.59300000000076, -- [4]
					105.2730000000011, -- [5]
					107.3859999999986, -- [6]
					["stacks"] = {
					},
					["source"] = "Ember of Taeshalach",
					["active"] = false,
				},
				[245916] = {
					29.95100000000093, -- [1]
					30.43599999999424, -- [2]
					37.88999999999942, -- [3]
					39.04899999999907, -- [4]
					63.13299999999435, -- [5]
					63.84199999999692, -- [6]
					63.84199999999692, -- [7]
					65.18499999999767, -- [8]
					65.20899999999529, -- [9]
					65.66399999999703, -- [10]
					78.80500000000029, -- [11]
					79.0969999999943, -- [12]
					80.4719999999943, -- [13]
					80.99299999999494, -- [14]
					81.30399999999645, -- [15]
					82.44900000000052, -- [16]
					93.75699999999779, -- [17]
					93.84300000000076, -- [18]
					93.94199999999546, -- [19]
					94.14600000000064, -- [20]
					["stacks"] = {
					},
					["source"] = "[*] Molten Remnants",
					["active"] = false,
				},
				[244736] = {
					77.91799999999785, -- [1]
					89.91500000000087, -- [2]
					["stacks"] = {
					},
					["source"] = "Aggramar",
					["active"] = false,
				},
			},
		}, -- [3]
		{
			["Zaruuk-Azralon"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					34.10900000000402, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Thorraddin"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.80600000000413, -- [1]
					33.83200000000215, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Enterverse-Tichondrius"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					45.4320000000007, -- [2]
					45.55299999999988, -- [3]
					47.80900000000111, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Despa-Dragonmaw"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[244899] = {
					10.73300000000018, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					35.33000000000175, -- [2]
					47.31500000000233, -- [3]
					47.80900000000111, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Darkneesh-Azralon"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					32.77700000000186, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253520] = {
					20.4210000000021, -- [1]
					30.42799999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
			},
			["Hadess-Azralon"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					32.7410000000018, -- [1]
					33.83200000000215, -- [2]
					34.12200000000303, -- [3]
					34.58899999999994, -- [4]
					35.0940000000046, -- [5]
					47.80900000000111, -- [6]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253520] = {
					20.4210000000021, -- [1]
					30.42799999999988, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
			},
			["Nohcolo-BleedingHollow"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Runicfury-Tichondrius"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Mikoriko-EmeraldDream"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Laegherie-Ragnaros"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Piston-Area52"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.80600000000413, -- [1]
					37.71600000000035, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Kotipëlto-Azralon"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
			},
			["Aedeath-Thrall"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.77700000000186, -- [1]
					32.98500000000058, -- [2]
					34.45599999999831, -- [3]
					36.62700000000041, -- [4]
					47.05500000000029, -- [5]
					47.80900000000111, -- [6]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Rðcha-Azralon"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[253020] = {
					32.77700000000186, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253520] = {
					20.4210000000021, -- [1]
					30.41100000000006, -- [2]
					["stacks"] = {
					},
					["source"] = "Noura, Mother of Flames",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
			},
			["Munnk-BleedingHollow"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.75700000000506, -- [1]
					39.06500000000233, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Ssalah-MoonGuard"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
			},
			["Torchx-Tichondrius"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.80600000000413, -- [1]
					47.80900000000111, -- [2]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
			["Helpplzboy-Darrowmere"] = {
				[253753] = {
					14.16500000000087, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[258018] = {
					4.186000000001513, -- [1]
					60.60300000000279, -- [2]
					["stacks"] = {
					},
					["source"] = "Diima, Mother of Gloom",
					["active"] = false,
				},
				[254239] = {
					60.60300000000279, -- [1]
					["stacks"] = {
					},
					["source"] = "[*] Boon of the Titans",
					["active"] = true,
				},
				[253020] = {
					32.8640000000014, -- [1]
					34.15899999999965, -- [2]
					46.50600000000122, -- [3]
					47.80900000000111, -- [4]
					["stacks"] = {
					},
					["source"] = "[*] Storm of Darkness",
					["active"] = false,
				},
			},
		}, -- [4]
	},
	["cooldowns_timeline"] = {
		{
			["Runicfury-Tichondrius"] = {
				{
					11.9120000000039, -- [1]
					"Runicfury-Tichondrius", -- [2]
					55233, -- [3]
				}, -- [1]
			},
			["Munnk-BleedingHollow"] = {
				{
					30.44099999999889, -- [1]
					"Argus the Unmaker", -- [2]
					122470, -- [3]
				}, -- [1]
			},
			["Kotipëlto-Azralon"] = {
				{
					33.70700000000215, -- [1]
					"[*] raid wide cooldown", -- [2]
					265202, -- [3]
				}, -- [1]
				{
					33.72200000000157, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [2]
				{
					35.37600000000384, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [3]
				{
					37.02000000000408, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [4]
				{
					38.66300000000047, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [5]
				{
					40.31100000000151, -- [1]
					"[*] raid wide cooldown", -- [2]
					64844, -- [3]
				}, -- [6]
			},
			["Aedeath-Thrall"] = {
				{
					30.42799999999988, -- [1]
					"Aedeath-Thrall", -- [2]
					48792, -- [3]
				}, -- [1]
			},
			["Rðcha-Azralon"] = {
				{
					23.94200000000274, -- [1]
					"Rðcha-Azralon", -- [2]
					184662, -- [3]
				}, -- [1]
			},
		}, -- [1]
		{
			["Runicfury-Tichondrius"] = {
				{
					11.65699999999924, -- [1]
					"Runicfury-Tichondrius", -- [2]
					55233, -- [3]
				}, -- [1]
				{
					38.59999999999855, -- [1]
					"Runicfury-Tichondrius", -- [2]
					48792, -- [3]
				}, -- [2]
			},
			["Thorraddin"] = {
				{
					40.83200000000215, -- [1]
					"Thorraddin", -- [2]
					642, -- [3]
				}, -- [1]
			},
			["Rðcha-Azralon"] = {
				{
					65.89299999999639, -- [1]
					"Rðcha-Azralon", -- [2]
					642, -- [3]
				}, -- [1]
			},
			["Helpplzboy-Darrowmere"] = {
				{
					19.59100000000035, -- [1]
					"Helpplzboy-Darrowmere", -- [2]
					48792, -- [3]
				}, -- [1]
				{
					35.93800000000192, -- [1]
					"Helpplzboy-Darrowmere", -- [2]
					48743, -- [3]
				}, -- [2]
			},
		}, -- [2]
		{
			["Piston-Area52"] = {
				{
					32.42899999999645, -- [1]
					"Piston-Area52", -- [2]
					184364, -- [3]
				}, -- [1]
			},
			["Laegherie-Ragnaros"] = {
				{
					72.74499999999534, -- [1]
					"Laegherie-Ragnaros", -- [2]
					184662, -- [3]
				}, -- [1]
			},
			["Despa-Dragonmaw"] = {
				{
					46.09599999999773, -- [1]
					"[*] raid wide cooldown", -- [2]
					97462, -- [3]
				}, -- [1]
				{
					83.01499999999942, -- [1]
					"Despa-Dragonmaw", -- [2]
					12975, -- [3]
				}, -- [2]
			},
			["Rðcha-Azralon"] = {
				{
					70.75599999999395, -- [1]
					"Rðcha-Azralon", -- [2]
					184662, -- [3]
				}, -- [1]
			},
			["Hadess-Azralon"] = {
				{
					5.791999999994005, -- [1]
					"Hadess-Azralon", -- [2]
					5277, -- [3]
				}, -- [1]
			},
			["Helpplzboy-Darrowmere"] = {
				{
					3.532999999995809, -- [1]
					"Helpplzboy-Darrowmere", -- [2]
					48743, -- [3]
				}, -- [1]
				{
					11.11399999999412, -- [1]
					"Helpplzboy-Darrowmere", -- [2]
					48792, -- [3]
				}, -- [2]
			},
		}, -- [3]
		{
			["Runicfury-Tichondrius"] = {
				{
					27.23800000000483, -- [1]
					"Runicfury-Tichondrius", -- [2]
					55233, -- [3]
				}, -- [1]
			},
			["Munnk-BleedingHollow"] = {
				{
					36.15699999999924, -- [1]
					"Noura, Mother of Flames", -- [2]
					122470, -- [3]
				}, -- [1]
			},
			["Rðcha-Azralon"] = {
				{
					33.77200000000448, -- [1]
					"Rðcha-Azralon", -- [2]
					184662, -- [3]
				}, -- [1]
			},
			["Helpplzboy-Darrowmere"] = {
				{
					42.12600000000384, -- [1]
					"Helpplzboy-Darrowmere", -- [2]
					48792, -- [3]
				}, -- [1]
			},
		}, -- [4]
	},
	["BossSpellCast"] = {
		{
			["Apocalypsis Module"] = {
				{
					34.65499999999884, -- [1]
					"Apocalypsis Module", -- [2]
					258029, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			["Harbor Saurid"] = {
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [1]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [2]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [3]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [4]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [5]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Goshocks-Area52", -- [5]
				}, -- [6]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Primal Earth Elemental", -- [5]
				}, -- [7]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Primal Earth Elemental", -- [5]
				}, -- [8]
				{
					171.9850000000006, -- [1]
					"Harbor Saurid", -- [2]
					24331, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [9]
			},
			["Argus the Unmaker"] = {
				{
					7.17500000000291, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Aedeath-Thrall", -- [5]
				}, -- [1]
				{
					15.71500000000378, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [2]
				{
					24.22800000000279, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [3]
				{
					33.93800000000192, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [4]
				{
					46.25200000000041, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					47.29499999999825, -- [1]
					"Argus the Unmaker", -- [2]
					258838, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [6]
			},
			["Gurubashi Thug"] = {
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [1]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [2]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [3]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [4]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [5]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [6]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [7]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [8]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [9]
				{
					171.9850000000006, -- [1]
					"Gurubashi Thug", -- [2]
					265754, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Ràijin-Area52", -- [5]
				}, -- [10]
			},
		}, -- [1]
		{
			["Argus the Unmaker"] = {
				{
					4.913000000000466, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [1]
				{
					10.98799999999756, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [2]
				{
					14.70300000000134, -- [1]
					"Argus the Unmaker", -- [2]
					257296, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					17.05000000000291, -- [1]
					"Argus the Unmaker", -- [2]
					248499, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Runicfury-Tichondrius", -- [5]
				}, -- [4]
				{
					90.5199999999968, -- [1]
					"Argus the Unmaker", -- [2]
					256542, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					93.53899999999703, -- [1]
					"Argus the Unmaker", -- [2]
					258399, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
		}, -- [2]
		{
			["Unknown"] = {
				{
					23.00499999999738, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					23.00499999999738, -- [1]
					"Unknown", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
			["Aggramar"] = {
				{
					11.73599999999715, -- [1]
					"Aggramar", -- [2]
					244693, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Laegherie-Ragnaros", -- [5]
				}, -- [1]
				{
					16.21899999999732, -- [1]
					"Aggramar", -- [2]
					245458, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					41.37700000000041, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					91.653999999995, -- [1]
					"Aggramar", -- [2]
					243431, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
			},
			["Flame of Taeshalach"] = {
				{
					25.01299999999901, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					25.01299999999901, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					77.63199999999779, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					77.63199999999779, -- [1]
					"Flame of Taeshalach", -- [2]
					244901, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					79.6449999999968, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					79.6449999999968, -- [1]
					"Flame of Taeshalach", -- [2]
					245631, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
			},
			["Ember of Taeshalach"] = {
				{
					30.42399999999907, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					34.59300000000076, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					56.80599999999686, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					77.78199999999924, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					77.78199999999924, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					77.78199999999924, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					77.79699999999866, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					80.55399999999645, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					84.22299999999814, -- [1]
					"Ember of Taeshalach", -- [2]
					244903, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					105.2730000000011, -- [1]
					"Ember of Taeshalach", -- [2]
					244912, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
			},
		}, -- [3]
		{
			["Noura, Mother of Flames"] = {
				{
					9.507000000005064, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.73300000000018, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [2]
				{
					20.40600000000268, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Rðcha-Azralon", -- [5]
				}, -- [3]
				{
					20.40600000000268, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Darkneesh-Azralon", -- [5]
				}, -- [4]
				{
					20.40600000000268, -- [1]
					"Noura, Mother of Flames", -- [2]
					253520, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Hadess-Azralon", -- [5]
				}, -- [5]
				{
					22.12000000000262, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [6]
				{
					33.02799999999843, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [7]
				{
					43.96700000000419, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [8]
				{
					47.58400000000256, -- [1]
					"Noura, Mother of Flames", -- [2]
					245627, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					54.88900000000285, -- [1]
					"Noura, Mother of Flames", -- [2]
					244899, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Despa-Dragonmaw", -- [5]
				}, -- [10]
			},
			["Diima, Mother of Gloom"] = {
				{
					4.145000000004075, -- [1]
					"Diima, Mother of Gloom", -- [2]
					250333, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			["Asara, Mother of Night"] = {
				{
					4.546000000002096, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					10.37000000000262, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
				{
					11.44200000000274, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [3]
				{
					20.52400000000489, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [4]
				{
					26.61699999999837, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [5]
				{
					30.69099999999889, -- [1]
					"Asara, Mother of Night", -- [2]
					252861, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [6]
				{
					38.74300000000221, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [7]
				{
					39.81399999999849, -- [1]
					"Asara, Mother of Night", -- [2]
					246329, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [8]
				{
					49.6820000000007, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [9]
				{
					55.75500000000466, -- [1]
					"Asara, Mother of Night", -- [2]
					245303, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [10]
			},
			["Thu'raya, Mother of the Cosmos"] = {
				{
					49.54499999999825, -- [1]
					"Thu'raya, Mother of the Cosmos", -- [2]
					250334, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [4]
	},
}
