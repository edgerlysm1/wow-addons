
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008371,
							["damage_from"] = {
								["Young Thistle Boar"] = true,
							},
							["targets"] = {
								["Young Thistle Boar"] = 39,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 39.008371,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1537839871,
							["friendlyfire_total"] = 0,
							["spec"] = 269,
							["nome"] = "Aqulaylaszun",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 20,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Young Thistle Boar"] = 33,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 13,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 33,
										["c_max"] = 13,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 7,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 3,
										["targets"] = {
											["Young Thistle Boar"] = 6,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 6,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 6,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 39.008371,
							["serial"] = "Player-76-09EF8EDB",
							["last_dps"] = 6.5439307163215,
							["custom"] = 0,
							["last_event"] = 1537839871,
							["damage_taken"] = 9.008371,
							["start_time"] = 1537839865,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 2.006061,
							["resource"] = 0.006061,
							["targets"] = {
								["Aqulaylaszun"] = 2,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["total"] = 2.006061,
							["nome"] = "Aqulaylaszun",
							["spec"] = 269,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1537839866,
							["alternatepower"] = 0.006061,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[100780] = {
										["id"] = 100780,
										["total"] = 2,
										["targets"] = {
											["Aqulaylaszun"] = 2,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-09EF8EDB",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 269,
							["grupo"] = true,
							["spell_cast"] = {
								[100780] = 1,
							},
							["buff_uptime"] = 6,
							["nome"] = "Aqulaylaszun",
							["pets"] = {
							},
							["last_event"] = 1537839871,
							["classe"] = "MONK",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EF8EDB",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Aqulaylaszun"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Young Thistle Boar",
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					38.968176, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 1.991056,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Aqulaylaszun"] = 39.008371,
						}, -- [1]
					},
				},
				["end_time"] = 12127.619,
				["combat_id"] = 1,
				["instance_type"] = "none",
				["frags"] = {
					["Young Thistle Boar"] = 1,
				},
				["data_fim"] = "21:44:31",
				["data_inicio"] = "21:44:25",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					39, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 2,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 12121.658,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
		},
	},
	["last_version"] = "v8.0.1.6449",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 79.9378967285156,
					["x"] = 377.707214355469,
					["w"] = 310.000030517578,
					["h"] = 158.000106811523,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09EF8EDB"] = {
		},
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Aqulaylaszun-Sargeras",
	["last_realversion"] = 134,
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "24",
	["nick_tag_cache"] = {
		["nextreset"] = 1539135828,
		["last_version"] = 10,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["combat_id"] = 1,
	["savedStyles"] = {
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["combat_counter"] = 4,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.016582,
					["damage_from"] = {
						["Young Thistle Boar"] = true,
					},
					["targets"] = {
						["Young Thistle Boar"] = 39,
					},
					["spec"] = 269,
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "MONK",
					["raid_targets"] = {
					},
					["total_without_pet"] = 39.016582,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1537839871,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Aqulaylaszun",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 20,
								["g_amt"] = 0,
								["n_max"] = 6,
								["targets"] = {
									["Young Thistle Boar"] = 33,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 13,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 6,
								["total"] = 33,
								["c_max"] = 13,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 1,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[100780] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 3,
								["targets"] = {
									["Young Thistle Boar"] = 6,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 6,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 6,
								["c_max"] = 0,
								["id"] = 100780,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 39.016582,
					["serial"] = "Player-76-09EF8EDB",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 9.016582,
					["start_time"] = 1537839862,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 2.007166,
					["resource"] = 0.007166,
					["targets"] = {
						["Aqulaylaszun"] = 2,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "MONK",
					["total"] = 2.007166,
					["nome"] = "Aqulaylaszun",
					["spec"] = 269,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[100780] = {
								["id"] = 100780,
								["total"] = 2,
								["targets"] = {
									["Aqulaylaszun"] = 2,
								},
								["counter"] = 1,
							},
						},
					},
					["tipo"] = 3,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.007166,
					["serial"] = "Player-76-09EF8EDB",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["buff_uptime_targets"] = {
					},
					["nome"] = "Aqulaylaszun",
					["spec"] = 269,
					["grupo"] = true,
					["buff_uptime"] = 6,
					["classe"] = "MONK",
					["spell_cast"] = {
						[100780] = 1,
					},
					["last_event"] = 0,
					["pets"] = {
					},
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[186406] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 186406,
								["uptime"] = 6,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["serial"] = "Player-76-09EF8EDB",
					["tipo"] = 4,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			48.016058, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 2.006061,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["end_time"] = 12127.619,
		["data_inicio"] = "21:44:25",
		["frags"] = {
		},
		["data_fim"] = "21:44:31",
		["overall_enemy_name"] = "Young Thistle Boar",
		["CombatSkillCache"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 5.96100000000115,
				["type"] = 0,
				["name"] = "Young Thistle Boar",
				["clock"] = "21:44:25",
			}, -- [1]
		},
		["start_time"] = 12121.658,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["totals_grupo"] = {
			39.008371, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 2.006061,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["force_font_outline"] = "",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09EF8EDB"] = 269,
	},
}
