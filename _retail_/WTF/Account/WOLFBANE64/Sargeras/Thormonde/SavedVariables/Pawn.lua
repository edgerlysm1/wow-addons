
PawnOptions = {
	["LastVersion"] = 2.0418,
	["LastPlayerFullName"] = "Thormonde-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 183021,
			["Level"] = 213,
			["Link"] = "|cffa335ee|Hitem:183021::::::::60:258::5:5:7188:6652:7193:1485:6646::::|h[Confidant's Favored Cap]|h|r",
		}, -- [1]
		{
			["ID"] = 178827,
			["Level"] = 226,
			["Link"] = "|cffa335ee|Hitem:178827::::::::60:258::35:6:7214:6652:7193:1501:5884:6646::::|h[Sin Stained Pendant]|h|r",
		}, -- [2]
		{
			["ID"] = 173247,
			["Level"] = 235,
			["Link"] = "|cffff8000|Hitem:173247::::::::60:258::63:5:7002:6649:6647:6758:1532::::|h[Twins of the Sun Priestess]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 182998,
			["Level"] = 220,
			["Link"] = "|cffa335ee|Hitem:182998:6265:::::::60:258::84:4:7188:6652:1485:6646::::|h[Robes of the Cursed Commando]|h|r",
		}, -- [5]
		{
			["ID"] = 183004,
			["Level"] = 226,
			["Link"] = "|cffa335ee|Hitem:183004::::::::60:258::6:5:7187:6652:7193:1498:6646::::|h[Shadewarped Sash]|h|r",
		}, -- [6]
		{
			["ID"] = 173246,
			["Level"] = 235,
			["Link"] = "|cffff8000|Hitem:173246::::::::60:258::63:5:7162:6649:6648:6758:1532::::|h[Talbadar's Stratagem]|h|r",
		}, -- [7]
		{
			["ID"] = 182979,
			["Level"] = 226,
			["Link"] = "|cffa335ee|Hitem:182979::::::::60:258::6:4:7187:6652:1498:6646:1:28:753:::|h[Slippers of the Forgotten Heretic]|h|r",
		}, -- [8]
		{
			["ID"] = 173249,
			["Level"] = 235,
			["Link"] = "|cffff8000|Hitem:173249:6220:::::::60:256::63:6:6983:7194:6649:6647:6758:1532::::|h[Eternal Call to the Void]|h|r",
		}, -- [9]
		{
			["ID"] = 183022,
			["Level"] = 213,
			["Link"] = "|cffa335ee|Hitem:183022::::::::60:258::5:4:7188:41:1485:6646:1:28:752:::|h[Impossibly Oversized Mitts]|h|r",
		}, -- [10]
		{
			["ID"] = 183037,
			["Level"] = 226,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cffa335ee|Hitem:183037:6166:173128::::::60:258::6:5:7187:6652:6935:1498:6646:1:28:753:::|h[Ritualist's Treasured Ring]|h|r",
		}, -- [11]
		{
			["ID"] = 183038,
			["Level"] = 226,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cffa335ee|Hitem:183038::::::::60:256::6:5:7187:6652:7193:1498:6646:1:28:753:::|h[Hyperlight Band]|h|r",
		}, -- [12]
		{
			["ID"] = 184021,
			["Level"] = 226,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cffa335ee|Hitem:184021::::::::60:258::6:4:7187:6652:1498:6646:1:28:753:::|h[Glyph of Assimilation]|h|r",
		}, -- [13]
		{
			["ID"] = 178769,
			["Level"] = 226,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cffa335ee|Hitem:178769::::::::60:258::35:5:7213:6652:1501:5884:6646::::|h[Infinitely Divisible Ooze]|h|r",
		}, -- [14]
		{
			["ID"] = 183033,
			["Level"] = 226,
			["Link"] = "|cffa335ee|Hitem:183033:6204:::::::60:258::6:4:7187:6652:1498:6646:1:28:753:::|h[Mantle of Manifest Sins]|h|r",
		}, -- [15]
		{
			["ID"] = 178829,
			["Level"] = 223,
			["Link"] = "|cffa335ee|Hitem:178829:6228:::::::60:258::35:5:7212:6652:1501:5881:6646::::|h[Nathrian Ferula]|h|r",
		}, -- [16]
		{
			["ID"] = 180473,
			["Level"] = 140,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cffa335ee|Hitem:180473::::::::57:258::11:2:6707:6902:2:28:1236:9:56:::|h[Culexwood Spellmace]|h|r",
		}, -- [17]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PRIEST",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
