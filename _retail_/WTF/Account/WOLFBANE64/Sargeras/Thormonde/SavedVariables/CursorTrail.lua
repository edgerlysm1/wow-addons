
CursorTrail_PlayerConfig = {
	["FadeOut"] = false,
	["UserOfsY"] = -2.1,
	["UserShowMouseLook"] = false,
	["ModelID"] = 166498,
	["UserAlpha"] = 1,
	["UserOfsX"] = 2,
	["UserScale"] = 0.5,
	["UserShadowAlpha"] = 0,
	["Strata"] = "HIGH",
	["UserShowOnlyInCombat"] = false,
}
