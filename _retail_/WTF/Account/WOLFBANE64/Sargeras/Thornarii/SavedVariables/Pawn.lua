
PawnOptions = {
	["LastVersion"] = 2.0418,
	["LastPlayerFullName"] = "Thornarii-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 122250,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:122250::::::::31:256:::1:3592::::|h[Tattered Dreadmist Mask]|h|r",
		}, -- [1]
		{
			["ID"] = 153130,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:153130::::::::31:256:::::::|h[Man'ari Training Amulet]|h|r",
		}, -- [2]
		{
			["ID"] = 122378,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:122378::::::::31:256:::1:3592::::|h[Exquisite Sunderseer Mantle]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 122384,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:122384::::::::31:256:::1:3592::::|h[Tattered Dreadmist Robe]|h|r",
		}, -- [5]
		{
			["ID"] = 106157,
			["Level"] = 24,
			["Link"] = "|cff1eff00|Hitem:106157::::::::22:256::11:1:3631:2:9:20:28:1765:::|h[Frostwolf Wind-Talker Cord]|h|r",
		}, -- [6]
		{
			["ID"] = 122256,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:122256::::::::31:256:::1:3592::::|h[Tattered Dreadmist Leggings]|h|r",
		}, -- [7]
		{
			["ID"] = 134641,
			["Level"] = 24,
			["Link"] = "|cff1eff00|Hitem:134641::::::::22:256::7:2:1723:1709:2:9:19:28:2030:::|h[Auxiliary's Treads of the Aurora]|h|r",
		}, -- [8]
		{
			["ID"] = 134643,
			["Level"] = 28,
			["Link"] = "|cff1eff00|Hitem:134643::::::::22:256::7:2:1723:1676:2:9:22:28:2031:::|h[Auxiliary's Cuffs of the Quickblade]|h|r",
		}, -- [9]
		{
			["ID"] = 134669,
			["Level"] = 31,
			["Link"] = "|cff1eff00|Hitem:134669::::::::25:256::7:2:1723:1690:2:9:25:28:2031:::|h[Auxiliary's Handwraps of the Fireflash]|h|r",
		}, -- [10]
		{
			["ID"] = 128169,
			["Level"] = 36,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff00ccff|Hitem:128169::::::::31:256:::1:3592::::|h[Signet of the Third Fleet]|h|r",
		}, -- [11]
		{
			["ID"] = 128169,
			["Level"] = 35,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff00ccff|Hitem:128169::::::::30:256:::1:3592::::|h[Signet of the Third Fleet]|h|r",
		}, -- [12]
		{
			["ID"] = 134690,
			["Level"] = 38,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:134690::::::::31:256::7:3:1723:1724:1710:2:9:31:28:2032:::|h[Recruit's Distinction of the Aurora]|h|r",
		}, -- [13]
		{
			["ID"] = 122370,
			["Level"] = 36,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122370::::::::31:256:::1:582::::|h[Inherited Insignia of the Horde]|h|r",
		}, -- [14]
		{
			["ID"] = 122262,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:122262::::::::31:256:::1:3592::::|h[Ancient Bloodmoon Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 122368,
			["Level"] = 36,
			["Link"] = "|cff00ccff|Hitem:122368::::::::31:256:::::::|h[Grand Staff of Jordan]|h|r",
		}, -- [16]
		{
			["ID"] = 134645,
			["Level"] = 31,
			["Link"] = "|cff1eff00|Hitem:134645::::::::31:256::7:2:1723:1707:2:9:25:28:2031:::|h[Recruit's Endgame of the Aurora]|h|r",
		}, -- [17]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PRIEST",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
