
DetailsTimeLineDB = {
	["max_segments"] = 4,
	["combat_data"] = {
	},
	["hide_on_combat"] = false,
	["IndividualSpells"] = {
		{
			[305913] = {
				{
					7.549000000006345, -- [1]
					"Gor'groth", -- [2]
					305913, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Próphét-Thrall", -- [5]
				}, -- [1]
			},
			[308527] = {
				{
					12.15800000000309, -- [1]
					"Gor'groth", -- [2]
					308527, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [1]
		{
			[321240] = {
				{
					10.07300000000396, -- [1]
					"Tunk", -- [2]
					321240, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
			[308455] = {
				{
					23.13300000000163, -- [1]
					"Tunk", -- [2]
					308455, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
			},
		}, -- [2]
	},
	["useicons"] = true,
	["cooldowns_timeline"] = {
	},
	["window_scale"] = 1,
	["deaths_data"] = {
	},
	["debuff_timeline"] = {
	},
	["backdrop_color"] = {
		0, -- [1]
		0, -- [2]
		0, -- [3]
		0.4, -- [4]
	},
	["BossSpellCast"] = {
		{
			["Gor'groth"] = {
				{
					7.549000000006345, -- [1]
					"Gor'groth", -- [2]
					305913, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
					"Próphét-Thrall", -- [5]
				}, -- [1]
				{
					12.15800000000309, -- [1]
					"Gor'groth", -- [2]
					308527, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [1]
		{
			["Tunk"] = {
				{
					10.07300000000396, -- [1]
					"Tunk", -- [2]
					321240, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [1]
				{
					23.13300000000163, -- [1]
					"Tunk", -- [2]
					308455, -- [3]
					"SPELL_CAST_SUCCESS", -- [4]
				}, -- [2]
			},
		}, -- [2]
	},
}
