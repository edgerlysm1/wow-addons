
PawnOptions = {
	["LastVersion"] = 2.0231,
	["LastPlayerFullName"] = "Thorrand-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "ROGUE",
	["LastAdded"] = 1,
}
