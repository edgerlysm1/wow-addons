
CursorTrail_PlayerConfig = {
	["Path"] = "spells\\lightningboltivus_missile.mdx",
	["UserOfsY"] = 0,
	["BaseOfsX"] = -0.3,
	["BaseOfsY"] = 4.3,
	["BaseScale"] = 0.05,
	["BaseStepX"] = 471,
	["UserAlpha"] = 1,
	["UserOfsX"] = 0,
	["UserShowOnlyInCombat"] = false,
	["Version"] = "8.0.1.1",
	["UserScale"] = 1,
	["BaseStepY"] = 471,
}
