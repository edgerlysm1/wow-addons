
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004729,
							["damage_from"] = {
								["Dire Wolf"] = true,
								["Environment (Falling)"] = true,
							},
							["targets"] = {
							},
							["on_hold"] = false,
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.004729,
							["spec"] = 260,
							["dps_started"] = false,
							["total"] = 0.004729,
							["classe"] = "ROGUE",
							["serial"] = "Player-76-09EE6691",
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["grupo"] = true,
							["end_time"] = 1541547840,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 133.004729,
							["start_time"] = 1541547840,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 12,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 12,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 12,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 260,
							["grupo"] = true,
							["buff_uptime"] = 20,
							["nome"] = "Thorrand",
							["pets"] = {
							},
							["last_event"] = 1541547840,
							["classe"] = "ROGUE",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[148396] = {
										["activedamt"] = 1,
										["id"] = 148396,
										["targets"] = {
										},
										["uptime"] = 10,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 12,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrand"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Dire Wolf",
				["combat_counter"] = 23,
				["playing_solo"] = true,
				["totals"] = {
					-0.00682100000000219, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorrand"] = {
						{
							true, -- [1]
							3, -- [2]
							113, -- [3]
							1541547852.144, -- [4]
							1102, -- [5]
							"Environment (Falling)", -- [6]
							nil, -- [7]
							3, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = false,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrand"] = 0.004729,
						}, -- [1]
					},
				},
				["end_time"] = 3490.575,
				["combat_id"] = 12,
				["TotalElapsedCombatTime"] = 3490.575,
				["CombatEndedAt"] = 3490.575,
				["frags"] = {
				},
				["data_fim"] = "18:44:00",
				["data_inicio"] = "18:43:51",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					0, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 3480.866,
				["contra"] = "Dire Wolf",
				["TimeData"] = {
				},
			}, -- [1]
			{
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.00866,
							["total"] = 431.00866,
							["damage_from"] = {
								["Venom Web Spider"] = true,
							},
							["targets"] = {
								["Venom Web Spider"] = 431,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 431.00866,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1541547811,
							["on_hold"] = false,
							["spec"] = 260,
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 1,
										["c_dmg"] = 94,
										["g_amt"] = 0,
										["n_max"] = 38,
										["targets"] = {
											["Venom Web Spider"] = 132,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 38,
										["n_min"] = 38,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 132,
										["c_max"] = 54,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 54,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 40,
									}, -- [1]
									[59830] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 39,
										["targets"] = {
											["Venom Web Spider"] = 39,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 39,
										["n_min"] = 39,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 39,
										["c_max"] = 0,
										["id"] = 59830,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[197834] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 47,
										["targets"] = {
											["Venom Web Spider"] = 187,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 187,
										["n_min"] = 46,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 187,
										["c_max"] = 0,
										["id"] = 197834,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[185763] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 73,
										["targets"] = {
											["Venom Web Spider"] = 73,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 73,
										["n_min"] = 73,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 73,
										["c_max"] = 0,
										["id"] = 185763,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09EE6691",
							["last_dps"] = 95.3981097830893,
							["custom"] = 0,
							["last_event"] = 1541547810,
							["damage_taken"] = 81.00866,
							["start_time"] = 1541547806,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 11,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorrand"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 0.002953,
							["total_without_pet"] = 50.002953,
							["total"] = 50.002953,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09EE6691",
							["totalabsorb"] = 0.002953,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrand"] = 50,
							},
							["totalover_without_pet"] = 0.002953,
							["healing_taken"] = 50.002953,
							["fight_component"] = true,
							["end_time"] = 1541547811,
							["targets_overheal"] = {
							},
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 25,
										["targets"] = {
											["Thorrand"] = 50,
										},
										["n_min"] = 25,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 50,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 50,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1541547810,
							["custom"] = 0,
							["last_event"] = 1541547810,
							["spec"] = 260,
							["totaldenied"] = 0.002953,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 11,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.002533,
							["resource"] = 0.002533,
							["targets"] = {
								["Thorrand"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 3.002533,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1541547810,
							["alternatepower"] = 0.002533,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[193315] = {
										["id"] = 193315,
										["total"] = 2,
										["targets"] = {
											["Thorrand"] = 2,
										},
										["counter"] = 2,
									},
									[185763] = {
										["id"] = 185763,
										["total"] = 1,
										["targets"] = {
											["Thorrand"] = 1,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 11,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[185763] = {
										["activedamt"] = 0,
										["id"] = 185763,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 8,
							["classe"] = "ROGUE",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[195627] = {
										["activedamt"] = 1,
										["id"] = 195627,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[185763] = {
										["activedamt"] = 1,
										["id"] = 185763,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["fight_component"] = true,
							["debuff_uptime"] = 1,
							["buff_uptime_targets"] = {
							},
							["spec"] = 260,
							["grupo"] = true,
							["spell_cast"] = {
								[193315] = 3,
								[185763] = 1,
							},
							["debuff_uptime_targets"] = {
							},
							["last_event"] = 1541547811,
							["nome"] = "Thorrand",
							["pets"] = {
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 11,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrand"] = true,
				},
				["CombatStartedAt"] = 3479.672,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Venom Web Spider",
				["combat_counter"] = 22,
				["playing_solo"] = true,
				["totals"] = {
					430.997738, -- [1]
					50, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrand"] = 50.002953,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrand"] = 431.00866,
						}, -- [1]
					},
				},
				["end_time"] = 3461.775,
				["combat_id"] = 11,
				["TotalElapsedCombatTime"] = 4.51800000000003,
				["CombatEndedAt"] = 3461.775,
				["frags"] = {
					["Venom Web Spider"] = 1,
				},
				["data_fim"] = "18:43:31",
				["data_inicio"] = "18:43:27",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					431, -- [1]
					50, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 3457.257,
				["contra"] = "Venom Web Spider",
				["TimeData"] = {
				},
			}, -- [2]
			{
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005235,
							["total"] = 406.005235,
							["damage_from"] = {
								["Venom Web Spider"] = true,
							},
							["targets"] = {
								["Venom Web Spider"] = 406,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 406.005235,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1541547797,
							["on_hold"] = false,
							["spec"] = 260,
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 39,
										["targets"] = {
											["Venom Web Spider"] = 77,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 77,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 77,
										["c_max"] = 0,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[2098] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 108,
										["targets"] = {
											["Venom Web Spider"] = 108,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 108,
										["n_min"] = 108,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 108,
										["c_max"] = 0,
										["id"] = 2098,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[59830] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 82,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Venom Web Spider"] = 82,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 82,
										["c_max"] = 82,
										["id"] = 59830,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 82,
									},
									[197834] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 93,
										["g_amt"] = 0,
										["n_max"] = 46,
										["targets"] = {
											["Venom Web Spider"] = 139,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 46,
										["n_min"] = 46,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 139,
										["c_max"] = 93,
										["id"] = 197834,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 93,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09EE6691",
							["last_dps"] = 116.869670408745,
							["custom"] = 0,
							["last_event"] = 1541547797,
							["damage_taken"] = 55.005235,
							["start_time"] = 1541547793,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 10,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorrand"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 0.008921,
							["total_without_pet"] = 50.008921,
							["total"] = 50.008921,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09EE6691",
							["totalabsorb"] = 0.008921,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrand"] = 50,
							},
							["totalover_without_pet"] = 0.008921,
							["healing_taken"] = 50.008921,
							["fight_component"] = true,
							["end_time"] = 1541547797,
							["targets_overheal"] = {
							},
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 25,
										["targets"] = {
											["Thorrand"] = 50,
										},
										["n_min"] = 25,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 50,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 50,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1541547797,
							["custom"] = 0,
							["last_event"] = 1541547797,
							["spec"] = 260,
							["totaldenied"] = 0.008921,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 10,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 2.001937,
							["resource"] = 0.001937,
							["targets"] = {
								["Thorrand"] = 2,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 2.001937,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1541547806,
							["alternatepower"] = 0.001937,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[193315] = {
										["id"] = 193315,
										["total"] = 2,
										["targets"] = {
											["Thorrand"] = 2,
										},
										["counter"] = 2,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 10,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 4,
							["pets"] = {
							},
							["spell_cast"] = {
								[193315] = 2,
								[2098] = 1,
							},
							["classe"] = "ROGUE",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["last_event"] = 1541547797,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 10,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrand"] = true,
				},
				["CombatStartedAt"] = 3444.089,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Venom Web Spider",
				["combat_counter"] = 21,
				["playing_solo"] = true,
				["totals"] = {
					405.990517, -- [1]
					50, -- [2]
					{
						0, -- [1]
						[0] = 1.992197,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrand"] = 50.008921,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrand"] = 406.005235,
						}, -- [1]
					},
				},
				["end_time"] = 3447.563,
				["combat_id"] = 10,
				["TotalElapsedCombatTime"] = 3.47400000000016,
				["CombatEndedAt"] = 3447.563,
				["frags"] = {
					["Venom Web Spider"] = 1,
				},
				["data_fim"] = "18:43:17",
				["data_inicio"] = "18:43:14",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					406, -- [1]
					50, -- [2]
					{
						0, -- [1]
						[0] = 2,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 3444.089,
				["contra"] = "Venom Web Spider",
				["TimeData"] = {
				},
			}, -- [3]
			{
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007505,
							["total"] = 409.007505,
							["damage_from"] = {
								["Nightbane Worgen"] = true,
							},
							["targets"] = {
								["Nightbane Worgen"] = 409,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 409.007505,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1541547763,
							["on_hold"] = false,
							["spec"] = 260,
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 84,
										["g_amt"] = 0,
										["n_max"] = 40,
										["targets"] = {
											["Nightbane Worgen"] = 201,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 117,
										["n_min"] = 19,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 201,
										["c_max"] = 84,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 84,
									}, -- [1]
									[59830] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 79,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Nightbane Worgen"] = 79,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 79,
										["c_max"] = 79,
										["id"] = 59830,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 79,
									},
									[197834] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 47,
										["targets"] = {
											["Nightbane Worgen"] = 93,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 93,
										["n_min"] = 46,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 93,
										["c_max"] = 0,
										["id"] = 197834,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[185763] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 36,
										["targets"] = {
											["Nightbane Worgen"] = 36,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 36,
										["n_min"] = 36,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 36,
										["c_max"] = 0,
										["id"] = 185763,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09EE6691",
							["last_dps"] = 88.779575645757,
							["custom"] = 0,
							["last_event"] = 1541547763,
							["damage_taken"] = 44.007505,
							["start_time"] = 1541547758,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 9,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorrand"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 6.007987,
							["total_without_pet"] = 44.007987,
							["total"] = 44.007987,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09EE6691",
							["totalabsorb"] = 0.007987,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrand"] = 50,
							},
							["totalover_without_pet"] = 0.007987,
							["healing_taken"] = 44.007987,
							["fight_component"] = true,
							["end_time"] = 1541547763,
							["targets_overheal"] = {
								["Thorrand"] = 6,
							},
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorrand"] = 6,
										},
										["n_max"] = 25,
										["targets"] = {
											["Thorrand"] = 44,
										},
										["n_min"] = 19,
										["counter"] = 2,
										["overheal"] = 6,
										["total"] = 44,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 44,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1541547763,
							["custom"] = 0,
							["last_event"] = 1541547763,
							["spec"] = 260,
							["totaldenied"] = 0.007987,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 9,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.002784,
							["resource"] = 0.002784,
							["targets"] = {
								["Thorrand"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 3.002784,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1541547793,
							["alternatepower"] = 0.002784,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[193315] = {
										["id"] = 193315,
										["total"] = 2,
										["targets"] = {
											["Thorrand"] = 2,
										},
										["counter"] = 2,
									},
									[185763] = {
										["id"] = 185763,
										["total"] = 1,
										["targets"] = {
											["Thorrand"] = 1,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 9,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["cc_break_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[193315] = {
										["cc_break_oque"] = {
											[6770] = 1,
										},
										["id"] = 193315,
										["cc_break"] = 1,
										["targets"] = {
											["Nightbane Worgen"] = 1,
										},
										["counter"] = 0,
									},
								},
							},
							["buff_uptime"] = 6,
							["classe"] = "ROGUE",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 5,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[185763] = {
										["activedamt"] = 1,
										["id"] = 185763,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["cc_break"] = 1.008758,
							["fight_component"] = true,
							["debuff_uptime"] = 2,
							["debuff_uptime_targets"] = {
							},
							["debuff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[6770] = {
										["activedamt"] = -1,
										["id"] = 6770,
										["targets"] = {
										},
										["actived_at"] = 1541547758,
										["uptime"] = 0,
										["appliedamt"] = 0,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[185763] = {
										["activedamt"] = 0,
										["id"] = 185763,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["buff_uptime_targets"] = {
							},
							["spec"] = 260,
							["grupo"] = true,
							["spell_cast"] = {
								[193315] = 2,
								[185763] = 1,
							},
							["cc_break_oque"] = {
								[6770] = 1,
							},
							["pets"] = {
							},
							["last_event"] = 1541547763,
							["nome"] = "Thorrand",
							["cc_break_targets"] = {
								["Nightbane Worgen"] = 1,
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 9,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrand"] = true,
				},
				["CombatStartedAt"] = 3409.335,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Nightbane Worgen",
				["combat_counter"] = 20,
				["playing_solo"] = true,
				["totals"] = {
					408.984892, -- [1]
					44, -- [2]
					{
						0, -- [1]
						[0] = 2.99811099999999,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 1,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrand"] = 44.007987,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrand"] = 409.007505,
						}, -- [1]
					},
				},
				["end_time"] = 3413.942,
				["combat_id"] = 9,
				["TotalElapsedCombatTime"] = 4.60699999999997,
				["CombatEndedAt"] = 3413.942,
				["frags"] = {
					["Nightbane Worgen"] = 1,
				},
				["data_fim"] = "18:42:44",
				["data_inicio"] = "18:42:39",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					409, -- [1]
					44, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 1,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 3409.335,
				["contra"] = "Nightbane Worgen",
				["TimeData"] = {
				},
			}, -- [4]
			{
				{
					["combatId"] = 8,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006205,
							["total"] = 407.006205,
							["damage_from"] = {
							},
							["targets"] = {
								["Nightbane Worgen"] = 407,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 407.006205,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1541547745,
							["on_hold"] = false,
							["spec"] = 260,
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 41,
										["targets"] = {
											["Nightbane Worgen"] = 41,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 41,
										["n_min"] = 41,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 41,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[2098] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 366,
										["g_amt"] = 0,
										["n_max"] = 0,
										["targets"] = {
											["Nightbane Worgen"] = 366,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 0,
										["n_min"] = 0,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 366,
										["c_max"] = 366,
										["id"] = 2098,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 0,
										["r_amt"] = 0,
										["c_min"] = 366,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09EE6691",
							["last_dps"] = 368.999279238419,
							["custom"] = 0,
							["last_event"] = 1541547744,
							["damage_taken"] = 0.006205,
							["start_time"] = 1541547744,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 8,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorrand"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 28.005827,
							["total_without_pet"] = 22.005827,
							["total"] = 22.005827,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09EE6691",
							["totalabsorb"] = 0.005827,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrand"] = 25,
							},
							["totalover_without_pet"] = 0.005827,
							["healing_taken"] = 22.005827,
							["fight_component"] = true,
							["end_time"] = 1541547745,
							["targets_overheal"] = {
								["Thorrand"] = 28,
							},
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorrand"] = 28,
										},
										["n_max"] = 22,
										["targets"] = {
											["Thorrand"] = 22,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 28,
										["total"] = 22,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 22,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1541547744,
							["custom"] = 0,
							["last_event"] = 1541547744,
							["spec"] = 260,
							["totaldenied"] = 0.005827,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 8,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 1.006088,
							["resource"] = 0.006088,
							["targets"] = {
								["Thorrand"] = 1,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["total"] = 1.006088,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1541547758,
							["alternatepower"] = 0.006088,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[193315] = {
										["id"] = 193315,
										["total"] = 1,
										["targets"] = {
											["Thorrand"] = 1,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 8,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 1,
							["pets"] = {
							},
							["spell_cast"] = {
								[2098] = 1,
							},
							["classe"] = "ROGUE",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["last_event"] = 1541547745,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 8,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrand"] = true,
				},
				["CombatStartedAt"] = 3395.06,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Nightbane Worgen",
				["combat_counter"] = 19,
				["playing_solo"] = true,
				["totals"] = {
					406.995578, -- [1]
					22, -- [2]
					{
						0, -- [1]
						[0] = 1,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrand"] = 22.005827,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrand"] = 407.006205,
						}, -- [1]
					},
				},
				["end_time"] = 3396.163,
				["combat_id"] = 8,
				["TotalElapsedCombatTime"] = 1.10300000000007,
				["CombatEndedAt"] = 3396.163,
				["frags"] = {
					["Nightbane Worgen"] = 1,
				},
				["data_fim"] = "18:42:26",
				["data_inicio"] = "18:42:25",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					407, -- [1]
					22, -- [2]
					{
						0, -- [1]
						[0] = 1,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 3395.06,
				["contra"] = "Nightbane Worgen",
				["TimeData"] = {
				},
			}, -- [5]
			{
				{
					["combatId"] = 7,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008538,
							["total"] = 419.008538,
							["damage_from"] = {
								["Nightbane Worgen"] = true,
								["Venom Web Spider"] = true,
							},
							["targets"] = {
								["Venom Web Spider"] = 419,
							},
							["pets"] = {
							},
							["friendlyfire"] = {
							},
							["colocacao"] = 1,
							["classe"] = "ROGUE",
							["raid_targets"] = {
							},
							["total_without_pet"] = 419.008538,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["end_time"] = 1541547735,
							["on_hold"] = false,
							["spec"] = 260,
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 79,
										["g_amt"] = 0,
										["n_max"] = 38,
										["targets"] = {
											["Venom Web Spider"] = 157,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 78,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 157,
										["c_max"] = 79,
										["MISS"] = 1,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 79,
									}, -- [1]
									[197834] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 48,
										["targets"] = {
											["Venom Web Spider"] = 189,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 189,
										["n_min"] = 47,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 189,
										["c_max"] = 0,
										["id"] = 197834,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
									[185763] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 73,
										["targets"] = {
											["Venom Web Spider"] = 73,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 73,
										["n_min"] = 73,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 73,
										["c_max"] = 0,
										["id"] = 185763,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09EE6691",
							["last_dps"] = 86.1271403905405,
							["custom"] = 0,
							["last_event"] = 1541547734,
							["damage_taken"] = 72.008538,
							["start_time"] = 1541547729,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 7,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorrand"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "ROGUE",
							["totalover"] = 0.001298,
							["total_without_pet"] = 50.001298,
							["total"] = 50.001298,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09EE6691",
							["totalabsorb"] = 0.001298,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrand"] = 50,
							},
							["totalover_without_pet"] = 0.001298,
							["healing_taken"] = 50.001298,
							["fight_component"] = true,
							["end_time"] = 1541547735,
							["targets_overheal"] = {
							},
							["nome"] = "Thorrand",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 25,
										["targets"] = {
											["Thorrand"] = 50,
										},
										["n_min"] = 25,
										["counter"] = 2,
										["overheal"] = 0,
										["total"] = 50,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 50,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1541547734,
							["custom"] = 0,
							["last_event"] = 1541547734,
							["spec"] = 260,
							["totaldenied"] = 0.001298,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 7,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.007871,
							["resource"] = 0.007871,
							["targets"] = {
								["Thorrand"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "ROGUE",
							["fight_component"] = true,
							["total"] = 3.007871,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1541547734,
							["alternatepower"] = 0.007871,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[193315] = {
										["id"] = 193315,
										["total"] = 2,
										["targets"] = {
											["Thorrand"] = 2,
										},
										["counter"] = 2,
									},
									[185763] = {
										["id"] = 185763,
										["total"] = 1,
										["targets"] = {
											["Thorrand"] = 1,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 7,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["flag_original"] = 1047,
							["nome"] = "Thorrand",
							["spec"] = 260,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 8,
							["pets"] = {
							},
							["spell_cast"] = {
								[193315] = 3,
								[185763] = 1,
							},
							["classe"] = "ROGUE",
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[195627] = {
										["activedamt"] = 1,
										["id"] = 195627,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[185763] = {
										["activedamt"] = 1,
										["id"] = 185763,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EE6691",
							["last_event"] = 1541547735,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 7,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrand"] = true,
				},
				["CombatStartedAt"] = 3380.172,
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Venom Web Spider",
				["combat_counter"] = 18,
				["playing_solo"] = true,
				["totals"] = {
					418.987794, -- [1]
					50, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
					["Thorrand"] = {
						{
							true, -- [1]
							1, -- [2]
							22, -- [3]
							1541547745.227, -- [4]
							1215, -- [5]
							"Nightbane Worgen", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
						}, -- [2]
						{
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 2,
					},
				},
				["instance_type"] = "none",
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrand"] = 50.001298,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrand"] = 419.008538,
						}, -- [1]
					},
				},
				["end_time"] = 3385.981,
				["combat_id"] = 7,
				["TotalElapsedCombatTime"] = 5.8090000000002,
				["CombatEndedAt"] = 3385.981,
				["frags"] = {
					["Venom Web Spider"] = 1,
				},
				["data_fim"] = "18:42:16",
				["data_inicio"] = "18:42:10",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					419, -- [1]
					50, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 3380.172,
				["contra"] = "Venom Web Spider",
				["TimeData"] = {
				},
			}, -- [6]
		},
	},
	["combat_counter"] = 23,
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 59.9689636230469,
					["x"] = -541.666168212891,
					["w"] = 310.000061035156,
					["h"] = 158.000030517578,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09EE6691"] = {
		},
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["active_profile"] = "Thorrand-Sargeras",
	["last_realversion"] = 135,
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["showamount"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["animate"] = false,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["nick_tag_cache"] = {
		["nextreset"] = 1542843562,
		["last_version"] = 10,
	},
	["ignore_nicktag"] = false,
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["last_version"] = "v8.0.1.6600",
	["combat_id"] = 12,
	["savedStyles"] = {
	},
	["last_instance_time"] = 0,
	["last_day"] = "06",
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.049672,
					["damage_from"] = {
						["Dire Wolf"] = true,
						["Nightbane Worgen"] = true,
						["Venom Web Spider"] = true,
					},
					["targets"] = {
						["Nightbane Worgen"] = 816,
						["Venom Web Spider"] = 1256,
					},
					["pets"] = {
					},
					["friendlyfire_total"] = 0,
					["raid_targets"] = {
					},
					["total_without_pet"] = 2072.049672,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["dps_started"] = false,
					["total"] = 2072.049672,
					["classe"] = "ROGUE",
					["end_time"] = 1541547736,
					["nome"] = "Thorrand",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 4,
								["b_amt"] = 1,
								["c_dmg"] = 257,
								["g_amt"] = 0,
								["n_max"] = 41,
								["targets"] = {
									["Nightbane Worgen"] = 242,
									["Venom Web Spider"] = 366,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 351,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 19,
								["total"] = 608,
								["c_max"] = 84,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 3,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 54,
								["n_amt"] = 12,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[2098] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 366,
								["g_amt"] = 0,
								["n_max"] = 108,
								["targets"] = {
									["Venom Web Spider"] = 108,
									["Nightbane Worgen"] = 366,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 108,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 474,
								["c_max"] = 366,
								["id"] = 2098,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[185763] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 73,
								["targets"] = {
									["Nightbane Worgen"] = 36,
									["Venom Web Spider"] = 146,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 182,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 182,
								["c_max"] = 0,
								["id"] = 185763,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[197834] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 93,
								["g_amt"] = 0,
								["n_max"] = 48,
								["targets"] = {
									["Nightbane Worgen"] = 93,
									["Venom Web Spider"] = 515,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 515,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["total"] = 608,
								["c_max"] = 93,
								["id"] = 197834,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 11,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[59830] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 161,
								["g_amt"] = 0,
								["n_max"] = 39,
								["targets"] = {
									["Venom Web Spider"] = 121,
									["Nightbane Worgen"] = 79,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 39,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 200,
								["c_max"] = 82,
								["id"] = 59830,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["on_hold"] = false,
					["spec"] = 260,
					["serial"] = "Player-76-09EE6691",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1541547712,
					["delay"] = 0,
					["damage_taken"] = 250.049672,
				}, -- [1]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["targets_overheal"] = {
						["Thorrand"] = 34,
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["heal_enemy_amt"] = 0,
					["totalover"] = 34.033121,
					["total_without_pet"] = 216.033121,
					["total"] = 216.033121,
					["targets_absorbs"] = {
					},
					["heal_enemy"] = {
					},
					["on_hold"] = false,
					["serial"] = "Player-76-09EE6691",
					["totalabsorb"] = 0.033121,
					["last_hps"] = 0,
					["targets"] = {
						["Thorrand"] = 225,
					},
					["totalover_without_pet"] = 0.033121,
					["healing_taken"] = 216.033121,
					["fight_component"] = true,
					["end_time"] = 1541547736,
					["healing_from"] = {
						["Thorrand"] = true,
					},
					["nome"] = "Thorrand",
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[59913] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thorrand"] = 34,
								},
								["n_max"] = 25,
								["targets"] = {
									["Thorrand"] = 216,
								},
								["n_min"] = 0,
								["counter"] = 10,
								["overheal"] = 34,
								["total"] = 216,
								["c_max"] = 0,
								["id"] = 59913,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 216,
								["n_amt"] = 10,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["grupo"] = true,
					["start_time"] = 1541547730,
					["classe"] = "ROGUE",
					["custom"] = 0,
					["tipo"] = 2,
					["spec"] = 260,
					["totaldenied"] = 0.033121,
					["delay"] = 0,
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 9.02289,
					["resource"] = 0.02289,
					["targets"] = {
						["Thorrand"] = 9,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "ROGUE",
					["fight_component"] = true,
					["total"] = 9.02289,
					["nome"] = "Thorrand",
					["spec"] = 260,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[193315] = {
								["id"] = 193315,
								["total"] = 6,
								["targets"] = {
									["Thorrand"] = 6,
								},
								["counter"] = 6,
							},
							[185763] = {
								["id"] = 185763,
								["total"] = 3,
								["targets"] = {
									["Thorrand"] = 3,
								},
								["counter"] = 3,
							},
						},
					},
					["tipo"] = 3,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.02289,
					["serial"] = "Player-76-09EE6691",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["debuff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[6770] = {
								["refreshamt"] = 0,
								["appliedamt"] = 0,
								["activedamt"] = -1,
								["uptime"] = 0,
								["id"] = 6770,
								["actived_at"] = 1541547758,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[185763] = {
								["refreshamt"] = 0,
								["activedamt"] = 0,
								["appliedamt"] = 2,
								["id"] = 185763,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["buff_uptime"] = 47,
					["classe"] = "ROGUE",
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[186406] = {
								["refreshamt"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 186406,
								["uptime"] = 31,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[195627] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 195627,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[148396] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 148396,
								["uptime"] = 10,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[185763] = {
								["refreshamt"] = 0,
								["activedamt"] = 3,
								["appliedamt"] = 3,
								["id"] = 185763,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["cc_break"] = 1.008758,
					["debuff_uptime"] = 3,
					["cc_break_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[193315] = {
								["cc_break_oque"] = {
									[6770] = 1,
								},
								["id"] = 193315,
								["cc_break"] = 1,
								["targets"] = {
									["Nightbane Worgen"] = 1,
								},
								["counter"] = 0,
							},
						},
					},
					["pets"] = {
					},
					["buff_uptime_targets"] = {
					},
					["spec"] = 260,
					["grupo"] = true,
					["spell_cast"] = {
						[193315] = 10,
						[185763] = 3,
						[2098] = 2,
					},
					["cc_break_oque"] = {
						[6770] = 1,
					},
					["debuff_uptime_targets"] = {
					},
					["last_event"] = 0,
					["nome"] = "Thorrand",
					["cc_break_targets"] = {
						["Nightbane Worgen"] = 1,
					},
					["serial"] = "Player-76-09EE6691",
					["tipo"] = 4,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 17,
		["totals"] = {
			2453.067616, -- [1]
			216.026986, -- [2]
			{
				0, -- [1]
				[0] = 9.015125,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 1.008758,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["end_time"] = 3490.575,
		["data_inicio"] = "18:42:10",
		["frags"] = {
		},
		["data_fim"] = "18:44:00",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 9.70900000000029,
				["type"] = 0,
				["name"] = "Dire Wolf",
				["clock"] = "18:43:51",
			}, -- [1]
			{
				["elapsed"] = 4.51800000000003,
				["type"] = 0,
				["name"] = "Venom Web Spider",
				["clock"] = "18:43:27",
			}, -- [2]
			{
				["elapsed"] = 3.47400000000016,
				["type"] = 0,
				["name"] = "Venom Web Spider",
				["clock"] = "18:43:14",
			}, -- [3]
			{
				["elapsed"] = 4.60699999999997,
				["type"] = 0,
				["name"] = "Nightbane Worgen",
				["clock"] = "18:42:39",
			}, -- [4]
			{
				["elapsed"] = 1.10300000000007,
				["type"] = 0,
				["name"] = "Nightbane Worgen",
				["clock"] = "18:42:25",
			}, -- [5]
			{
				["elapsed"] = 5.8090000000002,
				["type"] = 0,
				["name"] = "Venom Web Spider",
				["clock"] = "18:42:10",
			}, -- [6]
		},
		["start_time"] = 3461.355,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["totals_grupo"] = {
			2072.040872, -- [1]
			216.026986, -- [2]
			{
				0, -- [1]
				[0] = 9.015125,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 1.008758,
				["dead"] = 0,
			}, -- [4]
		},
	},
	["character_data"] = {
		["logons"] = 4,
	},
	["force_font_outline"] = "",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09F3B96C"] = 254,
		["Player-76-09EE6691"] = 260,
		["Player-76-09FA3C67"] = 72,
	},
}
