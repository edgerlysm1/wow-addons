
PawnOptions = {
	["LastVersion"] = 2.0418,
	["LastPlayerFullName"] = "Odintsun-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 134684,
			["Level"] = 19,
			["Link"] = "|cff1eff00|Hitem:134684::::::::15:65::7:2:1723:1690:2:9:15:28:2030:::|h[Front-Line Helm of the Fireflash]|h|r",
		}, -- [1]
		{
			["ID"] = 122667,
			["Level"] = 20,
			["Link"] = "|cff00ccff|Hitem:122667::::::::16:65:::1:3592::::|h[Eternal Emberfury Talisman]|h|r",
		}, -- [2]
		{
			["ID"] = 134686,
			["Level"] = 17,
			["Link"] = "|cff1eff00|Hitem:134686::::::::13:65::7:2:1723:1682:2:9:13:28:2030:::|h[Front-Line Shoulders of the Quickblade]|h|r",
		}, -- [3]
		[17] = {
			["ID"] = 134662,
			["Level"] = 19,
			["Link"] = "|cff1eff00|Hitem:134662::::::::15:65::7:2:1723:1676:2:9:15:28:2030:::|h[Recruit's Redoubt of the Quickblade]|h|r",
		},
		[7] = {
			["ID"] = 122264,
			["Level"] = 20,
			["Link"] = "|cff00ccff|Hitem:122264::::::::16:65:::1:5805::::|h[Burnished Legplates of Might]|h|r",
		},
		[15] = {
			["ID"] = 122262,
			["Level"] = 20,
			["Link"] = "|cff00ccff|Hitem:122262::::::::16:65:::1:3592::::|h[Ancient Bloodmoon Cloak]|h|r",
		},
		[10] = {
			["ID"] = 134683,
			["Level"] = 14,
			["Link"] = "|cff1eff00|Hitem:134683::::::::11:65::7:2:1723:1681:2:9:10:28:2030:::|h[Front-Line Gauntlets of the Quickblade]|h|r",
		},
		[16] = {
			["ID"] = 122354,
			["Level"] = 20,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cff00ccff|Hitem:122354::::::::16:65:::::::|h[Devout Aurastone Hammer]|h|r",
		},
		[14] = {
			["ID"] = 122370,
			["Level"] = 18,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122370::::::::15:65:::1:582::::|h[Inherited Insignia of the Horde]|h|r",
		},
		[5] = {
			["ID"] = 134655,
			["Level"] = 18,
			["Link"] = "|cff1eff00|Hitem:134655::::::::14:65::7:2:1723:1707:2:9:14:28:2030:::|h[Front-Line Breastplate of the Aurora]|h|r",
		},
		[13] = {
			["ID"] = 122370,
			["Level"] = 20,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122370::::::::16:65:::1:582::::|h[Inherited Insignia of the Horde]|h|r",
		},
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PALADIN",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
