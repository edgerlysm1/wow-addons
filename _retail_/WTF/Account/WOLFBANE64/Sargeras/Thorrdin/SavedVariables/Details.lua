
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["tipo"] = 2,
					["combatId"] = 16,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.002995,
							["damage_from"] = {
								["Scarlet Infantryman"] = true,
								["Scarlet Medic"] = true,
							},
							["targets"] = {
								["Scarlet Peasant"] = 6321,
								["Scarlet Infantryman"] = 8544,
								["Scarlet Medic"] = 9027,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 23892.002995,
							["friendlyfire_total"] = 0,
							["dps_started"] = false,
							["total"] = 23892.002995,
							["damage_taken"] = 3845.002995,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 4,
										["b_amt"] = 0,
										["c_dmg"] = 1608,
										["g_amt"] = 0,
										["n_max"] = 211,
										["targets"] = {
											["Scarlet Peasant"] = 526,
											["Scarlet Infantryman"] = 2114,
											["Scarlet Medic"] = 1223,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2255,
										["n_min"] = 102,
										["g_dmg"] = 0,
										["counter"] = 20,
										["total"] = 3863,
										["c_max"] = 416,
										["c_min"] = 394,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 14,
										["spellschool"] = 1,
										["MISS"] = 2,
									}, -- [1]
									[237680] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 271,
										["targets"] = {
											["Scarlet Infantryman"] = 270,
											["Scarlet Peasant"] = 271,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 541,
										["n_min"] = 270,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 541,
										["c_max"] = 0,
										["id"] = 237680,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66196] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 492,
										["targets"] = {
											["Scarlet Peasant"] = 485,
											["Scarlet Infantryman"] = 480,
											["Scarlet Medic"] = 492,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1457,
										["n_min"] = 480,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1457,
										["c_max"] = 0,
										["id"] = 66196,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222024] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 2161,
										["g_amt"] = 0,
										["n_max"] = 1158,
										["targets"] = {
											["Scarlet Peasant"] = 1146,
											["Scarlet Infantryman"] = 2242,
											["Scarlet Medic"] = 2161,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3388,
										["n_min"] = 1084,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 5549,
										["c_max"] = 2161,
										["id"] = 222024,
										["r_dmg"] = 0,
										["c_min"] = 2161,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[55095] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 901,
										["g_amt"] = 0,
										["n_max"] = 226,
										["targets"] = {
											["Scarlet Peasant"] = 676,
											["Scarlet Infantryman"] = 1352,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1127,
										["n_min"] = 225,
										["g_dmg"] = 0,
										["counter"] = 7,
										["total"] = 2028,
										["c_max"] = 451,
										["id"] = 55095,
										["r_dmg"] = 0,
										["c_min"] = 450,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222026] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 3923,
										["g_amt"] = 0,
										["n_max"] = 1005,
										["targets"] = {
											["Scarlet Peasant"] = 1976,
											["Scarlet Infantryman"] = 970,
											["Scarlet Medic"] = 3923,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2946,
										["n_min"] = 970,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 6869,
										["c_max"] = 2003,
										["id"] = 222026,
										["r_dmg"] = 0,
										["c_min"] = 1920,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66198] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 565,
										["targets"] = {
											["Scarlet Peasant"] = 565,
											["Scarlet Infantryman"] = 1116,
											["Scarlet Medic"] = 552,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2233,
										["n_min"] = 552,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 2233,
										["c_max"] = 0,
										["id"] = 66198,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[49184] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 676,
										["g_amt"] = 0,
										["n_max"] = 338,
										["targets"] = {
											["Scarlet Peasant"] = 676,
											["Scarlet Medic"] = 676,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 676,
										["n_min"] = 338,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1352,
										["c_max"] = 676,
										["id"] = 49184,
										["r_dmg"] = 0,
										["c_min"] = 676,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["last_dps"] = 786.82703754323,
							["end_time"] = 1530841761,
							["colocacao"] = 1,
							["last_event"] = 1530841761,
							["on_hold"] = false,
							["start_time"] = 1530841731,
							["serial"] = "Player-76-09B75621",
							["friendlyfire"] = {
							},
						}, -- [1]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 16,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "DEATHKNIGHT",
							["totalover"] = 0.007704,
							["total_without_pet"] = 759.007704,
							["total"] = 759.007704,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09B75621",
							["totalabsorb"] = 0.007704,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrdin"] = 759,
							},
							["totalover_without_pet"] = 0.007704,
							["healing_taken"] = 759.007704,
							["fight_component"] = true,
							["end_time"] = 1530841761,
							["heal_enemy_amt"] = 0,
							["nome"] = "Thorrdin",
							["spells"] = {
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 190,
										["targets"] = {
											["Thorrdin"] = 759,
										},
										["n_min"] = 189,
										["counter"] = 4,
										["overheal"] = 0,
										["total"] = 759,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["c_min"] = 0,
										["totaldenied"] = 0,
										["n_amt"] = 4,
										["n_curado"] = 759,
										["m_amt"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["healing_from"] = {
								["Thorrdin"] = true,
							},
							["tipo"] = 2,
							["custom"] = 0,
							["last_event"] = 1530841761,
							["totaldenied"] = 0.007704,
							["start_time"] = 1530841760,
							["delay"] = 1530841750,
							["spec"] = 251,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 16,
					["_ActorTable"] = {
						{
							["received"] = 115.003293,
							["resource"] = 0.003293,
							["targets"] = {
								["Thorrdin"] = 115,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "DEATHKNIGHT",
							["fight_component"] = true,
							["total"] = 115.003293,
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["tipo"] = 3,
							["last_event"] = 1530841758,
							["alternatepower"] = 0.003293,
							["spells"] = {
								["_ActorTable"] = {
									[49184] = {
										["id"] = 49184,
										["total"] = 20,
										["targets"] = {
											["Thorrdin"] = 20,
										},
										["counter"] = 2,
									},
									[49020] = {
										["id"] = 49020,
										["total"] = 80,
										["targets"] = {
											["Thorrdin"] = 80,
										},
										["counter"] = 4,
									},
									[195617] = {
										["id"] = 195617,
										["total"] = 15,
										["targets"] = {
											["Thorrdin"] = 15,
										},
										["counter"] = 3,
									},
								},
								["tipo"] = 7,
							},
							["serial"] = "Player-76-09B75621",
							["flag_original"] = 1297,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 16,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[55095] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 1,
										["id"] = 55095,
										["uptime"] = 22,
										["targets"] = {
										},
										["appliedamt"] = 4,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 30,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[51915] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 51915,
										["uptime"] = 30,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 22,
							["buff_uptime_targets"] = {
							},
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 2,
								[49576] = 1,
								[49020] = 4,
								[49143] = 5,
							},
							["tipo"] = 4,
							["last_event"] = 1530841761,
							["nome"] = "Thorrdin",
							["pets"] = {
							},
							["serial"] = "Player-76-09B75621",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 16,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrdin"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Scarlet Infantryman",
				["combat_counter"] = 26,
				["playing_solo"] = true,
				["totals"] = {
					23891.927636, -- [1]
					759, -- [2]
					{
						0, -- [1]
						[0] = 114.997169,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorrdin"] = 23892.002995,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrdin"] = 759.007704,
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 11132.541,
				["combat_id"] = 16,
				["TimeData"] = {
				},
				["hasSaved"] = true,
				["frags"] = {
					["Scarlet Peasant"] = 2,
					["Acherus Geist"] = 1,
					["Scarlet Medic"] = 1,
					["Scarlet Infantryman"] = 2,
				},
				["data_fim"] = "21:49:22",
				["data_inicio"] = "21:48:52",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					23892, -- [1]
					759, -- [2]
					{
						0, -- [1]
						[0] = 115,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 11102.176,
				["contra"] = "Scarlet Peasant",
				["instance_type"] = "none",
			}, -- [1]
			{
				{
					["tipo"] = 2,
					["combatId"] = 15,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.004054,
							["damage_from"] = {
								["Scarlet Medic"] = true,
								["Scarlet Captain"] = true,
							},
							["targets"] = {
								["Scarlet Medic"] = 8923,
								["Scarlet Captain"] = 10017,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 18940.004054,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 18940.004054,
							["damage_taken"] = 2593.004054,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 613,
										["g_amt"] = 0,
										["n_max"] = 208,
										["targets"] = {
											["Scarlet Medic"] = 1010,
											["Scarlet Captain"] = 822,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1219,
										["n_min"] = 102,
										["g_dmg"] = 0,
										["counter"] = 11,
										["total"] = 1832,
										["c_max"] = 407,
										["c_min"] = 206,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 8,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									[66196] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 963,
										["g_amt"] = 0,
										["n_max"] = 504,
										["targets"] = {
											["Scarlet Medic"] = 1459,
											["Scarlet Captain"] = 972,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1468,
										["n_min"] = 468,
										["g_dmg"] = 0,
										["counter"] = 4,
										["total"] = 2431,
										["c_max"] = 963,
										["id"] = 66196,
										["r_dmg"] = 0,
										["c_min"] = 963,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 225,
										["targets"] = {
											["Scarlet Medic"] = 450,
											["Scarlet Captain"] = 225,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 675,
										["n_min"] = 225,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 675,
										["c_max"] = 0,
										["id"] = 55095,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222024] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 4508,
										["g_amt"] = 0,
										["n_max"] = 1149,
										["targets"] = {
											["Scarlet Medic"] = 2208,
											["Scarlet Captain"] = 3449,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1149,
										["n_min"] = 1149,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 5657,
										["c_max"] = 2300,
										["id"] = 222024,
										["r_dmg"] = 0,
										["c_min"] = 2208,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[222026] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1003,
										["targets"] = {
											["Scarlet Medic"] = 2874,
											["Scarlet Captain"] = 1946,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4820,
										["n_min"] = 932,
										["g_dmg"] = 0,
										["counter"] = 5,
										["total"] = 4820,
										["c_max"] = 0,
										["id"] = 222026,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 5,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66198] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 2265,
										["g_amt"] = 0,
										["n_max"] = 584,
										["targets"] = {
											["Scarlet Medic"] = 584,
											["Scarlet Captain"] = 2265,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 584,
										["n_min"] = 584,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 2849,
										["c_max"] = 1145,
										["id"] = 66198,
										["r_dmg"] = 0,
										["c_min"] = 1120,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 338,
										["targets"] = {
											["Scarlet Medic"] = 338,
											["Scarlet Captain"] = 338,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 676,
										["n_min"] = 338,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 676,
										["c_max"] = 0,
										["id"] = 49184,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["last_dps"] = 1212.85886616291,
							["end_time"] = 1530841725,
							["colocacao"] = 1,
							["last_event"] = 1530841725,
							["friendlyfire"] = {
							},
							["start_time"] = 1530841710,
							["serial"] = "Player-76-09B75621",
							["friendlyfire_total"] = 0,
						}, -- [1]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 15,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "DEATHKNIGHT",
							["totalover"] = 0.008529,
							["total_without_pet"] = 760.008529,
							["total"] = 760.008529,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09B75621",
							["totalabsorb"] = 0.008529,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrdin"] = 760,
							},
							["totalover_without_pet"] = 0.008529,
							["healing_taken"] = 760.008529,
							["fight_component"] = true,
							["end_time"] = 1530841725,
							["heal_enemy_amt"] = 0,
							["nome"] = "Thorrdin",
							["spells"] = {
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
										},
										["n_max"] = 190,
										["targets"] = {
											["Thorrdin"] = 760,
										},
										["n_min"] = 190,
										["counter"] = 4,
										["overheal"] = 0,
										["total"] = 760,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["c_min"] = 0,
										["totaldenied"] = 0,
										["n_amt"] = 4,
										["n_curado"] = 760,
										["m_amt"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["healing_from"] = {
								["Thorrdin"] = true,
							},
							["tipo"] = 2,
							["custom"] = 0,
							["last_event"] = 1530841725,
							["totaldenied"] = 0.008529,
							["start_time"] = 1530841718,
							["delay"] = 0,
							["spec"] = 251,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 15,
					["_ActorTable"] = {
						{
							["received"] = 62.006313,
							["resource"] = 0.006313,
							["targets"] = {
								["Thorrdin"] = 62,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "DEATHKNIGHT",
							["fight_component"] = true,
							["total"] = 62.006313,
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["tipo"] = 3,
							["last_event"] = 1530841731,
							["alternatepower"] = 0.006313,
							["spells"] = {
								["_ActorTable"] = {
									[193486] = {
										["id"] = 193486,
										["total"] = 2,
										["targets"] = {
											["Thorrdin"] = 2,
										},
										["counter"] = 2,
									},
									[49020] = {
										["id"] = 49020,
										["total"] = 40,
										["targets"] = {
											["Thorrdin"] = 40,
										},
										["counter"] = 2,
									},
									[49184] = {
										["id"] = 49184,
										["total"] = 20,
										["targets"] = {
											["Thorrdin"] = 20,
										},
										["counter"] = 2,
									},
								},
								["tipo"] = 7,
							},
							["serial"] = "Player-76-09B75621",
							["flag_original"] = 1297,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 15,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[55095] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 55095,
										["uptime"] = 12,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 15,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[51915] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 51915,
										["uptime"] = 15,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 12,
							["buff_uptime_targets"] = {
							},
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49143] = 5,
								[49020] = 3,
							},
							["tipo"] = 4,
							["last_event"] = 1530841725,
							["nome"] = "Thorrdin",
							["pets"] = {
							},
							["serial"] = "Player-76-09B75621",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 15,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrdin"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Scarlet Medic",
				["combat_counter"] = 25,
				["playing_solo"] = true,
				["totals"] = {
					18939.964923, -- [1]
					759.995285, -- [2]
					{
						0, -- [1]
						[0] = 61.998321,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorrdin"] = 18940.004054,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrdin"] = 760.008529,
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 11096.537,
				["combat_id"] = 15,
				["TimeData"] = {
				},
				["hasSaved"] = true,
				["frags"] = {
					["Scarlet Peasant"] = 3,
					["Scarlet Medic"] = 1,
					["Scarlet Captain"] = 1,
				},
				["data_fim"] = "21:48:46",
				["data_inicio"] = "21:48:30",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					18940, -- [1]
					760, -- [2]
					{
						0, -- [1]
						[0] = 62,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 11080.921,
				["contra"] = "Scarlet Medic",
				["instance_type"] = "none",
			}, -- [2]
			{
				{
					["tipo"] = 2,
					["combatId"] = 14,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.001947,
							["damage_from"] = {
								["Scarlet Infantryman"] = true,
							},
							["targets"] = {
								["Scarlet Peasant"] = 3065,
								["Scarlet Infantryman"] = 8824,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 11889.001947,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 11889.001947,
							["damage_taken"] = 624.001947,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 649,
										["g_amt"] = 0,
										["n_max"] = 216,
										["targets"] = {
											["Scarlet Peasant"] = 966,
											["Scarlet Infantryman"] = 1227,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1544,
										["n_min"] = 97,
										["g_dmg"] = 0,
										["counter"] = 12,
										["total"] = 2193,
										["c_max"] = 439,
										["id"] = 1,
										["r_dmg"] = 0,
										["c_min"] = 210,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 10,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									}, -- [1]
									[66196] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1012,
										["g_amt"] = 0,
										["n_max"] = 482,
										["targets"] = {
											["Scarlet Infantryman"] = 1494,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 482,
										["n_min"] = 482,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1494,
										["c_max"] = 1012,
										["id"] = 66196,
										["r_dmg"] = 0,
										["c_min"] = 1012,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222024] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1166,
										["targets"] = {
											["Scarlet Peasant"] = 1166,
											["Scarlet Infantryman"] = 2307,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3473,
										["n_min"] = 1142,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 3473,
										["c_max"] = 0,
										["id"] = 222024,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 338,
										["targets"] = {
											["Scarlet Peasant"] = 338,
											["Scarlet Infantryman"] = 338,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 676,
										["n_min"] = 338,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 676,
										["c_max"] = 0,
										["id"] = 49184,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222026] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 987,
										["targets"] = {
											["Scarlet Infantryman"] = 1915,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1915,
										["n_min"] = 928,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1915,
										["c_max"] = 0,
										["id"] = 222026,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66198] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1093,
										["g_amt"] = 0,
										["n_max"] = 595,
										["targets"] = {
											["Scarlet Peasant"] = 595,
											["Scarlet Infantryman"] = 1093,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 595,
										["n_min"] = 595,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1688,
										["c_max"] = 1093,
										["id"] = 66198,
										["r_dmg"] = 0,
										["c_min"] = 1093,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 225,
										["targets"] = {
											["Scarlet Infantryman"] = 450,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 450,
										["n_min"] = 225,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 450,
										["c_max"] = 0,
										["id"] = 55095,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["last_dps"] = 804.289131849489,
							["end_time"] = 1530841672,
							["colocacao"] = 1,
							["last_event"] = 1530841671,
							["friendlyfire"] = {
							},
							["start_time"] = 1530841657,
							["serial"] = "Player-76-09B75621",
							["friendlyfire_total"] = 0,
						}, -- [1]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 14,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["targets_overheal"] = {
								["Thorrdin"] = 379,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "DEATHKNIGHT",
							["totalover"] = 379.004597,
							["total_without_pet"] = 380.004597,
							["total"] = 380.004597,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09B75621",
							["totalabsorb"] = 0.004597,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrdin"] = 380,
							},
							["totalover_without_pet"] = 0.004597,
							["healing_taken"] = 380.004597,
							["fight_component"] = true,
							["end_time"] = 1530841672,
							["heal_enemy_amt"] = 0,
							["nome"] = "Thorrdin",
							["spells"] = {
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorrdin"] = 379,
										},
										["n_max"] = 190,
										["targets"] = {
											["Thorrdin"] = 380,
										},
										["n_min"] = 190,
										["counter"] = 4,
										["overheal"] = 379,
										["total"] = 380,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["m_healed"] = 0,
										["c_min"] = 0,
										["totaldenied"] = 0,
										["n_amt"] = 4,
										["n_curado"] = 380,
										["m_amt"] = 0,
										["absorbed"] = 0,
									},
								},
								["tipo"] = 3,
							},
							["grupo"] = true,
							["healing_from"] = {
								["Thorrdin"] = true,
							},
							["tipo"] = 2,
							["custom"] = 0,
							["last_event"] = 1530841671,
							["totaldenied"] = 0.004597,
							["start_time"] = 1530841671,
							["delay"] = 1530841660,
							["spec"] = 251,
						}, -- [1]
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 14,
					["_ActorTable"] = {
						{
							["received"] = 104.003034,
							["resource"] = 0.003034,
							["targets"] = {
								["Thorrdin"] = 104,
							},
							["pets"] = {
							},
							["powertype"] = 6,
							["classe"] = "DEATHKNIGHT",
							["fight_component"] = true,
							["total"] = 104.003034,
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["tipo"] = 3,
							["last_event"] = 1530841710,
							["alternatepower"] = 0.003034,
							["spells"] = {
								["_ActorTable"] = {
									[49184] = {
										["id"] = 49184,
										["total"] = 33,
										["targets"] = {
											["Thorrdin"] = 33,
										},
										["counter"] = 4,
									},
									[193486] = {
										["id"] = 193486,
										["total"] = 1,
										["targets"] = {
											["Thorrdin"] = 1,
										},
										["counter"] = 1,
									},
									[195617] = {
										["id"] = 195617,
										["total"] = 10,
										["targets"] = {
											["Thorrdin"] = 10,
										},
										["counter"] = 2,
									},
									[49020] = {
										["id"] = 49020,
										["total"] = 60,
										["targets"] = {
											["Thorrdin"] = 60,
										},
										["counter"] = 3,
									},
								},
								["tipo"] = 7,
							},
							["serial"] = "Player-76-09B75621",
							["flag_original"] = 1297,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 14,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[55095] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 55095,
										["uptime"] = 12,
										["targets"] = {
										},
										["appliedamt"] = 2,
									},
								},
								["tipo"] = 9,
							},
							["buff_uptime"] = 15,
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[51915] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 51915,
										["uptime"] = 15,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["fight_component"] = true,
							["debuff_uptime"] = 12,
							["buff_uptime_targets"] = {
							},
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49020] = 3,
								[49143] = 2,
							},
							["tipo"] = 4,
							["last_event"] = 1530841672,
							["nome"] = "Thorrdin",
							["pets"] = {
							},
							["serial"] = "Player-76-09B75621",
							["debuff_uptime_targets"] = {
							},
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 14,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrdin"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Scarlet Infantryman",
				["combat_counter"] = 21,
				["playing_solo"] = true,
				["totals"] = {
					11888.971645, -- [1]
					379.99854, -- [2]
					{
						0, -- [1]
						[0] = 100.995853,
						["alternatepower"] = 0,
						[6] = 3,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorrdin"] = 22232.016078,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrdin"] = 690.009216,
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 11043.165,
				["combat_id"] = 14,
				["TimeData"] = {
				},
				["resincked"] = true,
				["hasSaved"] = true,
				["frags"] = {
					["Scarlet Peasant"] = 1,
					["Scarlet Infantryman"] = 1,
				},
				["data_fim"] = "21:47:53",
				["data_inicio"] = "21:47:38",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					11889, -- [1]
					380, -- [2]
					{
						0, -- [1]
						[0] = 101,
						["alternatepower"] = 0,
						[6] = 3,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 11028.383,
				["contra"] = "Scarlet Peasant",
				["instance_type"] = "none",
			}, -- [3]
			{
				{
					["tipo"] = 2,
					["combatId"] = 13,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003559,
							["damage_from"] = {
								["Death Knight Initiate"] = true,
							},
							["targets"] = {
								["Death Knight Initiate"] = 11273,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 11273.003559,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 11273.003559,
							["damage_taken"] = 1613.003559,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 221,
										["targets"] = {
											["Death Knight Initiate"] = 981,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 981,
										["n_min"] = 104,
										["g_dmg"] = 0,
										["counter"] = 8,
										["total"] = 981,
										["c_max"] = 0,
										["c_min"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 6,
										["spellschool"] = 1,
										["MISS"] = 2,
									}, -- [1]
									[50401] = {
										["c_amt"] = 4,
										["b_amt"] = 0,
										["c_dmg"] = 354,
										["g_amt"] = 0,
										["n_max"] = 46,
										["targets"] = {
											["Death Knight Initiate"] = 796,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 442,
										["n_min"] = 38,
										["g_dmg"] = 0,
										["counter"] = 14,
										["total"] = 796,
										["c_max"] = 93,
										["id"] = 50401,
										["r_dmg"] = 0,
										["c_min"] = 84,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 10,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66196] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 620,
										["targets"] = {
											["Death Knight Initiate"] = 1200,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1200,
										["n_min"] = 580,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1200,
										["c_max"] = 0,
										["id"] = 66196,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222024] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1232,
										["targets"] = {
											["Death Knight Initiate"] = 2403,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2403,
										["n_min"] = 1171,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2403,
										["c_max"] = 0,
										["id"] = 222024,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[55095] = {
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 1104,
										["g_amt"] = 0,
										["n_max"] = 276,
										["targets"] = {
											["Death Knight Initiate"] = 1380,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 276,
										["n_min"] = 276,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1380,
										["c_max"] = 552,
										["id"] = 55095,
										["r_dmg"] = 0,
										["c_min"] = 552,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222026] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1173,
										["targets"] = {
											["Death Knight Initiate"] = 2318,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2318,
										["n_min"] = 1145,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2318,
										["c_max"] = 0,
										["id"] = 222026,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66198] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 1240,
										["g_amt"] = 0,
										["n_max"] = 595,
										["targets"] = {
											["Death Knight Initiate"] = 1835,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 595,
										["n_min"] = 595,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1835,
										["c_max"] = 1240,
										["id"] = 66198,
										["r_dmg"] = 0,
										["c_min"] = 1240,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 360,
										["targets"] = {
											["Death Knight Initiate"] = 360,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 360,
										["n_min"] = 360,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 360,
										["c_max"] = 0,
										["id"] = 49184,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["last_dps"] = 754.097502107138,
							["end_time"] = 1530841477,
							["colocacao"] = 1,
							["last_event"] = 1530841472,
							["friendlyfire"] = {
							},
							["start_time"] = 1530841462,
							["serial"] = "Player-76-09B75621",
							["friendlyfire_total"] = 0,
						}, -- [1]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 13,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 13,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["resource"] = 0.004151,
							["targets"] = {
								["Thorrdin"] = 51,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "DEATHKNIGHT",
							["alternatepower"] = 0.004151,
							["nome"] = "Thorrdin",
							["spells"] = {
								["_ActorTable"] = {
									[193486] = {
										["id"] = 193486,
										["total"] = 1,
										["targets"] = {
											["Thorrdin"] = 1,
										},
										["counter"] = 1,
									},
									[49020] = {
										["id"] = 49020,
										["total"] = 40,
										["targets"] = {
											["Thorrdin"] = 40,
										},
										["counter"] = 2,
									},
									[49184] = {
										["id"] = 49184,
										["total"] = 10,
										["targets"] = {
											["Thorrdin"] = 10,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["received"] = 51.004151,
							["last_event"] = 1530841657,
							["total"] = 51.004151,
							["tipo"] = 3,
							["serial"] = "Player-76-09B75621",
							["spec"] = 251,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 13,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[51714] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 4,
										["id"] = 51714,
										["uptime"] = 10,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[55095] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 55095,
										["uptime"] = 9,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["pets"] = {
							},
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[51915] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 51915,
										["uptime"] = 15,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["debuff_uptime"] = 19,
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49576] = 1,
								[49020] = 2,
								[49143] = 2,
							},
							["tipo"] = 4,
							["last_event"] = 1530841477,
							["buff_uptime_targets"] = {
							},
							["debuff_uptime_targets"] = {
							},
							["serial"] = "Player-76-09B75621",
							["buff_uptime"] = 15,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 13,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrdin"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Death Knight Initiate",
				["combat_counter"] = 20,
				["playing_solo"] = true,
				["totals"] = {
					11272.968736, -- [1]
					-0.0084159999998974, -- [2]
					{
						0, -- [1]
						[0] = 50.987366,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = false,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorrdin"] = 11273.003559,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 10848.763,
				["combat_id"] = 13,
				["TimeData"] = {
				},
				["hasSaved"] = true,
				["frags"] = {
				},
				["data_fim"] = "21:44:38",
				["data_inicio"] = "21:44:23",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					11273, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 51,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 10833.814,
				["contra"] = "Death Knight Initiate",
				["instance_type"] = "none",
			}, -- [4]
			{
				{
					["tipo"] = 2,
					["combatId"] = 12,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.007698,
							["damage_from"] = {
								["Death Knight Initiate"] = true,
							},
							["targets"] = {
								["Death Knight Initiate"] = 11383,
							},
							["delay"] = 0,
							["pets"] = {
							},
							["custom"] = 0,
							["tipo"] = 1,
							["classe"] = "DEATHKNIGHT",
							["raid_targets"] = {
							},
							["total_without_pet"] = 11383.007698,
							["on_hold"] = false,
							["dps_started"] = false,
							["total"] = 11383.007698,
							["damage_taken"] = 1918.007698,
							["spells"] = {
								["_ActorTable"] = {
									{
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 216,
										["g_amt"] = 0,
										["n_max"] = 222,
										["targets"] = {
											["Death Knight Initiate"] = 860,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 644,
										["n_min"] = 104,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 860,
										["c_max"] = 216,
										["c_min"] = 216,
										["id"] = 1,
										["r_dmg"] = 0,
										["r_amt"] = 0,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["b_dmg"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["a_amt"] = 0,
										["n_amt"] = 4,
										["spellschool"] = 1,
										["MISS"] = 1,
									}, -- [1]
									[50401] = {
										["c_amt"] = 1,
										["b_amt"] = 0,
										["c_dmg"] = 84,
										["g_amt"] = 0,
										["n_max"] = 48,
										["targets"] = {
											["Death Knight Initiate"] = 705,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 621,
										["n_min"] = 38,
										["g_dmg"] = 0,
										["counter"] = 15,
										["total"] = 705,
										["c_max"] = 84,
										["id"] = 50401,
										["r_dmg"] = 0,
										["c_min"] = 84,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 14,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66196] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 621,
										["targets"] = {
											["Death Knight Initiate"] = 1196,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1196,
										["n_min"] = 575,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 1196,
										["c_max"] = 0,
										["id"] = 66196,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222024] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1239,
										["targets"] = {
											["Death Knight Initiate"] = 3606,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 3606,
										["n_min"] = 1150,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 3606,
										["c_max"] = 0,
										["id"] = 222024,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[55095] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 276,
										["targets"] = {
											["Death Knight Initiate"] = 552,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 552,
										["n_min"] = 276,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 552,
										["c_max"] = 0,
										["id"] = 55095,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[222026] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 1228,
										["targets"] = {
											["Death Knight Initiate"] = 2368,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 2368,
										["n_min"] = 1140,
										["g_dmg"] = 0,
										["counter"] = 2,
										["total"] = 2368,
										["c_max"] = 0,
										["id"] = 222026,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 2,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
									[66198] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 591,
										["targets"] = {
											["Death Knight Initiate"] = 1736,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 1736,
										["n_min"] = 567,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 1736,
										["c_max"] = 0,
										["id"] = 66198,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["a_dmg"] = 0,
										["spellschool"] = 1,
									},
									[49184] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 360,
										["targets"] = {
											["Death Knight Initiate"] = 360,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 360,
										["n_min"] = 360,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 360,
										["c_max"] = 0,
										["id"] = 49184,
										["r_dmg"] = 0,
										["c_min"] = 0,
										["r_amt"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["a_dmg"] = 0,
										["spellschool"] = 16,
									},
								},
								["tipo"] = 2,
							},
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["last_dps"] = 1332.59280004663,
							["end_time"] = 1530841418,
							["colocacao"] = 1,
							["last_event"] = 1530841418,
							["friendlyfire"] = {
							},
							["start_time"] = 1530841410,
							["serial"] = "Player-76-09B75621",
							["friendlyfire_total"] = 0,
						}, -- [1]
					},
				}, -- [1]
				{
					["tipo"] = 3,
					["combatId"] = 12,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["tipo"] = 7,
					["combatId"] = 12,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["resource"] = 0.007967,
							["targets"] = {
								["Thorrdin"] = 81,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "DEATHKNIGHT",
							["alternatepower"] = 0.007967,
							["nome"] = "Thorrdin",
							["spells"] = {
								["_ActorTable"] = {
									[49184] = {
										["id"] = 49184,
										["total"] = 20,
										["targets"] = {
											["Thorrdin"] = 20,
										},
										["counter"] = 2,
									},
									[49020] = {
										["id"] = 49020,
										["total"] = 60,
										["targets"] = {
											["Thorrdin"] = 60,
										},
										["counter"] = 3,
									},
									[193486] = {
										["id"] = 193486,
										["total"] = 1,
										["targets"] = {
											["Thorrdin"] = 1,
										},
										["counter"] = 1,
									},
								},
								["tipo"] = 7,
							},
							["grupo"] = true,
							["received"] = 81.007967,
							["last_event"] = 1530841462,
							["total"] = 81.007967,
							["tipo"] = 3,
							["serial"] = "Player-76-09B75621",
							["spec"] = 251,
						}, -- [1]
					},
				}, -- [3]
				{
					["tipo"] = 9,
					["combatId"] = 12,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["debuff_uptime_spells"] = {
								["_ActorTable"] = {
									[51714] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 4,
										["id"] = 51714,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
									[55095] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 0,
										["refreshamt"] = 0,
										["id"] = 55095,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["pets"] = {
							},
							["classe"] = "DEATHKNIGHT",
							["buff_uptime_spells"] = {
								["_ActorTable"] = {
									[51915] = {
										["counter"] = 0,
										["actived"] = false,
										["activedamt"] = 1,
										["refreshamt"] = 0,
										["id"] = 51915,
										["uptime"] = 8,
										["targets"] = {
										},
										["appliedamt"] = 1,
									},
								},
								["tipo"] = 9,
							},
							["debuff_uptime"] = 16,
							["nome"] = "Thorrdin",
							["spec"] = 251,
							["grupo"] = true,
							["spell_cast"] = {
								[49184] = 1,
								[49020] = 3,
								[49143] = 2,
							},
							["tipo"] = 4,
							["last_event"] = 1530841418,
							["buff_uptime_targets"] = {
							},
							["debuff_uptime_targets"] = {
							},
							["serial"] = "Player-76-09B75621",
							["buff_uptime"] = 8,
						}, -- [1]
					},
				}, -- [4]
				{
					["tipo"] = 2,
					["combatId"] = 12,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrdin"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Death Knight Initiate",
				["combat_counter"] = 19,
				["playing_solo"] = true,
				["totals"] = {
					11382.977684, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 80.994953,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
					["voidzone_damage"] = 0,
					["frags_total"] = 0,
				},
				["player_last_events"] = {
					["Thorrdin"] = {
						{
							true, -- [1]
							52373, -- [2]
							73, -- [3]
							1530841420.134, -- [4]
							7209, -- [5]
							"Death Knight Initiate", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [1]
						{
							true, -- [1]
							52373, -- [2]
							73, -- [3]
							1530841423.07, -- [4]
							7232, -- [5]
							"Death Knight Initiate", -- [6]
							nil, -- [7]
							32, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [2]
						{
							true, -- [1]
							1, -- [2]
							248, -- [3]
							1530841463.229, -- [4]
							9155, -- [5]
							"Death Knight Initiate", -- [6]
							nil, -- [7]
							1, -- [8]
							false, -- [9]
							-1, -- [10]
						}, -- [3]
						{
						}, -- [4]
						{
						}, -- [5]
						{
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						{
						}, -- [9]
						{
						}, -- [10]
						{
						}, -- [11]
						{
						}, -- [12]
						{
						}, -- [13]
						{
						}, -- [14]
						{
						}, -- [15]
						{
						}, -- [16]
						{
						}, -- [17]
						{
						}, -- [18]
						{
						}, -- [19]
						{
						}, -- [20]
						{
						}, -- [21]
						{
						}, -- [22]
						{
						}, -- [23]
						{
						}, -- [24]
						{
						}, -- [25]
						{
						}, -- [26]
						{
						}, -- [27]
						{
						}, -- [28]
						{
						}, -- [29]
						{
						}, -- [30]
						{
						}, -- [31]
						{
						}, -- [32]
						["n"] = 4,
					},
				},
				["frags_need_refresh"] = false,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["damage"] = {
						{
							["Thorrdin"] = 11383.007698,
						}, -- [1]
					},
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
				},
				["end_time"] = 10789.701,
				["combat_id"] = 12,
				["TimeData"] = {
				},
				["hasSaved"] = true,
				["frags"] = {
				},
				["data_fim"] = "21:43:39",
				["data_inicio"] = "21:43:31",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					11383, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 81,
						["alternatepower"] = 0,
						[6] = 0,
						[3] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["dead"] = 0,
						["cc_break"] = 0,
						["interrupt"] = 0,
						["debuff_uptime"] = 0,
						["dispell"] = 0,
						["cooldowns_defensive"] = 0,
					}, -- [4]
				},
				["start_time"] = 10781.159,
				["contra"] = "Death Knight Initiate",
				["instance_type"] = "none",
			}, -- [5]
		},
	},
	["last_version"] = "v7.3.5.5572",
	["character_data"] = {
		["logons"] = 5,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -73.3999633789063,
					["x"] = -538.800003051758,
					["w"] = 149.999969482422,
					["h"] = 96.0000152587891,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09B75621"] = {
		},
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorrdin-Sargeras",
	["last_realversion"] = 130,
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["animate"] = false,
			["showamount"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_DAMAGE_RANK"] = {
			["lasttry"] = {
			},
			["annouce"] = true,
			["dpshistory"] = {
			},
			["dps"] = 0,
			["author"] = "Details! Team",
			["level"] = 1,
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_TIME_ATTACK"] = {
			["enabled"] = true,
			["realm_last_shown"] = 40,
			["saved_as_anonymous"] = true,
			["recently_as_anonymous"] = true,
			["dps"] = 0,
			["disable_sharing"] = false,
			["history"] = {
			},
			["time"] = 40,
			["history_lastindex"] = 0,
			["realm_lastamt"] = 0,
			["realm_history"] = {
			},
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_DPS_TUNING"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["SpellBarsShowType"] = 1,
		},
		["DETAILS_PLUGIN_CHART_VIEWER"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
			["tabs"] = {
				{
					["name"] = "Your Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Player Damage Done",
					["texture"] = "line",
				}, -- [1]
				{
					["name"] = "Class Damage",
					["iType"] = "raid-DAMAGER",
					["segment_type"] = 1,
					["version"] = "v2.0",
					["data"] = "PRESET_DAMAGE_SAME_CLASS",
					["texture"] = "line",
				}, -- [2]
				{
					["name"] = "Raid Damage",
					["segment_type"] = 2,
					["version"] = "v2.0",
					["data"] = "Raid Damage Done",
					["texture"] = "line",
				}, -- [3]
				["last_selected"] = 1,
			},
			["options"] = {
				["auto_create"] = true,
				["show_method"] = 4,
				["window_scale"] = 1,
			},
		},
	},
	["nick_tag_cache"] = {
		["nextreset"] = 1532136318,
		["last_version"] = 10,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_day"] = "06",
	["combat_counter"] = 27,
	["combat_id"] = 16,
	["savedStyles"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.005971,
					["damage_from"] = {
					},
					["targets"] = {
						["Scarlet Captain"] = 0,
						["Scarlet Infantryman"] = 0,
						["Scarlet Peasant"] = 0,
						["Death Knight Initiate"] = 0,
						["Scarlet Medic"] = 0,
						["Unworthy Initiate"] = 0,
					},
					["pets"] = {
					},
					["damage_taken"] = 0.005971,
					["tipo"] = 1,
					["classe"] = "DEATHKNIGHT",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.005971,
					["delay"] = 0,
					["dps_started"] = false,
					["end_time"] = 1530840318,
					["spells"] = {
						["_ActorTable"] = {
							{
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Death Knight Initiate"] = 0,
									["Scarlet Medic"] = 0,
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 1,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							}, -- [1]
							[49184] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Scarlet Medic"] = 0,
									["Death Knight Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 49184,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[47632] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 47632,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							[55095] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Scarlet Medic"] = 0,
									["Death Knight Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 55095,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[70890] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 70890,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							[66196] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Scarlet Medic"] = 0,
									["Death Knight Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 66196,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[66198] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Scarlet Medic"] = 0,
									["Death Knight Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 66198,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[55090] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 55090,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							[222026] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Scarlet Medic"] = 0,
									["Death Knight Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 222026,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[222024] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Captain"] = 0,
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
									["Scarlet Medic"] = 0,
									["Death Knight Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 222024,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[194311] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 194311,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							[50401] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Death Knight Initiate"] = 0,
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 50401,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
							[237680] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Scarlet Infantryman"] = 0,
									["Scarlet Peasant"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 237680,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[85948] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
									["Unworthy Initiate"] = 0,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 85948,
								["r_dmg"] = 0,
								["r_amt"] = 0,
								["m_crit"] = 0,
								["m_amt"] = 0,
								["c_min"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["a_amt"] = 0,
								["a_dmg"] = 0,
							},
						},
						["tipo"] = 2,
					},
					["last_dps"] = 0,
					["nome"] = "Thorrdin",
					["spec"] = 252,
					["grupo"] = true,
					["total"] = 0.005971,
					["friendlyfire_total"] = 0,
					["custom"] = 0,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["start_time"] = 1530840315,
					["serial"] = "Player-76-09B75621",
					["on_hold"] = false,
				}, -- [1]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorb"] = 0.002616,
					["last_hps"] = 0,
					["healing_from"] = {
					},
					["targets_overheal"] = {
						["Thorrdin"] = 0,
					},
					["targets"] = {
						["Thorrdin"] = 0,
					},
					["spells"] = {
						["tipo"] = 3,
						["_ActorTable"] = {
							[59913] = {
								["c_amt"] = 0,
								["totalabsorb"] = 0,
								["targets_overheal"] = {
									["Thorrdin"] = 0,
								},
								["n_max"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["n_min"] = 0,
								["counter"] = 0,
								["overheal"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 59913,
								["targets_absorbs"] = {
								},
								["c_curado"] = 0,
								["m_crit"] = 0,
								["c_min"] = 0,
								["m_amt"] = 0,
								["n_curado"] = 0,
								["n_amt"] = 0,
								["totaldenied"] = 0,
								["m_healed"] = 0,
								["absorbed"] = 0,
							},
						},
					},
					["pets"] = {
					},
					["iniciar_hps"] = false,
					["totalover_without_pet"] = 0.002616,
					["healing_taken"] = 0.002616,
					["totalover"] = 0.002616,
					["total_without_pet"] = 0.002616,
					["spec"] = 251,
					["classe"] = "DEATHKNIGHT",
					["fight_component"] = true,
					["end_time"] = 1530913712,
					["heal_enemy_amt"] = 0,
					["total"] = 0.002616,
					["nome"] = "Thorrdin",
					["targets_absorbs"] = {
					},
					["grupo"] = true,
					["start_time"] = 1530913709,
					["heal_enemy"] = {
					},
					["serial"] = "Player-76-09B75621",
					["custom"] = 0,
					["last_event"] = 0,
					["on_hold"] = false,
					["totaldenied"] = 0.002616,
					["delay"] = 0,
					["tipo"] = 2,
				}, -- [1]
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["resource"] = 0.050472,
					["targets"] = {
						["Thorrdin"] = 0,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "DEATHKNIGHT",
					["alternatepower"] = 0.006877,
					["nome"] = "Thorrdin",
					["spells"] = {
						["_ActorTable"] = {
							[195757] = {
								["id"] = 195757,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
							[193486] = {
								["id"] = 193486,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
							[195617] = {
								["id"] = 195617,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
							[55090] = {
								["id"] = 55090,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
							[85948] = {
								["id"] = 85948,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
							[49020] = {
								["id"] = 49020,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
							[49184] = {
								["id"] = 49184,
								["total"] = 0,
								["targets"] = {
									["Thorrdin"] = 0,
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 7,
					},
					["grupo"] = true,
					["received"] = 0.006877,
					["tipo"] = 3,
					["total"] = 0.006877,
					["spec"] = 252,
					["serial"] = "Player-76-09B75621",
					["last_event"] = 0,
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["debuff_uptime_spells"] = {
						["_ActorTable"] = {
							[194310] = {
								["id"] = 194310,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[51714] = {
								["id"] = 51714,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[55095] = {
								["id"] = 55095,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["pets"] = {
					},
					["classe"] = "DEATHKNIGHT",
					["buff_uptime_spells"] = {
						["_ActorTable"] = {
							[51915] = {
								["id"] = 51915,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[51460] = {
								["id"] = 51460,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
						["tipo"] = 9,
					},
					["debuff_uptime"] = 0,
					["nome"] = "Thorrdin",
					["spec"] = 252,
					["grupo"] = true,
					["spell_cast"] = {
						[49020] = 0,
						[49143] = 0,
						[49576] = 0,
						[55090] = 0,
						[85948] = 0,
						[47541] = 0,
						[49184] = 0,
					},
					["debuff_uptime_targets"] = {
					},
					["last_event"] = 0,
					["buff_uptime_targets"] = {
					},
					["tipo"] = 4,
					["serial"] = "Player-76-09B75621",
					["buff_uptime"] = 0,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["totals_grupo"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["cooldowns_defensive"] = 0,
				["dispell"] = 0,
				["interrupt"] = 0,
				["debuff_uptime"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["damage_section"] = {
			},
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage"] = {
			},
		},
		["hasSaved"] = true,
		["frags"] = {
		},
		["data_fim"] = 0,
		["data_inicio"] = 0,
		["CombatSkillCache"] = {
		},
		["player_last_events"] = {
		},
		["start_time"] = 0,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["overall_refreshed"] = true,
	},
	["force_font_outline"] = "",
	["SoloTablesSaved"] = {
		["LastSelected"] = "DETAILS_PLUGIN_DAMAGE_RANK",
		["Mode"] = 1,
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09B75621"] = 251,
	},
}
