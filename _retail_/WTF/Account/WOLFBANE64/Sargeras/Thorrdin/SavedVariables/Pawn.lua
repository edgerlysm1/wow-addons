
PawnOptions = {
	["LastVersion"] = 2.0218,
	["LastPlayerFullName"] = "Thorrdin-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DEATHKNIGHT",
	["LastAdded"] = 1,
}
