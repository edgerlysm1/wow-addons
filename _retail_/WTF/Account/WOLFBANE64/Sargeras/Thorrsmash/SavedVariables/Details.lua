
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.003679,
							["damage_from"] = {
								["Mottled Boar"] = true,
							},
							["targets"] = {
								["Mottled Boar"] = 36,
							},
							["total"] = 36.003679,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "WARRIOR",
							["raid_targets"] = {
							},
							["total_without_pet"] = 36.003679,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1533779155,
							["friendlyfire_total"] = 0,
							["spec"] = 71,
							["nome"] = "Thorrsmash",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 16,
										["targets"] = {
											["Mottled Boar"] = 16,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 16,
										["n_min"] = 16,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 16,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
									[1464] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 20,
										["targets"] = {
											["Mottled Boar"] = 20,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 20,
										["n_min"] = 20,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 20,
										["c_max"] = 0,
										["id"] = 1464,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09DC0992",
							["last_dps"] = 16.5915571428427,
							["custom"] = 0,
							["last_event"] = 1533779154,
							["damage_taken"] = 4.003679,
							["start_time"] = 1533779153,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["healing_from"] = {
								["Thorrsmash"] = true,
							},
							["pets"] = {
							},
							["iniciar_hps"] = false,
							["classe"] = "WARRIOR",
							["totalover"] = 6.004342,
							["total_without_pet"] = 4.004342,
							["total"] = 4.004342,
							["targets_absorbs"] = {
							},
							["heal_enemy"] = {
							},
							["on_hold"] = false,
							["serial"] = "Player-76-09DC0992",
							["totalabsorb"] = 0.004342,
							["last_hps"] = 0,
							["targets"] = {
								["Thorrsmash"] = 5,
							},
							["totalover_without_pet"] = 0.004342,
							["healing_taken"] = 4.004342,
							["fight_component"] = true,
							["end_time"] = 1533779155,
							["targets_overheal"] = {
								["Thorrsmash"] = 6,
							},
							["nome"] = "Thorrsmash",
							["spells"] = {
								["tipo"] = 3,
								["_ActorTable"] = {
									[59913] = {
										["c_amt"] = 0,
										["totalabsorb"] = 0,
										["targets_overheal"] = {
											["Thorrsmash"] = 6,
										},
										["n_max"] = 4,
										["targets"] = {
											["Thorrsmash"] = 4,
										},
										["n_min"] = 0,
										["counter"] = 2,
										["overheal"] = 6,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 59913,
										["targets_absorbs"] = {
										},
										["c_curado"] = 0,
										["m_crit"] = 0,
										["c_min"] = 0,
										["m_amt"] = 0,
										["n_curado"] = 4,
										["n_amt"] = 2,
										["totaldenied"] = 0,
										["m_healed"] = 0,
										["absorbed"] = 0,
									},
								},
							},
							["grupo"] = true,
							["heal_enemy_amt"] = 0,
							["start_time"] = 1533779154,
							["custom"] = 0,
							["last_event"] = 1533779154,
							["spec"] = 71,
							["totaldenied"] = 0.004342,
							["delay"] = 0,
							["tipo"] = 2,
						}, -- [1]
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 3.008369,
							["resource"] = 0.008369,
							["targets"] = {
								["Thorrsmash"] = 3,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "WARRIOR",
							["fight_component"] = true,
							["total"] = 3.008369,
							["nome"] = "Thorrsmash",
							["spec"] = 71,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1533779153,
							["alternatepower"] = 0.008369,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[195707] = {
										["id"] = 195707,
										["total"] = 3,
										["targets"] = {
											["Thorrsmash"] = 3,
										},
										["counter"] = 1,
									},
								},
							},
							["serial"] = "Player-76-09DC0992",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["fight_component"] = true,
							["nome"] = "Thorrsmash",
							["spec"] = 71,
							["grupo"] = true,
							["spell_cast"] = {
								[1464] = 1,
							},
							["flag_original"] = 1297,
							["last_event"] = 0,
							["classe"] = "WARRIOR",
							["pets"] = {
							},
							["serial"] = "Player-76-09DC0992",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorrsmash"] = true,
				},
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Mottled Boar",
				["combat_counter"] = 6,
				["playing_solo"] = true,
				["totals"] = {
					35.996317, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
							["Thorrsmash"] = 33.056853,
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorrsmash"] = 523.062634,
						}, -- [1]
					},
				},
				["end_time"] = 30156.718,
				["combat_id"] = 1,
				["instance_type"] = "none",
				["resincked"] = true,
				["frags"] = {
					["Mottled Boar"] = 1,
				},
				["data_fim"] = "21:45:55",
				["data_inicio"] = "21:45:53",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					36, -- [1]
					4, -- [2]
					{
						0, -- [1]
						[0] = 3,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 30154.548,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
		},
	},
	["last_version"] = "v8.0.1.6035",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = -94.3516845703125,
					["x"] = -532.52751159668,
					["w"] = 309.999969482422,
					["h"] = 157.999984741211,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Thorrsmash-Sargeras",
	["last_realversion"] = 132,
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "08",
	["nick_tag_cache"] = {
		["nextreset"] = 1535074936,
		["last_version"] = 10,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["combat_id"] = 1,
	["savedStyles"] = {
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["combat_counter"] = 17,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 5,
		["totals"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["totals_grupo"] = {
			0, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["data_inicio"] = 0,
		["frags"] = {
		},
		["data_fim"] = 0,
		["CombatSkillCache"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["start_time"] = 0,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["player_last_events"] = {
		},
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["force_font_outline"] = "",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09DC0992"] = 71,
	},
}
