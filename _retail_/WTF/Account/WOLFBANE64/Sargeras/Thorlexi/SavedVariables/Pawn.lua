
PawnOptions = {
	["LastVersion"] = 2.033,
	["LastPlayerFullName"] = "Thorlexi-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 122250,
			["Level"] = 6,
			["Link"] = "|cff00ccff|Hitem:122250::::::::2:256:::1:3592:::|h[Tattered Dreadmist Mask]|h|r",
		}, -- [1]
		{
			["ID"] = 122664,
			["Level"] = 6,
			["Link"] = "|cff00ccff|Hitem:122664::::::::2:256:::1:3592:::|h[Eternal Horizon Choker]|h|r",
		}, -- [2]
		{
			["ID"] = 122378,
			["Level"] = 6,
			["Link"] = "|cff00ccff|Hitem:122378::::::::2:256:::1:3592:::|h[Exquisite Sunderseer Mantle]|h|r",
		}, -- [3]
		[13] = {
			["ID"] = 122362,
			["Level"] = 6,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122362::::::::2:256:::1:3592:::|h[Discerning Eye of the Beast]|h|r",
		},
		[7] = {
			["ID"] = 157733,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157733::::::::20:265::::::|h[Felburner's Leggings]|h|r",
		},
		[14] = {
			["ID"] = 122362,
			["Level"] = 6,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122362::::::::2:265:::1:3592:::|h[Discerning Eye of the Beast]|h|r",
		},
		[15] = {
			["ID"] = 122262,
			["Level"] = 6,
			["Link"] = "|cff00ccff|Hitem:122262::::::::2:256:::1:3592:::|h[Ancient Bloodmoon Cloak]|h|r",
		},
		[8] = {
			["ID"] = 157730,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157730::::::::20:265::::::|h[Felburner's Sandals]|h|r",
		},
		[16] = {
			["ID"] = 157652,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157652::::::::20:265::::::|h[Shadow-Binder's Spire]|h|r",
		},
		[17] = {
			["ID"] = 122367,
			["Level"] = 5,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cff00ccff|Hitem:122367::::::::1:256::::::|h[The Blessed Hammer of Grace]|h|r",
		},
		[5] = {
			["ID"] = 157734,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157734::::::::20:265::::::|h[Felburner's Robe]|h|r",
		},
		[10] = {
			["ID"] = 157731,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157731::::::::20:265::::::|h[Felburner's Handwraps]|h|r",
		},
		[11] = {
			["ID"] = 128169,
			["Level"] = 6,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff00ccff|Hitem:128169::::::::2:256:::1:3592:::|h[Signet of the Third Fleet]|h|r",
		},
		[6] = {
			["ID"] = 157736,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157736::::::::20:265::::::|h[Felburner's Cord]|h|r",
		},
		[12] = {
			["ID"] = 128169,
			["Level"] = 6,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff00ccff|Hitem:128169::::::::2:265:::1:3592:::|h[Signet of the Third Fleet]|h|r",
		},
		[9] = {
			["ID"] = 157737,
			["Level"] = 25,
			["Link"] = "|cff1eff00|Hitem:157737::::::::20:265::::::|h[Felburner's Wristwraps]|h|r",
		},
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "WARLOCK",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
