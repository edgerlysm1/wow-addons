
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 19,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008567,
							["total"] = 47.008567,
							["damage_from"] = {
							},
							["targets"] = {
								["Raider's Training Dummy"] = 47,
							},
							["pets"] = {
							},
							["spec"] = 267,
							["colocacao"] = 1,
							["aID"] = "76-0A4F2267",
							["raid_targets"] = {
							},
							["total_without_pet"] = 47.008567,
							["friendlyfire"] = {
							},
							["on_hold"] = false,
							["dps_started"] = false,
							["end_time"] = 1603114751,
							["friendlyfire_total"] = 0,
							["classe"] = "WARLOCK",
							["nome"] = "Thorlexi",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[29722] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 47,
										["targets"] = {
											["Raider's Training Dummy"] = 47,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 47,
										["n_min"] = 47,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 47,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 29722,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-0A4F2267",
							["last_dps"] = 7.994654251700532,
							["custom"] = 0,
							["last_event"] = 1603114745,
							["damage_taken"] = 0.008567,
							["start_time"] = 1603114745,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.003755,
							["classe"] = "UNKNOW",
							["damage_from"] = {
								["Thorlexi"] = true,
								["Sìlentblades"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["fight_component"] = true,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.003755,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["total"] = 0.003755,
							["aID"] = "31146",
							["serial"] = "Creature-0-3779-0-16989-31146-00008A8D34",
							["nome"] = "Raider's Training Dummy",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["end_time"] = 1603114751,
							["last_dps"] = 0,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 211.003755,
							["start_time"] = 1603114751,
							["delay"] = 0,
							["last_event"] = 0,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 19,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 19,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 19,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorlexi",
							["spec"] = 267,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 14,
							["pets"] = {
							},
							["aID"] = "76-0A4F2267",
							["last_event"] = 1603114751,
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[328136] = {
										["activedamt"] = 1,
										["id"] = 328136,
										["targets"] = {
										},
										["uptime"] = 6,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[313424] = {
										["activedamt"] = 1,
										["id"] = 313424,
										["targets"] = {
										},
										["uptime"] = 2,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0A4F2267",
							["classe"] = "WARLOCK",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 19,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorlexi"] = true,
				},
				["last_events_tables"] = {
				},
				["overall_added"] = true,
				["cleu_timeline"] = {
				},
				["alternate_power"] = {
				},
				["tempo_start"] = 1603114745,
				["enemy"] = "Raider's Training Dummy",
				["combat_counter"] = 36,
				["playing_solo"] = true,
				["totals"] = {
					46.99281200000002, -- [1]
					-0.002978, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = -0.007667999999995345,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["cleu_events"] = {
					["n"] = 1,
				},
				["CombatEndedAt"] = 6664.072,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["data_inicio"] = "09:39:06",
				["end_time"] = 6664.072,
				["totals_grupo"] = {
					47, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["combat_id"] = 19,
				["TotalElapsedCombatTime"] = 6664.072,
				["frags_need_refresh"] = false,
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorlexi"] = 47.008567,
						}, -- [1]
					},
				},
				["frags"] = {
				},
				["data_fim"] = "09:39:12",
				["instance_type"] = "none",
				["CombatSkillCache"] = {
				},
				["spells_cast_timeline"] = {
				},
				["start_time"] = 6658.192,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 18,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.008536,
							["damage_from"] = {
							},
							["targets"] = {
								["Raider's Training Dummy"] = 50,
							},
							["pets"] = {
							},
							["total"] = 50.008536,
							["on_hold"] = false,
							["classe"] = "WARLOCK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 50.008536,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1603114735,
							["aID"] = "76-0A4F2267",
							["friendlyfire_total"] = 0,
							["nome"] = "Thorlexi",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									[29722] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 50,
										["targets"] = {
											["Raider's Training Dummy"] = 50,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 50,
										["n_min"] = 50,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 50,
										["c_max"] = 0,
										["spellschool"] = 4,
										["id"] = 29722,
										["r_dmg"] = 0,
										["extra"] = {
										},
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["spec"] = 267,
							["serial"] = "Player-76-0A4F2267",
							["last_dps"] = 11.7033784226535,
							["custom"] = 0,
							["tipo"] = 1,
							["damage_taken"] = 0.008536,
							["start_time"] = 1603114731,
							["delay"] = 0,
							["last_event"] = 1603114731,
						}, -- [1]
						{
							["flag_original"] = 68136,
							["totalabsorbed"] = 0.007918,
							["damage_from"] = {
								["Thorlexi"] = true,
								["Sìlentblades"] = true,
							},
							["targets"] = {
							},
							["pets"] = {
							},
							["aID"] = "31146",
							["raid_targets"] = {
							},
							["total_without_pet"] = 0.007918,
							["classe"] = "UNKNOW",
							["fight_component"] = true,
							["dps_started"] = false,
							["end_time"] = 1603114735,
							["on_hold"] = false,
							["friendlyfire_total"] = 0,
							["nome"] = "Raider's Training Dummy",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
								},
							},
							["friendlyfire"] = {
							},
							["total"] = 0.007918,
							["serial"] = "Creature-0-3779-0-16989-31146-00008A8D34",
							["last_dps"] = 0,
							["custom"] = 0,
							["last_event"] = 0,
							["damage_taken"] = 500.007918,
							["start_time"] = 1603114735,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [2]
					},
				}, -- [1]
				{
					["combatId"] = 18,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 18,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 18,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["nome"] = "Thorlexi",
							["spec"] = 267,
							["grupo"] = true,
							["buff_uptime_targets"] = {
							},
							["buff_uptime"] = 9,
							["pets"] = {
							},
							["aID"] = "76-0A4F2267",
							["last_event"] = 1603114735,
							["tipo"] = 4,
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[328136] = {
										["activedamt"] = 1,
										["id"] = 328136,
										["targets"] = {
										},
										["uptime"] = 4,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
									[313424] = {
										["activedamt"] = 1,
										["id"] = 313424,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-0A4F2267",
							["classe"] = "WARLOCK",
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 18,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Thorlexi"] = true,
				},
				["CombatStartedAt"] = 6657.369,
				["tempo_start"] = 1603114731,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["combat_counter"] = 35,
				["playing_solo"] = true,
				["totals"] = {
					49.99219700000003, -- [1]
					-0.005342, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = -0.007434999999986758,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["totals_grupo"] = {
					50, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["frags_need_refresh"] = false,
				["instance_type"] = "none",
				["data_fim"] = "09:38:56",
				["pvp"] = true,
				["cleu_timeline"] = {
				},
				["enemy"] = "Raider's Training Dummy",
				["TotalElapsedCombatTime"] = 6647.858,
				["CombatEndedAt"] = 6647.858,
				["aura_timeline"] = {
				},
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Thorlexi"] = 50.008536,
						}, -- [1]
					},
				},
				["end_time"] = 6647.858,
				["combat_id"] = 18,
				["cleu_events"] = {
					["n"] = 1,
				},
				["spells_cast_timeline"] = {
				},
				["overall_added"] = true,
				["player_last_events"] = {
				},
				["CombatSkillCache"] = {
				},
				["data_inicio"] = "09:38:51",
				["start_time"] = 6643.585,
				["TimeData"] = {
				},
				["frags"] = {
				},
			}, -- [2]
		},
	},
	["last_version"] = "v9.0.1.7927",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1604410503,
		["last_version"] = 12,
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Default",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["showamount"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["useclasscolors"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["last_section_selected"] = "main",
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["last_day"] = "19",
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["last_realversion"] = 143,
	["combat_id"] = 19,
	["savedStyles"] = {
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["combat_counter"] = 36,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.020182,
					["friendlyfire_total"] = 0,
					["damage_from"] = {
					},
					["targets"] = {
						["Raider's Training Dummy"] = 97,
					},
					["friendlyfire"] = {
					},
					["pets"] = {
					},
					["end_time"] = 1603114736,
					["classe"] = "WARLOCK",
					["raid_targets"] = {
					},
					["total_without_pet"] = 97.020182,
					["aID"] = "76-0A4F2267",
					["dps_started"] = false,
					["total"] = 97.020182,
					["last_event"] = 0,
					["on_hold"] = false,
					["nome"] = "Thorlexi",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							[29722] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 50,
								["targets"] = {
									["Raider's Training Dummy"] = 97,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 97,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 2,
								["total"] = 97,
								["c_max"] = 0,
								["id"] = 29722,
								["r_dmg"] = 0,
								["extra"] = {
								},
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 2,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["spec"] = 267,
					["serial"] = "Player-76-0A4F2267",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1603114723,
					["delay"] = 0,
					["damage_taken"] = 0.020182,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.014062,
					["damage_from"] = {
						["Thorlexi"] = true,
						["Sìlentblades"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.014062,
					["last_event"] = 0,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["total"] = 0.014062,
					["fight_component"] = true,
					["aID"] = "31146",
					["nome"] = "Raider's Training Dummy",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["on_hold"] = false,
					["end_time"] = 1603114736,
					["friendlyfire_total"] = 0,
					["serial"] = "Creature-0-3779-0-16989-31146-00008A8D34",
					["custom"] = 0,
					["tipo"] = 1,
					["last_dps"] = 0,
					["start_time"] = 1603114733,
					["delay"] = 0,
					["damage_taken"] = 368.014062,
				}, -- [2]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["nome"] = "Thorlexi",
					["buff_uptime_targets"] = {
					},
					["spec"] = 267,
					["grupo"] = true,
					["buff_uptime"] = 23,
					["last_event"] = 0,
					["pets"] = {
					},
					["classe"] = "WARLOCK",
					["aID"] = "76-0A4F2267",
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[186406] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 186406,
								["uptime"] = 10,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[328136] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 328136,
								["uptime"] = 10,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[313424] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 313424,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["serial"] = "Player-76-0A4F2267",
					["tipo"] = 4,
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1603114731,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 34,
		["totals"] = {
			368.043767, -- [1]
			0.008320000000000001, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 194.015103,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "09:38:51",
		["end_time"] = 6664.072,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 5.880000000000109,
				["type"] = 0,
				["name"] = "Raider's Training Dummy",
				["clock"] = "09:39:06",
			}, -- [1]
			{
				["elapsed"] = 4.273000000000138,
				["type"] = 0,
				["name"] = "Raider's Training Dummy",
				["clock"] = "09:38:51",
			}, -- [2]
		},
		["totals_grupo"] = {
			97.01710299999999, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "09:39:12",
		["overall_enemy_name"] = "Raider's Training Dummy",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 6653.919,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["force_font_outline"] = "",
	["local_instances_config"] = {
		{
			["modo"] = 2,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["isLocked"] = false,
			["is_open"] = true,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["snap"] = {
			},
			["segment"] = 0,
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 36.8922119140625,
					["x"] = -765.4486694335938,
					["w"] = 185.9022216796875,
					["h"] = 119.3227157592773,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["horizontalSnap"] = false,
			["verticalSnap"] = false,
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
				[4] = 1,
			},
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 31.6158447265625,
					["x"] = -581.108642578125,
					["w"] = 186.6979217529297,
					["h"] = 226.4433441162109,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [2]
	},
	["character_data"] = {
		["logons"] = 11,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A4F2267"] = 267,
		["Player-76-0B19B9B9"] = 259,
	},
}
