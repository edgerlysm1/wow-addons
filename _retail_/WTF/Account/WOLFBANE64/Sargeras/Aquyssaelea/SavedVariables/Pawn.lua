
PawnOptions = {
	["LastVersion"] = 2.0228,
	["LastPlayerFullName"] = "Aquyssaelea-Sargeras",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "MONK",
	["LastAdded"] = 1,
}
