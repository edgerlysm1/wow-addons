
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
			{
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.005842,
							["damage_from"] = {
							},
							["targets"] = {
								["Training Dummy"] = 47,
							},
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 47.005842,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1537674453,
							["friendlyfire_total"] = 0,
							["spec"] = 269,
							["nome"] = "Aquyssaelea",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 2,
										["b_amt"] = 0,
										["c_dmg"] = 18,
										["g_amt"] = 0,
										["n_max"] = 6,
										["targets"] = {
											["Training Dummy"] = 36,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 18,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 6,
										["total"] = 36,
										["c_max"] = 12,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 4,
										["r_amt"] = 0,
										["c_min"] = 6,
									}, -- [1]
									[100780] = {
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Training Dummy"] = 11,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 11,
										["n_min"] = 3,
										["g_dmg"] = 0,
										["counter"] = 3,
										["total"] = 11,
										["c_max"] = 0,
										["id"] = 100780,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 3,
										["r_amt"] = 0,
										["c_min"] = 0,
									},
								},
							},
							["grupo"] = true,
							["total"] = 47.005842,
							["serial"] = "Player-76-09EEEA6C",
							["last_dps"] = 7.41534027449131,
							["custom"] = 0,
							["last_event"] = 1537674447,
							["damage_taken"] = 0.005842,
							["start_time"] = 1537674441,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 2,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 2,
					["tipo"] = 7,
					["_ActorTable"] = {
						{
							["received"] = 5.005827,
							["resource"] = 0.005827,
							["targets"] = {
								["Aquyssaelea"] = 5,
							},
							["pets"] = {
							},
							["powertype"] = 0,
							["classe"] = "MONK",
							["total"] = 5.005827,
							["nome"] = "Aquyssaelea",
							["spec"] = 269,
							["grupo"] = true,
							["flag_original"] = 1297,
							["last_event"] = 1537674447,
							["alternatepower"] = 0.005827,
							["spells"] = {
								["tipo"] = 7,
								["_ActorTable"] = {
									[100780] = {
										["id"] = 100780,
										["total"] = 5,
										["targets"] = {
											["Aquyssaelea"] = 5,
										},
										["counter"] = 3,
									},
								},
							},
							["serial"] = "Player-76-09EEEA6C",
							["tipo"] = 3,
						}, -- [1]
					},
				}, -- [3]
				{
					["combatId"] = 2,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 269,
							["grupo"] = true,
							["spell_cast"] = {
								[100780] = 3,
							},
							["buff_uptime"] = 12,
							["nome"] = "Aquyssaelea",
							["pets"] = {
							},
							["last_event"] = 1537674453,
							["classe"] = "MONK",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 12,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EEEA6C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 2,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Aquyssaelea"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Training Dummy",
				["combat_counter"] = 5,
				["playing_solo"] = true,
				["totals"] = {
					46.992591, -- [1]
					0, -- [2]
					{
						1, -- [1]
						[0] = 4,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = false,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Aquyssaelea"] = 47.005842,
						}, -- [1]
					},
				},
				["end_time"] = 54534.03,
				["combat_id"] = 2,
				["instance_type"] = "none",
				["frags"] = {
				},
				["data_fim"] = "23:47:33",
				["data_inicio"] = "23:47:21",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					47, -- [1]
					0, -- [2]
					{
						1, -- [1]
						[0] = 4,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 54521.86,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [1]
			{
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
						{
							["flag_original"] = 1297,
							["totalabsorbed"] = 0.006027,
							["damage_from"] = {
							},
							["targets"] = {
								["Deer"] = 4,
							},
							["total"] = 4.006027,
							["pets"] = {
							},
							["on_hold"] = false,
							["classe"] = "MONK",
							["raid_targets"] = {
							},
							["total_without_pet"] = 4.006027,
							["colocacao"] = 1,
							["friendlyfire"] = {
							},
							["dps_started"] = false,
							["end_time"] = 1537674438,
							["friendlyfire_total"] = 0,
							["spec"] = 269,
							["nome"] = "Aquyssaelea",
							["spells"] = {
								["tipo"] = 2,
								["_ActorTable"] = {
									{
										["c_amt"] = 0,
										["b_amt"] = 0,
										["c_dmg"] = 0,
										["g_amt"] = 0,
										["n_max"] = 4,
										["targets"] = {
											["Deer"] = 4,
										},
										["m_dmg"] = 0,
										["n_dmg"] = 4,
										["n_min"] = 4,
										["g_dmg"] = 0,
										["counter"] = 1,
										["total"] = 4,
										["c_max"] = 0,
										["id"] = 1,
										["r_dmg"] = 0,
										["spellschool"] = 1,
										["a_dmg"] = 0,
										["m_crit"] = 0,
										["a_amt"] = 0,
										["m_amt"] = 0,
										["successful_casted"] = 0,
										["b_dmg"] = 0,
										["n_amt"] = 1,
										["r_amt"] = 0,
										["c_min"] = 0,
									}, -- [1]
								},
							},
							["grupo"] = true,
							["serial"] = "Player-76-09EEEA6C",
							["last_dps"] = 40.06027,
							["custom"] = 0,
							["last_event"] = 1537674437,
							["damage_taken"] = 0.006027,
							["start_time"] = 1537674437,
							["delay"] = 0,
							["tipo"] = 1,
						}, -- [1]
					},
				}, -- [1]
				{
					["combatId"] = 1,
					["tipo"] = 3,
					["_ActorTable"] = {
					},
				}, -- [2]
				{
					["combatId"] = 1,
					["tipo"] = 7,
					["_ActorTable"] = {
					},
				}, -- [3]
				{
					["combatId"] = 1,
					["tipo"] = 9,
					["_ActorTable"] = {
						{
							["flag_original"] = 1047,
							["buff_uptime_targets"] = {
							},
							["spec"] = 269,
							["grupo"] = true,
							["buff_uptime"] = 1,
							["nome"] = "Aquyssaelea",
							["pets"] = {
							},
							["last_event"] = 1537674438,
							["classe"] = "MONK",
							["buff_uptime_spells"] = {
								["tipo"] = 9,
								["_ActorTable"] = {
									[186406] = {
										["activedamt"] = 1,
										["id"] = 186406,
										["targets"] = {
										},
										["uptime"] = 1,
										["appliedamt"] = 1,
										["refreshamt"] = 0,
										["actived"] = false,
										["counter"] = 0,
									},
								},
							},
							["serial"] = "Player-76-09EEEA6C",
							["tipo"] = 4,
						}, -- [1]
					},
				}, -- [4]
				{
					["combatId"] = 1,
					["tipo"] = 2,
					["_ActorTable"] = {
					},
				}, -- [5]
				["raid_roster"] = {
					["Aquyssaelea"] = true,
				},
				["overall_added"] = true,
				["last_events_tables"] = {
				},
				["alternate_power"] = {
				},
				["enemy"] = "Deer",
				["combat_counter"] = 4,
				["playing_solo"] = true,
				["totals"] = {
					3.994515, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
					["frags_total"] = 0,
					["voidzone_damage"] = 0,
				},
				["player_last_events"] = {
				},
				["frags_need_refresh"] = true,
				["__call"] = {
				},
				["PhaseData"] = {
					{
						1, -- [1]
						1, -- [2]
					}, -- [1]
					["heal_section"] = {
					},
					["heal"] = {
						{
						}, -- [1]
					},
					["damage_section"] = {
					},
					["damage"] = {
						{
							["Aquyssaelea"] = 4.006027,
						}, -- [1]
					},
				},
				["end_time"] = 54519.037,
				["combat_id"] = 1,
				["instance_type"] = "none",
				["frags"] = {
					["Deer"] = 1,
				},
				["data_fim"] = "23:47:18",
				["data_inicio"] = "23:47:17",
				["CombatSkillCache"] = {
				},
				["totals_grupo"] = {
					4, -- [1]
					0, -- [2]
					{
						0, -- [1]
						[0] = 0,
						["alternatepower"] = 0,
						[3] = 0,
						[6] = 0,
					}, -- [3]
					{
						["buff_uptime"] = 0,
						["ress"] = 0,
						["debuff_uptime"] = 0,
						["cooldowns_defensive"] = 0,
						["interrupt"] = 0,
						["dispell"] = 0,
						["cc_break"] = 0,
						["dead"] = 0,
					}, -- [4]
				},
				["start_time"] = 54517.986,
				["TimeData"] = {
				},
				["pvp"] = true,
			}, -- [2]
		},
	},
	["last_version"] = "v8.0.1.6449",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 56.7557983398438,
					["x"] = 381.704162597656,
					["w"] = 310.000030517578,
					["h"] = 158.000106811523,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["cached_talents"] = {
		["Player-76-09EEEA6C"] = {
		},
	},
	["last_instance_id"] = 0,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 0,
	["active_profile"] = "Aquyssaelea-Sargeras",
	["last_realversion"] = 134,
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["useclasscolors"] = false,
			["animate"] = false,
			["useplayercolor"] = false,
			["showamount"] = false,
			["author"] = "Details! Team",
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["enabled"] = true,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_texture"] = "Details Serenity",
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["hide_on_combat"] = false,
			["show_icon"] = 5,
			["opened"] = 0,
			["encounter_timers_dbm"] = {
			},
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["last_day"] = "22",
	["nick_tag_cache"] = {
		["nextreset"] = 1538970399,
		["last_version"] = 10,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["combat_id"] = 2,
	["savedStyles"] = {
	},
	["character_data"] = {
		["logons"] = 1,
	},
	["combat_counter"] = 5,
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.020034,
					["damage_from"] = {
					},
					["targets"] = {
						["Deer"] = 4,
						["Training Dummy"] = 47,
					},
					["spec"] = 269,
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "MONK",
					["raid_targets"] = {
					},
					["total_without_pet"] = 51.020034,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1537674438,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Aquyssaelea",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 18,
								["g_amt"] = 0,
								["n_max"] = 6,
								["targets"] = {
									["Deer"] = 4,
									["Training Dummy"] = 36,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 22,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 40,
								["c_max"] = 12,
								["id"] = 1,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[100780] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 4,
								["targets"] = {
									["Training Dummy"] = 11,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 11,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 3,
								["total"] = 11,
								["c_max"] = 0,
								["id"] = 100780,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 3,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 51.020034,
					["serial"] = "Player-76-09EEEA6C",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 0.020034,
					["start_time"] = 1537674422,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 5.009956,
					["resource"] = 0.009956,
					["targets"] = {
						["Aquyssaelea"] = 5,
					},
					["pets"] = {
					},
					["powertype"] = 0,
					["classe"] = "MONK",
					["total"] = 5.009956,
					["nome"] = "Aquyssaelea",
					["spec"] = 269,
					["grupo"] = true,
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[100780] = {
								["id"] = 100780,
								["total"] = 5,
								["targets"] = {
									["Aquyssaelea"] = 5,
								},
								["counter"] = 3,
							},
						},
					},
					["tipo"] = 3,
					["flag_original"] = 1297,
					["last_event"] = 0,
					["alternatepower"] = 0.009956,
					["serial"] = "Player-76-09EEEA6C",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["buff_uptime_targets"] = {
					},
					["nome"] = "Aquyssaelea",
					["spec"] = 269,
					["grupo"] = true,
					["pets"] = {
					},
					["spell_cast"] = {
						[100780] = 3,
					},
					["buff_uptime"] = 13,
					["last_event"] = 0,
					["tipo"] = 4,
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[186406] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 186406,
								["uptime"] = 13,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["serial"] = "Player-76-09EEEA6C",
					["classe"] = "MONK",
				}, -- [1]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 3,
		["totals"] = {
			51.024763, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 5.005827,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["frags_need_refresh"] = false,
		["__call"] = {
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
		["end_time"] = 54534.03,
		["data_inicio"] = "23:47:17",
		["frags"] = {
		},
		["data_fim"] = "23:47:33",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["segments_added"] = {
			{
				["elapsed"] = 12.1699999999983,
				["type"] = 0,
				["name"] = "Training Dummy",
				["clock"] = "23:47:21",
			}, -- [1]
			{
				["elapsed"] = 1.05099999999948,
				["type"] = 0,
				["name"] = "Deer",
				["clock"] = "23:47:17",
			}, -- [2]
		},
		["start_time"] = 54520.809,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["totals_grupo"] = {
			51.011869, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 5.005827,
				["alternatepower"] = 0,
				[3] = 0,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
	},
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["force_font_outline"] = "",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["announce_cooldowns"] = {
		["enabled"] = false,
		["ignored_cooldowns"] = {
		},
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-09EEEA6C"] = 269,
	},
}
