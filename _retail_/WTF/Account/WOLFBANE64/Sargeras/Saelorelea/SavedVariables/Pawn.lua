
PawnOptions = {
	["LastVersion"] = 2.0246,
	["ItemLevels"] = {
		{
			["ID"] = 153954,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153954::::::::110:259::::::|h[Lightdrinker Hood]|h|r",
		}, -- [1]
		{
			["ID"] = 153946,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153946::::::::110:259::::::|h[Lightdrinker Choker]|h|r",
		}, -- [2]
		{
			["ID"] = 153956,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153956::::::::110:259::::::|h[Lightdrinker Shoulders]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 153951,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153951::::::::110:259::::::|h[Lightdrinker Jerkin]|h|r",
		}, -- [5]
		{
			["ID"] = 153957,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153957::::::::110:259::::::|h[Lightdrinker Waistband]|h|r",
		}, -- [6]
		{
			["ID"] = 153955,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153955::::::::110:259::::::|h[Lightdrinker Britches]|h|r",
		}, -- [7]
		{
			["ID"] = 153952,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153952::::::::110:259::::::|h[Lightdrinker Boots]|h|r",
		}, -- [8]
		{
			["ID"] = 153958,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153958::::::::110:259::::::|h[Lightdrinker Bindings]|h|r",
		}, -- [9]
		{
			["ID"] = 153953,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153953::::::::110:259::::::|h[Lightdrinker Gloves]|h|r",
		}, -- [10]
		{
			["ID"] = 153949,
			["Level"] = 176,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff1eff00|Hitem:153949::::::::110:259::::::|h[Lightdrinker Ring of Onslaught]|h|r",
		}, -- [11]
		{
			["ID"] = 153948,
			["Level"] = 176,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff1eff00|Hitem:153948::::::::110:259::::::|h[Lightdrinker Band of Onslaught]|h|r",
		}, -- [12]
		{
			["ID"] = 153950,
			["Level"] = 176,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff1eff00|Hitem:153950::::::::110:259::::::|h[Lightdrinker Stone of Rage]|h|r",
		}, -- [13]
		{
			["ID"] = 153947,
			["Level"] = 176,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff1eff00|Hitem:153947::::::::110:259::::::|h[Lightdrinker Idol of Rage]|h|r",
		}, -- [14]
		{
			["ID"] = 153945,
			["Level"] = 176,
			["Link"] = "|cff1eff00|Hitem:153945::::::::110:259::::::|h[Lightdrinker Cloak of Rage]|h|r",
		}, -- [15]
		{
			["ID"] = 153959,
			["Level"] = 185,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cff1eff00|Hitem:153959::::::::110:259::::::|h[Lightdrinker Dagger]|h|r",
		}, -- [16]
		{
			["ID"] = 153960,
			["Level"] = 185,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cff1eff00|Hitem:153960::::::::110:259::::::|h[Lightdrinker Shiv]|h|r",
		}, -- [17]
	},
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Saelorelea-Sargeras",
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "ROGUE",
	["LastAdded"] = 1,
}
