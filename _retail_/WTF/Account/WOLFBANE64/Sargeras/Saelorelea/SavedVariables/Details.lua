
_detalhes_database = {
	["savedbuffs"] = {
	},
	["mythic_dungeon_id"] = 0,
	["tabela_historico"] = {
		["tabelas"] = {
		},
	},
	["last_version"] = "v8.2.0.7177",
	["SoloTablesSaved"] = {
		["Mode"] = 1,
	},
	["tabela_instancias"] = {
	},
	["on_death_menu"] = true,
	["nick_tag_cache"] = {
		["nextreset"] = 1570917049,
		["last_version"] = 11,
	},
	["last_instance_id"] = 1949,
	["announce_interrupts"] = {
		["enabled"] = false,
		["whisper"] = "",
		["channel"] = "SAY",
		["custom"] = "",
		["next"] = "",
	},
	["last_instance_time"] = 1569621102,
	["active_profile"] = "Saelorelea-Sargeras",
	["mythic_dungeon_currentsaved"] = {
		["dungeon_name"] = "",
		["started"] = false,
		["segment_id"] = 0,
		["ej_id"] = 0,
		["started_at"] = 0,
		["run_id"] = 0,
		["level"] = 0,
		["dungeon_zone_id"] = 0,
		["previous_boss_killed_at"] = 0,
	},
	["ignore_nicktag"] = false,
	["plugin_database"] = {
		["DETAILS_PLUGIN_TINY_THREAT"] = {
			["updatespeed"] = 1,
			["enabled"] = true,
			["animate"] = false,
			["useplayercolor"] = false,
			["author"] = "Details! Team",
			["useclasscolors"] = false,
			["playercolor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
			},
			["showamount"] = false,
		},
		["DETAILS_PLUGIN_TIME_LINE"] = {
			["enabled"] = true,
			["author"] = "Details! Team",
		},
		["DETAILS_PLUGIN_VANGUARD"] = {
			["enabled"] = true,
			["tank_block_color"] = {
				0.24705882, -- [1]
				0.0039215, -- [2]
				0, -- [3]
				0.8, -- [4]
			},
			["tank_block_texture"] = "Details Serenity",
			["show_inc_bars"] = false,
			["author"] = "Details! Team",
			["first_run"] = false,
			["tank_block_size"] = 150,
		},
		["DETAILS_PLUGIN_ENCOUNTER_DETAILS"] = {
			["enabled"] = true,
			["encounter_timers_bw"] = {
			},
			["max_emote_segments"] = 3,
			["author"] = "Details! Team",
			["window_scale"] = 1,
			["encounter_timers_dbm"] = {
			},
			["show_icon"] = 5,
			["opened"] = 0,
			["hide_on_combat"] = false,
		},
		["DETAILS_PLUGIN_RAIDCHECK"] = {
			["enabled"] = true,
			["food_tier1"] = true,
			["mythic_1_4"] = true,
			["food_tier2"] = true,
			["author"] = "Details! Team",
			["use_report_panel"] = true,
			["pre_pot_healers"] = false,
			["pre_pot_tanks"] = false,
			["food_tier3"] = true,
		},
	},
	["last_day"] = "27",
	["cached_talents"] = {
	},
	["announce_prepots"] = {
		["enabled"] = true,
		["channel"] = "SELF",
		["reverse"] = false,
	},
	["benchmark_db"] = {
		["frame"] = {
		},
	},
	["character_data"] = {
		["logons"] = 2,
	},
	["combat_id"] = 9,
	["savedStyles"] = {
	},
	["local_instances_config"] = {
		{
			["segment"] = 0,
			["sub_attribute"] = 1,
			["sub_atributo_last"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
				1, -- [5]
			},
			["is_open"] = true,
			["isLocked"] = false,
			["snap"] = {
			},
			["mode"] = 2,
			["attribute"] = 1,
			["pos"] = {
				["normal"] = {
					["y"] = 158.021484375,
					["x"] = -560.0338592529297,
					["w"] = 310,
					["h"] = 158.0000152587891,
				},
				["solo"] = {
					["y"] = 2,
					["x"] = 1,
					["w"] = 300,
					["h"] = 200,
				},
			},
		}, -- [1]
	},
	["force_font_outline"] = "",
	["announce_deaths"] = {
		["enabled"] = false,
		["last_hits"] = 1,
		["only_first"] = 5,
		["where"] = 1,
	},
	["tabela_overall"] = {
		{
			["tipo"] = 2,
			["_ActorTable"] = {
				{
					["flag_original"] = 1297,
					["totalabsorbed"] = 0.04429900000000001,
					["spec"] = 259,
					["damage_from"] = {
						["Horde Grunt"] = true,
						["Wyvern Rider"] = true,
						["Alliance Infantry"] = true,
					},
					["targets"] = {
						["Alliance Infantry"] = 22354,
						["Horde Grunt"] = 26179,
						["Wyvern Rider"] = 39799,
						["Target Dummy"] = 109865,
					},
					["pets"] = {
					},
					["last_event"] = 0,
					["classe"] = "ROGUE",
					["raid_targets"] = {
					},
					["total_without_pet"] = 198197.044299,
					["friendlyfire"] = {
					},
					["dps_started"] = false,
					["end_time"] = 1569621417,
					["friendlyfire_total"] = 0,
					["on_hold"] = false,
					["nome"] = "Saelorelea",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 91,
								["b_amt"] = 0,
								["c_dmg"] = 32320,
								["g_amt"] = 0,
								["n_max"] = 230,
								["targets"] = {
									["Alliance Infantry"] = 9016,
									["Horde Grunt"] = 12004,
									["Wyvern Rider"] = 13756,
									["Target Dummy"] = 43445,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 45901,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 429,
								["total"] = 78221,
								["c_max"] = 459,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 57,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 281,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[5374] = {
								["c_amt"] = 25,
								["b_amt"] = 0,
								["c_dmg"] = 12434,
								["g_amt"] = 0,
								["n_max"] = 328,
								["targets"] = {
									["Alliance Infantry"] = 3758,
									["Horde Grunt"] = 4739,
									["Wyvern Rider"] = 7517,
									["Target Dummy"] = 15713,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 19293,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 103,
								["total"] = 31727,
								["c_max"] = 655,
								["id"] = 5374,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 78,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[185565] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 92,
								["targets"] = {
									["Target Dummy"] = 460,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 460,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 5,
								["total"] = 460,
								["c_max"] = 0,
								["id"] = 185565,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 5,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[32645] = {
								["c_amt"] = 1,
								["b_amt"] = 0,
								["c_dmg"] = 2924,
								["g_amt"] = 0,
								["n_max"] = 1463,
								["targets"] = {
									["Alliance Infantry"] = 1170,
									["Horde Grunt"] = 2924,
									["Wyvern Rider"] = 2632,
									["Target Dummy"] = 4388,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 8190,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 7,
								["total"] = 11114,
								["c_max"] = 2924,
								["id"] = 32645,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 6,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[113780] = {
								["c_amt"] = 32,
								["b_amt"] = 0,
								["c_dmg"] = 5361,
								["g_amt"] = 0,
								["n_max"] = 107,
								["targets"] = {
									["Alliance Infantry"] = 1640,
									["Horde Grunt"] = 1310,
									["Wyvern Rider"] = 2788,
									["Target Dummy"] = 9889,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 10266,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 154,
								["total"] = 15627,
								["c_max"] = 214,
								["id"] = 113780,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 122,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[2818] = {
								["c_amt"] = 32,
								["b_amt"] = 0,
								["c_dmg"] = 7715,
								["g_amt"] = 0,
								["n_max"] = 150,
								["targets"] = {
									["Alliance Infantry"] = 2300,
									["Horde Grunt"] = 2747,
									["Wyvern Rider"] = 4255,
									["Target Dummy"] = 16945,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 18532,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 188,
								["total"] = 26247,
								["c_max"] = 300,
								["id"] = 2818,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 156,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[703] = {
								["c_amt"] = 8,
								["b_amt"] = 0,
								["c_dmg"] = 3073,
								["g_amt"] = 0,
								["n_max"] = 220,
								["targets"] = {
									["Alliance Infantry"] = 2409,
									["Horde Grunt"] = 1314,
									["Wyvern Rider"] = 1752,
									["Target Dummy"] = 4172,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 6574,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 38,
								["total"] = 9647,
								["c_max"] = 439,
								["id"] = 703,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 30,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[86392] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 470,
								["targets"] = {
									["Target Dummy"] = 470,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 470,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 1,
								["total"] = 470,
								["c_max"] = 0,
								["id"] = 86392,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 1,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[1943] = {
								["c_amt"] = 19,
								["b_amt"] = 0,
								["c_dmg"] = 8410,
								["g_amt"] = 0,
								["n_max"] = 230,
								["targets"] = {
									["Alliance Infantry"] = 2061,
									["Horde Grunt"] = 1141,
									["Wyvern Rider"] = 7099,
									["Target Dummy"] = 11700,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 13591,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 79,
								["total"] = 22001,
								["c_max"] = 458,
								["id"] = 1943,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 60,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
							[51723] = {
								["c_amt"] = 2,
								["b_amt"] = 0,
								["c_dmg"] = 767,
								["g_amt"] = 0,
								["n_max"] = 192,
								["targets"] = {
									["Target Dummy"] = 2683,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 1916,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 12,
								["total"] = 2683,
								["c_max"] = 384,
								["id"] = 51723,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 10,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["grupo"] = true,
					["total"] = 198197.044299,
					["serial"] = "Player-76-0A371A4E",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 24843.044299,
					["start_time"] = 1569621027,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [1]
				{
					["flag_original"] = 68136,
					["totalabsorbed"] = 0.030914,
					["damage_from"] = {
						["Saelorelea"] = true,
					},
					["targets"] = {
					},
					["pets"] = {
					},
					["on_hold"] = false,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 0.030914,
					["fight_component"] = true,
					["dps_started"] = false,
					["end_time"] = 1569621417,
					["friendlyfire"] = {
					},
					["last_event"] = 0,
					["nome"] = "Target Dummy",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
						},
					},
					["friendlyfire_total"] = 0,
					["total"] = 0.030914,
					["serial"] = "Creature-0-3783-1949-31971-107104-00000E84F2",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 109865.030914,
					["start_time"] = 1569621414,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [2]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.021285,
					["on_hold"] = false,
					["damage_from"] = {
						["Horde Grunt"] = true,
						["Wyvern Rider"] = true,
						["Saelorelea"] = true,
					},
					["targets"] = {
						["Horde Grunt"] = 109230,
						["Wyvern Rider"] = 284,
						["Saelorelea"] = 8472,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 117986.021285,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569621476,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Alliance Infantry",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 6878,
								["g_amt"] = 0,
								["n_max"] = 2033,
								["targets"] = {
									["Horde Grunt"] = 109230,
									["Wyvern Rider"] = 284,
									["Saelorelea"] = 8472,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 111108,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 197,
								["a_amt"] = 0,
								["total"] = 117986,
								["c_max"] = 2612,
								["DODGE"] = 11,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 6,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 5,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 170,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
						},
					},
					["monster"] = true,
					["total"] = 117986.021285,
					["serial"] = "Creature-0-3783-1949-31971-102592-00000E8446",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 38601.021285,
					["start_time"] = 1569621292,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [3]
				{
					["flag_original"] = 68168,
					["totalabsorbed"] = 0.009636,
					["on_hold"] = false,
					["damage_from"] = {
						["Saelorelea"] = true,
						["Magus Filinthus"] = true,
						["General Bret Hughes"] = true,
						["Alliance Lookout"] = true,
						["Alliance Infantry"] = true,
						["Deckhand"] = true,
						["Alliance Recruit"] = true,
						["Anchorite Taliah"] = true,
					},
					["targets"] = {
						["Saelorelea"] = 2388,
						["Magus Filinthus"] = 2703,
						["General Bret Hughes"] = 7912,
						["Alliance Lookout"] = 4676,
						["Alliance Infantry"] = 9190,
						["Deckhand"] = 15929,
						["Alliance Recruit"] = 10060,
						["Anchorite Taliah"] = 1623,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 54481.009636,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569621665,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Horde Grunt",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 34,
								["b_amt"] = 0,
								["c_dmg"] = 6108,
								["g_amt"] = 0,
								["n_max"] = 284,
								["targets"] = {
									["Saelorelea"] = 2388,
									["Magus Filinthus"] = 2703,
									["General Bret Hughes"] = 7912,
									["Alliance Lookout"] = 4676,
									["Alliance Infantry"] = 9190,
									["Deckhand"] = 15929,
									["Alliance Recruit"] = 10060,
									["Anchorite Taliah"] = 1623,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 48373,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 628,
								["a_amt"] = 0,
								["total"] = 54481,
								["c_max"] = 510,
								["MISS"] = 77,
								["id"] = 1,
								["r_dmg"] = 0,
								["DODGE"] = 72,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 73,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 372,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
							[275764] = {
								["c_amt"] = 0,
								["b_amt"] = 0,
								["c_dmg"] = 0,
								["g_amt"] = 0,
								["n_max"] = 0,
								["targets"] = {
								},
								["m_dmg"] = 0,
								["n_dmg"] = 0,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 0,
								["total"] = 0,
								["c_max"] = 0,
								["id"] = 275764,
								["r_dmg"] = 0,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["a_amt"] = 0,
								["m_amt"] = 0,
								["successful_casted"] = 49,
								["b_dmg"] = 0,
								["n_amt"] = 0,
								["r_amt"] = 0,
								["c_min"] = 0,
							},
						},
					},
					["monster"] = true,
					["total"] = 54481.009636,
					["serial"] = "Creature-0-3783-1949-31971-140810-00008E861E",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 380437.009636,
					["start_time"] = 1569621536,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [4]
				{
					["flag_original"] = 2632,
					["totalabsorbed"] = 0.015624,
					["on_hold"] = false,
					["damage_from"] = {
						["Alliance Recruit"] = true,
						["Deckhand"] = true,
						["Saelorelea"] = true,
						["Alliance Infantry"] = true,
					},
					["targets"] = {
						["Saelorelea"] = 13983,
						["Alliance Lookout"] = 9973,
						["Alliance Infantry"] = 7057,
						["Deckhand"] = 22045,
						["Alliance Recruit"] = 3903,
					},
					["pets"] = {
					},
					["fight_component"] = true,
					["classe"] = "UNKNOW",
					["raid_targets"] = {
					},
					["total_without_pet"] = 56961.015624,
					["last_event"] = 0,
					["dps_started"] = false,
					["end_time"] = 1569621665,
					["friendlyfire"] = {
					},
					["friendlyfire_total"] = 0,
					["nome"] = "Wyvern Rider",
					["spells"] = {
						["tipo"] = 2,
						["_ActorTable"] = {
							{
								["c_amt"] = 5,
								["b_amt"] = 0,
								["c_dmg"] = 3664,
								["g_amt"] = 0,
								["n_max"] = 3323,
								["targets"] = {
									["Saelorelea"] = 13983,
									["Alliance Lookout"] = 9973,
									["Alliance Infantry"] = 7057,
									["Deckhand"] = 22045,
									["Alliance Recruit"] = 3903,
								},
								["m_dmg"] = 0,
								["n_dmg"] = 53297,
								["n_min"] = 0,
								["g_dmg"] = 0,
								["counter"] = 70,
								["a_amt"] = 0,
								["total"] = 56961,
								["c_max"] = 911,
								["DODGE"] = 8,
								["id"] = 1,
								["r_dmg"] = 0,
								["MISS"] = 2,
								["a_dmg"] = 0,
								["m_crit"] = 0,
								["PARRY"] = 3,
								["m_amt"] = 0,
								["successful_casted"] = 0,
								["b_dmg"] = 0,
								["n_amt"] = 52,
								["r_amt"] = 0,
								["c_min"] = 0,
							}, -- [1]
						},
					},
					["monster"] = true,
					["total"] = 56961.015624,
					["serial"] = "Creature-0-3783-1949-31971-140852-00000E8614",
					["custom"] = 0,
					["tipo"] = 1,
					["damage_taken"] = 46830.015624,
					["start_time"] = 1569621563,
					["delay"] = 0,
					["last_dps"] = 0,
				}, -- [5]
			},
		}, -- [1]
		{
			["tipo"] = 3,
			["_ActorTable"] = {
			},
		}, -- [2]
		{
			["tipo"] = 7,
			["_ActorTable"] = {
				{
					["received"] = 0.0402,
					["resource"] = 94.04019999999998,
					["targets"] = {
						["Saelorelea"] = 0,
					},
					["pets"] = {
					},
					["powertype"] = 3,
					["classe"] = "ROGUE",
					["passiveover"] = 0.007709,
					["total"] = 0.0402,
					["tipo"] = 3,
					["nome"] = "Saelorelea",
					["spells"] = {
						["tipo"] = 7,
						["_ActorTable"] = {
							[35546] = {
								["total"] = 0,
								["id"] = 35546,
								["totalover"] = 0,
								["targets"] = {
									["Saelorelea"] = 0,
								},
								["counter"] = 4,
							},
						},
					},
					["grupo"] = true,
					["totalover"] = 0.007709,
					["flag_original"] = 1297,
					["alternatepower"] = 0.0402,
					["last_event"] = 0,
					["spec"] = 259,
					["serial"] = "Player-76-0A371A4E",
				}, -- [1]
			},
		}, -- [3]
		{
			["tipo"] = 9,
			["_ActorTable"] = {
				{
					["flag_original"] = 1047,
					["debuff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[2818] = {
								["refreshamt"] = 2,
								["activedamt"] = 1,
								["appliedamt"] = 13,
								["id"] = 2818,
								["uptime"] = 289,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[703] = {
								["refreshamt"] = 2,
								["activedamt"] = 1,
								["appliedamt"] = 9,
								["id"] = 703,
								["uptime"] = 76,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[1943] = {
								["refreshamt"] = 2,
								["activedamt"] = 0,
								["appliedamt"] = 12,
								["id"] = 1943,
								["uptime"] = 134,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["buff_uptime"] = 423,
					["classe"] = "ROGUE",
					["buff_uptime_spells"] = {
						["tipo"] = 9,
						["_ActorTable"] = {
							[5277] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 5277,
								["uptime"] = 3,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[1784] = {
								["refreshamt"] = 0,
								["activedamt"] = 2,
								["appliedamt"] = 2,
								["id"] = 1784,
								["uptime"] = 0,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[2823] = {
								["refreshamt"] = 0,
								["activedamt"] = 6,
								["appliedamt"] = 6,
								["id"] = 2823,
								["uptime"] = 378,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[1966] = {
								["refreshamt"] = 0,
								["activedamt"] = 1,
								["appliedamt"] = 1,
								["id"] = 1966,
								["uptime"] = 2,
								["targets"] = {
								},
								["counter"] = 0,
							},
							[32645] = {
								["refreshamt"] = 0,
								["activedamt"] = 7,
								["appliedamt"] = 7,
								["id"] = 32645,
								["uptime"] = 40,
								["targets"] = {
								},
								["counter"] = 0,
							},
						},
					},
					["debuff_uptime"] = 499,
					["buff_uptime_targets"] = {
					},
					["spec"] = 259,
					["grupo"] = true,
					["spell_cast"] = {
						[185565] = 4,
						[1966] = 1,
						[51723] = 12,
						[1329] = 52,
						[32645] = 7,
						[1943] = 14,
						[703] = 11,
						[27576] = 52,
					},
					["debuff_uptime_targets"] = {
					},
					["tipo"] = 4,
					["nome"] = "Saelorelea",
					["pets"] = {
					},
					["serial"] = "Player-76-0A371A4E",
					["last_event"] = 0,
				}, -- [1]
				{
					["monster"] = true,
					["tipo"] = 4,
					["nome"] = "Horde Grunt",
					["pets"] = {
					},
					["spell_cast"] = {
						[275764] = 49,
					},
					["flag_original"] = 2632,
					["last_event"] = 0,
					["fight_component"] = true,
					["serial"] = "Creature-0-3783-1949-31971-140810-00000E8637",
					["classe"] = "UNKNOW",
				}, -- [2]
			},
		}, -- [4]
		{
			["tipo"] = 2,
			["_ActorTable"] = {
			},
		}, -- [5]
		["raid_roster"] = {
		},
		["tempo_start"] = 1569621257,
		["last_events_tables"] = {
		},
		["alternate_power"] = {
		},
		["combat_counter"] = 6,
		["totals"] = {
			427625.0660179999, -- [1]
			-0.03655400000021633, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0.03249100000000001,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
			["frags_total"] = 0,
			["voidzone_damage"] = 0,
		},
		["player_last_events"] = {
		},
		["spells_cast_timeline"] = {
		},
		["frags_need_refresh"] = false,
		["aura_timeline"] = {
		},
		["__call"] = {
		},
		["data_inicio"] = "17:54:17",
		["end_time"] = 23241.882,
		["cleu_events"] = {
			["n"] = 1,
		},
		["segments_added"] = {
			{
				["elapsed"] = 23.81999999999971,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:05:25",
			}, -- [1]
			{
				["elapsed"] = 10.47000000000116,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:03:14",
			}, -- [2]
			{
				["elapsed"] = 26.23099999999977,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "18:02:24",
			}, -- [3]
			{
				["elapsed"] = 125.1599999999999,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:59:00",
			}, -- [4]
			{
				["elapsed"] = 30.85800000000018,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:58:10",
			}, -- [5]
			{
				["elapsed"] = 25.60099999999875,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:57:30",
			}, -- [6]
			{
				["elapsed"] = 160.0689999999995,
				["type"] = 5,
				["name"] = "Trash Cleanup",
				["clock"] = "17:54:17",
			}, -- [7]
		},
		["totals_grupo"] = {
			198197.03762, -- [1]
			0, -- [2]
			{
				0, -- [1]
				[0] = 0,
				["alternatepower"] = 0,
				[3] = 0.03249100000000001,
				[6] = 0,
			}, -- [3]
			{
				["buff_uptime"] = 0,
				["ress"] = 0,
				["debuff_uptime"] = 0,
				["cooldowns_defensive"] = 0,
				["interrupt"] = 0,
				["dispell"] = 0,
				["cc_break"] = 0,
				["dead"] = 0,
			}, -- [4]
		},
		["frags"] = {
		},
		["data_fim"] = "18:05:49",
		["overall_enemy_name"] = "-- x -- x --",
		["CombatSkillCache"] = {
		},
		["cleu_timeline"] = {
		},
		["start_time"] = 22839.673,
		["TimeData"] = {
			["Player Damage Done"] = {
			},
			["Raid Damage Done"] = {
			},
		},
		["PhaseData"] = {
			{
				1, -- [1]
				1, -- [2]
			}, -- [1]
			["heal_section"] = {
			},
			["heal"] = {
			},
			["damage_section"] = {
			},
			["damage"] = {
			},
		},
	},
	["combat_counter"] = 17,
	["announce_firsthit"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["last_realversion"] = 140,
	["announce_cooldowns"] = {
		["ignored_cooldowns"] = {
		},
		["enabled"] = false,
		["custom"] = "",
		["channel"] = "RAID",
	},
	["rank_window"] = {
		["last_difficulty"] = 15,
		["last_raid"] = "",
	},
	["announce_damagerecord"] = {
		["enabled"] = true,
		["channel"] = "SELF",
	},
	["cached_specs"] = {
		["Player-76-0A371A4E"] = 260,
	},
}
