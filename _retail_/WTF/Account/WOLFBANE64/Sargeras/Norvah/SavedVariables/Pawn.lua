
PawnOptions = {
	["LastVersion"] = 2.0324,
	["ItemLevels"] = {
		{
			["ID"] = 122250,
			["Level"] = 97,
			["Link"] = "|cff00ccff|Hitem:122250::::::::77:257:::1:3592:::|h[Tattered Dreadmist Mask]|h|r",
		}, -- [1]
		{
			["ID"] = 44376,
			["Level"] = 88,
			["Link"] = "|cff0070dd|Hitem:44376::::::::72:257::11::::|h[Necklace of Fragmented Light]|h|r",
		}, -- [2]
		{
			["ID"] = 122360,
			["Level"] = 97,
			["Link"] = "|cff00ccff|Hitem:122360::::::::77:257:::1:582:::|h[Tattered Dreadmist Mantle]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 122384,
			["Level"] = 97,
			["Link"] = "|cff00ccff|Hitem:122384::::::::77:257:::1:3592:::|h[Tattered Dreadmist Robe]|h|r",
		}, -- [5]
		{
			["ID"] = 37850,
			["Level"] = 97,
			["Link"] = "|cff0070dd|Hitem:37850::::::::76:257:512:1:1:4119:76:::|h[Flowing Sash of Order]|h|r",
		}, -- [6]
		{
			["ID"] = 122256,
			["Level"] = 97,
			["Link"] = "|cff00ccff|Hitem:122256::::::::77:257:::1:3592:::|h[Tattered Dreadmist Leggings]|h|r",
		}, -- [7]
		{
			["ID"] = 25957,
			["Level"] = 68,
			["Link"] = "|cff0070dd|Hitem:25957::::::::72:257::1::::|h[Ethereal Boots of the Skystrider]|h|r",
		}, -- [8]
		{
			["ID"] = 39676,
			["Level"] = 90,
			["Link"] = "|cff0070dd|Hitem:39676::::::::72:257::11::::|h[Wraps of the San'layn]|h|r",
		}, -- [9]
		{
			["ID"] = 24608,
			["Level"] = 63,
			["Link"] = "|cff1eff00|Hitem:24608::::::::72:257::1:1:1678:::|h[Laughing Skull Gloves of the Quickblade]|h|r",
		}, -- [10]
		{
			["ID"] = 38219,
			["Level"] = 90,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:38219::::::::72:257::11::::|h[Ring of Decimation]|h|r",
		}, -- [11]
		{
			["ID"] = 38219,
			["Level"] = 90,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff0070dd|Hitem:38219::::::::72:258::11::::|h[Ring of Decimation]|h|r",
		}, -- [12]
		{
			["ID"] = 122362,
			["Level"] = 97,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122362::::::::77:257:::1:3592:::|h[Discerning Eye of the Beast]|h|r",
		}, -- [13]
		{
			["ID"] = 122362,
			["Level"] = 95,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122362::::::::76:257:::1:3592:::|h[Discerning Eye of the Beast]|h|r",
		}, -- [14]
		{
			["ID"] = 122262,
			["Level"] = 97,
			["Link"] = "|cff00ccff|Hitem:122262::::::::77:257:::1:3592:::|h[Ancient Bloodmoon Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 122353,
			["Level"] = 97,
			["Link"] = "|cff00ccff|Hitem:122353:723:::::::77:257:::1:3592:::|h[Dignified Headmaster's Charge]|h|r",
		}, -- [16]
	},
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Norvah-Sargeras",
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PRIEST",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
