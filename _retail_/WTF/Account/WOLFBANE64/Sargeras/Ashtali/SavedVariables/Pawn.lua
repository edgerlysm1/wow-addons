
PawnOptions = {
	["LastVersion"] = 2.0402,
	["ItemLevels"] = {
		{
			["ID"] = 167762,
			["Level"] = 71,
			["Link"] = "|cffa335ee|Hitem:167762::::::::50:577:::6:6296:6300:1472:4786:5414:4775::::|h[Fathomstalker Headcover]|h|r",
		}, -- [1]
		{
			["ID"] = 158075,
			["Level"] = 110,
			["Link"] = "|cffe6cc80|Hitem:158075::::::::50:577::11:4:4932:4933:6316:1524::::|h[Heart of Azeroth]|h|r",
		}, -- [2]
		{
			["ID"] = 157979,
			["Level"] = 83,
			["Link"] = "|cff0070dd|Hitem:157979::::::::50:577::25:4:1492:4785:6259:4775::::|h[Seafarer Shoulderpads]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 167759,
			["Level"] = 71,
			["Link"] = "|cffa335ee|Hitem:167759::::::::50:577:::6:6296:6300:1472:4786:5413:4775::::|h[Fathomstalker Wraps]|h|r",
		}, -- [5]
		{
			["ID"] = 157980,
			["Level"] = 78,
			["Link"] = "|cff0070dd|Hitem:157980::::::::50:577::26:5:4803:6516:6513:1492:4785::::|h[Seafarer Belt]|h|r",
		}, -- [6]
		{
			["ID"] = 169809,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:169809::::::::50:577:::2:6288:6300::::|h[Fathomstalker Leggings]|h|r",
		}, -- [7]
		{
			["ID"] = 163383,
			["Level"] = 66,
			["Link"] = "|cff0070dd|Hitem:163383::::::::50:577::28:8:5125:40:6578:6579:6537:6515:1480:4785::::|h[7th Legionnaire's Boots]|h|r",
		}, -- [8]
		{
			["ID"] = 157981,
			["Level"] = 76,
			["Link"] = "|cff0070dd|Hitem:157981::::::::50:577::25:5:4803:6516:6515:1490:4785::::|h[Seafarer Armguards]|h|r",
		}, -- [9]
		{
			["ID"] = 170334,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:170334::::::::50:577:::2:6288:6300::::|h[Poen's Deepsea Grips]|h|r",
		}, -- [10]
		{
			["ID"] = 158161,
			["Level"] = 78,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff0070dd|Hitem:158161::::::::50:577::26:5:4803:6516:6513:1492:4785::::|h[Spearfisher's Band]|h|r",
		}, -- [11]
		{
			["ID"] = 158151,
			["Level"] = 60,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff0070dd|Hitem:158151::::::::50:577::27:5:4803:6516:6513:1474:4785::::|h[Zandalari Band]|h|r",
		}, -- [12]
		{
			["ID"] = 158164,
			["Level"] = 76,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff0070dd|Hitem:158164::::::::50:577::26:4:4803:6515:1490:4785::::|h[Plunderbeard's Flask]|h|r",
		}, -- [13]
		{
			["ID"] = 165660,
			["Level"] = 76,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff0070dd|Hitem:165660::::::::50:577::25:4:4803:6513:1490:4785::::|h[Chargestone of the Thunder King's Court]|h|r",
		}, -- [14]
		{
			["ID"] = 169486,
			["Level"] = 66,
			["Link"] = "|cffa335ee|Hitem:169486::::::::50:577:::2:6288:6300::::|h[Shirakess Drape]|h|r",
		}, -- [15]
		{
			["ID"] = 155271,
			["Level"] = 76,
			["AlsoFitsIn"] = 17,
			["Link"] = "|cff0070dd|Hitem:155271::::::::50:577::25:3:4803:1490:4785::::|h[Monkey's Paw Chopper]|h|r",
		}, -- [16]
		{
			["ID"] = 170294,
			["Level"] = 72,
			["AlsoFitsIn"] = 16,
			["Link"] = "|cffa335ee|Hitem:170294::::::::50:577::13:2:1715:5804::::|h[Uncanny Combatant's Cutlass of the Harmonious]|h|r",
		}, -- [17]
	},
	["AutoSelectScales"] = false,
	["UpgradeTracking"] = false,
	["LastPlayerFullName"] = "Ashtali-Sargeras",
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "DEMONHUNTER",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
