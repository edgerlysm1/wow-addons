
PawnOptions = {
	["LastVersion"] = 2.0311,
	["LastPlayerFullName"] = "Haraklem-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		[5] = {
			["ID"] = 58248,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:58248::::::::1:70::::::|h[Initiate's Vest]|h|r",
		},
		[16] = {
			["ID"] = 2361,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:2361::::::::1:70::::::|h[Battleworn Hammer]|h|r",
		},
		[7] = {
			["ID"] = 52549,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:52549::::::::1:70::::::|h[Initiate's Pants]|h|r",
		},
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PALADIN",
	["LastAdded"] = 1,
}
