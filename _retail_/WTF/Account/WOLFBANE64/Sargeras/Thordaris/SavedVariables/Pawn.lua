
PawnOptions = {
	["LastVersion"] = 2.033,
	["LastPlayerFullName"] = "Thordaris-Sargeras",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 122247,
			["Level"] = 60,
			["Link"] = "|cff00ccff|Hitem:122247::::::::56:253::::::|h[Mystical Coif of Elements]|h|r",
		}, -- [1]
		{
			["ID"] = 122668,
			["Level"] = 60,
			["Link"] = "|cff00ccff|Hitem:122668::::::::56:253::::::|h[Eternal Will of the Martyr]|h|r",
		}, -- [2]
		{
			["ID"] = 160916,
			["Level"] = 63,
			["Link"] = "|cff0070dd|Hitem:160916::::::::50:253::11:1:4775::::|h[Spaulders of the Champion]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 122379,
			["Level"] = 60,
			["Link"] = "|cff00ccff|Hitem:122379::::::::56:253:::1:582:::|h[Champion's Deathdealer Breastplate]|h|r",
		}, -- [5]
		{
			["ID"] = 158625,
			["Level"] = 56,
			["Link"] = "|cff1eff00|Hitem:158625::::::::50:253::11:1:4793:1:9:48:::|h[Crone-Seeker's Girdle]|h|r",
		}, -- [6]
		{
			["ID"] = 122253,
			["Level"] = 60,
			["Link"] = "|cff00ccff|Hitem:122253::::::::56:253::::::|h[Mystical Kilt of Elements]|h|r",
		}, -- [7]
		{
			["ID"] = 160191,
			["Level"] = 56,
			["Link"] = "|cff1eff00|Hitem:160191::::::::50:253::11:1:4790:1:9:48:::|h[Crone-Seeker's Treads]|h|r",
		}, -- [8]
		{
			["ID"] = 158629,
			["Level"] = 58,
			["Link"] = "|cff1eff00|Hitem:158629::::::::50:253::11:1:4793:1:9:49:::|h[Crone-Seeker's Shackles]|h|r",
		}, -- [9]
		{
			["ID"] = 160195,
			["Level"] = 56,
			["Link"] = "|cff1eff00|Hitem:160195::::::::50:253::11:1:4793:1:9:48:::|h[Crone-Seeker's Mitts]|h|r",
		}, -- [10]
		{
			["ID"] = 155163,
			["Level"] = 58,
			["AlsoFitsIn"] = 12,
			["Link"] = "|cff1eff00|Hitem:155163::::::::50:253::11:1:4790:1:9:49:::|h[Rise-Breacher's Band]|h|r",
		}, -- [11]
		{
			["ID"] = 155162,
			["Level"] = 57,
			["AlsoFitsIn"] = 11,
			["Link"] = "|cff0070dd|Hitem:155162::::::::50:253::11:2:4793:4794:1:9:48:::|h[Band of Wortcunning]|h|r",
		}, -- [12]
		{
			["ID"] = 122361,
			["Level"] = 60,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122361::::::::56:253:::1:5805:::|h[Swift Hand of Justice]|h|r",
		}, -- [13]
		{
			["ID"] = 122361,
			["Level"] = 59,
			["AlsoFitsIn"] = 13,
			["Link"] = "|cff00ccff|Hitem:122361::::::::55:253:::1:5805:::|h[Swift Hand of Justice]|h|r",
		}, -- [14]
		{
			["ID"] = 122261,
			["Level"] = 60,
			["Link"] = "|cff00ccff|Hitem:122261::::::::56:253:::1:5805:::|h[Inherited Cape of the Black Baron]|h|r",
		}, -- [15]
		{
			["ID"] = 122366,
			["Level"] = 60,
			["Link"] = "|cff00ccff|Hitem:122366::::::::56:253:::1:582:::|h[Upgraded Dwarven Hand Cannon]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "HUNTER",
	["LastAdded"] = 1,
}
PawnClassicScaleProviderOptions = nil
