
VWQL = {
	["Iamthorr-Sargeras"] = {
		["Filter"] = 63,
		["VERSION"] = 94,
		["RegularQuestMode"] = true,
		["Quests"] = {
		},
		["FilterType"] = {
		},
	},
	["AnchorQCBTop"] = 741.9653930664062,
	["OppositeContinentNazjatar"] = false,
	["Scale"] = 0.8,
	["DisableIconsGeneralMap947"] = false,
	["Thorddin-Sargeras"] = {
		["Filter"] = 63,
		["FilterType"] = {
		},
		["Quests"] = {
		},
		["VERSION"] = 94,
	},
	["DisableCalligraphy"] = true,
	["Thordaris-Sargeras"] = {
		["VERSION"] = 94,
		["FilterType"] = {
		},
		["Quests"] = {
		},
		["Filter"] = 63,
	},
	["Thorjitsu-Sargeras"] = {
		["Filter"] = 63,
		["FilterType"] = {
		},
		["Quests"] = {
			[56141] = true,
			[51841] = true,
			[51297] = true,
			[56398] = true,
		},
		["VERSION"] = 92,
	},
	["Sort"] = 5,
	["VERSION"] = 95,
	["AnchorQCBLeft"] = 499.0577392578125,
	["DisableIconsGeneral"] = false,
	["ReverseSort"] = false,
	["Thorlexi-Sargeras"] = {
		["VERSION"] = 94,
		["FilterType"] = {
		},
		["Quests"] = {
		},
		["Filter"] = 63,
	},
	["ShowQuestAchievements"] = false,
	["DisableRewardIcons"] = false,
	["Sneakythorr-Sargeras"] = {
		["VERSION"] = 94,
		["Filter"] = 63,
		["RegularQuestMode"] = true,
		["Quests"] = {
		},
		["FilterType"] = {
		},
	},
	["AzeriteFormat"] = 20,
	["MapIconsScale"] = 1,
	["SortPrio"] = {
	},
	["Thormonde-Sargeras"] = {
		["VERSION"] = 94,
		["Filter"] = 63,
		["RegularQuestMode"] = true,
		["Quests"] = {
		},
		["FilterType"] = {
		},
	},
	["Ignore"] = {
	},
	["DisableLFG"] = true,
	["Thornna-Sargeras"] = {
		["azeriteIgnoreFilter"] = true,
		["VERSION"] = 95,
		["Filter"] = 38,
		["bountyIgnoreFilter"] = true,
		["Quests"] = {
			[53812] = true,
			[53877] = true,
			[52339] = true,
			[53784] = true,
			[52196] = true,
			[52475] = true,
		},
		["FilterType"] = {
			["expulsom"] = true,
			["manapearl"] = true,
		},
	},
	["Thorrend-Sargeras"] = {
		["VERSION"] = 94,
		["FilterType"] = {
		},
		["Quests"] = {
		},
		["Filter"] = 63,
	},
	["Ashtali-Sargeras"] = {
		["Filter"] = 63,
		["FilterType"] = {
		},
		["Quests"] = {
		},
		["VERSION"] = 94,
	},
	["DisableShellGame"] = false,
	["Thornoxnun-Sargeras"] = {
		["VERSION"] = 92,
		["Filter"] = 63,
		["RegularQuestMode"] = true,
		["Quests"] = {
		},
		["FilterType"] = {
		},
	},
	["Thordren-Sargeras"] = {
		["Filter"] = 63,
		["FilterType"] = {
		},
		["Quests"] = {
		},
		["VERSION"] = 95,
	},
	["HideLegion"] = true,
}
