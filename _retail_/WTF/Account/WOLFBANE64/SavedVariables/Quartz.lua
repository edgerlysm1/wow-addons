
Quartz3DB = {
	["namespaces"] = {
		["Swing"] = {
		},
		["Buff"] = {
		},
		["Interrupt"] = {
		},
		["Flight"] = {
		},
		["Pet"] = {
			["profiles"] = {
				["Default"] = {
					["y"] = 70.39961242675781,
					["x"] = 821.6002807617188,
				},
				["PALADIN"] = {
					["x"] = 635.863525390625,
				},
				["Healing"] = {
					["x"] = 1046.66650390625,
				},
				["Sargeras"] = {
					["x"] = 635.863525390625,
				},
				["Thornna - Sargeras"] = {
					["x"] = 635.863525390625,
				},
			},
		},
		["LibDualSpec-1.0"] = {
		},
		["Player"] = {
			["profiles"] = {
				["Default"] = {
					["h"] = 28,
					["w"] = 210,
					["hideicon"] = true,
					["x"] = -0.00018310546875,
					["point"] = "CENTER",
					["border"] = "None",
					["font"] = "2002",
					["texture"] = "Minimalist",
					["y"] = -139.2000427246094,
				},
				["PALADIN"] = {
					["x"] = 610.863525390625,
				},
				["Healing"] = {
					["x"] = 1021.66650390625,
				},
				["Sargeras"] = {
					["x"] = 610.863525390625,
				},
				["Thornna - Sargeras"] = {
					["x"] = 610.863525390625,
				},
			},
		},
		["EnemyCasts"] = {
		},
		["GCD"] = {
		},
		["Focus"] = {
			["profiles"] = {
				["Default"] = {
					["x"] = 1276,
				},
				["PALADIN"] = {
					["x"] = 635.863525390625,
				},
				["Healing"] = {
					["x"] = 1046.66650390625,
				},
				["Sargeras"] = {
					["x"] = 635.863525390625,
				},
				["Thornna - Sargeras"] = {
					["x"] = 635.863525390625,
				},
			},
		},
		["Target"] = {
			["profiles"] = {
				["Default"] = {
					["x"] = 1276,
				},
				["PALADIN"] = {
					["x"] = 635.863525390625,
				},
				["Healing"] = {
					["x"] = 1046.66650390625,
				},
				["Sargeras"] = {
					["x"] = 635.863525390625,
				},
				["Thornna - Sargeras"] = {
					["x"] = 635.863525390625,
				},
			},
		},
		["Mirror"] = {
		},
		["Range"] = {
		},
		["Latency"] = {
		},
	},
	["profileKeys"] = {
		["Laviceyi - Sargeras"] = "Default",
		["Thorpez - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Odintsun - Sargeras"] = "Default",
		["Saelorelea - Sargeras"] = "Default",
		["Thorpawz - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Sneakythorr - Sargeras"] = "Default",
		["Rosselis - Sargeras"] = "Default",
		["Haraklem - Sargeras"] = "Default",
		["Ochla - Sargeras"] = "Default",
		["Thordaris - Sargeras"] = "Default",
		["Senithor - Sargeras"] = "Default",
		["Ashtali - Sargeras"] = "Default",
		["Thorjitsu - Sargeras"] = "Healing",
		["Thornna - Sargeras"] = "Default",
		["Tircathas - Sargeras"] = "Default",
		["Thorlexi - Sargeras"] = "Default",
		["Thornarii - Sargeras"] = "Default",
		["Thormonde - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
		["Thorraddin - Sargeras"] = "Default",
		["Iamthorr - Sargeras"] = "Default",
		["Thorjutsu - Sargeras"] = "Default",
		["Norvah - Sargeras"] = "Default",
		["Eiahu - Sargeras"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
		["Aatala - Sargeras"] = "Default",
		["Anthienisse - Illidan"] = "Default",
		["Thorwyyn - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Marraydel - Sargeras"] = "Default",
		["Nntailfa - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Healing",
	},
	["profiles"] = {
		["Default"] = {
			["modules"] = {
				["Tradeskill"] = false,
				["Buff"] = false,
				["Interrupt"] = false,
				["Flight"] = false,
				["Pet"] = false,
				["Mirror"] = false,
				["Range"] = false,
				["GCD"] = false,
				["Focus"] = false,
				["Target"] = false,
				["Timer"] = false,
				["Swing"] = false,
				["Latency"] = false,
			},
		},
		["PALADIN"] = {
		},
		["Thormonde - Sargeras"] = {
		},
		["Healing"] = {
		},
		["Sargeras"] = {
		},
		["Thornna - Sargeras"] = {
		},
	},
}
