
LootAppraiserDB = {
	["global"] = {
		["sessions"] = {
			{
				["noteworthyItems"] = {
					["124442"] = 1,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMarketAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1502987274,
				["start"] = 1502987235,
				["liv"] = 2624720,
				["totalItemsLooted"] = 1,
				["player"] = "Sairrus-Sargeras",
			}, -- [1]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["start"] = 1503007356,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [2]
			{
				["noteworthyItems"] = {
					["130183"] = 3,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503022177,
				["start"] = 1503022031,
				["liv"] = 32074015,
				["totalItemsLooted"] = 20,
				["player"] = "Thorddin-Sargeras",
			}, -- [3]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503024156,
				["start"] = 1503024111,
				["liv"] = 9000,
				["totalItemsLooted"] = 9,
				["player"] = "Thorddin-Sargeras",
			}, -- [4]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503024797,
				["start"] = 1503024255,
				["liv"] = 66013658,
				["totalItemsLooted"] = 103,
				["player"] = "Ashtali-Sargeras",
			}, -- [5]
			{
				["noteworthyItems"] = {
					["124442"] = 1,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503025008,
				["start"] = 1503024826,
				["liv"] = 14572217,
				["totalItemsLooted"] = 19,
				["player"] = "Ashtali-Sargeras",
			}, -- [6]
			{
				["noteworthyItems"] = {
					["130181"] = 1,
					["130183"] = 1,
					["130180"] = 3,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503056411,
				["start"] = 1503055975,
				["liv"] = 77935066,
				["totalItemsLooted"] = 24,
				["player"] = "Thorddin-Sargeras",
			}, -- [7]
			{
				["noteworthyItems"] = {
					["130179"] = 5,
					["130183"] = 4,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503059902,
				["start"] = 1503059584,
				["liv"] = 72049114,
				["totalItemsLooted"] = 44,
				["player"] = "Thorddin-Sargeras",
			}, -- [8]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503060900,
				["start"] = 1503060785,
				["liv"] = 38000,
				["totalItemsLooted"] = 38,
				["player"] = "Thorddin-Sargeras",
			}, -- [9]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503061752,
				["start"] = 1503061726,
				["liv"] = 4102524,
				["totalItemsLooted"] = 9,
				["player"] = "Sairrus-Sargeras",
			}, -- [10]
			{
				["noteworthyItems"] = {
					["141287"] = 1,
				},
				["mapID"] = 1017,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503064881,
				["start"] = 1503064876,
				["liv"] = 5840328,
				["totalItemsLooted"] = 7,
				["player"] = "Ashtali-Sargeras",
			}, -- [11]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["end"] = 1503066209,
				["start"] = 1503066204,
				["liv"] = 683754,
				["totalItemsLooted"] = 1,
				["player"] = "Ashtali-Sargeras",
			}, -- [12]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1033,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["start"] = 1503066598,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [13]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 2,
					["gat"] = 100,
				},
				["start"] = 1503068681,
				["liv"] = 0,
				["player"] = "Ashtali-Sargeras",
			}, -- [14]
			{
				["noteworthyItems"] = {
					["129034"] = 4,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503068951,
				["start"] = 1503068768,
				["liv"] = 36848268,
				["totalItemsLooted"] = 241,
				["player"] = "Ashtali-Sargeras",
			}, -- [15]
			{
				["noteworthyItems"] = {
					["129034"] = 4,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBGlobalMinBuyoutAvg",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503069263,
				["start"] = 1503069034,
				["liv"] = 19243052,
				["totalItemsLooted"] = 113,
				["player"] = "Ashtali-Sargeras",
			}, -- [16]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1021,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503073584,
				["start"] = 1503071656,
				["liv"] = 575628,
				["totalItemsLooted"] = 97,
				["player"] = "Ashtali-Sargeras",
			}, -- [17]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1046,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503079178,
				["start"] = 1503076523,
				["liv"] = 1989799,
				["totalItemsLooted"] = 34,
				["player"] = "Ashtali-Sargeras",
			}, -- [18]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503080443,
				["start"] = 1503080205,
				["liv"] = 35000,
				["totalItemsLooted"] = 35,
				["player"] = "Ashtali-Sargeras",
			}, -- [19]
			{
				["noteworthyItems"] = {
					["130181"] = 5,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503286835,
				["start"] = 1503286631,
				["liv"] = 149379750,
				["totalItemsLooted"] = 50,
				["player"] = "Thorddin-Sargeras",
			}, -- [20]
			{
				["noteworthyItems"] = {
					["130183"] = 2,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503317727,
				["start"] = 1503317428,
				["liv"] = 17886242,
				["totalItemsLooted"] = 71,
				["player"] = "Thorddin-Sargeras",
			}, -- [21]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503317963,
				["start"] = 1503317898,
				["liv"] = 6160000,
				["totalItemsLooted"] = 14,
				["player"] = "Ashtali-Sargeras",
			}, -- [22]
			{
				["noteworthyItems"] = {
					["130181"] = 5,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503318344,
				["start"] = 1503318141,
				["liv"] = 145421250,
				["totalItemsLooted"] = 38,
				["player"] = "Thorddin-Sargeras",
			}, -- [23]
			{
				["noteworthyItems"] = {
					["130180"] = 3,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503318581,
				["start"] = 1503318344,
				["liv"] = 17748604,
				["totalItemsLooted"] = 57,
				["player"] = "Thorddin-Sargeras",
			}, -- [24]
			{
				["noteworthyItems"] = {
					["130179"] = 3,
					["130181"] = 3,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503318862,
				["start"] = 1503318581,
				["liv"] = 95366116,
				["totalItemsLooted"] = 48,
				["player"] = "Thorddin-Sargeras",
			}, -- [25]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["start"] = 1503318862,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [26]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503333582,
				["start"] = 1503333506,
				["liv"] = 27000,
				["totalItemsLooted"] = 27,
				["player"] = "Thorddin-Sargeras",
			}, -- [27]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 341,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["start"] = 1503371400,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [28]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503430976,
				["start"] = 1503430849,
				["liv"] = 805494,
				["totalItemsLooted"] = 8,
				["player"] = "Thorddin-Sargeras",
			}, -- [29]
			{
				["noteworthyItems"] = {
					["130183"] = 2,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 100,
				},
				["end"] = 1503455340,
				["start"] = 1503454468,
				["liv"] = 8574169,
				["totalItemsLooted"] = 16,
				["player"] = "Thorddin-Sargeras",
			}, -- [30]
			{
				["noteworthyItems"] = {
					["130179"] = 1,
					["130183"] = 2,
					["130181"] = 2,
					["130180"] = 2,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["end"] = 1503456725,
				["start"] = 1503455377,
				["liv"] = 82215789,
				["totalItemsLooted"] = 121,
				["player"] = "Thorddin-Sargeras",
			}, -- [31]
			{
				["noteworthyItems"] = {
					["130179"] = 1,
					["130178"] = 1,
					["130180"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["end"] = 1503457913,
				["start"] = 1503457516,
				["liv"] = 9087658,
				["totalItemsLooted"] = 51,
				["player"] = "Thorddin-Sargeras",
			}, -- [32]
			{
				["noteworthyItems"] = {
					["130183"] = 3,
					["130178"] = 8,
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["end"] = 1503458546,
				["start"] = 1503458152,
				["liv"] = 43584633,
				["totalItemsLooted"] = 116,
				["player"] = "Thorddin-Sargeras",
			}, -- [33]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["start"] = 1503492609,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [34]
			{
				["noteworthyItems"] = {
					["130183"] = 5,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["end"] = 1503577293,
				["start"] = 1503576768,
				["liv"] = 43200495,
				["totalItemsLooted"] = 85,
				["player"] = "Thorddin-Sargeras",
			}, -- [35]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["end"] = 1503579328,
				["start"] = 1503577386,
				["liv"] = 638000,
				["totalItemsLooted"] = 638,
				["player"] = "Ashtali-Sargeras",
			}, -- [36]
			{
				["noteworthyItems"] = {
					["130178"] = 2,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1503591284,
				["start"] = 1503591161,
				["liv"] = 4002998,
				["totalItemsLooted"] = 21,
				["player"] = "Thorddin-Sargeras",
			}, -- [37]
			{
				["noteworthyItems"] = {
					["124442"] = 2,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1503591407,
				["start"] = 1503591361,
				["liv"] = 5399998,
				["totalItemsLooted"] = 2,
				["player"] = "Ashtali-Sargeras",
			}, -- [38]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1177,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1503615238,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [39]
			{
				["noteworthyItems"] = {
					["124442"] = 1,
				},
				["mapID"] = 1081,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1503626609,
				["start"] = 1503626525,
				["liv"] = 2862499,
				["totalItemsLooted"] = 3,
				["player"] = "Ashtali-Sargeras",
			}, -- [40]
			{
				["noteworthyItems"] = {
					["130183"] = 1,
					["130180"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1503629292,
				["start"] = 1503629062,
				["liv"] = 8348999,
				["totalItemsLooted"] = 20,
				["player"] = "Thorddin-Sargeras",
			}, -- [41]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1503664426,
				["start"] = 1503664336,
				["liv"] = 259999,
				["totalItemsLooted"] = 13,
				["player"] = "Thorddin-Sargeras",
			}, -- [42]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504310417,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [43]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504312528,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [44]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1171,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1504356176,
				["start"] = 1504354014,
				["liv"] = 200000,
				["totalItemsLooted"] = 26,
				["player"] = "Sairrus-Sargeras",
			}, -- [45]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504541338,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [46]
			{
				["noteworthyItems"] = {
					["124442"] = 1,
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1504551278,
				["start"] = 1504545121,
				["liv"] = 2050000,
				["totalItemsLooted"] = 167,
				["player"] = "Sairrus-Sargeras",
			}, -- [47]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504652292,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [48]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1171,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504664140,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [49]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504665239,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [50]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504715817,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [51]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1018,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["end"] = 1504744345,
				["start"] = 1504739947,
				["liv"] = 792231,
				["totalItemsLooted"] = 699,
				["player"] = "Ashtali-Sargeras",
			}, -- [52]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504790126,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [53]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 3,
					["gat"] = 50,
				},
				["start"] = 1504818369,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [54]
			{
				["noteworthyItems"] = {
					["151564"] = 17,
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 50,
				},
				["end"] = 1504917630,
				["start"] = 1504913625,
				["liv"] = 197253887,
				["totalItemsLooted"] = 327,
				["player"] = "Sairrus-Sargeras",
			}, -- [55]
			{
				["noteworthyItems"] = {
					["151721"] = 1,
					["151720"] = 1,
					["151718"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1504928412,
				["start"] = 1504928266,
				["liv"] = 69082978,
				["totalItemsLooted"] = 25,
				["player"] = "Thorddin-Sargeras",
			}, -- [56]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1504971091,
				["start"] = 1504967388,
				["liv"] = 275362786,
				["totalItemsLooted"] = 443,
				["player"] = "Sairrus-Sargeras",
			}, -- [57]
			{
				["noteworthyItems"] = {
					["151720"] = 4,
					["151721"] = 1,
					["151718"] = 4,
					["151722"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1504974616,
				["start"] = 1504973356,
				["liv"] = 187016100,
				["totalItemsLooted"] = 148,
				["player"] = "Thorddin-Sargeras",
			}, -- [58]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1504988375,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [59]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505016923,
				["start"] = 1505016397,
				["liv"] = 5892993,
				["totalItemsLooted"] = 15,
				["player"] = "Sairrus-Sargeras",
			}, -- [60]
			{
				["noteworthyItems"] = {
					["151579"] = 1,
					["151722"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505021087,
				["start"] = 1505020980,
				["liv"] = 17431999,
				["totalItemsLooted"] = 13,
				["player"] = "Thorddin-Sargeras",
			}, -- [61]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505056264,
				["start"] = 1505049046,
				["liv"] = 468713234,
				["totalItemsLooted"] = 818,
				["player"] = "Sairrus-Sargeras",
			}, -- [62]
			{
				["noteworthyItems"] = {
					["151719"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505056653,
				["start"] = 1505056543,
				["liv"] = 31197920,
				["totalItemsLooted"] = 21,
				["player"] = "Thorddin-Sargeras",
			}, -- [63]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1147,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505098255,
				["start"] = 1505097199,
				["liv"] = 588036,
				["totalItemsLooted"] = 1,
				["player"] = "Sairrus-Sargeras",
			}, -- [64]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505179604,
				["start"] = 1505178535,
				["liv"] = 71893373,
				["totalItemsLooted"] = 134,
				["player"] = "Sairrus-Sargeras",
			}, -- [65]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505270280,
				["start"] = 1505262945,
				["liv"] = 9844850,
				["totalItemsLooted"] = 93,
				["player"] = "Sairrus-Sargeras",
			}, -- [66]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505352288,
				["start"] = 1505347876,
				["liv"] = 393552460,
				["totalItemsLooted"] = 641,
				["player"] = "Sairrus-Sargeras",
			}, -- [67]
			{
				["noteworthyItems"] = {
					["151719"] = 1,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505353562,
				["start"] = 1505352962,
				["liv"] = 55237909,
				["totalItemsLooted"] = 50,
				["player"] = "Thorddin-Sargeras",
			}, -- [68]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1068,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1505537556,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [69]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505589772,
				["start"] = 1505588297,
				["liv"] = 55290770,
				["totalItemsLooted"] = 133,
				["player"] = "Sairrus-Sargeras",
			}, -- [70]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505660395,
				["start"] = 1505656657,
				["liv"] = 251446500,
				["totalItemsLooted"] = 630,
				["player"] = "Sairrus-Sargeras",
			}, -- [71]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1505870369,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [72]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1015,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1505949089,
				["start"] = 1505945155,
				["liv"] = 9324221,
				["totalItemsLooted"] = 169,
				["player"] = "Sairrus-Sargeras",
			}, -- [73]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1018,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506182318,
				["start"] = 1506178216,
				["liv"] = 10379117,
				["totalItemsLooted"] = 183,
				["player"] = "Sairrus-Sargeras",
			}, -- [74]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1506265692,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [75]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1147,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506306758,
				["start"] = 1506302545,
				["liv"] = 2084513,
				["totalItemsLooted"] = 11,
				["player"] = "Sairrus-Sargeras",
			}, -- [76]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1506356602,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [77]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1024,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506387990,
				["start"] = 1506387286,
				["liv"] = 155398,
				["totalItemsLooted"] = 8,
				["player"] = "Sairrus-Sargeras",
			}, -- [78]
			{
				["noteworthyItems"] = {
					["152998"] = 1,
				},
				["mapID"] = 1170,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506479024,
				["start"] = 1506476586,
				["liv"] = 27109372,
				["totalItemsLooted"] = 75,
				["player"] = "Sairrus-Sargeras",
			}, -- [79]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1506553439,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [80]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506656832,
				["start"] = 1506654025,
				["liv"] = 717000,
				["totalItemsLooted"] = 717,
				["player"] = "Sairrus-Sargeras",
			}, -- [81]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506783278,
				["start"] = 1506783094,
				["liv"] = 2151000,
				["totalItemsLooted"] = 2151,
				["player"] = "Sairrus-Sargeras",
			}, -- [82]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506816806,
				["start"] = 1506814628,
				["liv"] = 349000,
				["totalItemsLooted"] = 349,
				["player"] = "Sairrus-Sargeras",
			}, -- [83]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506857268,
				["start"] = 1506851857,
				["liv"] = 214274,
				["totalItemsLooted"] = 4,
				["player"] = "Sairrus-Sargeras",
			}, -- [84]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506865666,
				["start"] = 1506863764,
				["liv"] = 7899290,
				["totalItemsLooted"] = 91,
				["player"] = "Sairrus-Sargeras",
			}, -- [85]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1147,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1506913356,
				["start"] = 1506910419,
				["liv"] = 610242,
				["totalItemsLooted"] = 3,
				["player"] = "Sairrus-Sargeras",
			}, -- [86]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1507063607,
				["start"] = 1507059741,
				["liv"] = 178088,
				["totalItemsLooted"] = 6,
				["player"] = "Sairrus-Sargeras",
			}, -- [87]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1507596689,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [88]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1507598231,
				["start"] = 1507597812,
				["liv"] = 31391834,
				["totalItemsLooted"] = 22,
				["player"] = "Sairrus-Sargeras",
			}, -- [89]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1017,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1507868633,
				["start"] = 1507866452,
				["liv"] = 13694149,
				["totalItemsLooted"] = 51,
				["player"] = "Sairrus-Sargeras",
			}, -- [90]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1507985642,
				["start"] = 1507985298,
				["liv"] = 3316228,
				["totalItemsLooted"] = 11,
				["player"] = "Sairrus-Sargeras",
			}, -- [91]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1135,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1507988404,
				["start"] = 1507985642,
				["liv"] = 108961116,
				["totalItemsLooted"] = 340,
				["player"] = "Sairrus-Sargeras",
			}, -- [92]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1033,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1507996281,
				["start"] = 1507993225,
				["liv"] = 8879000,
				["totalItemsLooted"] = 93,
				["player"] = "Sairrus-Sargeras",
			}, -- [93]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 529,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508193725,
				["start"] = 1508191637,
				["liv"] = 874125,
				["totalItemsLooted"] = 11,
				["player"] = "Sairrus-Sargeras",
			}, -- [94]
			{
				["noteworthyItems"] = {
					["121071"] = 1,
				},
				["mapID"] = 1018,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508212584,
				["start"] = 1508204516,
				["liv"] = 24544395,
				["totalItemsLooted"] = 216,
				["player"] = "Sairrus-Sargeras",
			}, -- [95]
			{
				["noteworthyItems"] = {
					["141291"] = 1,
				},
				["mapID"] = 1067,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508284428,
				["start"] = 1508280576,
				["liv"] = 7626199,
				["totalItemsLooted"] = 62,
				["player"] = "Sairrus-Sargeras",
			}, -- [96]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508299162,
				["start"] = 1508299161,
				["liv"] = 326796,
				["totalItemsLooted"] = 2,
				["player"] = "Sairrus-Sargeras",
			}, -- [97]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1015,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508379807,
				["start"] = 1508376330,
				["liv"] = 1651037,
				["totalItemsLooted"] = 60,
				["player"] = "Sairrus-Sargeras",
			}, -- [98]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1508420035,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [99]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1508524240,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [100]
			{
				["noteworthyItems"] = {
					["141285"] = 1,
				},
				["mapID"] = 1015,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508685441,
				["start"] = 1508682406,
				["liv"] = 8587500,
				["totalItemsLooted"] = 102,
				["player"] = "Sairrus-Sargeras",
			}, -- [101]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1024,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508687918,
				["start"] = 1508685685,
				["liv"] = 10896564,
				["totalItemsLooted"] = 69,
				["player"] = "Sairrus-Sargeras",
			}, -- [102]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508698639,
				["start"] = 1508695489,
				["liv"] = 2205011,
				["totalItemsLooted"] = 4,
				["player"] = "Sairrus-Sargeras",
			}, -- [103]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1170,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1508800470,
				["start"] = 1508797482,
				["liv"] = 9880000,
				["totalItemsLooted"] = 704,
				["player"] = "Sairrus-Sargeras",
			}, -- [104]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1509052625,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [105]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1509546058,
				["start"] = 1509545586,
				["liv"] = 54000,
				["totalItemsLooted"] = 54,
				["player"] = "Sairrus-Sargeras",
			}, -- [106]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1509585536,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [107]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1509656741,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [108]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 261,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1509657680,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [109]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 261,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1509930724,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [110]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 261,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510016581,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [111]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1510019905,
				["start"] = 1510019138,
				["liv"] = 16399,
				["totalItemsLooted"] = 1,
				["player"] = "Thorddin-Sargeras",
			}, -- [112]
			{
				["noteworthyItems"] = {
					["151720"] = 3,
					["151579"] = 2,
					["151721"] = 5,
					["151722"] = 3,
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1510059868,
				["start"] = 1510059572,
				["liv"] = 63515221,
				["totalItemsLooted"] = 90,
				["player"] = "Thorddin-Sargeras",
			}, -- [113]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1510062352,
				["start"] = 1510062349,
				["liv"] = 52000,
				["totalItemsLooted"] = 52,
				["player"] = "Sairrus-Sargeras",
			}, -- [114]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510063521,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [115]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 261,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510178294,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [116]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510234893,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [117]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510551928,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [118]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510582948,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [119]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510838570,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [120]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1510922725,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [121]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 261,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1511019931,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [122]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1511142130,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [123]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = -1,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1511216580,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [124]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1511483619,
				["start"] = 1511478039,
				["liv"] = 2772000,
				["totalItemsLooted"] = 2772,
				["player"] = "Ashtali-Sargeras",
			}, -- [125]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1512912972,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [126]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1014,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1512923230,
				["liv"] = 0,
				["player"] = "Thorddin-Sargeras",
			}, -- [127]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1512924615,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [128]
			{
				["noteworthyItems"] = {
					["141578"] = 1,
				},
				["mapID"] = 1017,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1513390539,
				["start"] = 1513383950,
				["liv"] = 42093561,
				["totalItemsLooted"] = 133,
				["player"] = "Sairrus-Sargeras",
			}, -- [129]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 301,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["end"] = 1513476484,
				["start"] = 1513476483,
				["liv"] = 1599999,
				["totalItemsLooted"] = 1,
				["player"] = "Sairrus-Sargeras",
			}, -- [130]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 341,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1514729817,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [131]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 261,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1515862101,
				["liv"] = 0,
				["player"] = "Sairrus-Sargeras",
			}, -- [132]
			{
				["noteworthyItems"] = {
				},
				["mapID"] = 1161,
				["settings"] = {
					["priceSource"] = "DBMinBuyout",
					["qualityFilter"] = 1,
					["gat"] = 250,
				},
				["start"] = 1536504196,
				["liv"] = 0,
				["livGroup"] = 0,
				["player"] = "Thornna-Sargeras",
			}, -- [133]
		},
		["drops"] = {
			[151720] = {
				[1014] = 8,
			},
			[121155] = {
				[1041] = 1,
			},
			[130178] = {
				[1014] = 3,
				[1177] = 8,
			},
			[130180] = {
				[1014] = 8,
				[1177] = 2,
			},
			[151721] = {
				[1014] = 7,
			},
			[141285] = {
				[1015] = 1,
			},
			[151718] = {
				[1014] = 5,
			},
			[151722] = {
				[1014] = 5,
			},
			[124442] = {
				[1081] = 1,
				[1135] = 1,
				[1014] = 4,
				[1177] = 2,
			},
			[130179] = {
				[1177] = 6,
				[1014] = 4,
			},
			[130181] = {
				[1014] = 14,
				[1177] = 2,
			},
			[141291] = {
				[1067] = 1,
			},
			[121071] = {
				[1018] = 1,
			},
			[141578] = {
				[1017] = 1,
			},
			[151564] = {
				[1135] = 17,
			},
			[129034] = {
				[1177] = 8,
			},
			[152998] = {
				[1170] = 1,
			},
			[151579] = {
				[1014] = 3,
			},
			[141287] = {
				[1017] = 1,
			},
			[151719] = {
				[1014] = 2,
			},
			[130183] = {
				[1177] = 14,
				[1014] = 9,
			},
		},
	},
	["profileKeys"] = {
		["Thornarii - Sargeras"] = "Default",
		["Liashta - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
		["Thormonde - Sargeras"] = "Default",
		["Tralerodora - Illidan"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
		["Norial - Illidan"] = "Default",
		["Ashtali - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Odintsun - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Thorend - Illidan"] = "Default",
		["Thorwyyn - Sargeras"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["general"] = {
				["qualityFilter"] = "1",
				["goldAlertThreshold"] = "250",
			},
			["mainUI"] = {
				["top"] = 536.831115722656,
				["height"] = 266.948364257813,
				["left"] = 105.067665100098,
				["width"] = 410.000030517578,
			},
			["pricesource"] = {
				["source"] = "DBMinBuyout",
			},
			["startSessionPromptUI"] = {
				["height"] = 115,
				["top"] = 70.404296875,
				["left"] = 951.2880859375,
				["width"] = 249.999984741211,
			},
		},
	},
}
LALootDB = {
	["profileKeys"] = {
		["Thornarii - Sargeras"] = "Default",
		["Odintsun - Sargeras"] = "Default",
	},
}
