
Gladius2DB = {
	["profileKeys"] = {
		["Thorpez - Sargeras"] = "Thorpez - Sargeras",
		["Thorwin - Sargeras"] = "Thorwin - Sargeras",
		["Thorggar - Sargeras"] = "Thorggar - Sargeras",
		["Odintsun - Sargeras"] = "Odintsun - Sargeras",
		["Saelorelea - Sargeras"] = "Saelorelea - Sargeras",
		["Thorwyyn - Sargeras"] = "Thorwyyn - Sargeras",
		["Thordren - Sargeras"] = "Thordren - Sargeras",
		["Sneakythorr - Sargeras"] = "Sneakythorr - Sargeras",
		["Rosselis - Sargeras"] = "Rosselis - Sargeras",
		["Haraklem - Sargeras"] = "Haraklem - Sargeras",
		["Ochla - Sargeras"] = "Ochla - Sargeras",
		["Thordaris - Sargeras"] = "Thordaris - Sargeras",
		["Senithor - Sargeras"] = "Senithor - Sargeras",
		["Aquyssaelea - Sargeras"] = "Aquyssaelea - Sargeras",
		["Ashtali - Sargeras"] = "Ashtali - Sargeras",
		["Thorjitsu - Sargeras"] = "Thorjitsu - Sargeras",
		["Thornna - Sargeras"] = "Thornna - Sargeras",
		["Tircathas - Sargeras"] = "Tircathas - Sargeras",
		["Thorlexi - Sargeras"] = "Thorlexi - Sargeras",
		["Thornarii - Sargeras"] = "Thornarii - Sargeras",
		["Thormonde - Sargeras"] = "Thormonde - Sargeras",
		["Sairrus - Sargeras"] = "Sairrus - Sargeras",
		["Aqulaylaszun - Sargeras"] = "Aqulaylaszun - Sargeras",
		["Iamthorr - Sargeras"] = "Iamthorr - Sargeras",
		["Thorjutsu - Sargeras"] = "Thorjutsu - Sargeras",
		["Thorrand - Sargeras"] = "Thorrand - Sargeras",
		["Thorraddin - Sargeras"] = "Thorraddin - Sargeras",
		["Thornoxnun - Sargeras"] = "Thornoxnun - Sargeras",
		["Aatala - Sargeras"] = "Aatala - Sargeras",
		["Anthienisse - Illidan"] = "Anthienisse - Illidan",
		["Norvah - Sargeras"] = "Norvah - Sargeras",
		["Thorpawz - Sargeras"] = "Thorpawz - Sargeras",
		["Thorddin - Sargeras"] = "Thorddin - Sargeras",
		["Nntailfa - Sargeras"] = "Nntailfa - Sargeras",
		["Thorrend - Sargeras"] = "Thorrend - Sargeras",
	},
	["profiles"] = {
		["Thorpez - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
		},
		["Thorwin - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 947.492793369602,
			},
			["y"] = {
				["arena1"] = 482.812638086152,
			},
		},
		["Thorggar - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 544.466499571998,
			},
			["x"] = {
				["arena1"] = 1228.85866919937,
			},
		},
		["Odintsun - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 479.7332906643533,
			},
			["x"] = {
				["arena1"] = 1460.799848222727,
			},
		},
		["Saelorelea - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1083.257934570313,
			},
			["y"] = {
				["arena1"] = 549.5989990234375,
			},
		},
		["Thorwyyn - Sargeras"] = {
			["y"] = {
				["arena1"] = 490.9333235502236,
			},
			["tagsVersion"] = 4,
			["auraVersion"] = 1,
			["x"] = {
				["arena1"] = 1448.000205914184,
			},
		},
		["Thordren - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 998.066068018845,
			},
			["y"] = {
				["arena1"] = 564.356547768533,
			},
		},
		["Sneakythorr - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1298.44580080182,
			},
			["y"] = {
				["arena1"] = 74.2914516032602,
			},
		},
		["Rosselis - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1250.666622559227,
			},
			["y"] = {
				["arena1"] = 436.5332568248086,
			},
		},
		["Haraklem - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1126.532470703125,
			},
			["y"] = {
				["arena1"] = 496.75634765625,
			},
		},
		["Ochla - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1311.72705078125,
			},
			["y"] = {
				["arena1"] = 489.4199523925781,
			},
		},
		["Thordaris - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1104.000442253222,
			},
			["y"] = {
				["arena1"] = 457.6000468121638,
			},
		},
		["Senithor - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
		},
		["Aquyssaelea - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1019.70262916037,
			},
			["y"] = {
				["arena1"] = 386.402635390376,
			},
		},
		["Ashtali - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1159.995727539063,
			},
			["y"] = {
				["arena1"] = 511.2156677246094,
			},
		},
		["Thorjitsu - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1073.70990650682,
			},
			["y"] = {
				["arena1"] = 552.789603304787,
			},
		},
		["Thornna - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 471.6196429616102,
			},
			["x"] = {
				["arena1"] = 1071.508658235529,
			},
		},
		["Tircathas - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1102.984375,
			},
			["y"] = {
				["arena1"] = 435.6137390136719,
			},
		},
		["Thorlexi - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 521.955570295122,
			},
			["x"] = {
				["arena1"] = 1120.711488384673,
			},
		},
		["Thornarii - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 445.0665008942233,
			},
			["x"] = {
				["arena1"] = 1137.599724658321,
			},
		},
		["Thormonde - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 465.866550342238,
			},
			["x"] = {
				["arena1"] = 1065.601594257401,
			},
		},
		["Sairrus - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
		},
		["Aqulaylaszun - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 523.006634883816,
			},
			["x"] = {
				["arena1"] = 987.140027789836,
			},
		},
		["Iamthorr - Sargeras"] = {
			["y"] = {
				["arena1"] = 401.8667111953109,
			},
			["tagsVersion"] = 4,
			["auraVersion"] = 1,
			["x"] = {
				["arena1"] = 1286.399940681455,
			},
		},
		["Thorjutsu - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1209.394248697761,
			},
			["y"] = {
				["arena1"] = 491.945976595598,
			},
		},
		["Thorrand - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 512.145710301411,
			},
			["x"] = {
				["arena1"] = 1082.73630895099,
			},
		},
		["Thorraddin - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 471.420750037914,
			},
			["x"] = {
				["arena1"] = 918.605737984635,
			},
		},
		["Thornoxnun - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1304.533648904173,
			},
			["y"] = {
				["arena1"] = 409.8666707436241,
			},
		},
		["Aatala - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1327.466755056383,
			},
			["y"] = {
				["arena1"] = 430.1332322200105,
			},
		},
		["Anthienisse - Illidan"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
		},
		["Norvah - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 330.9334205071154,
			},
			["x"] = {
				["arena1"] = 1576.533901151037,
			},
		},
		["Thorpawz - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 454.5396083899395,
			},
			["x"] = {
				["arena1"] = 1165.403489764067,
			},
		},
		["Thorddin - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1147.04150390625,
			},
			["y"] = {
				["arena1"] = 535.1373291015625,
			},
		},
		["Nntailfa - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["x"] = {
				["arena1"] = 1150.906982421875,
			},
			["y"] = {
				["arena1"] = 504.6057434082031,
			},
		},
		["Thorrend - Sargeras"] = {
			["auraVersion"] = 1,
			["tagsVersion"] = 4,
			["y"] = {
				["arena1"] = 548.3709106445312,
			},
			["x"] = {
				["arena1"] = 1070.976440429688,
			},
		},
	},
}
