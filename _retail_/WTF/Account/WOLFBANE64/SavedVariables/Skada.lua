
SkadaDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["profileKeys"] = {
		["Ashtali - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["showself"] = false,
			["modeclicks"] = {
				["Healing"] = 1,
				["DPS"] = 4,
				["Damage"] = 2,
			},
			["modulesBlocked"] = {
				["Deaths"] = true,
				["CC"] = true,
				["Interrupts"] = true,
				["Power"] = true,
				["Debuffs"] = true,
				["Threat"] = true,
				["Enemies"] = true,
			},
			["windows"] = {
				{
					["barheight"] = 15,
					["classicons"] = false,
					["hidden"] = true,
					["y"] = 163.445785522461,
					["x"] = -271.916412353516,
					["title"] = {
						["font"] = "Diablo",
						["texture"] = "Kui status bar (brighter)",
					},
					["barfontflags"] = "OUTLINE",
					["background"] = {
						["height"] = 125.711135864258,
						["borderthickness"] = 2.5,
						["color"] = {
							["a"] = 1,
							["b"] = 1,
							["g"] = 1,
							["r"] = 1,
						},
						["texture"] = "Blizzard Garrison Background 2",
					},
					["point"] = "BOTTOM",
					["barfontsize"] = 11,
					["mode"] = "Damage",
					["bartexture"] = "Kui status bar (brighter)",
					["barwidth"] = 244.51106262207,
					["modeincombat"] = "Damage",
					["barfont"] = "Diablo",
				}, -- [1]
			},
			["icon"] = {
				["hide"] = true,
			},
			["report"] = {
				["channel"] = "instance_chat",
			},
			["columns"] = {
				["Damage_Damage"] = true,
				["Damage_DPS"] = true,
			},
			["updatefrequency"] = 0.1,
			["versions"] = {
				["1.6.7"] = true,
				["1.6.4"] = true,
				["1.6.3"] = true,
			},
			["setformat"] = 2,
			["reset"] = {
				["instance"] = 3,
			},
		},
	},
}
