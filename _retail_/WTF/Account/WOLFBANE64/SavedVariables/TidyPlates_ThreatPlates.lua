
ThreatPlatesDB = {
	["char"] = {
		["Thordaris - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Senithor - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Thorpez - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Thorwin - Sargeras"] = {
			["spec"] = {
				nil, -- [1]
				nil, -- [2]
				true, -- [3]
				false, -- [4]
			},
			["welcome"] = true,
		},
		["Ashtali - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				nil, -- [1]
				true, -- [2]
			},
		},
		["Thorjitsu - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				true, -- [1]
				[3] = false,
			},
		},
		["Thornna - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				nil, -- [1]
				true, -- [2]
				false, -- [3]
			},
		},
		["Odintsun - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				nil, -- [1]
				true, -- [2]
				false, -- [3]
			},
		},
		["Thorlexi - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Thornarii - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Thorwyyn - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Thordren - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Iamthorr - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Sneakythorr - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Thorddin - Sargeras"] = {
			["spec"] = {
				nil, -- [1]
				true, -- [2]
				false, -- [3]
			},
			["welcome"] = true,
		},
		["Thorraddin - Sargeras"] = {
			["spec"] = {
				nil, -- [1]
				true, -- [2]
				false, -- [3]
			},
			["welcome"] = true,
		},
		["Thornoxnun - Sargeras"] = {
			["spec"] = {
				true, -- [1]
				[3] = false,
			},
			["welcome"] = true,
		},
		["Aatala - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Norvah - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Thorrend - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = true,
			},
		},
		["Sairrus - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Rosselis - Sargeras"] = {
			["spec"] = {
				[3] = false,
			},
			["welcome"] = true,
		},
		["Thormonde - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Thorpawz - Sargeras"] = {
			["welcome"] = true,
			["spec"] = {
				nil, -- [1]
				nil, -- [2]
				true, -- [3]
				false, -- [4]
			},
		},
	},
	["profileKeys"] = {
		["Thordaris - Sargeras"] = "Default",
		["Senithor - Sargeras"] = "Default",
		["Thorpez - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Ashtali - Sargeras"] = "Default",
		["Thorjitsu - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Odintsun - Sargeras"] = "Default",
		["Thorlexi - Sargeras"] = "Default",
		["Thornarii - Sargeras"] = "Default",
		["Thorwyyn - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Iamthorr - Sargeras"] = "Default",
		["Sneakythorr - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Thorraddin - Sargeras"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
		["Aatala - Sargeras"] = "Default",
		["Norvah - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
		["Rosselis - Sargeras"] = "Default",
		["Thormonde - Sargeras"] = "Default",
		["Thorpawz - Sargeras"] = "Default",
	},
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["global"] = {
		["CheckNewLookAndFeel"] = true,
		["version"] = "10.1.5",
	},
	["profiles"] = {
		["Default"] = {
			["nameplate"] = {
				["toggle"] = {
					["MouseoverUnitScale"] = true,
					["TargetS"] = true,
					["MouseoverUnitAlpha"] = true,
					["MarkedA"] = true,
					["NoTargetA"] = true,
					["NonTargetA"] = false,
				},
				["alpha"] = {
					["OccludedUnits"] = 0.25,
					["NoTarget"] = 0.8999999999999999,
					["NonTarget"] = 0.5,
					["MouseoverUnit"] = 0.5,
				},
				["scale"] = {
					["CastingEnemyUnit"] = 1.15,
				},
			},
			["uniqueSettings"] = {
				nil, -- [1]
				{
				}, -- [2]
				{
				}, -- [3]
				{
				}, -- [4]
				{
				}, -- [5]
				{
				}, -- [6]
				{
				}, -- [7]
				{
				}, -- [8]
				{
				}, -- [9]
				{
				}, -- [10]
				{
				}, -- [11]
				{
				}, -- [12]
				{
				}, -- [13]
				{
				}, -- [14]
				nil, -- [15]
				{
				}, -- [16]
				{
				}, -- [17]
				nil, -- [18]
				nil, -- [19]
				{
				}, -- [20]
				{
				}, -- [21]
				nil, -- [22]
				nil, -- [23]
				{
				}, -- [24]
				{
				}, -- [25]
				nil, -- [26]
				{
				}, -- [27]
				{
				}, -- [28]
				{
				}, -- [29]
				nil, -- [30]
				nil, -- [31]
				{
				}, -- [32]
			},
			["questWidget"] = {
				["scale"] = 16,
				["x"] = 46,
				["y"] = 1,
			},
			["threat"] = {
				["ON"] = false,
			},
			["text"] = {
				["full"] = true,
				["amount"] = true,
				["AbsorbsAmount"] = true,
			},
			["Transparency"] = {
				["Fading"] = false,
			},
			["FocusWidget"] = {
				["r"] = 1,
				["g"] = 1,
				["VerticalOffset"] = 41,
				["b"] = 1,
			},
			["AuraWidget"] = {
				["CrowdControl"] = {
					["Scale"] = 1.5,
				},
				["y"] = 10,
				["Debuffs"] = {
					["FilterMode"] = "Allow",
					["FilterBySpell"] = {
						"Devouring Plague", -- [1]
						"Vampiric Touch", -- [2]
						"Shadow Word: Pain", -- [3]
					},
					["Scale"] = 1.4,
				},
				["ShowCooldownSpiral"] = true,
				["Buffs"] = {
					["Scale"] = 0.75,
					["ShowEnemy"] = false,
				},
			},
			["settings"] = {
				["eliteicon"] = {
					["x"] = -37,
					["theme"] = "stddragon",
					["y"] = 21,
				},
				["spellicon"] = {
					["show"] = false,
					["x"] = 46,
					["scale"] = 8,
					["y"] = -17,
				},
				["elitehealthborder"] = {
					["texture"] = "TP_EliteBorder_Thin",
				},
				["spelltext"] = {
					["shadow"] = false,
					["typeface"] = "2002",
					["flags"] = "OUTLINE",
				},
				["level"] = {
					["shadow"] = false,
					["align"] = "LEFT",
					["x"] = -33,
					["typeface"] = "2002",
					["flags"] = "OUTLINE",
					["size"] = 8,
				},
				["healthbar"] = {
					["width"] = 60,
					["height"] = 8,
				},
				["castbar"] = {
					["SpellNameText"] = {
						["VerticalOffset"] = 4,
					},
					["width"] = 68,
					["y"] = -14,
					["height"] = 6,
				},
				["threatborder"] = {
					["show"] = false,
				},
				["skullicon"] = {
					["show"] = false,
				},
				["name"] = {
					["FriendlyTextColorMode"] = "CLASS",
					["flags"] = "OUTLINE",
					["typeface"] = "2002",
					["y"] = 8,
					["size"] = 7,
				},
				["frame"] = {
					["height"] = 50.525,
					["y"] = -13,
					["width"] = 70,
				},
				["customtext"] = {
					["typeface"] = "2002",
					["size"] = 6,
					["SubtextColorUseSpecific"] = true,
				},
				["raidicon"] = {
					["scale"] = 12,
					["x"] = 27,
					["ShowInHeadlineView"] = true,
				},
			},
			["targetWidget"] = {
				["HorizontalOffset"] = 6,
				["ModeNames"] = true,
				["Size"] = 24,
			},
			["HeadlineView"] = {
				["customtext"] = {
					["size"] = 9,
				},
				["ForceNonAttackableUnits"] = true,
				["name"] = {
					["size"] = 8,
				},
			},
			["totemSettings"] = {
				["B1"] = {
					["Style"] = "special",
				},
			},
			["classWidget"] = {
				["scale"] = 16,
				["y"] = 11,
				["x"] = -35,
			},
			["friendlyClass"] = false,
			["ColorByReaction"] = {
				["FriendlyPlayer"] = {
					["g"] = 1,
					["b"] = 0.1215686274509804,
				},
			},
			["Visibility"] = {
				["FriendlyNPC"] = {
					["UseHeadlineView"] = true,
				},
			},
		},
		["Sargeras"] = {
			["uniqueSettings"] = {
			},
		},
		["PALADIN"] = {
			["uniqueSettings"] = {
				nil, -- [1]
				{
				}, -- [2]
				{
				}, -- [3]
				{
				}, -- [4]
				{
				}, -- [5]
				{
				}, -- [6]
				{
				}, -- [7]
				{
				}, -- [8]
				{
				}, -- [9]
				{
				}, -- [10]
				{
				}, -- [11]
				{
				}, -- [12]
				{
				}, -- [13]
				{
				}, -- [14]
				nil, -- [15]
				{
				}, -- [16]
				{
				}, -- [17]
				nil, -- [18]
				nil, -- [19]
				{
				}, -- [20]
				{
				}, -- [21]
				nil, -- [22]
				nil, -- [23]
				{
				}, -- [24]
				{
				}, -- [25]
				nil, -- [26]
				{
				}, -- [27]
				{
				}, -- [28]
				{
				}, -- [29]
				nil, -- [30]
				nil, -- [31]
				{
				}, -- [32]
			},
			["settings"] = {
				["frame"] = {
					["height"] = 52.875,
					["width"] = 154,
				},
			},
			["Visibility"] = {
				["FriendlyPlayer"] = {
					["Show"] = false,
				},
			},
		},
	},
}
