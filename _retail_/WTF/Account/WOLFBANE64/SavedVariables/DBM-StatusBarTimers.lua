
DBT_AllPersistentOptions = {
	["Default"] = {
		["DBM"] = {
			["StartColorPR"] = 1,
			["Scale"] = 1,
			["HugeBarsEnabled"] = true,
			["EnlargeBarsPercent"] = 0.125,
			["StartColorR"] = 1,
			["EndColorPR"] = 0.5019607843137255,
			["Sort"] = "Sort",
			["ExpandUpwardsLarge"] = false,
			["ExpandUpwards"] = true,
			["TimerPoint"] = "TOP",
			["EndColorDG"] = 0,
			["Alpha"] = 0.800000011920929,
			["HugeTimerPoint"] = "CENTER",
			["EndColorUIG"] = 0.9215686274509803,
			["StartColorUIR"] = 1,
			["StartColorAG"] = 0.5450980392156862,
			["EndColorDR"] = 1,
			["HugeBarXOffset"] = 0,
			["StartColorRR"] = 0.5019607843137255,
			["StartColorUIG"] = 1,
			["FillUpLargeBars"] = false,
			["HugeScale"] = 1.25,
			["BarYOffset"] = 0,
			["StartColorDG"] = 0.3019607843137255,
			["StartColorAR"] = 0.3764705882352941,
			["TextColorR"] = 1,
			["EndColorAER"] = 1,
			["StartColorIB"] = 1,
			["TextColorG"] = 1,
			["Font"] = "standardFont",
			["StartColorIR"] = 0.4705882352941176,
			["EndColorAEB"] = 0.2470588235294118,
			["Height"] = 25,
			["HugeSort"] = "Sort",
			["BarXOffset"] = 0,
			["EndColorB"] = 0,
			["EndColorAR"] = 0.1490196078431373,
			["EndColorUIB"] = 0.01176470588235294,
			["ClickThrough"] = false,
			["Decimal"] = 60,
			["StartColorDB"] = 1,
			["FadeBars"] = true,
			["TextColorB"] = 1,
			["EndColorIB"] = 1,
			["TimerY"] = -271.2671508789063,
			["HugeAlpha"] = 1,
			["EndColorRB"] = 0.3019607843137255,
			["TimerX"] = -292.6986389160156,
			["EndColorIR"] = 0.04705882352941176,
			["InlineIcons"] = true,
			["EndColorRR"] = 0.1098039215686275,
			["Bar7ForceLarge"] = false,
			["BarStyle"] = "NoAnim",
			["EnlargeBarTime"] = 11,
			["Spark"] = true,
			["StartColorPG"] = 0.7764705882352941,
			["StripCDText"] = true,
			["FontFlag"] = "OUTLINE",
			["EndColorAB"] = 1,
			["Width"] = 170,
			["EndColorPG"] = 0.4117647058823529,
			["StartColorRG"] = 1,
			["EndColorIG"] = 0.8784313725490196,
			["EndColorAEG"] = 0.04313725490196078,
			["Skin"] = "",
			["StartColorAEB"] = 0.4588235294117647,
			["NoBarFade"] = false,
			["IconRight"] = false,
			["KeepBars"] = true,
			["DynamicColor"] = true,
			["HugeTimerY"] = 34.78514099121094,
			["HugeTimerX"] = -452.805419921875,
			["ColorByType"] = true,
			["IconLeft"] = true,
			["FillUpBars"] = false,
			["StartColorRB"] = 0.5019607843137255,
			["StartColorIG"] = 0.9686274509803922,
			["StartColorAB"] = 1,
			["Texture"] = "Interface\\AddOns\\DBM-StatusBarTimers\\textures\\default.blp",
			["StartColorAEG"] = 0.4666666666666667,
			["EndColorDB"] = 1,
			["EndColorPB"] = 0.2862745098039216,
			["StartColorDR"] = 0.9019607843137255,
			["FlashBar"] = true,
			["EndColorUIR"] = 1,
			["EndColorRG"] = 1,
			["StartColorUIB"] = 0.06274509803921569,
			["StartColorG"] = 0.7019607843137254,
			["HugeBarYOffset"] = 0,
			["FontSize"] = 13,
			["EndColorR"] = 1,
			["StartColorPB"] = 0.4196078431372549,
			["IconLocked"] = true,
			["StartColorAER"] = 1,
			["EndColorAG"] = 0.3843137254901961,
			["HugeWidth"] = 177,
			["StartColorB"] = 0,
			["Bar7CustomInline"] = true,
			["EndColorG"] = 0,
			["TDecimal"] = 11,
		},
	},
}
