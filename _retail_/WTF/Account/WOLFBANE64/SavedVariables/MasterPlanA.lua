
MasterPlanAG = {
	["Sargeras"] = {
		["Botann"] = {
			["class"] = "ROGUE",
			["faction"] = "Alliance",
		},
		["Thorjitsu"] = {
			["faction"] = "Alliance",
			["class"] = "MONK",
		},
		["Thorddin"] = {
			["faction"] = "Alliance",
			["class"] = "PALADIN",
		},
	},
}
