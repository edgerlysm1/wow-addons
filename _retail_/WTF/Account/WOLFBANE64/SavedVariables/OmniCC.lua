
OmniCCDB = {
	["profileKeys"] = {
		["Thorddin - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Sneakythorr - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
		["Thorpawz - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Odintsun - Sargeras"] = "Default",
		["Thormonde - Sargeras"] = "Default",
		["Thornarii - Sargeras"] = "Default",
		["Thorraddin - Sargeras"] = "Default",
	},
	["global"] = {
		["dbVersion"] = 6,
		["addonVersion"] = "9.0.8",
	},
	["profiles"] = {
		["Default"] = {
			["rules"] = {
				{
					["enabled"] = false,
					["patterns"] = {
						"Aura", -- [1]
						"Buff", -- [2]
						"Debuff", -- [3]
					},
					["name"] = "Auras",
					["id"] = "auras",
				}, -- [1]
				{
					["enabled"] = false,
					["patterns"] = {
						"Plate", -- [1]
					},
					["name"] = "Unit Nameplates",
					["id"] = "plates",
				}, -- [2]
				{
					["enabled"] = false,
					["patterns"] = {
						"ActionButton", -- [1]
					},
					["name"] = "ActionBars",
					["id"] = "actions",
				}, -- [3]
			},
			["themes"] = {
				["Default"] = {
					["textStyles"] = {
						["soon"] = {
							["scale"] = 1.50000001490116,
						},
						["seconds"] = {
							["scale"] = 1.00000000745058,
						},
						["minutes"] = {
							["scale"] = 1.00000000745058,
						},
						["hours"] = {
							["scale"] = 0.75000000372529,
						},
						["charging"] = {
							["scale"] = 0.75000000372529,
						},
						["controlled"] = {
							["scale"] = 1.50000001490116,
						},
					},
					["fontFace"] = "Fonts\\2002B.TTF",
					["tenthsDuration"] = 10,
					["minSize"] = 0,
					["mmSSDuration"] = 120,
					["spiralOpacity"] = 0.519999988377094,
					["fontSize"] = 13,
					["minDuration"] = 2.5,
				},
			},
		},
	},
}
OmniCC4Config = {
	["groupSettings"] = {
		["base"] = {
			["enabled"] = true,
			["styles"] = {
				["seconds"] = {
					["scale"] = 1.00000000745058,
				},
				["minutes"] = {
					["scale"] = 1.00000000745058,
				},
				["soon"] = {
					["scale"] = 1.50000001490116,
				},
				["hours"] = {
					["scale"] = 0.75000000372529,
				},
				["charging"] = {
					["scale"] = 0.75000000372529,
				},
				["controlled"] = {
					["scale"] = 1.50000001490116,
				},
			},
			["fontSize"] = 13,
			["yOff"] = 0,
			["mmSSDuration"] = 120,
			["minSize"] = 0,
			["spiralOpacity"] = 0.519999988377094,
			["minDuration"] = 2.5,
			["xOff"] = 0,
			["tenthsDuration"] = 10,
			["fontOutline"] = "OUTLINE",
			["anchor"] = "CENTER",
			["minEffectDuration"] = 30,
			["scaleText"] = true,
			["fontFace"] = "Fonts\\2002B.TTF",
		},
	},
	["groups"] = {
	},
	["version"] = "7.0.1",
}
