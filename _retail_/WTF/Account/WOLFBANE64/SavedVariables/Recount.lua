
RecountDB = {
	["profileKeys"] = {
		["Thorddin - Sargeras"] = "Thorddin - Sargeras",
		["Sairrus - Sargeras"] = "Sairrus - Sargeras",
		["Thorwin - Sargeras"] = "Thorwin - Sargeras",
		["Tralerodora - Illidan"] = "Tralerodora - Illidan",
		["Ashtali - Sargeras"] = "Ashtali - Sargeras",
		["Norial - Illidan"] = "Norial - Illidan",
		["Thornoxnun - Sargeras"] = "Thornoxnun - Sargeras",
		["Thorjitsu - Sargeras"] = "Thorjitsu - Sargeras",
		["Thorrend - Sargeras"] = "Thorrend - Sargeras",
		["Liashta - Sargeras"] = "Liashta - Sargeras",
		["Botann - Sargeras"] = "Botann - Sargeras",
		["Norvah - Sargeras"] = "Norvah - Sargeras",
		["Thorend - Illidan"] = "Thorend - Illidan",
		["Thorwyyn - Sargeras"] = "Thorwyyn - Sargeras",
	},
	["profiles"] = {
		["Thorddin - Sargeras"] = {
			["GraphWindowY"] = -158.000030517578,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -201.724197387695,
					["x"] = 399.96337890625,
					["w"] = 197.000137329102,
					["h"] = 111.999961853027,
				},
			},
			["LastInstanceName"] = "The Battle for Gilneas",
			["RealtimeWindows"] = {
				["Realtime_Thorddin_DAMAGE"] = {
					"Thorddin", -- [1]
					"DAMAGE", -- [2]
					"'s DPS", -- [3]
					0, -- [4]
					3.05175781250e-005, -- [5]
					200, -- [6]
					232.000015258789, -- [7]
					false, -- [8]
				},
			},
			["CurDataSet"] = "LastFightData",
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["GraphWindowX"] = 355.5,
			["MainWindowHeight"] = 111.986526489258,
			["MainWindowWidth"] = 197.119140625,
			["MainWindowVis"] = false,
		},
		["Sairrus - Sargeras"] = {
			["MainWindow"] = {
				["Position"] = {
					["y"] = 74.0429077148438,
					["h"] = 63.0000038146973,
					["w"] = 140.000030517578,
					["x"] = -561.556106567383,
				},
				["BarText"] = {
					["NumFormat"] = 3,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["DetailWindowY"] = -141.64875793457,
			["DetailWindowX"] = 795.158935546875,
			["MainWindowVis"] = false,
			["LastInstanceName"] = "Antorus, the Burning Throne",
			["RealtimeWindows"] = {
				["Realtime_FPS_FPS"] = {
					"FPS", -- [1]
					"FPS", -- [2]
					"", -- [3]
					-474.923690795898, -- [4]
					337.516662597656, -- [5]
					150, -- [6]
					129.777801513672, -- [7]
					false, -- [8]
				},
				["Realtime_Latency_LAG"] = {
					"Latency", -- [1]
					"LAG", -- [2]
					"", -- [3]
					-625.414825439453, -- [4]
					335.4169921875, -- [5]
					150, -- [6]
					131.555419921875, -- [7]
					false, -- [8]
				},
			},
			["CurDataSet"] = "LastFightData",
			["MainWindowWidth"] = 140.000015258789,
			["MainWindowHeight"] = 63,
		},
		["Thorwin - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["h"] = 199.999954223633,
					["x"] = -0.447998046875,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Tralerodora - Illidan"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["h"] = 199.999954223633,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Ashtali - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -297.926315307617,
					["h"] = 127,
					["w"] = 176.999877929688,
					["x"] = -263.391296386719,
				},
			},
			["DetailWindowX"] = 248.732345581055,
			["LastInstanceName"] = "Halls of Valor",
			["CurDataSet"] = "LastFightData",
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["DetailWindowY"] = -242.924011230469,
			["MainWindowWidth"] = 177.486190795898,
			["MainWindowHeight"] = 127.30989074707,
		},
		["Norial - Illidan"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["w"] = 140.000015258789,
					["h"] = 200.000015258789,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Thornoxnun - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = 6.1035156250e-005,
					["w"] = 140.000061035156,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Thorjitsu - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -265,
					["x"] = 209,
					["w"] = 186,
					["h"] = 121.000015258789,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["MainWindowHeight"] = 121.000015258789,
			["LastInstanceName"] = "Skywall",
			["MainWindowWidth"] = 186,
			["CurDataSet"] = "LastFightData",
		},
		["Thorrend - Sargeras"] = {
			["MainWindow"] = {
				["Position"] = {
					["y"] = -287.333404541016,
					["x"] = -243.77783203125,
					["w"] = 195.555328369141,
					["h"] = 98.2222061157227,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["LastInstanceName"] = "Broken Shore",
			["MainWindowHeight"] = 98.2222137451172,
			["MainWindowWidth"] = 195.555328369141,
			["CurDataSet"] = "OverallData",
		},
		["Liashta - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = 6.1035156250e-005,
					["w"] = 139.999969482422,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Botann - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -3.05175781250e-005,
					["w"] = 139.999984741211,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Norvah - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -3.05175781250e-005,
					["w"] = 139.999984741211,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
		["Thorend - Illidan"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -3.55560302734375,
					["x"] = 90.2222290039063,
					["w"] = 249.33349609375,
					["h"] = 202.666687011719,
				},
			},
			["Colors"] = {
				["Realtime"] = {
					["Latency Top"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
			["MainWindowHeight"] = 202.666687011719,
			["MainWindowWidth"] = 249.33349609375,
			["RealtimeWindows"] = {
				["Realtime_FPS_FPS"] = {
					"FPS", -- [1]
					"FPS", -- [2]
					"", -- [3]
					-672.888954162598, -- [4]
					-93.9999389648438, -- [5]
					214.222091674805, -- [6]
					115.111152648926, -- [7]
					true, -- [8]
				},
				["Realtime_Latency_LAG"] = {
					"Latency", -- [1]
					"LAG", -- [2]
					"", -- [3]
					-672.666679382324, -- [4]
					26.6666259765625, -- [5]
					212.888778686523, -- [6]
					137.777755737305, -- [7]
					true, -- [8]
				},
			},
			["LastInstanceName"] = "Deadmines",
		},
		["Thorwyyn - Sargeras"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -3.05175781250e-005,
					["h"] = 199.999908447266,
					["w"] = 140.000076293945,
				},
			},
			["Colors"] = {
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["CurDataSet"] = "OverallData",
		},
	},
}
