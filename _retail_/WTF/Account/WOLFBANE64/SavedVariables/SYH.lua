
SalvageYardHDB = {
	["profileKeys"] = {
		["Thordaris - Sargeras"] = "Default",
		["Ashtali - Sargeras"] = "Default",
		["Thorjitsu - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Tircathas - Sargeras"] = "Default",
		["Thorlexi - Sargeras"] = "Default",
		["Thorwyyn - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Thorpawz - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Haraklem - Sargeras"] = "Default",
		["Anthienisse - Illidan"] = "Default",
		["Thorjutsu - Sargeras"] = "Default",
		["Thorraddin - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Nntailfa - Sargeras"] = "Default",
		["Thormonde - Sargeras"] = "Default",
	},
	["UserAutoSellList"] = {
		["Thick Paleo Steak"] = "|cffffffff|Hitem:154899::::::::120:65::::::|h[Thick Paleo Steak]|h|r",
		["Scarlet Diamond"] = "|cff0070dd|Hitem:154121::::::::120:65::::::|h[Scarlet Diamond]|h|r",
		["Deep Sea Satin"] = "|cff1eff00|Hitem:152577::::::::120:65::::::|h[Deep Sea Satin]|h|r",
		["Moist Fillet"] = "|cffffffff|Hitem:168645::::::::120:65::::::|h[Moist Fillet]|h|r",
		["Durable Flux"] = "|cffffffff|Hitem:160298::::::::120:65::::::|h[Durable Flux]|h|r",
		["Briny Flesh"] = "|cffffffff|Hitem:152631::::::::120:65::::::|h[Briny Flesh]|h|r",
		["Meaty Haunch"] = "|cffffffff|Hitem:154898::::::::120:65::::::|h[Meaty Haunch]|h|r",
		["Stringy Loins"] = "|cffffffff|Hitem:154897::::::::120:65::::::|h[Stringy Loins]|h|r",
		["Golden Beryl"] = "|cff1eff00|Hitem:153700::::::::120:65::::::|h[Golden Beryl]|h|r",
		["Rubbery Flank"] = "|cffffffff|Hitem:168303::::::::120:65::::::|h[Rubbery Flank]|h|r",
		["Nylon Thread"] = "|cffffffff|Hitem:159959::::::::120:65::::::|h[Nylon Thread]|h|r",
		["Embroidered Deep Sea Satin"] = "|cff0070dd|Hitem:158378::::::::120:65::::::|h[Embroidered Deep Sea Satin]|h|r",
		["Amberblaze"] = "|cff0070dd|Hitem:154123::::::::120:65::::::|h[Amberblaze]|h|r",
	},
	["UserBlackListKeyWord"] = {
	},
	["UserBlackList"] = {
	},
	["profiles"] = {
		["Default"] = {
			["AHPriceThreshold"] = 1001,
			["SellBlue"] = true,
			["SellGreenMaxLevel"] = 315.0430603027344,
			["TransmogPanel"] = {
				["position"] = {
					["y"] = 131.4351806640625,
					["x"] = 116.4991455078125,
				},
				["IsLocked"] = false,
			},
			["MainPanel"] = {
				["IsLocked"] = false,
				["position"] = {
					["y"] = 126.003173828125,
					["x"] = -228.0451965332031,
				},
			},
			["IgnorePanel"] = {
				["IsLocked"] = false,
				["position"] = {
					["y"] = 0,
					["x"] = 0,
				},
			},
			["SellListPanel"] = {
				["position"] = {
					["y"] = -101.7636108398438,
					["x"] = -297.7404174804688,
				},
				["IsLocked"] = false,
			},
			["PricePanel"] = {
				["IsLocked"] = false,
				["position"] = {
					["y"] = 0,
					["x"] = 0,
				},
			},
		},
	},
}
