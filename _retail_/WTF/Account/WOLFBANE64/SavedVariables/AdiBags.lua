
AdiBagsDB = {
	["namespaces"] = {
		["ItemLevel"] = {
		},
		["FilterOverride"] = {
			["profiles"] = {
				["Default"] = {
					["version"] = 3,
					["overrides"] = {
						[167374] = "Equipment#Set: Prot",
					},
				},
			},
		},
		["ItemCategory"] = {
			["profiles"] = {
				["Default"] = {
					["splitBySubclass"] = {
						["Gem"] = false,
					},
				},
			},
		},
		["NewItem"] = {
		},
		["AdiBags_TooltipInfo"] = {
		},
		["MoneyFrame"] = {
		},
		["ItemSets"] = {
		},
		["CurrencyFrame"] = {
			["profiles"] = {
				["Default"] = {
					["shown"] = {
						["Mark of the World Tree"] = false,
						["Brawler's Gold"] = false,
						["Nethershard"] = false,
						["Valor"] = false,
						["Illustrious Jewelcrafter's Token"] = false,
						["Seal of Inevitable Fate"] = false,
						["Order Resources"] = false,
						["Echoes of Battle"] = false,
						["Tol Barad Commendation"] = false,
						["War Resources"] = false,
						["Argent Commendation"] = false,
						["Sightless Eye"] = false,
						["Timeless Coin"] = false,
						["Grateful Offering"] = false,
						["Lingering Soul Fragment"] = false,
						["Ironpaw Token"] = false,
						["Seal of Broken Fate"] = false,
						["Coalescing Visions"] = false,
						["Lesser Charm of Good Fortune"] = false,
						["Oil"] = false,
						["Seal of Wartorn Fate"] = false,
						["Artifact Fragment"] = false,
						["Seal of Tempered Fate"] = false,
						["Seafarer's Dubloon"] = false,
						["Titan Residuum"] = false,
						["Mogu Rune of Fate"] = false,
						["Timewarped Badge"] = false,
						["7th Legion Service Medal"] = false,
						["Curious Coin"] = false,
						["Ancient Mana"] = false,
						["Veiled Argunite"] = false,
						["Epicurean's Award"] = false,
						["Timeworn Artifact"] = false,
						["Warforged Seal"] = false,
						["Wakening Essence"] = false,
						["Garrison Resources"] = false,
						["Apexis Crystal"] = false,
						["Darkmoon Prize Ticket"] = false,
						["Elder Charm of Good Fortune"] = false,
						["Prismatic Manapearl"] = false,
						["Spirit Shard"] = false,
						["Champion's Seal"] = false,
						["Legionfall War Supplies"] = false,
					},
					["text"] = {
						["name"] = "2002 Bold",
						["size"] = 20,
					},
					["hideZeroes"] = false,
				},
			},
		},
		["DataSource"] = {
		},
		["Junk"] = {
			["profiles"] = {
				["Default"] = {
					["include"] = {
						[164473] = true,
						[34664] = true,
						[21877] = true,
						[32230] = true,
					},
				},
			},
		},
		["Equipment"] = {
			["profiles"] = {
				["Default"] = {
					["dispatchRule"] = "slot",
				},
			},
		},
	},
	["profileKeys"] = {
		["Laviceyi - Sargeras"] = "Default",
		["Thorpez - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Jarrexion - Sargeras"] = "Default",
		["Thorrdin - Sargeras"] = "Default",
		["Odintsun - Sargeras"] = "Default",
		["Saelorelea - Sargeras"] = "Default",
		["Lochogh - Sargeras"] = "Default",
		["Thorwyyn - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Sneakythorr - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Haraklem - Sargeras"] = "Default",
		["Ochla - Sargeras"] = "Default",
		["Thorpawz - Sargeras"] = "Default",
		["Rosselis - Sargeras"] = "Default",
		["Thordaris - Sargeras"] = "Default",
		["Senithor - Sargeras"] = "Default",
		["Thornarii - Sargeras"] = "Default",
		["Thorggar - Sargeras"] = "Default",
		["Norvah - Sargeras"] = "Default",
		["Aquyssaelea - Sargeras"] = "Default",
		["Anthienisse - Illidan"] = "Default",
		["Ashtali - Sargeras"] = "Default",
		["Thorrand - Sargeras"] = "Default",
		["Thorjitsu - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Botann - Sargeras"] = "Default",
		["Linnadra - Sargeras"] = "Default",
		["Tircathas - Sargeras"] = "Default",
		["Thorlexi - Sargeras"] = "Default",
		["Eiahu - Sargeras"] = "Default",
		["Thorvinn - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
		["Aqulaylaszun - Sargeras"] = "Default",
		["Traydrana - Sargeras"] = "Default",
		["Iamthorr - Sargeras"] = "Default",
		["Thorraddin - Sargeras"] = "Default",
		["Songereirn - Sargeras"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
		["Aatala - Sargeras"] = "Default",
		["Fincamagar - Sargeras"] = "Default",
		["Thorjutsu - Sargeras"] = "Default",
		["Thorrsmash - Sargeras"] = "Default",
		["Marraydel - Sargeras"] = "Default",
		["Nntailfa - Sargeras"] = "Default",
		["Thormonde - Sargeras"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["scale"] = 1,
			["maxHeight"] = 0.75,
			["skin"] = {
				["BackpackColor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
				},
				["borderWidth"] = 34,
				["background"] = "Blizzard Garrison Background 2",
				["border"] = "Blizzard Dialog Gold",
				["insets"] = 0,
			},
			["bagFont"] = {
				["name"] = "Arial Narrow",
				["size"] = 24,
			},
			["positions"] = {
				["anchor"] = {
					["xOffset"] = 0,
					["yOffset"] = 72.5324630737305,
				},
				["Backpack"] = {
					["xOffset"] = -564.337890625,
					["yOffset"] = 229.7608795166016,
				},
				["Bank"] = {
					["xOffset"] = -1145.2412109375,
					["point"] = "TOPRIGHT",
					["yOffset"] = -361.1327514648438,
				},
			},
			["compactLayout"] = true,
			["rightClickConfig"] = false,
			["sectionFont"] = {
				["name"] = "Arial Narrow",
				["size"] = 16,
			},
			["positionMode"] = "manual",
			["columnWidth"] = {
				["Backpack"] = 10,
			},
		},
	},
}
