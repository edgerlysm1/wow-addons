
AucAdvancedConfig = {
	["users.Sargeras.Thorjitsu"] = "Default",
	["profile.Default"] = {
		["configator.left"] = 559.999877929688,
		["core"] = {
			["matcher"] = {
				["matcherlist"] = {
					"BeanCount", -- [1]
					"Undercut", -- [2]
				},
			},
		},
		["stat"] = {
			["sales"] = {
				["ignoredsigs"] = {
				},
				["upgraded32"] = true,
			},
		},
		["util"] = {
			["appraiser"] = {
				["item.123918.number"] = 5,
				["item.124461.fixed.bid"] = 2150000,
				["item.124117.match"] = false,
				["item.141592.match"] = false,
				["item.124461.duration"] = 720,
				["columnsortcurSort"] = 6,
				["item.123918.fixed.bid"] = 190000,
				["item.123956.model"] = "fixed",
				["item.123919.number"] = 1,
				["item.141592.fixed.bid"] = 9999999,
				["item.124461.fixed.buy"] = 2240000,
				["item.136708.fixed.buy"] = 25000000,
				["item.123956.duration"] = 720,
				["item.123918.duration"] = 720,
				["item.124117.duration"] = 1440,
				["item.124117.model"] = "fixed",
				["item.123956.number"] = 1,
				["item.124461.stack"] = 5,
				["item.123918.fixed.buy"] = 214999,
				["item.136685.fixed.bid"] = 66825000,
				["item.136708.duration"] = 720,
				["item.124117.fixed.buy"] = 12500,
				["item.136708.fixed.bid"] = 22000000,
				["item.136683.duration"] = 720,
				["item.136685.match"] = false,
				["item.124461.model"] = "fixed",
				["item.136685.model"] = "fixed",
				["item.136685.fixed.buy"] = 73500000,
				["item.123918.model"] = "fixed",
				["item.124437.stack"] = 100,
				["item.141592.model"] = "fixed",
				["columnwidth.Buyout"] = 68.9999313354492,
				["item.124117.number"] = 2,
				["item.136685.duration"] = 720,
				["item.141592.fixed.buy"] = 9999999,
				["item.123956.fixed.bid"] = 7500000,
				["item.123956.fixed.buy"] = 15000000,
				["item.123918.match"] = false,
				["item.123956.stack"] = 1,
				["classic"] = false,
				["item.136708.model"] = "fixed",
				["item.123918.stack"] = 20,
				["item.136708.match"] = false,
				["item.123919.stack"] = 20,
				["item.123919.match"] = false,
				["item.124117.fixed.bid"] = 12000,
				["item.136708.stack"] = 1,
				["columnsortcurDir"] = 1,
				["item.123956.match"] = false,
				["item.124461.match"] = false,
				["item.124461.number"] = 1,
			},
			["EasyBuyout"] = {
				["EGL.EBid.active"] = false,
				["EC.active"] = true,
				["EGL.EBid.limit"] = 1000000,
				["EBid.active"] = true,
				["active"] = true,
				["EGL.EBuy.active"] = false,
				["EGL.EBuy.limit"] = 10000000,
			},
			["compactui"] = {
				["priceperitem"] = true,
			},
			["automagic"] = {
				["ammailguix"] = 160,
				["SavedMailButtons"] = {
				},
				["uierrormsg"] = 1,
			},
			["ahwindowcontrol"] = {
				["auctionscale"] = 1.3,
				["compactuiscale"] = 5,
				["ahframeanchors"] = "CENTER:CENTER:15.1:35.0",
			},
		},
		["configator.top"] = 542,
	},
	["version"] = 1,
}
AucAdvancedData = {
	["UtilSearchUiData"] = {
		["Current"] = {
			["general.maxbuy"] = 1110000,
			["snatch.maxprice"] = 1,
			["general.name"] = "Felslate",
			["general.minbid"] = 10000,
			["general.minbuy"] = 10000,
			["columnsortcurSort"] = 2,
			["general.name.exact"] = true,
			["snatch.itemsList"] = {
				["123918:0:0"] = {
					["percent"] = 80,
					["link"] = "|cffffffff|Hitem:123918::::::::110:70::::::|h[Leystone Ore]|h|r",
				},
				["123919:0:0"] = {
					["percent"] = 1,
					["link"] = "|cffffffff|Hitem:123919::::::::110:70::::::|h[Felslate]|h|r",
				},
			},
			["ignoreitemprice.ignorelist"] = {
				["123919"] = 0,
			},
			["columnsortcurDir"] = 1,
			["general.maxbid"] = 1000000,
			["snatch.columnsortcurDir"] = 1,
			["snatch.columnsortcurSort"] = 1,
		},
		["Version"] = 1,
		["Selected"] = "Ore",
		["Global"] = {
			["configator.left"] = 229.999969482422,
			["configator.top"] = 634,
		},
		["SavedSearches"] = {
			["Ore"] = {
				["general.maxbuy"] = 1110000,
				["snatch.maxprice"] = 1,
				["general.name"] = "Felslate",
				["general.minbid"] = 10000,
				["general.minbuy"] = 10000,
				["columnsortcurSort"] = 2,
				["general.name.exact"] = true,
				["ignoreitemprice.ignorelist"] = {
					["123919"] = 0,
				},
				["snatch.itemsList"] = {
					["123918:0:0"] = {
						["percent"] = 80,
						["link"] = "|cffffffff|Hitem:123918::::::::110:70::::::|h[Leystone Ore]|h|r",
					},
					["123919:0:0"] = {
						["percent"] = 1,
						["link"] = "|cffffffff|Hitem:123919::::::::110:70::::::|h[Felslate]|h|r",
					},
				},
				["columnsortcurDir"] = 1,
				["general.maxbid"] = 1000000,
				["snatch.columnsortcurDir"] = 1,
				["snatch.columnsortcurSort"] = 1,
			},
		},
	},
	["Stats"] = {
	},
}
AucAdvancedServers = {
	["Version"] = 1,
	["Timestamp"] = 1472240121,
	["KnownServerKeys"] = {
		["Sargeras"] = 1473726554,
	},
	["KnownRealms"] = {
		["Sargeras"] = "Sargeras",
	},
}
