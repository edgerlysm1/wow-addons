
MacroToolkitDB = {
	["char"] = {
		["Sairrus - Sargeras"] = {
			["backups"] = {
			},
		},
		["Thornna - Sargeras"] = {
			["backups"] = {
			},
		},
	},
	["global"] = {
		["backups"] = {
		},
		["ebackups"] = {
		},
	},
	["profileKeys"] = {
		["Thorwin - Sargeras"] = "profile",
		["Jarrexion - Sargeras"] = "profile",
		["Thorrdin - Sargeras"] = "profile",
		["Saelorelea - Sargeras"] = "profile",
		["Lochogh - Sargeras"] = "profile",
		["Thorwyyn - Sargeras"] = "profile",
		["Thordren - Sargeras"] = "profile",
		["Sneakythorr - Sargeras"] = "profile",
		["Thorrend - Sargeras"] = "profile",
		["Haraklem - Sargeras"] = "profile",
		["Ochla - Sargeras"] = "profile",
		["Thordaris - Sargeras"] = "profile",
		["Aquyssaelea - Sargeras"] = "profile",
		["Anthienisse - Illidan"] = "profile",
		["Thorrand - Sargeras"] = "profile",
		["Thorjitsu - Sargeras"] = "profile",
		["Thorpawz - Sargeras"] = "profile",
		["Thornna - Sargeras"] = "profile",
		["Botann - Sargeras"] = "profile",
		["Linnadra - Sargeras"] = "profile",
		["Tircathas - Sargeras"] = "profile",
		["Thorlexi - Sargeras"] = "profile",
		["Ashtali - Sargeras"] = "profile",
		["Thorddin - Sargeras"] = "profile",
		["Thorraddin - Sargeras"] = "profile",
		["Aqulaylaszun - Sargeras"] = "profile",
		["Traydrana - Sargeras"] = "profile",
		["Thorrsmash - Sargeras"] = "profile",
		["Thorvinn - Sargeras"] = "profile",
		["Songereirn - Sargeras"] = "profile",
		["Thornoxnun - Sargeras"] = "profile",
		["Norvah - Sargeras"] = "profile",
		["Fincamagar - Sargeras"] = "profile",
		["Sairrus - Sargeras"] = "profile",
		["Thorggar - Sargeras"] = "profile",
		["Iamthorr - Sargeras"] = "profile",
		["Nntailfa - Sargeras"] = "profile",
		["Thorjutsu - Sargeras"] = "profile",
	},
	["profiles"] = {
		["profile"] = {
			["y"] = 259.967864990234,
			["x"] = 582.580932617188,
		},
	},
}
