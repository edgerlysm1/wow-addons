
THF_DB = {
	["TextFont"] = "Friz Quadrata TT",
	["Disabled"] = false,
	["Scale"] = 0.4490000152587891,
	["TitleFlag"] = "Outline",
	["TextFlag"] = "Outline",
	["TitleSize"] = 22,
	["Y"] = -144.625228881836,
	["X"] = -300.247161865234,
	["ClickThrough"] = true,
	["RelativePoint"] = "CENTER",
	["TextSize"] = 16,
	["Point"] = "CENTER",
	["LowerStrata"] = false,
	["TitleFont"] = "Friz Quadrata TT",
}
