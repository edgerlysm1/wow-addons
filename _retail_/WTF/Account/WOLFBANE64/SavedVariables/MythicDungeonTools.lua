
MythicDungeonToolsDB = {
	["profileKeys"] = {
		["Thordaris - Sargeras"] = "Thordaris - Sargeras",
		["Senithor - Sargeras"] = "Senithor - Sargeras",
		["Thorpez - Sargeras"] = "Thorpez - Sargeras",
		["Thorwin - Sargeras"] = "Thorwin - Sargeras",
		["Ashtali - Sargeras"] = "Ashtali - Sargeras",
		["Thorjitsu - Sargeras"] = "Thorjitsu - Sargeras",
		["Thornna - Sargeras"] = "Thornna - Sargeras",
		["Odintsun - Sargeras"] = "Odintsun - Sargeras",
		["Thorlexi - Sargeras"] = "Thorlexi - Sargeras",
		["Thornarii - Sargeras"] = "Thornarii - Sargeras",
		["Thorpawz - Sargeras"] = "Thorpawz - Sargeras",
		["Thordren - Sargeras"] = "Thordren - Sargeras",
		["Iamthorr - Sargeras"] = "Iamthorr - Sargeras",
		["Sneakythorr - Sargeras"] = "Sneakythorr - Sargeras",
		["Thormonde - Sargeras"] = "Thormonde - Sargeras",
		["Thorraddin - Sargeras"] = "Thorraddin - Sargeras",
		["Thornoxnun - Sargeras"] = "Thornoxnun - Sargeras",
		["Aatala - Sargeras"] = "Aatala - Sargeras",
		["Rosselis - Sargeras"] = "Rosselis - Sargeras",
		["Thorwyyn - Sargeras"] = "Thorwyyn - Sargeras",
		["Sairrus - Sargeras"] = "Sairrus - Sargeras",
		["Thorrend - Sargeras"] = "Thorrend - Sargeras",
		["Norvah - Sargeras"] = "Norvah - Sargeras",
		["Thorddin - Sargeras"] = "Thorddin - Sargeras",
	},
	["global"] = {
		["anchorTo"] = "CENTER",
		["colorPaletteInfo"] = {
			["customPaletteValues"] = {
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [1]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [2]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [3]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [4]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [5]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [6]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [7]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [8]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [9]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [10]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [11]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [12]
			},
		},
		["anchorFrom"] = "CENTER",
		["minimap"] = {
			["minimapPos"] = 178.18510619374,
		},
		["nonFullscreenScale"] = 1,
		["MDI"] = {
		},
		["maximized"] = false,
		["presets"] = {
			{
				{
					["mdi"] = {
						["freehold"] = 1,
						["beguiling"] = 1,
						["freeholdJoined"] = false,
					},
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["currentDungeonIdx"] = 1,
						["teeming"] = true,
						["riftOffsets"] = {
						},
						["pulls"] = {
							{
								["color"] = "228b22",
							}, -- [1]
						},
					},
					["difficulty"] = 10,
				}, -- [1]
			}, -- [1]
			{
				{
					["difficulty"] = 10,
					["week"] = 5,
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[5] = {
							},
						},
						["currentDungeonIdx"] = 2,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
					},
					["objects"] = {
					},
					["mdi"] = {
						["freehold"] = 1,
						["beguiling"] = 1,
						["freeholdJoined"] = false,
					},
				}, -- [1]
			}, -- [2]
			[15] = {
				{
					["mdi"] = {
						["freehold"] = 1,
						["beguiling"] = 1,
						["freeholdJoined"] = false,
					},
					["week"] = 1,
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							{
							}, -- [1]
						},
						["currentDungeonIdx"] = 15,
						["teeming"] = true,
						["selection"] = {
							1, -- [1]
						},
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
					},
					["objects"] = {
					},
					["difficulty"] = 10,
				}, -- [1]
				{
					["objects"] = {
					},
					["week"] = 5,
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[3] = {
								[161241] = {
									["y"] = -100.734435020344,
									["x"] = 465.1418732575077,
								},
								[161243] = {
									["y"] = -138.8499560479764,
									["x"] = 424.9746053925101,
								},
								[161124] = {
									["y"] = -278.5653285950014,
									["x"] = 463.5007870625046,
								},
							},
							[2] = {
							},
							[8] = {
								[161243] = {
									["y"] = -148.0259756760338,
									["x"] = 396.1773394948042,
								},
								[161241] = {
									["y"] = -95.27607875162506,
									["x"] = 442.1894261797538,
								},
							},
							[5] = {
								[161243] = {
									["y"] = -138.4152193546347,
									["x"] = 401.8708104200438,
								},
								[161244] = {
									["y"] = -101.3084632945739,
									["x"] = 438.8806675798546,
								},
							},
						},
						["currentDungeonIdx"] = 15,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["pulls"] = {
							{
								{
									3, -- [1]
									4, -- [2]
									5, -- [3]
									1, -- [4]
									2, -- [5]
									6, -- [6]
									7, -- [7]
								}, -- [1]
								{
									9, -- [1]
									1, -- [2]
									8, -- [3]
								}, -- [2]
								{
									5, -- [1]
								}, -- [3]
								{
									5, -- [1]
									6, -- [2]
									7, -- [3]
									8, -- [4]
								}, -- [4]
								nil, -- [5]
								{
									4, -- [1]
									3, -- [2]
									1, -- [3]
									2, -- [4]
								}, -- [6]
								nil, -- [7]
								{
									1, -- [1]
									3, -- [2]
									4, -- [3]
									5, -- [4]
									8, -- [5]
								}, -- [8]
								{
									1, -- [1]
									2, -- [2]
									8, -- [3]
									7, -- [4]
								}, -- [9]
								{
									1, -- [1]
									2, -- [2]
									6, -- [3]
								}, -- [10]
								{
									7, -- [1]
									6, -- [2]
									1, -- [3]
									8, -- [4]
								}, -- [11]
								{
									1, -- [1]
									7, -- [2]
									8, -- [3]
									2, -- [4]
									3, -- [5]
								}, -- [12]
								nil, -- [13]
								{
									24, -- [1]
									21, -- [2]
									25, -- [3]
									22, -- [4]
									23, -- [5]
									18, -- [6]
									16, -- [7]
									20, -- [8]
									17, -- [9]
									19, -- [10]
									9, -- [11]
									6, -- [12]
									7, -- [13]
									8, -- [14]
									10, -- [15]
									1, -- [16]
									2, -- [17]
									3, -- [18]
									4, -- [19]
									5, -- [20]
									34, -- [21]
									32, -- [22]
									33, -- [23]
									31, -- [24]
									13, -- [25]
									12, -- [26]
									14, -- [27]
									11, -- [28]
									15, -- [29]
									27, -- [30]
									28, -- [31]
									29, -- [32]
									26, -- [33]
									30, -- [34]
								}, -- [14]
								{
									7, -- [1]
									6, -- [2]
									8, -- [3]
									1, -- [4]
									9, -- [5]
									10, -- [6]
									4, -- [7]
									5, -- [8]
								}, -- [15]
								[24] = {
									1, -- [1]
									2, -- [2]
								},
								[25] = {
									1, -- [1]
								},
								[26] = {
									1, -- [1]
								},
								[27] = {
									1, -- [1]
									2, -- [2]
								},
								["color"] = "228b22",
								[20] = {
								},
								[21] = {
									3, -- [1]
									11, -- [2]
									2, -- [3]
								},
								[22] = {
									4, -- [1]
									11, -- [2]
									10, -- [3]
								},
								[23] = {
									2, -- [1]
									1, -- [2]
									7, -- [3]
								},
							}, -- [1]
						},
					},
					["text"] = "Preset 2 4",
					["mdi"] = {
						["beguiling"] = 1,
						["freeholdJoined"] = false,
						["freehold"] = 1,
					},
					["difficulty"] = 10,
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			},
		},
		["dataCollection"] = {
			[35] = {
				[165137] = {
					[320462] = {
					},
				},
			},
		},
		["dataCollectionCC"] = {
		},
		["version"] = 304,
		["xoffset"] = -27.99984169006348,
		["dataCollectionGUID"] = {
		},
		["currentPreset"] = {
			[15] = 2,
		},
		["yoffset"] = 55.69997406005859,
	},
}
