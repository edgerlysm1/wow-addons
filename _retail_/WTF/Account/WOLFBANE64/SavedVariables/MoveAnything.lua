
MADB = {
	["noMMMW"] = false,
	["characters"] = {
	},
	["alwaysShowNudger"] = false,
	["tooltips"] = true,
	["profiles"] = {
		["default"] = {
			["name"] = "default",
			["frames"] = {
				["PlayerDebuffsMover"] = {
					["orgPos"] = {
						"TOPRIGHT", -- [1]
						"BuffFrame", -- [2]
						"BOTTOMRIGHT", -- [3]
						0, -- [4]
						-60, -- [5]
					},
					["name"] = "PlayerDebuffsMover",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						1176.200439453125, -- [4]
						315.0007934570313, -- [5]
					},
				},
				["BasicActionButtonsMover"] = {
					["orgPos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						0, -- [4]
						0, -- [5]
					},
					["name"] = "BasicActionButtonsMover",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						0, -- [4]
						0, -- [5]
					},
				},
				["PlayerPowerBarAltMover"] = {
					["orgPos"] = {
						"BOTTOM", -- [1]
						"UIParent", -- [2]
						"BOTTOM", -- [3]
						0, -- [4]
						155, -- [5]
					},
					["name"] = "PlayerPowerBarAltMover",
					["scale"] = 1.064583889891638,
					["orgScale"] = 1,
					["pos"] = {
						"BOTTOM", -- [1]
						"UIParent", -- [2]
						"BOTTOM", -- [3]
						-321.6269228529225, -- [4]
						189.3691864013672, -- [5]
					},
				},
				["ObjectiveTrackerFrameMover"] = {
					["orgPos"] = {
						"TOPRIGHT", -- [1]
						"MinimapCluster", -- [2]
						"BOTTOMRIGHT", -- [3]
						-10, -- [4]
						0, -- [5]
					},
					["name"] = "ObjectiveTrackerFrameMover",
					["orgHeight"] = 700,
					["height"] = 253.8681640625,
					["orgWidth"] = 235.0000457763672,
					["pos"] = {
						"TOPRIGHT", -- [1]
						"MinimapCluster", -- [2]
						"BOTTOMRIGHT", -- [3]
						-740.925537109375, -- [4]
						-684.1329345703125, -- [5]
					},
					["width"] = 257.6663513183594,
				},
				["ExtraAbilityContainer"] = {
					["orgPos"] = {
						"CENTER", -- [1]
						"BT4BarExtraActionBar", -- [2]
						"TOPLEFT", -- [3]
						64, -- [4]
						-64, -- [5]
					},
					["name"] = "ExtraAbilityContainer",
					["scale"] = 1,
					["orgScale"] = 1,
					["pos"] = {
						"CENTER", -- [1]
						"BT4BarExtraActionBar", -- [2]
						"TOPLEFT", -- [3]
						18.4012451171875, -- [4]
						4.37530517578125, -- [5]
					},
				},
				["PlayerBuffsMover"] = {
					["orgPos"] = {
						"TOPRIGHT", -- [1]
						"UIParent", -- [2]
						"TOPRIGHT", -- [3]
						-205, -- [4]
						-13, -- [5]
					},
					["name"] = "PlayerBuffsMover",
					["hidden"] = true,
					["pos"] = {
						"TOPRIGHT", -- [1]
						"UIParent", -- [2]
						"TOPRIGHT", -- [3]
						-1543.400146484375, -- [4]
						-762.5997619628906, -- [5]
					},
				},
				["QuestLogPopupDetailFrame"] = {
					["UIPanelWindows"] = {
						["whileDead"] = 1,
						["pushable"] = 0,
						["area"] = "left",
					},
					["orgPos"] = {
						"TOPLEFT", -- [1]
						"UIParent", -- [2]
						"TOPLEFT", -- [3]
						0, -- [4]
						0, -- [5]
					},
					["name"] = "QuestLogPopupDetailFrame",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						0, -- [4]
						463.9999694824219, -- [5]
					},
				},
			},
		},
	},
	["frameListRows"] = 20,
	["dontHookCreateFrame"] = false,
	["modifiedFramesOnly"] = true,
	["squareMM"] = false,
	["playSound"] = true,
	["noBags"] = false,
	["closeGUIOnEscape"] = true,
}
