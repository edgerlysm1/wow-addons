
BeanCounterDB = {
	["Sargeras"] = {
		["Botann"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
			},
			["postedAuctions"] = {
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
			},
			["failedBidsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
			},
			["completedAuctions"] = {
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["failedBids"] = {
			},
		},
		["Thorjitsu"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
			},
			["postedAuctions"] = {
				["136924"] = {
					["item:136924::::::::100:::14::::"] = {
						"1;2837835;3153150;2880;100;1472315066;", -- [1]
					},
				},
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
			},
			["failedBidsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
				["54443"] = {
					["item:54443::::::::100:::::::"] = {
						"1;;;;2709998;2709998;Trìplock;1472257909;;A", -- [1]
						"1;;;;2709998;2709998;Trìplock;1472257782;;A", -- [2]
						"1;;;;2709998;2709998;Trìplock;1472257779;;A", -- [3]
						"1;;;;2709998;2709998;Trìplock;1472257777;;A", -- [4]
					},
				},
				["128312"] = {
					["item:128312::::::::100:::::::"] = {
						"1;;;;109969998;109969998;Wreckless;1472313136;;A", -- [1]
					},
				},
			},
			["completedAuctions"] = {
				["136924"] = {
					["item:136924::::::::100:::14::::"] = {
						"1;2995593;100;157657;3153150;2837835;Lethoian;1472485787;;A", -- [1]
					},
				},
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["failedBids"] = {
			},
		},
		["Thorddin"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
				["109076"] = {
					["item:109076::::::::100:::::::"] = {
						"20;3500000;Tigreess;boolean true;2;1473605719;", -- [1]
						"20;3500000;Tigreess;boolean true;3;1473605715;", -- [2]
						"20;3500000;Tigreess;boolean true;3;1473605711;", -- [3]
						"20;3500000;Tigreess;boolean true;3;1473605709;", -- [4]
					},
				},
				["136685"] = {
					["item:136685::::::::100:::13::::"] = {
					},
				},
				["123919"] = {
					["item:123919::::::::100:::::::"] = {
						"20;14052500;Superbabyj;boolean false;3;1473722915;", -- [1]
						"20;14052500;Superbabyj;boolean false;3;1473722915;", -- [2]
						"20;14052500;Superbabyj;boolean false;3;1473722914;", -- [3]
						"20;14052500;Superbabyj;boolean false;3;1473722896;", -- [4]
						"20;14052500;Superbabyj;boolean false;3;1473722893;", -- [5]
						"11;7508875;Imaelf;boolean false;3;1473722886;", -- [6]
						"20;14052500;Superbabyj;boolean false;3;1473722880;", -- [7]
						"1;537000;Vàlentìne;boolean false;1;1473637990;", -- [8]
						"1;488000;Vàlentìne;boolean false;1;1473637988;", -- [9]
						"1;537000;Vàlentìne;boolean false;2;1473637987;", -- [10]
						"1;488000;Vàlentìne;boolean false;2;1473637982;", -- [11]
						"1;465000;Vàlentìne;boolean false;1;1473635471;", -- [12]
						"1;443000;Vàlentìne;boolean false;1;1473635446;", -- [13]
						"1;422000;Vàlentìne;boolean false;1;1473635276;", -- [14]
						"1;422000;Vàlentìne;boolean false;1;1473635208;", -- [15]
						"1;422000;Vàlentìne;boolean false;1;1473635085;", -- [16]
						"1;422000;Vàlentìne;boolean false;1;1473635084;", -- [17]
						"1;422000;Vàlentìne;boolean false;1;1473635082;", -- [18]
						"1;422000;Vàlentìne;boolean false;1;1473635075;", -- [19]
						"1;422000;Vàlentìne;boolean false;1;1473634757;", -- [20]
						"1;402000;Vàlentìne;boolean false;1;1473634700;", -- [21]
						"1;334750;Moonjumper;boolean false;4;1473618277;", -- [22]
						"1;334750;Moonjumper;boolean false;4;1473618276;", -- [23]
						"1;334750;Moonjumper;boolean false;4;1473618271;", -- [24]
						"1;334750;Moonjumper;boolean false;4;1473618270;", -- [25]
						"1;334750;Moonjumper;boolean false;4;1473618269;", -- [26]
						"1;383000;Vàlentìne;boolean false;3;1473597104;", -- [27]
						"1;383000;Vàlentìne;boolean false;3;1473597103;", -- [28]
						"1;383000;Vàlentìne;boolean false;3;1473597100;", -- [29]
						"1;383000;Vàlentìne;boolean false;3;1473597100;", -- [30]
						"1;523999;Vinroth;boolean false;3;1473556405;", -- [31]
						"1;523999;Vinroth;boolean false;3;1473556402;", -- [32]
						"1;523999;Vinroth;boolean false;3;1473556401;", -- [33]
						"1;523999;Vinroth;boolean false;3;1473556400;", -- [34]
						"1;523999;Vinroth;boolean false;3;1473556399;", -- [35]
						"1;523999;Vinroth;boolean false;3;1473556396;", -- [36]
						"1;523999;Vinroth;boolean false;3;1473556395;", -- [37]
						"1;523999;Vinroth;boolean false;3;1473556395;", -- [38]
						"1;523999;Vinroth;boolean false;3;1473556394;", -- [39]
						"1;523999;Vinroth;boolean false;3;1473556393;", -- [40]
						"1;523999;Vinroth;boolean false;3;1473556393;", -- [41]
						"1;523999;Vinroth;boolean false;3;1473556392;", -- [42]
						"1;523999;Vinroth;boolean false;3;1473556392;", -- [43]
						"1;523999;Vinroth;boolean false;3;1473556391;", -- [44]
						"1;523999;Vinroth;boolean false;3;1473556390;", -- [45]
						"1;523999;Vinroth;boolean false;3;1473556384;", -- [46]
						"1;523999;Vinroth;boolean false;3;1473556383;", -- [47]
						"1;523999;Vinroth;boolean false;3;1473556381;", -- [48]
						"1;523999;Vinroth;boolean false;3;1473556380;", -- [49]
						"1;523999;Vinroth;boolean false;3;1473556379;", -- [50]
						"1;523999;Vinroth;boolean false;3;1473556379;", -- [51]
						"1;523999;Vinroth;boolean false;3;1473556378;", -- [52]
						"1;523999;Vinroth;boolean false;3;1473556377;", -- [53]
						"1;523999;Vinroth;boolean false;3;1473556376;", -- [54]
						"1;523999;Vinroth;boolean false;3;1473556376;", -- [55]
						"1;523999;Vinroth;boolean false;3;1473556375;", -- [56]
						"1;523999;Vinroth;boolean false;3;1473556374;", -- [57]
						"1;523999;Vinroth;boolean false;3;1473556371;", -- [58]
						"1;523999;Vinroth;boolean false;3;1473556370;", -- [59]
						"1;523999;Vinroth;boolean false;3;1473556369;", -- [60]
						"1;523999;Vinroth;boolean false;3;1473556369;", -- [61]
						"1;523999;Vinroth;boolean false;3;1473556368;", -- [62]
						"1;523999;Vinroth;boolean false;3;1473556368;", -- [63]
						"1;523999;Vinroth;boolean false;3;1473556367;", -- [64]
						"1;523999;Vinroth;boolean false;3;1473556367;", -- [65]
						"1;523999;Vinroth;boolean false;3;1473556366;", -- [66]
						"1;523999;Vinroth;boolean false;3;1473556366;", -- [67]
						"1;523999;Vinroth;boolean false;3;1473556365;", -- [68]
						"1;523999;Vinroth;boolean false;3;1473556365;", -- [69]
						"1;523999;Vinroth;boolean false;3;1473556362;", -- [70]
						"1;523999;Vinroth;boolean false;3;1473556362;", -- [71]
						"1;523999;Vinroth;boolean false;3;1473556361;", -- [72]
						"1;523999;Vinroth;boolean false;3;1473556360;", -- [73]
						"1;523999;Vinroth;boolean false;3;1473556360;", -- [74]
						"1;523999;Vinroth;boolean false;3;1473556359;", -- [75]
						"1;523999;Vinroth;boolean false;3;1473556359;", -- [76]
						"1;523999;Vinroth;boolean false;3;1473556358;", -- [77]
						"1;523999;Vinroth;boolean false;3;1473556358;", -- [78]
						"1;523999;Vinroth;boolean false;3;1473556357;", -- [79]
						"1;523999;Vinroth;boolean false;3;1473556357;", -- [80]
						"1;680000;Dontsneeze;boolean true;4;1473556271;", -- [81]
						"1;680000;Dontsneeze;boolean true;4;1473556271;", -- [82]
						"1;680000;Dontsneeze;boolean true;4;1473556270;", -- [83]
						"1;680000;Dontsneeze;boolean true;4;1473556260;", -- [84]
						"1;680000;Dontsneeze;boolean true;4;1473556257;", -- [85]
						"1;680000;Dontsneeze;boolean true;4;1473556256;", -- [86]
						"1;680000;Dontsneeze;boolean true;4;1473556255;", -- [87]
						"1;680000;Dontsneeze;boolean true;4;1473556254;", -- [88]
						"1;680000;Dontsneeze;boolean true;4;1473556249;", -- [89]
						"1;680000;Dontsneeze;boolean true;4;1473556211;", -- [90]
						"1;680000;Dontsneeze;boolean true;4;1473556211;", -- [91]
						"1;680000;Dontsneeze;boolean true;4;1473556210;", -- [92]
						"1;680000;Dontsneeze;boolean true;4;1473556209;", -- [93]
						"1;680000;Dontsneeze;boolean true;4;1473556208;", -- [94]
						"1;680000;Dontsneeze;boolean true;4;1473555863;", -- [95]
						"1;680000;Dontsneeze;boolean true;4;1473555855;", -- [96]
						"1;680000;Dontsneeze;boolean true;4;1473555855;", -- [97]
						"1;680000;Dontsneeze;boolean true;4;1473555850;", -- [98]
						"1;680000;Dontsneeze;boolean true;4;1473555836;", -- [99]
						"1;680000;Dontsneeze;boolean true;4;1473555833;", -- [100]
						"1;680000;Dontsneeze;boolean true;4;1473555831;", -- [101]
						"1;680000;Dontsneeze;boolean true;4;1473555827;", -- [102]
						"1;680000;Dontsneeze;boolean true;4;1473555795;", -- [103]
						"1;348000;Vàlentìne;boolean false;4;1473555528;", -- [104]
						"1;348000;Vàlentìne;boolean false;4;1473555527;", -- [105]
						"1;317000;Vàlentìne;boolean false;4;1473555443;", -- [106]
						"1;288000;Vàlentìne;boolean false;4;1473555440;", -- [107]
						"1;275000;Vàlentìne;boolean false;4;1473555418;", -- [108]
						"1;266125;Pattyheel;boolean false;3;1473555417;", -- [109]
						"1;263500;Pattyheel;boolean false;3;1473555416;", -- [110]
						"1;262000;Vàlentìne;boolean false;4;1473555395;", -- [111]
						"1;262000;Vàlentìne;boolean false;4;1473555394;", -- [112]
						"1;262000;Vàlentìne;boolean false;4;1473555390;", -- [113]
						"1;68065;Vorniir;boolean false;3;1473555387;", -- [114]
						"1;59065;Vorniir;boolean false;3;1473555383;", -- [115]
						"1;2950;Iluvpooc;boolean false;4;1473555272;", -- [116]
						"1;2950;Iluvpooc;boolean false;4;1473555270;", -- [117]
						"1;5440;Metaltheory;boolean false;3;1473553648;", -- [118]
						"1;2680;Iluvpooc;boolean false;4;1473553565;", -- [119]
						"1;2680;Iluvpooc;boolean false;4;1473553564;", -- [120]
						"1;2680;Iluvpooc;boolean false;4;1473553562;", -- [121]
						"1;2680;Iluvpooc;boolean false;4;1473553561;", -- [122]
					},
				},
				["124117"] = {
					["item:124117::::::::100:::::::"] = {
						"199;935300;Punchshi;boolean true;4;1473537430;", -- [1]
						"200;960000;Faunsama;boolean true;4;1473537429;", -- [2]
						"1;1485;Goochii;boolean true;3;1473537428;", -- [3]
						"1;1485;Goochii;boolean true;3;1473537427;", -- [4]
						"1;1485;Goochii;boolean true;3;1473537427;", -- [5]
						"1;1485;Goochii;boolean true;3;1473537424;", -- [6]
						"1;1485;Goochii;boolean true;3;1473537423;", -- [7]
						"1;1485;Goochii;boolean true;3;1473537423;", -- [8]
						"1;1485;Goochii;boolean true;3;1473537422;", -- [9]
						"1;1485;Goochii;boolean true;3;1473537422;", -- [10]
						"1;1485;Goochii;boolean true;3;1473537420;", -- [11]
						"1;1485;Goochii;boolean true;3;1473537419;", -- [12]
						"1;1485;Goochii;boolean true;3;1473537417;", -- [13]
						"1;1485;Goochii;boolean true;3;1473537416;", -- [14]
						"1;1485;Goochii;boolean true;3;1473537415;", -- [15]
						"1;1485;Goochii;boolean true;3;1473537415;", -- [16]
						"1;1485;Goochii;boolean true;3;1473537414;", -- [17]
						"1;1485;Goochii;boolean true;3;1473537414;", -- [18]
						"1;1485;Goochii;boolean true;3;1473537413;", -- [19]
						"1;1485;Goochii;boolean true;3;1473537413;", -- [20]
						"1;1485;Goochii;boolean true;3;1473537412;", -- [21]
						"1;1485;Goochii;boolean true;3;1473537411;", -- [22]
						"1;1485;Goochii;boolean true;3;1473537411;", -- [23]
						"1;1485;Goochii;boolean true;3;1473537406;", -- [24]
						"1;1485;Goochii;boolean true;3;1473537406;", -- [25]
						"1;1485;Goochii;boolean true;3;1473537405;", -- [26]
						"1;1485;Goochii;boolean true;3;1473537404;", -- [27]
						"1;1485;Goochii;boolean true;3;1473537399;", -- [28]
						"1;1485;Goochii;boolean true;3;1473537399;", -- [29]
						"1;1485;Goochii;boolean true;3;1473537398;", -- [30]
						"1;1485;Goochii;boolean true;3;1473537398;", -- [31]
						"1;1485;Goochii;boolean true;3;1473537396;", -- [32]
						"1;1485;Goochii;boolean true;3;1473537395;", -- [33]
						"1;1485;Goochii;boolean true;3;1473537394;", -- [34]
						"1;1485;Goochii;boolean true;3;1473537392;", -- [35]
						"1;1485;Goochii;boolean true;3;1473537391;", -- [36]
						"1;1485;Goochii;boolean true;3;1473537389;", -- [37]
						"1;1485;Goochii;boolean true;3;1473537389;", -- [38]
						"1;1485;Goochii;boolean true;3;1473537387;", -- [39]
						"1;1485;Goochii;boolean true;3;1473537386;", -- [40]
						"1;1485;Goochii;boolean true;3;1473537379;", -- [41]
						"1;1485;Goochii;boolean true;3;1473537378;", -- [42]
						"1;1485;Goochii;boolean true;3;1473537378;", -- [43]
						"1;1485;Goochii;boolean true;3;1473537371;", -- [44]
						"1;1485;Goochii;boolean true;3;1473537370;", -- [45]
						"1;1485;Goochii;boolean true;3;1473537370;", -- [46]
						"1;1485;Goochii;boolean true;3;1473537368;", -- [47]
						"1;1485;Goochii;boolean true;3;1473537368;", -- [48]
						"1;1485;Goochii;boolean true;3;1473537367;", -- [49]
						"1;1485;Goochii;boolean true;3;1473537367;", -- [50]
						"1;1485;Goochii;boolean true;3;1473537365;", -- [51]
						"1;1485;Goochii;boolean true;3;1473537365;", -- [52]
						"1;1485;Goochii;boolean true;3;1473537364;", -- [53]
						"1;1485;Goochii;boolean true;3;1473537364;", -- [54]
						"1;1485;Goochii;boolean true;3;1473537363;", -- [55]
						"1;1485;Goochii;boolean true;3;1473537362;", -- [56]
						"1;1485;Goochii;boolean true;3;1473537362;", -- [57]
						"1;1485;Goochii;boolean true;3;1473537362;", -- [58]
						"1;1485;Goochii;boolean true;3;1473537360;", -- [59]
						"1;1485;Goochii;boolean true;3;1473537360;", -- [60]
						"1;1485;Goochii;boolean true;3;1473537359;", -- [61]
						"1;1485;Goochii;boolean true;3;1473537359;", -- [62]
						"1;1485;Goochii;boolean true;3;1473537355;", -- [63]
						"1;1485;Goochii;boolean true;3;1473537355;", -- [64]
						"1;1485;Goochii;boolean true;3;1473537354;", -- [65]
						"1;1485;Goochii;boolean true;3;1473537353;", -- [66]
						"1;1485;Goochii;boolean true;3;1473537353;", -- [67]
						"1;1485;Goochii;boolean true;3;1473537349;", -- [68]
						"1;1485;Goochii;boolean true;3;1473537349;", -- [69]
						"1;1485;Goochii;boolean true;3;1473537348;", -- [70]
						"1;1485;Goochii;boolean true;3;1473537348;", -- [71]
						"1;1485;Goochii;boolean true;3;1473537347;", -- [72]
						"1;1485;Goochii;boolean true;3;1473537346;", -- [73]
						"1;1485;Goochii;boolean true;3;1473537346;", -- [74]
						"1;1485;Goochii;boolean true;3;1473537345;", -- [75]
						"1;1485;Goochii;boolean true;3;1473537345;", -- [76]
						"1;1485;Goochii;boolean true;3;1473537329;", -- [77]
						"1;1485;Goochii;boolean true;3;1473537328;", -- [78]
						"1;1485;Goochii;boolean true;3;1473537328;", -- [79]
						"1;1485;Goochii;boolean true;3;1473537327;", -- [80]
						"1;1485;Goochii;boolean true;3;1473537327;", -- [81]
						"1;1485;Goochii;boolean true;3;1473537326;", -- [82]
						"1;1485;Goochii;boolean true;3;1473537325;", -- [83]
						"1;1485;Goochii;boolean true;3;1473537325;", -- [84]
						"1;1485;Goochii;boolean true;3;1473537324;", -- [85]
						"1;1485;Goochii;boolean true;3;1473537323;", -- [86]
						"1;1485;Goochii;boolean true;3;1473537322;", -- [87]
						"1;1485;Goochii;boolean true;3;1473537322;", -- [88]
						"1;1485;Goochii;boolean true;3;1473537318;", -- [89]
						"1;1485;Goochii;boolean true;3;1473537317;", -- [90]
						"1;1485;Goochii;boolean true;3;1473537316;", -- [91]
						"1;1485;Goochii;boolean true;3;1473537315;", -- [92]
						"1;1485;Goochii;boolean true;3;1473537314;", -- [93]
						"1;1485;Goochii;boolean true;3;1473537312;", -- [94]
						"1;1485;Goochii;boolean true;3;1473537311;", -- [95]
						"1;1485;Goochii;boolean true;3;1473537310;", -- [96]
						"1;1485;Goochii;boolean true;3;1473537309;", -- [97]
						"1;1485;Goochii;boolean true;3;1473537308;", -- [98]
						"1;1485;Goochii;boolean true;3;1473537308;", -- [99]
						"1;1485;Goochii;boolean true;3;1473537307;", -- [100]
						"1;1485;Goochii;boolean true;3;1473537300;", -- [101]
						"1;1485;Goochii;boolean true;3;1473537299;", -- [102]
						"1;1485;Goochii;boolean true;3;1473537299;", -- [103]
						"1;1485;Goochii;boolean true;3;1473537298;", -- [104]
						"1;1485;Goochii;boolean true;3;1473537297;", -- [105]
						"1;1485;Goochii;boolean true;3;1473537297;", -- [106]
						"1;1485;Goochii;boolean true;3;1473537296;", -- [107]
						"1;1485;Goochii;boolean true;3;1473537295;", -- [108]
						"1;1485;Goochii;boolean true;3;1473537294;", -- [109]
						"1;1485;Goochii;boolean true;3;1473537293;", -- [110]
						"1;1485;Goochii;boolean true;3;1473537292;", -- [111]
						"1;1485;Goochii;boolean true;3;1473537289;", -- [112]
						"1;1485;Goochii;boolean true;3;1473537285;", -- [113]
						"1;1485;Goochii;boolean true;3;1473537284;", -- [114]
						"1;1485;Goochii;boolean true;3;1473537283;", -- [115]
						"1;1485;Goochii;boolean true;3;1473537283;", -- [116]
						"1;1485;Goochii;boolean true;3;1473537282;", -- [117]
						"1;1485;Goochii;boolean true;3;1473537281;", -- [118]
						"1;1485;Goochii;boolean true;3;1473537280;", -- [119]
						"1;1485;Goochii;boolean true;3;1473537279;", -- [120]
						"1;1485;Goochii;boolean true;3;1473537276;", -- [121]
						"1;1485;Goochii;boolean true;3;1473537274;", -- [122]
						"1;1485;Goochii;boolean true;3;1473537274;", -- [123]
						"1;1485;Goochii;boolean true;3;1473537272;", -- [124]
						"1;1485;Goochii;boolean true;3;1473537269;", -- [125]
						"1;1485;Goochii;boolean true;3;1473537226;", -- [126]
						"1;1485;Goochii;boolean true;3;1473536750;", -- [127]
						"1;1485;Goochii;boolean true;3;1473536746;", -- [128]
						"1;4797;Tsilya;boolean true;3;1473535272;", -- [129]
						"1;4797;Tsilya;boolean true;3;1473535269;", -- [130]
						"1;4797;Tsilya;boolean true;3;1473535269;", -- [131]
						"1;4797;Tsilya;boolean true;3;1473535268;", -- [132]
						"1;4797;Tsilya;boolean true;3;1473535265;", -- [133]
						"1;5000;Armoise;boolean true;4;1473535263;", -- [134]
						"1;5000;Armoise;boolean true;4;1473535262;", -- [135]
						"1;5000;Armoise;boolean true;4;1473535262;", -- [136]
						"1;4797;Tsilya;boolean true;3;1473535260;", -- [137]
						"1;4797;Tsilya;boolean true;3;1473535259;", -- [138]
						"1;4797;Tsilya;boolean true;3;1473535259;", -- [139]
						"1;4797;Tsilya;boolean true;3;1473535258;", -- [140]
						"1;4797;Tsilya;boolean true;3;1473535257;", -- [141]
						"1;4797;Tsilya;boolean true;3;1473535256;", -- [142]
						"1;4797;Tsilya;boolean true;3;1473535255;", -- [143]
						"1;4797;Tsilya;boolean true;3;1473535254;", -- [144]
						"1;4797;Tsilya;boolean true;3;1473535254;", -- [145]
						"1;4797;Tsilya;boolean true;3;1473535253;", -- [146]
						"1;4797;Tsilya;boolean true;3;1473535251;", -- [147]
						"1;4797;Tsilya;boolean true;3;1473535249;", -- [148]
						"1;4797;Tsilya;boolean true;3;1473535248;", -- [149]
						"1;4797;Tsilya;boolean true;3;1473535247;", -- [150]
						"1;4797;Tsilya;boolean true;3;1473535247;", -- [151]
						"1;4797;Tsilya;boolean true;3;1473535246;", -- [152]
						"1;4797;Tsilya;boolean true;3;1473535245;", -- [153]
						"1;4797;Tsilya;boolean true;3;1473535245;", -- [154]
						"1;4797;Tsilya;boolean true;3;1473535243;", -- [155]
						"1;4797;Tsilya;boolean true;3;1473535241;", -- [156]
						"1;4797;Tsilya;boolean true;3;1473535240;", -- [157]
						"1;4797;Tsilya;boolean true;3;1473535239;", -- [158]
						"1;4797;Tsilya;boolean true;3;1473535238;", -- [159]
						"1;4797;Tsilya;boolean true;3;1473535236;", -- [160]
						"1;4797;Tsilya;boolean true;3;1473535234;", -- [161]
						"1;4797;Tsilya;boolean true;3;1473535234;", -- [162]
						"1;4797;Tsilya;boolean true;3;1473535232;", -- [163]
						"1;4797;Tsilya;boolean true;3;1473535231;", -- [164]
						"1;4797;Tsilya;boolean true;3;1473535230;", -- [165]
						"1;4797;Tsilya;boolean true;3;1473535230;", -- [166]
						"1;4797;Tsilya;boolean true;3;1473535229;", -- [167]
						"1;4797;Tsilya;boolean true;3;1473535229;", -- [168]
						"1;4797;Tsilya;boolean true;3;1473535228;", -- [169]
						"1;4797;Tsilya;boolean true;3;1473535227;", -- [170]
						"1;4797;Tsilya;boolean true;3;1473535226;", -- [171]
						"1;4797;Tsilya;boolean true;3;1473535219;", -- [172]
						"1;4797;Tsilya;boolean true;3;1473535219;", -- [173]
						"1;4797;Tsilya;boolean true;3;1473535218;", -- [174]
						"1;4797;Tsilya;boolean true;3;1473535218;", -- [175]
						"1;4797;Tsilya;boolean true;3;1473535217;", -- [176]
						"1;4797;Tsilya;boolean true;3;1473535217;", -- [177]
						"1;4797;Tsilya;boolean true;3;1473535213;", -- [178]
						"1;4797;Tsilya;boolean true;3;1473535213;", -- [179]
						"1;4797;Tsilya;boolean true;3;1473535211;", -- [180]
						"1;4797;Tsilya;boolean true;3;1473535210;", -- [181]
						"1;4797;Tsilya;boolean true;3;1473535209;", -- [182]
						"1;4797;Tsilya;boolean true;3;1473535209;", -- [183]
						"1;4797;Tsilya;boolean true;3;1473535208;", -- [184]
						"1;4797;Tsilya;boolean true;3;1473535208;", -- [185]
						"1;4797;Tsilya;boolean true;3;1473535207;", -- [186]
						"1;4797;Tsilya;boolean true;3;1473535206;", -- [187]
						"1;4500;Askrmire;boolean true;4;1473534979;", -- [188]
						"1;4698;Anjaneya;boolean true;4;1473534978;", -- [189]
						"1;4698;Anjaneya;boolean true;4;1473534976;", -- [190]
						"1;4500;Askrmire;boolean true;4;1473534974;", -- [191]
						"1;3999;Raiseri;boolean true;3;1473534973;", -- [192]
						"1;4500;Askrmire;boolean true;4;1473534973;", -- [193]
						"1;4500;Askrmire;boolean true;4;1473534972;", -- [194]
						"1;4698;Anjaneya;boolean true;4;1473534965;", -- [195]
						"1;4698;Anjaneya;boolean true;4;1473534965;", -- [196]
						"1;4000;Gohku;boolean true;4;1473534956;", -- [197]
						"1;4000;Gohku;boolean true;4;1473534956;", -- [198]
						"1;3999;Raiseri;boolean true;3;1473534955;", -- [199]
						"1;3999;Raiseri;boolean true;3;1473534954;", -- [200]
						"1;3999;Raiseri;boolean true;3;1473534952;", -- [201]
						"1;3999;Raiseri;boolean true;3;1473534949;", -- [202]
						"1;3999;Raiseri;boolean true;3;1473534944;", -- [203]
						"1;1495;Xenos;boolean true;3;1473534928;", -- [204]
						"1;1495;Xenos;boolean true;3;1473534926;", -- [205]
						"1;1495;Xenos;boolean true;3;1473534924;", -- [206]
						"1;1495;Xenos;boolean true;3;1473534924;", -- [207]
						"1;1495;Xenos;boolean true;3;1473534923;", -- [208]
						"1;1495;Xenos;boolean true;3;1473534923;", -- [209]
						"1;1495;Xenos;boolean true;3;1473534923;", -- [210]
					},
				},
				["116268"] = {
					["item:116268::::::::100:::::::"] = {
					},
				},
				["123918"] = {
					["item:123918::::::::100:::::::"] = {
						"1;3262;Splewlock;boolean false;3;1473725482;Snatch", -- [1]
						"1;3262;Splewlock;boolean false;3;1473725481;Snatch", -- [2]
						"1;3262;Splewlock;boolean false;3;1473725479;Snatch", -- [3]
						"1;3262;Splewlock;boolean false;3;1473725478;Snatch", -- [4]
						"1;3262;Splewlock;boolean false;3;1473725477;Snatch", -- [5]
						"1;3262;Splewlock;boolean false;3;1473725476;Snatch", -- [6]
						"1;3262;Splewlock;boolean false;3;1473725474;Snatch", -- [7]
						"1;3422;Splewlock;boolean false;2;1473725474;Snatch", -- [8]
						"1;3422;Splewlock;boolean false;2;1473725473;Snatch", -- [9]
						"1;3422;Splewlock;boolean false;2;1473725473;Snatch", -- [10]
						"1;3422;Splewlock;boolean false;2;1473725472;Snatch", -- [11]
						"1;3422;Splewlock;boolean false;2;1473725471;Snatch", -- [12]
						"1;3422;Splewlock;boolean false;2;1473725471;Snatch", -- [13]
						"1;3422;Splewlock;boolean false;2;1473725470;Snatch", -- [14]
						"1;3592;Splewlock;boolean false;2;1473725469;Snatch", -- [15]
						"1;3592;Splewlock;boolean false;2;1473725469;Snatch", -- [16]
						"1;3592;Splewlock;boolean false;2;1473725448;Snatch", -- [17]
						"1;3762;Splewlock;boolean false;3;1473725448;Snatch", -- [18]
						"1;3422;Splewlock;boolean false;3;1473725447;Snatch", -- [19]
						"1;3422;Splewlock;boolean false;3;1473725446;Snatch", -- [20]
						"1;3422;Splewlock;boolean false;3;1473725445;Snatch", -- [21]
						"1;3422;Splewlock;boolean false;3;1473725445;Snatch", -- [22]
						"1;3422;Splewlock;boolean false;3;1473725444;Snatch", -- [23]
						"1;3422;Splewlock;boolean false;3;1473725443;Snatch", -- [24]
						"1;3422;Splewlock;boolean false;3;1473725442;Snatch", -- [25]
						"1;3422;Splewlock;boolean false;3;1473725441;Snatch", -- [26]
						"1;3422;Splewlock;boolean false;3;1473725440;Snatch", -- [27]
						"1;3422;Splewlock;boolean false;3;1473725440;Snatch", -- [28]
						"1;3422;Splewlock;boolean false;3;1473725439;Snatch", -- [29]
						"1;3422;Splewlock;boolean false;3;1473725437;Snatch", -- [30]
						"1;3422;Splewlock;boolean false;3;1473725437;Snatch", -- [31]
						"1;3422;Splewlock;boolean false;2;1473725436;Snatch", -- [32]
						"1;3422;Splewlock;boolean false;2;1473725435;Snatch", -- [33]
						"1;3422;Splewlock;boolean false;2;1473725435;Snatch", -- [34]
						"1;3422;Splewlock;boolean false;2;1473725434;Snatch", -- [35]
						"1;3422;Splewlock;boolean false;2;1473725434;Snatch", -- [36]
						"1;3422;Splewlock;boolean false;2;1473725433;Snatch", -- [37]
						"1;3422;Splewlock;boolean false;2;1473725432;Snatch", -- [38]
						"1;3422;Splewlock;boolean false;2;1473725431;Snatch", -- [39]
						"1;3422;Splewlock;boolean false;2;1473725430;Snatch", -- [40]
						"1;3422;Splewlock;boolean false;2;1473725429;Snatch", -- [41]
						"1;3422;Splewlock;boolean false;3;1473725429;Snatch", -- [42]
						"1;3422;Splewlock;boolean false;3;1473725428;Snatch", -- [43]
						"1;3422;Splewlock;boolean false;3;1473725428;Snatch", -- [44]
						"1;3592;Splewlock;boolean false;3;1473725427;Snatch", -- [45]
						"1;3592;Splewlock;boolean false;3;1473725426;Snatch", -- [46]
						"1;3592;Splewlock;boolean false;3;1473725426;Snatch", -- [47]
						"1;3592;Splewlock;boolean false;3;1473725425;Snatch", -- [48]
						"1;3592;Splewlock;boolean false;3;1473725424;Snatch", -- [49]
						"1;3592;Splewlock;boolean false;3;1473725424;Snatch", -- [50]
						"1;3592;Splewlock;boolean false;3;1473725423;Snatch", -- [51]
						"1;3592;Splewlock;boolean false;3;1473725422;Snatch", -- [52]
						"1;3592;Splewlock;boolean false;3;1473725421;Snatch", -- [53]
						"1;3422;Splewlock;boolean false;3;1473725421;Snatch", -- [54]
						"1;3422;Splewlock;boolean false;3;1473725419;Snatch", -- [55]
						"1;3592;Splewlock;boolean false;2;1473725417;Snatch", -- [56]
						"1;3592;Splewlock;boolean false;2;1473725416;Snatch", -- [57]
						"1;3592;Splewlock;boolean false;2;1473725416;Snatch", -- [58]
						"1;3942;Splewlock;boolean false;3;1473725415;Snatch", -- [59]
						"1;3942;Splewlock;boolean false;2;1473725414;Snatch", -- [60]
						"1;3942;Splewlock;boolean false;2;1473725413;Snatch", -- [61]
						"1;3942;Splewlock;boolean false;2;1473725412;Snatch", -- [62]
						"1;97862;Splewlock;boolean false;2;1473725411;Snatch", -- [63]
						"1;3422;Splewlock;boolean false;3;1473718071;", -- [64]
						"1;3422;Splewlock;boolean false;3;1473718070;", -- [65]
						"1;3262;Splewlock;boolean false;3;1473718069;", -- [66]
						"1;3422;Splewlock;boolean false;3;1473718069;", -- [67]
						"1;3422;Splewlock;boolean false;3;1473718052;", -- [68]
						"1;83570;Lizet;boolean false;3;1473605781;", -- [69]
						"1;83570;Lizet;boolean false;3;1473605772;", -- [70]
						"1;3740;Lizet;boolean false;3;1473597383;", -- [71]
						"1;2645;Lukeno;boolean false;4;1473537511;", -- [72]
						"1;1270;Cimby;boolean false;3;1473537442;", -- [73]
						"1;1270;Cimby;boolean false;3;1473537441;", -- [74]
					},
				},
				["124461"] = {
					["item:124461::::::::100:::::::"] = {
						"1;1995000;Angryfabio;boolean true;4;1473701703;", -- [1]
						"1;1995000;Angryfabio;boolean true;4;1473701701;", -- [2]
						"1;1995000;Angryfabio;boolean true;4;1473701700;", -- [3]
						"1;1970000;Womboscombo;boolean true;4;1473700692;", -- [4]
						"1;1970000;Womboscombo;boolean true;4;1473700690;", -- [5]
						"1;1970000;Womboscombo;boolean true;4;1473700688;", -- [6]
						"1;1970000;Womboscombo;boolean true;4;1473700687;", -- [7]
						"1;1970000;Womboscombo;boolean true;4;1473700686;", -- [8]
						"1;1970000;Womboscombo;boolean true;4;1473700669;", -- [9]
					},
				},
				["124444"] = {
					["item:124444::::::::100:::::::"] = {
						"1;4375800;Pleuve;boolean true;4;1473537437;", -- [1]
						"1;4375800;Pleuve;boolean true;4;1473537437;", -- [2]
						"1;4306500;Bymz;boolean true;4;1473537436;", -- [3]
					},
				},
			},
			["postedAuctions"] = {
				["136708"] = {
					["item:136708::::::::100:::::::"] = {
						"1;22000000;25000000;720;100;1473701493;", -- [1]
						"1;22000000;25000000;720;100;1473701493;", -- [2]
						"1;25000000;26500000;720;100;1473701123;", -- [3]
						"1;25000000;26500000;720;100;1473701123;", -- [4]
						"1;25000000;26500000;720;100;1473701122;", -- [5]
						"1;25000000;26500000;720;100;1473701121;", -- [6]
						"1;25000000;27000000;720;100;1473638071;", -- [7]
						"1;25000000;27000000;720;100;1473638070;", -- [8]
						"1;25000000;27000000;720;100;1473638069;", -- [9]
						"1;25000000;27000000;720;100;1473638068;", -- [10]
						"1;25000000;27000000;720;100;1473638067;", -- [11]
						"1;25000000;27000000;720;100;1473638066;", -- [12]
						"4;100000000;110000000;1440;100;1473607410;", -- [13]
						"1;20000000;25000000;2880;100;1473538862;", -- [14]
					},
				},
				["136683"] = {
					["item:136683::::::::100:::13::::"] = {
						"1;105797340;117552600;720;35499;1473536822;", -- [1]
					},
				},
				["123956"] = {
					["item:123956::::::::100:::::::"] = {
						"1;7000000;7500000;720;100;1473617695;", -- [1]
						"1;5782590;6425100;2880;100;1473538891;", -- [2]
						"1;5782590;6425100;2880;100;1473538890;", -- [3]
					},
				},
				["136685"] = {
					["item:136685::::::::100:::13::::"] = {
						"1;66825000;73500000;720;35763;1473719007;", -- [1]
					},
				},
				["141592"] = {
					["item:141592::::::::100:::11::::"] = {
						"1;9999999;9999999;2880;1200;1473471912;", -- [1]
					},
				},
				["124461"] = {
					["item:124461::::::::100:::::::"] = {
						"6;12900000;13440000;720;1575;1473701356;", -- [1]
						"6;12900000;13440000;720;1575;1473701355;", -- [2]
						"6;12900000;13440000;720;1575;1473701354;", -- [3]
						"6;12900000;13440000;720;1575;1473701353;", -- [4]
						"6;12900000;13440000;720;1575;1473701351;", -- [5]
						"5;10750000;11200000;720;1311;1473701066;", -- [6]
						"5;10750000;11200000;720;1311;1473701065;", -- [7]
						"5;10750000;11200000;720;1311;1473701065;", -- [8]
						"5;10750000;11200000;720;1311;1473701064;", -- [9]
						"10;20000000;22000000;720;2625;1473639097;", -- [10]
						"8;16000000;17600000;720;2100;1473639074;", -- [11]
						"6;12000000;13200000;720;1575;1473639063;", -- [12]
						"5;10000000;11000000;720;1311;1473639053;", -- [13]
					},
				},
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
				["123956"] = {
					["item:123956::::::::100:::::::"] = {
						"1;;100;;6425100;5782590;;1473538639;Cancelled;A", -- [1]
						"1;;100;;6425100;5782590;;1473538590;Cancelled;A", -- [2]
						"1;;100;;6425100;5782590;;1473538583;Cancelled;A", -- [3]
					},
				},
				["136708"] = {
					["item:136708::::::::100:::::::"] = {
						"1;;100;;34650032;31185029;;1473538644;Cancelled;A", -- [1]
					},
				},
				["136685"] = {
					["item:136685::::::::100:::13::::"] = {
						"1;;143052;;89100000;80190000;;1473718966;Cancelled;A", -- [1]
						"1;;35763;;108900000;98010000;;1473662149;;A", -- [2]
					},
				},
			},
			["failedBidsNeutral"] = {
			},
			["failedBids"] = {
				["136685"] = {
					["item:136685::::::::100:::13::::"] = {
						"1;;;;;30007649;Avengerx;1473718150;;A", -- [1]
						"1;;;;;30007649;Avengerx;1473718149;;A", -- [2]
					},
				},
				["123918"] = {
					["item:123918::::::::100:::::::"] = {
						"20;;;;;1500000;Angelolight;1473695809;;A", -- [1]
						"1;;;;;2972;Splewlock;1473659020;;A", -- [2]
						"1;;;;;2972;Splewlock;1473658945;;A", -- [3]
						"1;;;;;2832;Splewlock;1473658750;;A", -- [4]
						"1;;;;;2832;Splewlock;1473658741;;A", -- [5]
						"1;;;;;2832;Splewlock;1473658724;;A", -- [6]
						"1;;;;;2832;Splewlock;1473658720;;A", -- [7]
						"1;;;;;2832;Splewlock;1473658714;;A", -- [8]
						"1;;;;;2832;Splewlock;1473658608;;A", -- [9]
						"1;;;;;2832;Splewlock;1473658594;;A", -- [10]
						"20;;;;;1700000;Angelolight;1473654640;;A", -- [11]
						"20;;;;;1600000;Angelolight;1473654636;;A", -- [12]
						"1;;;;;2702;Splewlock;1473635414;;A", -- [13]
						"1;;;;;2702;Splewlock;1473631540;;A", -- [14]
						"1;;;;;2832;Splewlock;1473622974;;A", -- [15]
						"1;;;;;2832;Splewlock;1473622973;;A", -- [16]
						"1;;;;;2832;Splewlock;1473622970;;A", -- [17]
						"1;;;;;2832;Splewlock;1473622968;;A", -- [18]
						"1;;;;;2832;Splewlock;1473622964;;A", -- [19]
						"1;;;;;2832;Splewlock;1473622960;;A", -- [20]
						"1;;;;;2832;Splewlock;1473622956;;A", -- [21]
						"1;;;;;2582;Splewlock;1473621650;;A", -- [22]
						"1;;;;;2582;Splewlock;1473621650;;A", -- [23]
						"1;;;;;2582;Splewlock;1473621649;;A", -- [24]
						"1;;;;;2582;Splewlock;1473621649;;A", -- [25]
						"1;;;;;2582;Splewlock;1473621648;;A", -- [26]
						"1;;;;;2582;Splewlock;1473621646;;A", -- [27]
						"1;;;;;2582;Splewlock;1473621645;;A", -- [28]
						"1;;;;;2582;Splewlock;1473621645;;A", -- [29]
						"1;;;;;2582;Splewlock;1473621644;;A", -- [30]
						"1;;;;;2582;Splewlock;1473621643;;A", -- [31]
						"1;;;;;2582;Splewlock;1473621643;;A", -- [32]
						"1;;;;;2582;Splewlock;1473621642;;A", -- [33]
						"1;;;;;2582;Splewlock;1473621642;;A", -- [34]
						"1;;;;;2582;Splewlock;1473621641;;A", -- [35]
						"1;;;;;2582;Splewlock;1473621640;;A", -- [36]
						"1;;;;;2582;Splewlock;1473621640;;A", -- [37]
						"1;;;;;2582;Splewlock;1473621639;;A", -- [38]
						"1;;;;;2582;Splewlock;1473621639;;A", -- [39]
						"1;;;;;2582;Splewlock;1473621639;;A", -- [40]
						"1;;;;;2582;Splewlock;1473621638;;A", -- [41]
						"1;;;;;2832;Splewlock;1473621708;;A", -- [42]
						"1;;;;;2832;Splewlock;1473621706;;A", -- [43]
						"1;;;;;2832;Splewlock;1473621705;;A", -- [44]
						"1;;;;;2832;Splewlock;1473621702;;A", -- [45]
						"1;;;;;2832;Splewlock;1473621702;;A", -- [46]
						"1;;;;;2832;Splewlock;1473621699;;A", -- [47]
						"1;;;;;2832;Splewlock;1473621699;;A", -- [48]
						"1;;;;;2832;Splewlock;1473621697;;A", -- [49]
						"1;;;;;2832;Splewlock;1473621696;;A", -- [50]
						"1;;;;;2832;Splewlock;1473621696;;A", -- [51]
						"1;;;;;2832;Splewlock;1473621694;;A", -- [52]
						"1;;;;;2832;Splewlock;1473621694;;A", -- [53]
						"1;;;;;2832;Splewlock;1473621693;;A", -- [54]
						"1;;;;;2832;Splewlock;1473621692;;A", -- [55]
						"1;;;;;2702;Splewlock;1473621687;;A", -- [56]
						"1;;;;;2702;Splewlock;1473621686;;A", -- [57]
						"1;;;;;2702;Splewlock;1473621685;;A", -- [58]
						"1;;;;;2702;Splewlock;1473621683;;A", -- [59]
						"1;;;;;2702;Splewlock;1473621677;;A", -- [60]
						"1;;;;;2702;Splewlock;1473621674;;A", -- [61]
						"1;;;;;2582;Splewlock;1473621668;;A", -- [62]
						"1;;;;;2582;Splewlock;1473621668;;A", -- [63]
						"1;;;;;2582;Splewlock;1473621667;;A", -- [64]
						"1;;;;;2582;Splewlock;1473621666;;A", -- [65]
						"1;;;;;2582;Splewlock;1473621664;;A", -- [66]
						"1;;;;;2582;Splewlock;1473621663;;A", -- [67]
						"1;;;;;2582;Splewlock;1473621663;;A", -- [68]
						"1;;;;;2582;Splewlock;1473621662;;A", -- [69]
						"1;;;;;2582;Splewlock;1473621662;;A", -- [70]
						"1;;;;;2582;Splewlock;1473621661;;A", -- [71]
						"1;;;;;2582;Splewlock;1473621661;;A", -- [72]
						"1;;;;;2582;Splewlock;1473621660;;A", -- [73]
						"1;;;;;2582;Splewlock;1473621659;;A", -- [74]
						"1;;;;;2582;Splewlock;1473621658;;A", -- [75]
						"1;;;;;2582;Splewlock;1473621658;;A", -- [76]
						"1;;;;;2582;Splewlock;1473621658;;A", -- [77]
						"1;;;;;2582;Splewlock;1473621658;;A", -- [78]
						"1;;;;;2582;Splewlock;1473621657;;A", -- [79]
						"1;;;;;2582;Splewlock;1473621657;;A", -- [80]
						"1;;;;;2582;Splewlock;1473621657;;A", -- [81]
						"1;;;;;2582;Splewlock;1473621656;;A", -- [82]
						"1;;;;;2582;Splewlock;1473621655;;A", -- [83]
						"1;;;;;2582;Splewlock;1473621655;;A", -- [84]
						"1;;;;;2582;Splewlock;1473621654;;A", -- [85]
						"1;;;;;2582;Splewlock;1473621653;;A", -- [86]
						"1;;;;;2582;Splewlock;1473621653;;A", -- [87]
						"1;;;;;2582;Splewlock;1473621652;;A", -- [88]
						"1;;;;;2582;Splewlock;1473621650;;A", -- [89]
						"1;;;;;2582;Splewlock;1473621650;;A", -- [90]
						"1;;;;;2582;Splewlock;1473621649;;A", -- [91]
						"1;;;;;4805;Epikkmeme;1473622144;;A", -- [92]
						"1;;;;;4805;Epikkmeme;1473622141;;A", -- [93]
						"1;;;;;4805;Epikkmeme;1473622136;;A", -- [94]
						"1;;;;;4805;Epikkmeme;1473622134;;A", -- [95]
						"1;;;;;4805;Epikkmeme;1473622129;;A", -- [96]
						"1;;;;;4805;Epikkmeme;1473622126;;A", -- [97]
						"1;;;;;4805;Epikkmeme;1473621755;;A", -- [98]
						"1;;;;;4805;Epikkmeme;1473621754;;A", -- [99]
						"1;;;;;4805;Epikkmeme;1473621754;;A", -- [100]
						"1;;;;;4805;Epikkmeme;1473621753;;A", -- [101]
						"1;;;;;4805;Epikkmeme;1473621750;;A", -- [102]
						"1;;;;;4805;Epikkmeme;1473621750;;A", -- [103]
						"1;;;;;4805;Epikkmeme;1473621749;;A", -- [104]
						"1;;;;;4805;Epikkmeme;1473621748;;A", -- [105]
						"1;;;;;4805;Epikkmeme;1473621748;;A", -- [106]
						"1;;;;;4805;Epikkmeme;1473621747;;A", -- [107]
						"1;;;;;4805;Epikkmeme;1473621747;;A", -- [108]
						"1;;;;;3112;Splewlock;1473621742;;A", -- [109]
						"1;;;;;3112;Splewlock;1473621739;;A", -- [110]
						"1;;;;;3112;Splewlock;1473621739;;A", -- [111]
						"1;;;;;3112;Splewlock;1473621738;;A", -- [112]
						"1;;;;;3112;Splewlock;1473621736;;A", -- [113]
						"1;;;;;2972;Splewlock;1473621735;;A", -- [114]
						"1;;;;;2972;Splewlock;1473621734;;A", -- [115]
						"1;;;;;2972;Splewlock;1473621734;;A", -- [116]
						"1;;;;;2832;Splewlock;1473621731;;A", -- [117]
						"1;;;;;2832;Splewlock;1473621730;;A", -- [118]
						"1;;;;;2832;Splewlock;1473621730;;A", -- [119]
						"1;;;;;2832;Splewlock;1473621729;;A", -- [120]
						"1;;;;;2832;Splewlock;1473621728;;A", -- [121]
						"1;;;;;2832;Splewlock;1473621727;;A", -- [122]
						"1;;;;;2832;Splewlock;1473621726;;A", -- [123]
						"1;;;;;2832;Splewlock;1473621725;;A", -- [124]
						"1;;;;;2832;Splewlock;1473621724;;A", -- [125]
						"1;;;;;2832;Splewlock;1473621724;;A", -- [126]
						"1;;;;;2832;Splewlock;1473621722;;A", -- [127]
						"1;;;;;2832;Splewlock;1473621722;;A", -- [128]
						"1;;;;;2832;Splewlock;1473621721;;A", -- [129]
						"1;;;;;2832;Splewlock;1473621720;;A", -- [130]
						"1;;;;;2832;Splewlock;1473621720;;A", -- [131]
						"1;;;;;2832;Splewlock;1473621719;;A", -- [132]
						"1;;;;;2832;Splewlock;1473621718;;A", -- [133]
						"1;;;;;2832;Splewlock;1473621717;;A", -- [134]
						"1;;;;;2832;Splewlock;1473621716;;A", -- [135]
						"1;;;;;2832;Splewlock;1473621715;;A", -- [136]
						"1;;;;;2832;Splewlock;1473621715;;A", -- [137]
						"1;;;;;2832;Splewlock;1473621714;;A", -- [138]
						"1;;;;;2832;Splewlock;1473621714;;A", -- [139]
						"1;;;;;2832;Splewlock;1473621711;;A", -- [140]
						"1;;;;;2832;Splewlock;1473621710;;A", -- [141]
						"1;;;;;73262;Splewlock;1473621961;;A", -- [142]
						"1;;;;;100325;Chakadeath;1473621770;;A", -- [143]
						"1;;;;;4805;Epikkmeme;1473621767;;A", -- [144]
						"1;;;;;4805;Epikkmeme;1473621766;;A", -- [145]
						"1;;;;;4805;Epikkmeme;1473621765;;A", -- [146]
						"1;;;;;4805;Epikkmeme;1473621764;;A", -- [147]
						"1;;;;;4805;Epikkmeme;1473621764;;A", -- [148]
						"1;;;;;4805;Epikkmeme;1473621762;;A", -- [149]
						"1;;;;;4805;Epikkmeme;1473621761;;A", -- [150]
						"1;;;;;4805;Epikkmeme;1473621760;;A", -- [151]
						"1;;;;;4805;Epikkmeme;1473621759;;A", -- [152]
						"1;;;;;4805;Epikkmeme;1473621759;;A", -- [153]
						"1;;;;;4805;Epikkmeme;1473621758;;A", -- [154]
						"1;;;;;4805;Epikkmeme;1473621758;;A", -- [155]
						"1;;;;;4805;Epikkmeme;1473621757;;A", -- [156]
						"1;;;;;4805;Epikkmeme;1473621757;;A", -- [157]
						"1;;;;;4805;Epikkmeme;1473621756;;A", -- [158]
						"1;;;;;4805;Epikkmeme;1473621756;;A", -- [159]
						"1;;;;;4805;Epikkmeme;1473621755;;A", -- [160]
						"1;;;;;4805;Epikkmeme;1473621754;;A", -- [161]
						"1;;;;;2582;Splewlock;1473622032;;A", -- [162]
						"1;;;;;4805;Epikkmeme;1473620194;;A", -- [163]
						"1;;;;;4805;Epikkmeme;1473620189;;A", -- [164]
						"20;;;;;137800;Angelolight;1473619006;;A", -- [165]
						"20;;;;;137800;Angelolight;1473619004;;A", -- [166]
						"20;;;;;66700;Angelolight;1473618283;;A", -- [167]
						"20;;;;;60600;Angelolight;1473618147;;A", -- [168]
						"1;;;;;4375;Epikkmeme;1473614693;;A", -- [169]
						"1;;;;;4375;Epikkmeme;1473614689;;A", -- [170]
						"1;;;;;4375;Epikkmeme;1473614686;;A", -- [171]
						"1;;;;;4375;Epikkmeme;1473614682;;A", -- [172]
						"1;;;;;4375;Epikkmeme;1473614680;;A", -- [173]
						"1;;;;;4375;Epikkmeme;1473614677;;A", -- [174]
						"1;;;;;4375;Epikkmeme;1473614674;;A", -- [175]
						"1;;;;;4375;Epikkmeme;1473614669;;A", -- [176]
						"1;;;;;4375;Epikkmeme;1473614802;;A", -- [177]
						"1;;;;;4375;Epikkmeme;1473614799;;A", -- [178]
						"1;;;;;4375;Epikkmeme;1473614794;;A", -- [179]
						"1;;;;;4375;Epikkmeme;1473614790;;A", -- [180]
						"1;;;;;4375;Epikkmeme;1473614788;;A", -- [181]
						"1;;;;;4375;Epikkmeme;1473614786;;A", -- [182]
						"1;;;;;4375;Epikkmeme;1473614780;;A", -- [183]
						"1;;;;;4375;Epikkmeme;1473614777;;A", -- [184]
						"1;;;;;4375;Epikkmeme;1473614774;;A", -- [185]
						"1;;;;;4375;Epikkmeme;1473614772;;A", -- [186]
						"1;;;;;4375;Epikkmeme;1473614767;;A", -- [187]
						"1;;;;;4375;Epikkmeme;1473614764;;A", -- [188]
						"1;;;;;4375;Epikkmeme;1473614761;;A", -- [189]
						"1;;;;;4375;Epikkmeme;1473614760;;A", -- [190]
						"1;;;;;4375;Epikkmeme;1473614757;;A", -- [191]
						"1;;;;;4375;Epikkmeme;1473614753;;A", -- [192]
						"1;;;;;4375;Epikkmeme;1473614749;;A", -- [193]
						"1;;;;;4375;Epikkmeme;1473614746;;A", -- [194]
						"1;;;;;4375;Epikkmeme;1473614736;;A", -- [195]
						"1;;;;;4375;Epikkmeme;1473614734;;A", -- [196]
						"1;;;;;4375;Epikkmeme;1473614730;;A", -- [197]
						"1;;;;;4375;Epikkmeme;1473614727;;A", -- [198]
						"1;;;;;4375;Epikkmeme;1473614725;;A", -- [199]
						"1;;;;;4375;Epikkmeme;1473614719;;A", -- [200]
						"1;;;;;4375;Epikkmeme;1473614717;;A", -- [201]
						"1;;;;;4375;Epikkmeme;1473614714;;A", -- [202]
						"1;;;;;4375;Epikkmeme;1473614709;;A", -- [203]
						"1;;;;;4375;Epikkmeme;1473614707;;A", -- [204]
						"1;;;;;4375;Epikkmeme;1473614704;;A", -- [205]
						"1;;;;;4375;Epikkmeme;1473614702;;A", -- [206]
						"1;;;;;4375;Epikkmeme;1473614699;;A", -- [207]
						"1;;;;;4375;Epikkmeme;1473614696;;A", -- [208]
						"1;;;;;2582;Splewlock;1473614950;;A", -- [209]
						"1;;;;;2582;Splewlock;1473614946;;A", -- [210]
						"1;;;;;2582;Splewlock;1473614942;;A", -- [211]
						"1;;;;;2582;Splewlock;1473614939;;A", -- [212]
						"1;;;;;2582;Splewlock;1473614936;;A", -- [213]
						"1;;;;;2582;Splewlock;1473614925;;A", -- [214]
						"1;;;;;2582;Splewlock;1473614923;;A", -- [215]
						"1;;;;;2582;Splewlock;1473614920;;A", -- [216]
						"1;;;;;2582;Splewlock;1473614918;;A", -- [217]
						"1;;;;;2582;Splewlock;1473614913;;A", -- [218]
						"1;;;;;2582;Splewlock;1473614910;;A", -- [219]
						"1;;;;;2582;Splewlock;1473614908;;A", -- [220]
						"1;;;;;2582;Splewlock;1473614907;;A", -- [221]
						"1;;;;;2582;Splewlock;1473614904;;A", -- [222]
						"1;;;;;2582;Splewlock;1473614900;;A", -- [223]
						"1;;;;;2582;Splewlock;1473614898;;A", -- [224]
						"1;;;;;2582;Splewlock;1473614893;;A", -- [225]
						"1;;;;;4375;Epikkmeme;1473614818;;A", -- [226]
						"1;;;;;4375;Epikkmeme;1473614814;;A", -- [227]
						"1;;;;;4375;Epikkmeme;1473614811;;A", -- [228]
						"1;;;;;4375;Epikkmeme;1473614808;;A", -- [229]
						"1;;;;;4375;Epikkmeme;1473614806;;A", -- [230]
						"1;;;;;2582;Splewlock;1473614981;;A", -- [231]
						"1;;;;;2582;Splewlock;1473614978;;A", -- [232]
						"1;;;;;2582;Splewlock;1473614973;;A", -- [233]
						"1;;;;;2582;Splewlock;1473614968;;A", -- [234]
						"1;;;;;2582;Splewlock;1473614966;;A", -- [235]
						"1;;;;;2582;Splewlock;1473614962;;A", -- [236]
						"1;;;;;2582;Splewlock;1473614958;;A", -- [237]
						"1;;;;;2582;Splewlock;1473614956;;A", -- [238]
						"1;;;;;2582;Splewlock;1473614952;;A", -- [239]
						"1;;;;;2582;Splewlock;1473615121;;A", -- [240]
						"1;;;;;2582;Splewlock;1473615120;;A", -- [241]
						"1;;;;;2582;Splewlock;1473615117;;A", -- [242]
						"1;;;;;2582;Splewlock;1473615114;;A", -- [243]
						"1;;;;;2582;Splewlock;1473615110;;A", -- [244]
						"1;;;;;2582;Splewlock;1473615106;;A", -- [245]
						"1;;;;;2582;Splewlock;1473615105;;A", -- [246]
						"1;;;;;2582;Splewlock;1473615103;;A", -- [247]
						"1;;;;;2582;Splewlock;1473615101;;A", -- [248]
						"1;;;;;2582;Splewlock;1473615097;;A", -- [249]
						"1;;;;;2582;Splewlock;1473615094;;A", -- [250]
						"1;;;;;2582;Splewlock;1473615093;;A", -- [251]
						"1;;;;;2582;Splewlock;1473615092;;A", -- [252]
						"1;;;;;2582;Splewlock;1473615085;;A", -- [253]
						"1;;;;;2582;Splewlock;1473615081;;A", -- [254]
						"1;;;;;2582;Splewlock;1473615079;;A", -- [255]
						"1;;;;;2582;Splewlock;1473615076;;A", -- [256]
						"1;;;;;2582;Splewlock;1473615074;;A", -- [257]
						"1;;;;;2582;Splewlock;1473615072;;A", -- [258]
						"1;;;;;2582;Splewlock;1473615068;;A", -- [259]
						"1;;;;;2582;Splewlock;1473615064;;A", -- [260]
						"1;;;;;2582;Splewlock;1473615062;;A", -- [261]
						"1;;;;;2582;Splewlock;1473615060;;A", -- [262]
						"1;;;;;2582;Splewlock;1473615058;;A", -- [263]
						"1;;;;;2582;Splewlock;1473615057;;A", -- [264]
						"1;;;;;2582;Splewlock;1473615053;;A", -- [265]
						"1;;;;;2582;Splewlock;1473615050;;A", -- [266]
						"1;;;;;2582;Splewlock;1473615048;;A", -- [267]
						"1;;;;;2582;Splewlock;1473615043;;A", -- [268]
						"1;;;;;2582;Splewlock;1473615040;;A", -- [269]
						"1;;;;;2582;Splewlock;1473615036;;A", -- [270]
						"1;;;;;2582;Splewlock;1473615032;;A", -- [271]
						"1;;;;;2582;Splewlock;1473615030;;A", -- [272]
						"1;;;;;2582;Splewlock;1473615028;;A", -- [273]
						"1;;;;;2582;Splewlock;1473615024;;A", -- [274]
						"1;;;;;2582;Splewlock;1473615019;;A", -- [275]
						"1;;;;;2582;Splewlock;1473615017;;A", -- [276]
						"1;;;;;2582;Splewlock;1473615015;;A", -- [277]
						"1;;;;;2582;Splewlock;1473615012;;A", -- [278]
						"1;;;;;2582;Splewlock;1473615008;;A", -- [279]
						"1;;;;;2582;Splewlock;1473615005;;A", -- [280]
						"1;;;;;2582;Splewlock;1473615003;;A", -- [281]
						"1;;;;;2582;Splewlock;1473614997;;A", -- [282]
						"1;;;;;2582;Splewlock;1473614996;;A", -- [283]
						"1;;;;;2582;Splewlock;1473614993;;A", -- [284]
						"1;;;;;2582;Splewlock;1473614990;;A", -- [285]
						"1;;;;;2582;Splewlock;1473614988;;A", -- [286]
						"1;;;;;2582;Splewlock;1473614985;;A", -- [287]
						"1;;;;;2582;Splewlock;1473614983;;A", -- [288]
						"1;;;;;2832;Splewlock;1473615201;;A", -- [289]
						"1;;;;;2832;Splewlock;1473615200;;A", -- [290]
						"1;;;;;2832;Splewlock;1473615192;;A", -- [291]
						"1;;;;;2832;Splewlock;1473615189;;A", -- [292]
						"1;;;;;2832;Splewlock;1473615187;;A", -- [293]
						"1;;;;;2702;Splewlock;1473615186;;A", -- [294]
						"1;;;;;2702;Splewlock;1473615184;;A", -- [295]
						"1;;;;;2702;Splewlock;1473615181;;A", -- [296]
						"1;;;;;2702;Splewlock;1473615173;;A", -- [297]
						"1;;;;;2702;Splewlock;1473615172;;A", -- [298]
						"1;;;;;2582;Splewlock;1473615163;;A", -- [299]
						"1;;;;;2582;Splewlock;1473615158;;A", -- [300]
						"1;;;;;2582;Splewlock;1473615157;;A", -- [301]
						"1;;;;;2582;Splewlock;1473615149;;A", -- [302]
						"1;;;;;2582;Splewlock;1473615148;;A", -- [303]
						"1;;;;;2582;Splewlock;1473615145;;A", -- [304]
						"1;;;;;2582;Splewlock;1473615143;;A", -- [305]
						"1;;;;;2582;Splewlock;1473615142;;A", -- [306]
						"1;;;;;2582;Splewlock;1473615139;;A", -- [307]
						"1;;;;;2582;Splewlock;1473615136;;A", -- [308]
						"1;;;;;2582;Splewlock;1473615133;;A", -- [309]
						"1;;;;;2582;Splewlock;1473615130;;A", -- [310]
						"1;;;;;2582;Splewlock;1473615128;;A", -- [311]
						"1;;;;;2582;Splewlock;1473615126;;A", -- [312]
						"1;;;;;2582;Splewlock;1473615124;;A", -- [313]
						"1;;;;;3090;Lizet;1473610363;;A", -- [314]
						"1;;;;;3240;Lizet;1473610362;;A", -- [315]
						"1;;;;;3240;Lizet;1473610362;;A", -- [316]
						"1;;;;;3570;Lizet;1473610361;;A", -- [317]
						"1;;;;;3090;Lizet;1473610355;;A", -- [318]
						"1;;;;;2950;Lizet;1473609970;;A", -- [319]
						"1;;;;;3570;Lizet;1473609969;;A", -- [320]
						"1;;;;;3740;Lizet;1473609969;;A", -- [321]
						"1;;;;;3740;Lizet;1473609969;;A", -- [322]
						"1;;;;;3740;Lizet;1473609968;;A", -- [323]
						"1;;;;;3740;Lizet;1473609968;;A", -- [324]
						"1;;;;;3740;Lizet;1473609967;;A", -- [325]
						"1;;;;;3740;Lizet;1473609967;;A", -- [326]
						"1;;;;;3740;Lizet;1473609967;;A", -- [327]
						"1;;;;;3740;Lizet;1473609966;;A", -- [328]
						"1;;;;;3920;Lizet;1473609965;;A", -- [329]
						"1;;;;;3740;Lizet;1473609965;;A", -- [330]
						"1;;;;;3195;Lukeno;1473609200;;A", -- [331]
						"1;;;;;2905;Lukeno;1473607353;;A", -- [332]
						"1;;;;;3570;Lizet;1473605667;;A", -- [333]
						"1;;;;;3740;Lizet;1473605663;;A", -- [334]
						"1;;;;;3570;Lizet;1473605659;;A", -- [335]
						"1;;;;;3400;Lizet;1473605657;;A", -- [336]
						"1;;;;;3400;Lizet;1473605654;;A", -- [337]
						"1;;;;;3570;Lizet;1473605652;;A", -- [338]
						"1;;;;;3400;Lizet;1473605647;;A", -- [339]
						"1;;;;;3400;Lizet;1473605642;;A", -- [340]
						"1;;;;;3400;Lizet;1473605639;;A", -- [341]
						"1;;;;;3400;Lizet;1473605635;;A", -- [342]
						"1;;;;;3240;Lizet;1473605630;;A", -- [343]
						"1;;;;;3400;Lizet;1473605627;;A", -- [344]
						"1;;;;;3400;Lizet;1473605624;;A", -- [345]
						"1;;;;;3400;Lizet;1473605622;;A", -- [346]
						"1;;;;;3400;Lizet;1473605618;;A", -- [347]
						"1;;;;;3400;Lizet;1473605615;;A", -- [348]
						"1;;;;;3400;Lizet;1473605609;;A", -- [349]
						"1;;;;;3400;Lizet;1473605604;;A", -- [350]
						"1;;;;;3400;Lizet;1473605601;;A", -- [351]
						"1;;;;;3240;Lizet;1473605597;;A", -- [352]
						"1;;;;;3400;Lizet;1473605594;;A", -- [353]
						"1;;;;;3400;Lizet;1473605591;;A", -- [354]
						"1;;;;;3240;Lizet;1473605588;;A", -- [355]
						"1;;;;;3240;Lizet;1473605583;;A", -- [356]
						"1;;;;;3240;Lizet;1473605580;;A", -- [357]
						"1;;;;;3240;Lizet;1473605577;;A", -- [358]
						"1;;;;;3240;Lizet;1473605564;;A", -- [359]
						"1;;;;;3240;Lizet;1473605559;;A", -- [360]
						"1;;;;;3240;Lizet;1473605555;;A", -- [361]
						"1;;;;;3240;Lizet;1473605539;;A", -- [362]
						"1;;;;;3240;Lizet;1473605524;;A", -- [363]
						"1;;;;;3240;Lizet;1473605520;;A", -- [364]
						"1;;;;;3240;Lizet;1473605513;;A", -- [365]
						"1;;;;;3240;Lizet;1473605510;;A", -- [366]
						"1;;;;;3240;Lizet;1473605506;;A", -- [367]
						"1;;;;;3240;Lizet;1473605500;;A", -- [368]
						"1;;;;;3240;Lizet;1473605495;;A", -- [369]
						"1;;;;;4375;Epikkmeme;1473606848;;A", -- [370]
						"1;;;;;4375;Epikkmeme;1473606845;;A", -- [371]
						"1;;;;;4375;Epikkmeme;1473606840;;A", -- [372]
						"1;;;;;4375;Epikkmeme;1473606834;;A", -- [373]
						"1;;;;;3195;Lukeno;1473606829;;A", -- [374]
						"1;;;;;102905;Lukeno;1473606825;;A", -- [375]
						"1;;;;;102595;Lukeno;1473606822;;A", -- [376]
						"1;;;;;3195;Lukeno;1473606819;;A", -- [377]
						"1;;;;;3195;Lukeno;1473606817;;A", -- [378]
						"1;;;;;3195;Lukeno;1473606814;;A", -- [379]
						"1;;;;;2905;Lukeno;1473606810;;A", -- [380]
						"1;;;;;3195;Lukeno;1473606807;;A", -- [381]
						"1;;;;;3195;Lukeno;1473606804;;A", -- [382]
						"1;;;;;2905;Lukeno;1473606802;;A", -- [383]
						"1;;;;;88505;Lukeno;1473606718;;A", -- [384]
						"1;;;;;3195;Lukeno;1473606714;;A", -- [385]
						"1;;;;;3195;Lukeno;1473606714;;A", -- [386]
						"1;;;;;3195;Lukeno;1473606713;;A", -- [387]
						"1;;;;;3195;Lukeno;1473606713;;A", -- [388]
						"1;;;;;3195;Lukeno;1473606712;;A", -- [389]
						"1;;;;;3090;Lizet;1473606131;;A", -- [390]
						"1;;;;;3090;Lizet;1473606128;;A", -- [391]
						"1;;;;;3090;Lizet;1473606124;;A", -- [392]
						"1;;;;;2950;Lizet;1473596562;;A", -- [393]
						"1;;;;;3240;Lizet;1473596754;;A", -- [394]
						"1;;;;;80305;Lukeno;1473596713;;A", -- [395]
						"1;;;;;93195;Lukeno;1473596711;;A", -- [396]
						"1;;;;;2905;Lukeno;1473596705;;A", -- [397]
						"1;;;;;2905;Lukeno;1473596703;;A", -- [398]
						"1;;;;;2905;Lukeno;1473596701;;A", -- [399]
						"1;;;;;2905;Lukeno;1473596699;;A", -- [400]
						"1;;;;;2905;Lukeno;1473596695;;A", -- [401]
						"1;;;;;2905;Lukeno;1473596692;;A", -- [402]
						"1;;;;;2905;Lukeno;1473596690;;A", -- [403]
						"1;;;;;2905;Lukeno;1473596688;;A", -- [404]
						"1;;;;;2905;Lukeno;1473596685;;A", -- [405]
						"1;;;;;2905;Lukeno;1473596682;;A", -- [406]
						"1;;;;;2905;Lukeno;1473596680;;A", -- [407]
						"1;;;;;2905;Lukeno;1473596676;;A", -- [408]
						"1;;;;;3570;Lizet;1473596664;;A", -- [409]
						"1;;;;;3400;Lizet;1473596661;;A", -- [410]
						"1;;;;;3400;Lizet;1473596660;;A", -- [411]
						"1;;;;;3400;Lizet;1473596658;;A", -- [412]
						"1;;;;;3400;Lizet;1473596655;;A", -- [413]
						"1;;;;;3400;Lizet;1473596653;;A", -- [414]
						"1;;;;;3400;Lizet;1473596651;;A", -- [415]
						"1;;;;;3400;Lizet;1473596648;;A", -- [416]
						"1;;;;;3400;Lizet;1473596646;;A", -- [417]
						"1;;;;;3240;Lizet;1473596645;;A", -- [418]
						"1;;;;;3400;Lizet;1473596643;;A", -- [419]
						"1;;;;;3400;Lizet;1473596641;;A", -- [420]
						"1;;;;;3240;Lizet;1473596638;;A", -- [421]
						"1;;;;;3090;Lizet;1473596633;;A", -- [422]
						"1;;;;;3240;Lizet;1473596631;;A", -- [423]
						"1;;;;;3240;Lizet;1473596630;;A", -- [424]
						"1;;;;;3090;Lizet;1473596628;;A", -- [425]
						"1;;;;;3090;Lizet;1473596627;;A", -- [426]
						"1;;;;;3090;Lizet;1473596619;;A", -- [427]
						"1;;;;;3090;Lizet;1473596618;;A", -- [428]
						"1;;;;;3090;Lizet;1473596616;;A", -- [429]
						"1;;;;;3090;Lizet;1473596614;;A", -- [430]
						"1;;;;;3090;Lizet;1473596613;;A", -- [431]
						"1;;;;;3090;Lizet;1473596611;;A", -- [432]
						"1;;;;;3090;Lizet;1473596608;;A", -- [433]
						"1;;;;;3090;Lizet;1473596607;;A", -- [434]
						"1;;;;;3090;Lizet;1473596601;;A", -- [435]
						"1;;;;;3090;Lizet;1473596599;;A", -- [436]
						"1;;;;;2950;Lizet;1473596589;;A", -- [437]
						"1;;;;;2950;Lizet;1473596588;;A", -- [438]
						"1;;;;;2950;Lizet;1473596580;;A", -- [439]
						"1;;;;;2950;Lizet;1473596574;;A", -- [440]
						"1;;;;;2950;Lizet;1473596567;;A", -- [441]
						"1;;;;;92905;Lukeno;1473582911;;A", -- [442]
						"1;;;;;92905;Lukeno;1473582911;;A", -- [443]
						"1;;;;;92905;Lukeno;1473582910;;A", -- [444]
						"1;;;;;92905;Lukeno;1473582909;;A", -- [445]
						"1;;;;;92905;Lukeno;1473582908;;A", -- [446]
						"1;;;;;92905;Lukeno;1473582907;;A", -- [447]
						"1;;;;;92905;Lukeno;1473582905;;A", -- [448]
						"1;;;;;92905;Lukeno;1473582905;;A", -- [449]
						"1;;;;;3090;Lizet;1473593632;;A", -- [450]
						"1;;;;;3090;Lizet;1473593632;;A", -- [451]
						"1;;;;;3090;Lizet;1473593631;;A", -- [452]
						"1;;;;;3090;Lizet;1473593631;;A", -- [453]
						"1;;;;;3240;Lizet;1473593630;;A", -- [454]
						"1;;;;;2905;Lukeno;1473565635;;A", -- [455]
						"1;;;;;2905;Lukeno;1473565632;;A", -- [456]
						"1;;;;;1155;Cimby;1473543728;;A", -- [457]
						"1;;;;;1155;Cimby;1473543724;;A", -- [458]
						"1;;;;;1100;Pocketpímp;1473543686;;A", -- [459]
						"1;;;;;1100;Pocketpímp;1473543683;;A", -- [460]
						"1;;;;;1100;Pocketpímp;1473543677;;A", -- [461]
						"1;;;;;1100;Pocketpímp;1473543662;;A", -- [462]
						"1;;;;;1100;Pocketpímp;1473543656;;A", -- [463]
						"1;;;;;1100;Pocketpímp;1473543646;;A", -- [464]
						"1;;;;;1100;Pocketpímp;1473543639;;A", -- [465]
						"1;;;;;2645;Lukeno;1473539673;;A", -- [466]
						"1;;;;;2645;Lukeno;1473539667;;A", -- [467]
						"1;;;;;2645;Lukeno;1473539664;;A", -- [468]
						"1;;;;;2645;Lukeno;1473539661;;A", -- [469]
						"1;;;;;2645;Lukeno;1473539651;;A", -- [470]
						"1;;;;;2645;Lukeno;1473539649;;A", -- [471]
						"1;;;;;2645;Lukeno;1473539646;;A", -- [472]
						"1;;;;;2645;Lukeno;1473539641;;A", -- [473]
						"1;;;;;2905;Lukeno;1473539639;;A", -- [474]
						"1;;;;;2905;Lukeno;1473539635;;A", -- [475]
						"1;;;;;1155;Cimby;1473543770;;A", -- [476]
						"1;;;;;1155;Cimby;1473543766;;A", -- [477]
						"1;;;;;1155;Cimby;1473543762;;A", -- [478]
						"1;;;;;1155;Cimby;1473543758;;A", -- [479]
						"1;;;;;1155;Cimby;1473543755;;A", -- [480]
						"1;;;;;1155;Cimby;1473543753;;A", -- [481]
						"1;;;;;1155;Pocketpímp;1473543749;;A", -- [482]
						"1;;;;;1155;Pocketpímp;1473543743;;A", -- [483]
						"1;;;;;1155;Pocketpímp;1473543741;;A", -- [484]
						"1;;;;;1155;Pocketpímp;1473543739;;A", -- [485]
						"1;;;;;1155;Pocketpímp;1473543735;;A", -- [486]
						"1;;;;;2810;Lizet;1473547077;;A", -- [487]
						"1;;;;;2810;Lizet;1473547074;;A", -- [488]
						"1;;;;;2810;Lizet;1473547073;;A", -- [489]
						"1;;;;;2810;Lizet;1473547069;;A", -- [490]
						"1;;;;;2810;Lizet;1473547068;;A", -- [491]
						"1;;;;;2810;Lizet;1473547065;;A", -- [492]
						"1;;;;;1270;Cimby;1473547015;;A", -- [493]
						"1;;;;;1270;Cimby;1473547009;;A", -- [494]
						"1;;;;;1270;Cimby;1473547005;;A", -- [495]
						"1;;;;;1155;Pocketpímp;1473546996;;A", -- [496]
						"1;;;;;1155;Pocketpímp;1473546989;;A", -- [497]
						"1;;;;;1155;Pocketpímp;1473546985;;A", -- [498]
						"1;;;;;1155;Pocketpímp;1473546983;;A", -- [499]
						"1;;;;;1155;Pocketpímp;1473546981;;A", -- [500]
						"1;;;;;1155;Pocketpímp;1473546979;;A", -- [501]
						"1;;;;;1155;Pocketpímp;1473546977;;A", -- [502]
						"1;;;;;1270;Cimby;1473546977;;A", -- [503]
						"1;;;;;100000;Lizet;1473554353;;A", -- [504]
						"1;;;;;53090;Lizet;1473554352;;A", -- [505]
						"1;;;;;72905;Lukeno;1473554351;;A", -- [506]
						"1;;;;;2645;Lukeno;1473554030;;A", -- [507]
						"1;;;;;2645;Lukeno;1473554029;;A", -- [508]
						"1;;;;;2645;Lukeno;1473554023;;A", -- [509]
						"1;;;;;2645;Lukeno;1473554019;;A", -- [510]
						"1;;;;;2645;Lukeno;1473554018;;A", -- [511]
						"1;;;;;2645;Lukeno;1473554015;;A", -- [512]
						"1;;;;;2645;Lukeno;1473554014;;A", -- [513]
						"1;;;;;2645;Lukeno;1473554013;;A", -- [514]
						"1;;;;;2645;Lukeno;1473554012;;A", -- [515]
						"1;;;;;2645;Lukeno;1473554010;;A", -- [516]
						"1;;;;;2645;Lukeno;1473554010;;A", -- [517]
						"1;;;;;2645;Lukeno;1473554007;;A", -- [518]
						"1;;;;;1530;Lukeno;1473554006;;A", -- [519]
						"1;;;;;1530;Lukeno;1473554005;;A", -- [520]
						"1;;;;;1530;Lukeno;1473554004;;A", -- [521]
						"1;;;;;1530;Lukeno;1473554004;;A", -- [522]
						"1;;;;;1530;Lukeno;1473554001;;A", -- [523]
						"1;;;;;1460;Lukeno;1473553998;;A", -- [524]
						"1;;;;;1460;Lukeno;1473553978;;A", -- [525]
						"1;;;;;1460;Lukeno;1473553978;;A", -- [526]
						"1;;;;;2645;Lukeno;1473553977;;A", -- [527]
						"1;;;;;2645;Lukeno;1473553976;;A", -- [528]
						"1;;;;;1605;Lukeno;1473553974;;A", -- [529]
						"1;;;;;1530;Lukeno;1473553974;;A", -- [530]
						"1;;;;;2645;Lukeno;1473553973;;A", -- [531]
						"1;;;;;1605;Lukeno;1473553973;;A", -- [532]
						"1;;;;;1605;Lukeno;1473553971;;A", -- [533]
						"1;;;;;1460;Lukeno;1473553966;;A", -- [534]
						"1;;;;;1460;Lukeno;1473553966;;A", -- [535]
						"1;;;;;1460;Lukeno;1473553965;;A", -- [536]
						"1;;;;;1460;Lukeno;1473553963;;A", -- [537]
						"1;;;;;1460;Lukeno;1473553962;;A", -- [538]
						"1;;;;;1460;Lukeno;1473553961;;A", -- [539]
						"1;;;;;1460;Lukeno;1473553960;;A", -- [540]
						"1;;;;;2645;Lukeno;1473554248;;A", -- [541]
						"1;;;;;2645;Lukeno;1473554247;;A", -- [542]
						"1;;;;;2645;Lukeno;1473554246;;A", -- [543]
						"1;;;;;2645;Lukeno;1473554246;;A", -- [544]
						"1;;;;;2645;Lukeno;1473554246;;A", -- [545]
						"1;;;;;2645;Lukeno;1473554244;;A", -- [546]
						"1;;;;;2645;Lukeno;1473554243;;A", -- [547]
						"1;;;;;2645;Lukeno;1473554243;;A", -- [548]
						"1;;;;;2645;Lukeno;1473554242;;A", -- [549]
						"1;;;;;2645;Lukeno;1473554241;;A", -- [550]
						"1;;;;;2645;Lukeno;1473554240;;A", -- [551]
						"1;;;;;2645;Lukeno;1473554239;;A", -- [552]
						"1;;;;;2645;Lukeno;1473554237;;A", -- [553]
						"1;;;;;2810;Lizet;1473554230;;A", -- [554]
						"1;;;;;2810;Lizet;1473554229;;A", -- [555]
						"1;;;;;2810;Lizet;1473554228;;A", -- [556]
						"1;;;;;2810;Lizet;1473554226;;A", -- [557]
						"1;;;;;2810;Lizet;1473554225;;A", -- [558]
						"1;;;;;2810;Lizet;1473554223;;A", -- [559]
						"1;;;;;2810;Lizet;1473554222;;A", -- [560]
						"1;;;;;2810;Lizet;1473554221;;A", -- [561]
						"1;;;;;2810;Lizet;1473554220;;A", -- [562]
						"1;;;;;2810;Lizet;1473554219;;A", -- [563]
						"1;;;;;2680;Lizet;1473554054;;A", -- [564]
						"1;;;;;2680;Lizet;1473554052;;A", -- [565]
						"1;;;;;2680;Lizet;1473554051;;A", -- [566]
						"1;;;;;2680;Lizet;1473554050;;A", -- [567]
						"1;;;;;2680;Lizet;1473554049;;A", -- [568]
						"1;;;;;2680;Lizet;1473554047;;A", -- [569]
						"1;;;;;2680;Lizet;1473554047;;A", -- [570]
						"1;;;;;2680;Lizet;1473554046;;A", -- [571]
						"1;;;;;2680;Lizet;1473554045;;A", -- [572]
						"1;;;;;2680;Lizet;1473554044;;A", -- [573]
						"1;;;;;2645;Lukeno;1473554043;;A", -- [574]
						"1;;;;;2645;Lukeno;1473554042;;A", -- [575]
						"1;;;;;2645;Lukeno;1473554042;;A", -- [576]
						"1;;;;;2645;Lukeno;1473554041;;A", -- [577]
						"1;;;;;2645;Lukeno;1473554040;;A", -- [578]
						"1;;;;;2645;Lukeno;1473554040;;A", -- [579]
						"1;;;;;2645;Lukeno;1473554039;;A", -- [580]
						"1;;;;;2645;Lukeno;1473554037;;A", -- [581]
						"1;;;;;2645;Lukeno;1473554035;;A", -- [582]
						"1;;;;;2645;Lukeno;1473554034;;A", -- [583]
						"1;;;;;2645;Lukeno;1473554031;;A", -- [584]
						"1;;;;;2645;Lukeno;1473554031;;A", -- [585]
						"1;;;;;2645;Lukeno;1473554030;;A", -- [586]
						"1;;;;;2905;Lukeno;1473554158;;A", -- [587]
						"1;;;;;2810;Lizet;1473554151;;A", -- [588]
						"1;;;;;2810;Lizet;1473554149;;A", -- [589]
						"1;;;;;2810;Lizet;1473554107;;A", -- [590]
						"1;;;;;2810;Lizet;1473554105;;A", -- [591]
						"1;;;;;2810;Lizet;1473554105;;A", -- [592]
						"1;;;;;2810;Lizet;1473554104;;A", -- [593]
						"1;;;;;2810;Lizet;1473554102;;A", -- [594]
						"1;;;;;2810;Lizet;1473554101;;A", -- [595]
						"1;;;;;2810;Lizet;1473554100;;A", -- [596]
						"1;;;;;2810;Lizet;1473554099;;A", -- [597]
						"1;;;;;2810;Lizet;1473554099;;A", -- [598]
						"1;;;;;2810;Lizet;1473554098;;A", -- [599]
						"1;;;;;2810;Lizet;1473554097;;A", -- [600]
						"1;;;;;2810;Lizet;1473554096;;A", -- [601]
						"1;;;;;2810;Lizet;1473554095;;A", -- [602]
						"1;;;;;2810;Lizet;1473554094;;A", -- [603]
						"1;;;;;2810;Lizet;1473554091;;A", -- [604]
						"1;;;;;2810;Lizet;1473554090;;A", -- [605]
						"1;;;;;2810;Lizet;1473554089;;A", -- [606]
						"1;;;;;2810;Lizet;1473554088;;A", -- [607]
						"1;;;;;2810;Lizet;1473554087;;A", -- [608]
						"1;;;;;2810;Lizet;1473554086;;A", -- [609]
						"1;;;;;2810;Lizet;1473554086;;A", -- [610]
						"1;;;;;2810;Lizet;1473554085;;A", -- [611]
						"1;;;;;2810;Lizet;1473554084;;A", -- [612]
						"1;;;;;2810;Lizet;1473554083;;A", -- [613]
						"1;;;;;2810;Lizet;1473554082;;A", -- [614]
						"1;;;;;2810;Lizet;1473554080;;A", -- [615]
						"1;;;;;2680;Lizet;1473554074;;A", -- [616]
						"1;;;;;2680;Lizet;1473554073;;A", -- [617]
						"1;;;;;2680;Lizet;1473554072;;A", -- [618]
						"1;;;;;2680;Lizet;1473554070;;A", -- [619]
						"1;;;;;2680;Lizet;1473554070;;A", -- [620]
						"1;;;;;2680;Lizet;1473554069;;A", -- [621]
						"1;;;;;2680;Lizet;1473554068;;A", -- [622]
						"1;;;;;2680;Lizet;1473554067;;A", -- [623]
						"1;;;;;2680;Lizet;1473554065;;A", -- [624]
						"1;;;;;2680;Lizet;1473554063;;A", -- [625]
						"1;;;;;2680;Lizet;1473554057;;A", -- [626]
						"1;;;;;2680;Lizet;1473554056;;A", -- [627]
						"1;;;;;2680;Lizet;1473554056;;A", -- [628]
						"1;;;;;2680;Lizet;1473554055;;A", -- [629]
						"1;;;;;2680;Lizet;1473554054;;A", -- [630]
						"1;;;;;1270;Cimby;1473548750;;A", -- [631]
						"1;;;;;1270;Cimby;1473548745;;A", -- [632]
						"1;;;;;1270;Cimby;1473548742;;A", -- [633]
						"1;;;;;1270;Cimby;1473548740;;A", -- [634]
						"1;;;;;1270;Cimby;1473548735;;A", -- [635]
						"1;;;;;1270;Cimby;1473548730;;A", -- [636]
						"1;;;;;1270;Cimby;1473548727;;A", -- [637]
						"1;;;;;1270;Cimby;1473548725;;A", -- [638]
						"1;;;;;1270;Cimby;1473548721;;A", -- [639]
						"1;;;;;1270;Cimby;1473548715;;A", -- [640]
						"1;;;;;1270;Cimby;1473548710;;A", -- [641]
						"1;;;;;1270;Cimby;1473548707;;A", -- [642]
						"1;;;;;1270;Cimby;1473548704;;A", -- [643]
						"1;;;;;1270;Cimby;1473548698;;A", -- [644]
						"1;;;;;1270;Cimby;1473548694;;A", -- [645]
						"1;;;;;1270;Cimby;1473548690;;A", -- [646]
						"1;;;;;1270;Cimby;1473548686;;A", -- [647]
						"1;;;;;1270;Cimby;1473548682;;A", -- [648]
						"1;;;;;1270;Cimby;1473548680;;A", -- [649]
						"1;;;;;1270;Cimby;1473548677;;A", -- [650]
						"1;;;;;2810;Lizet;1473547103;;A", -- [651]
						"1;;;;;1100;Pocketpímp;1473537464;;A", -- [652]
						"1;;;;;1100;Pocketpímp;1473537461;;A", -- [653]
					},
				},
				["123919"] = {
					["item:123919::::::::100:::::::"] = {
						"5;;;;;2124000;Galithrambor;1473726285;;A", -- [1]
						"1;;;;;298310;Iluvpooc;1473724963;;A", -- [2]
						"1;;;;;531820;Iluvpooc;1473723749;;A", -- [3]
						"1;;;;;530820;Iluvpooc;1473723743;;A", -- [4]
						"1;;;;;530120;Iluvpooc;1473723739;;A", -- [5]
						"1;;;;;258820;Iluvpooc;1473723719;;A", -- [6]
						"1;;;;;258820;Iluvpooc;1473723715;;A", -- [7]
						"1;;;;;258820;Iluvpooc;1473723711;;A", -- [8]
						"1;;;;;10240;Iluvpooc;1473723698;;A", -- [9]
						"1;;;;;425820;Iluvpooc;1473723284;;A", -- [10]
						"1;;;;;258820;Iluvpooc;1473723278;;A", -- [11]
						"1;;;;;246820;Iluvpooc;1473723272;;A", -- [12]
						"1;;;;;246820;Iluvpooc;1473723268;;A", -- [13]
						"1;;;;;246820;Iluvpooc;1473723261;;A", -- [14]
						"1;;;;;236310;Iluvpooc;1473723237;;A", -- [15]
						"1;;;;;204820;Iluvpooc;1473723230;;A", -- [16]
						"1;;;;;177510;Iluvpooc;1473723225;;A", -- [17]
						"1;;;;;177120;Iluvpooc;1473723219;;A", -- [18]
						"1;;;;;271310;Iluvpooc;1473723191;;A", -- [19]
						"1;;;;;177120;Iluvpooc;1473720956;;A", -- [20]
						"1;;;;;177120;Iluvpooc;1473720956;;A", -- [21]
						"1;;;;;235820;Iluvpooc;1473722598;;A", -- [22]
						"1;;;;;214820;Iluvpooc;1473722478;;A", -- [23]
						"1;;;;;195120;Iluvpooc;1473722471;;A", -- [24]
						"1;;;;;247310;Iluvpooc;1473722466;;A", -- [25]
						"1;;;;;424820;Iluvpooc;1473722439;;A", -- [26]
						"1;;;;;235820;Iluvpooc;1473722386;;A", -- [27]
						"1;;;;;160720;Iluvpooc;1473722382;;A", -- [28]
						"1;;;;;235820;Iluvpooc;1473722381;;A", -- [29]
						"1;;;;;215310;Iluvpooc;1473722378;;A", -- [30]
						"1;;;;;185920;Iluvpooc;1473722377;;A", -- [31]
						"1;;;;;224820;Iluvpooc;1473722375;;A", -- [32]
						"1;;;;;225310;Iluvpooc;1473722373;;A", -- [33]
						"1;;;;;224820;Iluvpooc;1473722372;;A", -- [34]
						"1;;;;;235820;Iluvpooc;1473722370;;A", -- [35]
						"1;;;;;224820;Iluvpooc;1473722366;;A", -- [36]
						"1;;;;;161110;Iluvpooc;1473722365;;A", -- [37]
						"1;;;;;235820;Iluvpooc;1473722334;;A", -- [38]
						"1;;;;;185920;Iluvpooc;1473722326;;A", -- [39]
						"1;;;;;214820;Iluvpooc;1473722312;;A", -- [40]
						"1;;;;;224820;Iluvpooc;1473722295;;A", -- [41]
						"1;;;;;204820;Iluvpooc;1473722289;;A", -- [42]
						"1;;;;;204820;Iluvpooc;1473722288;;A", -- [43]
						"1;;;;;205310;Iluvpooc;1473722287;;A", -- [44]
						"1;;;;;204820;Iluvpooc;1473722283;;A", -- [45]
						"1;;;;;214820;Iluvpooc;1473722282;;A", -- [46]
						"1;;;;;214820;Iluvpooc;1473722279;;A", -- [47]
						"1;;;;;235820;Iluvpooc;1473722278;;A", -- [48]
						"1;;;;;168720;Iluvpooc;1473722277;;A", -- [49]
						"1;;;;;214820;Iluvpooc;1473722274;;A", -- [50]
						"1;;;;;195610;Iluvpooc;1473722274;;A", -- [51]
						"1;;;;;214820;Iluvpooc;1473722273;;A", -- [52]
						"1;;;;;204820;Iluvpooc;1473722272;;A", -- [53]
						"1;;;;;214820;Iluvpooc;1473722270;;A", -- [54]
						"1;;;;;185920;Iluvpooc;1473722269;;A", -- [55]
						"1;;;;;195120;Iluvpooc;1473722268;;A", -- [56]
						"1;;;;;195120;Iluvpooc;1473722266;;A", -- [57]
						"1;;;;;186310;Iluvpooc;1473722264;;A", -- [58]
						"1;;;;;185920;Iluvpooc;1473722263;;A", -- [59]
						"1;;;;;177120;Iluvpooc;1473722262;;A", -- [60]
						"1;;;;;177510;Iluvpooc;1473722260;;A", -- [61]
						"1;;;;;168720;Iluvpooc;1473722259;;A", -- [62]
						"1;;;;;168720;Iluvpooc;1473721861;;A", -- [63]
						"1;;;;;168720;Iluvpooc;1473721845;;A", -- [64]
						"1;;;;;169110;Iluvpooc;1473721843;;A", -- [65]
						"1;;;;;161110;Iluvpooc;1473721839;;A", -- [66]
						"1;;;;;8040;Iluvpooc;1473721713;;A", -- [67]
						"1;;;;;185920;Iluvpooc;1473720964;;A", -- [68]
						"1;;;;;185920;Iluvpooc;1473720962;;A", -- [69]
						"1;;;;;185920;Iluvpooc;1473720959;;A", -- [70]
						"1;;;;;185920;Iluvpooc;1473720958;;A", -- [71]
						"1;;;;;168720;Iluvpooc;1473718873;;A", -- [72]
						"1;;;;;168720;Iluvpooc;1473718872;;A", -- [73]
						"1;;;;;168720;Iluvpooc;1473718870;;A", -- [74]
						"1;;;;;168720;Iluvpooc;1473718866;;A", -- [75]
						"1;;;;;168720;Iluvpooc;1473718864;;A", -- [76]
						"1;;;;;168720;Iluvpooc;1473718861;;A", -- [77]
						"1;;;;;402000;Vàlentìne;1473635257;;A", -- [78]
						"1;;;;;402000;Vàlentìne;1473635253;;A", -- [79]
						"1;;;;;402000;Vàlentìne;1473635241;;A", -- [80]
						"1;;;;;402000;Vàlentìne;1473635225;;A", -- [81]
						"1;;;;;402000;Vàlentìne;1473635222;;A", -- [82]
						"1;;;;;402000;Vàlentìne;1473635214;;A", -- [83]
						"1;;;;;334750;Moonjumper;1473634441;;A", -- [84]
						"1;;;;;334750;Moonjumper;1473634438;;A", -- [85]
						"1;;;;;334750;Moonjumper;1473634436;;A", -- [86]
						"1;;;;;334750;Moonjumper;1473634434;;A", -- [87]
						"1;;;;;334750;Moonjumper;1473634432;;A", -- [88]
						"1;;;;;334750;Moonjumper;1473634429;;A", -- [89]
						"1;;;;;334750;Moonjumper;1473634423;;A", -- [90]
						"1;;;;;334750;Moonjumper;1473634421;;A", -- [91]
						"1;;;;;334750;Moonjumper;1473634419;;A", -- [92]
						"1;;;;;334750;Moonjumper;1473634417;;A", -- [93]
						"1;;;;;334750;Moonjumper;1473634415;;A", -- [94]
						"1;;;;;334750;Moonjumper;1473634411;;A", -- [95]
						"1;;;;;334750;Moonjumper;1473634408;;A", -- [96]
						"1;;;;;334750;Moonjumper;1473634404;;A", -- [97]
						"1;;;;;422000;Vàlentìne;1473635296;;A", -- [98]
						"1;;;;;422000;Vàlentìne;1473635291;;A", -- [99]
						"1;;;;;422000;Vàlentìne;1473635287;;A", -- [100]
						"1;;;;;422000;Vàlentìne;1473635282;;A", -- [101]
						"1;;;;;422000;Vàlentìne;1473635278;;A", -- [102]
						"1;;;;;422000;Vàlentìne;1473635273;;A", -- [103]
						"1;;;;;422000;Vàlentìne;1473635271;;A", -- [104]
						"1;;;;;422000;Vàlentìne;1473635267;;A", -- [105]
						"1;;;;;402000;Vàlentìne;1473635263;;A", -- [106]
						"1;;;;;402000;Vàlentìne;1473635259;;A", -- [107]
						"1;;;;;488000;Vàlentìne;1473637149;;A", -- [108]
						"1;;;;;443000;Vàlentìne;1473637144;;A", -- [109]
						"1;;;;;334750;Moonjumper;1473633520;;A", -- [110]
						"1;;;;;334750;Moonjumper;1473633517;;A", -- [111]
						"1;;;;;334750;Moonjumper;1473633511;;A", -- [112]
						"1;;;;;334750;Moonjumper;1473633509;;A", -- [113]
						"1;;;;;334750;Moonjumper;1473633504;;A", -- [114]
						"1;;;;;334750;Moonjumper;1473633499;;A", -- [115]
						"1;;;;;334750;Moonjumper;1473633496;;A", -- [116]
						"1;;;;;334750;Moonjumper;1473633493;;A", -- [117]
						"1;;;;;365000;Vàlentìne;1473627942;;A", -- [118]
						"1;;;;;365000;Vàlentìne;1473627939;;A", -- [119]
						"1;;;;;365000;Vàlentìne;1473627936;;A", -- [120]
						"1;;;;;317000;Vàlentìne;1473621401;;A", -- [121]
						"1;;;;;317000;Vàlentìne;1473621400;;A", -- [122]
						"1;;;;;317000;Vàlentìne;1473621400;;A", -- [123]
						"1;;;;;104110;Iluvpooc;1473621398;;A", -- [124]
						"1;;;;;103920;Iluvpooc;1473621397;;A", -- [125]
						"1;;;;;103920;Iluvpooc;1473621396;;A", -- [126]
						"1;;;;;103920;Iluvpooc;1473621395;;A", -- [127]
						"1;;;;;103920;Iluvpooc;1473621394;;A", -- [128]
						"1;;;;;103920;Iluvpooc;1473621394;;A", -- [129]
						"1;;;;;103920;Iluvpooc;1473621393;;A", -- [130]
						"1;;;;;103920;Iluvpooc;1473621392;;A", -- [131]
						"1;;;;;103920;Iluvpooc;1473621392;;A", -- [132]
						"1;;;;;103920;Iluvpooc;1473621390;;A", -- [133]
						"1;;;;;103920;Iluvpooc;1473621390;;A", -- [134]
						"1;;;;;4110;Iluvpooc;1473621389;;A", -- [135]
						"28;;;;;258100;Angelolight;1473621389;;A", -- [136]
						"1;;;;;4110;Iluvpooc;1473621388;;A", -- [137]
						"1;;;;;3920;Iluvpooc;1473621387;;A", -- [138]
						"1;;;;;3740;Iluvpooc;1473621386;;A", -- [139]
						"1;;;;;3740;Iluvpooc;1473621385;;A", -- [140]
						"1;;;;;3740;Iluvpooc;1473621383;;A", -- [141]
						"1;;;;;3740;Iluvpooc;1473621383;;A", -- [142]
						"1;;;;;3740;Iluvpooc;1473621380;;A", -- [143]
						"1;;;;;348000;Vàlentìne;1473621514;;A", -- [144]
						"1;;;;;348000;Vàlentìne;1473621513;;A", -- [145]
						"1;;;;;348000;Vàlentìne;1473621512;;A", -- [146]
						"1;;;;;348000;Vàlentìne;1473621511;;A", -- [147]
						"1;;;;;334750;Moonjumper;1473621509;;A", -- [148]
						"1;;;;;334750;Moonjumper;1473621508;;A", -- [149]
						"1;;;;;334750;Moonjumper;1473621508;;A", -- [150]
						"1;;;;;334750;Moonjumper;1473621507;;A", -- [151]
						"1;;;;;334750;Moonjumper;1473621506;;A", -- [152]
						"1;;;;;334750;Moonjumper;1473621504;;A", -- [153]
						"1;;;;;334750;Moonjumper;1473621503;;A", -- [154]
						"1;;;;;334750;Moonjumper;1473621502;;A", -- [155]
						"1;;;;;334750;Moonjumper;1473621501;;A", -- [156]
						"1;;;;;334750;Moonjumper;1473621500;;A", -- [157]
						"1;;;;;334750;Moonjumper;1473621500;;A", -- [158]
						"1;;;;;334750;Moonjumper;1473621499;;A", -- [159]
						"1;;;;;334750;Moonjumper;1473621498;;A", -- [160]
						"1;;;;;334750;Moonjumper;1473621497;;A", -- [161]
						"1;;;;;334750;Moonjumper;1473621495;;A", -- [162]
						"1;;;;;334750;Moonjumper;1473621491;;A", -- [163]
						"1;;;;;334750;Moonjumper;1473621489;;A", -- [164]
						"1;;;;;334750;Moonjumper;1473621488;;A", -- [165]
						"1;;;;;334750;Moonjumper;1473621487;;A", -- [166]
						"1;;;;;334750;Moonjumper;1473621487;;A", -- [167]
						"1;;;;;334750;Moonjumper;1473621486;;A", -- [168]
						"1;;;;;334750;Moonjumper;1473621485;;A", -- [169]
						"1;;;;;334750;Moonjumper;1473621485;;A", -- [170]
						"1;;;;;334750;Moonjumper;1473621484;;A", -- [171]
						"1;;;;;334750;Moonjumper;1473621483;;A", -- [172]
						"1;;;;;334750;Moonjumper;1473621480;;A", -- [173]
						"1;;;;;334750;Moonjumper;1473621479;;A", -- [174]
						"1;;;;;334750;Moonjumper;1473621479;;A", -- [175]
						"1;;;;;334750;Moonjumper;1473621478;;A", -- [176]
						"1;;;;;334750;Moonjumper;1473621477;;A", -- [177]
						"1;;;;;334750;Moonjumper;1473621476;;A", -- [178]
						"1;;;;;334750;Moonjumper;1473621475;;A", -- [179]
						"1;;;;;334750;Moonjumper;1473621475;;A", -- [180]
						"1;;;;;334750;Moonjumper;1473621474;;A", -- [181]
						"1;;;;;334750;Moonjumper;1473621474;;A", -- [182]
						"1;;;;;332000;Vàlentìne;1473621472;;A", -- [183]
						"1;;;;;332000;Vàlentìne;1473621472;;A", -- [184]
						"1;;;;;317000;Vàlentìne;1473621471;;A", -- [185]
						"1;;;;;317000;Vàlentìne;1473621471;;A", -- [186]
						"1;;;;;104110;Iluvpooc;1473621468;;A", -- [187]
						"1;;;;;103920;Iluvpooc;1473621467;;A", -- [188]
						"1;;;;;3920;Iluvpooc;1473621461;;A", -- [189]
						"1;;;;;334750;Moonjumper;1473621403;;A", -- [190]
						"1;;;;;332000;Vàlentìne;1473621402;;A", -- [191]
						"1;;;;;332000;Vàlentìne;1473621401;;A", -- [192]
						"1;;;;;332000;Vàlentìne;1473621401;;A", -- [193]
						"1;;;;;383000;Vàlentìne;1473621557;;A", -- [194]
						"1;;;;;383000;Vàlentìne;1473621556;;A", -- [195]
						"1;;;;;383000;Vàlentìne;1473621555;;A", -- [196]
						"1;;;;;383000;Vàlentìne;1473621554;;A", -- [197]
						"1;;;;;383000;Vàlentìne;1473621552;;A", -- [198]
						"1;;;;;383000;Vàlentìne;1473621551;;A", -- [199]
						"1;;;;;383000;Vàlentìne;1473621550;;A", -- [200]
						"1;;;;;383000;Vàlentìne;1473621549;;A", -- [201]
						"1;;;;;383000;Vàlentìne;1473621549;;A", -- [202]
						"1;;;;;383000;Vàlentìne;1473621548;;A", -- [203]
						"1;;;;;383000;Vàlentìne;1473621548;;A", -- [204]
						"1;;;;;383000;Vàlentìne;1473621547;;A", -- [205]
						"1;;;;;383000;Vàlentìne;1473621546;;A", -- [206]
						"1;;;;;383000;Vàlentìne;1473621546;;A", -- [207]
						"1;;;;;383000;Vàlentìne;1473621545;;A", -- [208]
						"1;;;;;383000;Vàlentìne;1473621545;;A", -- [209]
						"1;;;;;383000;Vàlentìne;1473621544;;A", -- [210]
						"1;;;;;383000;Vàlentìne;1473621544;;A", -- [211]
						"1;;;;;383000;Vàlentìne;1473621543;;A", -- [212]
						"1;;;;;383000;Vàlentìne;1473621543;;A", -- [213]
						"1;;;;;383000;Vàlentìne;1473621542;;A", -- [214]
						"1;;;;;383000;Vàlentìne;1473621540;;A", -- [215]
						"1;;;;;383000;Vàlentìne;1473621540;;A", -- [216]
						"1;;;;;383000;Vàlentìne;1473621539;;A", -- [217]
						"1;;;;;365000;Vàlentìne;1473621537;;A", -- [218]
						"1;;;;;365000;Vàlentìne;1473621534;;A", -- [219]
						"1;;;;;365000;Vàlentìne;1473621533;;A", -- [220]
						"1;;;;;365000;Vàlentìne;1473621531;;A", -- [221]
						"1;;;;;348000;Vàlentìne;1473621527;;A", -- [222]
						"1;;;;;348000;Vàlentìne;1473621526;;A", -- [223]
						"1;;;;;348000;Vàlentìne;1473621525;;A", -- [224]
						"1;;;;;348000;Vàlentìne;1473621525;;A", -- [225]
						"1;;;;;348000;Vàlentìne;1473621524;;A", -- [226]
						"1;;;;;348000;Vàlentìne;1473621524;;A", -- [227]
						"1;;;;;348000;Vàlentìne;1473621521;;A", -- [228]
						"1;;;;;348000;Vàlentìne;1473621520;;A", -- [229]
						"1;;;;;348000;Vàlentìne;1473621519;;A", -- [230]
						"1;;;;;348000;Vàlentìne;1473621518;;A", -- [231]
						"1;;;;;348000;Vàlentìne;1473621517;;A", -- [232]
						"1;;;;;348000;Vàlentìne;1473621517;;A", -- [233]
						"1;;;;;348000;Vàlentìne;1473621515;;A", -- [234]
						"1;;;;;348000;Vàlentìne;1473621514;;A", -- [235]
						"1;;;;;348000;Vàlentìne;1473621513;;A", -- [236]
						"1;;;;;443000;Vàlentìne;1473621583;;A", -- [237]
						"1;;;;;443000;Vàlentìne;1473621583;;A", -- [238]
						"1;;;;;443000;Vàlentìne;1473621581;;A", -- [239]
						"1;;;;;443000;Vàlentìne;1473621581;;A", -- [240]
						"1;;;;;422000;Vàlentìne;1473621580;;A", -- [241]
						"1;;;;;422000;Vàlentìne;1473621579;;A", -- [242]
						"1;;;;;422000;Vàlentìne;1473621578;;A", -- [243]
						"1;;;;;422000;Vàlentìne;1473621578;;A", -- [244]
						"1;;;;;422000;Vàlentìne;1473621577;;A", -- [245]
						"1;;;;;422000;Vàlentìne;1473621576;;A", -- [246]
						"1;;;;;422000;Vàlentìne;1473621575;;A", -- [247]
						"1;;;;;422000;Vàlentìne;1473621574;;A", -- [248]
						"1;;;;;422000;Vàlentìne;1473621572;;A", -- [249]
						"1;;;;;422000;Vàlentìne;1473621571;;A", -- [250]
						"1;;;;;422000;Vàlentìne;1473621570;;A", -- [251]
						"1;;;;;422000;Vàlentìne;1473621569;;A", -- [252]
						"1;;;;;422000;Vàlentìne;1473621569;;A", -- [253]
						"1;;;;;422000;Vàlentìne;1473621568;;A", -- [254]
						"1;;;;;422000;Vàlentìne;1473621568;;A", -- [255]
						"1;;;;;422000;Vàlentìne;1473621567;;A", -- [256]
						"1;;;;;422000;Vàlentìne;1473621566;;A", -- [257]
						"1;;;;;383000;Vàlentìne;1473621563;;A", -- [258]
						"1;;;;;383000;Vàlentìne;1473621563;;A", -- [259]
						"1;;;;;383000;Vàlentìne;1473621562;;A", -- [260]
						"1;;;;;383000;Vàlentìne;1473621561;;A", -- [261]
						"1;;;;;383000;Vàlentìne;1473621561;;A", -- [262]
						"1;;;;;383000;Vàlentìne;1473621560;;A", -- [263]
						"1;;;;;383000;Vàlentìne;1473621559;;A", -- [264]
						"1;;;;;383000;Vàlentìne;1473621559;;A", -- [265]
						"1;;;;;383000;Vàlentìne;1473621557;;A", -- [266]
						"1;;;;;383000;Vàlentìne;1473622085;;A", -- [267]
						"1;;;;;383000;Vàlentìne;1473622083;;A", -- [268]
						"1;;;;;383000;Vàlentìne;1473622072;;A", -- [269]
						"1;;;;;383000;Vàlentìne;1473622070;;A", -- [270]
						"1;;;;;383000;Vàlentìne;1473622069;;A", -- [271]
						"1;;;;;383000;Vàlentìne;1473622068;;A", -- [272]
						"1;;;;;383000;Vàlentìne;1473622067;;A", -- [273]
						"1;;;;;383000;Vàlentìne;1473622067;;A", -- [274]
						"1;;;;;365000;Vàlentìne;1473622066;;A", -- [275]
						"1;;;;;465000;Vàlentìne;1473621982;;A", -- [276]
						"1;;;;;465000;Vàlentìne;1473621981;;A", -- [277]
						"1;;;;;465000;Vàlentìne;1473621981;;A", -- [278]
						"1;;;;;465000;Vàlentìne;1473621980;;A", -- [279]
						"1;;;;;465000;Vàlentìne;1473621979;;A", -- [280]
						"1;;;;;422000;Vàlentìne;1473621977;;A", -- [281]
						"1;;;;;465000;Vàlentìne;1473621976;;A", -- [282]
						"1;;;;;422000;Vàlentìne;1473621973;;A", -- [283]
						"1;;;;;422000;Vàlentìne;1473621972;;A", -- [284]
						"1;;;;;422000;Vàlentìne;1473621970;;A", -- [285]
						"1;;;;;422000;Vàlentìne;1473621970;;A", -- [286]
						"1;;;;;422000;Vàlentìne;1473621969;;A", -- [287]
						"1;;;;;422000;Vàlentìne;1473621967;;A", -- [288]
						"1;;;;;422000;Vàlentìne;1473621966;;A", -- [289]
						"1;;;;;422000;Vàlentìne;1473621965;;A", -- [290]
						"1;;;;;422000;Vàlentìne;1473621964;;A", -- [291]
						"28;;;;;283100;Angelolight;1473621825;;A", -- [292]
						"1;;;;;348000;Vàlentìne;1473622054;;A", -- [293]
						"1;;;;;348000;Vàlentìne;1473622053;;A", -- [294]
						"1;;;;;383000;Vàlentìne;1473622049;;A", -- [295]
						"1;;;;;383000;Vàlentìne;1473622049;;A", -- [296]
						"1;;;;;383000;Vàlentìne;1473622039;;A", -- [297]
						"1;;;;;383000;Vàlentìne;1473622038;;A", -- [298]
						"1;;;;;383000;Vàlentìne;1473622037;;A", -- [299]
						"1;;;;;383000;Vàlentìne;1473622036;;A", -- [300]
						"1;;;;;383000;Vàlentìne;1473622034;;A", -- [301]
						"1;;;;;383000;Vàlentìne;1473622033;;A", -- [302]
						"1;;;;;383000;Vàlentìne;1473622032;;A", -- [303]
						"1;;;;;383000;Vàlentìne;1473622028;;A", -- [304]
						"1;;;;;422000;Vàlentìne;1473622023;;A", -- [305]
						"1;;;;;422000;Vàlentìne;1473622022;;A", -- [306]
						"1;;;;;422000;Vàlentìne;1473622020;;A", -- [307]
						"1;;;;;422000;Vàlentìne;1473622020;;A", -- [308]
						"1;;;;;422000;Vàlentìne;1473622019;;A", -- [309]
						"1;;;;;402000;Vàlentìne;1473622018;;A", -- [310]
						"1;;;;;422000;Vàlentìne;1473622016;;A", -- [311]
						"1;;;;;402000;Vàlentìne;1473622015;;A", -- [312]
						"1;;;;;422000;Vàlentìne;1473622012;;A", -- [313]
						"1;;;;;422000;Vàlentìne;1473622011;;A", -- [314]
						"1;;;;;422000;Vàlentìne;1473622010;;A", -- [315]
						"1;;;;;402000;Vàlentìne;1473622009;;A", -- [316]
						"1;;;;;465000;Vàlentìne;1473622008;;A", -- [317]
						"1;;;;;422000;Vàlentìne;1473622007;;A", -- [318]
						"1;;;;;422000;Vàlentìne;1473622005;;A", -- [319]
						"1;;;;;422000;Vàlentìne;1473622004;;A", -- [320]
						"1;;;;;422000;Vàlentìne;1473622003;;A", -- [321]
						"1;;;;;348000;Vàlentìne;1473622001;;A", -- [322]
						"1;;;;;348000;Vàlentìne;1473622001;;A", -- [323]
						"1;;;;;365000;Vàlentìne;1473622000;;A", -- [324]
						"1;;;;;365000;Vàlentìne;1473621999;;A", -- [325]
						"1;;;;;365000;Vàlentìne;1473621999;;A", -- [326]
						"1;;;;;365000;Vàlentìne;1473621998;;A", -- [327]
						"1;;;;;348000;Vàlentìne;1473621997;;A", -- [328]
						"1;;;;;422000;Vàlentìne;1473621995;;A", -- [329]
						"1;;;;;422000;Vàlentìne;1473621994;;A", -- [330]
						"1;;;;;422000;Vàlentìne;1473621994;;A", -- [331]
						"1;;;;;422000;Vàlentìne;1473621993;;A", -- [332]
						"1;;;;;422000;Vàlentìne;1473621993;;A", -- [333]
						"1;;;;;422000;Vàlentìne;1473621992;;A", -- [334]
						"1;;;;;422000;Vàlentìne;1473621990;;A", -- [335]
						"1;;;;;465000;Vàlentìne;1473621986;;A", -- [336]
						"1;;;;;422000;Vàlentìne;1473621986;;A", -- [337]
						"1;;;;;465000;Vàlentìne;1473621985;;A", -- [338]
						"1;;;;;465000;Vàlentìne;1473621984;;A", -- [339]
						"1;;;;;465000;Vàlentìne;1473621984;;A", -- [340]
						"28;;;;;77100;Angelolight;1473618910;;A", -- [341]
						"1;;;;;3570;Iluvpooc;1473617301;;A", -- [342]
						"1;;;;;3570;Iluvpooc;1473617299;;A", -- [343]
						"1;;;;;3570;Iluvpooc;1473617297;;A", -- [344]
						"1;;;;;3400;Iluvpooc;1473617290;;A", -- [345]
						"1;;;;;3570;Iluvpooc;1473617287;;A", -- [346]
						"1;;;;;3570;Iluvpooc;1473617285;;A", -- [347]
						"1;;;;;3740;Iluvpooc;1473617283;;A", -- [348]
						"1;;;;;3740;Iluvpooc;1473617280;;A", -- [349]
						"1;;;;;3740;Iluvpooc;1473617276;;A", -- [350]
						"1;;;;;3570;Iluvpooc;1473617256;;A", -- [351]
						"1;;;;;3570;Iluvpooc;1473617254;;A", -- [352]
						"1;;;;;3740;Iluvpooc;1473617251;;A", -- [353]
						"1;;;;;3570;Iluvpooc;1473617247;;A", -- [354]
						"1;;;;;3570;Iluvpooc;1473617245;;A", -- [355]
						"1;;;;;3570;Iluvpooc;1473617242;;A", -- [356]
						"1;;;;;3570;Iluvpooc;1473617231;;A", -- [357]
						"1;;;;;3570;Iluvpooc;1473617227;;A", -- [358]
						"1;;;;;3570;Iluvpooc;1473617225;;A", -- [359]
						"1;;;;;3570;Iluvpooc;1473607174;;A", -- [360]
						"1;;;;;3570;Iluvpooc;1473607172;;A", -- [361]
						"1;;;;;3570;Iluvpooc;1473607170;;A", -- [362]
						"1;;;;;3570;Iluvpooc;1473607166;;A", -- [363]
						"1;;;;;3570;Iluvpooc;1473607162;;A", -- [364]
						"1;;;;;3570;Iluvpooc;1473607159;;A", -- [365]
						"1;;;;;3570;Iluvpooc;1473607157;;A", -- [366]
						"1;;;;;3400;Iluvpooc;1473607155;;A", -- [367]
						"1;;;;;3400;Iluvpooc;1473607152;;A", -- [368]
						"1;;;;;3400;Iluvpooc;1473607149;;A", -- [369]
						"1;;;;;3400;Iluvpooc;1473607145;;A", -- [370]
						"1;;;;;3400;Iluvpooc;1473607142;;A", -- [371]
						"1;;;;;3570;Iluvpooc;1473607139;;A", -- [372]
						"1;;;;;3400;Iluvpooc;1473607136;;A", -- [373]
						"1;;;;;3400;Iluvpooc;1473607131;;A", -- [374]
						"1;;;;;3400;Iluvpooc;1473607129;;A", -- [375]
						"1;;;;;3400;Iluvpooc;1473607127;;A", -- [376]
						"1;;;;;3400;Iluvpooc;1473607125;;A", -- [377]
						"1;;;;;3400;Iluvpooc;1473607122;;A", -- [378]
						"1;;;;;3400;Iluvpooc;1473607118;;A", -- [379]
						"1;;;;;3400;Iluvpooc;1473607116;;A", -- [380]
						"1;;;;;3400;Iluvpooc;1473607113;;A", -- [381]
						"1;;;;;3400;Iluvpooc;1473607095;;A", -- [382]
						"1;;;;;2604;Falioic;1473599287;;A", -- [383]
						"1;;;;;2604;Falioic;1473599287;;A", -- [384]
						"1;;;;;2604;Falioic;1473599286;;A", -- [385]
						"1;;;;;2604;Falioic;1473599286;;A", -- [386]
						"1;;;;;2604;Falioic;1473599285;;A", -- [387]
						"1;;;;;2604;Falioic;1473599285;;A", -- [388]
						"1;;;;;2604;Falioic;1473599285;;A", -- [389]
						"1;;;;;2604;Falioic;1473599285;;A", -- [390]
						"1;;;;;2604;Falioic;1473599285;;A", -- [391]
						"1;;;;;2604;Falioic;1473599284;;A", -- [392]
						"1;;;;;2604;Falioic;1473599284;;A", -- [393]
						"1;;;;;2604;Falioic;1473599284;;A", -- [394]
						"1;;;;;2604;Falioic;1473599283;;A", -- [395]
						"1;;;;;2604;Falioic;1473599283;;A", -- [396]
						"1;;;;;2604;Falioic;1473599283;;A", -- [397]
						"1;;;;;2604;Falioic;1473599282;;A", -- [398]
						"1;;;;;2604;Falioic;1473599282;;A", -- [399]
						"1;;;;;2604;Falioic;1473599281;;A", -- [400]
						"1;;;;;2604;Falioic;1473599281;;A", -- [401]
						"1;;;;;2604;Falioic;1473599281;;A", -- [402]
						"1;;;;;2734;Falioic;1473599280;;A", -- [403]
						"1;;;;;2604;Falioic;1473599280;;A", -- [404]
						"1;;;;;2734;Falioic;1473599280;;A", -- [405]
						"1;;;;;2734;Falioic;1473599279;;A", -- [406]
						"1;;;;;288000;Vàlentìne;1473596373;;A", -- [407]
						"1;;;;;288000;Vàlentìne;1473596371;;A", -- [408]
						"1;;;;;288000;Vàlentìne;1473596370;;A", -- [409]
						"1;;;;;288000;Vàlentìne;1473596369;;A", -- [410]
						"1;;;;;288000;Vàlentìne;1473596369;;A", -- [411]
						"1;;;;;288000;Vàlentìne;1473596369;;A", -- [412]
						"1;;;;;288000;Vàlentìne;1473596368;;A", -- [413]
						"1;;;;;288000;Vàlentìne;1473596367;;A", -- [414]
						"1;;;;;288000;Vàlentìne;1473596366;;A", -- [415]
						"1;;;;;288000;Vàlentìne;1473596364;;A", -- [416]
						"1;;;;;348000;Vàlentìne;1473596442;;A", -- [417]
						"1;;;;;332000;Vàlentìne;1473596440;;A", -- [418]
						"1;;;;;348000;Vàlentìne;1473596439;;A", -- [419]
						"1;;;;;348000;Vàlentìne;1473596438;;A", -- [420]
						"1;;;;;348000;Vàlentìne;1473596437;;A", -- [421]
						"1;;;;;348000;Vàlentìne;1473596434;;A", -- [422]
						"1;;;;;348000;Vàlentìne;1473596432;;A", -- [423]
						"1;;;;;348000;Vàlentìne;1473596432;;A", -- [424]
						"1;;;;;348000;Vàlentìne;1473596430;;A", -- [425]
						"1;;;;;348000;Vàlentìne;1473596429;;A", -- [426]
						"1;;;;;348000;Vàlentìne;1473596428;;A", -- [427]
						"1;;;;;348000;Vàlentìne;1473596428;;A", -- [428]
						"1;;;;;348000;Vàlentìne;1473596426;;A", -- [429]
						"1;;;;;348000;Vàlentìne;1473596425;;A", -- [430]
						"1;;;;;348000;Vàlentìne;1473596424;;A", -- [431]
						"1;;;;;348000;Vàlentìne;1473596423;;A", -- [432]
						"1;;;;;348000;Vàlentìne;1473596422;;A", -- [433]
						"1;;;;;348000;Vàlentìne;1473596417;;A", -- [434]
						"1;;;;;348000;Vàlentìne;1473596416;;A", -- [435]
						"1;;;;;348000;Vàlentìne;1473596415;;A", -- [436]
						"1;;;;;348000;Vàlentìne;1473596414;;A", -- [437]
						"1;;;;;348000;Vàlentìne;1473596414;;A", -- [438]
						"1;;;;;348000;Vàlentìne;1473596412;;A", -- [439]
						"1;;;;;317000;Vàlentìne;1473596411;;A", -- [440]
						"1;;;;;317000;Vàlentìne;1473596409;;A", -- [441]
						"1;;;;;317000;Vàlentìne;1473596408;;A", -- [442]
						"1;;;;;317000;Vàlentìne;1473596407;;A", -- [443]
						"1;;;;;317000;Vàlentìne;1473596405;;A", -- [444]
						"1;;;;;317000;Vàlentìne;1473596404;;A", -- [445]
						"1;;;;;317000;Vàlentìne;1473596401;;A", -- [446]
						"1;;;;;317000;Vàlentìne;1473596399;;A", -- [447]
						"1;;;;;317000;Vàlentìne;1473596397;;A", -- [448]
						"1;;;;;302000;Vàlentìne;1473596395;;A", -- [449]
						"1;;;;;317000;Vàlentìne;1473596394;;A", -- [450]
						"1;;;;;302000;Vàlentìne;1473596393;;A", -- [451]
						"1;;;;;317000;Vàlentìne;1473596391;;A", -- [452]
						"1;;;;;302000;Vàlentìne;1473596390;;A", -- [453]
						"1;;;;;302000;Vàlentìne;1473596388;;A", -- [454]
						"1;;;;;317000;Vàlentìne;1473596387;;A", -- [455]
						"1;;;;;302000;Vàlentìne;1473596387;;A", -- [456]
						"1;;;;;302000;Vàlentìne;1473596386;;A", -- [457]
						"1;;;;;302000;Vàlentìne;1473596385;;A", -- [458]
						"1;;;;;317000;Vàlentìne;1473596384;;A", -- [459]
						"1;;;;;302000;Vàlentìne;1473596384;;A", -- [460]
						"1;;;;;317000;Vàlentìne;1473596384;;A", -- [461]
						"1;;;;;302000;Vàlentìne;1473596382;;A", -- [462]
						"1;;;;;317000;Vàlentìne;1473596381;;A", -- [463]
						"1;;;;;302000;Vàlentìne;1473596380;;A", -- [464]
						"1;;;;;302000;Vàlentìne;1473596378;;A", -- [465]
						"1;;;;;288000;Vàlentìne;1473596373;;A", -- [466]
						"1;;;;;288000;Vàlentìne;1473596534;;A", -- [467]
						"1;;;;;302000;Vàlentìne;1473596532;;A", -- [468]
						"1;;;;;317000;Vàlentìne;1473596531;;A", -- [469]
						"1;;;;;317000;Vàlentìne;1473596529;;A", -- [470]
						"1;;;;;317000;Vàlentìne;1473596528;;A", -- [471]
						"1;;;;;317000;Vàlentìne;1473596527;;A", -- [472]
						"1;;;;;317000;Vàlentìne;1473596526;;A", -- [473]
						"1;;;;;317000;Vàlentìne;1473596525;;A", -- [474]
						"1;;;;;317000;Vàlentìne;1473596524;;A", -- [475]
						"1;;;;;317000;Vàlentìne;1473596522;;A", -- [476]
						"1;;;;;317000;Vàlentìne;1473596521;;A", -- [477]
						"1;;;;;317000;Vàlentìne;1473596520;;A", -- [478]
						"1;;;;;332000;Vàlentìne;1473596519;;A", -- [479]
						"1;;;;;348000;Vàlentìne;1473596518;;A", -- [480]
						"1;;;;;332000;Vàlentìne;1473596517;;A", -- [481]
						"1;;;;;348000;Vàlentìne;1473596513;;A", -- [482]
						"1;;;;;332000;Vàlentìne;1473596512;;A", -- [483]
						"1;;;;;348000;Vàlentìne;1473596508;;A", -- [484]
						"1;;;;;402000;Vàlentìne;1473596490;;A", -- [485]
						"1;;;;;402000;Vàlentìne;1473596489;;A", -- [486]
						"1;;;;;402000;Vàlentìne;1473596488;;A", -- [487]
						"1;;;;;402000;Vàlentìne;1473596484;;A", -- [488]
						"1;;;;;383000;Vàlentìne;1473596482;;A", -- [489]
						"1;;;;;383000;Vàlentìne;1473596480;;A", -- [490]
						"1;;;;;383000;Vàlentìne;1473596480;;A", -- [491]
						"1;;;;;383000;Vàlentìne;1473596478;;A", -- [492]
						"1;;;;;383000;Vàlentìne;1473596478;;A", -- [493]
						"1;;;;;383000;Vàlentìne;1473596477;;A", -- [494]
						"1;;;;;383000;Vàlentìne;1473596476;;A", -- [495]
						"1;;;;;383000;Vàlentìne;1473596475;;A", -- [496]
						"1;;;;;383000;Vàlentìne;1473596474;;A", -- [497]
						"1;;;;;383000;Vàlentìne;1473596472;;A", -- [498]
						"1;;;;;383000;Vàlentìne;1473596469;;A", -- [499]
						"1;;;;;383000;Vàlentìne;1473596467;;A", -- [500]
						"1;;;;;383000;Vàlentìne;1473596466;;A", -- [501]
						"1;;;;;383000;Vàlentìne;1473596464;;A", -- [502]
						"1;;;;;383000;Vàlentìne;1473596464;;A", -- [503]
						"1;;;;;383000;Vàlentìne;1473596462;;A", -- [504]
						"1;;;;;332000;Vàlentìne;1473596460;;A", -- [505]
						"1;;;;;348000;Vàlentìne;1473596458;;A", -- [506]
						"1;;;;;332000;Vàlentìne;1473596458;;A", -- [507]
						"1;;;;;348000;Vàlentìne;1473596457;;A", -- [508]
						"1;;;;;348000;Vàlentìne;1473596453;;A", -- [509]
						"1;;;;;332000;Vàlentìne;1473596452;;A", -- [510]
						"1;;;;;348000;Vàlentìne;1473596452;;A", -- [511]
						"1;;;;;348000;Vàlentìne;1473596449;;A", -- [512]
						"1;;;;;383000;Vàlentìne;1473596447;;A", -- [513]
						"1;;;;;348000;Vàlentìne;1473596446;;A", -- [514]
						"1;;;;;348000;Vàlentìne;1473596444;;A", -- [515]
						"1;;;;;288000;Vàlentìne;1473587412;;A", -- [516]
						"1;;;;;288000;Vàlentìne;1473587408;;A", -- [517]
						"1;;;;;275000;Vàlentìne;1473587406;;A", -- [518]
						"1;;;;;275000;Vàlentìne;1473587405;;A", -- [519]
						"1;;;;;275000;Vàlentìne;1473587400;;A", -- [520]
						"1;;;;;275000;Vàlentìne;1473587398;;A", -- [521]
						"1;;;;;288000;Vàlentìne;1473587393;;A", -- [522]
						"1;;;;;288000;Vàlentìne;1473587390;;A", -- [523]
						"1;;;;;288000;Vàlentìne;1473587387;;A", -- [524]
						"1;;;;;262000;Vàlentìne;1473587377;;A", -- [525]
						"1;;;;;262000;Vàlentìne;1473587376;;A", -- [526]
						"1;;;;;262000;Vàlentìne;1473587374;;A", -- [527]
						"1;;;;;262000;Vàlentìne;1473587372;;A", -- [528]
						"1;;;;;262000;Vàlentìne;1473587372;;A", -- [529]
						"1;;;;;262000;Vàlentìne;1473587369;;A", -- [530]
						"1;;;;;262000;Vàlentìne;1473587368;;A", -- [531]
						"1;;;;;262000;Vàlentìne;1473587364;;A", -- [532]
						"1;;;;;262000;Vàlentìne;1473587362;;A", -- [533]
						"1;;;;;262000;Vàlentìne;1473587359;;A", -- [534]
						"1;;;;;262000;Vàlentìne;1473587357;;A", -- [535]
						"1;;;;;262000;Vàlentìne;1473587356;;A", -- [536]
						"1;;;;;348000;Vàlentìne;1473578662;;A", -- [537]
						"1;;;;;348000;Vàlentìne;1473578660;;A", -- [538]
						"1;;;;;348000;Vàlentìne;1473578658;;A", -- [539]
						"1;;;;;348000;Vàlentìne;1473578656;;A", -- [540]
						"1;;;;;348000;Vàlentìne;1473578654;;A", -- [541]
						"1;;;;;348000;Vàlentìne;1473587517;;A", -- [542]
						"1;;;;;348000;Vàlentìne;1473587513;;A", -- [543]
						"1;;;;;348000;Vàlentìne;1473587510;;A", -- [544]
						"1;;;;;348000;Vàlentìne;1473587509;;A", -- [545]
						"1;;;;;348000;Vàlentìne;1473587506;;A", -- [546]
						"1;;;;;348000;Vàlentìne;1473587503;;A", -- [547]
						"1;;;;;348000;Vàlentìne;1473587501;;A", -- [548]
						"1;;;;;348000;Vàlentìne;1473587499;;A", -- [549]
						"1;;;;;348000;Vàlentìne;1473587498;;A", -- [550]
						"1;;;;;348000;Vàlentìne;1473587496;;A", -- [551]
						"1;;;;;348000;Vàlentìne;1473587495;;A", -- [552]
						"1;;;;;348000;Vàlentìne;1473587494;;A", -- [553]
						"1;;;;;348000;Vàlentìne;1473587493;;A", -- [554]
						"1;;;;;348000;Vàlentìne;1473587491;;A", -- [555]
						"1;;;;;348000;Vàlentìne;1473587490;;A", -- [556]
						"1;;;;;348000;Vàlentìne;1473587488;;A", -- [557]
						"1;;;;;348000;Vàlentìne;1473587485;;A", -- [558]
						"1;;;;;348000;Vàlentìne;1473587484;;A", -- [559]
						"1;;;;;317000;Vàlentìne;1473587482;;A", -- [560]
						"1;;;;;317000;Vàlentìne;1473587481;;A", -- [561]
						"1;;;;;317000;Vàlentìne;1473587480;;A", -- [562]
						"1;;;;;317000;Vàlentìne;1473587478;;A", -- [563]
						"1;;;;;317000;Vàlentìne;1473587477;;A", -- [564]
						"1;;;;;317000;Vàlentìne;1473587475;;A", -- [565]
						"1;;;;;317000;Vàlentìne;1473587473;;A", -- [566]
						"1;;;;;317000;Vàlentìne;1473587470;;A", -- [567]
						"1;;;;;317000;Vàlentìne;1473587468;;A", -- [568]
						"1;;;;;317000;Vàlentìne;1473587467;;A", -- [569]
						"1;;;;;317000;Vàlentìne;1473587465;;A", -- [570]
						"1;;;;;317000;Vàlentìne;1473587464;;A", -- [571]
						"1;;;;;317000;Vàlentìne;1473587463;;A", -- [572]
						"1;;;;;317000;Vàlentìne;1473587461;;A", -- [573]
						"1;;;;;317000;Vàlentìne;1473587459;;A", -- [574]
						"1;;;;;317000;Vàlentìne;1473587456;;A", -- [575]
						"1;;;;;317000;Vàlentìne;1473587455;;A", -- [576]
						"1;;;;;317000;Vàlentìne;1473587453;;A", -- [577]
						"1;;;;;317000;Vàlentìne;1473587450;;A", -- [578]
						"1;;;;;317000;Vàlentìne;1473587450;;A", -- [579]
						"1;;;;;317000;Vàlentìne;1473587448;;A", -- [580]
						"1;;;;;317000;Vàlentìne;1473587447;;A", -- [581]
						"1;;;;;317000;Vàlentìne;1473587445;;A", -- [582]
						"1;;;;;317000;Vàlentìne;1473587441;;A", -- [583]
						"1;;;;;317000;Vàlentìne;1473587440;;A", -- [584]
						"1;;;;;317000;Vàlentìne;1473587438;;A", -- [585]
						"1;;;;;288000;Vàlentìne;1473587416;;A", -- [586]
						"1;;;;;262000;Vàlentìne;1473570638;;A", -- [587]
						"1;;;;;262000;Vàlentìne;1473570634;;A", -- [588]
						"1;;;;;262000;Vàlentìne;1473570629;;A", -- [589]
						"1;;;;;262000;Vàlentìne;1473570625;;A", -- [590]
						"1;;;;;262000;Vàlentìne;1473570622;;A", -- [591]
						"1;;;;;262000;Vàlentìne;1473570616;;A", -- [592]
						"1;;;;;262000;Vàlentìne;1473570612;;A", -- [593]
						"1;;;;;216750;Pattyheel;1473570580;;A", -- [594]
						"1;;;;;446990;Metaltheory;1473563532;;A", -- [595]
						"1;;;;;446990;Metaltheory;1473563529;;A", -- [596]
						"1;;;;;446990;Metaltheory;1473563524;;A", -- [597]
						"1;;;;;446710;Metaltheory;1473563521;;A", -- [598]
						"1;;;;;523999;Vinroth;1473568170;;A", -- [599]
						"1;;;;;523999;Vinroth;1473568166;;A", -- [600]
						"1;;;;;523999;Vinroth;1473568163;;A", -- [601]
						"1;;;;;523999;Vinroth;1473568162;;A", -- [602]
						"1;;;;;523999;Vinroth;1473568160;;A", -- [603]
						"1;;;;;523999;Vinroth;1473568158;;A", -- [604]
						"1;;;;;523999;Vinroth;1473568156;;A", -- [605]
						"1;;;;;523999;Vinroth;1473568155;;A", -- [606]
						"1;;;;;523999;Vinroth;1473568152;;A", -- [607]
						"1;;;;;523999;Vinroth;1473568150;;A", -- [608]
						"1;;;;;523999;Vinroth;1473568148;;A", -- [609]
						"1;;;;;523999;Vinroth;1473568147;;A", -- [610]
						"1;;;;;523999;Vinroth;1473568146;;A", -- [611]
						"1;;;;;523999;Vinroth;1473568144;;A", -- [612]
						"1;;;;;523999;Vinroth;1473568142;;A", -- [613]
						"1;;;;;523999;Vinroth;1473568139;;A", -- [614]
						"1;;;;;523999;Vinroth;1473568137;;A", -- [615]
						"1;;;;;523999;Vinroth;1473568136;;A", -- [616]
						"1;;;;;523999;Vinroth;1473568133;;A", -- [617]
						"1;;;;;523999;Vinroth;1473568132;;A", -- [618]
						"1;;;;;523999;Vinroth;1473568131;;A", -- [619]
						"1;;;;;523999;Vinroth;1473568130;;A", -- [620]
						"1;;;;;523999;Vinroth;1473568126;;A", -- [621]
						"1;;;;;523999;Vinroth;1473568123;;A", -- [622]
						"1;;;;;459065;Vorniir;1473566621;;A", -- [623]
						"1;;;;;454965;Vorniir;1473566620;;A", -- [624]
						"1;;;;;451965;Vorniir;1473566617;;A", -- [625]
						"1;;;;;2950;Iluvpooc;1473560200;;A", -- [626]
						"1;;;;;2950;Iluvpooc;1473560198;;A", -- [627]
						"1;;;;;2950;Iluvpooc;1473560195;;A", -- [628]
						"1;;;;;2950;Iluvpooc;1473560194;;A", -- [629]
						"1;;;;;2950;Iluvpooc;1473560192;;A", -- [630]
						"1;;;;;2950;Iluvpooc;1473560190;;A", -- [631]
						"1;;;;;2950;Iluvpooc;1473560182;;A", -- [632]
						"1;;;;;2950;Iluvpooc;1473560179;;A", -- [633]
						"1;;;;;2950;Iluvpooc;1473560176;;A", -- [634]
						"1;;;;;2950;Iluvpooc;1473560172;;A", -- [635]
						"1;;;;;2950;Iluvpooc;1473560170;;A", -- [636]
						"1;;;;;2950;Iluvpooc;1473560165;;A", -- [637]
						"1;;;;;2950;Iluvpooc;1473560163;;A", -- [638]
						"1;;;;;2950;Iluvpooc;1473560161;;A", -- [639]
						"1;;;;;2950;Iluvpooc;1473560159;;A", -- [640]
						"1;;;;;2950;Iluvpooc;1473560153;;A", -- [641]
						"1;;;;;2950;Iluvpooc;1473560151;;A", -- [642]
						"1;;;;;2950;Iluvpooc;1473560149;;A", -- [643]
						"1;;;;;2950;Iluvpooc;1473560147;;A", -- [644]
						"1;;;;;2950;Iluvpooc;1473560146;;A", -- [645]
						"1;;;;;2950;Iluvpooc;1473560141;;A", -- [646]
						"1;;;;;2950;Iluvpooc;1473560139;;A", -- [647]
						"1;;;;;2950;Iluvpooc;1473560136;;A", -- [648]
						"1;;;;;2950;Iluvpooc;1473560132;;A", -- [649]
						"1;;;;;2950;Iluvpooc;1473560130;;A", -- [650]
						"1;;;;;2950;Iluvpooc;1473560126;;A", -- [651]
						"1;;;;;2950;Iluvpooc;1473560124;;A", -- [652]
						"1;;;;;2950;Iluvpooc;1473560122;;A", -- [653]
						"1;;;;;2950;Iluvpooc;1473560119;;A", -- [654]
						"1;;;;;2950;Iluvpooc;1473560116;;A", -- [655]
						"1;;;;;2950;Iluvpooc;1473560112;;A", -- [656]
						"1;;;;;2950;Iluvpooc;1473560104;;A", -- [657]
						"1;;;;;2950;Iluvpooc;1473560102;;A", -- [658]
						"1;;;;;2950;Iluvpooc;1473560100;;A", -- [659]
						"1;;;;;2950;Iluvpooc;1473560096;;A", -- [660]
						"1;;;;;2950;Iluvpooc;1473560090;;A", -- [661]
						"1;;;;;5710;Metaltheory;1473560062;;A", -- [662]
						"1;;;;;5710;Metaltheory;1473560061;;A", -- [663]
						"1;;;;;5710;Metaltheory;1473560058;;A", -- [664]
						"1;;;;;5710;Metaltheory;1473560057;;A", -- [665]
						"1;;;;;5710;Metaltheory;1473560054;;A", -- [666]
						"1;;;;;9684;Splewlock;1473560045;;A", -- [667]
						"1;;;;;9224;Splewlock;1473560042;;A", -- [668]
						"1;;;;;9224;Splewlock;1473560039;;A", -- [669]
						"1;;;;;9224;Splewlock;1473560034;;A", -- [670]
						"1;;;;;6280;Metaltheory;1473560030;;A", -- [671]
						"1;;;;;5990;Metaltheory;1473560027;;A", -- [672]
						"1;;;;;5990;Metaltheory;1473560024;;A", -- [673]
						"1;;;;;5990;Metaltheory;1473560020;;A", -- [674]
						"1;;;;;5710;Metaltheory;1473560017;;A", -- [675]
						"1;;;;;5710;Metaltheory;1473560015;;A", -- [676]
						"1;;;;;2950;Iluvpooc;1473559058;;A", -- [677]
						"1;;;;;2950;Iluvpooc;1473559050;;A", -- [678]
						"1;;;;;2950;Iluvpooc;1473559046;;A", -- [679]
						"1;;;;;2950;Iluvpooc;1473559043;;A", -- [680]
						"1;;;;;2950;Iluvpooc;1473559040;;A", -- [681]
						"1;;;;;2950;Iluvpooc;1473559038;;A", -- [682]
						"1;;;;;2950;Iluvpooc;1473559035;;A", -- [683]
						"1;;;;;2950;Iluvpooc;1473559030;;A", -- [684]
						"1;;;;;2950;Iluvpooc;1473559027;;A", -- [685]
						"1;;;;;262000;Vàlentìne;1473558401;;A", -- [686]
						"1;;;;;263500;Pattyheel;1473558399;;A", -- [687]
						"1;;;;;266125;Pattyheel;1473558396;;A", -- [688]
						"1;;;;;270875;Pattyheel;1473558394;;A", -- [689]
						"1;;;;;275000;Vàlentìne;1473558391;;A", -- [690]
						"1;;;;;275000;Vàlentìne;1473558389;;A", -- [691]
						"1;;;;;275000;Vàlentìne;1473558383;;A", -- [692]
						"1;;;;;275000;Vàlentìne;1473558380;;A", -- [693]
						"1;;;;;275000;Vàlentìne;1473558378;;A", -- [694]
						"1;;;;;275000;Vàlentìne;1473558375;;A", -- [695]
						"1;;;;;275000;Vàlentìne;1473558370;;A", -- [696]
						"1;;;;;275000;Vàlentìne;1473558366;;A", -- [697]
						"1;;;;;275000;Vàlentìne;1473558365;;A", -- [698]
						"1;;;;;275000;Vàlentìne;1473558364;;A", -- [699]
						"1;;;;;275000;Vàlentìne;1473558363;;A", -- [700]
						"1;;;;;275000;Vàlentìne;1473558361;;A", -- [701]
						"1;;;;;9684;Splewlock;1473557372;;A", -- [702]
						"1;;;;;10164;Splewlock;1473557368;;A", -- [703]
						"1;;;;;11744;Splewlock;1473557364;;A", -- [704]
						"1;;;;;12324;Splewlock;1473557363;;A", -- [705]
						"1;;;;;12324;Splewlock;1473557361;;A", -- [706]
						"1;;;;;12324;Splewlock;1473557360;;A", -- [707]
						"1;;;;;12324;Splewlock;1473557359;;A", -- [708]
						"1;;;;;13574;Splewlock;1473557358;;A", -- [709]
						"1;;;;;13574;Splewlock;1473557358;;A", -- [710]
						"1;;;;;13574;Splewlock;1473557357;;A", -- [711]
						"1;;;;;456700;Splewlock;1473557355;;A", -- [712]
						"1;;;;;500200;Splewlock;1473557353;;A", -- [713]
						"1;;;;;505794;Splewlock;1473557351;;A", -- [714]
						"1;;;;;523999;Vinroth;1473557343;;A", -- [715]
						"1;;;;;523999;Vinroth;1473557342;;A", -- [716]
						"1;;;;;523999;Vinroth;1473557341;;A", -- [717]
						"1;;;;;523999;Vinroth;1473557340;;A", -- [718]
						"1;;;;;523999;Vinroth;1473557338;;A", -- [719]
						"1;;;;;523999;Vinroth;1473557285;;A", -- [720]
						"1;;;;;523999;Vinroth;1473557284;;A", -- [721]
						"1;;;;;523999;Vinroth;1473557230;;A", -- [722]
						"1;;;;;523999;Vinroth;1473557222;;A", -- [723]
						"1;;;;;523999;Vinroth;1473557221;;A", -- [724]
						"1;;;;;523999;Vinroth;1473557219;;A", -- [725]
						"1;;;;;523999;Vinroth;1473557217;;A", -- [726]
						"1;;;;;523999;Vinroth;1473557214;;A", -- [727]
						"1;;;;;523999;Vinroth;1473557209;;A", -- [728]
						"1;;;;;5710;Metaltheory;1473556647;;A", -- [729]
						"1;;;;;5990;Metaltheory;1473556643;;A", -- [730]
						"1;;;;;5710;Metaltheory;1473556641;;A", -- [731]
						"1;;;;;5990;Metaltheory;1473556639;;A", -- [732]
						"1;;;;;5710;Metaltheory;1473556636;;A", -- [733]
						"1;;;;;5710;Metaltheory;1473556629;;A", -- [734]
						"1;;;;;5710;Metaltheory;1473556624;;A", -- [735]
						"1;;;;;5710;Metaltheory;1473556620;;A", -- [736]
						"1;;;;;5710;Metaltheory;1473556614;;A", -- [737]
						"1;;;;;5710;Metaltheory;1473556611;;A", -- [738]
						"1;;;;;5440;Metaltheory;1473556606;;A", -- [739]
						"1;;;;;405990;Metaltheory;1473555740;;A", -- [740]
						"1;;;;;405990;Metaltheory;1473555729;;A", -- [741]
						"1;;;;;405990;Metaltheory;1473555723;;A", -- [742]
						"1;;;;;405710;Metaltheory;1473555721;;A", -- [743]
						"1;;;;;59065;Vorniir;1473555718;;A", -- [744]
						"1;;;;;53665;Vorniir;1473555716;;A", -- [745]
						"1;;;;;56265;Vorniir;1473555714;;A", -- [746]
						"1;;;;;68065;Vorniir;1473555712;;A", -- [747]
						"1;;;;;5440;Metaltheory;1473555368;;A", -- [748]
						"1;;;;;5190;Metaltheory;1473555366;;A", -- [749]
						"1;;;;;5440;Metaltheory;1473555365;;A", -- [750]
						"1;;;;;5440;Metaltheory;1473555363;;A", -- [751]
						"1;;;;;2680;Iluvpooc;1473553711;;A", -- [752]
						"1;;;;;2680;Iluvpooc;1473553710;;A", -- [753]
						"1;;;;;2680;Iluvpooc;1473553706;;A", -- [754]
						"1;;;;;2680;Iluvpooc;1473553695;;A", -- [755]
						"1;;;;;2680;Iluvpooc;1473553693;;A", -- [756]
						"1;;;;;2680;Iluvpooc;1473553692;;A", -- [757]
						"1;;;;;2680;Iluvpooc;1473553691;;A", -- [758]
						"1;;;;;2680;Iluvpooc;1473553690;;A", -- [759]
						"1;;;;;2680;Iluvpooc;1473553689;;A", -- [760]
						"1;;;;;2680;Iluvpooc;1473553686;;A", -- [761]
						"1;;;;;2680;Iluvpooc;1473553685;;A", -- [762]
						"1;;;;;2680;Iluvpooc;1473553683;;A", -- [763]
						"1;;;;;2680;Iluvpooc;1473553683;;A", -- [764]
						"1;;;;;2680;Iluvpooc;1473553681;;A", -- [765]
						"1;;;;;2680;Iluvpooc;1473553681;;A", -- [766]
						"1;;;;;2680;Iluvpooc;1473553681;;A", -- [767]
						"1;;;;;2680;Iluvpooc;1473553680;;A", -- [768]
						"1;;;;;2680;Iluvpooc;1473553679;;A", -- [769]
						"1;;;;;2680;Iluvpooc;1473553677;;A", -- [770]
						"1;;;;;2680;Iluvpooc;1473553675;;A", -- [771]
						"1;;;;;2680;Iluvpooc;1473553842;;A", -- [772]
						"1;;;;;2680;Iluvpooc;1473553842;;A", -- [773]
						"1;;;;;5440;Metaltheory;1473553840;;A", -- [774]
						"1;;;;;5190;Metaltheory;1473553837;;A", -- [775]
						"1;;;;;5190;Metaltheory;1473553836;;A", -- [776]
						"1;;;;;4950;Metaltheory;1473553836;;A", -- [777]
						"1;;;;;4720;Metaltheory;1473553835;;A", -- [778]
						"1;;;;;4720;Metaltheory;1473553834;;A", -- [779]
						"1;;;;;8794;Splewlock;1473553823;;A", -- [780]
						"1;;;;;8384;Splewlock;1473553822;;A", -- [781]
						"1;;;;;8384;Splewlock;1473553821;;A", -- [782]
						"1;;;;;2680;Iluvpooc;1473553815;;A", -- [783]
						"1;;;;;2680;Iluvpooc;1473553802;;A", -- [784]
						"1;;;;;2680;Iluvpooc;1473553802;;A", -- [785]
						"1;;;;;2680;Iluvpooc;1473553801;;A", -- [786]
						"1;;;;;2680;Iluvpooc;1473553800;;A", -- [787]
						"1;;;;;2680;Iluvpooc;1473553799;;A", -- [788]
						"1;;;;;2680;Iluvpooc;1473553797;;A", -- [789]
						"1;;;;;10664;Splewlock;1473553785;;A", -- [790]
						"1;;;;;10664;Splewlock;1473553784;;A", -- [791]
						"1;;;;;10664;Splewlock;1473553783;;A", -- [792]
						"1;;;;;10164;Splewlock;1473553783;;A", -- [793]
						"1;;;;;7994;Splewlock;1473553781;;A", -- [794]
						"1;;;;;7994;Splewlock;1473553779;;A", -- [795]
						"1;;;;;8384;Splewlock;1473553777;;A", -- [796]
						"1;;;;;8384;Splewlock;1473553773;;A", -- [797]
						"1;;;;;7994;Splewlock;1473553772;;A", -- [798]
						"1;;;;;8384;Splewlock;1473553772;;A", -- [799]
						"1;;;;;2680;Iluvpooc;1473553766;;A", -- [800]
						"1;;;;;2680;Iluvpooc;1473553764;;A", -- [801]
						"1;;;;;2680;Iluvpooc;1473553759;;A", -- [802]
						"1;;;;;4720;Metaltheory;1473553757;;A", -- [803]
						"1;;;;;4950;Metaltheory;1473553753;;A", -- [804]
						"1;;;;;5190;Metaltheory;1473553752;;A", -- [805]
						"1;;;;;4950;Metaltheory;1473553751;;A", -- [806]
						"1;;;;;5190;Metaltheory;1473553748;;A", -- [807]
						"1;;;;;4950;Metaltheory;1473553748;;A", -- [808]
						"1;;;;;4720;Metaltheory;1473553747;;A", -- [809]
						"1;;;;;2680;Iluvpooc;1473553739;;A", -- [810]
						"1;;;;;2680;Iluvpooc;1473553738;;A", -- [811]
						"1;;;;;2680;Iluvpooc;1473553734;;A", -- [812]
						"1;;;;;2680;Iluvpooc;1473553733;;A", -- [813]
						"1;;;;;2680;Iluvpooc;1473553729;;A", -- [814]
						"1;;;;;2680;Iluvpooc;1473553728;;A", -- [815]
						"1;;;;;2680;Iluvpooc;1473553728;;A", -- [816]
						"1;;;;;2680;Iluvpooc;1473553726;;A", -- [817]
						"1;;;;;2680;Iluvpooc;1473553726;;A", -- [818]
						"1;;;;;2680;Iluvpooc;1473553720;;A", -- [819]
						"1;;;;;2680;Iluvpooc;1473553720;;A", -- [820]
						"1;;;;;2680;Iluvpooc;1473553715;;A", -- [821]
						"1;;;;;4720;Metaltheory;1473553875;;A", -- [822]
						"1;;;;;4720;Metaltheory;1473553874;;A", -- [823]
						"1;;;;;11744;Splewlock;1473553847;;A", -- [824]
						"1;;;;;11744;Splewlock;1473553846;;A", -- [825]
						"1;;;;;11744;Splewlock;1473553845;;A", -- [826]
						"1;;;;;2680;Iluvpooc;1473553844;;A", -- [827]
						"1;;;;;10664;Splewlock;1473553844;;A", -- [828]
					},
				},
				["124101"] = {
					["item:124101::::::::100:::::::"] = {
						"9;;;;;57925;Haemophilia;1473638386;;A", -- [1]
						"9;;;;;52625;Haemophilia;1473632952;;A", -- [2]
					},
				},
			},
			["completedAuctions"] = {
				["136708"] = {
					["item:136708::::::::100:::::::"] = {
						"1;26125100;100;1375000;27500000;25000000;Toxicsausage;1473625839;;A", -- [1]
						"1;26125100;100;1375000;27500000;25000000;Toxicsausage;1473625833;;A", -- [2]
						"1;26125100;100;1375000;27500000;25000000;Toxicsausage;1473625831;;A", -- [3]
						"1;23740600;100;1249500;24990000;20000000;Ikeru;1473612724;;A", -- [4]
						"1;23750100;100;1250000;25000000;20000000;Ikeru;1473599190;;A", -- [5]
					},
				},
				["136683"] = {
					["item:136683::::::::100:::13::::"] = {
						"1;84680499;35499;4455000;89100000;80190000;Senthius;1473622744;;A", -- [1]
					},
				},
				["124117"] = {
					["item:124117::::::::100:::::::"] = {
						"200;2375900;900;125000;2500000;2400000;Rhàegal;1473552285;;A", -- [1]
						"200;2375900;900;125000;2500000;2400000;Eraana;1473550423;;A", -- [2]
					},
				},
				["123956"] = {
					["item:123956::::::::100:::::::"] = {
						"1;14250100;100;750000;15000000;7500000;Nalyna;1473720087;;A", -- [1]
						"1;14250100;100;750000;15000000;7500000;Rebornpanda;1473721712;;A", -- [2]
						"1;8075100;100;425000;8500000;8000000;Angslandh;1473633555;;A", -- [3]
						"1;7125100;100;375000;7500000;7500000;Angslandh;1473633553;;A", -- [4]
						"1;7125100;100;375000;7500000;7000000;Bevicicki;1473615487;;A", -- [5]
					},
				},
				["123918"] = {
					["item:123918::::::::100:::::::"] = {
						"20;4090231;5250;214999;4299980;3800000;Dockworker;1473538029;;A", -- [1]
						"20;4090231;5250;214999;4299980;3800000;Nickkcannon;1473537611;;A", -- [2]
						"20;4090231;5250;214999;4299980;3800000;Nickkcannon;1473537608;;A", -- [3]
						"20;4090231;5250;214999;4299980;3800000;Kryptoxik;1473537413;;A", -- [4]
						"20;4090231;5250;214999;4299980;3800000;Kryptoxik;1473537398;;A", -- [5]
					},
				},
				["124461"] = {
					["item:124461::::::::100:::::::"] = {
						"10;21282625;2625;1120000;22400000;21500000;Searèd;1473721427;;A", -- [1]
						"10;21282625;2625;1120000;22400000;21500000;Searèd;1473721424;;A", -- [2]
						"10;21282625;2625;1120000;22400000;21500000;Searèd;1473721416;;A", -- [3]
						"5;10641311;1311;560000;11200000;10750000;Searèd;1473721398;;A", -- [4]
						"5;10641311;1311;560000;11200000;10750000;Banana;1473701205;;A", -- [5]
						"1;2257461;261;118800;2376000;2138400;Bödysnåtcher;1473601181;;A", -- [6]
					},
				},
				["123919"] = {
					["item:123919::::::::100:::::::"] = {
						"20;16224200;21000;852800;17056000;12830400;Htymknab;1473563980;;A", -- [1]
						"20;14128500;21000;742500;14850000;13365000;Qûestions;1473536576;;A", -- [2]
						"20;14128500;21000;742500;14850000;13365000;Qûestions;1473536573;;A", -- [3]
						"20;14128500;21000;742500;14850000;13365000;Qûestions;1473536572;;A", -- [4]
					},
				},
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
				["109076"] = {
					["item:109076::::::::100:::::::"] = {
						"20;;;;3500000;3500000;Tigreess;1473605309;;A", -- [1]
						"20;;;;3500000;3500000;Tigreess;1473605306;;A", -- [2]
						"20;;;;3500000;3500000;Tigreess;1473605303;;A", -- [3]
						"20;;;;3500000;3500000;Tigreess;1473605302;;A", -- [4]
					},
				},
				["2459"] = {
					["item:2459::::::::100:::::::"] = {
						"20;;;;3900000;3900000;Schulke;1473701637;;A", -- [1]
					},
				},
				["118711"] = {
					["item:118711::::::::100:::::::"] = {
						"10;;;;2623490;2623490;Whosmyfriend;1473441253;;A", -- [1]
						"10;;;;2623490;2623490;Whosmyfriend;1473441247;;A", -- [2]
						"10;;;;2623490;2623490;Whosmyfriend;1473441242;;A", -- [3]
					},
				},
				["124461"] = {
					["item:124461::::::::100:::::::"] = {
						"1;;;;1970000;1970000;Womboscombo;1473700569;;A", -- [1]
						"1;;;;1970000;1970000;Womboscombo;1473700568;;A", -- [2]
						"1;;;;1960000;1960000;Ruffoaddy;1473700567;;A", -- [3]
						"1;;;;1960000;1960000;Ruffoaddy;1473700566;;A", -- [4]
						"1;;;;1960000;1960000;Ruffoaddy;1473700565;;A", -- [5]
						"1;;;;1960000;1960000;Ruffoaddy;1473700564;;A", -- [6]
						"1;;;;1970000;1970000;Womboscombo;1473700643;;A", -- [7]
						"1;;;;1970000;1970000;Womboscombo;1473700642;;A", -- [8]
						"1;;;;1970000;1970000;Womboscombo;1473700641;;A", -- [9]
						"1;;;;1970000;1970000;Womboscombo;1473700640;;A", -- [10]
						"1;;;;1970000;1970000;Womboscombo;1473700637;;A", -- [11]
						"1;;;;1970000;1970000;Womboscombo;1473700636;;A", -- [12]
						"1;;;;1970000;1970000;Womboscombo;1473700635;;A", -- [13]
						"1;;;;1970000;1970000;Womboscombo;1473700633;;A", -- [14]
						"1;;;;1970000;1970000;Womboscombo;1473700632;;A", -- [15]
						"1;;;;1970000;1970000;Womboscombo;1473700631;;A", -- [16]
						"1;;;;1970000;1970000;Womboscombo;1473700631;;A", -- [17]
						"1;;;;1970000;1970000;Womboscombo;1473700630;;A", -- [18]
						"1;;;;1970000;1970000;Womboscombo;1473700629;;A", -- [19]
						"1;;;;1970000;1970000;Womboscombo;1473700627;;A", -- [20]
						"1;;;;1970000;1970000;Womboscombo;1473700626;;A", -- [21]
						"1;;;;1970000;1970000;Womboscombo;1473700624;;A", -- [22]
						"1;;;;1970000;1970000;Womboscombo;1473700622;;A", -- [23]
						"1;;;;1970000;1970000;Womboscombo;1473700620;;A", -- [24]
						"1;;;;1970000;1970000;Womboscombo;1473700619;;A", -- [25]
						"1;;;;1970000;1970000;Womboscombo;1473700618;;A", -- [26]
						"1;;;;1970000;1970000;Womboscombo;1473700616;;A", -- [27]
						"1;;;;1970000;1970000;Womboscombo;1473700616;;A", -- [28]
						"1;;;;1970000;1970000;Womboscombo;1473700616;;A", -- [29]
						"1;;;;1970000;1970000;Womboscombo;1473700615;;A", -- [30]
						"1;;;;1970000;1970000;Womboscombo;1473700613;;A", -- [31]
						"1;;;;1970000;1970000;Womboscombo;1473700612;;A", -- [32]
						"1;;;;1970000;1970000;Womboscombo;1473700610;;A", -- [33]
						"1;;;;1970000;1970000;Womboscombo;1473700590;;A", -- [34]
						"1;;;;1970000;1970000;Womboscombo;1473700589;;A", -- [35]
						"1;;;;1970000;1970000;Womboscombo;1473700587;;A", -- [36]
						"1;;;;1970000;1970000;Womboscombo;1473700587;;A", -- [37]
						"1;;;;1970000;1970000;Womboscombo;1473700586;;A", -- [38]
						"1;;;;1970000;1970000;Womboscombo;1473700585;;A", -- [39]
						"1;;;;1970000;1970000;Womboscombo;1473700584;;A", -- [40]
						"1;;;;1970000;1970000;Womboscombo;1473700583;;A", -- [41]
						"1;;;;1970000;1970000;Womboscombo;1473700582;;A", -- [42]
						"1;;;;1970000;1970000;Womboscombo;1473700581;;A", -- [43]
						"1;;;;1970000;1970000;Womboscombo;1473700578;;A", -- [44]
						"1;;;;1970000;1970000;Womboscombo;1473700576;;A", -- [45]
						"1;;;;1970000;1970000;Womboscombo;1473700575;;A", -- [46]
						"1;;;;1970000;1970000;Womboscombo;1473700574;;A", -- [47]
						"1;;;;1970000;1970000;Womboscombo;1473700573;;A", -- [48]
						"1;;;;1970000;1970000;Womboscombo;1473700572;;A", -- [49]
						"1;;;;1970000;1970000;Womboscombo;1473700572;;A", -- [50]
						"1;;;;1970000;1970000;Womboscombo;1473700571;;A", -- [51]
						"1;;;;1999999;1999999;Lykwida;1473700830;;A", -- [52]
						"1;;;;1999999;1999999;Lykwida;1473700829;;A", -- [53]
						"1;;;;1995000;1995000;Angryfabio;1473700828;;A", -- [54]
						"1;;;;1995000;1995000;Angryfabio;1473700825;;A", -- [55]
						"1;;;;1995000;1995000;Angryfabio;1473700825;;A", -- [56]
						"1;;;;1995000;1995000;Angryfabio;1473700823;;A", -- [57]
						"1;;;;1995000;1995000;Angryfabio;1473700822;;A", -- [58]
						"1;;;;1995000;1995000;Angryfabio;1473700820;;A", -- [59]
						"1;;;;1995000;1995000;Angryfabio;1473700819;;A", -- [60]
						"1;;;;1995000;1995000;Angryfabio;1473700818;;A", -- [61]
						"1;;;;1995000;1995000;Angryfabio;1473700816;;A", -- [62]
						"1;;;;1995000;1995000;Angryfabio;1473700815;;A", -- [63]
						"1;;;;1995000;1995000;Angryfabio;1473700813;;A", -- [64]
						"1;;;;1995000;1995000;Angryfabio;1473700810;;A", -- [65]
						"1;;;;1995000;1995000;Angryfabio;1473700810;;A", -- [66]
						"1;;;;1995000;1995000;Angryfabio;1473700808;;A", -- [67]
						"1;;;;1995000;1995000;Angryfabio;1473700806;;A", -- [68]
						"1;;;;1995000;1995000;Angryfabio;1473700805;;A", -- [69]
						"1;;;;1995000;1995000;Angryfabio;1473700804;;A", -- [70]
						"1;;;;1995000;1995000;Angryfabio;1473700697;;A", -- [71]
						"1;;;;1995000;1995000;Angryfabio;1473700695;;A", -- [72]
						"1;;;;1995000;1995000;Angryfabio;1473700693;;A", -- [73]
						"1;;;;1995000;1995000;Angryfabio;1473700691;;A", -- [74]
						"1;;;;1995000;1995000;Angryfabio;1473700690;;A", -- [75]
						"1;;;;1995000;1995000;Angryfabio;1473700688;;A", -- [76]
						"1;;;;1995000;1995000;Angryfabio;1473700687;;A", -- [77]
						"1;;;;1995000;1995000;Angryfabio;1473700687;;A", -- [78]
						"1;;;;1995000;1995000;Angryfabio;1473700671;;A", -- [79]
						"1;;;;1995000;1995000;Angryfabio;1473700670;;A", -- [80]
						"1;;;;1995000;1995000;Angryfabio;1473700669;;A", -- [81]
						"1;;;;1995000;1995000;Angryfabio;1473700668;;A", -- [82]
						"1;;;;1995000;1995000;Angryfabio;1473700666;;A", -- [83]
						"1;;;;1995000;1995000;Angryfabio;1473700666;;A", -- [84]
						"1;;;;1995000;1995000;Angryfabio;1473700663;;A", -- [85]
						"1;;;;1995000;1995000;Angryfabio;1473700662;;A", -- [86]
						"1;;;;1995000;1995000;Angryfabio;1473700661;;A", -- [87]
						"1;;;;1995000;1995000;Angryfabio;1473700660;;A", -- [88]
						"1;;;;1995000;1995000;Angryfabio;1473700658;;A", -- [89]
						"1;;;;1995000;1995000;Angryfabio;1473700658;;A", -- [90]
						"1;;;;1995000;1995000;Angryfabio;1473700657;;A", -- [91]
						"1;;;;1995000;1995000;Angryfabio;1473700655;;A", -- [92]
						"1;;;;1995000;1995000;Angryfabio;1473700654;;A", -- [93]
						"1;;;;1995000;1995000;Angryfabio;1473700653;;A", -- [94]
						"1;;;;1995000;1995000;Angryfabio;1473700651;;A", -- [95]
						"1;;;;1995000;1995000;Angryfabio;1473700651;;A", -- [96]
						"1;;;;1995000;1995000;Angryfabio;1473700649;;A", -- [97]
						"1;;;;1995000;1995000;Angryfabio;1473700647;;A", -- [98]
						"1;;;;1995000;1995000;Angryfabio;1473700645;;A", -- [99]
						"1;;;;1995000;1995000;Angryfabio;1473700644;;A", -- [100]
						"1;;;;1995000;1995000;Angryfabio;1473700643;;A", -- [101]
						"1;;;;1999999;1999999;Lykwida;1473700885;;A", -- [102]
						"1;;;;1999999;1999999;Lykwida;1473700882;;A", -- [103]
						"1;;;;1999999;1999999;Lykwida;1473700882;;A", -- [104]
						"1;;;;1999999;1999999;Lykwida;1473700881;;A", -- [105]
						"1;;;;1999999;1999999;Lykwida;1473700881;;A", -- [106]
						"1;;;;1999999;1999999;Lykwida;1473700880;;A", -- [107]
						"1;;;;1999999;1999999;Lykwida;1473700877;;A", -- [108]
						"1;;;;1999999;1999999;Lykwida;1473700877;;A", -- [109]
						"1;;;;1999999;1999999;Lykwida;1473700875;;A", -- [110]
						"1;;;;1999999;1999999;Lykwida;1473700875;;A", -- [111]
						"1;;;;1999999;1999999;Lykwida;1473700874;;A", -- [112]
						"1;;;;1999999;1999999;Lykwida;1473700872;;A", -- [113]
						"1;;;;1999999;1999999;Lykwida;1473700872;;A", -- [114]
						"1;;;;1999999;1999999;Lykwida;1473700871;;A", -- [115]
						"1;;;;1999999;1999999;Lykwida;1473700871;;A", -- [116]
						"1;;;;1999999;1999999;Lykwida;1473700870;;A", -- [117]
						"1;;;;1999999;1999999;Lykwida;1473700870;;A", -- [118]
						"1;;;;1999999;1999999;Lykwida;1473700870;;A", -- [119]
						"1;;;;1999999;1999999;Lykwida;1473700869;;A", -- [120]
						"1;;;;1999999;1999999;Lykwida;1473700863;;A", -- [121]
						"1;;;;1999999;1999999;Lykwida;1473700862;;A", -- [122]
						"1;;;;1999999;1999999;Lykwida;1473700859;;A", -- [123]
						"1;;;;1999999;1999999;Lykwida;1473700859;;A", -- [124]
						"1;;;;1999999;1999999;Lykwida;1473700858;;A", -- [125]
						"1;;;;1999999;1999999;Lykwida;1473700857;;A", -- [126]
						"1;;;;1999999;1999999;Lykwida;1473700856;;A", -- [127]
						"1;;;;1999999;1999999;Lykwida;1473700856;;A", -- [128]
						"1;;;;1999999;1999999;Lykwida;1473700855;;A", -- [129]
						"1;;;;1999999;1999999;Lykwida;1473700854;;A", -- [130]
						"1;;;;1999999;1999999;Lykwida;1473700854;;A", -- [131]
						"1;;;;1999999;1999999;Lykwida;1473700854;;A", -- [132]
						"1;;;;1999999;1999999;Lykwida;1473700853;;A", -- [133]
						"1;;;;1999999;1999999;Lykwida;1473700849;;A", -- [134]
						"1;;;;1999999;1999999;Lykwida;1473700849;;A", -- [135]
						"1;;;;1999999;1999999;Lykwida;1473700848;;A", -- [136]
						"1;;;;1999999;1999999;Lykwida;1473700848;;A", -- [137]
						"1;;;;1999999;1999999;Lykwida;1473700848;;A", -- [138]
						"1;;;;1999999;1999999;Lykwida;1473700847;;A", -- [139]
						"1;;;;1999999;1999999;Lykwida;1473700844;;A", -- [140]
						"1;;;;1999999;1999999;Lykwida;1473700844;;A", -- [141]
						"1;;;;1999999;1999999;Lykwida;1473700843;;A", -- [142]
						"1;;;;1999999;1999999;Lykwida;1473700843;;A", -- [143]
						"1;;;;1999999;1999999;Lykwida;1473700842;;A", -- [144]
						"1;;;;1999999;1999999;Lykwida;1473700841;;A", -- [145]
						"1;;;;1999999;1999999;Lykwida;1473700841;;A", -- [146]
						"1;;;;1999999;1999999;Lykwida;1473700841;;A", -- [147]
						"1;;;;1999999;1999999;Lykwida;1473700840;;A", -- [148]
						"1;;;;1999999;1999999;Lykwida;1473700839;;A", -- [149]
						"1;;;;1999999;1999999;Lykwida;1473700838;;A", -- [150]
						"1;;;;1999999;1999999;Lykwida;1473700834;;A", -- [151]
					},
				},
				["124436"] = {
					["item:124436::::::::100:::::::"] = {
						"5;;;;265000;265000;Smallpòx;1473290504;;A", -- [1]
						"5;;;;265000;265000;Smallpòx;1473290500;;A", -- [2]
						"5;;;;265000;265000;Smallpòx;1473290485;;A", -- [3]
					},
				},
				["124444"] = {
					["item:124444::::::::100:::::::"] = {
						"1;;;;4375800;4375800;Pleuve;1473536750;;A", -- [1]
						"1;;;;4375800;4375800;Pleuve;1473536746;;A", -- [2]
					},
				},
				["136708"] = {
					["item:136708::::::::100:::::::"] = {
						"1;;;;10000000;10000000;Uthedar;1473637993;;A", -- [1]
						"1;;;;10000000;10000000;Uthedar;1473637990;;A", -- [2]
						"1;;;;10000000;10000000;Uthedar;1473637989;;A", -- [3]
						"1;;;;10000000;10000000;Uthedar;1473637988;;A", -- [4]
						"1;;;;10000000;10000000;Uthedar;1473637986;;A", -- [5]
						"1;;;;10000000;10000000;Uthedar;1473637984;;A", -- [6]
						"1;;;;24990000;23740500;Damuguaw;1473607207;;A", -- [7]
						"1;;;;24990000;23740500;Damuguaw;1473607202;;A", -- [8]
						"1;;;;24990000;23740500;Damuguaw;1473607153;;A", -- [9]
						"1;;;;24990000;23740500;Damuguaw;1473607138;;A", -- [10]
					},
				},
				["123919"] = {
					["item:123919::::::::100:::::::"] = {
						"1;;;;1170000;605120;Iluvpooc;1473725152;;A", -- [1]
						"1;;;;1170000;608820;Iluvpooc;1473725136;;A", -- [2]
						"1;;;;1170000;603820;Iluvpooc;1473725121;;A", -- [3]
						"1;;;;1170000;604820;Iluvpooc;1473725121;;A", -- [4]
						"1;;;;1170000;600820;Iluvpooc;1473725085;;A", -- [5]
						"1;;;;1170000;609310;Iluvpooc;1473725061;;A", -- [6]
						"1;;;;1170000;603820;Iluvpooc;1473725016;;A", -- [7]
						"1;;;;1170000;600820;Iluvpooc;1473725010;;A", -- [8]
						"1;;;;1170000;604820;Iluvpooc;1473725010;;A", -- [9]
						"1;;;;1170000;603820;Iluvpooc;1473724976;;A", -- [10]
						"1;;;;1170000;603820;Iluvpooc;1473724965;;A", -- [11]
						"1;;;;1170000;605610;Iluvpooc;1473724956;;A", -- [12]
						"1;;;;1170000;600820;Iluvpooc;1473724946;;A", -- [13]
						"1;;;;1170000;603120;Iluvpooc;1473724845;;A", -- [14]
						"1;;;;;591000;Vàlentìne;1473638366;;A", -- [15]
						"1;;;;;591000;Vàlentìne;1473638330;;A", -- [16]
						"1;;;;;591000;Vàlentìne;1473638326;;A", -- [17]
						"1;;;;;591000;Vàlentìne;1473638291;;A", -- [18]
						"1;;;;;537000;Vàlentìne;1473637855;;A", -- [19]
						"1;;;;;537000;Vàlentìne;1473637823;;A", -- [20]
						"1;;;;;537000;Vàlentìne;1473637809;;A", -- [21]
						"1;;;;;488000;Vàlentìne;1473637747;;A", -- [22]
						"1;;;;;488000;Vàlentìne;1473637737;;A", -- [23]
						"1;;;;;537000;Vàlentìne;1473637732;;A", -- [24]
						"1;;;;;537000;Vàlentìne;1473637726;;A", -- [25]
						"1;;;;;537000;Vàlentìne;1473637722;;A", -- [26]
						"1;;;;;537000;Vàlentìne;1473637722;;A", -- [27]
						"1;;;;;537000;Vàlentìne;1473637717;;A", -- [28]
						"1;;;;;488000;Vàlentìne;1473637692;;A", -- [29]
						"1;;;;;443000;Vàlentìne;1473637019;;A", -- [30]
						"1;;;;;443000;Vàlentìne;1473637000;;A", -- [31]
						"1;;;;;443000;Vàlentìne;1473636985;;A", -- [32]
						"1;;;;;465000;Vàlentìne;1473636964;;A", -- [33]
						"1;;;;;443000;Vàlentìne;1473636960;;A", -- [34]
						"1;;;;;443000;Vàlentìne;1473636949;;A", -- [35]
						"1;;;;;465000;Vàlentìne;1473636930;;A", -- [36]
						"1;;;;;443000;Vàlentìne;1473636915;;A", -- [37]
						"1;;;;;443000;Vàlentìne;1473636889;;A", -- [38]
						"1;;;;;443000;Vàlentìne;1473636880;;A", -- [39]
						"1;;;;;443000;Vàlentìne;1473636880;;A", -- [40]
						"1;;;;;422000;Vàlentìne;1473636737;;A", -- [41]
						"1;;;;;422000;Vàlentìne;1473636731;;A", -- [42]
						"1;;;;;443000;Vàlentìne;1473636731;;A", -- [43]
						"1;;;;;422000;Vàlentìne;1473636711;;A", -- [44]
						"1;;;;;422000;Vàlentìne;1473636711;;A", -- [45]
						"1;;;;;422000;Vàlentìne;1473636707;;A", -- [46]
						"1;;;;;422000;Vàlentìne;1473636696;;A", -- [47]
						"1;;;;;443000;Vàlentìne;1473636671;;A", -- [48]
						"1;;;;;422000;Vàlentìne;1473636656;;A", -- [49]
						"1;;;;;422000;Vàlentìne;1473636637;;A", -- [50]
						"1;;;;;422000;Vàlentìne;1473636617;;A", -- [51]
						"1;;;;;402000;Vàlentìne;1473636611;;A", -- [52]
						"1;;;;;422000;Vàlentìne;1473636577;;A", -- [53]
						"1;;;;;422000;Vàlentìne;1473636511;;A", -- [54]
						"1;;;;;402000;Vàlentìne;1473636471;;A", -- [55]
						"1;;;;;402000;Vàlentìne;1473636435;;A", -- [56]
						"1;;;;;422000;Vàlentìne;1473636411;;A", -- [57]
						"1;;;;;402000;Vàlentìne;1473636396;;A", -- [58]
						"1;;;;;422000;Vàlentìne;1473636396;;A", -- [59]
						"1;;;;;402000;Vàlentìne;1473636350;;A", -- [60]
						"1;;;;;488000;Vàlentìne;1473637634;;A", -- [61]
						"1;;;;;512000;Vàlentìne;1473637619;;A", -- [62]
						"1;;;;;488000;Vàlentìne;1473637604;;A", -- [63]
						"1;;;;;512000;Vàlentìne;1473637604;;A", -- [64]
						"1;;;;;488000;Vàlentìne;1473637600;;A", -- [65]
						"1;;;;;512000;Vàlentìne;1473637580;;A", -- [66]
						"1;;;;;488000;Vàlentìne;1473637574;;A", -- [67]
						"1;;;;;512000;Vàlentìne;1473637574;;A", -- [68]
						"1;;;;;488000;Vàlentìne;1473637570;;A", -- [69]
						"1;;;;;488000;Vàlentìne;1473637544;;A", -- [70]
						"1;;;;;488000;Vàlentìne;1473637544;;A", -- [71]
						"1;;;;;488000;Vàlentìne;1473637535;;A", -- [72]
						"1;;;;;488000;Vàlentìne;1473637529;;A", -- [73]
						"1;;;;;488000;Vàlentìne;1473637529;;A", -- [74]
						"1;;;;;488000;Vàlentìne;1473637520;;A", -- [75]
						"1;;;;;488000;Vàlentìne;1473637514;;A", -- [76]
						"1;;;;;488000;Vàlentìne;1473637510;;A", -- [77]
						"1;;;;;488000;Vàlentìne;1473637510;;A", -- [78]
						"1;;;;;488000;Vàlentìne;1473637505;;A", -- [79]
						"1;;;;;488000;Vàlentìne;1473637489;;A", -- [80]
						"1;;;;;488000;Vàlentìne;1473637474;;A", -- [81]
						"1;;;;;488000;Vàlentìne;1473637465;;A", -- [82]
						"1;;;;;488000;Vàlentìne;1473637450;;A", -- [83]
						"1;;;;;488000;Vàlentìne;1473637439;;A", -- [84]
						"1;;;;;488000;Vàlentìne;1473637435;;A", -- [85]
						"1;;;;;488000;Vàlentìne;1473637429;;A", -- [86]
						"1;;;;;488000;Vàlentìne;1473637429;;A", -- [87]
						"1;;;;;488000;Vàlentìne;1473637420;;A", -- [88]
						"1;;;;;488000;Vàlentìne;1473637414;;A", -- [89]
						"1;;;;;488000;Vàlentìne;1473637407;;A", -- [90]
						"1;;;;;488000;Vàlentìne;1473637390;;A", -- [91]
						"1;;;;;488000;Vàlentìne;1473637365;;A", -- [92]
						"1;;;;;488000;Vàlentìne;1473637360;;A", -- [93]
						"1;;;;;465000;Vàlentìne;1473637355;;A", -- [94]
						"1;;;;;488000;Vàlentìne;1473637318;;A", -- [95]
						"1;;;;;488000;Vàlentìne;1473637262;;A", -- [96]
						"1;;;;;465000;Vàlentìne;1473637257;;A", -- [97]
						"1;;;;;465000;Vàlentìne;1473637242;;A", -- [98]
						"1;;;;;465000;Vàlentìne;1473637227;;A", -- [99]
						"1;;;;;465000;Vàlentìne;1473637191;;A", -- [100]
						"1;;;;;465000;Vàlentìne;1473637157;;A", -- [101]
						"1;;;;;465000;Vàlentìne;1473637106;;A", -- [102]
						"1;;;;;465000;Vàlentìne;1473637086;;A", -- [103]
						"1;;;;;465000;Vàlentìne;1473637061;;A", -- [104]
						"1;;;;;443000;Vàlentìne;1473637040;;A", -- [105]
						"1;;;;;604875;Pattyheel;1473596866;;A", -- [106]
						"1;;;;;597125;Pattyheel;1473596866;;A", -- [107]
						"1;;;;;454965;Vorniir;1473566129;;A", -- [108]
						"1;;;;;454965;Vorniir;1473566122;;A", -- [109]
						"1;;;;230000;230000;Karalea;1473556259;;A", -- [110]
						"1;;;;230000;230000;Karalea;1473556257;;A", -- [111]
						"1;;;;230000;230000;Karalea;1473556257;;A", -- [112]
						"1;;;;230000;230000;Karalea;1473556256;;A", -- [113]
						"1;;;;230000;230000;Karalea;1473556255;;A", -- [114]
						"1;;;;230000;230000;Karalea;1473556248;;A", -- [115]
						"1;;;;680000;680000;Dontsneeze;1473555692;;A", -- [116]
						"1;;;;680000;680000;Dontsneeze;1473555691;;A", -- [117]
						"1;;;;680000;680000;Dontsneeze;1473555691;;A", -- [118]
						"1;;;;680000;680000;Dontsneeze;1473555689;;A", -- [119]
						"1;;;;680000;680000;Dontsneeze;1473555688;;A", -- [120]
						"1;;;;680000;680000;Dontsneeze;1473555685;;A", -- [121]
						"1;;;;680000;680000;Dontsneeze;1473555683;;A", -- [122]
						"1;;;;680000;680000;Dontsneeze;1473555682;;A", -- [123]
						"1;;;;680000;680000;Dontsneeze;1473555681;;A", -- [124]
						"1;;;;680000;680000;Dontsneeze;1473555680;;A", -- [125]
						"1;;;;680000;680000;Dontsneeze;1473555679;;A", -- [126]
						"1;;;;680000;680000;Dontsneeze;1473555676;;A", -- [127]
						"1;;;;680000;680000;Dontsneeze;1473555668;;A", -- [128]
						"1;;;;680000;680000;Dontsneeze;1473555666;;A", -- [129]
						"1;;;;680000;680000;Dontsneeze;1473555663;;A", -- [130]
						"1;;;;680000;680000;Dontsneeze;1473555662;;A", -- [131]
						"1;;;;680000;680000;Dontsneeze;1473555656;;A", -- [132]
					},
				},
				["123918"] = {
					["item:123918::::::::100:::::::"] = {
						"1;;;;279825;84110;Lizet;1473617197;;A", -- [1]
						"1;;;;279825;4110;Lizet;1473617182;;A", -- [2]
						"1;;;;279825;84310;Lizet;1473617173;;A", -- [3]
						"1;;;;279825;4110;Lizet;1473616096;;A", -- [4]
						"1;;;;279825;83570;Lizet;1473616092;;A", -- [5]
						"1;;;;279825;84110;Lizet;1473616077;;A", -- [6]
						"1;;;;279825;83400;Lizet;1473616022;;A", -- [7]
						"1;;;;279825;108390;Lizet;1473616007;;A", -- [8]
						"1;;;;279825;84110;Lizet;1473615977;;A", -- [9]
						"1;;;;279825;110200;Lizet;1473615936;;A", -- [10]
						"1;;;;279825;103240;Lizet;1473615872;;A", -- [11]
						"1;;;;279825;83920;Lizet;1473615842;;A", -- [12]
						"1;;;;279825;113240;Lizet;1473615821;;A", -- [13]
						"1;;;;279825;103240;Lizet;1473615782;;A", -- [14]
						"1;;;;279825;103240;Lizet;1473615746;;A", -- [15]
						"1;;;;279825;103240;Lizet;1473615736;;A", -- [16]
						"1;;;;279825;103240;Lizet;1473615727;;A", -- [17]
						"1;;;;279825;103240;Lizet;1473615691;;A", -- [18]
						"1;;;;279825;123240;Lizet;1473615686;;A", -- [19]
						"1;;;;279825;103240;Lizet;1473615682;;A", -- [20]
						"1;;;;279825;103090;Lizet;1473615586;;A", -- [21]
						"1;;;;279825;103090;Lizet;1473615562;;A", -- [22]
						"1;;;;279825;103090;Lizet;1473615556;;A", -- [23]
						"1;;;;279825;103090;Lizet;1473615522;;A", -- [24]
						"1;;;;279825;133090;Lizet;1473615516;;A", -- [25]
						"1;;;;279825;103090;Lizet;1473615477;;A", -- [26]
						"1;;;;279825;103090;Lizet;1473615466;;A", -- [27]
						"1;;;;279825;103090;Lizet;1473615462;;A", -- [28]
						"1;;;;279825;84310;Lizet;1473617023;;A", -- [29]
						"1;;;;279825;84110;Lizet;1473616943;;A", -- [30]
						"1;;;;279825;84110;Lizet;1473616882;;A", -- [31]
						"1;;;;279825;84110;Lizet;1473616882;;A", -- [32]
						"1;;;;279825;83920;Lizet;1473616821;;A", -- [33]
						"1;;;;279825;83920;Lizet;1473616802;;A", -- [34]
						"1;;;;279825;3920;Lizet;1473616791;;A", -- [35]
						"1;;;;279825;4110;Lizet;1473616782;;A", -- [36]
						"1;;;;279825;84110;Lizet;1473616772;;A", -- [37]
						"1;;;;279825;84110;Lizet;1473616757;;A", -- [38]
						"1;;;;279825;84310;Lizet;1473616736;;A", -- [39]
						"1;;;;279825;83740;Lizet;1473616701;;A", -- [40]
						"1;;;;279825;83920;Lizet;1473616701;;A", -- [41]
						"1;;;;279825;83920;Lizet;1473616682;;A", -- [42]
						"1;;;;279825;83740;Lizet;1473616637;;A", -- [43]
						"1;;;;279825;84110;Lizet;1473616637;;A", -- [44]
						"1;;;;279825;83920;Lizet;1473616631;;A", -- [45]
						"1;;;;279825;83740;Lizet;1473616612;;A", -- [46]
						"1;;;;279825;83740;Lizet;1473616567;;A", -- [47]
						"1;;;;279825;83740;Lizet;1473616547;;A", -- [48]
						"1;;;;279825;83740;Lizet;1473616547;;A", -- [49]
						"1;;;;279825;83740;Lizet;1473616541;;A", -- [50]
						"1;;;;279825;83740;Lizet;1473616526;;A", -- [51]
						"1;;;;279825;83740;Lizet;1473616516;;A", -- [52]
						"1;;;;279825;83740;Lizet;1473616501;;A", -- [53]
						"1;;;;279825;83740;Lizet;1473616492;;A", -- [54]
						"1;;;;279825;83570;Lizet;1473616451;;A", -- [55]
						"1;;;;279825;83740;Lizet;1473616451;;A", -- [56]
						"1;;;;279825;83570;Lizet;1473616417;;A", -- [57]
						"1;;;;279825;83740;Lizet;1473616402;;A", -- [58]
						"1;;;;279825;83740;Lizet;1473616381;;A", -- [59]
						"1;;;;279825;83570;Lizet;1473616381;;A", -- [60]
						"1;;;;279825;83570;Lizet;1473616377;;A", -- [61]
						"1;;;;279825;83570;Lizet;1473616362;;A", -- [62]
						"1;;;;279825;83740;Lizet;1473616357;;A", -- [63]
						"1;;;;279825;83570;Lizet;1473616312;;A", -- [64]
						"1;;;;279825;83570;Lizet;1473616312;;A", -- [65]
						"1;;;;279825;83570;Lizet;1473616296;;A", -- [66]
						"1;;;;279825;83570;Lizet;1473616296;;A", -- [67]
						"1;;;;279825;83570;Lizet;1473616291;;A", -- [68]
						"1;;;;279825;83570;Lizet;1473616221;;A", -- [69]
						"1;;;;279825;83570;Lizet;1473616216;;A", -- [70]
						"1;;;;279825;83570;Lizet;1473616212;;A", -- [71]
						"1;;;;279825;83570;Lizet;1473616182;;A", -- [72]
						"1;;;;279825;83570;Lizet;1473616176;;A", -- [73]
						"1;;;;279825;4110;Lizet;1473616167;;A", -- [74]
						"1;;;;279825;83570;Lizet;1473616157;;A", -- [75]
						"1;;;;279825;4110;Lizet;1473616146;;A", -- [76]
						"1;;;;279825;84110;Lizet;1473616131;;A", -- [77]
					},
				},
				["116268"] = {
					["item:116268::::::::100:::::::"] = {
						"20;;;;1059980;1059980;Rowynne;1473720730;;A", -- [1]
						"12;;;;635988;635988;Rowynne;1473720728;;A", -- [2]
						"19;;;;997500;997500;Aould;1473720726;;A", -- [3]
					},
				},
				["124117"] = {
					["item:124117::::::::100:::::::"] = {
						"1;;;;1495;1495;Xenos;1473534908;;A", -- [1]
						"1;;;;1495;1495;Xenos;1473534906;;A", -- [2]
						"1;;;;1495;1495;Xenos;1473534906;;A", -- [3]
						"1;;;;1495;1495;Xenos;1473534905;;A", -- [4]
						"1;;;;1495;1495;Xenos;1473534905;;A", -- [5]
						"1;;;;1495;1495;Xenos;1473534904;;A", -- [6]
						"1;;;;1495;1495;Xenos;1473534903;;A", -- [7]
						"1;;;;1495;1495;Xenos;1473534900;;A", -- [8]
						"1;;;;4797;4797;Tsilya;1473534956;;A", -- [9]
						"1;;;;4797;4797;Tsilya;1473534955;;A", -- [10]
						"1;;;;4797;4797;Tsilya;1473534955;;A", -- [11]
						"1;;;;4797;4797;Tsilya;1473534953;;A", -- [12]
						"1;;;;4797;4797;Tsilya;1473534952;;A", -- [13]
						"1;;;;4797;4797;Tsilya;1473534951;;A", -- [14]
						"1;;;;4797;4797;Tsilya;1473534951;;A", -- [15]
						"1;;;;4797;4797;Tsilya;1473534951;;A", -- [16]
						"1;;;;4797;4797;Tsilya;1473534946;;A", -- [17]
						"1;;;;4797;4797;Tsilya;1473534945;;A", -- [18]
						"1;;;;4797;4797;Tsilya;1473534943;;A", -- [19]
						"1;;;;4797;4797;Tsilya;1473534941;;A", -- [20]
						"1;;;;4797;4797;Tsilya;1473534940;;A", -- [21]
						"1;;;;3999;3999;Raiseri;1473534938;;A", -- [22]
						"1;;;;4500;4500;Askrmire;1473534936;;A", -- [23]
						"1;;;;4698;4698;Anjaneya;1473534935;;A", -- [24]
						"1;;;;4698;4698;Anjaneya;1473534934;;A", -- [25]
						"1;;;;4698;4698;Anjaneya;1473534934;;A", -- [26]
						"1;;;;4698;4698;Anjaneya;1473534933;;A", -- [27]
						"1;;;;4698;4698;Anjaneya;1473534932;;A", -- [28]
						"1;;;;4000;4000;Gohku;1473534930;;A", -- [29]
						"1;;;;4000;4000;Gohku;1473534928;;A", -- [30]
						"1;;;;3999;3999;Raiseri;1473534927;;A", -- [31]
						"1;;;;3999;3999;Raiseri;1473534927;;A", -- [32]
						"1;;;;3999;3999;Raiseri;1473534926;;A", -- [33]
						"1;;;;3999;3999;Raiseri;1473534925;;A", -- [34]
						"1;;;;3999;3999;Raiseri;1473534925;;A", -- [35]
						"1;;;;3999;3999;Raiseri;1473534924;;A", -- [36]
						"1;;;;3999;3999;Raiseri;1473534924;;A", -- [37]
						"1;;;;3999;3999;Raiseri;1473534923;;A", -- [38]
						"1;;;;3999;3999;Raiseri;1473534923;;A", -- [39]
						"1;;;;3999;3999;Raiseri;1473534922;;A", -- [40]
						"1;;;;3999;3999;Raiseri;1473534922;;A", -- [41]
						"1;;;;1495;1495;Xenos;1473534921;;A", -- [42]
						"1;;;;2250;2250;Guideng;1473534920;;A", -- [43]
						"1;;;;1495;1495;Xenos;1473534918;;A", -- [44]
						"1;;;;1495;1495;Xenos;1473534918;;A", -- [45]
						"1;;;;1495;1495;Xenos;1473534917;;A", -- [46]
						"1;;;;1495;1495;Xenos;1473534916;;A", -- [47]
						"1;;;;1495;1495;Xenos;1473534916;;A", -- [48]
						"1;;;;1495;1495;Xenos;1473534915;;A", -- [49]
						"1;;;;1495;1495;Xenos;1473534913;;A", -- [50]
						"1;;;;1495;1495;Xenos;1473534912;;A", -- [51]
						"1;;;;1495;1495;Xenos;1473534912;;A", -- [52]
						"1;;;;1495;1495;Xenos;1473534911;;A", -- [53]
						"1;;;;1495;1495;Xenos;1473534911;;A", -- [54]
						"1;;;;1495;1495;Xenos;1473534910;;A", -- [55]
						"1;;;;1495;1495;Xenos;1473534909;;A", -- [56]
						"1;;;;1495;1495;Xenos;1473534909;;A", -- [57]
						"1;;;;1495;1495;Xenos;1473534908;;A", -- [58]
						"1;;;;4797;4797;Tsilya;1473535016;;A", -- [59]
						"1;;;;4797;4797;Tsilya;1473535015;;A", -- [60]
						"1;;;;4797;4797;Tsilya;1473535015;;A", -- [61]
						"1;;;;4797;4797;Tsilya;1473535015;;A", -- [62]
						"1;;;;4797;4797;Tsilya;1473535014;;A", -- [63]
						"1;;;;4797;4797;Tsilya;1473535014;;A", -- [64]
						"1;;;;4797;4797;Tsilya;1473535013;;A", -- [65]
						"1;;;;4797;4797;Tsilya;1473535012;;A", -- [66]
						"1;;;;4797;4797;Tsilya;1473535001;;A", -- [67]
						"1;;;;4797;4797;Tsilya;1473534999;;A", -- [68]
						"1;;;;4797;4797;Tsilya;1473534999;;A", -- [69]
						"1;;;;4797;4797;Tsilya;1473534998;;A", -- [70]
						"1;;;;4797;4797;Tsilya;1473534997;;A", -- [71]
						"1;;;;4797;4797;Tsilya;1473534996;;A", -- [72]
						"1;;;;4797;4797;Tsilya;1473534993;;A", -- [73]
						"1;;;;4797;4797;Tsilya;1473534992;;A", -- [74]
						"1;;;;4797;4797;Tsilya;1473534991;;A", -- [75]
						"1;;;;4797;4797;Tsilya;1473534990;;A", -- [76]
						"1;;;;4797;4797;Tsilya;1473534989;;A", -- [77]
						"1;;;;4797;4797;Tsilya;1473534988;;A", -- [78]
						"1;;;;4797;4797;Tsilya;1473534987;;A", -- [79]
						"1;;;;4797;4797;Tsilya;1473534987;;A", -- [80]
						"1;;;;4797;4797;Tsilya;1473534987;;A", -- [81]
						"1;;;;4797;4797;Tsilya;1473534987;;A", -- [82]
						"1;;;;4797;4797;Tsilya;1473534986;;A", -- [83]
						"1;;;;4797;4797;Tsilya;1473534985;;A", -- [84]
						"1;;;;4797;4797;Tsilya;1473534985;;A", -- [85]
						"1;;;;4797;4797;Tsilya;1473534984;;A", -- [86]
						"1;;;;4797;4797;Tsilya;1473534981;;A", -- [87]
						"1;;;;4797;4797;Tsilya;1473534981;;A", -- [88]
						"1;;;;4797;4797;Tsilya;1473534979;;A", -- [89]
						"1;;;;4797;4797;Tsilya;1473534978;;A", -- [90]
						"1;;;;4797;4797;Tsilya;1473534977;;A", -- [91]
						"1;;;;4797;4797;Tsilya;1473534976;;A", -- [92]
						"1;;;;4797;4797;Tsilya;1473534975;;A", -- [93]
						"1;;;;4797;4797;Tsilya;1473534974;;A", -- [94]
						"1;;;;4797;4797;Tsilya;1473534974;;A", -- [95]
						"1;;;;4797;4797;Tsilya;1473534973;;A", -- [96]
						"1;;;;4797;4797;Tsilya;1473534973;;A", -- [97]
						"1;;;;4797;4797;Tsilya;1473534973;;A", -- [98]
						"1;;;;4797;4797;Tsilya;1473534967;;A", -- [99]
						"1;;;;4797;4797;Tsilya;1473534967;;A", -- [100]
						"1;;;;4797;4797;Tsilya;1473534966;;A", -- [101]
						"1;;;;4797;4797;Tsilya;1473534963;;A", -- [102]
						"1;;;;4797;4797;Tsilya;1473534962;;A", -- [103]
						"1;;;;4797;4797;Tsilya;1473534961;;A", -- [104]
						"1;;;;4797;4797;Tsilya;1473534958;;A", -- [105]
						"1;;;;4797;4797;Tsilya;1473534958;;A", -- [106]
						"1;;;;4797;4797;Tsilya;1473534958;;A", -- [107]
						"1;;;;4797;4797;Tsilya;1473534957;;A", -- [108]
						"199;;;;935300;935300;Punchshi;1473535396;;A", -- [109]
						"200;;;;960000;960000;Faunsama;1473535391;;A", -- [110]
						"1;;;;1485;1485;Goochii;1473535218;;A", -- [111]
						"1;;;;1485;1485;Goochii;1473535217;;A", -- [112]
						"1;;;;1485;1485;Goochii;1473535216;;A", -- [113]
						"1;;;;1485;1485;Goochii;1473535214;;A", -- [114]
						"1;;;;1485;1485;Goochii;1473535214;;A", -- [115]
						"1;;;;1485;1485;Goochii;1473535212;;A", -- [116]
						"1;;;;1485;1485;Goochii;1473535211;;A", -- [117]
						"1;;;;1485;1485;Goochii;1473535210;;A", -- [118]
						"1;;;;1485;1485;Goochii;1473535209;;A", -- [119]
						"1;;;;1485;1485;Goochii;1473535209;;A", -- [120]
						"1;;;;1485;1485;Goochii;1473535209;;A", -- [121]
						"1;;;;1485;1485;Goochii;1473535208;;A", -- [122]
						"1;;;;1485;1485;Goochii;1473535206;;A", -- [123]
						"1;;;;1485;1485;Goochii;1473535205;;A", -- [124]
						"1;;;;1485;1485;Goochii;1473535205;;A", -- [125]
						"1;;;;1485;1485;Goochii;1473535204;;A", -- [126]
						"1;;;;1485;1485;Goochii;1473535202;;A", -- [127]
						"1;;;;1485;1485;Goochii;1473535202;;A", -- [128]
						"1;;;;1485;1485;Goochii;1473535201;;A", -- [129]
						"1;;;;1485;1485;Goochii;1473535201;;A", -- [130]
						"1;;;;1485;1485;Goochii;1473535200;;A", -- [131]
						"1;;;;1485;1485;Goochii;1473535200;;A", -- [132]
						"1;;;;1485;1485;Goochii;1473535199;;A", -- [133]
						"1;;;;1485;1485;Goochii;1473535198;;A", -- [134]
						"1;;;;1485;1485;Goochii;1473535197;;A", -- [135]
						"1;;;;1485;1485;Goochii;1473535195;;A", -- [136]
						"1;;;;1485;1485;Goochii;1473535195;;A", -- [137]
						"1;;;;1485;1485;Goochii;1473535194;;A", -- [138]
						"1;;;;1485;1485;Goochii;1473535192;;A", -- [139]
						"1;;;;1485;1485;Goochii;1473535192;;A", -- [140]
						"1;;;;1485;1485;Goochii;1473535191;;A", -- [141]
						"1;;;;1485;1485;Goochii;1473535190;;A", -- [142]
						"1;;;;1485;1485;Goochii;1473535189;;A", -- [143]
						"1;;;;1485;1485;Goochii;1473535188;;A", -- [144]
						"1;;;;1485;1485;Goochii;1473535188;;A", -- [145]
						"1;;;;1485;1485;Goochii;1473535187;;A", -- [146]
						"1;;;;1485;1485;Goochii;1473535187;;A", -- [147]
						"1;;;;1485;1485;Goochii;1473535186;;A", -- [148]
						"1;;;;1485;1485;Goochii;1473535185;;A", -- [149]
						"1;;;;1485;1485;Goochii;1473535185;;A", -- [150]
						"1;;;;1485;1485;Goochii;1473535185;;A", -- [151]
						"1;;;;1485;1485;Goochii;1473535184;;A", -- [152]
						"1;;;;1485;1485;Goochii;1473535183;;A", -- [153]
						"1;;;;1485;1485;Goochii;1473535183;;A", -- [154]
						"1;;;;1485;1485;Goochii;1473535182;;A", -- [155]
						"1;;;;1485;1485;Goochii;1473535181;;A", -- [156]
						"1;;;;1485;1485;Goochii;1473535180;;A", -- [157]
						"1;;;;4797;4797;Tsilya;1473535016;;A", -- [158]
						"1;;;;1485;1485;Goochii;1473535278;;A", -- [159]
						"1;;;;1485;1485;Goochii;1473535275;;A", -- [160]
						"1;;;;1485;1485;Goochii;1473535274;;A", -- [161]
						"1;;;;1485;1485;Goochii;1473535270;;A", -- [162]
						"1;;;;1485;1485;Goochii;1473535269;;A", -- [163]
						"1;;;;1485;1485;Goochii;1473535269;;A", -- [164]
						"1;;;;1485;1485;Goochii;1473535266;;A", -- [165]
						"1;;;;1485;1485;Goochii;1473535265;;A", -- [166]
						"1;;;;1485;1485;Goochii;1473535264;;A", -- [167]
						"1;;;;1485;1485;Goochii;1473535264;;A", -- [168]
						"1;;;;1485;1485;Goochii;1473535262;;A", -- [169]
						"1;;;;1485;1485;Goochii;1473535261;;A", -- [170]
						"1;;;;1485;1485;Goochii;1473535260;;A", -- [171]
						"1;;;;1485;1485;Goochii;1473535260;;A", -- [172]
						"1;;;;1485;1485;Goochii;1473535259;;A", -- [173]
						"1;;;;1485;1485;Goochii;1473535257;;A", -- [174]
						"1;;;;1485;1485;Goochii;1473535256;;A", -- [175]
						"1;;;;1485;1485;Goochii;1473535255;;A", -- [176]
						"1;;;;1485;1485;Goochii;1473535254;;A", -- [177]
						"1;;;;1485;1485;Goochii;1473535254;;A", -- [178]
						"1;;;;1485;1485;Goochii;1473535252;;A", -- [179]
						"1;;;;1485;1485;Goochii;1473535250;;A", -- [180]
						"1;;;;1485;1485;Goochii;1473535250;;A", -- [181]
						"1;;;;1485;1485;Goochii;1473535250;;A", -- [182]
						"1;;;;1485;1485;Goochii;1473535249;;A", -- [183]
						"1;;;;1485;1485;Goochii;1473535248;;A", -- [184]
						"1;;;;1485;1485;Goochii;1473535248;;A", -- [185]
						"1;;;;1485;1485;Goochii;1473535247;;A", -- [186]
						"1;;;;1485;1485;Goochii;1473535247;;A", -- [187]
						"1;;;;1485;1485;Goochii;1473535245;;A", -- [188]
						"1;;;;1485;1485;Goochii;1473535242;;A", -- [189]
						"1;;;;1485;1485;Goochii;1473535241;;A", -- [190]
						"1;;;;1485;1485;Goochii;1473535240;;A", -- [191]
						"1;;;;1485;1485;Goochii;1473535239;;A", -- [192]
						"1;;;;1485;1485;Goochii;1473535237;;A", -- [193]
						"1;;;;1485;1485;Goochii;1473535236;;A", -- [194]
						"1;;;;1485;1485;Goochii;1473535235;;A", -- [195]
						"1;;;;1485;1485;Goochii;1473535235;;A", -- [196]
						"1;;;;1485;1485;Goochii;1473535234;;A", -- [197]
						"1;;;;1485;1485;Goochii;1473535233;;A", -- [198]
						"1;;;;1485;1485;Goochii;1473535232;;A", -- [199]
						"1;;;;1485;1485;Goochii;1473535232;;A", -- [200]
						"1;;;;1485;1485;Goochii;1473535231;;A", -- [201]
						"1;;;;1485;1485;Goochii;1473535231;;A", -- [202]
						"1;;;;1485;1485;Goochii;1473535230;;A", -- [203]
						"1;;;;1485;1485;Goochii;1473535229;;A", -- [204]
						"1;;;;1485;1485;Goochii;1473535227;;A", -- [205]
						"1;;;;1485;1485;Goochii;1473535221;;A", -- [206]
						"1;;;;1485;1485;Goochii;1473535221;;A", -- [207]
						"1;;;;1485;1485;Goochii;1473535220;;A", -- [208]
						"1;;;;4797;4797;Tsilya;1473535083;;A", -- [209]
						"1;;;;4797;4797;Tsilya;1473535083;;A", -- [210]
						"1;;;;4797;4797;Tsilya;1473535083;;A", -- [211]
						"1;;;;4797;4797;Tsilya;1473535082;;A", -- [212]
						"1;;;;4797;4797;Tsilya;1473535082;;A", -- [213]
						"1;;;;5000;5000;Armoise;1473535081;;A", -- [214]
						"1;;;;5000;5000;Armoise;1473535080;;A", -- [215]
						"1;;;;5000;5000;Armoise;1473535080;;A", -- [216]
						"1;;;;4797;4797;Tsilya;1473535077;;A", -- [217]
						"1;;;;4797;4797;Tsilya;1473535062;;A", -- [218]
						"1;;;;4797;4797;Tsilya;1473535055;;A", -- [219]
						"1;;;;4797;4797;Tsilya;1473535055;;A", -- [220]
						"1;;;;4797;4797;Tsilya;1473535054;;A", -- [221]
						"1;;;;4797;4797;Tsilya;1473535053;;A", -- [222]
						"1;;;;4797;4797;Tsilya;1473535053;;A", -- [223]
						"1;;;;4797;4797;Tsilya;1473535053;;A", -- [224]
						"1;;;;4797;4797;Tsilya;1473535051;;A", -- [225]
						"1;;;;4797;4797;Tsilya;1473535051;;A", -- [226]
						"1;;;;4797;4797;Tsilya;1473535049;;A", -- [227]
						"1;;;;4797;4797;Tsilya;1473535048;;A", -- [228]
						"1;;;;4797;4797;Tsilya;1473535045;;A", -- [229]
						"1;;;;4797;4797;Tsilya;1473535045;;A", -- [230]
						"1;;;;4797;4797;Tsilya;1473535044;;A", -- [231]
						"1;;;;4797;4797;Tsilya;1473535043;;A", -- [232]
						"1;;;;4797;4797;Tsilya;1473535043;;A", -- [233]
						"1;;;;4797;4797;Tsilya;1473535042;;A", -- [234]
						"1;;;;4797;4797;Tsilya;1473535042;;A", -- [235]
						"1;;;;4797;4797;Tsilya;1473535042;;A", -- [236]
						"1;;;;4797;4797;Tsilya;1473535039;;A", -- [237]
						"1;;;;4797;4797;Tsilya;1473535039;;A", -- [238]
						"1;;;;4797;4797;Tsilya;1473535039;;A", -- [239]
						"1;;;;4797;4797;Tsilya;1473535038;;A", -- [240]
						"1;;;;4797;4797;Tsilya;1473535037;;A", -- [241]
						"1;;;;4797;4797;Tsilya;1473535036;;A", -- [242]
						"1;;;;4797;4797;Tsilya;1473535034;;A", -- [243]
						"1;;;;4797;4797;Tsilya;1473535028;;A", -- [244]
						"1;;;;4797;4797;Tsilya;1473535027;;A", -- [245]
						"1;;;;4797;4797;Tsilya;1473535026;;A", -- [246]
						"1;;;;4797;4797;Tsilya;1473535026;;A", -- [247]
						"1;;;;4797;4797;Tsilya;1473535025;;A", -- [248]
						"1;;;;4797;4797;Tsilya;1473535024;;A", -- [249]
						"1;;;;4797;4797;Tsilya;1473535024;;A", -- [250]
						"1;;;;4797;4797;Tsilya;1473535023;;A", -- [251]
						"1;;;;4797;4797;Tsilya;1473535022;;A", -- [252]
						"1;;;;4797;4797;Tsilya;1473535022;;A", -- [253]
						"1;;;;4797;4797;Tsilya;1473535021;;A", -- [254]
						"1;;;;4797;4797;Tsilya;1473535021;;A", -- [255]
						"1;;;;4797;4797;Tsilya;1473535020;;A", -- [256]
						"1;;;;4797;4797;Tsilya;1473535019;;A", -- [257]
						"1;;;;4797;4797;Tsilya;1473535019;;A", -- [258]
					},
				},
				["124103"] = {
					["item:124103::::::::100:::::::"] = {
						"1;;;;500000;500000;Vitaesaurus;1473047005;;A", -- [1]
						"1;;;;500000;500000;Vitaesaurus;1473047001;;A", -- [2]
						"1;;;;500000;500000;Jabermeister;1473046936;;A", -- [3]
						"1;;;;500000;500000;Jabermeister;1473046934;;A", -- [4]
						"1;;;;489999;489999;Shadowpep;1473046926;;A", -- [5]
						"1;;;;500000;500000;Jabermeister;1473046923;;A", -- [6]
						"1;;;;500000;500000;Jabermeister;1473046919;;A", -- [7]
						"1;;;;489999;489999;Shadowpep;1473046915;;A", -- [8]
						"1;;;;489999;489999;Shadowpep;1473046907;;A", -- [9]
						"1;;;;489999;489999;Shadowpep;1473046900;;A", -- [10]
					},
				},
				["116266"] = {
					["item:116266::::::::100:::::::"] = {
						"5;;;;999995;999995;Lightjudge;1473701704;;A", -- [1]
						"5;;;;999995;999995;Lightjudge;1473701703;;A", -- [2]
						"5;;;;999995;999995;Lightjudge;1473701702;;A", -- [3]
						"5;;;;989980;989980;Foxmulder;1473701702;;A", -- [4]
					},
				},
			},
		},
		["Norvah"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
			},
			["postedAuctions"] = {
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
			},
			["failedBidsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
			},
			["completedAuctions"] = {
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["failedBids"] = {
			},
		},
		["Sairrus"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
			},
			["postedAuctions"] = {
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
			},
			["failedBidsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
			},
			["completedAuctions"] = {
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["failedBids"] = {
			},
		},
		["Liashta"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
			},
			["postedAuctions"] = {
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
			},
			["failedBidsNeutral"] = {
			},
			["failedBids"] = {
			},
			["completedAuctions"] = {
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
			},
		},
		["Thorrend"] = {
			["vendorsell"] = {
			},
			["postedBids"] = {
			},
			["postedAuctions"] = {
			},
			["completedBidsBuyoutsNeutral"] = {
			},
			["vendorbuy"] = {
			},
			["failedAuctions"] = {
			},
			["failedBidsNeutral"] = {
			},
			["completedBidsBuyouts"] = {
			},
			["completedAuctions"] = {
			},
			["failedAuctionsNeutral"] = {
			},
			["completedAuctionsNeutral"] = {
			},
			["failedBids"] = {
			},
		},
	},
}
BeanCounterDBSettings = {
	["configator.left"] = 279.999938964844,
	["util.beancounter.ButtonuseDateCheck"] = false,
	["columnsortcurDir"] = 1,
	["columnsortcurSort"] = 1,
	["Sargeras"] = {
		["Botann"] = {
			["tasks.sortArray"] = 1472556693,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1472556693,
			["wealth"] = 262823,
			["tasks.prunePostedDB"] = 1472556693,
			["mailbox"] = {
			},
		},
		["Thorjitsu"] = {
			["tasks.sortArray"] = 1472365802,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1472365802,
			["wealth"] = 27493733,
			["tasks.prunePostedDB"] = 1474037969,
			["mailbox"] = {
				{
					["time"] = 17.5650005340576,
					["sender"] = "Auction House",
					["subject"] = "Auction successful: Felbat Pup",
					["read"] = 2,
				}, -- [1]
				{
					["time"] = 17.5892715454102,
					["sender"] = "Auction House",
					["subject"] = "Auction expired: Felbat Pup",
					["read"] = 1,
				}, -- [2]
				{
					["time"] = 12.0625581741333,
					["sender"] = "The Postmaster",
					["subject"] = "Recovered Item",
					["read"] = 1,
				}, -- [3]
				{
					["time"] = 12.0625,
					["sender"] = "The Postmaster",
					["subject"] = "Recovered Item",
					["read"] = 1,
				}, -- [4]
			},
		},
		["Thorddin"] = {
			["tasks.sortArray"] = 1472374800,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1472374800,
			["wealth"] = 2910947204,
			["tasks.prunePostedDB"] = 1473915330,
			["mailbox"] = {
				{
					["subject"] = "Outbid on Felslate (5)",
					["read"] = 1,
					["time"] = 29.9974880218506,
					["sender"] = "Auction House",
				}, -- [1]
				{
					["subject"] = "Auction successful: Leystone Hoofplates",
					["read"] = 1,
					["time"] = 29.9445495605469,
					["sender"] = "Auction House",
				}, -- [2]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9843635559082,
					["sender"] = "Auction House",
				}, -- [3]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9841785430908,
					["sender"] = "Auction House",
				}, -- [4]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9840049743652,
					["sender"] = "Auction House",
				}, -- [5]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9840049743652,
					["sender"] = "Auction House",
				}, -- [6]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9835987091065,
					["sender"] = "Auction House",
				}, -- [7]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9833106994629,
					["sender"] = "Auction House",
				}, -- [8]
				{
					["subject"] = "Auction successful: Demonsteel Bar (10)",
					["read"] = 1,
					["time"] = 29.9412612915039,
					["sender"] = "Auction House",
				}, -- [9]
				{
					["subject"] = "Auction successful: Demonsteel Bar (10)",
					["read"] = 1,
					["time"] = 29.9412155151367,
					["sender"] = "Auction House",
				}, -- [10]
				{
					["subject"] = "Auction successful: Demonsteel Bar (10)",
					["read"] = 1,
					["time"] = 29.9411334991455,
					["sender"] = "Auction House",
				}, -- [11]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9827899932861,
					["sender"] = "Auction House",
				}, -- [12]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9827308654785,
					["sender"] = "Auction House",
				}, -- [13]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9827308654785,
					["sender"] = "Auction House",
				}, -- [14]
				{
					["subject"] = "Auction successful: Demonsteel Bar (5)",
					["read"] = 1,
					["time"] = 29.9409255981445,
					["sender"] = "Auction House",
				}, -- [15]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9823265075684,
					["sender"] = "Auction House",
				}, -- [16]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9822101593018,
					["sender"] = "Auction House",
				}, -- [17]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9821872711182,
					["sender"] = "Auction House",
				}, -- [18]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9820957183838,
					["sender"] = "Auction House",
				}, -- [19]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9819793701172,
					["sender"] = "Auction House",
				}, -- [20]
				{
					["subject"] = "Auction won: Felslate",
					["read"] = 1,
					["time"] = 29.9808216094971,
					["sender"] = "Auction House",
				}, -- [21]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9681358337402,
					["sender"] = "Auction House",
				}, -- [22]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9680557250977,
					["sender"] = "Auction House",
				}, -- [23]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9680099487305,
					["sender"] = "Auction House",
				}, -- [24]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9677886962891,
					["sender"] = "Auction House",
				}, -- [25]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9677429199219,
					["sender"] = "Auction House",
				}, -- [26]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9676856994629,
					["sender"] = "Auction House",
				}, -- [27]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9675350189209,
					["sender"] = "Auction House",
				}, -- [28]
				{
					["subject"] = "Auction successful: Leystone Hoofplates",
					["read"] = 1,
					["time"] = 29.9257411956787,
					["sender"] = "Auction House",
				}, -- [29]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9627437591553,
					["sender"] = "Auction House",
				}, -- [30]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9626846313477,
					["sender"] = "Auction House",
				}, -- [31]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9626045227051,
					["sender"] = "Auction House",
				}, -- [32]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9625587463379,
					["sender"] = "Auction House",
				}, -- [33]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9624881744385,
					["sender"] = "Auction House",
				}, -- [34]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9622097015381,
					["sender"] = "Auction House",
				}, -- [35]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9621181488037,
					["sender"] = "Auction House",
				}, -- [36]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9620609283447,
					["sender"] = "Auction House",
				}, -- [37]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9620018005371,
					["sender"] = "Auction House",
				}, -- [38]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9616775512695,
					["sender"] = "Auction House",
				}, -- [39]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9358100891113,
					["sender"] = "Auction House",
				}, -- [40]
				{
					["subject"] = "Outbid on Felslate",
					["read"] = 1,
					["time"] = 29.9357986450195,
					["sender"] = "Auction House",
				}, -- [41]
				{
					["sender"] = "Auction House",
					["read"] = 1,
					["subject"] = "Auction won: Draenic Invisibility Potion (20)",
					["time"] = 29.9998950958252,
				}, -- [42]
				{
					["sender"] = "Auction House",
					["read"] = 1,
					["subject"] = "Auction won: Draenic Invisibility Potion (12)",
					["time"] = 29.9998722076416,
				}, -- [43]
				{
					["sender"] = "Auction House",
					["read"] = 1,
					["subject"] = "Auction won: Draenic Invisibility Potion (19)",
					["time"] = 29.999849319458,
				}, -- [44]
				{
					["sender"] = "Auction House",
					["read"] = 2,
					["subject"] = "Auction cancelled: Consecrated Spike",
					["time"] = 29.999906539917,
				}, -- [45]
			},
		},
		["Norvah"] = {
			["tasks.sortArray"] = 1472558025,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1472558025,
			["wealth"] = 1094984,
			["tasks.prunePostedDB"] = 1472558025,
			["mailbox"] = {
			},
		},
		["Sairrus"] = {
			["tasks.sortArray"] = 1473232364,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1473232364,
			["wealth"] = 542471,
			["tasks.prunePostedDB"] = 1473232364,
			["mailbox"] = {
			},
		},
		["Liashta"] = {
			["tasks.sortArray"] = 1472762296,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1472762296,
			["wealth"] = 10226652,
			["tasks.prunePostedDB"] = 1472762296,
			["mailbox"] = {
			},
		},
		["Thorrend"] = {
			["tasks.sortArray"] = 1473484074,
			["version"] = 3.04,
			["faction"] = "Alliance",
			["tasks.compactDB"] = 1473484074,
			["wealth"] = 74030103,
			["tasks.prunePostedDB"] = 1473484074,
			["mailbox"] = {
			},
		},
	},
	["configator.top"] = 609,
}
BeanCounterDBNames = {
	["136924:"] = "cff0070dd;Felbat Pup",
	["128312:"] = "cff0070dd;Elixir of the Rapid Mind",
	["123919:"] = "cffffffff;Felslate",
	["124436:"] = "cffffffff;Foxflower Flux",
	["123918:"] = "cffffffff;Leystone Ore",
	["109076:"] = "cffffffff;Goblin Glider Kit",
	["141592:"] = "cff0070dd;Technique: Codex of the Tranquil Mind",
	["136683:"] = "cff0070dd;Terrorspike",
	["124461:"] = "cffffffff;Demonsteel Bar",
	["124117:"] = "cffffffff;Lean Shank",
	["124444:"] = "cff1eff00;Infernal Brimstone",
	["116268:"] = "cffffffff;Draenic Invisibility Potion",
	["136708:"] = "cffffffff;Demonsteel Stirrups",
	["124103:"] = "cffffffff;Foxflower",
	["136685:"] = "cff0070dd;Consecrated Spike",
	["118711:"] = "cffffffff;Draenic Water Walking Elixir",
	["123956:"] = "cffffffff;Leystone Hoofplates",
	["124101:"] = "cffffffff;Aethril",
	["54443:"] = "cff1eff00;Embersilk Bag",
}
BeanCounterAccountDB = {
	{
		["count"] = 0,
		["link"] = "|cffffffff|Hitem:123918::::::::110:70::::::|h[Leystone Ore]|h|r",
	}, -- [1]
}
BeanCounterMailPatch = nil
