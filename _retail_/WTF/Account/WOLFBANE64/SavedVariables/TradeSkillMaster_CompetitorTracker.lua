
TradeSkillMaster_CompetitorTrackerDB = {
	["factionrealm"] = {
		["Horde - Sargeras"] = {
			["logoutTime"] = 1531956787,
			["loginTime"] = 1531956754,
		},
		["Alliance - Sargeras"] = {
			["loginTime"] = 1536503171,
			["logoutTime"] = 1536503292,
		},
	},
	["profileKeys"] = {
		["Thorwyyn - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Traydrana - Sargeras"] = "Default",
		["Ashtali - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Thornoxnun - Sargeras"] = "Default",
		["Thorjitsu - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Botann - Sargeras"] = "Default",
		["Liashta - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Sairrus - Sargeras"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["treeGroupStatus"] = {
				["groups"] = {
					false, -- [1]
					["1"] = true,
				},
				["scrollvalue"] = 0,
				["fullwidth"] = 801,
				["selected"] = 1,
				["treesizable"] = true,
			},
		},
	},
}
