
dbBFA = {
	["char"] = {
		["Ashtali - Sargeras"] = {
			["firstun"] = false,
		},
		["Thorrend - Sargeras"] = {
			["firstun"] = false,
		},
		["Thorddin - Sargeras"] = {
			["firstun"] = false,
		},
		["Thornna - Sargeras"] = {
			["firstun"] = false,
		},
	},
	["profileKeys"] = {
		["Ashtali - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
	},
	["global"] = {
		["warn01_seen"] = 0,
		["firstrun"] = false,
		["news"] = {
		},
		["warn02_seen"] = 0,
	},
	["profiles"] = {
		["Default"] = {
			["toggles"] = {
				["ELITEOVERCAP"] = true,
				["SPARE"] = false,
				["SORTMISSION"] = "Garrison_SortMissions_Class",
				["NEVERKILLTROOPS"] = false,
				["MAKEITVERYQUICK"] = false,
				["NOBLACKLIST"] = false,
				["MOVEPANEL"] = true,
				["MAXIMIZEXP"] = true,
				["USEALLY"] = false,
				["SAVETROOPS"] = false,
				["IGNOREINACTIVE"] = true,
				["BONUS"] = true,
				["SORTMISSION2"] = "Garrison_SortMissions_Chance",
				["BONUSCHANCE"] = 100,
				["IGNORELOW"] = false,
				["NOWARN"] = false,
				["PREFERHIGH"] = false,
				["NOTROOPS"] = false,
				["MAKEITQUICK"] = false,
				["TROOPALERT"] = true,
				["MAXCHAMP"] = 3,
				["IGNOREBUSY"] = true,
				["BASECHANCE"] = 95,
			},
			["showmenu"] = true,
			["blacklist"] = {
				[2162] = true,
				[1888] = true,
				[1882] = false,
				[2144] = true,
				[1897] = true,
				[1887] = false,
				[2130] = true,
			},
		},
	},
}
