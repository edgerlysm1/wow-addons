
OneRing_Config = {
	["CharProfiles"] = {
		["Sargeras-Sairrus-3"] = "default",
	},
	["_GameVersion"] = "9.0.5",
	["_OPieVersion"] = "Xe 2a (3.104)",
	["ProfileStorage"] = {
		["default"] = {
			["ClickActivation"] = true,
			["UseDefaultBindings"] = false,
			["RotationTokens"] = {
				["ABue3Nphr22"] = "ABue3Nphr2y",
				["ABuespjWRxwn"] = "ABuespjWRxwG",
				["ABueso/2o9p"] = "ABuespjWRxwb",
				["ABuespjWRxg"] = "ABuespjWRxwk",
			},
			["Bindings"] = {
				["MainRing"] = "CTRL-BUTTON3",
				["AddOns"] = false,
			},
		},
	},
	["PersistentStorage"] = {
		["RingKeeper"] = {
			["OPieDeletedRings"] = {
				["SpecMenu"] = true,
			},
			["ialization"] = {
				{
					"specset", -- [1]
					1, -- [2]
					["sliceToken"] = "ABue3Nphr2i",
				}, -- [1]
				{
					"specset", -- [1]
					2, -- [2]
					["sliceToken"] = "ABue3Nphr2u",
				}, -- [2]
				{
					"specset", -- [1]
					3, -- [2]
					["sliceToken"] = "ABue3Nphr2y",
				}, -- [3]
				["name"] = "Specialization",
				["save"] = true,
			},
			["MainRing"] = {
				{
					"ring", -- [1]
					"AddOns", -- [2]
					["sliceToken"] = "ABueso/2o9p",
					["caption"] = "Addons",
					["rotationMode"] = "reset",
				}, -- [1]
				{
					"ring", -- [1]
					"onsumables", -- [2]
					["sliceToken"] = "ABuespaWaNa",
					["rotationMode"] = "reset",
				}, -- [2]
				{
					"ring", -- [1]
					"Travel", -- [2]
					["sliceToken"] = "ABuespjWRxg",
				}, -- [3]
				{
					"ring", -- [1]
					"Toys", -- [2]
					["sliceToken"] = "ABuespjWRxwn",
				}, -- [4]
				{
					"ring", -- [1]
					"ialization", -- [2]
					["sliceToken"] = "ABue3Nphr22",
				}, -- [5]
				["name"] = "Main Ring",
				["save"] = true,
				["hotkey"] = "F11",
			},
			["CommonTrades"] = {
				{
					["id"] = "/cast {{spell:3908/51309}}",
					["sliceToken"] = "OPCCTt",
				}, -- [1]
				{
					["id"] = "/cast {{spell:2108/51302}}",
					["sliceToken"] = "OPCCTl",
				}, -- [2]
				{
					["id"] = "/cast {{spell:2018/51300}}",
					["sliceToken"] = "OPCCTb",
				}, -- [3]
				{
					["id"] = "/cast [mod] {{spell:31252}}; {{spell:25229/51311}};",
					["sliceToken"] = "OPCCTj",
				}, -- [4]
				{
					["id"] = "/cast [mod] {{spell:13262}}; {{spell:7411/51313}}",
					["sliceToken"] = "OPCCTe",
				}, -- [5]
				{
					["id"] = "/cast {{spell:2259/51304}}",
					["sliceToken"] = "OPCCTa",
				}, -- [6]
				{
					["id"] = "/cast [mod] {{spell:818}}; {{spell:2550/51296}}",
					["sliceToken"] = "OPCCTc",
				}, -- [7]
				{
					["id"] = "/cast [mod] {{spell:51005}}; {{spell:45357/45363}}",
					["sliceToken"] = "OPCCTi",
				}, -- [8]
				{
					["id"] = "/cast {{spell:4036/51306}}",
					["sliceToken"] = "OPCCTg",
				}, -- [9]
				{
					["id"] = 53428,
					["sliceToken"] = "OPCCTu",
				}, -- [10]
				["name"] = "Trade Skills",
				["save"] = true,
				["hotkey"] = "ALT-T",
				["limit"] = "Sairrus-Sargeras",
			},
			["RaidBuffs"] = {
				{
					"item", -- [1]
					133571, -- [2]
					["sliceToken"] = "ABuesoX0=y1P",
				}, -- [1]
				{
					"item", -- [1]
					127847, -- [2]
					["sliceToken"] = "ABuesoX0=y1O",
				}, -- [2]
				{
					"item", -- [1]
					140587, -- [2]
					["sliceToken"] = "ABuesoX0=y1I",
				}, -- [3]
				{
					"item", -- [1]
					141446, -- [2]
					["sliceToken"] = "ABuespjWRxe",
				}, -- [4]
				["name"] = "Raid Buffs",
				["save"] = true,
			},
			["OPieFlagStore"] = {
				["StoreVersion"] = 2,
			},
			["Toys"] = {
				{
					"toy", -- [1]
					35275, -- [2]
					["sliceToken"] = "ABuespjWRxwH",
				}, -- [1]
				{
					"toy", -- [1]
					97919, -- [2]
					["sliceToken"] = "ABuespjWRxw0",
				}, -- [2]
				{
					"toy", -- [1]
					143727, -- [2]
					["sliceToken"] = "ABuespjWRxwG",
				}, -- [3]
				{
					"item", -- [1]
					112090, -- [2]
					["sliceToken"] = "ABuefhDczFr",
				}, -- [4]
				{
					"toy", -- [1]
					134031, -- [2]
					["sliceToken"] = "ABuehIM5UZe",
				}, -- [5]
				["name"] = "Toys",
				["save"] = true,
			},
			["AddOns"] = {
				{
					"macro", -- [1]
					"AdiBags", -- [2]
					["caption"] = "AdiBags",
					["sliceToken"] = "ABueso/Xv7g",
				}, -- [1]
				{
					"opie.databroker.launcher", -- [1]
					"|cffb4b4ffALL THE THINGS|r", -- [2]
					["caption"] = "ALL THE THINGS",
					["sliceToken"] = "ABuesp3hjex",
				}, -- [2]
				{
					"opie.databroker.launcher", -- [1]
					"Bartender4", -- [2]
					["sliceToken"] = "ABuesp3hje5",
					["caption"] = "Bartender 4",
					["clickUsingRightButton"] = true,
				}, -- [3]
				{
					"macro", -- [1]
					"Cursor Trail", -- [2]
					["caption"] = "Cursor Trail",
					["sliceToken"] = "ABueso/Xv7d",
				}, -- [4]
				{
					"macro", -- [1]
					"Deadly Boss Mods", -- [2]
					["caption"] = "Deadly Boss Mods",
					["sliceToken"] = "ABuespaWaNs",
				}, -- [5]
				{
					"macro", -- [1]
					"DialogKey", -- [2]
					["caption"] = "DialogKey",
					["sliceToken"] = "ABuespaWaN3",
				}, -- [6]
				{
					"macro", -- [1]
					"Leatrix Plus", -- [2]
					["caption"] = "Leatrix Plus",
					["sliceToken"] = "ABuespaWaNf",
				}, -- [7]
				{
					"opie.databroker.launcher", -- [1]
					"LootAppraiser", -- [2]
					["sliceToken"] = "ABuesp3hjel",
				}, -- [8]
				{
					"macro", -- [1]
					"NameplateSCT", -- [2]
					["caption"] = "NameplateSCT",
					["sliceToken"] = "ABuespfBUsr",
				}, -- [9]
				{
					"macro", -- [1]
					"OPie", -- [2]
					["caption"] = "OPie",
					["sliceToken"] = "ABueso/Xv73",
				}, -- [10]
				{
					"opie.databroker.launcher", -- [1]
					"TradeSkillMaster", -- [2]
					["caption"] = "TradeSkillMaster",
					["sliceToken"] = "ABuesp3hjez",
				}, -- [11]
				{
					"macro", -- [1]
					"XLoot", -- [2]
					["caption"] = "XLoot",
					["sliceToken"] = "ABuespjWRxw6",
				}, -- [12]
				{
					"macro", -- [1]
					"WeakAuras2", -- [2]
					["caption"] = "WeakAuras2",
					["sliceToken"] = "ABuespjWRxwb",
				}, -- [13]
				{
					"opie.databroker.launcher", -- [1]
					"X-Perl UnitFrames", -- [2]
					["caption"] = "X-Perl UnitFrames",
					["sliceToken"] = "ABuesp3hjek",
				}, -- [14]
				["offset"] = 0,
				["name"] = "AddOns",
				["save"] = true,
				["hotkey"] = "F11",
			},
			["onsumables"] = {
				{
					"item", -- [1]
					141446, -- [2]
					["sliceToken"] = "ABuespjWRxr",
				}, -- [1]
				{
					"item", -- [1]
					159867, -- [2]
					["sliceToken"] = "ABue3BOIM=g",
				}, -- [2]
				{
					"item", -- [1]
					154882, -- [2]
					["sliceToken"] = "ABuedqfX1p1",
				}, -- [3]
				{
					"item", -- [1]
					113509, -- [2]
					["sliceToken"] = "ABuedqfX1pr",
				}, -- [4]
				{
					"item", -- [1]
					152812, -- [2]
					["sliceToken"] = "ABuedaIqTGu",
				}, -- [5]
				{
					"item", -- [1]
					152813, -- [2]
					["sliceToken"] = "ABuedaIqTGy",
				}, -- [6]
				{
					"item", -- [1]
					174906, -- [2]
					["sliceToken"] = "ABue4LJ1Elg",
				}, -- [7]
				{
					"item", -- [1]
					152639, -- [2]
					["sliceToken"] = "ABuefhDczF1",
				}, -- [8]
				{
					"item", -- [1]
					162518, -- [2]
					["sliceToken"] = "ABuef4D4WEe",
				}, -- [9]
				{
					"item", -- [1]
					163765, -- [2]
					["sliceToken"] = "ABuefl0zX31",
				}, -- [10]
				{
					"item", -- [1]
					163759, -- [2]
					["caption"] = "test",
					["sliceToken"] = "ABuefl0zX3r",
				}, -- [11]
				{
					"item", -- [1]
					163784, -- [2]
					["sliceToken"] = "ABueghNDz2r",
				}, -- [12]
				{
					"item", -- [1]
					168310, -- [2]
					["sliceToken"] = "ABuegTI9Wse",
				}, -- [13]
				["name"] = "Consumables",
				["save"] = true,
			},
			["MageTravel"] = {
				{
					["id"] = "/cast [mod] {{spell:224871}}; {{spell:224869}}",
					["sliceToken"] = "OPCMPb",
				}, -- [1]
				{
					["id"] = "/cast [mod] {{spell:132620/132626}}; {{spell:132621/132627}}",
					["sliceToken"] = "OPCMPv",
				}, -- [2]
				{
					["id"] = "/cast [mod] {{spell:53142}}; {{spell:53140}}",
					["sliceToken"] = "OPCMPr",
				}, -- [3]
				{
					["id"] = "/cast [mod] {{spell:35717/33691}}; {{spell:33690}}",
					["sliceToken"] = "OPCMPs",
				}, -- [4]
				{
					["id"] = "/cast [mod] {{spell:10059}}; {{spell:3561}}",
					["sliceToken"] = "OPCMPw",
				}, -- [5]
				{
					["id"] = "/cast [mod] {{spell:11419}}; {{spell:3565}}",
					["sliceToken"] = "OPCMPd",
				}, -- [6]
				{
					["id"] = "/cast [mod] {{spell:11420}}; {{spell:3566}}",
					["sliceToken"] = "OPCMPt",
				}, -- [7]
				{
					["id"] = "/cast [mod] {{spell:11416}}; {{spell:3562}}",
					["sliceToken"] = "OPCMPi",
				}, -- [8]
				{
					["id"] = "/cast [mod] {{spell:11417}}; {{spell:3567}}",
					["sliceToken"] = "OPCMPo",
				}, -- [9]
				{
					"ring", -- [1]
					"ExtraPortals", -- [2]
					["sliceToken"] = "OPCMPe",
					["onlyNonEmpty"] = true,
				}, -- [10]
				{
					["id"] = "/cast [mod] {{spell:32267}}; {{spell:32272}}",
					["sliceToken"] = "OPCMPl",
				}, -- [11]
				{
					["id"] = "/cast [mod] {{spell:32266}}; {{spell:32271}}",
					["sliceToken"] = "OPCMPx",
				}, -- [12]
				["limit"] = "MAGE",
				["save"] = true,
				["internal"] = true,
				["hotkey"] = "ALT-G",
				["name"] = "Portals and Teleports",
			},
			["Travel"] = {
				{
					"toy", -- [1]
					172179, -- [2]
					["sliceToken"] = "ABuehWLj9Lr",
				}, -- [1]
				{
					"item", -- [1]
					140192, -- [2]
					["sliceToken"] = "ABuespjWRxd",
				}, -- [2]
				{
					["id"] = "/cast [mod] {{spell:10059}}; {{spell:3561}}\n",
					["sliceToken"] = "ABuespjWRx4",
				}, -- [3]
				{
					["id"] = "/cast [mod] {{spell:11416}}; {{spell:3562}}",
					["sliceToken"] = "ABuespjWRxc",
				}, -- [4]
				{
					["id"] = "/cast [mod] {{spell:11419}}; {{spell:3565}}",
					["sliceToken"] = "ABuespjWRxx",
				}, -- [5]
				{
					["id"] = "/cast [mod] {{spell:32266}}; {{spell:32271}}",
					["sliceToken"] = "ABuespjWRxwj",
				}, -- [6]
				{
					["id"] = "/cast [mod] {{spell:49360}}; {{spell:49359}}",
					["sliceToken"] = "ABuespjWRxw4",
				}, -- [7]
				{
					["id"] = "/cast [mod] {{spell:33691}}; {{spell:33690}}",
					["sliceToken"] = "ABuespjWRxwh",
				}, -- [8]
				{
					["id"] = "/cast [mod] {{spell:53142}}; {{spell:53140}}",
					["sliceToken"] = "ABuespjWRxwg",
				}, -- [9]
				{
					["id"] = "/cast [mod] {{spell:88345}}; {{spell:88342}}",
					["sliceToken"] = "ABuespjWRxwf",
				}, -- [10]
				{
					["id"] = "/cast [mod] {{spell:132620}}; {{spell:132621}}",
					["sliceToken"] = "ABuespjWRxwd",
				}, -- [11]
				{
					["id"] = "/cast [mod] {{spell:224871}}; {{spell:224869}}",
					["sliceToken"] = "ABuespjWRxw3",
				}, -- [12]
				{
					["id"] = 193759,
					["sliceToken"] = "ABuespjWRxwk",
				}, -- [13]
				{
					"item", -- [1]
					141605, -- [2]
					["sliceToken"] = "ABuespjWRxwJ",
				}, -- [14]
				["name"] = "Travel",
				["save"] = true,
			},
		},
	},
	["_GameLocale"] = "enUS",
}
OPie_SavedData = nil
