
GoldSave = {
	["Thorjitsu_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thorjitsu",
		["gold"] = 53606392,
	},
	["Thorddin_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thorddin",
		["gold"] = 34073240,
	},
	["Thordaris_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thordaris",
		["gold"] = 71154730,
	},
	["Iamthorr_Sargeras::Neutral"] = {
		["show"] = true,
		["name"] = "Iamthorr",
		["gold"] = 580,
	},
	["Sairrus_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Sairrus",
		["gold"] = 54642935,
	},
	["Sneakythorr_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Sneakythorr",
		["gold"] = 44456083,
	},
	["Senithor_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Senithor",
		["gold"] = 974547,
	},
	["Thormonde_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thormonde",
		["gold"] = 50328822,
	},
	["Rosselis_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Rosselis",
		["gold"] = 10000,
	},
	["Thorraddin_Sargeras::Horde"] = {
		["show"] = true,
		["name"] = "Thorraddin",
		["gold"] = 276521907,
	},
	["Thornna_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thornna",
		["gold"] = 55688123,
	},
	["Thordren_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thordren",
		["gold"] = 22574343,
	},
	["Thorwyyn_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thorwyyn",
		["gold"] = 642147,
	},
	["Thorpez_Sargeras::Horde"] = {
		["show"] = true,
		["name"] = "Thorpez",
		["gold"] = 1397538,
	},
	["Ashtali_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Ashtali",
		["gold"] = 39092138,
	},
	["Thorlexi_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thorlexi",
		["gold"] = 8114,
	},
	["Liashta_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Liashta",
		["gold"] = 10226652,
	},
	["Aatala_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Aatala",
		["gold"] = 10000,
	},
	["Norvah_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Norvah",
		["gold"] = 1065731,
	},
	["Thorwin_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thorwin",
		["gold"] = 11799158,
	},
	["Thorrend_Sargeras::Alliance"] = {
		["show"] = true,
		["name"] = "Thorrend",
		["gold"] = 37114212,
	},
}
