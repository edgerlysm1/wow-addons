
MethodAltManagerDB = {
	["alts"] = 1,
	["data"] = {
		["Player-76-081379BF"] = {
			["stygia"] = 4833,
			["guid"] = "Player-76-081379BF",
			["run_history"] = {
			},
			["dungeon"] = "Unknown",
			["conquest_earned"] = 4141,
			["stored_anima"] = 3336,
			["ilevel"] = 222.25,
			["level"] = "?",
			["time_until_reset"] = 41296,
			["worldboss"] = false,
			["charlevel"] = 60,
			["max_conduit_charges"] = 10,
			["nathria_mythic"] = 7,
			["expires"] = 1620745199,
			["data_obtained"] = 1620358303,
			["renown"] = 39,
			["name"] = "Thormonde",
			["conquest_total"] = 616,
			["soul_ash"] = 890,
			["class"] = "PRIEST",
			["conduit_charges"] = 10,
		},
	},
}
