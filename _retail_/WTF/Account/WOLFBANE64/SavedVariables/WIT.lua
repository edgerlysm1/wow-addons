
WITDB = {
	["Version"] = "v3.1.2",
	["Settings"] = {
		["MinimapIcon"] = {
		},
		["RecorderMinPrice"] = 0,
		["BagValueMinQuality"] = 1,
		["Modules"] = {
			["Dashboard"] = {
				["ShowCurrentContent"] = true,
				["Farms"] = {
				},
			},
		},
		["PricingSelect"] = "DBMarket",
		["BagValueMinPrice"] = 0,
		["RecorderMinQuality"] = 1,
		["BelowTresholdValue"] = 1,
		["LegacyPricingSelect"] = "DBMarket",
	},
	["UserData"] = {
		["Farms"] = {
		},
	},
}
