
Postal3DB = {
	["profileKeys"] = {
		["Thorjitsu - Sargeras"] = "Thorjitsu - Sargeras",
		["Liashta - Sargeras"] = "Liashta - Sargeras",
		["Thorddin - Sargeras"] = "Thorddin - Sargeras",
		["Botann - Sargeras"] = "Botann - Sargeras",
		["Norvah - Sargeras"] = "Norvah - Sargeras",
		["Sairrus - Sargeras"] = "Sairrus - Sargeras",
		["Thorrend - Sargeras"] = "Thorrend - Sargeras",
	},
	["global"] = {
		["BlackBook"] = {
			["alts"] = {
				"Botann|Sargeras|Alliance|68|ROGUE", -- [1]
				"Liashta|Sargeras|Alliance|70|DEATHKNIGHT", -- [2]
				"Norvah|Sargeras|Alliance|70|PRIEST", -- [3]
				"Sairrus|Sargeras|Alliance|100|MAGE", -- [4]
				"Thorddin|Sargeras|Alliance|110|PALADIN", -- [5]
				"Thorjitsu|Sargeras|Alliance|100|MONK", -- [6]
				"Thorrend|Sargeras|Alliance|100|WARRIOR", -- [7]
			},
		},
	},
	["profiles"] = {
		["Thorjitsu - Sargeras"] = {
			["BlackBook"] = {
				["recent"] = {
					"Grizzledwarf|Sargeras|Alliance", -- [1]
				},
			},
		},
		["Liashta - Sargeras"] = {
		},
		["Thorddin - Sargeras"] = {
			["BlackBook"] = {
				["recent"] = {
					"Thorjitsu|Sargeras|Alliance", -- [1]
					"Cacophobia|Sargeras|Alliance", -- [2]
					"Kateshubby|Sargeras|Alliance", -- [3]
					"Liashta|Sargeras|Alliance", -- [4]
				},
			},
		},
		["Botann - Sargeras"] = {
		},
		["Norvah - Sargeras"] = {
		},
		["Sairrus - Sargeras"] = {
		},
		["Thorrend - Sargeras"] = {
		},
	},
}
