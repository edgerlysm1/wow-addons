
MSBTProfiles_SavedVars = {
	["profiles"] = {
		["Default"] = {
			["hideNames"] = true,
			["scrollAreas"] = {
				["Incoming"] = {
					["stickyDirection"] = "Down",
					["stickyTextAlignIndex"] = 2,
					["offsetX"] = -95,
					["direction"] = "Up",
					["behavior"] = "MSBT_NORMAL",
					["offsetY"] = -132,
					["animationStyle"] = "Straight",
					["scrollHeight"] = 125,
				},
				["Custom2"] = {
					["scrollHeight"] = 95,
					["name"] = "Incoming (Healing)",
					["offsetY"] = -98,
					["textAlignIndex"] = 1,
					["offsetX"] = -164,
				},
				["Notification"] = {
					["offsetY"] = -115,
					["disabled"] = true,
					["offsetX"] = -603,
				},
				["Static"] = {
					["offsetX"] = -405,
					["offsetY"] = -91,
				},
				["Custom1"] = {
					["stickyDirection"] = "Down",
					["scrollHeight"] = 95,
					["name"] = "Outgoing (Crits)",
					["stickyBehavior"] = "Jiggle",
					["offsetY"] = -106,
					["offsetX"] = 98,
				},
				["Outgoing"] = {
					["critFontAlpha"] = 1,
					["behavior"] = "GrowDown",
					["stickyDirection"] = "Up",
					["scrollWidth"] = 150,
					["offsetX"] = 71,
					["scrollHeight"] = 50,
					["offsetY"] = -136,
					["animationStyle"] = "Horizontal",
					["direction"] = "Right",
				},
			},
			["throttleList"] = {
				["Mind Sear"] = 4,
				["Vampiric Embrace"] = false,
				["Mind Flay"] = 4,
				["Atonement"] = 3,
				["Vampiric Touch"] = false,
			},
			["abilitySuppressions"] = {
				["Instant Poison"] = true,
				["Main Gauche"] = true,
				["Shadow Word: Pain"] = true,
				["Ancient Flame"] = true,
				["Pet"] = true,
				["Shadowy Apparition"] = true,
			},
			["hotThrottleDuration"] = 0,
			["critOutlineIndex"] = 2,
			["hideFullHoTOverheals"] = false,
			["normalOutlineIndex"] = 2,
			["dotThrottleDuration"] = 0,
			["creationVersion"] = "5.7.146",
			["critFontSize"] = 30,
			["animationSpeed"] = 80,
			["events"] = {
				["INCOMING_SPELL_DOT_CRIT"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_PARRY"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DAMAGE"] = {
					["disabled"] = true,
				},
				["OUTGOING_MISS"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DEFLECT"] = {
					["disabled"] = true,
				},
				["SELF_HOT"] = {
					["scrollArea"] = "Custom2",
				},
				["OUTGOING_SPELL_DAMAGE_CRIT"] = {
					["scrollArea"] = "Custom1",
				},
				["OUTGOING_HEAL_CRIT"] = {
					["scrollArea"] = "Custom2",
				},
				["INCOMING_HEAL"] = {
					["scrollArea"] = "Custom2",
				},
				["OUTGOING_HEAL"] = {
					["scrollArea"] = "Custom2",
				},
				["PET_OUTGOING_SPELL_ABSORB"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_MISS"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_HOT_CRIT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_EVADE"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_BLOCK"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_HEAL"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_ABSORB"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_MISS"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_HEAL_CRIT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_RESIST"] = {
					["disabled"] = true,
				},
				["OUTGOING_DODGE"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_IMMUNE"] = {
					["disabled"] = true,
				},
				["INCOMING_ABSORB"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_PARRY"] = {
					["disabled"] = true,
				},
				["SELF_HEAL_CRIT"] = {
					["scrollArea"] = "Custom2",
				},
				["OUTGOING_PARRY"] = {
					["disabled"] = true,
				},
				["OUTGOING_HOT_CRIT"] = {
					["scrollArea"] = "Custom2",
				},
				["INCOMING_SPELL_DAMAGE_SHIELD_CRIT"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_REFLECT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_DEFLECT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DAMAGE_SHIELD"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_MISS"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_HOT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_IMMUNE"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DAMAGE_SHIELD_CRIT"] = {
					["disabled"] = true,
				},
				["OUTGOING_DAMAGE_CRIT"] = {
					["disabled"] = true,
				},
				["OUTGOING_DAMAGE"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_DODGE"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_DODGE"] = {
					["disabled"] = true,
				},
				["OUTGOING_ABSORB"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_BLOCK"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_DISPEL"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DOT"] = {
					["disabled"] = true,
				},
				["NOTIFICATION_EXPERIENCE_GAIN"] = {
					["disabled"] = false,
					["scrollArea"] = "Custom2",
				},
				["INCOMING_IMMUNE"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_DOT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_DAMAGE"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_BLOCK"] = {
					["disabled"] = true,
				},
				["NOTIFICATION_PET_COOLDOWN"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_DAMAGE_CRIT"] = {
					["disabled"] = true,
				},
				["NOTIFICATION_ITEM_COOLDOWN"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_DEFLECT"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_INTERRUPT"] = {
					["disabled"] = true,
				},
				["OUTGOING_DEFLECT"] = {
					["disabled"] = true,
				},
				["SELF_HOT_CRIT"] = {
					["scrollArea"] = "Custom2",
				},
				["OUTGOING_IMMUNE"] = {
					["disabled"] = true,
				},
				["OUTGOING_HOT"] = {
					["scrollArea"] = "Custom2",
				},
				["OUTGOING_BLOCK"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_IMMUNE"] = {
					["disabled"] = true,
				},
				["NOTIFICATION_COOLDOWN"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DODGE"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_DAMAGE_SHIELD"] = {
					["disabled"] = true,
				},
				["INCOMING_ENVIRONMENTAL"] = {
					["disabled"] = true,
				},
				["INCOMING_SPELL_RESIST"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DAMAGE_CRIT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_DOT_CRIT"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_ABSORB"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_SPELL_EVADE"] = {
					["disabled"] = true,
				},
				["PET_OUTGOING_PARRY"] = {
					["disabled"] = true,
				},
				["SELF_HEAL"] = {
					["scrollArea"] = "Custom2",
				},
			},
			["hideSkills"] = true,
			["groupNumbers"] = true,
			["normalFontName"] = "2002",
			["critFontName"] = "2002",
			["normalFontSize"] = 22,
		},
	},
	["userDisabled"] = true,
}
MSBT_SavedMedia = {
	["fonts"] = {
	},
	["sounds"] = {
	},
}
