
TradeSkillMaster_VendorDB = {
	["char"] = {
		["Sairrus - Sargeras"] = {
			["migrated"] = true,
		},
		["Thordren - Sargeras"] = {
			["migrated"] = true,
		},
		["Traydrana - Sargeras"] = {
			["migrated"] = true,
		},
		["Tralerodora - Illidan"] = {
			["migrated"] = true,
		},
		["Norial - Illidan"] = {
			["migrated"] = true,
		},
		["Thorrsmash - Sargeras"] = {
			["migrated"] = true,
		},
		["Thorjitsu - Sargeras"] = {
			["migrated"] = true,
		},
		["Thorrend - Sargeras"] = {
			["migrated"] = true,
		},
		["Thornna - Sargeras"] = {
			["migrated"] = true,
		},
		["Botann - Sargeras"] = {
			["migrated"] = true,
		},
		["Thorwin - Sargeras"] = {
			["migrated"] = true,
		},
		["Thorend - Illidan"] = {
			["migrated"] = true,
		},
		["Thorddin - Sargeras"] = {
			["migrated"] = true,
		},
	},
	["profileKeys"] = {
		["Sairrus - Sargeras"] = "Default",
		["Thordren - Sargeras"] = "Default",
		["Traydrana - Sargeras"] = "Default",
		["Tralerodora - Illidan"] = "Default",
		["Norial - Illidan"] = "Default",
		["Thorrsmash - Sargeras"] = "Default",
		["Thorjitsu - Sargeras"] = "Default",
		["Thorrend - Sargeras"] = "Default",
		["Thornna - Sargeras"] = "Default",
		["Botann - Sargeras"] = "Default",
		["Thorwin - Sargeras"] = "Default",
		["Thorend - Illidan"] = "Default",
		["Thorddin - Sargeras"] = "Default",
	},
}
