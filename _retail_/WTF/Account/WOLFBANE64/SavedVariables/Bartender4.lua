
Bartender4DB = {
	["namespaces"] = {
		["StatusTrackingBar"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Thorwyyn - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
					},
				},
				["Thornarii - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Basic UI PC"] = {
					["enabled"] = true,
					["position"] = {
						["y"] = 26,
						["x"] = -446.0500164649711,
						["point"] = "BOTTOM",
						["scale"] = 1.100000023841858,
					},
					["version"] = 3,
				},
				["Iamthorr - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
					},
				},
				["Thormonde - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
					},
				},
				["Thorpez - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Default"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
					},
				},
				["Rosselis - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Aatala - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
					},
				},
				["Anthienisse - Illidan"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Norvah - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Odintsun - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Marraydel - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Senithor - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["scale"] = 1.264999985694885,
						["x"] = -515,
						["point"] = "BOTTOM",
						["y"] = 62,
					},
				},
				["Thorpawz - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
					},
				},
			},
		},
		["ActionBars"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = 188.5000457763672,
								["x"] = -231.4999694824219,
								["point"] = "BOTTOM",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Senithor - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Thorpez - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Odintsun - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Thorpawz - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Basic UI PC"] = {
					["actionbars"] = {
						{
							["showgrid"] = true,
							["rows"] = 4,
							["version"] = 3,
							["position"] = {
								["y"] = 142.37060546875,
								["x"] = -91.382568359375,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.5,
							},
							["visibility"] = {
								["always"] = false,
							},
							["states"] = {
								["ctrl"] = 9,
								["shift"] = 2,
							},
						}, -- [1]
						{
							["showgrid"] = true,
							["rows"] = 4,
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -74.4999694824219,
								["x"] = -309.000610351563,
								["point"] = "RIGHT",
							},
							["visibility"] = {
								["always"] = false,
								["overridebar"] = false,
								["vehicleui"] = false,
							},
						}, -- [2]
						{
							["showgrid"] = true,
							["rows"] = 3,
							["version"] = 3,
							["position"] = {
								["y"] = 126.37060546875,
								["x"] = -194.382690429688,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.5,
							},
							["padding"] = 5,
							["visibility"] = {
								["always"] = false,
								["overridebar"] = false,
								["vehicleui"] = false,
							},
							["states"] = {
								["enabled"] = true,
								["shift"] = 8,
							},
						}, -- [3]
						{
							["showgrid"] = true,
							["rows"] = 3,
							["buttons"] = 3,
							["version"] = 3,
							["position"] = {
								["y"] = 126.3706359863281,
								["x"] = -112.882568359375,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.5,
							},
							["padding"] = 5,
							["visibility"] = {
								["always"] = false,
								["overridebar"] = false,
								["vehicleui"] = false,
							},
							["states"] = {
								["enabled"] = true,
								["shift"] = 5,
							},
						}, -- [4]
						{
							["hideequipped"] = true,
							["version"] = 3,
							["position"] = {
								["y"] = 44.00000381469727,
								["x"] = 617.7593383789062,
								["point"] = "BOTTOM",
							},
							["padding"] = 6,
							["visibility"] = {
								["nopet"] = false,
								["customdata"] = "[petbattle]hide;[nopet]hide;show",
								["overridebar"] = false,
								["always"] = true,
								["vehicleui"] = false,
							},
						}, -- [5]
						{
							["showgrid"] = true,
							["rows"] = 2,
							["version"] = 3,
							["position"] = {
								["y"] = 104.6576018125197,
								["x"] = -95.62505258742749,
								["point"] = "BOTTOM",
								["scale"] = 0.8500000238418579,
							},
							["padding"] = 0,
							["visibility"] = {
								["always"] = false,
							},
						}, -- [6]
						{
							["showgrid"] = true,
							["rows"] = 2,
							["enabled"] = true,
							["version"] = 3,
							["position"] = {
								["y"] = 65.4500212907797,
								["x"] = -187.849947947616,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.850000023841858,
							},
							["padding"] = 0,
						}, -- [7]
						{
							["rows"] = 3,
							["enabled"] = true,
							["buttons"] = 3,
							["version"] = 3,
							["position"] = {
								["y"] = 123.80004119873,
								["x"] = -250.984497070313,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.5,
							},
						}, -- [8]
						{
							["version"] = 3,
							["position"] = {
								["y"] = 174.428619384766,
								["x"] = -165.214538574219,
								["point"] = "CENTER",
							},
						}, -- [9]
						{
							["rows"] = 3,
							["buttons"] = 3,
							["version"] = 3,
							["position"] = {
								["y"] = 123.80004119873,
								["x"] = -274.38330078125,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.5,
							},
						}, -- [10]
					},
				},
				["Iamthorr - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = 188.5000457763672,
								["x"] = -231.4999694824219,
								["point"] = "BOTTOM",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Thornarii - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Thorwyyn - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Marraydel - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = 188.5000457763672,
								["x"] = -231.4999694824219,
								["point"] = "BOTTOM",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Norvah - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = 188.5000457763672,
								["x"] = -231.4999694824219,
								["point"] = "BOTTOM",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Aatala - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Rosselis - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["actionbars"] = {
						{
							["version"] = 3,
							["showgrid"] = true,
							["rows"] = 4,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = 37.4998626708984,
								["x"] = -309.000610351563,
								["point"] = "RIGHT",
							},
						}, -- [1]
						{
							["version"] = 3,
							["showgrid"] = true,
							["rows"] = 4,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = -116.500183105469,
								["x"] = -309.000610351563,
								["point"] = "RIGHT",
							},
						}, -- [2]
						{
							["position"] = {
								["y"] = 172.500122070313,
								["x"] = -1.50000751018524,
								["point"] = "LEFT",
							},
							["version"] = 3,
							["showgrid"] = true,
							["rows"] = 2,
							["padding"] = 5,
							["visibility"] = {
								["always"] = false,
							},
							["buttons"] = 8,
						}, -- [3]
						{
							["position"] = {
								["y"] = 96.5001068115235,
								["x"] = 164.5,
								["point"] = "LEFT",
							},
							["version"] = 3,
							["showgrid"] = true,
							["rows"] = 2,
							["padding"] = 5,
							["visibility"] = {
								["always"] = false,
							},
							["buttons"] = 4,
						}, -- [4]
						{
							["version"] = 3,
							["showgrid"] = true,
							["buttons"] = 2,
							["padding"] = 6,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = -15.4999084472656,
								["x"] = 163.999847412109,
								["point"] = "LEFT",
							},
						}, -- [5]
						{
							["version"] = 3,
							["showgrid"] = true,
							["position"] = {
								["y"] = 121,
								["x"] = 329,
								["point"] = "BOTTOM",
								["scale"] = 0.85,
							},
							["padding"] = 0,
							["visibility"] = {
								["always"] = false,
							},
							["rows"] = 2,
						}, -- [6]
						{
							["showgrid"] = true,
							["buttons"] = 1,
							["version"] = 3,
							["position"] = {
								["y"] = -127.499862670898,
								["x"] = -88.5002136230469,
								["point"] = "CENTER",
							},
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Anthienisse - Illidan"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = 188.5000457763672,
								["x"] = -231.4999694824219,
								["point"] = "BOTTOM",
							},
						}, -- [2]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["rows"] = 12,
							["padding"] = 5,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
				["Basic UI"] = {
					["actionbars"] = {
						{
							["version"] = 3,
							["showgrid"] = true,
							["rows"] = 4,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = -115.087280273438,
								["x"] = 274.988189697266,
								["point"] = "CENTER",
							},
						}, -- [1]
						{
							["version"] = 3,
							["showgrid"] = true,
							["rows"] = 4,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = -116.500183105469,
								["x"] = -309.000610351563,
								["point"] = "RIGHT",
							},
						}, -- [2]
						{
							["showgrid"] = true,
							["version"] = 3,
							["position"] = {
								["y"] = 172.500122070313,
								["x"] = -1.50000751018524,
								["point"] = "LEFT",
							},
							["rows"] = 3,
							["padding"] = 5,
							["visibility"] = {
								["always"] = false,
							},
							["states"] = {
								["enabled"] = true,
								["shift"] = 8,
							},
						}, -- [3]
						{
							["showgrid"] = true,
							["version"] = 3,
							["buttons"] = 4,
							["rows"] = 2,
							["padding"] = 5,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = 5.49993896484375,
								["x"] = 189.405838012695,
								["point"] = "LEFT",
							},
						}, -- [4]
						{
							["version"] = 3,
							["showgrid"] = true,
							["buttons"] = 2,
							["padding"] = 6,
							["visibility"] = {
								["always"] = false,
							},
							["position"] = {
								["y"] = 67.9614715576172,
								["x"] = 187.759170532227,
								["point"] = "LEFT",
							},
						}, -- [5]
						{
							["version"] = 3,
							["showgrid"] = true,
							["position"] = {
								["y"] = 121,
								["x"] = 329,
								["point"] = "BOTTOM",
								["scale"] = 0.85,
							},
							["padding"] = 0,
							["visibility"] = {
								["always"] = false,
							},
							["rows"] = 2,
						}, -- [6]
						{
							["showgrid"] = true,
							["buttons"] = 1,
							["version"] = 3,
							["position"] = {
								["y"] = -127.499862670898,
								["x"] = -88.5002136230469,
								["point"] = "CENTER",
							},
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Default"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 102,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 102,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
				["Thormonde - Sargeras"] = {
					["actionbars"] = {
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 41.75,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["y"] = -227.5000457763672,
								["x"] = -231.5001220703125,
								["point"] = "CENTER",
							},
						}, -- [2]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [3]
						{
							["padding"] = 5,
							["rows"] = 12,
							["version"] = 3,
							["position"] = {
								["y"] = 610,
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
							},
						}, -- [4]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
							},
						}, -- [5]
						{
							["padding"] = 6,
							["version"] = 3,
							["position"] = {
								["y"] = 110,
								["x"] = -510,
								["point"] = "BOTTOM",
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						nil, -- [9]
						{
						}, -- [10]
					},
				},
			},
		},
		["LibDualSpec-1.0"] = {
		},
		["ExtraActionBar"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Senithor - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000305175781,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Thorpez - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000305175781,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Odintsun - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 287.0000610351563,
						["x"] = -63.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Thorpawz - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 287.0000610351563,
						["x"] = -63.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC"] = {
					["position"] = {
						["y"] = -81.89253234863281,
						["x"] = 139.8485717773438,
						["point"] = "CENTER",
					},
					["version"] = 3,
				},
				["Iamthorr - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Thornarii - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 287.0000610351563,
						["x"] = -63.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000305175781,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Marraydel - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Norvah - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Aatala - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000305175781,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Rosselis - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000305175781,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -169.500007629395,
						["x"] = 164.499786376953,
						["point"] = "CENTER",
					},
				},
				["Anthienisse - Illidan"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Basic UI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -169.500007629395,
						["x"] = 164.499786376953,
						["point"] = "CENTER",
					},
				},
				["Default"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 287.0000610351563,
						["x"] = -63.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Thormonde - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 287.0000610351563,
						["x"] = -63.50006103515625,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["ZoneAbilityBar"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000610351563,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 121.300048828125,
						["x"] = 321.232055664063,
						["point"] = "BOTTOM",
					},
				},
				["Iamthorr - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Thorpez - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000610351563,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Anthienisse - Illidan"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Default"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -16.39326477050781,
						["x"] = 373.6192016601563,
						["point"] = "CENTER",
					},
				},
				["Aatala - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000610351563,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Rosselis - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000610351563,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -160.500030517578,
						["x"] = -31.5000610351563,
						["point"] = "CENTER",
					},
				},
				["Marraydel - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Basic UI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -160.500030517578,
						["x"] = -31.5000610351563,
						["point"] = "CENTER",
					},
				},
				["Norvah - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -170.4999618530273,
						["x"] = -31.5,
						["point"] = "CENTER",
					},
				},
				["Senithor - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 223.0000610351563,
						["x"] = -31.50006103515625,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["MicroMenu"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Senithor - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Thorpez - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Odintsun - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Thorpawz - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["y"] = 41.75,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
				["Basic UI PC"] = {
					["enabled"] = false,
					["fadeoutalpha"] = 0.35,
					["rows"] = 11,
					["fadeout"] = true,
					["position"] = {
						["y"] = -126.9977111816406,
						["x"] = -31.999267578125,
						["point"] = "RIGHT",
						["scale"] = 1,
					},
					["fadeoutdelay"] = 0.1,
					["padding"] = -2,
					["version"] = 3,
				},
				["Iamthorr - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["y"] = 41.75,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
				["Thornarii - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Thorwyyn - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["y"] = 41.75,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
				["Marraydel - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Norvah - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Aatala - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["y"] = 41.75,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
				["Rosselis - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["fadeoutdelay"] = 0.1,
					["fadeoutalpha"] = 0.35,
					["position"] = {
						["y"] = 164,
						["x"] = -162,
						["point"] = "BOTTOMRIGHT",
						["scale"] = 1,
					},
					["version"] = 3,
					["padding"] = -2,
					["rows"] = 2,
					["fadeout"] = true,
				},
				["Anthienisse - Illidan"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["scale"] = 1,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["y"] = 41.75,
					},
				},
				["Basic UI"] = {
					["fadeout"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 164,
						["x"] = -162,
						["point"] = "BOTTOMRIGHT",
						["scale"] = 1,
					},
					["fadeoutdelay"] = 0.1,
					["padding"] = -2,
					["rows"] = 2,
					["fadeoutalpha"] = 0.35,
				},
				["Default"] = {
					["enabled"] = false,
					["version"] = 3,
					["position"] = {
						["y"] = 41.99996038079189,
						["x"] = -198.7999336302273,
						["point"] = "CENTER",
						["scale"] = 0.800000011920929,
					},
				},
				["Thormonde - Sargeras"] = {
					["padding"] = -2,
					["version"] = 3,
					["position"] = {
						["y"] = 41.75,
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
			},
		},
		["XPBar"] = {
			["profiles"] = {
				["Thorjitsu - Sargeras"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 58,
						["x"] = -687.675024747851,
						["point"] = "BOTTOM",
						["scale"] = 1.32500004768372,
					},
				},
				["Basic UI PC"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 21.2000007629395,
						["x"] = -3.90869586825176,
						["point"] = "BOTTOMLEFT",
						["scale"] = 1.32500004768372,
					},
				},
				["MAGE"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 57,
						["x"] = -516,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 58,
						["x"] = -687.675024747851,
						["point"] = "BOTTOM",
						["scale"] = 1.32500004768372,
					},
				},
				["Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 57,
						["x"] = -516,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 58,
						["x"] = -687.675024747851,
						["point"] = "BOTTOM",
						["scale"] = 1.32500004768372,
					},
				},
			},
		},
		["APBar"] = {
			["profiles"] = {
				["Thorjitsu - Sargeras"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 40,
						["x"] = -690.270022273062,
						["point"] = "BOTTOM",
						["scale"] = 1.33000004291534,
					},
				},
				["Basic UI PC"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 21.2799994182586,
						["x"] = -9.07875307767472,
						["point"] = "BOTTOMLEFT",
						["scale"] = 1.33000004291534,
					},
					["clickthrough"] = true,
					["visibility"] = {
						["combat"] = true,
					},
				},
				["MAGE"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 73,
						["x"] = -516,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 40,
						["x"] = -690.270022273062,
						["point"] = "BOTTOM",
						["scale"] = 1.33000004291534,
					},
				},
				["Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 73,
						["x"] = -516,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["enabled"] = true,
					["clickthrough"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 40,
						["x"] = -690.270022273062,
						["point"] = "BOTTOM",
						["scale"] = 1.33000004291534,
					},
				},
			},
		},
		["BlizzardArt"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Senithor - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Thorpez - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Odintsun - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Thorpawz - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC"] = {
					["artLayout"] = "ONEBAR",
					["position"] = {
						["y"] = 7,
						["x"] = -344.9249573764973,
						["point"] = "BOTTOM",
						["scale"] = 1.350000023841858,
					},
					["version"] = 3,
				},
				["Iamthorr - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Thornarii - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Marraydel - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Norvah - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Aatala - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Rosselis - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["artLayout"] = "ONEBAR",
					["version"] = 3,
					["position"] = {
						["y"] = -48.4999847412109,
						["x"] = -295.000183105469,
						["point"] = "CENTER",
					},
				},
				["Anthienisse - Illidan"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI"] = {
					["artLayout"] = "ONEBAR",
					["version"] = 3,
					["position"] = {
						["y"] = -48.4999847412109,
						["x"] = -295.000183105469,
						["point"] = "CENTER",
					},
				},
				["Default"] = {
					["enabled"] = true,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
					["version"] = 3,
					["artLayout"] = "TWOBAR",
				},
				["Thormonde - Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["Vehicle"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50003051757813,
						["x"] = 104.5000915527344,
						["point"] = "CENTER",
					},
				},
				["Senithor - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Thorpez - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Odintsun - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Thorpawz - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Basic UI PC"] = {
					["position"] = {
						["y"] = -221.1666259765625,
						["x"] = 96.36663818359375,
						["point"] = "CENTER",
					},
					["version"] = 3,
				},
				["Iamthorr - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50003051757813,
						["x"] = 104.5000915527344,
						["point"] = "CENTER",
					},
				},
				["Thornarii - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Marraydel - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50003051757813,
						["x"] = 104.5000915527344,
						["point"] = "CENTER",
					},
				},
				["Norvah - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50003051757813,
						["x"] = 104.5000915527344,
						["point"] = "CENTER",
					},
				},
				["Aatala - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Rosselis - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -101.499954223633,
						["x"] = 103.499816894531,
						["point"] = "CENTER",
					},
				},
				["Anthienisse - Illidan"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50003051757813,
						["x"] = 104.5000915527344,
						["point"] = "CENTER",
					},
				},
				["Basic UI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -101.499954223633,
						["x"] = 103.499816894531,
						["point"] = "CENTER",
					},
				},
				["Default"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
				["Thormonde - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 42.50006103515625,
						["x"] = 104.4998779296875,
						["point"] = "CENTER",
					},
				},
			},
		},
		["BagBar"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Senithor - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Thorpez - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Odintsun - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Thorpawz - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC"] = {
					["fadeoutdelay"] = 0.1,
					["enabled"] = false,
					["onebag"] = true,
					["fadeoutalpha"] = 0.25,
					["position"] = {
						["y"] = 112.89054107666,
						["x"] = -36.0001220703125,
						["point"] = "BOTTOMRIGHT",
					},
					["visibility"] = {
						["combat"] = true,
						["vehicle"] = false,
						["vehicleui"] = false,
					},
					["version"] = 3,
				},
				["Iamthorr - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Thornarii - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Marraydel - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Norvah - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Aatala - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Rosselis - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["fadeout"] = true,
					["position"] = {
						["y"] = 90,
						["x"] = -164.000122070313,
						["point"] = "BOTTOMRIGHT",
					},
					["version"] = 3,
					["fadeoutalpha"] = 0.25,
					["visibility"] = {
						["vehicle"] = false,
						["combat"] = false,
						["vehicleui"] = false,
					},
					["fadeoutdelay"] = 0.1,
				},
				["Anthienisse - Illidan"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI"] = {
					["fadeoutdelay"] = 0.1,
					["position"] = {
						["y"] = 90,
						["x"] = -164.000122070313,
						["point"] = "BOTTOMRIGHT",
					},
					["version"] = 3,
					["fadeoutalpha"] = 0.25,
					["visibility"] = {
						["vehicle"] = false,
						["combat"] = false,
						["vehicleui"] = false,
					},
					["fadeout"] = true,
				},
				["Default"] = {
					["enabled"] = false,
					["version"] = 3,
					["position"] = {
						["y"] = 1.499969482421875,
						["x"] = 58.49993896484375,
						["point"] = "CENTER",
					},
				},
				["Thormonde - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["StanceBar"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99998092651367,
						["x"] = -82.49998474121094,
						["point"] = "CENTER",
					},
				},
				["Senithor - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Thorpez - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Odintsun - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Thorpawz - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Basic UI PC"] = {
					["position"] = {
						["y"] = 162.65051651001,
						["x"] = 40.6247406005859,
						["point"] = "BOTTOMLEFT",
					},
					["version"] = 3,
					["visibility"] = {
						["always"] = true,
					},
					["clickthrough"] = true,
				},
				["Iamthorr - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99998092651367,
						["x"] = -82.49998474121094,
						["point"] = "CENTER",
					},
				},
				["Thornarii - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Marraydel - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99998092651367,
						["x"] = -82.49998474121094,
						["point"] = "CENTER",
					},
				},
				["Norvah - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99998092651367,
						["x"] = -82.49998474121094,
						["point"] = "CENTER",
					},
				},
				["Aatala - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Rosselis - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -15,
						["x"] = -82.50007629394531,
						["point"] = "CENTER",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["version"] = 3,
					["clickthrough"] = true,
					["visibility"] = {
						["always"] = true,
					},
					["position"] = {
						["y"] = 162.65051651001,
						["x"] = 40.6247406005859,
						["point"] = "BOTTOMLEFT",
					},
				},
				["Anthienisse - Illidan"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -14.99998092651367,
						["x"] = -82.49998474121094,
						["point"] = "CENTER",
					},
				},
				["Basic UI"] = {
					["version"] = 3,
					["clickthrough"] = true,
					["visibility"] = {
						["always"] = true,
					},
					["position"] = {
						["y"] = 162.65051651001,
						["x"] = 40.6247406005859,
						["point"] = "BOTTOMLEFT",
					},
				},
				["Default"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 135,
						["x"] = -460,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
				["Thormonde - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
						["scale"] = 1,
					},
				},
			},
		},
		["PetBar"] = {
			["profiles"] = {
				["Laviceyi - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Senithor - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Thorpez - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Odintsun - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Thorpawz - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -239.7135009765625,
						["x"] = -601.1741027832031,
						["point"] = "CENTER",
					},
				},
				["Iamthorr - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Thornarii - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Thorwyyn - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Marraydel - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Norvah - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Aatala - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Rosselis - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -78.5000305175781,
						["x"] = -2.99999213218689,
						["point"] = "LEFT",
					},
				},
				["Anthienisse - Illidan"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = -78.5000305175781,
						["x"] = -2.99999213218689,
						["point"] = "LEFT",
					},
				},
				["Default"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 135,
						["x"] = -120,
						["point"] = "BOTTOM",
					},
				},
				["Thormonde - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 143,
						["x"] = -120,
						["point"] = "BOTTOM",
					},
				},
			},
		},
		["RepBar"] = {
			["profiles"] = {
				["Thorjitsu - Sargeras"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 111.820520728494,
						["x"] = -166.250015511513,
						["point"] = "BOTTOMLEFT",
						["scale"] = 1.33000004291534,
					},
				},
				["Basic UI PC"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 111.820520728494,
						["x"] = -166.250015511513,
						["point"] = "BOTTOMLEFT",
						["scale"] = 1.33000004291534,
					},
				},
				["MAGE"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 65,
						["x"] = -516,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 111.820520728494,
						["x"] = -166.250015511513,
						["point"] = "BOTTOMLEFT",
						["scale"] = 1.33000004291534,
					},
				},
				["Sargeras"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["y"] = 65,
						["x"] = -516,
						["point"] = "BOTTOM",
					},
				},
				["Basic UI PC (Edit Mode)"] = {
					["version"] = 3,
					["position"] = {
						["y"] = 111.820520728494,
						["x"] = -166.250015511513,
						["point"] = "BOTTOMLEFT",
						["scale"] = 1.33000004291534,
					},
				},
			},
		},
	},
	["profileKeys"] = {
		["Thordaris - Sargeras"] = "Default",
		["Thornarii - Sargeras"] = "Basic UI PC",
		["Senithor - Sargeras"] = "Senithor - Sargeras",
		["Thorrsmash - Sargeras"] = "Basic UI PC",
		["Thorpawz - Sargeras"] = "Thorpawz - Sargeras",
		["Thorrdin - Sargeras"] = "Basic UI PC",
		["Thorpez - Sargeras"] = "Thorpez - Sargeras",
		["Thorwin - Sargeras"] = "Basic UI PC",
		["Thorrand - Sargeras"] = "Basic UI PC",
		["Thorjitsu - Sargeras"] = "Basic UI PC",
		["Thormonde - Sargeras"] = "Basic UI PC",
		["Thornna - Sargeras"] = "Basic UI PC",
		["Odintsun - Sargeras"] = "Basic UI PC",
		["Thorrend - Sargeras"] = "Default",
		["Thorend - Illidan"] = "Basic UI PC (Edit Mode)",
		["Thorlexi - Sargeras"] = "Basic UI PC",
		["Sairrus - Sargeras"] = "Default",
		["Thorddin - Sargeras"] = "Basic UI PC",
		["Thorwyyn - Sargeras"] = "Thorwyyn - Sargeras",
		["Thordren - Sargeras"] = "Basic UI PC",
		["Iamthorr - Sargeras"] = "Default",
		["Sneakythorr - Sargeras"] = "Basic UI PC",
		["Rosselis - Sargeras"] = "Rosselis - Sargeras",
		["Thorvinn - Sargeras"] = "Basic UI",
		["Thornoxnun - Sargeras"] = "Basic UI PC",
		["Aatala - Sargeras"] = "Aatala - Sargeras",
		["Norvah - Sargeras"] = "Default",
		["Thorraddin - Sargeras"] = "Default",
		["Anthienisse - Illidan"] = "Anthienisse - Illidan",
		["Marraydel - Sargeras"] = "Marraydel - Sargeras",
		["Ashtali - Sargeras"] = "Default",
		["Laviceyi - Sargeras"] = "Default",
	},
	["profiles"] = {
		["Laviceyi - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Senithor - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Thorpez - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Odintsun - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Thorpawz - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Basic UI PC"] = {
			["blizzardVehicle"] = true,
			["focuscastmodifier"] = false,
			["minimapIcon"] = {
				["minimapPos"] = 349.370196808681,
				["hide"] = false,
			},
			["outofrange"] = "hotkey",
		},
		["Iamthorr - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Thornarii - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Thorwyyn - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Marraydel - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Norvah - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Aatala - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Rosselis - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Basic UI PC (Edit Mode)"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["minimapIcon"] = {
				["minimapPos"] = 202.694945456346,
				["hide"] = false,
			},
			["outofrange"] = "hotkey",
		},
		["Anthienisse - Illidan"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Basic UI"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["minimapIcon"] = {
				["hide"] = false,
			},
			["outofrange"] = "hotkey",
		},
		["Default"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
		["Thormonde - Sargeras"] = {
			["focuscastmodifier"] = false,
			["blizzardVehicle"] = true,
			["outofrange"] = "hotkey",
		},
	},
}
