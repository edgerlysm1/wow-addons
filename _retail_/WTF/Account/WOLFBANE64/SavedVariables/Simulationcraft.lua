
SimulationCraftDB = {
	["profileKeys"] = {
		["Thordaris - Sargeras"] = "Thordaris - Sargeras",
		["Thornarii - Sargeras"] = "Thornarii - Sargeras",
		["Senithor - Sargeras"] = "Senithor - Sargeras",
		["Thorggar - Sargeras"] = "Thorggar - Sargeras",
		["Thorlexi - Sargeras"] = "Thorlexi - Sargeras",
		["Thorddin - Sargeras"] = "Thorddin - Sargeras",
		["Thorpez - Sargeras"] = "Thorpez - Sargeras",
		["Thorwin - Sargeras"] = "Thorwin - Sargeras",
		["Ashtali - Sargeras"] = "Ashtali - Sargeras",
		["Thorjitsu - Sargeras"] = "Thorjitsu - Sargeras",
		["Haraklem - Sargeras"] = "Haraklem - Sargeras",
		["Thornna - Sargeras"] = "Thornna - Sargeras",
		["Odintsun - Sargeras"] = "Odintsun - Sargeras",
		["Anthienisse - Illidan"] = "Anthienisse - Illidan",
		["Tircathas - Sargeras"] = "Tircathas - Sargeras",
		["Saelorelea - Sargeras"] = "Saelorelea - Sargeras",
		["Thorpawz - Sargeras"] = "Thorpawz - Sargeras",
		["Thormonde - Sargeras"] = "Thormonde - Sargeras",
		["Thorwyyn - Sargeras"] = "Thorwyyn - Sargeras",
		["Thordren - Sargeras"] = "Thordren - Sargeras",
		["Iamthorr - Sargeras"] = "Iamthorr - Sargeras",
		["Sneakythorr - Sargeras"] = "Sneakythorr - Sargeras",
		["Norvah - Sargeras"] = "Norvah - Sargeras",
		["Thorrend - Sargeras"] = "Thorrend - Sargeras",
		["Thornoxnun - Sargeras"] = "Thornoxnun - Sargeras",
		["Aatala - Sargeras"] = "Aatala - Sargeras",
		["Rosselis - Sargeras"] = "Rosselis - Sargeras",
		["Thorjutsu - Sargeras"] = "Thorjutsu - Sargeras",
		["Sairrus - Sargeras"] = "Sairrus - Sargeras",
		["Ochla - Sargeras"] = "Ochla - Sargeras",
		["Nntailfa - Sargeras"] = "Nntailfa - Sargeras",
		["Thorraddin - Sargeras"] = "Thorraddin - Sargeras",
	},
	["profiles"] = {
		["Thordaris - Sargeras"] = {
		},
		["Thornarii - Sargeras"] = {
		},
		["Senithor - Sargeras"] = {
		},
		["Thorggar - Sargeras"] = {
		},
		["Thorlexi - Sargeras"] = {
		},
		["Thorddin - Sargeras"] = {
		},
		["Thorpez - Sargeras"] = {
		},
		["Thorwin - Sargeras"] = {
		},
		["Ashtali - Sargeras"] = {
		},
		["Thorjitsu - Sargeras"] = {
		},
		["Haraklem - Sargeras"] = {
		},
		["Thornna - Sargeras"] = {
		},
		["Odintsun - Sargeras"] = {
		},
		["Anthienisse - Illidan"] = {
		},
		["Tircathas - Sargeras"] = {
		},
		["Saelorelea - Sargeras"] = {
		},
		["Thorpawz - Sargeras"] = {
		},
		["Thormonde - Sargeras"] = {
		},
		["Thorwyyn - Sargeras"] = {
		},
		["Thordren - Sargeras"] = {
		},
		["Iamthorr - Sargeras"] = {
		},
		["Sneakythorr - Sargeras"] = {
		},
		["Norvah - Sargeras"] = {
		},
		["Thorrend - Sargeras"] = {
		},
		["Thornoxnun - Sargeras"] = {
		},
		["Aatala - Sargeras"] = {
		},
		["Rosselis - Sargeras"] = {
		},
		["Thorjutsu - Sargeras"] = {
		},
		["Sairrus - Sargeras"] = {
		},
		["Ochla - Sargeras"] = {
		},
		["Nntailfa - Sargeras"] = {
		},
		["Thorraddin - Sargeras"] = {
		},
	},
}
