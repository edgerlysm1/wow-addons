
PawnOptions = {
	["LastVersion"] = 2.0311,
	["LastPlayerFullName"] = "Anthienisse-Illidan",
	["AutoSelectScales"] = true,
	["ItemLevels"] = {
		{
			["ID"] = 122250,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122250::::::::4:64:::1:3592:::|h[Tattered Dreadmist Mask]|h|r",
		}, -- [1]
		{
			["ID"] = 122664,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122664::::::::4:64:::1:3592:::|h[Eternal Horizon Choker]|h|r",
		}, -- [2]
		{
			["ID"] = 122360,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122360::::::::4:64:::1:582:::|h[Tattered Dreadmist Mantle]|h|r",
		}, -- [3]
		nil, -- [4]
		{
			["ID"] = 122384,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122384::::::::4:64:::1:3592:::|h[Tattered Dreadmist Robe]|h|r",
		}, -- [5]
		nil, -- [6]
		{
			["ID"] = 122256,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122256::::::::4:64:::1:3592:::|h[Tattered Dreadmist Leggings]|h|r",
		}, -- [7]
		{
			["ID"] = 20895,
			["Level"] = 1,
			["Link"] = "|cffffffff|Hitem:20895::::::::4:64::::::|h[Apprentice's Boots]|h|r",
		}, -- [8]
		nil, -- [9]
		nil, -- [10]
		nil, -- [11]
		nil, -- [12]
		{
			["ID"] = 122362,
			["Level"] = 8,
			["AlsoFitsIn"] = 14,
			["Link"] = "|cff00ccff|Hitem:122362::::::::4:64:::1:3592:::|h[Discerning Eye of the Beast]|h|r",
		}, -- [13]
		nil, -- [14]
		{
			["ID"] = 122262,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122262::::::::4:64:::1:3592:::|h[Ancient Bloodmoon Cloak]|h|r",
		}, -- [15]
		{
			["ID"] = 122353,
			["Level"] = 8,
			["Link"] = "|cff00ccff|Hitem:122353::::::::4:64:::1:3592:::|h[Dignified Headmaster's Charge]|h|r",
		}, -- [16]
	},
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "MAGE",
	["LastAdded"] = 1,
}
