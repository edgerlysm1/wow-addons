
PawnOptions = {
	["LastVersion"] = 2.0214,
	["LastPlayerFullName"] = "Thorend-Illidan",
	["AutoSelectScales"] = true,
	["UpgradeTracking"] = false,
	["LastKeybindingsSet"] = 1,
}
PawnMrRobotScaleProviderOptions = {
	["LastClass"] = "PALADIN",
	["LastAdded"] = 1,
}
