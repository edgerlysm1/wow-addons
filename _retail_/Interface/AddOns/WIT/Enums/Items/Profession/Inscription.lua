local WIT, core = ...

core.Items.Inscription = {

    -- Vanilla 

    MoonglowInk = 39469, 
    MidnightInk = 39774,
    HuntersInk = 43115,
    LionsInk = 43116,
    DawnstarInk = 43117,
    JadefireInk = 43118,
    RoyalInk = 43119,
    CelestialInk = 43120,
    FireyInk = 43121,
    InkOfTheSky = 43123,
    ShimmeringInk = 43122,

    -- TBC
	
	EthrealInk = 43124,
	DarkflameInk = 43125,

    -- WotLK
	
	InkOfTheSea = 43126,
	SnowfallInk = 43127,

    -- Cata
	
	BlackfallowInk = 61978,
	InfernoInk = 61981,

    -- MOP
	
	InkOfDreams = 79254,
	StarlightInk = 79255,

    -- WOD
	
	CeruleanPigment = 114931,

    -- Legion
	
	RoseatePigment = 129032,
	SallowPigment = 129034,

    -- BFA
	
	UltramarineInk = 158187,
	CrimsonInk = 158188,
	ViridecentInk = 158189,
	MaroonInk = 168663,
	
	-- Shadowlands
	
	UmbralInk = 173058,
	TranquilInk = 175970,
	LuminousInk = 173059,
	
	
}