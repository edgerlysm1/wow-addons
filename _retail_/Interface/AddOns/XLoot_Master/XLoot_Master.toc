## Interface: 90001
## Title: XLoot Master
## Notes: Configurable Master Looter interface
## Version: 9.0-7
## Author: Xuerian, Dridzt
## X-Category: Inventory

## Dependencies: XLoot

load.xml
