## Interface: 90001
## Title: XLoot
## Notes: Core module for Loot and Looting-related UI improvements
## Version: 9.0-7
## Author: Xuerian
## X-Category: Inventory
## X-Name: XLoot

## SavedVariables: XLootADB
## OptionalDeps: Ace3, LibStub, ButtonFacade, Masque, Pawn

#@no-lib-strip@
Libs\Embeds.xml
#@end-no-lib-strip@

localization.lua
XLoot.lua
skins.lua
SKIN_TWEAKS.lua
stacks.lua
helpers.lua


