## Interface: 90001
## Title: XLoot Group
## Notes: Customizable and skinnable loot roll, alert, and reroll frames
## Version: 9.0-7
## Author: Xuerian
## X-Category: Inventory

## Dependencies: XLoot

load.xml
