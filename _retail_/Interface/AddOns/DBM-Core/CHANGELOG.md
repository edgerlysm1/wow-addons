# Deadly Boss Mods Core

## [9.0.25-15-g1413ff8](https://github.com/DeadlyBossMods/DeadlyBossMods/tree/1413ff8a42a29453087634e4f4142a504b0e92b8) (2021-04-13)
[Full Changelog](https://github.com/DeadlyBossMods/DeadlyBossMods/compare/9.0.25...1413ff8a42a29453087634e4f4142a504b0e92b8) [Previous Releases](https://github.com/DeadlyBossMods/DeadlyBossMods/releases)

- redundancy  
- Hunters have tranq shot, which removed magic. Fixes #562 (#563)  
    * Hunters have tranq shot, which removed magic. Fixes #562  
- Sort per-bar (#561)  
- De-bom files (#560)  
- Smarter IsSpellID (#559)  
- de-BOM and make sure all toc consistently use CRLF while at it since they were half unix and have windows  
- improve eruption emote whisper backup with additional detection spellID. I've still never seen the UNIT\_TARGET scan fail, but some user reports claimed they didn't get alerts for spell so maybe this will solve problem for them now that backup event is more robust  
- Backport fix (#558)  
- Smarter expansion naming (#553)  
    * Smarter expansion naming  
    Also remove LDB\_CAT_ locales, as they're completely unused (even by pseudo LDB code)  
- Fix Luacheck (#557)  
- Changed client check logic to not flag two clients as valid  
- Fix bar sorting (Force updates on cases we missed) (#552)  
- Register all boss unit IDs on generals, in the super off chance adds somehow steal positoiin 1 or 2 (i've never seen it happen, but doesn't hurt i guess to just scan them all)  
- Adjustments to mortanis  
- Bump alpha  
