## Interface: 90001
## Title: XLoot Frame
## Notes: A customizable, skinnable loot frame.
## Version: 9.0-7
## Author: Xuerian
## X-Category: Inventory

## Dependencies: XLoot

load.xml
