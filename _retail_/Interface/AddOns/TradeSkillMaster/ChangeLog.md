## v4.10.25 Changes

* [Retail] Removed some bonus IDs which were added in the previous version which have no effect on items

[Known Issues](http://support.tradeskillmaster.com/display/KB/TSM4+Currently+Known+Issues)
