## Interface: 90001
## Title: XLoot Options
## Notes: Configuration interface for XLoot
## Version: 9.0-7
## Author: Xuerian
## X-Category: Inventory

## Dependencies: XLoot
## LoadOnDemand: 1

load.xml
