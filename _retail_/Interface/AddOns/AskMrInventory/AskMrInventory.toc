﻿## Interface: 70000
## X-Curse-Project-Name: AskMrInventory

## Title: Ark Inventory [Rules] AskMrRobot Rule
## Notes: AskMrRobot rule for ArkInventory
## Author: 3Doubloons
## Version: 10000

## LoadOnDemand: 0
## RequiredDeps: ArkInventoryRules, AskMrRobot

## LoadManagers: AddonLoader
## X-LoadOn-Always:delayed

Libs\LibStub\LibStub.lua
Libs\AceAddon-3.0\AceAddon-3.0.xml

AskMrInventory.lua