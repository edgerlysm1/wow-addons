﻿-- License: LGPL v2.1 (this file specifically)
local Amr = LibStub("AceAddon-3.0"):GetAddon("AskMrRobot")

local rule = ArkInventoryRules:NewModule( "ArkInventoryRules_AskMrRobot" )

local AllSpecs = {}

function rule:OnEnable( )
	local Amr_ImportCharacter = Amr.ImportCharacter
	Amr.ImportCharacter = function(...)
		ret = Amr_ImportCharacter(...)
		ArkInventory.ItemCacheClear( )
		return ret
	end

	for i = 1, GetNumSpecializations() do
		AllSpecs[i] = i
	end
	
	local registered
	
	registered = ArkInventoryRules.Register( self, "amr", rule.execute_amr )
	
	-- note: if you require another mod to be loaded you will need to add it in the .toc file
	-- in which case make sure you check that that mod actually got loaded (it might not be installed)
	
end

function rule.execute_amr( ... )
	
	-- always check for the hyperlink and that it's an actual item, not a spell (pet/mount)
	if not ArkInventoryRules.Object.h or ArkInventoryRules.Object.class ~= "item" then
		return false
	end
	
	local fn = "amr" -- your rule function name, needs to be set so that error messages are readable
	
	local ac = select( '#', ... )
	local specs = {}
	
	if ac == 0 then
		specs = AllSpecs
	end
	
	for ax = 1, ac do -- loop through the supplied ... arguments
		local arg = select( ax, ... )
		if type( arg ) == "number" then
			table.insert(specs, arg )
		else 
			error( string.format( ArkInventory.Localise["RULE_FAILED_ARGUMENT_IS_NOT"], fn, ax, ArkInventory.Localise["NUMBER"] ), 0 )
		end
	end
	
	for _, spec in pairs(specs) do
		if (Amr.db.char.ExtraItemData[spec] or {})[ArkInventoryRules.Object.info.id] then
			return true
		end
	end
	
	-- always return false at the end
	return false
	
end