# [3.2.3](https://github.com/WeakAuras/WeakAuras2/tree/3.2.3) (2021-04-11)

[Full Changelog](https://github.com/WeakAuras/WeakAuras2/compare/3.2.2...3.2.3)

## Highlights

 - Bug fixes
- New border shapes 

## Commits

InfusOnWoW (4):

- Don't reset Search filter for most aura changes
- Distinguish between Rated and Unrated PVP in Instance Size Type
- Update GCD if the duration changes
- BT2: Show spell name to exact spell id field

Stanzilla (3):

- Update WeakAurasModelPaths from wow.tools
- Switch to the packager's new multi toc feature
- Update WeakAurasModelPaths from wow.tools

asaka-wa (1):

- Add border shapes (#3012)

mrbuds (2):

- ensure a few "percentX" property doesn't return a boolean fix type error when they are use in conditions
- fix potential nil error in dbm & bw triggers fixes #2982

