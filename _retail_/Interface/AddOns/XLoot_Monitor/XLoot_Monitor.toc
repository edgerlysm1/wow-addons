## Interface: 90001
## Title: XLoot Monitor
## Notes: A graphical presentation of items looted by yourself and your group
## Version: 9.0-7
## Author: Xuerian
## X-Category: Inventory

## Dependencies: XLoot

load.xml
