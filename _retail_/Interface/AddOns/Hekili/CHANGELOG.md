# Hekili

## [v9.0.5-1.0.6](https://github.com/Hekili/hekili/tree/v9.0.5-1.0.6) (2021-04-04)
[Full Changelog](https://github.com/Hekili/hekili/compare/v9.0.5-1.0.5...v9.0.5-1.0.6) [Previous Releases](https://github.com/Hekili/hekili/releases)

- Affliction:  Make it slightly easier to replicate holding MR when PS is toggled off (PS is still not on a toggle by default).  
- Affliction:  Fix updated APL.  
- Support trinket.X.ready\_cooldown (fixes Balance CD issue).  
